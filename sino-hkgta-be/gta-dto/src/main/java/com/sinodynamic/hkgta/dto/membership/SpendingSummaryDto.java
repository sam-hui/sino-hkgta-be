package com.sinodynamic.hkgta.dto.membership;

import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class SpendingSummaryDto {
	private Date orderDate;
	private String academyNo;
	private String member;
	private String description;
	private BigDecimal spending;
	private BigDecimal totalSpending;
	private String itemCatagory;
	public String getOrderDate() {
		if (null == orderDate) {
			return "";
		}
		return DtoHelper.date2String(orderDate,"yyyy-MM-dd HH:mm:ss");
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getAcademyNo() {
		return academyNo;
	}
	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}
	public String getMember() {
		return DtoHelper.nvl(member);
	}
	public void setMember(String member) {
		this.member = member;
	}
	public String getDescription() {
		return DtoHelper.nvl(description);
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getSpending() {
		return spending;
	}
	public void setSpending(BigDecimal spending) {
		this.spending = spending;
	}
	public BigDecimal getTotalSpending() {
		return totalSpending;
	}
	public void setTotalSpending(BigDecimal totalSpending) {
		this.totalSpending = totalSpending;
	}
	public String getItemCatagory() {
		return itemCatagory;
	}
	public void setItemCatagory(String itemCatagory) {
		this.itemCatagory = itemCatagory;
	}
	
}
