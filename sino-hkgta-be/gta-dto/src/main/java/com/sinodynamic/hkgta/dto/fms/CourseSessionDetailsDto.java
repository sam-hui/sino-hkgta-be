package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class CourseSessionDetailsDto implements Serializable {
    private static final long serialVersionUID = 1L;

    	private Long sysId;
	private Date beginDatetime;
	private Long coachTimeslotId;
	private String createBy;
	private Timestamp createDate;
	private Date endDatetime;
	private Long sessionNo;
	private String updateBy;
	private Date updateDate;
	private Long courseId;
	private String gatherLocation;
	private String status;
	private String coach;
	private String otherTrainLocation;
	
	
	public Long getSysId() {
		return sysId;
	}
	public void setSysId(Object sysId) {
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}
	public Date getBeginDatetime() {
		return beginDatetime;
	}
	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}
	public Long getCoachTimeslotId() {
		return coachTimeslotId;
	}
	public void setCoachTimeslotId(Object coachTimeslotId) {
		this.coachTimeslotId = (coachTimeslotId!=null ? NumberUtils.toLong(coachTimeslotId.toString()):null);
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public Date getEndDatetime() {
		return endDatetime;
	}
	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}
	public Long getSessionNo() {
		return sessionNo;
	}
	public void setSessionNo(Object sessionNo) {
		this.sessionNo = (sessionNo!=null ? NumberUtils.toLong(sessionNo.toString()):null);
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Object courseId) {
		this.courseId = (courseId!=null ? NumberUtils.toLong(courseId.toString()):null);
	}
	public String getGatherLocation() {
		return gatherLocation;
	}
	public void setGatherLocation(String gatherLocation) {
		this.gatherLocation = gatherLocation;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getOtherTrainLocation() {
	    return otherTrainLocation;
	}
	public void setOtherTrainLocation(String otherTrainLocation) {
	    this.otherTrainLocation = otherTrainLocation;
	}
	
	
	

}
