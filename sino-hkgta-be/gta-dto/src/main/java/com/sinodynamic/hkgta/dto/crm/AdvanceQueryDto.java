package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.List;

/**
 * @author Angus_Zhu
 */
public class AdvanceQueryDto implements Serializable {

	private static final long serialVersionUID = 1L;

	public String groupOp;
	public List<AdvanceQueryDto> groups;
	public List<SearchRuleDto> rules;

	public String sortBy;
	
	public String ascending;
	
	public String codeSortBy;
	
	public String codeAscending;
	
	
	public String getGroupOp() {
		return groupOp;
	}

	public void setGroupOp(String groupOp) {
		this.groupOp = groupOp;
	}

	public List<AdvanceQueryDto> getGroups() {
		return groups;
	}

	public void setGroups(List<AdvanceQueryDto> groups) {
		this.groups = groups;
	}

	public List<SearchRuleDto> getRules() {
		return rules;
	}

	public void setRules(List<SearchRuleDto> rules) {
		this.rules = rules;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getAscending() {
		return ascending;
	}

	public void setAscending(String ascending) {
		this.ascending = ascending;
	}

	public String getCodeSortBy() {
		return codeSortBy;
	}

	public void setCodeSortBy(String codeSortBy) {
		this.codeSortBy = codeSortBy;
	}

	public String getCodeAscending() {
		return codeAscending;
	}

	public void setCodeAscending(String codeAscending) {
		this.codeAscending = codeAscending;
	}




}
