package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class RatePlanDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8977264012936137377L;
	private String ratePlanCode;
	private String ratePlanName;

	public String getRatePlanCode() {
		return ratePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}

	public String getRatePlanName() {
		return ratePlanName;
	}

	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
