package com.sinodynamic.hkgta.dto.crm;

import java.math.BigInteger;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class EnrollmentListResponseDto {
	private BigInteger customerId;
	private String enrollName;
	private String status;
	private Date enrollDate;
	private String servicePlan;
	private BigInteger enrollId;
	private String contactEmail;
	private boolean unRead;
	private String salesPerson;
	private String companyName;
	private Long planNo;
	private boolean dependentCreationRight;
	private Date effectiveDate;
	
	public BigInteger getEnrollId() {
		return enrollId;
	}
	public void setEnrollId(BigInteger enrollId) {
		this.enrollId = enrollId;
	}
	public String getContactEmail() {
		return DtoHelper.nvl(contactEmail);
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public BigInteger getCustomerId() {
		return customerId;
	}
	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}
	public String getEnrollName() {
		return DtoHelper.nvl(enrollName);
	}
	public void setEnrollName(String enrollName) {
		this.enrollName = enrollName;
	}

	public String getStatus() {
		return DtoHelper.nvl(status);
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEnrollDate() {
		if (enrollDate == null) {
			return "";
		}
		return DtoHelper.getYMDDateAndDateDiff(enrollDate);
	}
	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}
	public String getSalesPerson() {
		return DtoHelper.nvl(salesPerson);
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	public String getServicePlan() {
		return DtoHelper.nvl(servicePlan);
	}
	public void setServicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}
	public boolean isUnRead() {
		return unRead;
	}
	public void setUnRead(boolean unRead) {
		this.unRead = unRead;
	}
	public String getCompanyName() {
		return DtoHelper.nvl(companyName);
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getPlanNo() {
		return planNo;
	}
	public void setPlanNo(Object planNo) {
		if(planNo instanceof BigInteger){
			this.planNo = ((BigInteger) planNo).longValue();
		}else if(planNo instanceof Integer){
			this.planNo = ((Integer) planNo).longValue();
		}else if(planNo instanceof String){
			this.planNo = Long.valueOf((String) planNo);
		}else{
			this.planNo = (Long) planNo;
		}
	}
	public boolean isDependentCreationRight() {
		return dependentCreationRight;
	}
	public void setDependentCreationRight(boolean dependentCreationRight) {
		this.dependentCreationRight = dependentCreationRight;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
}
