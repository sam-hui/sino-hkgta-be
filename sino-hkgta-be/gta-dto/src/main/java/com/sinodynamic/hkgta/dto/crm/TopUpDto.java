package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

public class TopUpDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String staffUserId;
	
	private String paymentMethod;

	private BigDecimal topUpAmount;

	private Long customerId;

	private String terminalId;

	private String agentTransactionNo;
	
	private Long transactionNo;
	
	private String topUpLocation;
	
	
	public String getTopUpLocation() {
		return topUpLocation;
	}

	public void setTopUpLocation(String topUpLocation) {
		this.topUpLocation = topUpLocation;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getTopUpAmount() {
		if (topUpAmount == null) {
			topUpAmount = new BigDecimal("0");
		}
		return topUpAmount;
	}

	public void setTopUpAmount(BigDecimal topUpAmount) {
		this.topUpAmount = topUpAmount;

	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getAgentTransactionNo() {
		return agentTransactionNo;
	}

	public void setAgentTransactionNo(String agentTransactionNo) {
		this.agentTransactionNo = agentTransactionNo;
	}

	public String getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public Long getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	
}
