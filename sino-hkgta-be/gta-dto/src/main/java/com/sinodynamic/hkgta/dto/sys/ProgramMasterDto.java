package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;
import java.util.List;

public class ProgramMasterDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4455599471295529864L;
	private String programId;
	private String menuName;
	private String programType;
	private boolean checked;
	private String accessRight;
	private String urlPath;
	private List<ProgramMasterDto> children;
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getProgramType() {
		return programType;
	}
	public void setProgramType(String programType) {
		this.programType = programType;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getAccessRight() {
		return accessRight;
	}
	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}
	public List<ProgramMasterDto> getChildren() {
		return children;
	}
	public void setChildren(List<ProgramMasterDto> children) {
		this.children = children;
	}
	public String getUrlPath() {
		return urlPath;
	}
	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}
	

}
