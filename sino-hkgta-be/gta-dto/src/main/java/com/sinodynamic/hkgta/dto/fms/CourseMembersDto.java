package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

public class CourseMembersDto implements Serializable {

	private static final long serialVersionUID = -1371912768347486929L;

	public CourseMembersDto() {
		super();

	}

	public CourseMembersDto(String courseType, String expired, String status) {
		
	}

	private Long courseId;
	private String courseName;
	private Long capacity;
	private Long enrollment;
	private String memberAcceptance;
	private List<MemberInfoDto> members;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Long capacity) {
		this.capacity = capacity;
	}

	public String getMemberAcceptance() {
		return memberAcceptance;
	}

	public void setMemberAcceptance(String memberAcceptance) {
		this.memberAcceptance = memberAcceptance;
	}

	public List<MemberInfoDto> getMembers() {
		return members;
	}

	public void setMembers(List<MemberInfoDto> members) {
		this.members = members;
	}

	public Long getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Object enrollment) {
		this.enrollment = (enrollment!=null ? NumberUtils.toLong(enrollment.toString()):null);
	}


	
}
