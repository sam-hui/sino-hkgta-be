package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

/**
 * @author Junfeng_Yan
 * @since May 11 2015
 */
public class MemberCardPrintDto implements Serializable{

	private String academyNo;
	
//	private String surName;
//	
//	private String givenName;
	
	
	private String customerName;
	
	private String expiryDate;
	

	public MemberCardPrintDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getAcademyNo() {
		return academyNo;
	}


	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "MemberCardPrintDto [academyNo=" + academyNo + ", customerName="
				+ customerName + ", expiryDate=" + expiryDate + "]";
	}

}
