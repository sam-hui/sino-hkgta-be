package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CustomerOrderTransDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long transactionNo;
	private String agentTransactionNo;
	private BigDecimal paidAmount;
	private String paymentMethodCode;
	private String status;
	private String terminalId;
	private String transactionTimestamp;
	private String transcriptFileName;
	private Long orderNo;
	private String paymentReceivedBy;
	private BigDecimal availableBalance;
	private String redirectUrl;
	private String posItemNo;
	private String readBy;
	private int unReadCount;
	private Boolean read;
	private String createByUserName;
	private String createByUserId;
	private String paymentLocationCode;
	
	public CustomerOrderTransDto() {
	}

	public Long getTransactionNo() {
		return this.transactionNo;
	}

	public void setTransactionNo(Object transactionNo) {
		this.transactionNo = transactionNo != null ? NumberUtils.toLong(transactionNo.toString()) : null;
	}

	public String getAgentTransactionNo() {
		return DtoHelper.nvl(this.agentTransactionNo);
	}

	public void setAgentTransactionNo(String agentTransactionNo) {
		this.agentTransactionNo = agentTransactionNo;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaymentMethodCode() {
		return DtoHelper.nvl(this.paymentMethodCode);
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getStatus() {
		return DtoHelper.nvl(this.status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTerminalId() {
		return DtoHelper.nvl(this.terminalId);
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTransactionTimestamp() {
		return DtoHelper.nvl(this.transactionTimestamp);
	}

	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Object orderNo) {
		this.orderNo = orderNo != null ? NumberUtils.toLong(orderNo.toString()) : null;;
	}

	public String getTranscriptFileName() {
		return DtoHelper.nvl(this.transcriptFileName);
	}

	public void setTranscriptFileName(String transcriptFileName) {
		this.transcriptFileName = transcriptFileName;
	}

	public String getPaymentReceivedBy() {
		return paymentReceivedBy;
	}

	public void setPaymentReceivedBy(String paymentReceivedBy) {
		this.paymentReceivedBy = paymentReceivedBy;
	}

	public BigDecimal getAvailableBalance()
	{
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance)
	{
		this.availableBalance = availableBalance;
	}

	public String getRedirectUrl() {
	    return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
	    this.redirectUrl = redirectUrl;
	}

	public String getPosItemNo() {
	    return posItemNo;
	}

	public void setPosItemNo(String posItemNo) {
	    this.posItemNo = posItemNo;
	}

	public String getReadBy() {
		return readBy;
	}

	public void setReadBy(String readBy) {
		this.readBy = readBy;
	}

	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}
	
	

	public int getUnReadCount() {
		return unReadCount;
	}

	public void setUnReadCount(int unReadCount) {
		this.unReadCount = unReadCount;
	}

	public String getCreateByUserName() {
		return createByUserName;
	}

	public void setCreateByUserName(String createByUserName) {
		this.createByUserName = createByUserName;
	}

	public String getCreateByUserId() {
		return createByUserId;
	}

	public void setCreateByUserId(String createByUserId) {
		this.createByUserId = createByUserId;
	}

	public String getPaymentLocationCode() {
		return paymentLocationCode;
	}

	public void setPaymentLocationCode(String paymentLocationCode) {
		this.paymentLocationCode = paymentLocationCode;
	}
	

}
