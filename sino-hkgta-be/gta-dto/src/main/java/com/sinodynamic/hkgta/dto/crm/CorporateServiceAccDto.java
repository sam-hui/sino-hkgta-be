package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

public class CorporateServiceAccDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int accNo;
	private int corporateId;
	private Date expiryDate;
	private Date effectiveDate;
	private String periodCode;
	
	public String getPeriodCode() {
		return periodCode;
	}
	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}
	public int getAccNo() {
		return accNo;
	}
	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}
	public int getCorporateId() {
		return corporateId;
	}
	public void setCorporateId(int corporateId) {
		this.corporateId = corporateId;
	}
	//	@JsonIgnore
//	public void setCorporateId2(BigInteger corporateId) {
//		if(null !=corporateId) {
//			this.corporateId = corporateId.longValue();
//		}
//	}
//	@JsonIgnore
//	public void setAccNo2(BigInteger accNo) {
//		if(null !=accNo) {
//			this.accNo = accNo.longValue();
//		}
//	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	
}
