package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;


public class CourseEnrollmentDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long courseId;
	private Long customerId;
	private String remark;
	private Boolean isPay;
	private String paymentMethod;
	private String device;
	private String paymentLocation;
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Boolean getIsPay() {
		return isPay;
	}
	public void setIsPay(Boolean isPay) {
		this.isPay = isPay;
	}

	public String getDevice() {
	    return device;
	}

	public void setDevice(String device) {
	    this.device = device;
	}

	public String getPaymentLocation() {
	    return paymentLocation;
	}

	public void setPaymentLocation(String paymentLocation) {
	    this.paymentLocation = paymentLocation;
	}

}

