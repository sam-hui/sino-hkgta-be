package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FacilityMasterChangeDto implements Serializable, IFacilityTimeslotLogDto {

	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private Long srcFacilityNo;
	
	private String srcBookingDate;
	
	private Integer srcStartTime;
	
	private Integer srcEndTime;
	
	private Long targetFacilityNo;
	
	private String targetBookingDate;
	
	private Integer targetStartTime;
	
	private Integer targetEndTime;
	
	private String status;
	
	private String ballFeedingStatus;
	
	private String updateBy;
	
	private String facilityType;
	
	private String remark;

	public Long getSrcFacilityNo() {
		return srcFacilityNo;
	}
	
	@Override
	public Long getFacilityNo() {
		return this.getSrcFacilityNo();
	}

	public void setSrcFacilityNo(Long srcFacilityNo) {
		this.srcFacilityNo = srcFacilityNo;
	}

	public String getSrcBookingDate() {
		return srcBookingDate;
	}

	public void setSrcBookingDate(String srcBookingDate) {
		this.srcBookingDate = srcBookingDate;
	}

	public Integer getSrcStartTime() {
		return srcStartTime;
	}

	public void setSrcStartTime(Integer srcStartTime) {
		this.srcStartTime = srcStartTime;
	}

	public Integer getSrcEndTime() {
		return srcEndTime;
	}

	public void setSrcEndTime(Integer srcEndTime) {
		this.srcEndTime = srcEndTime;
	}

	public Long getTargetFacilityNo() {
		return targetFacilityNo;
	}
	
	public void setTargetFacilityNo(Long targetFacilityNo) {
		this.targetFacilityNo = targetFacilityNo;
	}

	public String getTargetBookingDate() {
		return targetBookingDate;
	}

	public void setTargetBookingDate(String targetBookingDate) {
		this.targetBookingDate = targetBookingDate;
	}

	public Integer getTargetStartTime() {
		return targetStartTime;
	}

	public void setTargetStartTime(Integer targetStartTime) {
		this.targetStartTime = targetStartTime;
	}

	public Integer getTargetEndTime() {
		return targetEndTime;
	}

	public void setTargetEndTime(Integer targetEndTime) {
		this.targetEndTime = targetEndTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBallFeedingStatus() {
		return ballFeedingStatus;
	}

	public void setBallFeedingStatus(String ballFeedingStatus) {
		this.ballFeedingStatus = ballFeedingStatus;
	}

	@Override
	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public Date getBeginDatetime() {
		try {
			return dateFormat.parse(this.srcBookingDate + " " + this.srcStartTime + ":00:00");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Date getEndDatetime() {
		try {
			Date endDt = dateFormat.parse(this.srcBookingDate + " " + this.srcEndTime + ":00:00");
			return new Date(endDt.getTime() + 3600l * 1000l);
		} catch (Exception e) {
			return null;
		}
	}
	
}