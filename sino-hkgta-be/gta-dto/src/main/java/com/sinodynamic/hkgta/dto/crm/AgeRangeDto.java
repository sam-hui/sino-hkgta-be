package com.sinodynamic.hkgta.dto.crm;

public class AgeRangeDto {
	private String ageRangeCode;
	private int ageFrom;
	private int ageTo;
	private String description;
	public String getAgeRangeCode() {
		return ageRangeCode;
	}
	public void setAgeRangeCode(String ageRangeCode) {
		this.ageRangeCode = ageRangeCode;
	}
	public int getAgeFrom() {
		return ageFrom;
	}
	public void setAgeFrom(int ageFrom) {
		this.ageFrom = ageFrom;
	}
	public int getAgeTo() {
		return ageTo;
	}
	public void setAgeTo(int ageTo) {
		this.ageTo = ageTo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
