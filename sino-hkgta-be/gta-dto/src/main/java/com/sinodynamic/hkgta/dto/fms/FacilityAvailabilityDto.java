package com.sinodynamic.hkgta.dto.fms;

public class FacilityAvailabilityDto {
	private int hour;
	private Boolean available;
	public int getHour()
	{
		return hour;
	}
	public void setHour(int hour)
	{
		this.hour = hour;
	}
	public Boolean getAvailable()
	{
		return available;
	}
	public void setAvailable(Boolean available)
	{
		this.available = available;
	}
}
