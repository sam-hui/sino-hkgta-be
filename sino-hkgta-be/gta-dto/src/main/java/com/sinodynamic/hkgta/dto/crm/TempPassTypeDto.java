package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class TempPassTypeDto implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4185462211415858370L;

	private Long tempPassId;
	
	private String tempPassName;
	
	private String status;
	
	private String statusValue;
	
	private String startDate;
	
	private String expiryDate;
	
	
	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	public Long getTempPassId()
	{
		return tempPassId;
	}

	public void setTempPassId(Object tempPassId)
	{
		this.tempPassId = tempPassId != null ? NumberUtils.toLong(tempPassId.toString()) : null;
	}

	public String getTempPassName()
	{
		return tempPassName;
	}

	public void setTempPassName(String tempPassName)
	{
		this.tempPassName = tempPassName;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getExpiryDate()
	{
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate)
	{
		this.expiryDate = expiryDate;
	}

}
