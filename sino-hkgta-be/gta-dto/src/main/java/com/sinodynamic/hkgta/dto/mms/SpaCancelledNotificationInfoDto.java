package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.util.Date;

public class SpaCancelledNotificationInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String userId;
    private Date startDatetime;
    private Date endDatetime;
    private String serviceName;
    private String employFN;
    private String employLN;

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public Date getStartDatetime() {
	return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
	this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
	return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
	this.endDatetime = endDatetime;
    }

    public String getServiceName() {
	return serviceName;
    }

    public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
    }

    public String getEmployFN() {
	return employFN;
    }

    public void setEmployFN(String employFN) {
	this.employFN = employFN;
    }

    public String getEmployLN() {
	return employLN;
    }

    public void setEmployLN(String employLN) {
	this.employLN = employLN;
    }

}
