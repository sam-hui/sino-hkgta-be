package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class FacilityTimeslotQueryDto implements Serializable {

	private static final long serialVersionUID = 430887760348794557L;

	private Long facilityCount;
	private Date beginDatetime;

	public Date getBeginDatetime()
	{
		return beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime)
	{
		this.beginDatetime = beginDatetime;
	}

	public Long getFacilityCount()
	{
		return facilityCount;
	}

	public void setFacilityCount(Object facilityCount)
	{
		this.facilityCount =  (facilityCount!=null ? NumberUtils.toLong(facilityCount.toString()):null);
	}



}
