package com.sinodynamic.hkgta.dto.statement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class StatementDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long customerId;

	private String academyId;

	private String memberName;

	private String memberType;

	private BigDecimal closingBalance;

	private String deliveryStatus;

	private String memberEmail;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if (customerId instanceof BigInteger) {
			this.customerId = ((BigInteger) customerId).longValue();
		} else if (customerId instanceof Integer) {
			this.customerId = ((Integer) customerId).longValue();
		} else if (customerId instanceof String) {
			this.customerId = Long.valueOf((String) customerId);
		} else {
			this.customerId = (Long) customerId;
		}
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public BigDecimal getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}

}
