package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

public class AssignProgramDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -425607631851291110L;
	/**
	 * 
	 */
	private Long roleId;
	private String roleName;
    private String userType;
	private List<ProgramDto> programs;
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Object roleId) {
		this.roleId =  (roleId!=null ? NumberUtils.toLong(roleId.toString()):null);
	}
	public List<ProgramDto> getPrograms() {
		return programs;
	}
	public void setPrograms(List<ProgramDto> programs) {
		this.programs = programs;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUserType()
	{
		return userType;
	}
	public void setUserType(String userType)
	{
		this.userType = userType;
	}
	
	

}
