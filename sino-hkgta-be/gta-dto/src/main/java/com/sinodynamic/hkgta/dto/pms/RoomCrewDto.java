package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RoomCrewDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long crewId;

	private String userId;

	private String title;

	private String userName;

	private Long[] roomIds;

	private String updateBy;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getCrewId() {
		return crewId;
	}

	public void setCrewId(Long crewId) {
		this.crewId = crewId;
	}

	public Long[] getRoomIds() {
		return roomIds;
	}

	public void setRoomIds(Long[] roomIds) {
		this.roomIds = roomIds;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
