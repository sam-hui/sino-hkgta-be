package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RoomResDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4471367304984109417L;
	private String roomTypeCode;
	private String roomRateCode;
	private Long roomBookingCount;
	private Long adultCount;
	private Long childCount;
	/**
	 * yyyy-MM-dd
	 */
	private String startDate;
	/**
	 * yyyy-MM-dd
	 */
	private String endDate;
	/**
	 * "Male","Female","Unknown"
	 */
	private String gender;
	private String namePrefix;
	private String givenName;
	private String surname;
	private String phoneNumber;
	private String email;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String membershipID;
	
	private String reservationID;

	private Long customerId;
	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getRoomRateCode() {
		return roomRateCode;
	}

	public void setRoomRateCode(String roomRateCode) {
		this.roomRateCode = roomRateCode;
	}

	

	public Long getRoomBookingCount() {
		return roomBookingCount;
	}

	public void setRoomBookingCount(Long roomBookingCount) {
		this.roomBookingCount = roomBookingCount;
	}

	

	public Long getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Long adultCount) {
		this.adultCount = adultCount;
	}

	public Long getChildCount() {
		return childCount;
	}

	public void setChildCount(Long childCount) {
		this.childCount = childCount;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNamePrefix() {
		return namePrefix;
	}

	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getMembershipID() {
		return membershipID;
	}

	public void setMembershipID(String membershipID) {
		this.membershipID = membershipID;
	}

	public String getReservationID() {
		return reservationID;
	}

	public void setReservationID(String reservationID) {
		this.reservationID = reservationID;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
