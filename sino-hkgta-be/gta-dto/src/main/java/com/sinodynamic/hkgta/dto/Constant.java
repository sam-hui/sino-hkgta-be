package com.sinodynamic.hkgta.dto;

@Deprecated
public class Constant {

	// NoticeType for email
	public static final String NOTICE_TYPE_PRESENTATION = "P";
	public static final String NOTICE_TYPE_ENROLLMENT = "E";
	public static final String NOTICE_TYPE_RENEWAL = "R";
	public static final String NOTICE_TYPE_TRANSACTION = "T";
	public static final String NOTICE_TYPE_EXTENSION = "X";
	public static final String NOTICE_TYPE_TEMP = "M";
	public static final String NOTICE_TYPE_STAFF = "S";
	public static final String NOTICE_TYPE_FACILITY = "F";
	
	// Service Plan offer code
	public static final String SERVICE_PLAN_OFFER_CODE_RENEWAL = "RENEW";
	public static final String SERVICE_PLAN_TYPE_RENEWAL = "Renewal";
	public static final String SERVICE_PLAN_TYPE_ENROLLMENT = "Enrollment";

	// permit card master status literal
	public static final String PERMIT_CARD_MASTER_OH = "OH";
	public static final String PERMIT_CARD_MASTER_ISS = "ISS";
	public static final String PERMIT_CARD_MASTER_CAN = "CAN";
	public static final String PERMIT_CARD_MASTER_LOST = "LOST";

	public static final String ENROLL_PRICE_PREFIX = "SRV000000";
	public static final String ENROLL_PRICE_ITEM_PREFIX = "SRV";
	public static final String RENEW_PRICE_PREFIX = "RENEW000000";
	public static final String RENEW_PRICE_ITEM_PREFIX = "RENEW";
	public static final String TOPUP_ITEM_NO = "CVT0000001";
	public static final String SRV_HI_PREFIX = "SRVHI000000";
	public static final String SRV_HI_RATE_ITEM_PREFIX = "SRVHI";
	public static final String GSSHI_RATE_ITEM_PREFIX = "GSSHI";
	public static final String TSSHI_RATE_ITEM_PREFIX = "SRVHI";
	public static final String GSSLO_RATE_ITEM_PREFIX = "GSSLO";
	public static final String TSSLO_RATE_ITEM_PREFIX = "SRVLO";
	public static final String FACILITY_PRICE_PREFIX = "FMS";

	public static final String ENROLL_PRICE_DESC_SUFFIX = " Plan - Enrollment";
	public static final String RENEW_PRICE_DESC_SUFFIX = " Plan - Renewal";
	public static final String SPECIAL_PRICE_DESC_SUFFIX = " Plan - Special";

	public static final String TRANSACTION_BALANCEDUE_ALL = "All";

	public static final String TRANSACTION_BALANCEDUE_PENDING = "PENDING";// PendingPayment
	public static final String TRANSACTION_BALANCEDUE_COMPLETED = "COMPLETED";// CompletedPayment

	public static final String DELETE_NO = "N";
	public static final String DELETE_YES = "Y";
	
	public static final String NO = "N";
	public static final String YES = "Y";

	public static final String Member_Status_ACT = "ACT";
	public static final String Member_Status_NACT = "NACT";

	public static final String General_Status_ACT = "ACT";
	public static final String General_Status_NACT = "NACT";
	public static final String General_Status_EXP = "EXP";

	// SFTP literal define
	public static final String SFTP_HOST = "hkgta.virtual.account.sftp.host";
	public static final String SFTP_PORT = "hkgta.virtual.account.sftp.port";
	public static final String SFTP_USERNAME = "hkgta.virtual.account.sftp.username";
	public static final String SFTP_PASSWORD = "hkgta.virtual.account.sftp.password";
	public static final String SFTP_RECEIVE_PATH = "hkgta.virtual.account.sftp.receive.path";
	public static final String SFTP_RECEIVE_BACK_PATH = "hkgta.virtual.account.sftp.receive.back.path";
	public static final String SFTP_TIMEOUT = "hkgta.virtual.account.sftp.timeout";
	public static final String SFTP_SNED_PATH = "hkgta.virtual.account.sftp.upload.path";
	public static final int SFTP_DEFAULT_PORT = 22;
	public static final int SFTP_DEFAULT_TIMEOUT = 60000;

	// CSV file name format
	public static final String CSV_UPLOAD_PATH = "csv.upload.path";
	public static final String CSV_UPLOAD_BACK_PATH = "csv.upload.back.path";
	public static final String CSV_DOWNLOAD_PATH = "csv.download.path";
	public static final String CSV_DOWNLOAD_BACK_PATH = "csv.download.back.path";

	// search sysconfig from global_parameter
	public static final String CLIENT_ID = "ClientID";
	public static final String PHYSICAL_ACCOUNT = "PhysicalAccount";
	public static final String RECORD_CODE = "RecordCode";
	public static final String PAYER_NAME = "PayerName";

	public static final String ROLE_OFFICER = "Manager";
	public static final String ROLE_SALES = "SALES";

	public static final String ADVANCEDRESPERIOD_GOLF = "GolfingBayAdvancedResPeriod";
	public static final String ADVANCEDRESPERIOD_TENNIS = "TennisCourtAdvancedResPeriod";
	public static final String FACILITY_TYPE_GOLF = "GOLF";
	public static final String FACILITY_TYPE_TENNIS = "TENNIS";

	// sort by column for contractor help
	public static final String COL_TEMP_USER = "tempUser";
	public static final String COL_PASS_TYPE = "temporaryPassType";
	public static final String COL_CARD_ID = "cardId";
	public static final String COL_ACT_DATE = "activationDate";
	public static final String COL_DEACT_DATE = "deactivationDate";
	public static final String COL_REFERRAL = "referral";
	public static final String COL_STATUS = "status";

	public static final String LOGIN_FAIL_TRY_TIME = "login.failed.try.times";
	public static final String LOGIN_FAIL_TRY_TIME_LOCK = "login.failed.try.times.lock";
	public static final String LOGIN_FAIL_TRY_PERIOD = "login.failed.try.period";
	public static final String LOGIN_FAIL_LOCK_PERIOD = "login.failed.lock.period";
	public static final String LOGIN_DEVICE_CHECK_LIST = "login.device.check.list";

	public static final String BALLFEEDSWITCH = "BALLFEEDSWITCH";
	public static final int MAX_FACILITY_ADVANCEDRESPERIOD = 1000;
	public static final int MAX_FACILITY_ITEMPRICE = 1000000;

	// sort by column for course list
	public static final String COL_COURSE_ID = "courseId";
	public static final String COL_COURSE_NAME = "courseName";
	public static final String COL_REG_DATE = "regDate";
	public static final String COL_COURSE_PERIOD = "periodStart";
	public static final String COL_COURSE_STATUS = "status";
	public static final String COL_ENROLLMENT = "enrollment";

	public static final String GOLFBAYTYPE = "GOLFBAYTYPE";
	public static final String TENNISCRT = "TENNISCRT";
	
	
	public enum PriceCatagory {
		Enrollment, RENEW, SRV, TOPUP
	}

	public enum RateType {
		HI,LO,OFF
	}
	public enum StaffType {
		/*
		 * PS-Permanent staff; CS- contract staff; PT-part time staff;
		 *  FTR- Full time Tennis Trainer/Coach;PTR-part time tennis trainer/coach; 
		 * FTG - full time golf trainer; PTG - part time golf trainer;
		 */
		PS,CS,PT,
		FTR,PTR,
		FTG,PTG
	}

	public enum Purchase_Daypass_Type {
		Staff, Member
	}

	public enum Purchase_Flag_Type {
		OK, FAIL
	}
	
	public enum Subscriber_Type {
		STF, M
	}
	
	public enum Status {
		ACT, NACT,
		// for table customer_order_hd. OPN-open, CAN-cancelled, CMP-paid and
		// transaction completed, REJ - Rejected
		OPN, CAN, CMP, REJ,
		// for table customer_order_permit_card. ACT-activated; RA - ready to
		// activate (card_no not mapped yet); PND-pending
		RA, PND,
		// for table customer_order_trans. NULL/empty-paid but not confirmed;
		// SUC - payment success; FAIL - payment not success; VOID- paid and
		// success but cancelled finally \n
		SUC, FAIL, VOID,
		RFU, // refund
		QT // quit (terminated/resigned)
	}

	public enum AnnualRepeat {
		Y, N
	}

	public enum ServiceplanType {
		SERVICEPLAN, DAYPASS
	}

	public enum memberType {
		/*
		 * IPM – Individual Primary Member;IDM – Individual Dependent Member;
		 * CPM – Corporate Primary Member; CDM – Corporate Dependent Member; MG
		 * – Member Guest;HG – House Gues
		 */
		IPM, IDM, CPM, CDM, MG,HG
	}

	// spring security authorization filter access right for specific
	// url/program_master
	public static final String SECURITY_ACCESS_RIGHT_ALL = "*";
	public static final String SECURITY_ACCESS_RIGHT_READ = "R";
	public static final String SECURITY_ACCESS_RIGHT_CREATE_UPDATE = "U"; // create
																			// +
																			// update
																			// +
																			// read;
																			// all(*)
																			// except
																			// delete.
	// public static final String SECURITY_ACCESS_RIGHT_DELETE = "D"; //
	// equivalent to ALL(*)
	public static final String DAY_PASS_PURCHASING = "Day Pass Purchasing";

	// payment account type
	public static final String VIRTUAL_ACCOUNT = "VA";
	public static final String BANK_ACCOUNT = "BNK";

	// topup method type
	public static final String VIRTUAL = "VA";
	public static final String CASH = "CASH";
	public static final String CASH_Value = "CV";
	public static final String CREDIT = "CC";
	public static final String CREDIT_CARD = "VISA";

	// virtual acc status
	// AV-available; US-used; NR-Not ready; DEL-deleted
	public static final String VIR_ACC_STATUS_VA = "VA";
	public static final String VIR_ACC_STATUS_US = "US";
	public static final String VIR_ACC_STATUS_NR = "NR";
	public static final String VIR_ACC_STATUS_DEL = "DEL";

	// member payment account status
	public static final String PAYMENT_ACC_STATUS_ACT = "ACT";
	public static final String PAYMENT_ACC_STATUS_NACT = "NACT";

	// staff timeslot status
	// OP-Occupied; LV-on leave; SL-sick leave
	public static final String STAFF_TIMESLOT_OP = "OP";
	public static final String STAFF_TIMESLOT_LV = "LV";
	public static final String STAFF_TIMESLOT_SL = "SL";

	// staff timeslot dutyCategory
	// NULL-no category; TRAINER-Trainer for a course; CLNROOM- Room cleaning;
	// MEET-meeting; .....etc
	public static final String STAFF_TIMESLOT_TRAINER = "TRAINER";
	public static final String STAFF_TIMESLOT_CLNROOM = "CLNROOM";
	public static final String STAFF_TIMESLOT_MEET = "MEET";

	// staff timeslot dutyDescription
	public static final String STAFF_TIMESLOT_CATEGORY_DESCRIPTION = "Primary TRAINER";
	
	public static final String COL_CREATE_DATE = "createDate";
	
	// Common placeholder for userName and fromName in email content
	public static final String PLACE_HOLDER_FROM_USER = "{fromname}";
	public static final String PLACE_HOLDER_TO_CUSTOMER = "{username}";
	public static final String HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER = "HKGTA";

	public static final String INTERNAL_REMARK_REFUND = "refund to cash value";
	
	public static final int REFUND_PERIOD_DEFAULT_HOURS = 96;
}
