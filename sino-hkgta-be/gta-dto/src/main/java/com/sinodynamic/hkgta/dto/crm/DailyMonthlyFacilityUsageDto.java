package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.math.NumberUtils;

public class DailyMonthlyFacilityUsageDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String resvDate;

	private Long resvId;

	private String resvIdString;

	private String facilityType;

	private String resvType;

	private String academyId;

	private String patronName;

	private String memberType;

	private String serviceDate;

	private String paymentMethodCode;

	private String paymentMedia;

	private String location;

	private Long noOfFacility;

	private BigDecimal paidAmount;

	private String status;

	private String createBy;

	private Long transferFromResvId;

	public String getResvDate() {
		return resvDate;
	}

	public void setResvDate(String resvDate) {
		this.resvDate = resvDate;
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Object resvId) {
		this.resvId = (resvId != null ? NumberUtils.toLong(resvId.toString()) : null);
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getResvType() {
		return resvType;
	}

	public void setResvType(String resvType) {
		this.resvType = resvType;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getPaymentMedia() {
		return paymentMedia;
	}

	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getNoOfFacility() {
		return noOfFacility;
	}

	public void setNoOfFacility(Object noOfFacility) {
		this.noOfFacility = (noOfFacility != null ? NumberUtils.toLong(noOfFacility.toString()) : null);
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getResvIdString() {
		return resvIdString;
	}

	public void setResvIdString(String resvIdString) {
		this.resvIdString = resvIdString;
	}

	public Long getTransferFromResvId() {
		return transferFromResvId;
	}

	public void setTransferFromResvId(Object transferFromResvId) {
		this.transferFromResvId = (transferFromResvId != null ? NumberUtils.toLong(transferFromResvId.toString()) : null);
	}

}
