package com.sinodynamic.hkgta.dto.fms;

import java.util.Date;

import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;

public class FacilityItemStatusDto implements IFacilityTimeslotLogDto {

	private Long facilityTimeslotId;
	private Long facilityNo;
	private String facilityName;
	private Date beginDatetime;
	private Date endDatetime;
	private String status;
	private String remark;
	private String salutation;
	private String given_name;
	private String surname;
	private Date begin_datetime_book;
	private Date end_datetime_book;
	private String updateBy;
	private String beginDate;
	private String ballFeedingStatus;
	private Long resvId;
	private Date createDate;
	private String reserveType;
	
	private FacilityTimeslot facilityTimeslot; 
	@Override
	public Long getFacilityNo() {
		return facilityNo;
	}
	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}
	public String getFacilityName() {
		return facilityName;
	}
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	@Override
	public Date getBeginDatetime() {
		return beginDatetime;
	}
	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}
	@Override
	public Date getEndDatetime() {
		return endDatetime;
	}
	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getGiven_name() {
		return given_name;
	}
	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Date getBegin_datetime_book() {
		return begin_datetime_book;
	}
	public void setBegin_datetime_book(Date begin_datetime_book) {
		this.begin_datetime_book = begin_datetime_book;
	}
	public Date getEnd_datetime_book() {
		return end_datetime_book;
	}
	public void setEnd_datetime_book(Date end_datetime_book) {
		this.end_datetime_book = end_datetime_book;
	}
	public FacilityTimeslot getFacilityTimeslot() {
		return facilityTimeslot;
	}
	public void setFacilityTimeslot(FacilityTimeslot facilityTimeslot) {
		this.facilityTimeslot = facilityTimeslot;
	}
	@Override
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}
	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}
	public String getBallFeedingStatus() {
		return ballFeedingStatus;
	}
	public void setBallFeedingStatus(String ballFeedingStatus) {
		this.ballFeedingStatus = ballFeedingStatus;
	}
	public Long getResvId() {
		return resvId;
	}
	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}
	public Date getCreateDate()
	{
		return createDate;
	}
	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}
	@Override
	public String getRemark()
	{
		return remark;
	}
	public void setRemark(String remark)
	{
		this.remark = remark;
	}
	public String getReserveType()
	{
		return reserveType;
	}
	public void setReserveType(String reserveType)
	{
		this.reserveType = reserveType;
	}

}
