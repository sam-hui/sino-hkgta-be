package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

public class SpaPicPathDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long  picId;
	private String picType;
	private String picFileName;
	
	public SpaPicPathDto()
	{
		
	}

	public Long getPicId() {
		return picId;
	}

	public void setPicId(Long picId) {
		this.picId = picId;
	}

	public String getPicType() {
		return picType;
	}

	public void setPicType(String picType) {
		this.picType = picType;
	}

	public String getPicFileName() {
		return picFileName;
	}

	public void setPicFileName(String picFileName) {
		this.picFileName = picFileName;
	}

	
}
