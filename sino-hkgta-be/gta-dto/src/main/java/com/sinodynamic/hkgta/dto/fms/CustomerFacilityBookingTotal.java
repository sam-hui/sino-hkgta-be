package com.sinodynamic.hkgta.dto.fms;

import java.math.BigDecimal;

public class CustomerFacilityBookingTotal
{
	private BigDecimal total;

	public BigDecimal getTotal()
	{
		return total;
	}

	public void setTotal(BigDecimal total)
	{
		this.total = total;
	}
}
