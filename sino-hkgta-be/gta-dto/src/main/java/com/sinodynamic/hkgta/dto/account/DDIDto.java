package com.sinodynamic.hkgta.dto.account;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDIDto implements Serializable {
	private String bankId;
	private String bankCode;
	private BigDecimal amount;
	private String status;
	private Long transactionNo;
	private Long customerId;

	private String accountNumber;
	private String accountName;
	private String originatorBankCode;

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	
	

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getOriginatorBankCode() {
		return originatorBankCode;
	}

	public void setOriginatorBankCode(String originatorBankCode) {
		this.originatorBankCode = originatorBankCode;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
