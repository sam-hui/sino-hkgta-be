package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

public class ActivateMemberDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long customerId;
	
	private String loginId;
	
	private String academyNo;
	
	private Date activationDate;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}
	
}
