package com.sinodynamic.hkgta.dto.memberapp;

import java.io.Serializable;
/**
 * 
 * @author Allen_Yu
 *
 */
public class CandidateCustomerDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Long getCandidateId() {
		return candidateId;
	}
	public void setCandidateId(Long candidateId) {
		this.candidateId = candidateId;
	}
	private Long candidateId;
	private String givenName;
	private String givenNameNls;
	private String surname;
	private String surnameNls;
	private String phoneMobile;
	private Long dayPassId;
	
	public CandidateCustomerDto(){
		
	}
	public CandidateCustomerDto(String givenName, String givenNameNls, String surname, String surnameNls,
			String phoneMobile) {
		super();
		this.givenName = givenName;
		this.givenNameNls = givenNameNls;
		this.surname = surname;
		this.surnameNls = surnameNls;
		this.phoneMobile = phoneMobile;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getGivenNameNls() {
		return givenNameNls;
	}
	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = givenNameNls;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSurnameNls() {
		return surnameNls;
	}
	public void setSurnameNls(String surnameNls) {
		this.surnameNls = surnameNls;
	}
	public String getPhoneMobile() {
		return phoneMobile;
	}
	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Long getDayPassId() {
		return dayPassId;
	}
	public void setDayPassId(Long dayPassId) {
		this.dayPassId = dayPassId;
	}
	
	
}
