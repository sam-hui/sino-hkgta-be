package com.sinodynamic.hkgta.dto.crm;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.PresentationBatch;

public class PresentationRequestWrapper {

	private PresentationBatch presentationBatch;
	
	private List<PresentMaterialDto> materialist;

	public PresentationBatch getPresentationBatch() {
		return presentationBatch;
	}

	public void setPresentationBatch(PresentationBatch presentationBatch) {
		this.presentationBatch = presentationBatch;
	}

	public List<PresentMaterialDto> getMaterialist() {
		return materialist;
	}

	public void setMaterialist(List<PresentMaterialDto> materialist) {
		this.materialist = materialist;
	}
}
