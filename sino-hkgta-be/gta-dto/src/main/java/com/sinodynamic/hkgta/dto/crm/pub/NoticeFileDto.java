package com.sinodynamic.hkgta.dto.crm.pub;

public class NoticeFileDto {

	private long fileId;
	
	private String fileName;

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
