package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class DailyEnrollmentDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String enrollDate;
	
	private String planName;
	
	private String academyId;
	
	private String patronName;
	
	private String nationallity;
	
	private String company;
	
	private String jobTitle;
	
	private String accountManager;
	
	private BigDecimal servicePlanFee;
	
	private Long qty;
	
	private String createBy;
	
	private String createDate;
	
	private String enrollStatus;
	
	private String businessNature;
	
	public String getBusinessNature() {
		return businessNature;
	}

	public void setBusinessNature(String businessNature) {
		this.businessNature = businessNature;
	}

	public String getBusinessNatureDB() {
		return businessNatureDB;
	}

	public void setBusinessNatureDB(String businessNatureDB) {
		this.businessNatureDB = businessNatureDB;
	}

	private String businessNatureDB;

	public String getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(String enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getNationallity() {
		return nationallity;
	}

	public void setNationallity(String nationallity) {
		this.nationallity = nationallity;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	public BigDecimal getServicePlanFee() {
		return servicePlanFee;
	}

	public void setServicePlanFee(BigDecimal servicePlanFee) {
		this.servicePlanFee = servicePlanFee;
	}

	public Long getQty() {
		return qty;
	}

	public void setQty(Object qty) {
		if (qty instanceof BigInteger) {
			this.qty = ((BigInteger) qty).longValue();
		} else if (qty instanceof Integer) {
			this.qty = ((Integer) qty).longValue();
		} else if (qty instanceof String) {
			this.qty = Long.valueOf((String) qty);
		} else {
			this.qty = (Long) qty;
		}
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}
	

	
}
