package com.sinodynamic.hkgta.dto.push;

import java.io.Serializable;

public class RegisterResultDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5615719461796162393L;
    private String status;
    private String errorCode;
    private String errorMessageEn;
    private String errorMessageTc;
    private String errorMessageSc;
    private String endpointArn;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessageEn() {
		return errorMessageEn;
	}
	public void setErrorMessageEn(String errorMessageEn) {
		this.errorMessageEn = errorMessageEn;
	}
	public String getErrorMessageTc() {
		return errorMessageTc;
	}
	public void setErrorMessageTc(String errorMessageTc) {
		this.errorMessageTc = errorMessageTc;
	}
	public String getErrorMessageSc() {
		return errorMessageSc;
	}
	public void setErrorMessageSc(String errorMessageSc) {
		this.errorMessageSc = errorMessageSc;
	}
	public String getEndpointArn() {
		return endpointArn;
	}
	public void setEndpointArn(String endpointArn) {
		this.endpointArn = endpointArn;
	}
    
	
	

}
