package com.sinodynamic.hkgta.dto.memberapp;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;

public class MemberAppCustomerAdditionInfoDto {
	private Long customerId;
	private List<CustomerAdditionInfoDto> customerAdditionInfoDtos;
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public List<CustomerAdditionInfoDto> getCustomerAdditionInfoDtos() {
		return customerAdditionInfoDtos;
	}
	public void setCustomerAdditionInfoDtos(
			List<CustomerAdditionInfoDto> customerAdditionInfoDtos) {
		this.customerAdditionInfoDtos = customerAdditionInfoDtos;
	}

}
