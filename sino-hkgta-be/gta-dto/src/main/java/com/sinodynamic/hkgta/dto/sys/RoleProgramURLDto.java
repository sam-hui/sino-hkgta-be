package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class RoleProgramURLDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8248970296393034828L;
	private String roleName;
	private String accessRight;
	private String uriPath;
	
	public String getRoleName()
	{
		return roleName;
	}
	
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}
	
	public String getAccessRight() {
		return accessRight;
	}
	
	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}

	public String getUriPath()
	{
		return uriPath;
	}

	public void setUriPath(String uriPath)
	{
		this.uriPath = uriPath;
	}
	

}
