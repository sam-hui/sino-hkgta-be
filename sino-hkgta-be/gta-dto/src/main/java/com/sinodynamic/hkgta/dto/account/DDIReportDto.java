package com.sinodynamic.hkgta.dto.account;

import java.util.ArrayList;
import java.util.List;

public class DDIReportDto {
	
	private String startDate;
	private String endDate;
	private List<DDIReportDetailDto> details;
	private int noOfRecord;
	
	public DDIReportDto(){
		super();
		details = new ArrayList<DDIReportDetailDto>();
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public List<DDIReportDetailDto> getDetails() {
		return details;
	}
	public void setDetails(List<DDIReportDetailDto> details) {
		this.details = details;
	}
	public int getNoOfRecord() {
		return noOfRecord;
	}
	public void setNoOfRecord(int noOfRecord) {
		this.noOfRecord = noOfRecord;
	}

	@Override
	public String toString() {
		return "DDIReport [startDate=" + startDate + ", endDate=" + endDate
				+ ", details=" + details + ", noOfRecord=" + noOfRecord + "]";
	}
	
	

}
