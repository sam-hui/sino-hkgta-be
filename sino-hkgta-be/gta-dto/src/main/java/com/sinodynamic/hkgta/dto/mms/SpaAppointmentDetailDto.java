package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

public class SpaAppointmentDetailDto implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	private String categoryCode;
	private String subCategoryCode;
	private String serviceCode;
	private String serviceName;
	private String serviceDescription;
	private Long   serviceTime;
	private BigDecimal   serviceCost;
	private String   startDatetime;
	private String   endDatetime;
	private String therapistType;  //0--female  1--male  -1 --any
	private String therapistName;
	private String therapistCode;
	
	
	
		
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	public String getTherapistCode() {
		return therapistCode;
	}
	public void setTherapistCode(String therapistCode) {
		this.therapistCode = therapistCode;
	}	
	
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getSubCategoryCode() {
		return subCategoryCode;
	}
	public void setSubCategoryCode(String subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public Long getServiceTime() {
		return serviceTime;
	}
	public void setServiceTime(Long serviceTime) {
		this.serviceTime = serviceTime;
	}
	
	public BigDecimal getServiceCost() {
		return serviceCost;
	}
	public void setServiceCost(BigDecimal serviceCost) {
		this.serviceCost = serviceCost;
	}
	
	
	public String getStartDatetime() {
		return startDatetime;
	}
	public void setStartDatetime(String startDatetime) {
		this.startDatetime = startDatetime;
	}
	public String getEndDatetime() {
		return endDatetime;
	}
	public void setEndDatetime(String endDatetime) {
		this.endDatetime = endDatetime;
	}
	public String getTherapistType() {
		return therapistType;
	}
	public void setTherapistType(String therapistType) {
		this.therapistType = therapistType;
	}
	public String getTherapistName() {
		return therapistName;
	}
	public void setTherapistName(String therapistName) {
		this.therapistName = therapistName;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
