package com.sinodynamic.hkgta.dto.push;

import java.io.Serializable;

public class PushDto implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4047208315574823487L;
	private String deviceArn;
    private String message;
	public String getDeviceArn() {
		return deviceArn;
	}
	public void setDeviceArn(String deviceArn) {
		this.deviceArn = deviceArn;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    
}
