package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

public class StaffMasterInfoDto implements Serializable{
	
	private static final long serialVersionUID = 3173671759881359185L;

	private String userId;
	
	private String contactEmail;
	
	private String staffNo;
	
	private String staffType;
	
	private String staffTypeDisplay;
	
	private String status;
	
	private String positionTitle;
	
	private String positionTitleName;
	
	private String employDate;
	
	private Date colEmployDate;
	
	private String quitDate;
	
	private Date colQuitDate;
	
	private String staffName;
	
	private BigInteger bookings;
	
	private BigDecimal highPrice;
	
	private BigDecimal lowPrice;
	
	private Date createDate;
	
	private Long departId;
	
	private String departName;
	
	@JsonIgnore
	public Date getCreateDate()
	{
		return createDate;
	}

	public Date getColEmployDate()
	{
		return colEmployDate;
	}


	public String getDepartName()
	{
		return departName;
	}

	public void setDepartName(String departName)
	{
		this.departName = departName;
	}

	public String getPositionTitleName()
	{
		return positionTitleName;
	}

	public void setPositionTitleName(String positionTitleName)
	{
		this.positionTitleName = positionTitleName;
	}

	public void setColEmployDate(Date colEmployDate)
	{
		this.colEmployDate = colEmployDate;
	}


	public Date getColQuitDate()
	{
		return colQuitDate;
	}


	public void setColQuitDate(Date colQuitDate)
	{
		this.colQuitDate = colQuitDate;
	}


	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}


	public StaffMasterInfoDto() {
		super();
	}

	public String getEmployDate() {
		return employDate;
	}

	public void setEmployDate(String employDate) {
		this.employDate = employDate;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getStaffNo() {
		return staffNo;
	}

	public void setStaffNo(String staffNo) {
		this.staffNo = staffNo;
	}

	public String getStaffType() {
		return staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public String getQuitDate() {
		return quitDate;
	}

	public void setQuitDate(String quitDate) {
		this.quitDate = quitDate;
	}


	public String getStaffName()
	{
		return staffName;
	}


	public void setStaffName(String staffName)
	{
		this.staffName = staffName;
	}


	public BigInteger getBookings()
	{
		return bookings;
	}


	public void setBookings(BigInteger bookings)
	{
		this.bookings = bookings;
	}


	public BigDecimal getHighPrice()
	{
		return highPrice;
	}


	public void setHighPrice(BigDecimal highPrice)
	{
		this.highPrice = highPrice;
	}


	public BigDecimal getLowPrice()
	{
		return lowPrice;
	}


	public void setLowPrice(BigDecimal lowPrice)
	{
		this.lowPrice = lowPrice;
	}

	public String getStaffTypeDisplay() {
		return staffTypeDisplay;
	}

	public void setStaffTypeDisplay(String staffTypeDisplay) {
		this.staffTypeDisplay = staffTypeDisplay;
	}
	
	

	

	public Long getDepartId() {
		return departId;
	}

	public void setDepartId(Object departId) {
		this.departId =  (departId!=null ? NumberUtils.toLong(departId.toString()):null);
	}

	@Override
	public String toString() {
		return "StaffMasterDto [userId=" + userId + ", contactEmail="
				+ contactEmail + ", staffNo=" + staffNo + ", staffType="
				+ staffType + ", status=" + status + ", positionTitle="
				+ positionTitle + ", employDate=" + employDate + ", quitDate="
				+ quitDate + "]";
	}


}
