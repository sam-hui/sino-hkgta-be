package com.sinodynamic.hkgta.dto.crm;

public class ScanCardMappingDto {

	private String cardType;
	private Long customerId;
	private String salutation;
	private String givenName;
	private String surname;
	private String memberType;
	private String memberName;
	private String academyNo;
	private String portraitPhoto;
	private String status;
	
	
	

	public ScanCardMappingDto() {
		super();
	}

	public ScanCardMappingDto(String cardType, Long customerId, String salutation, String givenName, String surname, String memberType,String academyNo, String portraitPhoto, String status) {
		super();
		this.cardType = cardType;
		this.customerId = customerId;
		this.salutation = salutation;
		this.givenName = givenName;
		this.surname = surname;
		this.memberType = memberType;
		this.academyNo = academyNo;
		this.portraitPhoto = portraitPhoto;
		this.status = status;
		
	}
	
	public void setMemberName()
	{
		this.memberName = this.salutation + " " + this.givenName + " " + this.surname ;
	}
	
	public String getMemberName()
	{
		return this.memberName;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getSalutation()
	{
		return salutation;
	}

	public void setSalutation(String salutation)
	{
		this.salutation = salutation;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getMemberType()
	{
		return memberType;
	}

	public void setMemberType(String memberType)
	{
		this.memberType = memberType;
	}

	public String getAcademyNo()
	{
		return academyNo;
	}

	public void setAcademyNo(String academyNo)
	{
		this.academyNo = academyNo;
	}

	public String getPortraitPhoto()
	{
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto)
	{
		this.portraitPhoto = portraitPhoto;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	
}
