package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.sql.Date;

public class CoachingDetailDto implements Serializable {
	private String id;// coaching id or course id
	
	private Date beginDate;
	
	private Date endDate;
	
	private String studentName;// for coaching
	
	private String facilityType;//for coaching
	
	private String faclilityName;// for coaching
	
	private String venue;// for course
	
	private boolean isCoaching;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getFaclilityName() {
		return faclilityName;
	}

	public void setFaclilityName(String faclilityName) {
		this.faclilityName = faclilityName;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public boolean isCoaching() {
		return isCoaching;
	}

	public void setCoaching(boolean isCoaching) {
		this.isCoaching = isCoaching;
	}
	
	
	
	

}
