package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CorporateMemberDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long corporateId;
	
	private Long customerId;
	
	private String academyNo;
	
	private String fullName;
	
	private String companyName;
	
	private String planName;
	
	private Date createDate;
	
	private String officer;
	
	private BigDecimal totalCreditLimit;

	private Long superiorMemberId;
	
	private String memberType;
	
	private String status;
	
	private String statusValue;
	
	private String memberName;
	
	private BigDecimal remainSum;
	
	private BigDecimal totalSum; 
	
	private Integer memberLimitRuleVersion;
	
	private Boolean isActivationEmailEnabled;
	
	private Date activationDate;
	
	private String creditLimit;
	
	private Boolean dependentCreationRight;
	
	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Object corporateId) {
		if(corporateId instanceof BigInteger){
			this.corporateId = ((BigInteger) corporateId).longValue();
		}else if(corporateId instanceof Integer){
			this.corporateId = ((Integer) corporateId).longValue();
		}else if(corporateId instanceof String){
			this.corporateId = Long.valueOf((String) corporateId);
		}else{
			this.corporateId = (Long) corporateId;
		}
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCreateDate() {
		return DtoHelper.getYMDDateAndDateDiff(createDate);
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getOfficer() {
		return officer;
	}

	public void setOfficer(String officer) {
		this.officer = officer;
	}

	public BigDecimal getTotalCreditLimit() {
		return totalCreditLimit;
	}

	public void setTotalCreditLimit(BigDecimal totalCreditLimit) {
		this.totalCreditLimit = totalCreditLimit;
	}

	public Long getSuperiorMemberId() {
		return superiorMemberId;
	}

	public void setSuperiorMemberId(Object superiorMemberId) {
		if(superiorMemberId instanceof BigInteger){
			this.superiorMemberId = ((BigInteger) superiorMemberId).longValue();
		}else if(superiorMemberId instanceof Integer){
			this.superiorMemberId = ((Integer) superiorMemberId).longValue();
		}else if(superiorMemberId instanceof String){
			this.superiorMemberId = Long.valueOf((String) superiorMemberId);
		}else{
			this.superiorMemberId = (Long) superiorMemberId;
		}
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public BigDecimal getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(BigDecimal totalSum) {
		this.totalSum = totalSum;
	}

	public BigDecimal getRemainSum() {
		return remainSum;
	}

	public void setRemainSum(BigDecimal remainSum) {
		this.remainSum = remainSum;
	}

	public Integer getMemberLimitRuleVersion() {
		return memberLimitRuleVersion;
	}

	public void setMemberLimitRuleVersion(Object memberLimitRuleVersion) {
		
		if(memberLimitRuleVersion instanceof BigInteger) {
			this.memberLimitRuleVersion = ((BigInteger) memberLimitRuleVersion).intValue();
		}
		else if(memberLimitRuleVersion instanceof Long) {
			this.memberLimitRuleVersion = ((Long)memberLimitRuleVersion).intValue();
		}
		else if(memberLimitRuleVersion instanceof String) {
			this.memberLimitRuleVersion = Integer.valueOf((String)memberLimitRuleVersion);
		}
		else {
			this.memberLimitRuleVersion = (Integer)memberLimitRuleVersion;
		}

	}

	public Boolean getIsActivationEmailEnabled() {
		return isActivationEmailEnabled;
	}

	public void setIsActivationEmailEnabled(Boolean isActivationEmailEnabled) {
		this.isActivationEmailEnabled = isActivationEmailEnabled;
	}

	public String getActivationDate() {
		if (activationDate == null) {
			return "";
		}
		return DtoHelper.date2String(activationDate, "yyyy-MM-dd");
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public Boolean getDependentCreationRight() {
		return dependentCreationRight;
	}

	public void setDependentCreationRight(Boolean dependentCreationRight) {
		this.dependentCreationRight = dependentCreationRight;
	}
	
	
}
