package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class GuessRoomReservationRefundDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private GuessRoomReservationDetailDto guessRoomReservationDetail;

	public GuessRoomReservationDetailDto getGuessRoomReservationDetail() {
		return guessRoomReservationDetail;
	}

	public void setGuessRoomReservationDetail(GuessRoomReservationDetailDto guessRoomReservationDetail) {
		this.guessRoomReservationDetail = guessRoomReservationDetail;
	}
	
	
}
