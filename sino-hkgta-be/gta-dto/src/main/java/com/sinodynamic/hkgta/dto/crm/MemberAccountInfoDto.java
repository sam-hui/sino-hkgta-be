package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.crm.EnrollmentHistoryDto;
import com.sinodynamic.hkgta.entity.crm.FacilityEntitlementDto;

public class MemberAccountInfoDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long customerId;
	private String academyNo;
	private String memberStatus;
	private String enrollStatus;
	private Long contractLength;
	private String serviceAccount;
	private Date effectiveDate;
	private Date expiryDate;
	private String planName;
	private BigDecimal orderTotalAmount;
	private String memberType;
	private BigDecimal limitValue;
	private Integer memberLimitRuleVersion;
	private BigDecimal dailyQuota;
	private String loginId;
	private List<FacilityEntitlementDto> facilityEntitlements = new ArrayList<FacilityEntitlementDto>();
	private List<EnrollmentHistoryDto> enrollmentHistoryDtos = new ArrayList<EnrollmentHistoryDto>();
	private List<MemberLimitRuleDto> servicePlanRightMasterDtos = new ArrayList<MemberLimitRuleDto>();
	private String companyName;
	private Long creditLimitId;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAcademyNo() {
		return DtoHelper.nvl(academyNo);
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getMemberStatus() {
		return DtoHelper.nvl(memberStatus);
	}

	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}

	public Long getContractLength() {
		return contractLength;
	}

	public void setContractLength(Long contractLength) {
		this.contractLength = contractLength;
	}

	public String getServiceAccount() {
		return DtoHelper.nvl(serviceAccount);
	}

	public void setServiceAccount(String serviceAccount) {
		this.serviceAccount = serviceAccount;
	}

	public String getEffectiveDate() {
		if (effectiveDate == null) {
			return "";
		}
		return DtoHelper.date2String(effectiveDate, "yyyy-MMM-dd");
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
		if (expiryDate == null) {
			return "";
		}
		return DtoHelper.date2String(expiryDate, "yyyy-MMM-dd");
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPlanName() {
		return DtoHelper.nvl(planName);
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}

	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	public String getMemberType() {
		return DtoHelper.nvl(memberType);
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public List<FacilityEntitlementDto> getFacilityEntitlements() {
		return facilityEntitlements;
	}

	public void setFacilityEntitlements(List<FacilityEntitlementDto> facilityEntitlements) {
		this.facilityEntitlements = facilityEntitlements;
	}

	public List<EnrollmentHistoryDto> getEnrollmentHistoryDtos() {
		return enrollmentHistoryDtos;
	}

	public void setEnrollmentHistoryDtos(List<EnrollmentHistoryDto> enrollmentHistoryDtos) {
		this.enrollmentHistoryDtos = enrollmentHistoryDtos;
	}

	public BigDecimal getLimitValue() {
		return limitValue;
	}

	public void setLimitValue(BigDecimal limitValue) {
		this.limitValue = limitValue;
	}

	public List<MemberLimitRuleDto> getServicePlanRightMasterDtos() {
		return servicePlanRightMasterDtos;
	}

	public void setServicePlanRightMasterDtos(
			List<MemberLimitRuleDto> servicePlanRightMasterDtos) {
		this.servicePlanRightMasterDtos = servicePlanRightMasterDtos;
	}

	public BigDecimal getDailyQuota() {
		return dailyQuota;
	}

	public void setDailyQuota(BigDecimal dailyQuota) {
		this.dailyQuota = dailyQuota;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Integer getMemberLimitRuleVersion() {
		return memberLimitRuleVersion;
	}

	public void setMemberLimitRuleVersion(Object memberLimitRuleVersion) {

		if (memberLimitRuleVersion instanceof BigInteger) {
			this.memberLimitRuleVersion = ((BigInteger) memberLimitRuleVersion)
					.intValue();
		} else if (memberLimitRuleVersion instanceof Long) {
			this.memberLimitRuleVersion = ((Long) memberLimitRuleVersion)
					.intValue();
		} else if (memberLimitRuleVersion instanceof String) {
			this.memberLimitRuleVersion = Integer
					.parseInt((String) memberLimitRuleVersion);
		} else {
			this.memberLimitRuleVersion = (Integer) memberLimitRuleVersion;
		}

	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCreditLimitId() {
		return creditLimitId;
	}

	public void setCreditLimitId(Long creditLimitId) {
		this.creditLimitId = creditLimitId;
	}
	
}
