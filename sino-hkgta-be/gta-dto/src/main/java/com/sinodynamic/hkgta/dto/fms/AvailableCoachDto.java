package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

/**
 * @author kevin_Liang
 *
 */
public class AvailableCoachDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3645134136634440399L;

	private String coachId;
	private String portraitPhoto;
	private String nickname;
	private String price;
	private String personalInfo;
	private String qualification;
	private String speciality;
	public String getCoachId() {
		return coachId;
	}
	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}
	public String getPortraitPhoto() {
		return portraitPhoto;
	}
	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getPersonalInfo() {
		return personalInfo;
	}
	public void setPersonalInfo(String personalInfo) {
		this.personalInfo = personalInfo;
	}
	
	public String getQualification()
	{
		return qualification;
	}
	public void setQualification(String qualification)
	{
		this.qualification = qualification;
	}
	public String getSpeciality()
	{
		return speciality;
	}
	public void setSpeciality(String speciality)
	{
		this.speciality = speciality;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coachId == null) ? 0 : coachId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvailableCoachDto other = (AvailableCoachDto) obj;
		if (coachId == null) {
			if (other.coachId != null)
				return false;
		} else if (!coachId.equals(other.coachId))
			return false;
		
		return true;
	}
	
}
