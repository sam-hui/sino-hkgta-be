package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ChargePostingDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8168077561605598337L;
	private String ticketID;
	private BigDecimal amount;
	private String pmsRevenueCode;
	private String resvID;
	private String description;

	public String getTicketID() {
		return ticketID;
	}

	public void setTicketID(String ticketID) {
		this.ticketID = ticketID;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPmsRevenueCode() {
		return pmsRevenueCode;
	}

	public void setPmsRevenueCode(String pmsRevenueCode) {
		this.pmsRevenueCode = pmsRevenueCode;
	}

	public String getResvID() {
		return resvID;
	}

	public void setResvID(String resvID) {
		this.resvID = resvID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
