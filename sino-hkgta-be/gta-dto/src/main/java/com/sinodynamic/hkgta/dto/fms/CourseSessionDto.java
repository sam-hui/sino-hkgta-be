package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class CourseSessionDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long sysId;
    private String courseId;
    private String sessionNo;
    private String beginDatetime;
    private String endDatetime;
    private String gatherLocation;

    private String courseName;
    private String coachUserId;
    private String sessionTime;
    private String status;
    private String otherTrainLocation;
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCourseName() {
	return courseName;
    }

    public void setCourseName(String courseName) {
	this.courseName = courseName;
    }

    public Long getSysId() {
	return sysId;
    }

    public void setSysId(Long sysId) {
	this.sysId = sysId;
    }

    public String getBeginDatetime() {
	return beginDatetime;
    }

    public void setBeginDatetime(String beginDatetime) {
	this.beginDatetime = beginDatetime;
    }

    public String getEndDatetime() {
	return endDatetime;
    }

    public void setEndDatetime(String endDatetime) {
	this.endDatetime = endDatetime;
    }

    public String getCourseId() {
	return courseId;
    }

    public void setCourseId(String courseId) {
	this.courseId = courseId;
    }

    public String getSessionNo() {
	return sessionNo;
    }

    public void setSessionNo(String sessionNo) {
	this.sessionNo = sessionNo;
    }

    public String getGatherLocation() {
	return gatherLocation;
    }

    public void setGatherLocation(String gatherLocation) {
	this.gatherLocation = gatherLocation;
    }

    public String getCoachUserId() {
        return coachUserId;
    }

    public void setCoachUserId(String coachUserId) {
        this.coachUserId = coachUserId;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getOtherTrainLocation() {
        return otherTrainLocation;
    }

    public void setOtherTrainLocation(String otherTrainLocation) {
        this.otherTrainLocation = otherTrainLocation;
    }
    
}
