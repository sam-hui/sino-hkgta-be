/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.util.List;

/**
 * @author Tony_Dong
 *
 */
public class CoachReservationLstDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6676657350218694814L;
	
	private List<CoachReservationDto> coachReservLst;

	/**
	 * @return the coachReserv
	 */
	public List<CoachReservationDto> getCoachReservLst() {
		return coachReservLst;
	}

	/**
	 * @param coachReserv the coachReserv to set
	 */
	public void setCoachReserv(List<CoachReservationDto> coachReservLst) {
		this.coachReservLst = coachReservLst;
	}
	
	
	

}
