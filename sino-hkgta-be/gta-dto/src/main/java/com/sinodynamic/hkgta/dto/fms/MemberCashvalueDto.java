package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.math.NumberUtils;

public class MemberCashvalueDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long customerId;
    private String customerName;
    private String academyId;
    private BigDecimal cashValue;
    private Long transactionId;
    private BigDecimal creditLimit;
    private BigDecimal spendingLimit;

    public Long getCustomerId() {
	return customerId;
    }

    public void setCustomerId(Object customerId) {
	this.customerId =  (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
    }

    public String getCustomerName() {
	return customerName;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    public String getAcademyId() {
	return academyId;
    }

    public void setAcademyId(String academyId) {
	this.academyId = academyId;
    }

    public BigDecimal getCashValue() {
	return cashValue;
    }

    public void setCashValue(BigDecimal cashValue) {
	this.cashValue = cashValue;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getSpendingLimit() {
        return spendingLimit;
    }

    public void setSpendingLimit(BigDecimal spendingLimit) {
        this.spendingLimit = spendingLimit;
    }
    
}
