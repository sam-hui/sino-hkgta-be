package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import com.sinodynamic.hkgta.dto.DtoHelper;



public class CardsListResponseDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String academyNo;
	private BigInteger customerId;
	private String memberName;
	private String memberType;
	private String memberTypeName;
	private BigInteger superiorMemberId;
	private Integer cardNo;
	private String cardTypeName;
	private String cardPurpose;
	private String remark;
	private String cardNoCode;
	
	
	public String getMemberTypeName() {
		return memberTypeName;
	}
	public void setMemberTypeName(String memberTypeName) {
		this.memberTypeName = memberTypeName;
	}
	public String getCardNoCode() {
		return cardNoCode;
	}
	public void setCardNoCode(String cardNoCode) {
		this.cardNoCode = cardNoCode;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public BigInteger getSuperiorMemberId() {
		return superiorMemberId;
	}
	public void setSuperiorMemberId(BigInteger superiorMemberId) {
		this.superiorMemberId = superiorMemberId;
	}
	public Integer getCardNo() {
		return cardNo;
	}
	public void setCardNo(Integer cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardIssueDate() {
		return DtoHelper.getYMDDateAndDateDiff(cardIssueDate);
	}
	public void setCardIssueDate(Timestamp cardIssueDate) {
		this.cardIssueDate = cardIssueDate;
	}
	private String status;
	private Timestamp cardIssueDate;
	private Timestamp statusUpdateDate;
	private String updateBy;
	
	public String getAcademyNo() {
		return academyNo;
	}
	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getCardTypeName() {
		return cardTypeName;
	}
	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}
	public String getCardPurpose() {
		return cardPurpose;
	}
	public void setCardPurpose(String cardPurpose) {
		this.cardPurpose = cardPurpose;
	}
	public BigInteger getCustomerId() {
		return customerId;
	}
	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusUpdateDate() {
		return DtoHelper.getYMDDateAndDateDiff(statusUpdateDate);
	}
	public void setStatusUpdateDate(Timestamp statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	
    
}
