/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.util.Date;

import antlr.StringUtils;

/**
 * @author Tony_Dong
 *
 */
public class CoachReservationDto implements Serializable, Comparable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4148238137887813820L;

	private Date beginTime;

	private Date endTime;

	private String category;

	private String customerName;
	
	private String customerId;

	private String academyNo;
	
	private String facilityName;

	private String facilityType;
	
	private String bayType;
	
	private String sessionId;
	
	private String portraitPhoto;
	
	private String courseName;

	private Long capacity;

	private Long attendance;

	private Long leftHours;
	
	private Long id;
	
	private String status;
	
	private Double timeGap;
	
	private Integer getStatusOrder(String status)
	{

		if (status.equals("RSV"))
		{
			return 1;
		}
		
		if (status.equals("ATD"))
		{
			return 2;
		}
		
		if (status.equals("COMPLETE"))
		{
			return 3;
		}
		
		if (status.equals("CAN"))
		{
			return 4;
		}
		return 0;
	}
	
	private Integer getCategoryOrder(String category)
	{

		if (category.equals("Coaching"))
		{
			return 1;
		}
		
		if (category.equals("Course"))
		{
			return 2;
		}
		
		return 0;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
		if (this.endTime.before(new Date()))
		{
			this.status = "COMPLETE";
		}
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Long capacity) {
		this.capacity = capacity;
	}

	public Long getAttendance() {
		return attendance;
	}

	public void setAttendance(Long attendance) {
		this.attendance = attendance;
	}

	public Long getLeftHours() {
		return leftHours;
	}

	public void setLeftHours(Long leftHours) {
		this.leftHours = leftHours;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBayType()
	{
		return bayType;
	}

	public void setBayType(String bayType)
	{
		this.bayType = bayType;
	}

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(String sessionId)
	{
		this.sessionId = sessionId;
	}

	public String getPortraitPhoto()
	{
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto)
	{
		this.portraitPhoto = portraitPhoto;
	}

	public String getAcademyNo()
	{
		return academyNo;
	}

	public void setAcademyNo(String academyNo)
	{
		this.academyNo = academyNo;
	}

	public String getCourseName()
	{
		return courseName;
	}

	public void setCourseName(String courseName)
	{
		this.courseName = courseName;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		if(null == this.status || this.status.isEmpty())
		{
			this.status = status;
		}
	}

	public Double getTimeGap()
	{
		return timeGap;
	}

	public void setTimeGap(Double timeGap)
	{
		this.timeGap = timeGap;
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}

	@Override
	public int compareTo(Object o)
	{
		CoachReservationDto other = (CoachReservationDto)o;
		
		if(o == null)
			return 1;
		
		if(this.status!=null && other.status!=null)
		{
			int compareStatus = getStatusOrder(this.status).compareTo(getStatusOrder(other.status));
			if(compareStatus == 0)
			{
				int compareTimegap = other.timeGap.compareTo(this.timeGap);
				if (compareTimegap == 0)
				{
					int compareCategory = getCategoryOrder(this.category).compareTo(getCategoryOrder(other.category));
					return compareCategory;
				}
				else
				{
					return compareTimegap;
				}
			}
			else
			{
				return compareStatus;
			}
		}
		
		return 0;
		
	}
}
