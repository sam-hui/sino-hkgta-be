package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.crm.StaffCoachInfo;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.UserMaster;


public class RequestStaffDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7000030754674311099L;

	private UserMaster user;
	
	private StaffMaster staffMaster;
	
	private StaffProfile staffProfile;
	
	private StaffCoachInfo staffCoachInfo;

	public RequestStaffDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserMaster getUser() {
		return user;
	}

	public void setUser(UserMaster user) {
		this.user = user;
	}

	public StaffCoachInfo getStaffCoachInfo()
	{
		return staffCoachInfo;
	}

	public void setStaffCoachInfo(StaffCoachInfo staffCoachInfo)
	{
		this.staffCoachInfo = staffCoachInfo;
	}

	public StaffMaster getStaffMaster() {
		return staffMaster;
	}

	public void setStaffMaster(StaffMaster staffMaster) {
		this.staffMaster = staffMaster;
	}

	public StaffProfile getStaffProfile() {
		return staffProfile;
	}

	public void setStaffProfile(StaffProfile staffProfile) {
		this.staffProfile = staffProfile;
	}

	@Override
	public String toString() {
		return "RequestStaffDto [user=" + user + ", staffMaster=" + staffMaster
				+ ", staffProfile=" + staffProfile + ", getUser()=" + getUser()
				+ ", getStaffMaster()=" + getStaffMaster()
				+ ", getStaffProfile()=" + getStaffProfile() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
}
