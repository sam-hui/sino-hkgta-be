package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

public class SpaAppointmentSynDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String extAppointmentId;
    private String extInvoiceNo;
    private String guestCode;
    private String extServiceCode;
    private String serviceName;
    private String serviceDescription;
    private String servicePrice;
    private String therapistNo;
    private String startTime;
    private String endTime;
    private String serviceTime;
    private String status;
    private String serviceInternalCost;
    
    private String recoveryTime;
    private String actualTime;
    private String extNote;
    private String checkInTime;
    private String rebooked;
    private String creationDate;
    private String createdBy;
    private String employeeFName;
    private String employeeLName;

    public String getServiceInternalCost() {
	return serviceInternalCost;
    }

    public void setServiceInternalCost(String serviceInternalCost) {
	this.serviceInternalCost = serviceInternalCost;
    }

    public String getExtAppointmentId() {
	return extAppointmentId;
    }

    public void setExtAppointmentId(String extAppointmentId) {
	this.extAppointmentId = extAppointmentId;
    }

    public String getExtInvoiceNo() {
	return extInvoiceNo;
    }

    public void setExtInvoiceNo(String extInvoiceNo) {
	this.extInvoiceNo = extInvoiceNo;
    }

    public String getGuestCode() {
	return guestCode;
    }

    public void setGuestCode(String guestCode) {
	this.guestCode = guestCode;
    }

    public String getExtServiceCode() {
	return extServiceCode;
    }

    public void setExtServiceCode(String extServiceCode) {
	this.extServiceCode = extServiceCode;
    }

    public String getServiceDescription() {
	return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
	this.serviceDescription = serviceDescription;
    }

    public String getTherapistNo() {
	return therapistNo;
    }

    public void setTherapistNo(String therapistNo) {
	this.therapistNo = therapistNo;
    }

    public String getStartTime() {
	return startTime;
    }

    public void setStartTime(String startTime) {
	this.startTime = startTime;
    }

    public String getEndTime() {
	return endTime;
    }

    public void setEndTime(String endTime) {
	this.endTime = endTime;
    }

    public String getServiceTime() {
	return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
	this.serviceTime = serviceTime;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getServiceName() {
	return serviceName;
    }

    public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
    }

    public String getServicePrice() {
	return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
	this.servicePrice = servicePrice;
    }

    public String getRecoveryTime() {
	return recoveryTime;
    }

    public void setRecoveryTime(String recoveryTime) {
	this.recoveryTime = recoveryTime;
    }

    public String getActualTime() {
	return actualTime;
    }

    public void setActualTime(String actualTime) {
	this.actualTime = actualTime;
    }

    public String getExtNote() {
	return extNote;
    }

    public void setExtNote(String extNote) {
	this.extNote = extNote;
    }

    public String getCheckInTime() {
	return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
	this.checkInTime = checkInTime;
    }

    public String getRebooked() {
	return rebooked;
    }

    public void setRebooked(String rebooked) {
	this.rebooked = rebooked;
    }

    public String getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(String creationDate) {
	this.creationDate = creationDate;
    }

    public String getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
    }

    public String getEmployeeFName() {
        return employeeFName;
    }

    public void setEmployeeFName(String employeeFName) {
        this.employeeFName = employeeFName;
    }

    public String getEmployeeLName() {
        return employeeLName;
    }

    public void setEmployeeLName(String employeeLName) {
        this.employeeLName = employeeLName;
    }

}
