package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class MonthlyEnrollmentDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String transactionDate;
	
	private Long transactionId;
	
	private Long orderNo;
	
	private String orderDate;
	
	private String academyId;
	
	private String patronName;
	
	private BigDecimal orderTotalAmount;
	
	private String salesMan;
	
	private String enrollType;
	
	private String paymentMethodCode;
	
	private String paymentMedia;
	
	private String location;
	
	private String status;
	
	private String createBy;
	
	private String updateBy;
	
	private String updateDate;
	
	private String auditBy;
	
	private String auditDate;
	
	private Long qty;
	
	private BigDecimal transAmount;
	
	private String planName;

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate =  transactionDate;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Object transactionId) {
		if (transactionId instanceof BigInteger) {
			this.transactionId = ((BigInteger) transactionId).longValue();
		} else if (transactionId instanceof Integer) {
			this.transactionId = ((Integer) transactionId).longValue();
		} else if (transactionId instanceof String) {
			this.transactionId = Long.valueOf((String) transactionId);
		} else {
			this.transactionId = (Long) transactionId;
		}
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Object orderNo) {
		if (orderNo instanceof BigInteger) {
			this.orderNo = ((BigInteger) orderNo).longValue();
		} else if (orderNo instanceof Integer) {
			this.orderNo = ((Integer) orderNo).longValue();
		} else if (orderNo instanceof String) {
			this.orderNo = Long.valueOf((String) orderNo);
		} else {
			this.orderNo = (Long) orderNo;
		}
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}

	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	public String getSalesMan() {
		return salesMan;
	}

	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	public String getEnrollType() {
		return enrollType;
	}

	public void setEnrollType(String enrollType) {
		this.enrollType = enrollType;
	}

	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getPaymentMedia() {
		return paymentMedia;
	}

	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getAuditBy() {
		return auditBy;
	}

	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}

	public String getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}

	public Long getQty() {
		return qty;
	}

	public void setQty(Object qty) {
		if (qty instanceof BigInteger) {
			this.qty = ((BigInteger) qty).longValue();
		} else if (qty instanceof Integer) {
			this.qty = ((Integer) qty).longValue();
		} else if (qty instanceof String) {
			this.qty = Long.valueOf((String) qty);
		} else {
			this.qty = (Long) qty;
		}
	}

	public BigDecimal getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}
	
	
}
