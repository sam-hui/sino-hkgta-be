package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;

public class CustomerCheckExsitDto implements Serializable{

	private static final long serialVersionUID = -8085423820019982178L;

	private BigInteger customerID;
	
	private String academyID;
	
	private String memberType;
	
	private String status;
	
	private String surname;
	
	private String givenName;
	
	private String salutation;
	
	private String isDeleted;
	
	private String enrollStatus;

	private String preEnrollStatus;
	
	private String followPerson;
		
	public BigInteger getCustomerID() {
		return customerID;
	}

	public void setCustomerID(BigInteger customerID) {
		this.customerID = customerID;
	}

	public String getAcademyID() {
		return academyID;
	}

	public void setAcademyID(String academyID) {
		this.academyID = academyID;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "CustomerCheckExsitDto [customerID=" + customerID
				+ ", academyID=" + academyID + ", memberType=" + memberType
				+ ", status=" + status + ", surname=" + surname
				+ ", givenName=" + givenName + ", salutation=" + salutation
				+ "]";
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public String getPreEnrollStatus() {
		return preEnrollStatus;
	}

	public void setPreEnrollStatus(String preEnrollStatus) {
		this.preEnrollStatus = preEnrollStatus;
	}

	public String getFollowPerson() {
		return followPerson;
	}

	public void setFollowPerson(String followPerson) {
		this.followPerson = followPerson;
	}
	
	
}
