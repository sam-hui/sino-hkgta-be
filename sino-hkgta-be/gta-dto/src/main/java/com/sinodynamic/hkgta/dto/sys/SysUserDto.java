package com.sinodynamic.hkgta.dto.sys;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.staff.UserMasterDto;

public class SysUserDto extends UserMasterDto {

	private static final long serialVersionUID = 1L;

	private List<Long> roleIdList;

	private List<Map<String, Object>> roleList;
	
	public List<Long> getRoleIdList() {
		return roleIdList;
	}
	
	
	public void setRoleIdList(List<Long> roleIdList) {
		this.roleIdList = roleIdList;
	}


	public List<Map<String, Object>> getRoleList() {
		return roleList;
	}


	public void setRoleList(List<Map<String, Object>> roleList) {
		this.roleList = roleList;
	}
	
	
	
}
