package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class DailyLeadsDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private String patronName;
	private String accountManager;
	private String classification;
	private String nationality;
	private String businessNature;
	private String company;
	private String jobTitle;
	private Long qty;
	private String createBy;
	private String createDate;
	private String enrollStatus;
	public String getPatronName() {
		return patronName;
	}
	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}
	public String getAccountManager() {
		return accountManager;
	}
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getBusinessNature() {
		return businessNature;
	}
	public void setBusinessNature(String businessNature) {
		this.businessNature = businessNature;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Object qty) {
		if (qty instanceof BigInteger) {
			this.qty = ((BigInteger) qty).longValue();
		} else if (qty instanceof Integer) {
			this.qty = ((Integer) qty).longValue();
		} else if (qty instanceof String) {
			this.qty = Long.valueOf((String) qty);
		} else {
			this.qty = (Long) qty;
		}
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getEnrollStatus() {
		return enrollStatus;
	}
	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}
	
}
