package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class CancelReservationDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7955375086372267175L;
	private String resId;
	private String membershipID;
	private String memberName;
	private String reservationTimeSpanStart;
	private String reservationTimeSpanEnd;

	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}

	public String getMembershipID() {
		return membershipID;
	}

	public void setMembershipID(String membershipID) {
		this.membershipID = membershipID;
	}

	public String getReservationTimeSpanStart() {
		return reservationTimeSpanStart;
	}

	public void setReservationTimeSpanStart(String reservationTimeSpanStart) {
		this.reservationTimeSpanStart = reservationTimeSpanStart;
	}

	public String getReservationTimeSpanEnd() {
		return reservationTimeSpanEnd;
	}

	public void setReservationTimeSpanEnd(String reservationTimeSpanEnd) {
		this.reservationTimeSpanEnd = reservationTimeSpanEnd;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
