package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.util.Date;

public class PurchasedDaypassDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1769773534225031420L;
			private String memberBuy;
			
			private Integer pageSize;
	        
	        private String show;
	        
	        public java.math.BigInteger getCustomerId() {
				return customerId;
			}

			public void setCustomerId(java.math.BigInteger customerId) {
				this.customerId = customerId;
			}



			private  String order;
	    	
	    	private  String propertyName;
	    	
	    	private   java.math.BigInteger customerId;   
	    	
	    	private String contactEmail;
	    	
	    	public String getContactEmail() {
				return contactEmail;
			}

			public void setContactEmail(String contactEmail) {
				this.contactEmail = contactEmail;
			}

			

			public String getOrder() {
				return order;
			}

			public void setOrder(String order) {
				this.order = order;
			}

			public String getPropertyName() {
				return propertyName;
			}

			public void setPropertyName(String propertyName) {
				this.propertyName = propertyName;
			}
                        
	        public String getShow() {
				return show;
			}

			public void setShow(String show) {
				this.show = show;
			}

			

		

			public Integer getPageSize() {
				return pageSize;
			}

			public void setPageSize(Integer pageSize) {
				this.pageSize = pageSize;
			}



			private int pageNumber;
	
	        public int getPageNumber() {
				return pageNumber;
			}

			public void setPageNumber(int pageNumber) {
				this.pageNumber = pageNumber;
			}

			private Long  planNo;

			private java.math.BigInteger orderNo;
	        
//	        private BigDecimal orderTotalAmount;

			private java.math.BigInteger orderTotalAmount;
		
			public java.math.BigInteger getOrderNo() {
				return orderNo;
			}

			public void setOrderNo(java.math.BigInteger orderNo) {
				this.orderNo = orderNo;
			}

			public java.math.BigInteger getOrderTotalAmount() {
				return orderTotalAmount;
			}

			public void setOrderTotalAmount(java.math.BigInteger orderTotalAmount) {
				this.orderTotalAmount = orderTotalAmount;
			}
	        
	        private Date orderDate;
	        
	        private Date effectiveEndDate;
	        
	        private Date effectiveStartDate;
				        								
			public Date getOrderDate() {
				return orderDate;
			}

			public void setOrderDate(Date orderDate) {
				this.orderDate = orderDate;
			}

			public Date getEffectiveEndDate() {
				return effectiveEndDate;
			}

			public void setEffectiveEndDate(Date effectiveEndDate) {
				this.effectiveEndDate = effectiveEndDate;
			}

			public Date getEffectiveStartDate() {
				return effectiveStartDate;
			}

			public void setEffectiveStartDate(Date effectiveStartDate) {
				this.effectiveStartDate = effectiveStartDate;
			}

			private String memberName;
	        
	        public String getMemberName() {
				return memberName;
			}

			public void setMemberName(String memberName) {
				this.memberName = memberName;
			}

			private String orderStatus;
	

			public String getOrderStatus() {
				return orderStatus;
			}

			public void setOrderStatus(String orderStatus) {
				this.orderStatus = orderStatus;
			}

			public Long getPlanNo() {
				return planNo;
			}

			public void setPlanNo(Long planNo) {
				this.planNo = planNo;
			}

			public String getMemberBuy() {
				return memberBuy;
			}

			public void setMemberBuy(String memberBuy) {
				this.memberBuy = memberBuy;
			}
			
			
	        
	        
	

}
