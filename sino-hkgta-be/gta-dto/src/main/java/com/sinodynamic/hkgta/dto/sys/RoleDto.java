package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class RoleDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 557455574721204327L;
	private Long roleId;
	private String status;
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Object roleId) {
		this.roleId = (roleId!=null ? NumberUtils.toLong(roleId.toString()):null);;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
