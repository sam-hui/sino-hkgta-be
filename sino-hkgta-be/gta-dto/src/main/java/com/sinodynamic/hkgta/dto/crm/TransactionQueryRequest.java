package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class TransactionQueryRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -210719186784397013L;

	private String sortBy = "PresentName";
	private String createBy;
	private String sortType = "DESC";
	private String presentName;
	private Integer pageSize = 10;
	private Integer pageNumber = 1;

	
	/*
	 * OrderType has 2 option, 1 means Asc, 0 means Desc
	 */
	
	public boolean isDesc()
	{
		return (this.sortType.equals("DESC"));
	}
	
	
	public void setPageSize(Integer pageSize) {
		this.pageSize = 10;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getPresentName() {
		return presentName;
	}

	public void setPresentName(String presentName) {
		this.presentName = presentName;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

}
