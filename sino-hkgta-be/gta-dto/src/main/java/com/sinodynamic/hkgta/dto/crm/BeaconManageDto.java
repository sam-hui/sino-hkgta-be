package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class BeaconManageDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String beaconName;
	
	private String locationType;
	
	private Long ibeaconId;
	
	private Long ibeaconIdDB;
	
	private String status;
	
	private Long customerId;

	private Long minor;
	
	private Long major;
	
	private String guid;
	
	private Long poiId;
	
	private String venueCode;
	
	private String venueName;
	
	private String targetType;
	
	private Long presentRoomId;
	
	private Long pastRoomId;
	
	private String roomNo;
	
	private Date createDate;
	
	private String location;

	public String getBeaconName() {
		return beaconName;
	}

	public void setBeaconName(String beaconName) {
		this.beaconName = beaconName;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public Long getIbeaconId() {
		return ibeaconId;
	}

	public void setIbeaconId(Object ibeaconId) {
		if(ibeaconId instanceof BigInteger){
			this.ibeaconId = ((BigInteger) ibeaconId).longValue();
		}else if(ibeaconId instanceof Integer){
			this.ibeaconId = ((Integer) ibeaconId).longValue();
		}else if(ibeaconId instanceof String){
			this.ibeaconId = Long.valueOf((String) ibeaconId);
		}else{
			this.ibeaconId = (Long) ibeaconId;
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public Long getMinor() {
		return minor;
	}

	public void setMinor(Object minor) {
		if(minor instanceof BigInteger){
			this.minor = ((BigInteger) minor).longValue();
		}else if(minor instanceof Integer){
			this.minor = ((Integer) minor).longValue();
		}else if(minor instanceof String){
			this.minor = Long.valueOf((String) minor);
		}else{
			this.minor = (Long) minor;
		}
	}

	public Long getMajor() {
		return major;
	}

	public void setMajor(Object major) {
		if(major instanceof BigInteger){
			this.major = ((BigInteger) major).longValue();
		}else if(major instanceof Integer){
			this.major = ((Integer) major).longValue();
		}else if(major instanceof String){
			this.major = Long.valueOf((String) major);
		}else{
			this.major = (Long) major;
		}
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getIbeaconIdDB() {
		return ibeaconIdDB;
	}

	public void setIbeaconIdDB(Object ibeaconIdDB) {
		if(ibeaconIdDB instanceof BigInteger){
			this.ibeaconIdDB = ((BigInteger) ibeaconIdDB).longValue();
		}else if(ibeaconIdDB instanceof Integer){
			this.ibeaconIdDB = ((Integer) ibeaconIdDB).longValue();
		}else if(ibeaconIdDB instanceof String){
			this.ibeaconIdDB = Long.valueOf((String) ibeaconIdDB);
		}else{
			this.ibeaconIdDB = (Long) ibeaconIdDB;
		}
	}

	public Long getPoiId() {
		return poiId;
	}

	public void setPoiId(Object poiId) {
		if(poiId instanceof BigInteger){
			this.poiId = ((BigInteger) poiId).longValue();
		}else if(poiId instanceof Integer){
			this.poiId = ((Integer) poiId).longValue();
		}else if(poiId instanceof String){
			this.poiId = Long.valueOf((String) poiId);
		}else{
			this.poiId = (Long) poiId;
		}
	}

	public String getVenueCode() {
		return venueCode;
	}

	public void setVenueCode(String venueCode) {
		this.venueCode = venueCode;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public Long getPresentRoomId() {
		return presentRoomId;
	}

	public void setPresentRoomId(Object presentRoomId) {
		if(presentRoomId instanceof BigInteger){
			this.presentRoomId = ((BigInteger) presentRoomId).longValue();
		}else if(presentRoomId instanceof Integer){
			this.presentRoomId = ((Integer) presentRoomId).longValue();
		}else if(presentRoomId instanceof String){
			this.presentRoomId = Long.valueOf((String) presentRoomId);
		}else{
			this.presentRoomId = (Long) presentRoomId;
		}
	}

	public Long getPastRoomId() {
		return pastRoomId;
	}

	public void setPastRoomId(Object pastRoomId) {
		if(pastRoomId instanceof BigInteger){
			this.pastRoomId = ((BigInteger) pastRoomId).longValue();
		}else if(pastRoomId instanceof Integer){
			this.pastRoomId = ((Integer) pastRoomId).longValue();
		}else if(pastRoomId instanceof String){
			this.pastRoomId = Long.valueOf((String) pastRoomId);
		}else{
			this.pastRoomId = (Long) pastRoomId;
		}
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
}
