package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CustomerEnrollmentDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer pageSize;
	private Integer currentPage;
	private Long enrollId;

	private Long customerId;
	private Long orderNo;
	private String contactEmail;
	
	private String createBy;

	private String enrollType;

	private String internalRemark;

	private String salesFollowBy;

	private String status;
	private String statusOrder;
	
	private String subscribeContractLength;

	private String subscribePlanNo;

	private String updateBy;

	private Date updateDate;
	//add for enrollment settlement
	private String enrollDate;
	//acception DB value
	private Date enrollDateDB;
	
	private String enrollDateOrder;
	
	private String academyNo;
	private BigInteger failTransNo;
	private BigDecimal balanceDue;
	private Integer noOfDays;
	private String orderStatus;
	private String offerCode;
	private String orderBy;
	private String isAscending;
	
	private String effectiveDate;
	
	private Date enrollCreateDate;
	
	private Boolean isActivationEmailEnabled;
	
	private String companyName;
	
	private Long enrollId2;
	
	private Long customerId2;
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getIsAscending() {
		return isAscending;
	}

	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Object noOfDays) {
		if(noOfDays instanceof BigInteger){
			this.noOfDays = ((BigInteger)noOfDays).intValue();
		}
		else if(noOfDays instanceof Long){
			this.noOfDays = ((Long)noOfDays).intValue();
		}
		else if(noOfDays instanceof String){
			this.noOfDays = Integer.valueOf((String)noOfDays);
		}
		else{
			this.noOfDays = (Integer)noOfDays;
		}
		
	}

	public BigDecimal getBalanceDue() {
		return balanceDue;
	}

	public void setBalanceDue(BigDecimal balanceDue) {
		this.balanceDue = balanceDue;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public BigInteger getFailTransNo() {
		return failTransNo;
	}

	public void setFailTransNo(BigInteger failTransNo) {
		this.failTransNo = failTransNo;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Object orderNo) {
		this.orderNo =(orderNo!=null ? NumberUtils.toLong(orderNo.toString()):null);
	}
	@JsonIgnore
	public void setOrderNo2(BigInteger orderNo) {
		if(null !=orderNo) {
			this.orderNo = orderNo.longValue();
		}
	}
	/**private String creationDate;

	@JsonIgnore
	public Date getCreateDate() {
		return createDate;
	}
	解决日期序列化
	 * 
	 */
	
	//refer to Member in the page
	private String memberName;
	private String memberNameOrder;
	
	public Date getEnrollDateDB() {
		return enrollDateDB;
	}

	public void setEnrollDateDB(Date enrollDateDB) {
		this.enrollDateDB = enrollDateDB;
	}

	public String getMemberNameOrder() {
		return memberNameOrder;
	}

	public void setMemberNameOrder(String memberNameOrder) {
		this.memberNameOrder = memberNameOrder;
	}

	public String getStatusOrder() {
		return statusOrder;
	}

	public void setStatusOrder(String statusOrder) {
		this.statusOrder = statusOrder;
	}

	public String getEnrollDateOrder() {
		return enrollDateOrder;
	}

	public void setEnrollDateOrder(String enrollDateOrder) {
		this.enrollDateOrder = enrollDateOrder;
	}

	//refer to Member Type in the page
	private String passPeriodType;
	//show total unread comments
	private String remarkNo;
	
	private Date effectiveDateDB;
	
	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	

	public String getEnrollDate() {
		return  DtoHelper.getYMDDateAndDateDiff(enrollDateDB);
	}

	public void setEnrollDate(String enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getPassPeriodType() {
		return passPeriodType;
	}

	public void setPassPeriodType(String passPeriodType) {
		this.passPeriodType = passPeriodType;
	}

	public String getRemarkNo() {
		return remarkNo;
	}

	public void setRemarkNo(String remarkNo) {
		this.remarkNo = remarkNo;
	}

	public CustomerEnrollmentDto() {
	}

	public Long getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(Object enrollId) {
		this.enrollId =(enrollId!=null ? NumberUtils.toLong(enrollId.toString()):null);
	}
	
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getEnrollType() {
		return this.enrollType;
	}

	public void setEnrollType(String enrollType) {
		this.enrollType = enrollType;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getSalesFollowBy() {
		return this.salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public String getStatus() {
		if(this.status.equalsIgnoreCase("TOA") && getNoOfDays() != null && getNoOfDays().intValue() > 0){
			this.status = this.status + "(" + getNoOfDays().toString() + " Days)";
		}
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubscribeContractLength() {
		return subscribeContractLength;
	}

	public void setSubscribeContractLength(String subscribeContractLength) {
		this.subscribeContractLength = subscribeContractLength;
	}

	public String getSubscribePlanNo() {
		return subscribePlanNo;
	}

	public void setSubscribePlanNo(String subscribePlanNo) {
		this.subscribePlanNo = subscribePlanNo;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId =(customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}
	
	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getEnrollCreateDate() {
		return enrollCreateDate;
	}

	public void setEnrollCreateDate(Date enrollCreateDate) {
		this.enrollCreateDate = enrollCreateDate;
	}

	public Boolean getIsActivationEmailEnabled() {
		return isActivationEmailEnabled;
	}

	public void setIsActivationEmailEnabled(Boolean isActivationEmailEnabled) {
		this.isActivationEmailEnabled = isActivationEmailEnabled;
	}

	public Date getEffectiveDateDB() {
		return effectiveDateDB;
	}

	public void setEffectiveDateDB(Date effectiveDateDB) {
		this.effectiveDateDB = effectiveDateDB;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getEnrollId2() {
		return enrollId2;
	}

	public void setEnrollId2(Object enrollId2) {
		this.enrollId2 =(enrollId2!=null ? NumberUtils.toLong(enrollId2.toString()):null);
	}

	public Long getCustomerId2() {
		return customerId2;
	}

	public void setCustomerId2(Object customerId2) {
		this.customerId2 =(customerId2!=null ? NumberUtils.toLong(customerId2.toString()):null);
	}
	
}