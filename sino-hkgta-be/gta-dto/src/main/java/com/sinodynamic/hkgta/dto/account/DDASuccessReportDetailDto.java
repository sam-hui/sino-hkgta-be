package com.sinodynamic.hkgta.dto.account;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDASuccessReportDetailDto implements Serializable{
	
	private String originatorId;
	private String originatorBankCode; //003 - SCB
	private String originatorAccountNumber;
	private String ddaType; // ? // S - SCB
	private String buyerCode; // ? //"" if empty
	private String buyerName; // ? //"" if empty
	private String draweeBankCode;
	private String draweeBankName;
	private String draweeBranchCode; // "" if empty
	private String debitAccountNumber;
	private String debitAccountName;
	private String startDate;//YYYYMMDD
	private String endDate;//YYYYMMDD
	private String mandateType; // ? //"" if empty or FIXED or VARIABLE 
	private String frequency; // ? //"" if empty
	private String amount; //24,6
	private String remarks; 
	private String lastStatusUpdateDate; // ? - ref only ?//format:DDMMYYYY
	private String valueDate; // ? //format:YYYYMMDD
	private String micrCode; // ? 
	private String transactionId; // ? //"" if empty
	private String mandateRemarks; // "A"
	private String mandateSeqNo; // ? 
	private String debtorId; // ? //"" if empty
	private String rcmsMandateManagementId; // ?
	private String reference; // our membership id -- VA account no.
	private String mandateStatus; //Accepted
	private String mandateStatusCode; //A
	private String currencyCode; //? - HKD ?
	private String debtorIdType1;  // ?
	private String debtorIdNumber1; // ?
	private String debtorIdType2; // ?
	private String debtorIdNumber2; // ?
	
	
	public String getOriginatorId() {
		return originatorId;
	}
	public void setOriginatorId(String originatorId) {
		this.originatorId = originatorId;
	}
	public String getOriginatorBankCode() {
		return originatorBankCode;
	}
	public void setOriginatorBankCode(String originatorBankCode) {
		this.originatorBankCode = originatorBankCode;
	}
	public String getOriginatorAccountNumber() {
		return originatorAccountNumber;
	}
	public void setOriginatorAccountNumber(String originatorAccountNumber) {
		this.originatorAccountNumber = originatorAccountNumber;
	}
	public String getDdaType() {
		return ddaType;
	}
	public void setDdaType(String ddaType) {
		this.ddaType = ddaType;
	}
	public String getBuyerCode() {
		return buyerCode;
	}
	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getDraweeBankCode() {
		return draweeBankCode;
	}
	public void setDraweeBankCode(String draweeBankCode) {
		this.draweeBankCode = draweeBankCode;
	}
	public String getDraweeBankName() {
		return draweeBankName;
	}
	public void setDraweeBankName(String draweeBankName) {
		this.draweeBankName = draweeBankName;
	}
	public String getDraweeBranchCode() {
		return draweeBranchCode;
	}
	public void setDraweeBranchCode(String draweeBranchCode) {
		this.draweeBranchCode = draweeBranchCode;
	}
	public String getDebitAccountNumber() {
		return debitAccountNumber;
	}
	public void setDebitAccountNumber(String debitAccountNumber) {
		this.debitAccountNumber = debitAccountNumber;
	}
	public String getDebitAccountName() {
		return debitAccountName;
	}
	public void setDebitAccountName(String debitAccountName) {
		this.debitAccountName = debitAccountName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getMandateType() {
		return mandateType;
	}
	public void setMandateType(String mandateType) {
		this.mandateType = mandateType;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getLastStatusUpdateDate() {
		return lastStatusUpdateDate;
	}
	public void setLastStatusUpdateDate(String lastStatusUpdateDate) {
		this.lastStatusUpdateDate = lastStatusUpdateDate;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public String getMicrCode() {
		return micrCode;
	}
	public void setMicrCode(String micrCode) {
		this.micrCode = micrCode;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getMandateRemarks() {
		return mandateRemarks;
	}
	public void setMandateRemarks(String mandateRemarks) {
		this.mandateRemarks = mandateRemarks;
	}
	public String getMandateSeqNo() {
		return mandateSeqNo;
	}
	public void setMandateSeqNo(String mandateSeqNo) {
		this.mandateSeqNo = mandateSeqNo;
	}
	public String getDebtorId() {
		return debtorId;
	}
	public void setDebtorId(String debtorId) {
		this.debtorId = debtorId;
	}
	public String getRcmsMandateManagementId() {
		return rcmsMandateManagementId;
	}
	public void setRcmsMandateManagementId(String rcmsMandateManagementId) {
		this.rcmsMandateManagementId = rcmsMandateManagementId;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getMandateStatus() {
		return mandateStatus;
	}
	public void setMandateStatus(String mandateStatus) {
		this.mandateStatus = mandateStatus;
	}
	public String getMandateStatusCode() {
		return mandateStatusCode;
	}
	public void setMandateStatusCode(String mandateStatusCode) {
		this.mandateStatusCode = mandateStatusCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getDebtorIdType1() {
		return debtorIdType1;
	}
	public void setDebtorIdType1(String debtorIdType1) {
		this.debtorIdType1 = debtorIdType1;
	}
	public String getDebtorIdNumber1() {
		return debtorIdNumber1;
	}
	public void setDebtorIdNumber1(String debtorIdNumber1) {
		this.debtorIdNumber1 = debtorIdNumber1;
	}
	public String getDebtorIdType2() {
		return debtorIdType2;
	}
	public void setDebtorIdType2(String debtorIdType2) {
		this.debtorIdType2 = debtorIdType2;
	}
	public String getDebtorIdNumber2() {
		return debtorIdNumber2;
	}
	public void setDebtorIdNumber2(String debtorIdNumber2) {
		this.debtorIdNumber2 = debtorIdNumber2;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
