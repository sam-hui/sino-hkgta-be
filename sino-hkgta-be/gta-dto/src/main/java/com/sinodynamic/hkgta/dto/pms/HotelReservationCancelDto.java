package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class HotelReservationCancelDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date currentDate;
	private String userId;
	
	private String terminal;//APP / WEB
	private String cancelReason;//why cancel
	private String requesterType = "CUSTOMER";//who request
	private boolean createRefundRequest;//if create refund request
	
	private List<HotelReservationCancelItemDto> hotelReservationCancelItems;//this is required

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	
	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getRequesterType() {
		return requesterType;
	}

	public void setRequesterType(String requesterType) {
		this.requesterType = requesterType;
	}

	public boolean isCreateRefundRequest() {
		return createRefundRequest;
	}

	public void setCreateRefundRequest(boolean createRefundRequest) {
		this.createRefundRequest = createRefundRequest;
	}

	public List<HotelReservationCancelItemDto> getHotelReservationCancelItems() {
		return hotelReservationCancelItems;
	}

	public void setHotelReservationCancelItems(
			List<HotelReservationCancelItemDto> hotelReservationCancelItems) {
		this.hotelReservationCancelItems = hotelReservationCancelItems;
	}

	public static class HotelReservationCancelItemDto implements Serializable {

		private static final long serialVersionUID = 1L;

		private String reservationId;

		public String getReservationId() {
			return reservationId;
		}

		public void setReservationId(String reservationId) {
			this.reservationId = reservationId;
		}
	}
}
