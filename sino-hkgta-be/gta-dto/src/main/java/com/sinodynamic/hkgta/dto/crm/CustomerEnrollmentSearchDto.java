package com.sinodynamic.hkgta.dto.crm;

public class CustomerEnrollmentSearchDto {
	private String pageNumber;
	private String pageSize;
	private String searchText;
	private String sortBy;
	private String isAscending;
	private String customerStatus;
	private String enrollStatus;
	private String isMyClient;
	private String userId;
	public CustomerEnrollmentSearchDto() {

	}
	public CustomerEnrollmentSearchDto(String pageNumber, String pageSize,
			String searchText, String sortBy, String isAscending,
			String customerStatus, String enrollStatus, String isMyClient,String userId) {
		super();
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.searchText = searchText;
		this.sortBy = sortBy;
		this.isAscending = isAscending;
		this.customerStatus = customerStatus;
		this.enrollStatus = enrollStatus;
		this.isMyClient = isMyClient;
		this.userId = userId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getIsAscending() {
		return isAscending;
	}
	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}
	public String getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	public String getEnrollStatus() {
		return enrollStatus;
	}
	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}
	public String getIsMyClient() {
		return isMyClient;
	}
	public void setIsMyClient(String isMyClient) {
		this.isMyClient = isMyClient;
	}

}
