package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class CustomerAdditionInfoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String customerId;

	private String captionId;

	private String createBy;

	private String customerInput;

	private String sysId;

	private String updateBy;

	private String category;

	private String caption;

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCustomerInput() {
		return this.customerInput;
	}

	public void setCustomerInput(String customerInput) {
		this.customerInput = customerInput;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getCaptionId() {
		return captionId;
	}

	public void setCaptionId(Object captionId) {
		if (captionId instanceof Integer) {
			this.captionId = String.valueOf((Integer) captionId);
		} else {
			this.captionId = (String) captionId;
		}
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

}