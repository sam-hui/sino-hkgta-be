package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class DeliveryRecordDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String academyId;

	private String member;

	private String memberType;

	private BigDecimal balance;

	private String deliveryStatus;

	private String smtpErrorCode;
	
	private Long customerId;
	
	private String nativeDeliveryStatus;

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getSmtpErrorCode() {
		return smtpErrorCode;
	}

	public void setSmtpErrorCode(String smtpErrorCode) {
		this.smtpErrorCode = smtpErrorCode;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public String getNativeDeliveryStatus() {
		return nativeDeliveryStatus;
	}

	public void setNativeDeliveryStatus(String nativeDeliveryStatus) {
		this.nativeDeliveryStatus = nativeDeliveryStatus;
	}

	
}
