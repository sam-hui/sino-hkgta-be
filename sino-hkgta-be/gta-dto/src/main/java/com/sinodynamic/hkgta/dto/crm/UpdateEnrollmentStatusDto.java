package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class UpdateEnrollmentStatusDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
    private String enrollId;
	
	private String status;

	public String getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(String enrollId) {
		this.enrollId = enrollId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
