package com.sinodynamic.hkgta.dto.fms;

import java.sql.Timestamp;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

public class FacilityItemDto {
    
	public static final String ZONE_COACHING = "ZONE-COACHING";
	public static final String ZONE_PRACTICE = "ZONE-PRACTICE";
	public static final String GOLFBAYTYPE_RH = "GOLFBAYTYPE-RH";
	public static final String GOLFBAYTYPE_LH = "GOLFBAYTYPE-LH";
	public static final String GOLFBAYTYPE_CAR = "GOLFBAYTYPE-CAR";
	
	private Long facilityNo;
	private String facilityName;
	private String facilityDescription;

	private String status;
	private Timestamp createDate;
	private String createBy;
	private Timestamp updateDate;
	private String updateBy;
	private String venueName;
//	private String attributeId;
	
	private String attributes;
//	private String zone;
	
	public String getFacilityName() {
		return facilityName;
	}
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	public String getFacilityDescription() {
		return facilityDescription;
	}
	public void setFacilityDescription(String facilityDescription) {
		this.facilityDescription = facilityDescription;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Long getFacilityNo() {
		return facilityNo;
	}
	public void setFacilityNo(Object facilityNo) {
		this.facilityNo = (facilityNo!=null ? NumberUtils.toLong(facilityNo.toString()):null);
	}
	public String getVenueName() {
	    return venueName;
	}
	public void setVenueName(String venueName) {
	    this.venueName = venueName;
	}
	public String getAttributeId() {
	    
	    if (StringUtils.isEmpty(attributes)) return null;
	    if (attributes.contains(GOLFBAYTYPE_RH)) return GOLFBAYTYPE_RH;
	    if (attributes.contains(GOLFBAYTYPE_LH)) return GOLFBAYTYPE_LH;
	    if (attributes.contains(GOLFBAYTYPE_CAR)) return GOLFBAYTYPE_CAR;
	    return "";
	}
	
//	public void setAttributeId(String attributeId) {
//	    this.attributeId = attributeId;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((facilityNo == null) ? 0 : facilityNo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacilityItemDto other = (FacilityItemDto) obj;
		if (facilityNo == null) {
			if (other.facilityNo != null)
				return false;
		} else if (!facilityNo.equals(other.facilityNo))
			return false;
		return true;
	}
	
	
	public String getAttributes() {
	    return attributes;
	}
	public void setAttributes(String attributes) {
	    this.attributes = attributes;
	}
	
	public String getZone() {
	    
	    if (StringUtils.isEmpty(attributes)) return null;
	    if (attributes.contains(ZONE_PRACTICE)) return ZONE_PRACTICE;
	    if (attributes.contains(ZONE_COACHING)) return ZONE_COACHING;
	    return "";
	}
	
//	public void setZone(String zone) {
//	    this.zone = zone;
//	}
	
	
	
}