package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class MemberFacilityBookingDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long resvId;
	
	private Long orderNo;
	
	private Long customerId;
	
	private String customerName;
	
	private String facilityType;
	
	private String facilitySubtypeId;
	
	private String bookingDate;
	
	private String attributeType;
	
	private String ageRangeCode;
	
	private Integer count;
	
	private Integer beginTime;
	
	private Integer endTime;
	
	private String paymentMethod;

	private String updateBy;
	
	private Long highPriceNumber;
	
	private Long lowPriceNumber;
	
	private BigDecimal highPriceTotal;
	
	private BigDecimal lowPriceTotal;
	
	private BigDecimal totalPrice;
	
	private BigDecimal availableBalance;
	
	private BigDecimal balance;
	
	private Long transactionId;
	
	private BigDecimal personalCreditLimit;
	
	private BigDecimal spendingLimit;
	
	private String spendingLimitUnit;
	
	private Boolean isThisTransactionValid;
	
	private String description;
	
	private String academyNo;
	
	private String contactEmail;
	private String coachId;
	private String coachName;
	
	private String highPriceItemNo;
	private String lowPriceItemNo;
	private String reserveVia;
	private String redirectUrl;
	private String location;
	
	private boolean createOffer;
	private Long originResvId;
	
	private List<CustomerFacilityBookingDetail> customerFacilityBookingDetailList;
	
	private Map<String,Integer> itemNoMap;
	
	
	public Map<String, Integer> getItemNoMap() {
		return itemNoMap;
	}

	public void setItemNoMap(Map<String, Integer> itemNoMap) {
		this.itemNoMap = itemNoMap;
	}

	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

	public String getReserveVia() {
		return reserveVia;
	}

	public void setReserveVia(String reserveVia) {
		this.reserveVia = reserveVia;
	}

	public String getCoachName() {
		return coachName;
	}

	public String getHighPriceItemNo()
	{
		return highPriceItemNo;
	}

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	public void setHighPriceItemNo(String highPriceItemNo)
	{
		this.highPriceItemNo = highPriceItemNo;
	}

	public String getLowPriceItemNo()
	{
		return lowPriceItemNo;
	}

	public void setLowPriceItemNo(String lowPriceItemNo)
	{
		this.lowPriceItemNo = lowPriceItemNo;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public String getCoachId() {
		return coachId;
	}

	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}

	public MemberFacilityBookingDto() {
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

	public String getFacilitySubtypeId()
	{
		return facilitySubtypeId;
	}

	public void setFacilitySubtypeId(String facilitySubtypeId)
	{
		this.facilitySubtypeId = facilitySubtypeId;
	}

	public String getBookingDate()
	{
		return bookingDate;
	}

	public void setBookingDate(String bookingDate)
	{
		this.bookingDate = bookingDate;
	}

	public String getAttributeType()
	{
		return attributeType;
	}

	public void setAttributeType(String attributeType)
	{
		this.attributeType = attributeType;
	}

	public String getAgeRangeCode()
	{
		return ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode)
	{
		this.ageRangeCode = ageRangeCode;
	}

	public Integer getCount()
	{
		return count;
	}

	public void setCount(Integer count)
	{
		this.count = count;
	}

	

	public Integer getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Integer beginTime) {
		this.beginTime = beginTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Long getHighPriceNumber()
	{
		return highPriceNumber;
	}

	public void setHighPriceNumber(Long highPriceNumber)
	{
		this.highPriceNumber = highPriceNumber;
	}

	public Long getLowPriceNumber()
	{
		return lowPriceNumber;
	}

	public void setLowPriceNumber(Long lowPriceNumber)
	{
		this.lowPriceNumber = lowPriceNumber;
	}

	public BigDecimal getHighPriceTotal()
	{
		return highPriceTotal;
	}

	public void setHighPriceTotal(BigDecimal highPriceTotal)
	{
		this.highPriceTotal = highPriceTotal;
	}

	public BigDecimal getLowPriceTotal()
	{
		return lowPriceTotal;
	}

	public void setLowPriceTotal(BigDecimal lowPriceTotal)
	{
		this.lowPriceTotal = lowPriceTotal;
	}

	public Long getResvId()
	{
		return resvId;
	}

	public void setResvId(Long resvId)
	{
		this.resvId = resvId;
	}

	public Long getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(Long customerId)
	{
		this.customerId = customerId;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	public BigDecimal getAvailableBalance()
	{
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance)
	{
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBalance()
	{
		return balance;
	}

	public void setBalance(BigDecimal balance)
	{
		this.balance = balance;
	}

	public Long getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(Long transactionId)
	{
		this.transactionId = transactionId;
	}

	public BigDecimal getPersonalCreditLimit()
	{
		return personalCreditLimit;
	}

	public void setPersonalCreditLimit(BigDecimal personalCreditLimit)
	{
		this.personalCreditLimit = personalCreditLimit;
	}

	public BigDecimal getSpendingLimit()
	{
		return spendingLimit;
	}

	public void setSpendingLimit(BigDecimal spendingLimit)
	{
		this.spendingLimit = spendingLimit;
	}

	public String getSpendingLimitUnit()
	{
		return spendingLimitUnit;
	}

	public void setSpendingLimitUnit(String spendingLimitUnit)
	{
		this.spendingLimitUnit = spendingLimitUnit;
	}

	public Boolean getIsThisTransactionValid()
	{
		return isThisTransactionValid;
	}

	public void setIsThisTransactionValid(Boolean isThisTransactionValid)
	{
		this.isThisTransactionValid = isThisTransactionValid;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Long getOrderNo()
	{
		return orderNo;
	}

	public void setOrderNo(Long orderNo)
	{
		this.orderNo = orderNo;
	}

	public String getAcademyNo()
	{
		return academyNo;
	}

	public void setAcademyNo(String academyNo)
	{
		this.academyNo = academyNo;
	}

	public String getContactEmail()
	{
		return contactEmail;
	}

	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}

	public List<CustomerFacilityBookingDetail> getCustomerFacilityBookingDetailList()
	{
		return customerFacilityBookingDetailList;
	}

	public void setCustomerFacilityBookingDetailList(List<CustomerFacilityBookingDetail> customerFacilityBookingDetailList)
	{
		this.customerFacilityBookingDetailList = customerFacilityBookingDetailList;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public boolean isCreateOffer() {
		return createOffer;
	}

	public void setCreateOffer(boolean createOffer) {
		this.createOffer = createOffer;
	}

	public Long getOriginResvId() {
		return originResvId;
	}

	public void setOriginResvId(Long originResvId) {
		this.originResvId = originResvId;
	}
}