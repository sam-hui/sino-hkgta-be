package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;

public class FacilityTimeslotLogDto implements Serializable {

	private static final long serialVersionUID = -8274191717615723417L;

	private Long logId;
	private Long facilityTimeslotId;
	private String facilityType;
	private Long facilityNo;
	private Date beginDatetime;
	private Date endDatetime;
	private String status;
	private String reserveType;
	private Long transferFromTimeslotId;
	private String internalRemark;
	private Date createDate;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public Long getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}

	public Date getBeginDatetime() {
		return beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public Date getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReserveType() {
		return reserveType;
	}

	public void setReserveType(String reserveType) {
		this.reserveType = reserveType;
	}

	public Long getTransferFromTimeslotId() {
		return transferFromTimeslotId;
	}

	public void setTransferFromTimeslotId(Long transferFromTimeslotId) {
		this.transferFromTimeslotId = transferFromTimeslotId;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public static List<FacilityTimeslotLogDto> parseEntities(List<FacilityTimeslotLog> list) {
		List<FacilityTimeslotLogDto> retList = new ArrayList<FacilityTimeslotLogDto>();
		if (list != null && list.size() == 0) {
			return retList;
		}
		FacilityTimeslotLogDto dto = null;
		for (FacilityTimeslotLog log : list) {
			dto = new FacilityTimeslotLogDto();
			dto.setCreateDate(log.getCreateDate());
			dto.setFacilityNo(log.getFacilityNo());
			dto.setInternalRemark(log.getInternalRemark());
			dto.setLogId(log.getLogId());
			dto.setBeginDatetime(log.getBeginDatetime());
			dto.setEndDatetime(log.getEndDatetime());
			retList.add(dto);
		}
		return retList;
	}
}
