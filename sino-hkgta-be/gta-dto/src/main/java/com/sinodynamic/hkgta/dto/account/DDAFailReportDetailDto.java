package com.sinodynamic.hkgta.dto.account;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDAFailReportDetailDto implements Serializable{
	
	private String originatorId;
	private String originatorBankCode; //003 - SCB
	private String originatorAccountNumber;
	private String ddaType; // ? // S - SCB
	private String buyerCode; // ? //"" if empty
	private String buyerName; // ? //"" if empty
	private String draweeBankCode;
	private String draweeBankName;
	private String draweeBranchCode; // "" if empty
	private String debitAccountNumber;
	private String debitAccountName;
	private String startDate;//DDMMYYYY
	private String endDate;//DDMMYYYY 
	private String mandateType; //"V"
	private String frequency; //"" if empty
	private String amount; //24,6
	private String remarks;
	private String rejectCode;//Return to CMS
	private String rejectDesc;//Return to CMS
	private String valueDate;
	private String micrCode;
	private String debtorId;
	private String reference;
	private String deletionDate;
	private String reactivaitonDate;
	private String lastStatusUpdateDate;
	private String actionIndicator;
	private String resultIndicator;
	private String mandateStatus;
	private String mandateStatusCode;
	private String currencyCode;
	private String debtorIdType1;
	private String debtorIdNumber1;
	private String debtorIdType2;
	private String debtorIdNumber2;
	
	public String getOriginatorId() {
		return originatorId;
	}
	public void setOriginatorId(String originatorId) {
		this.originatorId = originatorId;
	}
	public String getOriginatorBankCode() {
		return originatorBankCode;
	}
	public void setOriginatorBankCode(String originatorBankCode) {
		this.originatorBankCode = originatorBankCode;
	}
	public String getOriginatorAccountNumber() {
		return originatorAccountNumber;
	}
	public void setOriginatorAccountNumber(String originatorAccountNumber) {
		this.originatorAccountNumber = originatorAccountNumber;
	}
	public String getDdaType() {
		return ddaType;
	}
	public void setDdaType(String ddaType) {
		this.ddaType = ddaType;
	}
	public String getBuyerCode() {
		return buyerCode;
	}
	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getDraweeBankCode() {
		return draweeBankCode;
	}
	public void setDraweeBankCode(String draweeBankCode) {
		this.draweeBankCode = draweeBankCode;
	}
	public String getDraweeBankName() {
		return draweeBankName;
	}
	public void setDraweeBankName(String draweeBankName) {
		this.draweeBankName = draweeBankName;
	}
	public String getDraweeBranchCode() {
		return draweeBranchCode;
	}
	public void setDraweeBranchCode(String draweeBranchCode) {
		this.draweeBranchCode = draweeBranchCode;
	}
	public String getDebitAccountNumber() {
		return debitAccountNumber;
	}
	public void setDebitAccountNumber(String debitAccountNumber) {
		this.debitAccountNumber = debitAccountNumber;
	}
	public String getDebitAccountName() {
		return debitAccountName;
	}
	public void setDebitAccountName(String debitAccountName) {
		this.debitAccountName = debitAccountName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getMandateType() {
		return mandateType;
	}
	public void setMandateType(String mandateType) {
		this.mandateType = mandateType;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRejectCode() {
		return rejectCode;
	}
	public void setRejectCode(String rejectCode) {
		this.rejectCode = rejectCode;
	}
	public String getRejectDesc() {
		return rejectDesc;
	}
	public void setRejectDesc(String rejectDesc) {
		this.rejectDesc = rejectDesc;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public String getMicrCode() {
		return micrCode;
	}
	public void setMicrCode(String micrCode) {
		this.micrCode = micrCode;
	}
	public String getDebtorId() {
		return debtorId;
	}
	public void setDebtorId(String debtorId) {
		this.debtorId = debtorId;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getDeletionDate() {
		return deletionDate;
	}
	public void setDeletionDate(String deletionDate) {
		this.deletionDate = deletionDate;
	}
	public String getReactivaitonDate() {
		return reactivaitonDate;
	}
	public void setReactivaitonDate(String reactivaitonDate) {
		this.reactivaitonDate = reactivaitonDate;
	}
	public String getLastStatusUpdateDate() {
		return lastStatusUpdateDate;
	}
	public void setLastStatusUpdateDate(String lastStatusUpdateDate) {
		this.lastStatusUpdateDate = lastStatusUpdateDate;
	}
	public String getActionIndicator() {
		return actionIndicator;
	}
	public void setActionIndicator(String actionIndicator) {
		this.actionIndicator = actionIndicator;
	}
	public String getResultIndicator() {
		return resultIndicator;
	}
	public void setResultIndicator(String resultIndicator) {
		this.resultIndicator = resultIndicator;
	}
	public String getMandateStatus() {
		return mandateStatus;
	}
	public void setMandateStatus(String mandateStatus) {
		this.mandateStatus = mandateStatus;
	}
	public String getMandateStatusCode() {
		return mandateStatusCode;
	}
	public void setMandateStatusCode(String mandateStatusCode) {
		this.mandateStatusCode = mandateStatusCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getDebtorIdType1() {
		return debtorIdType1;
	}
	public void setDebtorIdType1(String debtorIdType1) {
		this.debtorIdType1 = debtorIdType1;
	}
	public String getDebtorIdNumber1() {
		return debtorIdNumber1;
	}
	public void setDebtorIdNumber1(String debtorIdNumber1) {
		this.debtorIdNumber1 = debtorIdNumber1;
	}
	public String getDebtorIdType2() {
		return debtorIdType2;
	}
	public void setDebtorIdType2(String debtorIdType2) {
		this.debtorIdType2 = debtorIdType2;
	}
	public String getDebtorIdNumber2() {
		return debtorIdNumber2;
	}
	public void setDebtorIdNumber2(String debtorIdNumber2) {
		this.debtorIdNumber2 = debtorIdNumber2;
	}
	/*@Override
	public String toString() {
		return "DDAFailReportDetail [originatorId=" + originatorId
				+ ", originatorBankCode=" + originatorBankCode
				+ ", originatorAccountNumber=" + originatorAccountNumber
				+ ", ddaType=" + ddaType + ", buyerCode=" + buyerCode
				+ ", buyerName=" + buyerName + ", draweeBankCode="
				+ draweeBankCode + ", draweeBankName=" + draweeBankName
				+ ", draweeBranchCode=" + draweeBranchCode
				+ ", debitAccountNumber=" + debitAccountNumber
				+ ", debitAccountName=" + debitAccountName + ", startDate="
				+ startDate + ", endDate=" + endDate + ", mandateType="
				+ mandateType + ", frequency=" + frequency + ", amount="
				+ amount + ", remarks=" + remarks + ", rejectCode="
				+ rejectCode + ", rejectDesc=" + rejectDesc + ", valueDate="
				+ valueDate + ", micrCode=" + micrCode + ", debtorId="
				+ debtorId + ", reference=" + reference + ", deletionDate="
				+ deletionDate + ", reactivaitonDate=" + reactivaitonDate
				+ ", lastStatusUpdateDate=" + lastStatusUpdateDate
				+ ", actionIndicator=" + actionIndicator + ", resultIndicator="
				+ resultIndicator + ", mandateStatus=" + mandateStatus
				+ ", mandateStatusCode=" + mandateStatusCode
				+ ", currencyCode=" + currencyCode + ", debtorIdType1="
				+ debtorIdType1 + ", debtorIdNumber1=" + debtorIdNumber1
				+ ", debtorIdType2=" + debtorIdType2 + ", debtorIdNumber2="
				+ debtorIdNumber2 + "]";
	}*/
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
		
}
