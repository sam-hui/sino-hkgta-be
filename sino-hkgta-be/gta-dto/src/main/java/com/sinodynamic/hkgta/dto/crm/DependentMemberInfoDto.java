package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class DependentMemberInfoDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5161271929830212754L;


	public DependentMemberInfoDto(){
		super();
	}
	
	public DependentMemberInfoDto(Long customerId) {
		super();
		this.customerId = customerId;
	}
	
	private Long customerId;
	private String academyNo;
	private String memberName;
	private String nationality;
	private Date firstJoinDate;
	private Date dateOfBirth;
	private String portraitPhoto;
	private String phoneMobile;
	private String contactEmail;
	private BigDecimal transactionLimit;
	private boolean daypassPurchase;
	private boolean facility;
	private boolean training;
	private boolean events;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
//		if(customerId instanceof Long){
//			this.customerId = BigInteger.valueOf(((Long)customerId).longValue());
//		}else{
			this.customerId = customerId;
//		}
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setSurname(String memberName) {
		this.memberName = memberName;
	}

	public Date getFirstJoinDate() {
		return firstJoinDate;
	}

	public void setFirstJoinDate(Date firstJoinDate) {
		this.firstJoinDate = firstJoinDate;
	}

	public String getDateOfBirth() {
		if(dateOfBirth==null) return "";
		return DtoHelper.getYMDFormatDate(dateOfBirth);
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public BigDecimal getTransactionLimit() {
//		if(transactionLimit == null) return BigDecimal.ZERO;
		return transactionLimit;
	}

	public void setTransactionLimit(BigDecimal transactionLimit) {
		this.transactionLimit = transactionLimit;
	}

	public boolean getDaypassPurchase() {
		
		return daypassPurchase;
	}

	public void setDaypassPurchase(boolean daypassPurchase) {
		this.daypassPurchase = daypassPurchase;
	}

	public boolean isFacility() {
		return facility;
	}

	public void setFacility(boolean facility) {
		this.facility = facility;
	}

	public boolean isTraining() {
		return training;
	}

	public void setTraining(boolean training) {
		this.training = training;
	}

	public boolean isEvents() {
		return events;
	}

	public void setEvents(boolean events) {
		this.events = events;
	}

	
}
