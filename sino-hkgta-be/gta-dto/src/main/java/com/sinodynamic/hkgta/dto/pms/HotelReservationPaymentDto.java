package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * This Dto is for online payment
 * */
public class HotelReservationPaymentDto implements Serializable {
	public static class HotelReservationItemInfoDto implements Serializable {

		private static final long serialVersionUID = 1L;
		private BigDecimal itemAmount;
		private BigDecimal amountAfterTax;
		private String itemNo;
		private String reservationId;
		private Long nights;
		
		public BigDecimal getItemAmount() {
			return itemAmount;
		}

		public void setItemAmount(BigDecimal itemAmount) {
			this.itemAmount = itemAmount;
		}
		
		public BigDecimal getAmountAfterTax() {
			return amountAfterTax;
		}

		public void setAmountAfterTax(BigDecimal amountAfterTax) {
			this.amountAfterTax = amountAfterTax;
		}

		public String getItemNo() {
			return itemNo;
		}

		public void setItemNo(String itemNo) {
			this.itemNo = itemNo;
		}

		public String getReservationId() {
			return reservationId;
		}

		public void setReservationId(String reservationId) {
			this.reservationId = reservationId;
		}

		public Long getNights() {
			return nights;
		}

		public void setNights(Long nights) {
			this.nights = nights;
		}

		
	}

	public static class HotelReservationPaymentInfoDto implements Serializable {

		private static final long serialVersionUID = 1L;
		private BigDecimal paidAmount;
		private String agentTransactionNo;
		private String terminalId;
		

		public BigDecimal getPaidAmount() {
			return paidAmount;
		}

		public void setPaidAmount(BigDecimal paidAmount) {
			this.paidAmount = paidAmount;
		}

		public String getAgentTransactionNo() {
			return agentTransactionNo;
		}

		public void setAgentTransactionNo(String agentTransactionNo) {
			this.agentTransactionNo = agentTransactionNo;
		}

		public String getTerminalId() {
			return terminalId;
		}

		public void setTerminalId(String terminalId) {
			this.terminalId = terminalId;
		}

		
	}
	
	public static class CardInfoDto implements Serializable {
		private static final long serialVersionUID = 1L;
		private String certificateNumber;
		/**
		 * MC - MasterCard, VI - Visa;
		 */
		private String cardCode;
		private String maskedCardNumber;
		/**
		 * YYMM
		 */
		private String expireDate;
		private String cardHolderName;

		public String getCertificateNumber() {
			return certificateNumber;
		}

		public void setCertificateNumber(String certificateNumber) {
			this.certificateNumber = certificateNumber;
		}

		public String getCardCode() {
			return cardCode;
		}

		public void setCardCode(String cardCode) {
			this.cardCode = cardCode;
		}

		public String getMaskedCardNumber() {
			return maskedCardNumber;
		}

		public void setMaskedCardNumber(String maskedCardNumber) {
			this.maskedCardNumber = maskedCardNumber;
		}

		public String getExpireDate() {
			return expireDate;
		}

		public void setExpireDate(String expireDate) {
			this.expireDate = expireDate;
		}

		public String getCardHolderName() {
			return cardHolderName;
		}

		public void setCardHolderName(String cardHolderName) {
			this.cardHolderName = cardHolderName;
		}
		
	}
	
	public static class PaymentResult implements Serializable {

		private static final long serialVersionUID = 1L;

		private String transactionNo;
		private Long orderNo;
		private BigDecimal leftAmount;
		private BigDecimal limitValue;

		public String getTransactionNo() {
			return transactionNo;
		}

		public void setTransactionNo(String transactionNo) {
			this.transactionNo = transactionNo;
		}

		public Long getOrderNo() {
			return orderNo;
		}

		public void setOrderNo(Long orderNo) {
			this.orderNo = orderNo;
		}

		public BigDecimal getLeftAmount() {
			return leftAmount;
		}

		public void setLeftAmount(BigDecimal leftAmount) {
			this.leftAmount = leftAmount;
		}

		public BigDecimal getLimitValue() {
			return limitValue;
		}

		public void setLimitValue(BigDecimal limitValue) {
			this.limitValue = limitValue;
		}
	}

	private static final long serialVersionUID = 1L;

	private Date currentDate;
	private String userId;
	private Long customerId;
	private String paymentMethod;
	private String terminal;
	private CardInfoDto cardInfo;
	private PaymentResult paymentResult;
	private List<HotelReservationPaymentInfoDto> hotelReservationPaymentsInfo;
	private List<HotelReservationItemInfoDto> hotelReservationItemsInfo;
	
	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	@Deprecated
	public void setStaffUserId(String userId) {//It's the current user id instead of staff user id
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public CardInfoDto getCardInfo() {
		return cardInfo;
	}

	public void setCardInfo(CardInfoDto cardInfo) {
		this.cardInfo = cardInfo;
	}

	public PaymentResult getPaymentResult() {
		return paymentResult;
	}

	public void setPaymentResult(PaymentResult paymentResult) {
		this.paymentResult = paymentResult;
	}

	public List<HotelReservationPaymentInfoDto> getHotelReservationPaymentsInfo() {
		return hotelReservationPaymentsInfo;
	}

	public void setHotelReservationPaymentsInfo(
			List<HotelReservationPaymentInfoDto> hotelReservationPaymentsInfo) {
		this.hotelReservationPaymentsInfo = hotelReservationPaymentsInfo;
	}

	public List<HotelReservationItemInfoDto> getHotelReservationItemsInfo() {
		return hotelReservationItemsInfo;
	}

	public void setHotelReservationItemsInfo(
			List<HotelReservationItemInfoDto> hotelReservationItemsInfo) {
		this.hotelReservationItemsInfo = hotelReservationItemsInfo;
	}

	

}
