package com.sinodynamic.hkgta.dto.push;

import java.io.Serializable;

public class SubscriptDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2684254002300970296L;
	private String topicArn;
	private String endporintArn;
	public String getTopicArn() {
		return topicArn;
	}
	public void setTopicArn(String topicArn) {
		this.topicArn = topicArn;
	}
	public String getEndporintArn() {
		return endporintArn;
	}
	public void setEndporintArn(String endporintArn) {
		this.endporintArn = endporintArn;
	}
	

}
