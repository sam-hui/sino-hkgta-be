package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class BeaconMemberDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long customerId;
	
	private String status;
	
	private String portraitPhoto;
	
	private String fullName;
	
	private String academyNo;
	
	private String phone;
	
	private String greeting;
	
	private ReservationDataDto reservation;
	
	private String accommodationType;
	
	private String foodBeverage;
	
	private String hobbies;
	
	private String foodAllergy;
	
	private String vegetarian;
	
	private Date lastTimestamp;

	private String timeDifference;
	
    private String givenNameNls;
    
    private String surnameNls;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	public ReservationDataDto getReservation() {
		return reservation;
	}

	public void setReservation(ReservationDataDto reservation) {
		this.reservation = reservation;
	}

	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}

	public String getFoodBeverage() {
		return foodBeverage;
	}

	public void setFoodBeverage(String foodBeverage) {
		this.foodBeverage = foodBeverage;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getFoodAllergy() {
		return foodAllergy;
	}

	public void setFoodAllergy(String foodAllergy) {
		this.foodAllergy = foodAllergy;
	}

	public String getVegetarian() {
		return vegetarian;
	}

	public void setVegetarian(String vegetarian) {
		this.vegetarian = vegetarian;
	}

	public Date getLastTimestamp() {
		return lastTimestamp;
	}

	public void setLastTimestamp(Date lastTimestamp) {
		this.lastTimestamp = lastTimestamp;
	}

	public String getTimeDifference() {
		return timeDifference;
	}

	public void setTimeDifference(String timeDifference) {
		this.timeDifference = timeDifference;
	}

	public String getGivenNameNls() {
		return givenNameNls;
	}

	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = givenNameNls;
	}

	public String getSurnameNls() {
		return surnameNls;
	}

	public void setSurnameNls(String surnameNls) {
		this.surnameNls = surnameNls;
	}
	
}
