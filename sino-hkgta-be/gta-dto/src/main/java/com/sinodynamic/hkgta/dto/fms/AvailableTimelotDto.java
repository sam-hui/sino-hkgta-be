package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

/**
 * @author Kevin_Liang
 *
 */
public class AvailableTimelotDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3680624824960939523L;
    private String timeslotId;
    private String timeslot;
	public String getTimeslotId() {
		return timeslotId;
	}
	public void setTimeslotId(String timeslotId) {
		this.timeslotId = timeslotId;
	}
	public String getTimeslot() {
		return timeslot;
	}
	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}
}
