package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.math.BigDecimal;

public class MMSPaymentDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private  String invoiceNo;
	private Long customerId;
	private BigDecimal amount;
	private String paymentMethod;
	private String orderNo;
	private Long transNo;
	private BigDecimal availiableBalance;
	private String isRemote;
	private String redirectUrl;
	private String settleTime;
	
	
	
	public String getSettleTime() {
		return settleTime;
	}
	public void setSettleTime(String settleTime) {
		this.settleTime = settleTime;
	}
	public String getIsRemote() {
		return isRemote;
	}
	public void setIsRemote(String isRemote) {
		this.isRemote = isRemote;
	}
	public BigDecimal getAvailiableBalance() {
		return availiableBalance;
	}
	public void setAvailiableBalance(BigDecimal availiableBalance) {
		this.availiableBalance = availiableBalance;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public Long getTransNo() {
		return transNo;
	}
	public void setTransNo(Long transNo) {
		this.transNo = transNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
		
}
