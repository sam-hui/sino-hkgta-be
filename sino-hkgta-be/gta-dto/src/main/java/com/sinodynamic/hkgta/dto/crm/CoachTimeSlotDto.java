package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

public class CoachTimeSlotDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8448456399208991894L;
	
	private String userId;
	
	private Date timeSlotBeginTime;
	
	private Date timeSlotEndTime;

	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}




	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}




	/**
	 * @return the timeSlotBeginTime
	 */
	public Date getTimeSlotBeginTime() {
		return timeSlotBeginTime;
	}




	/**
	 * @param timeSlotBeginTime the timeSlotBeginTime to set
	 */
	public void setTimeSlotBeginTime(Date timeSlotBeginTime) {
		this.timeSlotBeginTime = timeSlotBeginTime;
	}




	/**
	 * @return the timeSlotEndTime
	 */
	public Date getTimeSlotEndTime() {
		return timeSlotEndTime;
	}




	/**
	 * @param timeSlotEndTime the timeSlotEndTime to set
	 */
	public void setTimeSlotEndTime(Date timeSlotEndTime) {
		this.timeSlotEndTime = timeSlotEndTime;
	}




	
	/**
	 * 
	 */
	public CoachTimeSlotDto() {
		// TODO Auto-generated constructor stub
	}



}
