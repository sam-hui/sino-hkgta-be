package com.sinodynamic.hkgta.dto.crm;

import java.util.List;


public class AdvertiseImageUploadDto {

	private List<AdvertiseImageDto> advertiseImages;

	public List<AdvertiseImageDto> getAdvertiseImages()
	{
		return advertiseImages;
	}

	public void setAdvertiseImages(List<AdvertiseImageDto> advertiseImages)
	{
		this.advertiseImages = advertiseImages;
	}

}
