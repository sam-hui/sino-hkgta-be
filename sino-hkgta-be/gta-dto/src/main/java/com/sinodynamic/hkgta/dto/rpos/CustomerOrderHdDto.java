package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class CustomerOrderHdDto  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2333109774064074647L;
	private Long orderNo;
	private BigDecimal orderTotalAmount;
	private String customerSurname;
	private String customerGivenname;
	private String customerSalutation;
	private Date effectiveDate;
	private BigDecimal balanceDue;
	private boolean hasRemark;
	private String priceDescription;
	
	
	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}
	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}
	public String getCustomerSurname() {
		return customerSurname;
	}
	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}
	public String getCustomerGivenname() {
		return customerGivenname;
	}
	public void setCustomerGivenname(String customerGivenname) {
		this.customerGivenname = customerGivenname;
	}
	public String getCustomerSalutation() {
		return customerSalutation;
	}
	public void setCustomerSalutation(String customerSalutation) {
		this.customerSalutation = customerSalutation;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public BigDecimal getBalanceDue() {
		return balanceDue;
	}
	public void setBalanceDue(BigDecimal balanceDue) {
		this.balanceDue = balanceDue;
	}
	public boolean isHasRemark() {
		return hasRemark;
	}
	public void setHasRemark(boolean hasRemark) {
		this.hasRemark = hasRemark;
	}
	public String getPriceDescription() {
		return priceDescription;
	}
	public void setPriceDescription(String priceDescription) {
		this.priceDescription = priceDescription;
	}
}
