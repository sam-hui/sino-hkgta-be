package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

@SuppressWarnings("deprecation")
public class MemberTransactionDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date transactionTimestamp;
	private Long transactionNo;
	private String paymentMethodCode;
	private String itemCatagory;
	private String memberName;
	private BigDecimal paidAmount;
	private String description;
	private String transactionTime;
	private String transactionDate;

	public String getTransactionTimestamp() {
		if (transactionTimestamp == null) {
			return "";
		}
		return DtoHelper.getYMDDateAndDateDiff(transactionTimestamp);
	}

	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}

	public Long getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getPaymentMethodCode() {
		return DtoHelper.nvl(paymentMethodCode);
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getItemCatagory() {
		return DtoHelper.nvl(itemCatagory);
	}

	public void setItemCatagory(String itemCatagory) {
		this.itemCatagory = itemCatagory;
	}

	public String getMemberName() {
		return DtoHelper.nvl(memberName);
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

}
