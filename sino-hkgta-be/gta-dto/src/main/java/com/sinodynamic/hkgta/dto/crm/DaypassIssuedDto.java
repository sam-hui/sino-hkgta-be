package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class DaypassIssuedDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String customerId;
    private String guest;
    private String dayPassId;
    private String dayPassType;

    public String getCustomerId() {
	return customerId;
    }

    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    public String getGuest() {
	return guest;
    }

    public void setGuest(String guest) {
	this.guest = guest;
    }

    public String getDayPassId() {
	return dayPassId;
    }

    public void setDayPassId(String dayPassId) {
	this.dayPassId = dayPassId;
    }

    public String getDayPassType() {
	return dayPassType;
    }

    public void setDayPassType(String dayPassType) {
	this.dayPassType = dayPassType;
    }

}
