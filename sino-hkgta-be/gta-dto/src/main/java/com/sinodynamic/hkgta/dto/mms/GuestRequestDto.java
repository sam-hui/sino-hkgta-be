package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

public class GuestRequestDto implements Serializable {

    private static final long serialVersionUID = -2493240585969994371L;
    
    private Long sysId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String guestId;
    private String guestCode;
    private String mobilePhone;
    private String email;
    private String gender;
    private String passwordOld;
    private String passwordNew1;
    private String passwordNew2;
    private String address1;
    private String address2;
    private String city;
    private String stateId;
    private String countryId;
    private String zipCode;
    private String userPassword;
    private String action;
    
    
    public Long getSysId() {
        return sysId;
    }

    public void setSysId(Object sysId) {
	
        this.sysId = (sysId == null? null : Long.parseLong(String.valueOf(sysId)));
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getMiddleName() {
	return middleName;
    }

    public void setMiddleName(String middleName) {
	this.middleName = middleName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getGuestId() {
	return guestId;
    }

    public void setGuestId(String guestId) {
	this.guestId = guestId;
    }

    public String getGuestCode() {
	return guestCode;
    }

    public void setGuestCode(String guestCode) {
	this.guestCode = guestCode;
    }

    public String getMobilePhone() {
	return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
	this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getGender() {
	return gender;
    }

    public void setGender(Object gender) {
	this.gender = (gender == null? null : String.valueOf(gender));
    }

    public String getPasswordOld() {
	return passwordOld;
    }

    public void setPasswordOld(String passwordOld) {
	this.passwordOld = passwordOld;
    }

    public String getPasswordNew1() {
	return passwordNew1;
    }

    public void setPasswordNew1(String passwordNew1) {
	this.passwordNew1 = passwordNew1;
    }

    public String getPasswordNew2() {
	return passwordNew2;
    }

    public void setPasswordNew2(String passwordNew2) {
	this.passwordNew2 = passwordNew2;
    }

    public String getAddress1() {
	return address1;
    }

    public void setAddress1(String address1) {
	this.address1 = address1;
    }

    public String getAddress2() {
	return address2;
    }

    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getStateId() {
	return stateId;
    }

    public void setStateId(String stateId) {
	this.stateId = stateId;
    }

    public String getCountryId() {
	return countryId;
    }

    public void setCountryId(String countryId) {
	this.countryId = countryId;
    }

    public String getZipCode() {
	return zipCode;
    }

    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    public String getUserPassword() {
	return userPassword;
    }

    public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
