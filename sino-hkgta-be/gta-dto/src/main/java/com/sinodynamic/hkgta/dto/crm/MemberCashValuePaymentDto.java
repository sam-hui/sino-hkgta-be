package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class MemberCashValuePaymentDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/*required*/
	private Long customerId;
	
	private BigDecimal totalAmount;
	
	private Map<String,Integer> itemNoMap;
	private String[] itemNos;
	
	private Integer orderQty;
	private String userId;
	private String reserveVia;
	/*not required*/
	private String paymentMethod;
	
	private Boolean isOnlinePayment;
	private String location;
	
	private String terminalId;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getReserveVia() {
		return reserveVia;
	}

	public void setReserveVia(String reserveVia) {
		this.reserveVia = reserveVia;
	}

	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount)
	{
		this.totalAmount = totalAmount;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public Boolean getIsOnlinePayment()
	{
		return isOnlinePayment;
	}

	public void setIsOnlinePayment(Boolean isOnlinePayment)
	{
		this.isOnlinePayment = isOnlinePayment;
	}

	public Map<String, Integer> getItemNoMap()
	{
		return itemNoMap;
	}

	public void setItemNoMap(Map<String, Integer> itemNoMap)
	{
		this.itemNoMap = itemNoMap;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String[] getItemNos() {
		return itemNos;
	}

	public void setItemNos(String[] itemNos) {
		this.itemNos = itemNos;
	}

	public Integer getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(Integer orderQty) {
		this.orderQty = orderQty;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	
	
}
