package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;

public class RoomDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long roomId;

	private String roomNo;

	private String status;
	
	private String frontdeskStatus;

	private Boolean noResponse;

	private Long roomCrewCount;

	private Long adHocCount;
	private Long routineCount;
	private Long scheduleCount;
	private Long maintenanceCount;

	

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Object roomId) {
		this.roomId = roomId != null ? NumberUtils.toLong(roomId.toString()) : null;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getNoResponse() {
		return noResponse;
	}

	public void setNoResponse(Boolean noResponse) {
		this.noResponse = noResponse;
	}

	

	

	public Long getRoomCrewCount() {
		return roomCrewCount;
	}

	public void setRoomCrewCount(Object roomCrewCount) {
		this.roomCrewCount = (roomCrewCount!=null ? NumberUtils.toLong(roomCrewCount.toString()):null);
	}

	public Long getAdHocCount() {
		return adHocCount;
	}

	public void setAdHocCount(Object adHocCount) {
		this.adHocCount = (adHocCount!=null ? NumberUtils.toLong(adHocCount.toString()):null);
	}

	public Long getRoutineCount() {
		return routineCount;
	}

	public void setRoutineCount(Object routineCount) {
		this.routineCount = (routineCount!=null ? NumberUtils.toLong(routineCount.toString()):null);
	}

	public Long getScheduleCount() {
		return scheduleCount;
	}

	public void setScheduleCount(Object scheduleCount) {
		this.scheduleCount = (scheduleCount!=null ? NumberUtils.toLong(scheduleCount.toString()):null);
	}

	public Long getMaintenanceCount() {
		return maintenanceCount;
	}

	public void setMaintenanceCount(Object maintenanceCount) {
		this.maintenanceCount =  (maintenanceCount!=null ? NumberUtils.toLong(maintenanceCount.toString()):null);;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getFrontdeskStatus()
	{
		return frontdeskStatus;
	}

	public void setFrontdeskStatus(String frontdeskStatus)
	{
		this.frontdeskStatus = frontdeskStatus;
	}
}
