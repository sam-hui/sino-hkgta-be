package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class SpaRetreatDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long displayOrder;
	private Long retId;
	private String retName;
	private String picPath;
	private String description;
	private String status;
	private Long itemSize;
	
	public SpaRetreatDto(){
		
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Long getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Object displayOrder) {
		this.displayOrder = (displayOrder!=null ? NumberUtils.toLong(displayOrder.toString()):null);
	}
	public Long getRetId() {
		return retId;
	}
	public void setRetId(Object retId) {
		this.retId = (retId!=null ? NumberUtils.toLong(retId.toString()):null);
	}
	public String getRetName() {
		return retName;
	}
	public void setRetName(String retName) {
		this.retName = retName;
	}
	public Long getItemSize() {
		return itemSize;
	}
	public void setItemSize(Object itemSize) {
		this.itemSize = (itemSize!=null ? NumberUtils.toLong(itemSize.toString()):null);
	}
	
	
}
