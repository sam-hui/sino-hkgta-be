package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class MemberAttendanceDto implements Serializable {

	private static final long serialVersionUID = -1371912768347486929L;

	public MemberAttendanceDto() {
		super();

	}

	public MemberAttendanceDto(Long courseId, Long enrollId) {
		this.courseId = courseId;
		this.enrollId = enrollId;

	}

	private Long enrollId;
	private Long courseId;
	private Long sysId;
	private Long sessionNo;
	private Long attendId;
	private Long courseSessionId;
	private String attendance;
	private Date updateDate;
	private String sessionStatus;

	public Long getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(Object enrollId) {
		this.enrollId = (enrollId!=null ? NumberUtils.toLong(enrollId.toString()):null);
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Object courseId) {
		this.courseId = (courseId!=null ? NumberUtils.toLong(courseId.toString()):null);
	}

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Object sysId) {
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}

	public Long getSessionNo() {
		return sessionNo;
	}

	public void setSessionNo(Object sessionNo) {
		this.sessionNo = (sessionNo!=null ? NumberUtils.toLong(sessionNo.toString()):null);
	}

	public Long getAttendId() {
		return attendId;
	}

	public void setAttendId(Object attendId) {
		this.attendId = (attendId!=null ? NumberUtils.toLong(attendId.toString()):null);
	}

	public Long getCourseSessionId() {
		return courseSessionId;
	}

	public void setCourseSessionId(Object courseSessionId) {
		this.courseSessionId = (courseSessionId!=null ? NumberUtils.toLong(courseSessionId.toString()):null);
	}

	public String getAttendance() {
		return attendance;
	}

	public void setAttendance(String attendance) {
		this.attendance = attendance;
	}

	public Date getUpdateDate() {
	    return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
	    this.updateDate = updateDate;
	}

	public String getSessionStatus() {
	    return sessionStatus;
	}

	public void setSessionStatus(String sessionStatus) {
	    this.sessionStatus = sessionStatus;
	}
	
}
