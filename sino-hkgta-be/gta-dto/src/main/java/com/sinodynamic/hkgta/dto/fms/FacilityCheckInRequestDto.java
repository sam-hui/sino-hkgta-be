package com.sinodynamic.hkgta.dto.fms;

public class FacilityCheckInRequestDto {

	private Long resvId;
	
	private Long[] facilityNo;

	public long getResvId() {
		return resvId;
	}

	public void setResvId(long resvId) {
		this.resvId = resvId;
	}

	public Long[] getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long[] facilityNo) {
		this.facilityNo = facilityNo;
	}

	
}
