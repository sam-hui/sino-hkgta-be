package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class StaffListDto implements Serializable{
	
	public String sortBy;
	
	public Boolean isAscending;
	
	public String email;
	
	public String staffNo;
	
	public String staffType;
	public String status;
	public String positionTitle;
	
	public String employeeBeginDate;
	public String employeeEndDate;
	public String quitBeginDate;
	public String quitEndDate;
	public Integer pageSize = 10;
	public Integer pageNumber = 1;
	
	
	public String getSortBy()
	{
		return sortBy;
	}


	public void setSortBy(String sortBy)
	{
		this.sortBy = sortBy;
	}


	public Boolean getIsAscending()
	{
		return isAscending;
	}


	public void setIsAscending(Boolean isAscending)
	{
		this.isAscending = isAscending;
	}


	public String getEmail()
	{
		return email;
	}


	public void setEmail(String email)
	{
		this.email = email;
	}


	public String getStaffNo()
	{
		return staffNo;
	}


	public void setStaffNo(String staffNo)
	{
		this.staffNo = staffNo;
	}


	public String getStaffType()
	{
		return staffType;
	}


	public void setStaffType(String staffType)
	{
		this.staffType = staffType;
	}


	public String getStatus()
	{
		return status;
	}


	public void setStatus(String status)
	{
		this.status = status;
	}


	public String getPositionTitle()
	{
		return positionTitle;
	}


	public void setPositionTitle(String positionTitle)
	{
		this.positionTitle = positionTitle;
	}


	public String getEmployeeBeginDate()
	{
		return employeeBeginDate;
	}


	public void setEmployeeBeginDate(String employeeBeginDate)
	{
		this.employeeBeginDate = employeeBeginDate;
	}


	public String getEmployeeEndDate()
	{
		return employeeEndDate;
	}


	public void setEmployeeEndDate(String employeeEndDate)
	{
		this.employeeEndDate = employeeEndDate;
	}


	public String getQuitBeginDate()
	{
		return quitBeginDate;
	}


	public void setQuitBeginDate(String quitBeginDate)
	{
		this.quitBeginDate = quitBeginDate;
	}


	public String getQuitEndDate()
	{
		return quitEndDate;
	}


	public void setQuitEndDate(String quitEndDate)
	{
		this.quitEndDate = quitEndDate;
	}


	public Integer getPageSize()
	{
		return pageSize;
	}


	public void setPageSize(Integer pageSize)
	{
		this.pageSize = pageSize;
	}


	public Integer getPageNumber()
	{
		return pageNumber;
	}


	public void setPageNumber(Integer pageNumber)
	{
		this.pageNumber = pageNumber;
	}


	@Override
	public String toString() {
		return "StaffListDto [sortBy=" + sortBy + ", isAscending="
				+ isAscending + ", email=" + email + ", staffno=" + staffNo
				+ ", stafftype=" + staffType + ", status=" + status
				+ ", positiontitle=" + positionTitle + ", employeebegindate="
				+ employeeBeginDate + ", employeeenddate=" + employeeEndDate
				+ ", quitbegindate=" + quitBeginDate + ", quitenddate="
				+ quitEndDate + ", pageSize=" + pageSize + ", pageNumber="
				+ pageNumber + "]";
	}
}
