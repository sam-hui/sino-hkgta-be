package com.sinodynamic.hkgta.dto.crm;

import java.util.List;

public class FolderAndDetailsDto {

	     public Long getFolderId() {
		return folderId;
	}
	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

		public List<MaterialFolderDetailsDto> getMaterialList() {
		return materialList;
	}
	public void setMaterialList(List<MaterialFolderDetailsDto> materialList) {
		this.materialList = materialList;
	}

		private Long folderId;
	     private String folderName;
	     List<MaterialFolderDetailsDto> materialList;
	
}
