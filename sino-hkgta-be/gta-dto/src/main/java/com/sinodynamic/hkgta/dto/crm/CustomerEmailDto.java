package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class CustomerEmailDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long customerId;
	
	private Long number;
	
	private String emailType;
	
	private String to;
	
	private String subject;
	
	private String content;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	
	
}
