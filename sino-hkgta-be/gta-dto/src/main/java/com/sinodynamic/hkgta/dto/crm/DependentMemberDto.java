package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class DependentMemberDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long customerId;
	private String academyNo;
	private String memberName;
	private String memberType;
	private String status;
	private Date firstJoinDate;
	private Date expiryDate;
	private String planName;
	private String enrollStatus;
	private String portraitPhoto;
	private String servedTime;
	private Long planNo;
	private boolean dependentCreationRight;
	private String mobilePhone;
	private String contactEmail;
	private String relationship;

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getExpiryDate() {
		if (expiryDate == null) {
			return "";
		}
		return DtoHelper.getYMDDate(expiryDate);
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId =(customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}

	public String getAcademyNo() {
		return DtoHelper.nvl(academyNo);
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getMemberName() {
		return DtoHelper.nvl(memberName);
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberType() {
		return DtoHelper.nvl(memberType);
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getStatus() {
		return DtoHelper.nvl(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirstJoinDate() {
		if (firstJoinDate == null) {
			return "";
		}
		return DtoHelper.getYMDDateAndDateDiff(firstJoinDate);
	}

	public void setFirstJoinDate(Date firstJoinDate) {
		this.firstJoinDate = firstJoinDate;
	}

	public String getServedTime() {
		return servedTime;
	}

	public void setServedTime(String servedTime) {
		this.servedTime = servedTime;
	}

	public DependentMemberDto(Long customerId, String academyNo, String memberName, String memberType, String status, Date firstJoinDate) {
		super();
		this.customerId = customerId;
		this.academyNo = academyNo;
		this.memberName = memberName;
		this.memberType = memberType;
		this.status = status;
		this.firstJoinDate = firstJoinDate;
	}

	public DependentMemberDto() {

	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Object planNo) {
		if (planNo instanceof BigInteger) {
			this.planNo = ((BigInteger) planNo).longValue();
		} else if (planNo instanceof Integer) {
			this.planNo = ((Integer) planNo).longValue();
		} else if (planNo instanceof String) {
			this.planNo = Long.valueOf((String) planNo);
		} else {
			this.planNo = (Long) planNo;
		}
	}

	public boolean isDependentCreationRight() {
		return dependentCreationRight;
	}

	public void setDependentCreationRight(boolean dependentCreationRight) {
		this.dependentCreationRight = dependentCreationRight;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	

}
