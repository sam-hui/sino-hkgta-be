package com.sinodynamic.hkgta.dto.pos;

import org.apache.commons.lang.math.NumberUtils;



public class RestaurantCustomerBookingDto
{
	private Long resvId;
	
	private String restaurantId;
	
	private Long customerId;
	
	private String bookTime;
	
	private Long partySize;
	
	private String status;
	
	private String bookVia;
	
	private String remark;
	
	private String memberName;
	
	private String academyNo;

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Object resvId) {
		this.resvId = (resvId!=null ? NumberUtils.toLong(resvId.toString()):null);
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getBookTime() {
		return bookTime;
	}

	public void setBookTime(String bookTime) {
		this.bookTime = bookTime;
	}

	public Long getPartySize() {
		return partySize;
	}

	public void setPartySize(Object partySize) {
		this.partySize = (partySize!=null ? NumberUtils.toLong(partySize.toString()):null);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookVia() {
		return bookVia;
	}

	public void setBookVia(String bookVia) {
		this.bookVia = bookVia;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}
	
	

}
