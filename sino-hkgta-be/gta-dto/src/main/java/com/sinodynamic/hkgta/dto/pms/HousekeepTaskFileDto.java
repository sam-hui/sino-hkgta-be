package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class HousekeepTaskFileDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long fileId;

	private String serverFile;

	private String fileType;

	

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getServerFile() {
		return serverFile;
	}

	public void setServerFile(String serverFile) {
		this.serverFile = serverFile;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
