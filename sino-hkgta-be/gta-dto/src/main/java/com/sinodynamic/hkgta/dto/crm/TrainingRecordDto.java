/**
 * Project Name:gta-dto
 * File Name:TrainningItem.java
 * Package Name:com.sinodynamic.hkgta.dto.crm
 * Create Date:Jul 9, 2015 10:58:19 AM
 *
 */

package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sinodynamic.hkgta.dto.DtoHelper;

/**
 * TrainningRecordDto <br/>
 * Function: Dislpay the training record 
 * sessions. <br/>
 * Create Date: Jul 9, 2015 10:58:19 AM <br/>
 * 
 * @author Vian Tang
 * @version
 */
public class TrainingRecordDto implements Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long facilityTimeslotId;// for coaching

    private Long customerId;// for course session

    private Date attendTime;
    
    private String attendTimeApp;
    
    private String coachName;
    
	private String attendTimeToConvert;
	
	private String comment;
	
	private BigDecimal score;

	private String beginBookTime;
	
	private String endBookTime;
	
	private Date trainerCommentDate;
	
	private String trainerCommentDateToConvert;
	
	private String assignment;
	
	private String trainerImagePath;
	
	private String facilityType;
	
	
	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getAttendTimeApp() {
		if(null != attendTime)
			this.attendTimeApp = DtoHelper.getYMDDateAndDateDiff(attendTime);
		return this.attendTimeApp;
	}

	public void setAttendTimeApp(String attendTimeApp) {
		this.attendTimeApp = attendTimeApp;
	}

	public String getTrainerImagePath() {
		return trainerImagePath;
	}

	public void setTrainerImagePath(String trainerImagePath) {
		this.trainerImagePath = trainerImagePath;
	}

	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	public Date getTrainerCommentDate() {
		return trainerCommentDate;
	}

	public void setTrainerCommentDate(Date trainerCommentDate) {
		this.trainerCommentDate = trainerCommentDate;
	}

	public String getTrainerCommentDateToConvert() {
		if (null != trainerCommentDate) {
			trainerCommentDateToConvert = DtoHelper.getYMMDDateAndDateDiff(trainerCommentDate);
		}
		return trainerCommentDateToConvert;
	}

	public void setTrainerCommentDateToConvert(String trainerCommentDateToConvert) {
		this.trainerCommentDateToConvert = trainerCommentDateToConvert;
	}

	public String getBeginBookTime() {
		return beginBookTime;
	}

	public void setBeginBookTime(String beginBookTime) {
		this.beginBookTime = beginBookTime;
	}

	public String getEndBookTime() {
		return endBookTime;
	}

	public void setEndBookTime(String endBookTime) {
		this.endBookTime = endBookTime;
	}

	public String getAttendTimeToConvert() throws Exception {
		if (null != this.beginBookTime && null != this.endBookTime){
			attendTimeToConvert = DtoHelper.getYMDDatePeriodAndDateDiff(beginBookTime, endBookTime);
		}
		return attendTimeToConvert;
	}

	public void setAttendTimeToConvert(String attendTimeToConvert) {
		this.attendTimeToConvert = attendTimeToConvert;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public Long getFacilityTimeslotId()
	{
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Object facilityTimeslotId)
	{
		if (facilityTimeslotId instanceof BigInteger) {
			this.facilityTimeslotId = ((BigInteger) facilityTimeslotId).longValue();
		}
		else if (facilityTimeslotId instanceof Integer) {
			this.facilityTimeslotId = ((Integer) facilityTimeslotId).longValue();
		}
		else if (facilityTimeslotId instanceof String) {
			this.facilityTimeslotId = Long.parseLong((String) facilityTimeslotId);
		}
		else {
			this.facilityTimeslotId = (Long) facilityTimeslotId;
		}
		
	}

	public Long getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(Object customerId)
	{
		if (customerId instanceof BigInteger) {
			this.customerId = ((BigInteger) customerId).longValue();
		}
		else if (customerId instanceof Integer) {
			this.customerId = ((Integer) customerId).longValue();
		}
		else if (customerId instanceof String) {
			this.customerId = Long.parseLong((String) customerId);
		}
		else {
			this.customerId = (Long) customerId;
		}
	}

	public Date getAttendTime()
	{
		return attendTime;
	}

	public void setAttendTime(Date attendTime)
	{
		this.attendTime = attendTime;
	}

	public String getCoachName()
	{
		return coachName;
	}

	public void setCoachName(String coachName)
	{
		this.coachName = coachName;
	}

}
