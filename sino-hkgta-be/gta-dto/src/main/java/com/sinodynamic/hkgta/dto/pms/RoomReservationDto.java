package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;
/**
 * This Dto is for PMS system
 * */
public class RoomReservationDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private BigDecimal amountAfterTax;
	private String reservationId;
	private String roomTypeCode;
	private String roomTypeName;
	private String startDate;
	private String endDate;
	private Long noOfStayingNight;
	
	private Long customerId;
	private Date requestDate;
	
	private Date createDate;
    private String createBy;
    private Date updateDate;
    private String updateBy;
    
    private String confirmId;
	private String academyID;
    private String patronName;
    private String roomId;
    private String rateCode;
    private String status;
    
    private String arrivalDate;
    private String departureDate;
    private String paymentMethod;
    private Long totalCharge;
    

	public Long getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(Long totalCharge) {
		this.totalCharge = totalCharge;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getConfirmId() {
		return confirmId;
	}

	public void setConfirmId(String confirmId) {
		this.confirmId = confirmId;
	}

	public String getAcademyID() {
		return academyID;
	}

	public void setAcademyID(String academyID) {
		this.academyID = academyID;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		if(null!=arrivalDate){
			Date date = DtoHelper.parseString2Date(arrivalDate, "yyyy-MM-dd");
			this.arrivalDate = DtoHelper.date2String(date, "yyyy-MMM-dd")  + DtoHelper.getNoOfDays(date);
		}
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		if(null!=departureDate){
			Date date = DtoHelper.parseString2Date(departureDate, "yyyy-MM-dd");
			this.departureDate = DtoHelper.date2String(date, "yyyy-MMM-dd") + DtoHelper.getNoOfDays(date);
		}
	}

    
	public BigDecimal getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(BigDecimal amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	

	public Long getNoOfStayingNight() {
		return noOfStayingNight;
	}

	public void setNoOfStayingNight(Long noOfStayingNight) {
		this.noOfStayingNight = noOfStayingNight;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	
}
