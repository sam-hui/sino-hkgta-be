package com.sinodynamic.hkgta.dto.crm;

import java.math.BigInteger;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.dto.DtoHelper;


public class LeadListResponseDto {
	private BigInteger customerId;
    private String lead;
    private String status;
    private String salesPerson;
    private String contactEmail;
    private Date createDate;
    private boolean unRead = false; 
    private String creationDate;
	private String companyName;
	private String contactClassCode;
	private Date  effectiveDate;
	
	@JsonIgnore
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreationDate() {
		if(this.createDate != null){
			return DtoHelper.getYMDDateAndDateDiff(createDate);
		}
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public String getLead() {
		return DtoHelper.nvl(lead);
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

	public String getStatus() {
		return DtoHelper.nvl(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSalesPerson() {
		return DtoHelper.nvl(salesPerson);
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public boolean getUnRead() {
		return unRead;
	}

	public void setUnRead(boolean unRead) {
		this.unRead = unRead;
	}

	public String getContactEmail() {
		return DtoHelper.nvl(contactEmail);
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCompanyName() {
		return DtoHelper.nvl(companyName);
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactClassCode() {
		return DtoHelper.nvl(contactClassCode);
	}

	public void setContactClassCode(String contactClassCode) {
		this.contactClassCode = contactClassCode;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	
}
