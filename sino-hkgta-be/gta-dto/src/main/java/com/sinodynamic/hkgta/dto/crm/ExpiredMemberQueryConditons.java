package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class ExpiredMemberQueryConditons implements Serializable{

	/**
	 * SearchTypes has 2 options: 1 is searching for expiring members 30/60/90 days
	 * 2 is searching for expired member
	 */
	private String searchType;
	
	/**
	 * when SearchTypes equals "1" expiringDays has 3 options:30,60,90
	 * 
	 * when SearchTypes equals "2" expiringDays is null;
	 */
	private Integer expiringDays;
	
	
	private String sortBy;
	
	private String searchCondtion;
	
	/*
	 * OrderType has 2 option:ASC ,DESC
	 */
	private String sortType = "";
	
	private Integer pageSize = 10;
	
	private Integer pageNumber = 1;
	
	

	public ExpiredMemberQueryConditons() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public Integer getExpiringDays() {
		return expiringDays;
	}

	public void setExpiringDays(Integer expiringDays) {
		this.expiringDays = expiringDays;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	

	public String getSearchCondtion() {
		return searchCondtion;
	}

	public void setSearchCondtion(String searchCondtion) {
		this.searchCondtion = searchCondtion;
	}

	@Override
	public String toString() {
		return "ExpiredMemberQueryConditons [searchType=" + searchType
				+ ", expiringDays=" + expiringDays + ", sortBy=" + sortBy
				+ ", searchCondtion=" + searchCondtion + ", sortType="
				+ sortType + ", pageSize=" + pageSize + ", pageNumber="
				+ pageNumber + "]";
	}
}
