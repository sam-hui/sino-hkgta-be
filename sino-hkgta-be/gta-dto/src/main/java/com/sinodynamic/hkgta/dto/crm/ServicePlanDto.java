package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityTypeDto;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;

public class ServicePlanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	Long planNo;
	String planName;
	String passNatureCode;
	Long contractLengthMonth;
	String passPeriodType;
	String effectiveStartDate;
	String effectiveEndDate;
	String status;
	Date createDate;
	String createBy;
	Date updateDate;
	String updateBy;
	AgeRange ageRange;
	PosServiceItemPrice posServiceItemPrice;
	private List<FacilityTypeDto> facilityTypes = new ArrayList<FacilityTypeDto>();
	private List<ServicePlanRightDto> servicePlanRights = new ArrayList<ServicePlanRightDto>();
	private List<ServicePlanPosDto> servicePlanPosDtos = new ArrayList<ServicePlanPosDto>();
	
	//used by day pass begin
	//refer to Purchased By in the page
	private String subscriberType;
	//refer to checkbox Sunday-Saturday in the page
	private String weeklyRepeatBit;
	private BigDecimal lowRate;
	private BigDecimal highRate;
	private List<String> specialDate;
	
	public List<FacilityTypeDto> getFacilityTypes() {
		return facilityTypes;
	}
	public void setFacilityTypes(List<FacilityTypeDto> facilityTypes) {
		this.facilityTypes = facilityTypes;
	}
	public List<ServicePlanRightDto> getServicePlanRights() {
		return servicePlanRights;
	}
	public void setServicePlanRights(List<ServicePlanRightDto> servicePlanRights) {
		this.servicePlanRights = servicePlanRights;
	}
	public List<ServicePlanPosDto> getServicePlanPosDtos() {
		return servicePlanPosDtos;
	}
	public void setServicePlanPosDtos(List<ServicePlanPosDto> servicePlanPosDtos) {
		this.servicePlanPosDtos = servicePlanPosDtos;
	}
	public List<String> getSpecialDate() {
		return specialDate;
	}
	public void setSpecialDate(List<String> specialDate) {
		this.specialDate = specialDate;
	}
	public String getSubscriberType() {
		return subscriberType;
	}
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}
	public String getWeeklyRepeatBit() {
		return weeklyRepeatBit;
	}
	public void setWeeklyRepeatBit(String weeklyRepeatBit) {
		this.weeklyRepeatBit = weeklyRepeatBit;
	}

	public BigDecimal getLowRate() {
		return lowRate;
	}
	public void setLowRate(BigDecimal lowRate) {
		this.lowRate = lowRate;
	}
	public BigDecimal getHighRate() {
		return highRate;
	}
	public void setHighRate(BigDecimal highRate) {
		this.highRate = highRate;
	}
	public Long getPlanNo() {
		return planNo;
	}
	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}
	public Long getContractLengthMonth() {
		return contractLengthMonth;
	}
	public void setContractLengthMonth(Long contractLengthMonth) {
		this.contractLengthMonth = contractLengthMonth;
	}
	public AgeRange getAgeRange() {
		return ageRange;
	}
	public void setAgeRange(AgeRange ageRange) {
		this.ageRange = ageRange;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPassNatureCode() {
		return passNatureCode;
	}
	public void setPassNatureCode(String passNatureCode) {
		this.passNatureCode = passNatureCode;
	}
	public String getPassPeriodType() {
		return passPeriodType;
	}
	public void setPassPeriodType(String passPeriodType) {
		this.passPeriodType = passPeriodType;
	}
	
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public PosServiceItemPrice getPosServiceItemPrice() {
		return posServiceItemPrice;
	}
	public void setPosServiceItemPrice(PosServiceItemPrice posServiceItemPrice) {
		this.posServiceItemPrice = posServiceItemPrice;
	}
}
