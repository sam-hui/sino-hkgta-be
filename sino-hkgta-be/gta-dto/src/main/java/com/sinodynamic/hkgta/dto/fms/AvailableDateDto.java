package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class AvailableDateDto implements Serializable{
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coachId == null) ? 0 : coachId.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvailableDateDto other = (AvailableDateDto) obj;
		if (coachId == null) {
			if (other.coachId != null)
				return false;
		} else if (!coachId.equals(other.coachId))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -8070390197496562150L;
	private String coachId;
	private String date;
	public String getCoachId() {
		return coachId;
	}
	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
