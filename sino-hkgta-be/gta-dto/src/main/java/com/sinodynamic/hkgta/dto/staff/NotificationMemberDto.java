package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


public class NotificationMemberDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String customerId;
	
	private String customerName;
	
	private String gender;
	
	private Long age;
	
	private String email;
	
	private String phoneMobile;
	
	private Long planNo;
	
	private String memberType;
	
	private String status;
	
	private Date expiryDate;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if (customerId instanceof BigInteger) {
			this.customerId = ((BigInteger)customerId).toString();
		}
		else if (customerId instanceof Integer) {
			this.customerId = ((Integer)customerId).toString();
		}
		else if (customerId instanceof Long) {
			this.customerId = ((Long)customerId).toString();
		}
		else {
			this.customerId = (String)customerId;
		}
		
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(Object gender) {
		if(gender instanceof Character){
			this.gender = String.valueOf(gender);
		}
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Object age) {
		if (age instanceof BigInteger) {
			this.age = ((BigInteger)age).longValue();
		}
		else if (age instanceof Long) {
			this.age = (Long)age;
		}
		else if (age instanceof String) {
			this.age = Long.parseLong((String)age);
		}
		else {
			this.age = (Long)age;
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Object planNo) {
		if (planNo instanceof BigInteger) {
			this.planNo = ((BigInteger)planNo).longValue();
		}
		else if (planNo instanceof String) {
			this.planNo = Long.parseLong((String)planNo);
		}
		else {
			this.planNo = (Long)planNo;
		}
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	

}
