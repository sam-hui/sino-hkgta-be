package com.sinodynamic.hkgta.dto.statement;

import java.io.Serializable;

public class StatementParamsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String year;
	private String month;
	private boolean isAll;
	private Long[] customerIds;
	private boolean isSent;
	private String userId;
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public boolean getIsAll() {
		return isAll;
	}
	public void setIsAll(boolean isAll) {
		this.isAll = isAll;
	}
	public Long[] getCustomerIds() {
		return customerIds;
	}
	public void setCustomerIds(Long[] customerIds) {
		this.customerIds = customerIds;
	}
	public boolean isSent() {
		return isSent;
	}
	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}
	public void setAll(boolean isAll) {
		this.isAll = isAll;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
