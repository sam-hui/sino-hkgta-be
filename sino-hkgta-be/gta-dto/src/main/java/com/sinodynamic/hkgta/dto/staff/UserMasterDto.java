package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;


public class UserMasterDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;
	
	private String createBy;
	
	private String defaultModule;
	
	private String loginId;
	
	private String nickname;
	
	private String password;
	
	private String status;
	
	private String userType;
	
	private String updateBy;
	
	private boolean hasProfile;
	
	private boolean isSuperAdmin;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public String getDefaultModule()
	{
		return defaultModule;
	}

	public void setDefaultModule(String defaultModule)
	{
		this.defaultModule = defaultModule;
	}

	public String getLoginId()
	{
		return loginId;
	}

	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getUserType()
	{
		return userType;
	}

	public void setUserType(String userType)
	{
		this.userType = userType;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public boolean isHasProfile()
	{
		return hasProfile;
	}

	public void setHasProfile(boolean hasProfile)
	{
		this.hasProfile = hasProfile;
	}

	public boolean isSuperAdmin()
	{
		return isSuperAdmin;
	}

	public void setSuperAdmin(boolean isSuperAdmin)
	{
		this.isSuperAdmin = isSuperAdmin;
	}
	
}