package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RoomMemberDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7936492387046859018L;
	private Long roomId;
	private String roomNo;
	private String status;
	private String serviceStatus;
	private String frontdeskStatus;
	private List<RoomTaskDto> tasks;

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RoomTaskDto> getTasks() {
		return tasks;
	}

	public void setTasks(List<RoomTaskDto> tasks) {
		this.tasks = tasks;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getFrontdeskStatus()
	{
		return frontdeskStatus;
	}

	public void setFrontdeskStatus(String frontdeskStatus)
	{
		this.frontdeskStatus = frontdeskStatus;
	}
}
