package com.sinodynamic.hkgta.dto.fms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.entity.fms.FacilityMaster;

public class FacilityCheckInDto<T> {

	private Date beginDate;
	
	private Long count;
	
	private Long checkedInCount;
	
	private List<FacilityMasterDto> checkedInFacilities;
	
	private T availabilities;

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getCheckedInCount() {
		return checkedInCount;
	}

	public void setCheckedInCount(Long checkedInCount) {
		this.checkedInCount = checkedInCount;
	}

	public T getAvailabilities() {
		return availabilities;
	}

	public void setAvailabilities(T availabilities) {
		this.availabilities = availabilities;
	}

	public List<FacilityMasterDto> getCheckedInFacilities() {
		return checkedInFacilities;
	}

	public void setCheckedInFacilities(List<FacilityMasterDto> checkedInFacilities) {
		this.checkedInFacilities = checkedInFacilities;
	}

}