package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class CustomerAddressDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String customerId;

	private String addressType;

	private String address;

	private String hkDistrict;

	public CustomerAddressDto() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getHkDistrict() {
		return this.hkDistrict;
	}

	public void setHkDistrict(String hkDistrict) {
		this.hkDistrict = hkDistrict;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

}