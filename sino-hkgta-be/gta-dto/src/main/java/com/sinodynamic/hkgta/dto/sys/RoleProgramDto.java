package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class RoleProgramDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8248970296393034828L;
	private Long roleId;
	private String programIds;
	private String accessRight;
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Object roleId) {
		this.roleId = (roleId!=null ? NumberUtils.toLong(roleId.toString()):null);
	}
	public String getProgramIds() {
		return programIds;
	}
	public void setProgramIds(String programIds) {
		this.programIds = programIds;
	}
	public String getAccessRight() {
		return accessRight;
	}
	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}

}
