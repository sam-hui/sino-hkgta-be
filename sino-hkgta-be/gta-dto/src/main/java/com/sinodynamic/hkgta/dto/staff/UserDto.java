package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;


public class UserDto implements Serializable{

	private static final long serialVersionUID = 7000030754674311099L;
	
	private String randomPsw;

	private UserMasterDto user;
	
	private StaffMasterDto staffMaster;
	
	private StaffProfileDto staffProfile;
	
	private StaffCoachInfoDto staffCoachInfo;

	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserMasterDto getUser()
	{
		return user;
	}

	public void setUser(UserMasterDto user)
	{
		this.user = user;
	}

	public StaffMasterDto getStaffMaster()
	{
		return staffMaster;
	}

	public void setStaffMaster(StaffMasterDto staffMaster)
	{
		this.staffMaster = staffMaster;
	}

	public StaffProfileDto getStaffProfile()
	{
		return staffProfile;
	}

	public void setStaffProfile(StaffProfileDto staffProfile)
	{
		this.staffProfile = staffProfile;
	}

	public StaffCoachInfoDto getStaffCoachInfo()
	{
		return staffCoachInfo;
	}

	public void setStaffCoachInfo(StaffCoachInfoDto staffCoachInfo)
	{
		this.staffCoachInfo = staffCoachInfo;
	}

	public String getRandomPsw()
	{
		return randomPsw;
	}

	public void setRandomPsw(String randomPsw)
	{
		this.randomPsw = randomPsw;
	}
	
	

}
