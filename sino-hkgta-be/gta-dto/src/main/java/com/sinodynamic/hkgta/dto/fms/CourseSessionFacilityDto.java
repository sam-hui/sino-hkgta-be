package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class CourseSessionFacilityDto implements Serializable{

	private Long sysId;
	private Long facilityTimeslotId;
	public Long getSysId() {
		return sysId;
	}
	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}
	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}
	public void setFacilityTimeslotId(Object facilityTimeslotId) {
		this.facilityTimeslotId = (facilityTimeslotId!=null ? NumberUtils.toLong(facilityTimeslotId.toString()):null);
	}
	
}
