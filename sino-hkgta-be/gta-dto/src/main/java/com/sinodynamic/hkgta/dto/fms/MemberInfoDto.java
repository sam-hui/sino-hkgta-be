package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class MemberInfoDto implements Serializable {

	private static final long serialVersionUID = -1371912768347486929L;

	public MemberInfoDto() {
		super();

	}

	public MemberInfoDto(Long courseId, String status) {
		this.courseId = courseId;
		this.status = status;

	}

	private Long enrollId;
	private Long courseId;
	private Date enrollDate;
	private Long customerId;
	private String memberId;
	private String memberName;
	private String status;
	private Date createDate;
	private String courseName;
	private Long capacity;
	private Long enrollmentQuota;
	private String emailAddress;
	private List<MemberAttendanceDto> attendance;
	private String memberAcceptance;
	private String portraitPhoto;
	
	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Object courseId) {
		this.courseId = NumberUtils.toLong(courseId.toString());
	}

	public String getEnrollDate() {
		return DtoHelper.getYMDDateTimeAndDateDiff(enrollDate);
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = NumberUtils.toLong(customerId.toString());
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getCreateDate() {
		return DtoHelper.getYMDDateAndDateDiff(createDate);
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Object capacity) {
		this.capacity = NumberUtils.toLong(capacity.toString());
	}

	public Long getEnrollmentQuota() {
		return enrollmentQuota;
	}

	public void setEnrollmentQuota(Object enrollmentQuota) {
		this.enrollmentQuota = NumberUtils.toLong(enrollmentQuota.toString());
	}
	
	public String getEmailAddress() {
	    return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
	    this.emailAddress = emailAddress;
	}

	public List<MemberAttendanceDto> getAttendance() {
		return attendance;
	}

	public void setAttendance(List<MemberAttendanceDto> attendance) {
		this.attendance = attendance;
	}

	public Long getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(Object enrollId) {
		this.enrollId = NumberUtils.toLong(enrollId.toString());
	}

	public String getMemberAcceptance() {
		return memberAcceptance;
	}

	public void setMemberAcceptance(String memberAcceptance) {
		this.memberAcceptance = memberAcceptance;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	
}
