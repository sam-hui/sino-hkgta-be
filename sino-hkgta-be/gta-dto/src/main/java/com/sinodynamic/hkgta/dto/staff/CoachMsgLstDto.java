/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.util.List;

/**
 * @author Tony_Dong
 *
 */
public class CoachMsgLstDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5920057796869591935L;
	
	private List<CoachMsgDto> coachMsgLst;

	/**
	 * @return the coachMsgLst
	 */
	public List<CoachMsgDto> getCoachMsgLst() {
		return coachMsgLst;
	}

	/**
	 * @param coachMsgLst the coachMsgLst to set
	 */
	public void setCoachMsgLst(List<CoachMsgDto> coachMsgLst) {
		this.coachMsgLst = coachMsgLst;
	}
	
	

}
