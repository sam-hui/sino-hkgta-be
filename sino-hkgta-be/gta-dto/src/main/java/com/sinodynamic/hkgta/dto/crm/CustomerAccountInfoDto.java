package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class CustomerAccountInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String planName;
	
	private String memberName;
	
	private String academyNo;

	private Date effectiveDate;

	private Date expiryDate;

	private Long contractLength;

	private BigDecimal orderTotalAmount;

	private String serviceAccount;

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Long getContractLength() {
		return contractLength;
	}

	public void setContractLength(Object contractLength) {
		this.contractLength =(contractLength!=null ? NumberUtils.toLong(contractLength.toString()):null);
	}

	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}

	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	public String getServiceAccount() {
		return serviceAccount;
	}

	public void setServiceAccount(String serviceAccount) {
		this.serviceAccount = serviceAccount;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

}
