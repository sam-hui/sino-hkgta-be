package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.util.List;

public class SpaAppointmentDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long customerId;
	private String memberName;
	private String accademyNo;
	private String invoiceNo;
	
	private String paymentMethod;
	
	private List<SpaAppointmentDetailDto> serviceDetails;

	
	
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAccademyNo() {
		return accademyNo;
	}

	public void setAccademyNo(String accademyNo) {
		this.accademyNo = accademyNo;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public List<SpaAppointmentDetailDto> getServiceDetails() {
		return serviceDetails;
	}

	public void setServiceDetails(List<SpaAppointmentDetailDto> serviceDetails) {
		this.serviceDetails = serviceDetails;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	

}
