package com.sinodynamic.hkgta.integration.spa.service;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sinodynamic.hkgta.integration.spa.request.AddGuestRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentListRequest;
import com.sinodynamic.hkgta.integration.spa.request.AvailableRoomsRequest;
import com.sinodynamic.hkgta.integration.spa.request.BaseRequest;
import com.sinodynamic.hkgta.integration.spa.request.BookAppointmentRequest;
import com.sinodynamic.hkgta.integration.spa.request.CCPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.request.CashPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAppointmentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetCenterTherapistsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetServiceDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceDetails;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceDetailsList;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceRevenueRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceVoidRequest;
import com.sinodynamic.hkgta.integration.spa.request.PaymentByCustomRequest;
import com.sinodynamic.hkgta.integration.spa.request.QueryStatePKRequest;
import com.sinodynamic.hkgta.integration.spa.request.ServicesListByCategoryCodeRequest;
import com.sinodynamic.hkgta.integration.spa.request.SubCategoriesListRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistsAvailableForServiceRequest;
import com.sinodynamic.hkgta.integration.spa.request.UpdateGuestRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentServiceCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AvailableRoomsResponse;
import com.sinodynamic.hkgta.integration.spa.response.BaseResponse;
import com.sinodynamic.hkgta.integration.spa.response.BookAppointmentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CCPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CashPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CountryListResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAppointmentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTimeSlotsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetInvoiceRevenueResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetSubcategoriesResponse;
import com.sinodynamic.hkgta.integration.spa.response.GuestAppointmentsListResponse;
import com.sinodynamic.hkgta.integration.spa.response.InvoiceVoidResponse;
import com.sinodynamic.hkgta.integration.spa.response.PaymentByCustomResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCategoryResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCenterTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryServiceListResponse;
import com.sinodynamic.hkgta.integration.spa.response.RegistGuestResponse;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.SetInvoiceDetails;
import com.sinodynamic.hkgta.integration.spa.response.StatePKResponse;
import com.sinodynamic.hkgta.integration.spa.response.UpdateGuestResponse;
import com.sinodynamic.hkgta.integration.util.BeanValidate;
import com.sinodynamic.hkgta.integration.util.MMSAPIException;
import com.sinodynamic.hkgta.integration.util.SetInvoiceDetailsDeserializer;
import com.sinodynamic.hkgta.integration.util.UrlParameterAssemble;

@Service
public class SpaServcieManagerImpl  implements SpaServcieManager {
	
	final Logger logger = LoggerFactory.getLogger(SpaServcieManagerImpl.class); 
	
	@Value(value ="#{spaProperties['guestAPI.url']}")
	private String guestAPIUrl;
	
	@Value(value ="#{spaProperties['posAPI.url']}")
	private String posAPIUrl;
	
	@Value(value ="#{spaProperties['categoryAPI.url']}")
	private String categoryAPIUrl;
	
	@Value(value ="#{spaProperties['countryListAPI.url']}")
	private String countryListAPIUrl;
	
	@Value(value ="#{spaProperties['statePKAPI.url']}")
	private String statePKAPIUrl;
	
	@Value(value ="#{spaProperties['appointmentAPI.url']}")
	private String appointmentAPIUrl;
	
	@Value(value ="#{spaProperties['paymentAPI.url']}")
	private String paymentAPIUrl;
	
	@Value(value ="#{spaProperties['revenueAPI.url']}")
	private String revenueAPI;
	
	@Value(value ="#{spaProperties['collectionAPI.url']}")
	private String collectionAPI;
	
	@Value(value ="#{spaProperties['userName']}")
	private String userName;
	
	@Value(value ="#{spaProperties['organizationId']}")
	private String organizationId;
	
	@Value(value ="#{spaProperties['centerId']}")
	private String centerId;
	
	@Value(value ="#{spaProperties['centerCode']}")
	private String centerCode;

	@Value(value ="#{spaProperties['appVersion']}")
	private String appVersion;
	
	@Value(value ="#{spaProperties['userPassword']}")
	private String userPassword;
	
	@Value(value ="#{spaProperties['accountName']}")
	private String accountName;
	
	@Autowired
	private RestTemplate  restTemplate;
	
	private  String postMMS(String apiUrl,Map<String,String> params) throws ResourceAccessException {
		initMMSRequestParams(params);
		MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
		for(String key: params.keySet()) {
			requestEntity.add(key, params.get(key));
		}
		return restTemplate.postForObject(apiUrl, requestEntity,String.class);

	}
	
	private  String postMMS2(String apiUrl,Map<String,String> params) throws ResourceAccessException{
		
		params.put("userName", userName);
		params.put("userPassword", userPassword);
		params.put("accountName", accountName);
		params.put("centerId", centerId);
		params.put("appVersion", appVersion);
		
		MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
		for(String key: params.keySet()) {
			requestEntity.add(key, params.get(key));
		}
		return restTemplate.postForObject(apiUrl, requestEntity,String.class);

	}
	
	
	private void initMMSRequestParams(Map<String, String> params) {
		params.put("userName", userName);
		params.put("userPassword", userPassword);
		params.put("accountName", accountName);
		//params.put("organizationId", organizationId);
		//params.put("centerId", centerId);
		params.put("centerCode", centerCode);
		params.put("appVersion", appVersion);
	}
	
	
	private <T extends BaseResponse> BaseResponse execute(String apiUrl,String methodName,BaseRequest request,Class<T> responseClazz){
		try {
			Map<String, String> params = new HashMap<String,String>();
			if(null != request) {
				BeanValidate.validateFields(request, request.getClass());
				BeanValidate.validateObject(request, request.getClass());
				params = UrlParameterAssemble.assembleQueryMap(request, request.getClass());
			}
			BaseResponse response = null;
			if(StringUtils.isNotBlank(methodName)) {
				params.put("methodName", methodName);
			}
			String json = postMMS(apiUrl,params);
			ObjectMapper mapper = new ObjectMapper();  
			response = mapper.readValue(json, responseClazz);
			return response;
		} catch(ResourceAccessException e) {
			throw e;
		} catch (Exception e) {
			throw new MMSAPIException(e);
		}
	}
	 
	
	@Override
	public RegistGuestResponse registGuest(AddGuestRequest guest){
		return (RegistGuestResponse)execute(guestAPIUrl,"guestRegister",guest,RegistGuestResponse.class);
	}

	@Override
	public UpdateGuestResponse updateGuest(UpdateGuestRequest guest) {
		return (UpdateGuestResponse)execute(guestAPIUrl,"setGuestProperties",guest,UpdateGuestResponse.class);
	}
	
	@Override
	public QueryCategoryResponse getCategoriesList() {
		return (QueryCategoryResponse)execute(categoryAPIUrl,"categoriesList",null,QueryCategoryResponse.class);
	}

	@Override
	public QueryServiceListResponse getServicesListByCategoryCode(ServicesListByCategoryCodeRequest request){
		return (QueryServiceListResponse)execute(categoryAPIUrl,"servicesList",request,QueryServiceListResponse.class);
	}

	@Override
	public ServiceDetailsResponse queryServiceDetails(GetServiceDetailsRequest request){
		return (ServiceDetailsResponse)execute(categoryAPIUrl,"serviceDetails",request,ServiceDetailsResponse.class);
	}

	@Override
	public CountryListResponse queryCountryList() {
		return (CountryListResponse)execute(countryListAPIUrl,null,null,CountryListResponse.class);
	}

	@Override
	public StatePKResponse queryStatePK(QueryStatePKRequest request){
		return (StatePKResponse)execute(statePKAPIUrl,null,null,StatePKResponse.class);
	}

	@Override
	public AvailableRoomsResponse queryAvailableRooms(AvailableRoomsRequest request) {
		return (AvailableRoomsResponse)execute(appointmentAPIUrl,"roomsAvailable",request,AvailableRoomsResponse.class);
	}

	@Override
	public QueryCenterTherapistsResponse getCenterTherapists(GetCenterTherapistsRequest request) {
		return (QueryCenterTherapistsResponse)execute(appointmentAPIUrl,"therapistsInCenter",request,QueryCenterTherapistsResponse.class);
	}

	@Override
	public BookAppointmentResponse bookAppointment(BookAppointmentRequest request){
		return (BookAppointmentResponse)execute(appointmentAPIUrl,"appointmentBook",request,BookAppointmentResponse.class);
	}

	@Override
	public GetAvailableTimeSlotsResponse getAvailableTimeSlots(GetAvailableTimeSlotsRequest request) {
		return (GetAvailableTimeSlotsResponse)execute(appointmentAPIUrl,"timeslotsAvailable",request,GetAvailableTimeSlotsResponse.class);
	}

	@Override
	public CashPaymentResponse cashPayment(CashPaymentRequest request) {
		return (CashPaymentResponse)execute(paymentAPIUrl,"paymentByCash",request,CashPaymentResponse.class);
	}

	@Override
	public CCPaymentResponse creditCardPayment(CCPaymentRequest request) {
		return (CCPaymentResponse)execute(paymentAPIUrl,"paymentByCC",request,CCPaymentResponse.class);
	}

	@Override
	public GetSubcategoriesResponse getSubCategoriesListByCode(SubCategoriesListRequest request) {
		return (GetSubcategoriesResponse)execute(categoryAPIUrl,"subcategoriesList",request,GetSubcategoriesResponse.class);
	}

	@Override
	public GetAvailableTherapistsResponse therapistsAvailableForService(TherapistsAvailableForServiceRequest request) {
		return (GetAvailableTherapistsResponse)execute(appointmentAPIUrl,"therapistsAvailableForService",request,GetAvailableTherapistsResponse.class);
	}

	@Override
	public InvoiceVoidResponse invoiceVoid(InvoiceVoidRequest request) {
		return (InvoiceVoidResponse)execute(appointmentAPIUrl,"invoiceVoid",request,InvoiceVoidResponse.class);
	}

	@Override
	public GuestAppointmentsListResponse guestAppointmentsList(AppointmentListRequest request){
		return (GuestAppointmentsListResponse)execute(guestAPIUrl,"guestAppointmentsList",request,GuestAppointmentsListResponse.class);
	}

	@Override
	public AppointmentCancelResponse appointmentCancel(InvoiceCancelRequest request) {
		return (AppointmentCancelResponse)execute(appointmentAPIUrl,"appointmentCancel",request,AppointmentCancelResponse.class);
	}

	@Override
	public AppointmentDetailsResponse appointmentDetails(AppointmentDetailsRequest request){
		return (AppointmentDetailsResponse)execute(appointmentAPIUrl,"appointmentDetails",request,AppointmentDetailsResponse.class);
	}

	@Override
	public GetInvoiceRevenueResponse getInvoiceRevenue(InvoiceRevenueRequest request){
		return (GetInvoiceRevenueResponse)execute(revenueAPI,"getInvoiceRevenue",request,GetInvoiceRevenueResponse.class);
	}

	@Override
	public GetAppointmentsResponse getAppointments(GetAppointmentsRequest request) {
		return (GetAppointmentsResponse)execute(appointmentAPIUrl,"getAppointments",request,GetAppointmentsResponse.class);
	}

	@Override
	public PaymentByCustomResponse paymentByCustom(PaymentByCustomRequest request) {
		return (PaymentByCustomResponse)execute(paymentAPIUrl,"paymentByCustom",request,PaymentByCustomResponse.class);
	}

	@Override
	public AppointmentServiceCancelResponse appointmentServiceCancel(AppointmentCancelRequest request) {
		return (AppointmentServiceCancelResponse)execute(appointmentAPIUrl,"appointmentServiceCancel",request,AppointmentServiceCancelResponse.class);
	}

	@Override
	public GetPaymentsResponse getPayments(GetPaymentsRequest request) {
		try {
			if(null ==request.getFromDate() || null ==request.getToDate()) {
				throw new IllegalArgumentException("The parameter fromDate and toDate can't be null");
			}
			
			if(request.getFromDate().after(request.getToDate())) {
				throw new IllegalArgumentException("The parameter fromDate must before toDate");
			}
			
			Map<String,String> params = new HashMap<String,String>();
			params.put("methodName", "getPayments");
			params.put("fromDate", new SimpleDateFormat("yyyy-MM-dd").format(request.getFromDate()));
			params.put("toDate", new SimpleDateFormat("yyyy-MM-dd").format(request.getToDate()));
			String json = postMMS2(collectionAPI,params);
			ObjectMapper mapper = new ObjectMapper();  
			GetPaymentsResponse response = mapper.readValue(json, GetPaymentsResponse.class);
			return response;
		} catch(Exception e) {
			throw new MMSAPIException(e);
		}
		//return (GetPaymentsResponse)execute(collectionAPI,"getPayments",request,GetPaymentsResponse.class);
	}


	@Override
	public List<SetInvoiceDetails> setInvoiceDetails(InvoiceDetailsList invoiceDetailsList) {
		try {
			StringBuffer stringBuf = new StringBuffer();
			stringBuf.append("[");
			List<InvoiceDetails> list = invoiceDetailsList.getInvoiceDetailsList();
			for(InvoiceDetails invoiceDetails :list){
				stringBuf.append("{");
				stringBuf.append("\"InvoiceNo\"").append(":").append("\"").append(invoiceDetails.getInvoiceNo()).append("\"").append(",");
				stringBuf.append("\"CenterId\"").append(":").append("\"").append(invoiceDetails.getCenterId()).append("\"").append(",");
				String invoiceItemsListValue = UrlParameterAssemble.assembleString(invoiceDetails.getInvoiceItemsList(),invoiceDetails.getInvoiceItemsList().getClass(),"$");
				invoiceItemsListValue = invoiceItemsListValue.substring(0, invoiceItemsListValue.length()-1);
				stringBuf.append("\"InvoiceItemsList\"").append(":").append("\"").append(invoiceItemsListValue).append("\"").append(",");
				stringBuf.append("\"PaymentsList\"").append(":").append("\"").append(UrlParameterAssemble.assembleString(invoiceDetails.getPaymentsList(),invoiceDetails.getPaymentsList().getClass(),"$")).append("\"");
				stringBuf.append("}");
			}
			
			stringBuf.append("]");
			
			Map<String, String> params = new HashMap<String,String>();
			params.put("InvoiceDetails", stringBuf.toString());
			
			params.put("methodName", "setInvoiceDetails");
			//String json = getMMS("https://demo.managemyspa.com/api/v100/services/api/v100/integrations/POS/POS.aspx",params);
			String json =postMMS(posAPIUrl,params);
			JsonParser parser = new JsonParser();
			JsonElement el = parser.parse(json);
			if (el.isJsonArray()) {
				List<SetInvoiceDetails> response = null;
				Type listType = new TypeToken<List<SetInvoiceDetails>>(){}.getType();
				final GsonBuilder gsonBuilder = new GsonBuilder();
			    gsonBuilder.registerTypeAdapter(SetInvoiceDetails.class, new SetInvoiceDetailsDeserializer());
				Gson gson = gsonBuilder.create();
				response = gson.fromJson(json, listType);
				return response;
			} else {
				throw new MMSAPIException("faild, the message as following:" + json);
			}
			
		}catch(Exception e) {
			throw new MMSAPIException(e);
		}
	}

}
