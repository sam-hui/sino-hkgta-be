package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class AvailableRoomsRequest extends BaseRequest {
	
	private static final long serialVersionUID = -7479976658034793257L;
	@UrlParameter(name = "ServiceCode")
	@FieldRules(nullable = false)
	private String serviceCode;
	private String therapistCode;
	@FieldRules(nullable = false)
	@UrlParameter(name = "startDate",dateFormat="yyyy-MM-dd HH:mm:ss")
	private Date startDate;
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getTherapistCode() {
		return therapistCode;
	}
	public void setTherapistCode(String therapistCode) {
		this.therapistCode = therapistCode;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	
}