package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.ClassValidator;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

@ClassValidator(validatMethod="validate")
public class UpdateGuestRequest extends BaseRequest {

	private static final long serialVersionUID = -2493240585969994371L;
	@UrlParameter(name="firstName",pos=1)
	private String firstName;
	@UrlParameter(name="middleName",pos=2)
	private String middleName;
	@UrlParameter(name="lastName",pos=3)
	private String lastName;
	@UrlParameter(name="guestId",pos=4)
	private String guestId;
	@UrlParameter(name="guestCode",pos=5)
	private String guestCode;
	@UrlParameter(name="mobilePhone",pos=6)
	private String mobilePhone;
	@UrlParameter(name="email",pos=7)
	private String email;
	@UrlParameter(name="gender",pos=8,map="M=1,F=0")
	private String gender;
	@UrlParameter(name="PasswordOld",pos=9)
	private String passwordOld;
	@UrlParameter(name="passwordNew1",pos=10)
	private String passwordNew1;
	@UrlParameter(name="passwordNew2",pos=11)
	private String passwordNew2;
	@UrlParameter(name="address1",pos=12)
	private String address1;
	@UrlParameter(name="address2",pos=13)
	private String address2;
	@UrlParameter(name="city",pos=14)
	private String city;
	@UrlParameter(name="stateId",pos=15)
	private String stateId;
	@UrlParameter(name="countryId",pos=16)
	private String countryId;
	@UrlParameter(name="zipCode",pos=17)
	private String zipCode;
	@UrlParameter(name="userPassword",pos=18)
	private String userPassword;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGuestId() {
		return guestId;
	}
	public void setGuestId(String guestId) {
		this.guestId = guestId;
	}
	public String getGuestCode() {
		return guestCode;
	}
	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPasswordOld() {
		return passwordOld;
	}
	public void setPasswordOld(String passwordOld) {
		this.passwordOld = passwordOld;
	}
	public String getPasswordNew1() {
		return passwordNew1;
	}
	public void setPasswordNew1(String passwordNew1) {
		this.passwordNew1 = passwordNew1;
	}
	public String getPasswordNew2() {
		return passwordNew2;
	}
	public void setPasswordNew2(String passwordNew2) {
		this.passwordNew2 = passwordNew2;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public void validate() {
		if(null ==getGuestCode() && null ==getGuestId()) {
			throw new IllegalArgumentException("The parameter guestCode and guestId can't both be null");
		}
	}

}
