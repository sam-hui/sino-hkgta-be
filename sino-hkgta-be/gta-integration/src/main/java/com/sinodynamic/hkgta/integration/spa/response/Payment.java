package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "CenterCode", "CenterName", "InvoiceNo", "ReceiptNo",
		"TransactionNo", "InvoiceDate", "InvoiceCenterCode",
		"InvoiceCenterName", "GuestCode", "GuestName", "PaymentType",
		"PaymentDate", "PaymentAmount", "TipAmount", "CardNumber",
		"CardInvoiceNo", "CardBankName", "CardExpiry", "CheckNumber",
		"CheckBankName", "CheckDate", "CustomName", "MembershipName",
		"GiftCardCode", "PrePaidCardCode", "LPProgramName" })
public class Payment extends BaseResponse{

	private static final long serialVersionUID = 3075356519144070439L;
	@JsonProperty("CenterCode")
	private String centerCode;
	@JsonProperty("CenterName")
	private String centerName;
	@JsonProperty("InvoiceNo")
	private String invoiceNo;
	@JsonProperty("ReceiptNo")
	private String receiptNo;
	@JsonProperty("TransactionNo")
	private String transactionNo;
	@JsonProperty("InvoiceDate")
	private String invoiceDate;
	@JsonProperty("InvoiceCenterCode")
	private String invoiceCenterCode;
	@JsonProperty("InvoiceCenterName")
	private String invoiceCenterName;
	@JsonProperty("GuestCode")
	private String guestCode;
	@JsonProperty("GuestName")
	private String guestName;
	@JsonProperty("PaymentType")
	private String paymentType;
	@JsonProperty("PaymentDate")
	private String paymentDate;
	@JsonProperty("PaymentAmount")
	private String paymentAmount;
	@JsonProperty("TipAmount")
	private String tipAmount;
	@JsonProperty("CardNumber")
	private String cardNumber;
	@JsonProperty("CardInvoiceNo")
	private String cardInvoiceNo;
	@JsonProperty("CardBankName")
	private String cardBankName;
	@JsonProperty("CardExpiry")
	private String cardExpiry;
	@JsonProperty("CheckNumber")
	private String checkNumber;
	@JsonProperty("CheckBankName")
	private String checkBankName;
	@JsonProperty("CheckDate")
	private String checkDate;
	@JsonProperty("CustomName")
	private String customName;
	@JsonProperty("MembershipName")
	private String membershipName;
	@JsonProperty("GiftCardCode")
	private String giftCardCode;
	@JsonProperty("PrePaidCardCode")
	private String prePaidCardCode;
	@JsonProperty("LPProgramName")
	private String lPProgramName;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	

	public String getCenterCode() {
		return centerCode;
	}

	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceCenterCode() {
		return invoiceCenterCode;
	}

	public void setInvoiceCenterCode(String invoiceCenterCode) {
		this.invoiceCenterCode = invoiceCenterCode;
	}

	public String getInvoiceCenterName() {
		return invoiceCenterName;
	}

	public void setInvoiceCenterName(String invoiceCenterName) {
		this.invoiceCenterName = invoiceCenterName;
	}

	public String getGuestCode() {
		return guestCode;
	}

	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getTipAmount() {
		return tipAmount;
	}

	public void setTipAmount(String tipAmount) {
		this.tipAmount = tipAmount;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardInvoiceNo() {
		return cardInvoiceNo;
	}

	public void setCardInvoiceNo(String cardInvoiceNo) {
		this.cardInvoiceNo = cardInvoiceNo;
	}

	public String getCardBankName() {
		return cardBankName;
	}

	public void setCardBankName(String cardBankName) {
		this.cardBankName = cardBankName;
	}

	public String getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCheckBankName() {
		return checkBankName;
	}

	public void setCheckBankName(String checkBankName) {
		this.checkBankName = checkBankName;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public String getMembershipName() {
		return membershipName;
	}

	public void setMembershipName(String membershipName) {
		this.membershipName = membershipName;
	}

	public String getGiftCardCode() {
		return giftCardCode;
	}

	public void setGiftCardCode(String giftCardCode) {
		this.giftCardCode = giftCardCode;
	}

	public String getPrePaidCardCode() {
		return prePaidCardCode;
	}

	public void setPrePaidCardCode(String prePaidCardCode) {
		this.prePaidCardCode = prePaidCardCode;
	}

	public String getlPProgramName() {
		return lPProgramName;
	}

	public void setlPProgramName(String lPProgramName) {
		this.lPProgramName = lPProgramName;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}