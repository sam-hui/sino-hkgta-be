package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class QueryStatePKRequest extends BaseRequest {
	
	private static final long serialVersionUID = 3545269690167104224L;
	@UrlParameter(name="statename",pos=1)
	private String statename;
	@UrlParameter(name="countrycode",pos=2)
	private String countryCode;
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
}
