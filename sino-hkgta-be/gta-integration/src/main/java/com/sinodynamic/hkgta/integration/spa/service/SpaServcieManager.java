package com.sinodynamic.hkgta.integration.spa.service;

import java.util.List;

import com.sinodynamic.hkgta.integration.spa.request.AddGuestRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentListRequest;
import com.sinodynamic.hkgta.integration.spa.request.AvailableRoomsRequest;
import com.sinodynamic.hkgta.integration.spa.request.BookAppointmentRequest;
import com.sinodynamic.hkgta.integration.spa.request.CCPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.request.CashPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAppointmentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetCenterTherapistsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetServiceDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceDetailsList;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceRevenueRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceVoidRequest;
import com.sinodynamic.hkgta.integration.spa.request.PaymentByCustomRequest;
import com.sinodynamic.hkgta.integration.spa.request.QueryStatePKRequest;
import com.sinodynamic.hkgta.integration.spa.request.ServicesListByCategoryCodeRequest;
import com.sinodynamic.hkgta.integration.spa.request.SubCategoriesListRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistsAvailableForServiceRequest;
import com.sinodynamic.hkgta.integration.spa.request.UpdateGuestRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentServiceCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AvailableRoomsResponse;
import com.sinodynamic.hkgta.integration.spa.response.BookAppointmentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CCPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CashPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CountryListResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAppointmentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTimeSlotsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetInvoiceRevenueResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetSubcategoriesResponse;
import com.sinodynamic.hkgta.integration.spa.response.GuestAppointmentsListResponse;
import com.sinodynamic.hkgta.integration.spa.response.InvoiceVoidResponse;
import com.sinodynamic.hkgta.integration.spa.response.PaymentByCustomResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCategoryResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCenterTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryServiceListResponse;
import com.sinodynamic.hkgta.integration.spa.response.RegistGuestResponse;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.SetInvoiceDetails;
import com.sinodynamic.hkgta.integration.spa.response.StatePKResponse;
import com.sinodynamic.hkgta.integration.spa.response.UpdateGuestResponse;
import com.sinodynamic.hkgta.integration.util.MMSAPIException;

public interface SpaServcieManager {
	
	/**
	 * Add  Guest profile
	 * @param request
	 * @return
	 * @throws MMSAPIException
	 */
	public RegistGuestResponse registGuest(AddGuestRequest request);
	
	/**
	 * Update guest profile
	 * @param guest
	 * @return
	 * @throws Exception
	 */
	public UpdateGuestResponse updateGuest(UpdateGuestRequest request);
	
	/**
	 * This API returns the list of all service categories defined in the system 
	 * which have atleast one service created under them.
	 * @return
	 * @throws Exception
	 */
	public QueryCategoryResponse getCategoriesList();

	/**
	 * This API would be enhanced to add one more parameter ShowAll, which if set, 
	 * would return even the services which do not have “ShowInCatalog” set. 
	 * @param categoryCode
	 * @return
	 * @throws Exception
	 */
	public QueryServiceListResponse getServicesListByCategoryCode(ServicesListByCategoryCodeRequest request);
	
	/**
	 * This API returns the details of the service passed as parameter.
	 * @param serviceId
	 * @return
	 * @throws Exception
	 */
	public ServiceDetailsResponse queryServiceDetails(GetServiceDetailsRequest request);
	
	/**
	 * This API returns the list of all Countries defined in the system.
	 * @return
	 * @throws Exception
	 */
	public CountryListResponse  queryCountryList();
	
	/**
	 * This API returns the StatePK for the given State Name and Country Code.
	 * @param statename
	 * @param countryCode
	 * @return
	 */
	public StatePKResponse queryStatePK(QueryStatePKRequest request);
	
	/**
	 * This API returns the list of Rooms available for a service / Date / Therapist combination
	 * @param serviceCode
	 * @param therapistCode
	 * @param startDate
	 * @return
	 */
	public AvailableRoomsResponse queryAvailableRooms(AvailableRoomsRequest request);
	
	/**
	 * This API returns the details of Therapists scheduled to work in a given center on a requested date.
	 * @throws Exception
	 */
	public QueryCenterTherapistsResponse getCenterTherapists(GetCenterTherapistsRequest request);
	
	/**
	 * This API books an appointment for a service for a guest at the specified timeslot.
	 * @param bookAppointmentParameters
	 * @return
	 * @throws Exception
	 */
	public BookAppointmentResponse bookAppointment(BookAppointmentRequest bookAppointmentParameters);
	
	/**
	 * This API returns the list of timeslots available for a service / Date / Therapist combination.
	 * @param getAvailableTimeSlotsParameters
	 * @return
	 * @throws Exception
	 */
	public GetAvailableTimeSlotsResponse getAvailableTimeSlots(GetAvailableTimeSlotsRequest getAvailableTimeSlotsParameters);
	
	/**
	 * This API stores the details of successful cash payment in MMS
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public CashPaymentResponse cashPayment(CashPaymentRequest parameters);
	
	/**
	 * This API stores the details of successful Credit card payment in MMS
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public CCPaymentResponse creditCardPayment(CCPaymentRequest parameters);
	
	/**
	 * This API returns the list of all subcategories under a requested Category.
	 * @param categoryCode
	 * @return
	 * @throws Exception
	 */
	public GetSubcategoriesResponse getSubCategoriesListByCode(SubCategoriesListRequest request);
	
	
	/**
	 * creates a membership invoice and accepts payment for the same. 
	 * This API also accepts a membership code and updates the same in our system
	 * @param invoiceDetailsList
	 * @return
	 * @throws MMSAPIException
	 */
	public List<SetInvoiceDetails>  setInvoiceDetails(InvoiceDetailsList invoiceDetailsList);
	
	/**
	 * This API returns the details of Therapists who can perform the requested service  in a given center on a requested date.
	 * @param requestDate
	 * @param serviceId
	 * @param type
	 * @return
	 * @throws MMSAPIException
	 */
	public GetAvailableTherapistsResponse therapistsAvailableForService(TherapistsAvailableForServiceRequest request);
	
	/**
	 * This API voids Invoice, Appointment and all related objects for appointments for which payments did not happen or payments did not go through
	 * @param parameters
	 * @return
	 * @throws MMSAPIException
	 */
	public InvoiceVoidResponse invoiceVoid(InvoiceVoidRequest parameters);
	
	/**
	 * This API gets the details of all the reservations made by a member in MMS.
	 * @param guestCode
	 * @param start
	 * @param limit
	 * @return
	 * @throws MMSAPIException
	 */
	public GuestAppointmentsListResponse guestAppointmentsList(AppointmentListRequest request);
	
	/**
	 * This API cancels an appointment  for a given Invoice Number  in a given center.
	 * @param invoiceNo
	 * @return
	 * @throws MMSAPIException
	 */
	public AppointmentCancelResponse appointmentCancel(InvoiceCancelRequest request);
	
	/**
	 * This API gets details of an appointment for a given Invoice number and center code
	 * @param invoiceNo
	 * @return
	 * @throws MMSAPIException
	 */
	public AppointmentDetailsResponse appointmentDetails(AppointmentDetailsRequest request);
	
	/**
	 * This API returns the revenue recognized from invoices in a specified time period. 
	 * The maximum number of days the API returns in a single call is 7 days.
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public GetInvoiceRevenueResponse  getInvoiceRevenue(InvoiceRevenueRequest request);
	
	/**
	 * This API returns the appointment details in a specified time period. 
	 * @param fromDate
	 * @param toDate
	 * @param status
	 * @return
	 */
	public GetAppointmentsResponse getAppointments(GetAppointmentsRequest request);
	
	
	/**
	 * This API stores the details of successful Custom payment in MMS
	 * @param parameters
	 * @return
	 */
	public PaymentByCustomResponse paymentByCustom(PaymentByCustomRequest parameters);
	
	/**
	 * This API returns the item level details of all payments collected in a time period. 
	 * The maximum number of days the API returns in a single call is 7 days.
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws MMSAPIException
	 */
	public GetPaymentsResponse getPayments(GetPaymentsRequest request);
	
	/**
	 * This API deletes a service from an appointment group.
	 * @param appointmentId
	 * @param comments
	 * @return
	 * @throws MMSAPIException
	 */
	public AppointmentServiceCancelResponse appointmentServiceCancel(AppointmentCancelRequest request);
	
}
