package com.sinodynamic.hkgta.integration.spa.request;

public enum PaymentInstrument {
	CashDetails,AmexDetails,DinersCardDetails,DiscoverCardDetails,MasterCardDetails,VisaCardDetails
}
