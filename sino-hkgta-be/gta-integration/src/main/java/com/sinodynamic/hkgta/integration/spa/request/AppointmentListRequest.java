package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class AppointmentListRequest extends BaseRequest {
	
	private static final long serialVersionUID = 1258395741618986622L;
	
	
	@UrlParameter(name = "guestCode")
	@FieldRules(nullable = false)
	private String guestCode;
	@UrlParameter(name = "start")
	private int start;
	@UrlParameter(name = "limit")
	private int limit;
	public String getGuestCode() {
		return guestCode;
	}
	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	
}
