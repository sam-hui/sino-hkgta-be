package com.sinodynamic.hkgta.integration.push.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.dto.push.PushDto;
import com.sinodynamic.hkgta.dto.push.PushResultDto;
import com.sinodynamic.hkgta.dto.push.PushTopicDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.dto.push.RegisterResultDto;
import com.sinodynamic.hkgta.dto.push.SubscriptDto;
import com.sinodynamic.hkgta.dto.push.SubscriptResultDto;
import com.sinodynamic.hkgta.integration.util.HttpClientUtil;
import com.sinodynamic.hkgta.util.JsonUtil;

@Service
public class PushServiceImpl implements PushService {
    
	@Value(value ="#{pushProperties['pushapi.url']}")
	private String apiUrl;	
	
	@Value(value ="#{pushProperties['socketTimeout']}")
	private int socketTimeout;
	
	@Value(value ="#{pushProperties['connectTimeout']}")
	private int connectTimeout;
	
	@Override
	public RegisterResultDto register(RegisterDto dto) throws Exception {
		String url = apiUrl + "register.api";
		String content = new Gson().toJson(dto);
		String result = HttpClientUtil.post(url, JsonUtil.jsonToMapWithStringValue(content), socketTimeout, connectTimeout);
		RegisterResultDto resultDto = new Gson().fromJson(result, RegisterResultDto.class);
		return resultDto;
	}

	@Override
	public PushResultDto push(PushDto dto) throws Exception {
		String url = apiUrl + "push.api";
		String content = new Gson().toJson(dto);
		String result = HttpClientUtil.post(url, JsonUtil.jsonToMapWithStringValue(content), socketTimeout, connectTimeout);
		PushResultDto resultDto = new Gson().fromJson(result, PushResultDto.class);
		return resultDto;
	}
	
	@Override
	public SubscriptResultDto subscript(SubscriptDto dto) throws Exception {
		String url = apiUrl + "subscript.api";
		String content = new Gson().toJson(dto);
		String result = HttpClientUtil.post(url, JsonUtil.jsonToMapWithStringValue(content), socketTimeout, connectTimeout);
		SubscriptResultDto resultDto = new Gson().fromJson(result, SubscriptResultDto.class);
		return resultDto;
	}
	
	@Override
	public PushResultDto pushTopic(PushTopicDto dto) throws Exception {
		String url = apiUrl + "pushByTopic.api";
		String content = new Gson().toJson(dto);
		String result = HttpClientUtil.post(url, JsonUtil.jsonToMapWithStringValue(content), socketTimeout, connectTimeout);
		PushResultDto resultDto = new Gson().fromJson(result, PushResultDto.class);
		return resultDto;
	}
}
