package com.sinodynamic.hkgta.integration.spa.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.integration.spa.em.Gender;
import com.sinodynamic.hkgta.integration.util.UrlParameter;
import com.sinodynamic.hkgta.integration.util.UrlParameterAssemble;

public class InvoiceDetails implements Serializable{
	private static final long serialVersionUID = -991709334004088864L;

	@UrlParameter(pos = 1, name = "InvoiceNo")
	private String invoiceNo;
	
	@UrlParameter(pos = 2, name = "CenterId")
	private String centerId;
	
	@UrlParameter(pos = 3, name = "InvoiceItemsList")
	private InvoiceItemsList invoiceItemsList;

	@UrlParameter(pos = 4, name = "PaymentsList")
	private PaymentsList paymentsList;
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public InvoiceItemsList getInvoiceItemsList() {
		return invoiceItemsList;
	}

	public void setInvoiceItemsList(InvoiceItemsList invoiceItemsList) {
		this.invoiceItemsList = invoiceItemsList;
	}
	
	
	
	public PaymentsList getPaymentsList() {
		return paymentsList;
	}

	public void setPaymentsList(PaymentsList paymentsList) {
		this.paymentsList = paymentsList;
	}

	public static void main(String[] args) throws Exception {
    	InvoiceItemsList obj = new InvoiceItemsList();
    	obj.setGuid("0965805f-4478-4ee1-a8f9-c874d8d899b8");
    	obj.setFirstName("raghav");
    	obj.setLastName("g");
    	obj.setPhoneNumber("9849853248");
    	obj.setGender(Gender.MALE.getCode());
    	obj.setSaleByEmployeeId("undefined");
    	obj.setPrice(new BigDecimal(3000));
    	obj.setDiscount("0");
    	obj.setFinalSalePrice(new BigDecimal(3000));
    	obj.setTaxes(new BigDecimal(0));
    	obj.setCreatedByEmployeeGUID("d01486e6-0daf-43a6-bf9b-468923e10da0");
    	obj.setQuantity(1);
    	obj.setGuestEmailId("graghav@sohamonline.com");
    	obj.setStartTime(new Date());
    	
    	
    	PaymentsList pay = new PaymentsList();
		pay.setPaymentInstrument("AmexDetails");
		pay.setAmexNumber("88888888");
		pay.setAmexExpiry("12/25");
		pay.setAmexRcptNumber("123456");
		pay.setAmount(new BigDecimal(1000));
		
		InvoiceDetails invoice = new InvoiceDetails();
		invoice.setCenterId("7737a338-89e4-4492-9cd1-3dca4f6c9e72");
		invoice.setInvoiceNo("1f97ddaa");
		invoice.setInvoiceItemsList(obj);
		invoice.setPaymentsList(pay);
    	String str = UrlParameterAssemble.assembleString(invoice,invoice.getClass(),"$");
    	System.out.println(str);
    }
}
