package com.sinodynamic.hkgta.integration.util;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.sinodynamic.hkgta.integration.spa.response.SetInvoiceDetails;

public class SetInvoiceDetailsDeserializer implements JsonDeserializer<SetInvoiceDetails> {

	@Override
	public SetInvoiceDetails deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		
		final JsonObject jsonObject = json.getAsJsonObject();
		 
	    final String invoiceNo = jsonObject.get("Invoice-No").getAsString();
	    final String status = jsonObject.get("status").getAsString();
	 
	  
	 
	    final SetInvoiceDetails setInvoiceDetails = new SetInvoiceDetails();
	    setInvoiceDetails.setInvoiceNo(invoiceNo);
	    setInvoiceDetails.setStatus(status);
	    return setInvoiceDetails;
	}

}
