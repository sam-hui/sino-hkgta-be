package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.sinodynamic.hkgta.integration.util.ClassValidator;
import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

@ClassValidator(validatMethod="validate")
public class GetAvailableTimeSlotsRequest extends BaseRequest{
	private static final long serialVersionUID = -5362425919343091221L;

	@UrlParameter(name="StartTime",dateFormat="yyyy-MM-dd HH:mm:ss")
	@FieldRules(nullable=false)
	private Date startTime;
	
	@UrlParameter(name="type")
	private String therapistRequestType;

	@UrlParameter(name="therapistCode")
	private String therapistCode; 
	
	@UrlParameter(name="AdvanceBookingSlots")
	private boolean advanceBookingSlots; 
	
	@UrlParameter(name="serviceCodeList",formatterMethod="formatter")
	private List<String> serviceCodeList; 
	
	
	@UrlParameter(name="allAvailableSlots")
	private boolean allAvailableSlots; 
	
	@UrlParameter(name="CheckRoomAvailablity")
	private boolean checkRoomAvailablity;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getTherapistRequestType() {
		return therapistRequestType;
	}

	public void setTherapistRequestType(String therapistRequestType) {
		this.therapistRequestType = therapistRequestType;
	}

	

	public String getTherapistCode() {
		return therapistCode;
	}

	public void setTherapistCode(String therapistCode) {
		this.therapistCode = therapistCode;
	}

	public boolean isAdvanceBookingSlots() {
		return advanceBookingSlots;
	}

	public void setAdvanceBookingSlots(boolean advanceBookingSlots) {
		this.advanceBookingSlots = advanceBookingSlots;
	}


	public List<String> getServiceCodeList() {
		return serviceCodeList;
	}

	public void setServiceCodeList(List<String> serviceCodeList) {
		this.serviceCodeList = serviceCodeList;
	}

	public boolean isAllAvailableSlots() {
		return allAvailableSlots;
	}

	public void setAllAvailableSlots(boolean allAvailableSlots) {
		this.allAvailableSlots = allAvailableSlots;
	}

	public boolean isCheckRoomAvailablity() {
		return checkRoomAvailablity;
	}

	public void setCheckRoomAvailablity(boolean checkRoomAvailablity) {
		this.checkRoomAvailablity = checkRoomAvailablity;
	}

	public String formatter(List<String> strs) {
		String result = "";
		StringBuffer sbf = new StringBuffer();
		for(String str:strs) {
			sbf.append(str).append(",");
		}
		result = sbf.toString();
		if(!StringUtils.isBlank(result)) {
			result = StringUtils.removeEnd(result,",");
		}
		return result;
	}
	
	public void validate() {
		String type = getTherapistRequestType();
		if ("2".equals(type)) {
			String therapistCode = getTherapistCode();
			if (StringUtils.isBlank(therapistCode)) {
    			throw new IllegalArgumentException("As the type is 2, so therapistid can't be null");
			}
		}
	}
  
	
}