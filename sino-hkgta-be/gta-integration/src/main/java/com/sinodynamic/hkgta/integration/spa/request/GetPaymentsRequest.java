package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class GetPaymentsRequest extends BaseRequest {

	private static final long serialVersionUID = 4915201575766998748L;
	@UrlParameter(name = "fromDate", dateFormat = "yyyy-MM-dd")
	@FieldRules(nullable = false)
	private Date fromDate;
	@UrlParameter(name = "toDate", dateFormat = "yyyy-MM-dd")
	@FieldRules(nullable = false)
	private Date toDate;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
