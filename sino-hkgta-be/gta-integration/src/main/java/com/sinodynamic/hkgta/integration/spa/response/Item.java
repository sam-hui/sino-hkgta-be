package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "RowNum", "id", "resourceFile", "Type", "HasVariant",
		"IsVariant", "Name", "Description", "Duration", "Price", "PriceText",
		"IncludesTax", "CatalogNew", "CatalogRecommended", "VideoURL",
		"CanBook", "ShowPrice", "SortOrder", "CenterTaxId", "RecoveryTime",
		"FinalPrice" })
public class Item extends BaseResponse {
	private static final long serialVersionUID = 1535798639672859194L;
	@JsonProperty("RowNum")
	private Integer rowNum;
	@JsonProperty("id")
	private String id;
	@JsonProperty("resourceFile")
	private Object resourceFile;
	@JsonProperty("Type")
	private Integer type;
	@JsonProperty("HasVariant")
	private Integer hasVariant;
	@JsonProperty("IsVariant")
	private Integer isVariant;
	@JsonProperty("Name")
	private String name;
	@JsonProperty("Description")
	private String description;
	@JsonProperty("Duration")
	private Integer duration;
	@JsonProperty("Price")
	private Integer price;
	@JsonProperty("PriceText")
	private Object priceText;
	@JsonProperty("IncludesTax")
	private Integer includesTax;
	@JsonProperty("CatalogNew")
	private Integer catalogNew;
	@JsonProperty("CatalogRecommended")
	private Integer catalogRecommended;
	@JsonProperty("VideoURL")
	private Object videoURL;
	@JsonProperty("CanBook")
	private Integer canBook;
	@JsonProperty("ShowPrice")
	private Integer showPrice;
	@JsonProperty("SortOrder")
	private Integer sortOrder;
	@JsonProperty("CenterTaxId")
	private Object centerTaxId;
	@JsonProperty("RecoveryTime")
	private Integer recoveryTime;
	@JsonProperty("FinalPrice")
	private Integer finalPrice;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	

	

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Object getResourceFile() {
		return resourceFile;
	}

	public void setResourceFile(Object resourceFile) {
		this.resourceFile = resourceFile;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getHasVariant() {
		return hasVariant;
	}

	public void setHasVariant(Integer hasVariant) {
		this.hasVariant = hasVariant;
	}

	public Integer getIsVariant() {
		return isVariant;
	}

	public void setIsVariant(Integer isVariant) {
		this.isVariant = isVariant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Object getPriceText() {
		return priceText;
	}

	public void setPriceText(Object priceText) {
		this.priceText = priceText;
	}

	public Integer getIncludesTax() {
		return includesTax;
	}

	public void setIncludesTax(Integer includesTax) {
		this.includesTax = includesTax;
	}

	public Integer getCatalogNew() {
		return catalogNew;
	}

	public void setCatalogNew(Integer catalogNew) {
		this.catalogNew = catalogNew;
	}

	public Integer getCatalogRecommended() {
		return catalogRecommended;
	}

	public void setCatalogRecommended(Integer catalogRecommended) {
		this.catalogRecommended = catalogRecommended;
	}

	public Object getVideoURL() {
		return videoURL;
	}

	public void setVideoURL(Object videoURL) {
		this.videoURL = videoURL;
	}

	public Integer getCanBook() {
		return canBook;
	}

	public void setCanBook(Integer canBook) {
		this.canBook = canBook;
	}

	public Integer getShowPrice() {
		return showPrice;
	}

	public void setShowPrice(Integer showPrice) {
		this.showPrice = showPrice;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Object getCenterTaxId() {
		return centerTaxId;
	}

	public void setCenterTaxId(Object centerTaxId) {
		this.centerTaxId = centerTaxId;
	}

	public Integer getRecoveryTime() {
		return recoveryTime;
	}

	public void setRecoveryTime(Integer recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	public Integer getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Integer finalPrice) {
		this.finalPrice = finalPrice;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}