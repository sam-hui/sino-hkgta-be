package com.sinodynamic.hkgta.integration.spa.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.sinodynamic.hkgta.integration.spa.em.Gender;
import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;
import com.sinodynamic.hkgta.integration.util.UrlParameterAssemble;

/**
 * Except for SaleByEmployeeId, StartTime, EndTime and GuestEmailId, all are mandatory fields.
 * @author Nick_Xiong
 *  Membership GUID 
	$ 3 
	$ GuestFirstName
	$ GuestLastName
	$ PhoneNumber
	$ GuestGender
	$ SaleByEmployeeId
	$ StartTime
	$ EndTime
	$ Price
	$ Discount
	$ FinalSalePrice
	$ Taxes
	$ CreatedByEmployeeGUID
	$ Quantity
	$ GuestEmailId
	$ Membership Code,
 */
public class InvoiceItemsList implements Serializable{
	
	private static final long serialVersionUID = 3723513026633832043L;

	@UrlParameter(pos=1, name = "guid")
	@FieldRules(nullable=false)
	private String guid;
	
	@UrlParameter(pos=2, name = "constant")
	private String constant;
	
	@UrlParameter(pos=3, name = "firstName")
	@FieldRules(nullable=false)
	private String firstName;
	
	@UrlParameter(pos=4, name = "lastName")
	@FieldRules(nullable=false)
	private String lastName;
	
	@UrlParameter(pos=5, name = "phoneNumber")
	@FieldRules(nullable=false)
	private String phoneNumber;
	
	@UrlParameter(pos=6, name = "gender",map="M=1,F=0")
	@FieldRules(nullable=false)
	private String gender;
	
	@UrlParameter(pos=7, name = "saleByEmployeeId")
	private String saleByEmployeeId;
	
	@UrlParameter(pos=8, name = "startTime",dateFormat="yyyy-MM-dd")
	private Date startTime;
	
	@UrlParameter(pos=9, name = "endTime",dateFormat="yyyy-MM-dd")
	private Date endTime;
	
	@UrlParameter(pos=10, name = "price")
	@FieldRules(nullable=false)
	private BigDecimal price;
	
	@UrlParameter(pos=11, name = "discount")
	@FieldRules(nullable=false)
	private String discount;
	
	@UrlParameter(pos=12, name = "finalSalePrice")
	@FieldRules(nullable=false)
	private BigDecimal finalSalePrice;
	
	@UrlParameter(pos=13, name = "taxes")
	@FieldRules(nullable=false)
	private BigDecimal taxes;
	
	@UrlParameter(pos=14, name = "createdByEmployeeGUID")
	@FieldRules(nullable=false)
    private String createdByEmployeeGUID;
    
	@UrlParameter(pos=15, name = "quantity")
	@FieldRules(nullable=false)
    private int quantity;
    
	@UrlParameter(pos=16, name = "guestEmailId")
    private String guestEmailId;
    
	@UrlParameter(pos=17, name = "membershipCode")
    private String membershipCode ;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	

	public String getConstant() {
		return "3";
	}

	public void setConstant(String constant) {
		this.constant = "3";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSaleByEmployeeId() {
		return saleByEmployeeId;
	}

	public void setSaleByEmployeeId(String saleByEmployeeId) {
		this.saleByEmployeeId = saleByEmployeeId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public BigDecimal getFinalSalePrice() {
		return finalSalePrice;
	}

	public void setFinalSalePrice(BigDecimal finalSalePrice) {
		this.finalSalePrice = finalSalePrice;
	}

	public BigDecimal getTaxes() {
		return taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public String getCreatedByEmployeeGUID() {
		return createdByEmployeeGUID;
	}

	public void setCreatedByEmployeeGUID(String createdByEmployeeGUID) {
		this.createdByEmployeeGUID = createdByEmployeeGUID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getGuestEmailId() {
		return guestEmailId;
	}

	public void setGuestEmailId(String guestEmailId) {
		this.guestEmailId = guestEmailId;
	}

	public String getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(String membershipCode) {
		this.membershipCode = membershipCode;
	}
    
    public String toString() {
    	return ToStringBuilder.reflectionToString(this);
    }
    
    public static void main(String[] args) throws Exception {
    	InvoiceItemsList obj = new InvoiceItemsList();
    	obj.setGuid("0965805f-4478-4ee1-a8f9-c874d8d899b8");
    	obj.setFirstName("raghav");
    	obj.setLastName("g");
    	obj.setPhoneNumber("9849853248");
    	obj.setGender(Gender.MALE.getCode());
    	obj.setSaleByEmployeeId("undefined");
    	obj.setPrice(new BigDecimal(3000));
    	obj.setDiscount("0");
    	obj.setFinalSalePrice(new BigDecimal(3000));
    	obj.setTaxes(new BigDecimal(0));
    	obj.setCreatedByEmployeeGUID("d01486e6-0daf-43a6-bf9b-468923e10da0");
    	obj.setQuantity(1);
    	obj.setGuestEmailId("graghav@sohamonline.com");
    	//obj.setStartTime(new Date());
    	
    	String str = UrlParameterAssemble.assembleString(obj,obj.getClass(),"$");
    	System.out.println(str);
    }
}
