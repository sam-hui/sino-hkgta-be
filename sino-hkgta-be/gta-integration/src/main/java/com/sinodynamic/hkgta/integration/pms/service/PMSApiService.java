package com.sinodynamic.hkgta.integration.pms.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.sinodynamic.hkgta.dto.pms.CancelReservationDto;
import com.sinodynamic.hkgta.dto.pms.ChargePostingDto;
import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
import com.sinodynamic.hkgta.dto.pms.FoodItemDto;
import com.sinodynamic.hkgta.dto.pms.HotelPaymentDto;
import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.util.response.MessageResult;

public interface PMSApiService {

	  /**
	   * query available rooms
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public MessageResult getkHotelAvailable(CheckHotelAvailableDto dto) throws Exception;
	  /**
	   * create reservation
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public MessageResult hotelReservation(RoomResDto dto) throws Exception;
	  /**
	   * cancel reservation which haven't paid 
	   * @param resId
	   * @return
	   * @throws Exception
	   */
	  public MessageResult ignoreReservation(String resId) throws Exception;
	  /**
	   * pay by cash value or credit card
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public MessageResult payment(HotelPaymentDto dto) throws Exception;
	  /**
	   * cancel reservation which have paid
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public MessageResult cancelReservation(CancelReservationDto dto) throws Exception;
	  /**
	   * ???????
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public MessageResult chargePostingReserved(ChargePostingDto dto) throws Exception;
	  /**
	   * update room status
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public MessageResult updateRoomStatus(UpdateRoomStatusDto dto) throws Exception;
	  /**
	   * query room status by roomId
	   * @param roomId
	   * @return
	   * @throws ClientProtocolException
	   * @throws IOException
	   */
	  public String queryHouseKeepingStatusByRoomId(String roomId) throws ClientProtocolException, IOException;
	  /**
	   * query all room status list
	   * @return
	   * @throws ClientProtocolException
	   * @throws IOException
	   */
	  public Map<String,String> queryHouseKeepingStatus() throws ClientProtocolException, IOException;
	  /**
	   * query food list
	   * @param outedId
	   * @return
	   * @throws Exception
	   */
	  public List<FoodItemDto> queryOrdersByOutletId(String outedId) throws Exception;
	  /**
	   * modify guest Profiles message
	   * @param dto
	   * @return
	   * @throws Exception
	   */
	  public boolean modifyReservation(RoomResDto dto) throws Exception;
	  
}
