package com.sinodynamic.hkgta.integration.spa.em;

public enum AppointmentStatus {

	ALL("0"), OPEN("1"), CLOSED("2"), CANCEL("3");

	private AppointmentStatus(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	String code;
}
