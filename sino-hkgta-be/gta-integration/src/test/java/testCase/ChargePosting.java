package testCase;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.ChargePostingDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:context-test.xml"})
public class ChargePosting {

	@Autowired
	private PMSApiService PMSService;

	//@Test
	public void chargePostingReserved() throws Exception {
		ChargePostingDto dto = new ChargePostingDto();
        dto.setAmount(new BigDecimal("991"));
        dto.setDescription("TEST Payment");
        dto.setPmsRevenueCode("4800");
        dto.setResvID("255");
        dto.setTicketID("001");
		MessageResult result = PMSService.chargePostingReserved(dto);
		Map<String, String> propertiesMap = result.getProperties();
		//String resultCodeStr = propertiesMap.get("resultCode");
		//String messageStr = propertiesMap.get("message");
		System.out.println(result.isSuccess());
	}
	
}

