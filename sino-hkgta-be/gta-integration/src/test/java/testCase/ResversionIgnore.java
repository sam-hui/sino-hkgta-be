package testCase;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:context-test.xml"})
public class ResversionIgnore {

	@Autowired
	private PMSApiService PMSService;
		
	//@Test
	public void testHotelReservationIgnore() throws Exception {
		MessageResult result = PMSService.ignoreReservation("258");
		System.out.println(result);
		Map<String, String> propertiesMap = result.getProperties();
		//String resultCode = propertiesMap.get("resultCode");
		//String message = propertiesMap.get("message");
	}
	
}
