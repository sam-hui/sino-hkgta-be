package test;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sinodynamic.hkgta.integration.spa.em.AppointmentStatus;
import com.sinodynamic.hkgta.integration.spa.em.CustomPaymentMethod;
import com.sinodynamic.hkgta.integration.spa.em.Gender;
import com.sinodynamic.hkgta.integration.spa.request.AddGuestRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentListRequest;
import com.sinodynamic.hkgta.integration.spa.request.AvailableRoomsRequest;
import com.sinodynamic.hkgta.integration.spa.request.BookAppointmentRequest;
import com.sinodynamic.hkgta.integration.spa.request.CCPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.request.CashPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAppointmentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetCenterTherapistsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetServiceDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceDetails;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceDetailsList;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceItemsList;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceRevenueRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceVoidRequest;
import com.sinodynamic.hkgta.integration.spa.request.PaymentByCustomRequest;
import com.sinodynamic.hkgta.integration.spa.request.PaymentsList;
import com.sinodynamic.hkgta.integration.spa.request.QueryStatePKRequest;
import com.sinodynamic.hkgta.integration.spa.request.ServiceInfoItem;
import com.sinodynamic.hkgta.integration.spa.request.ServicesListByCategoryCodeRequest;
import com.sinodynamic.hkgta.integration.spa.request.SubCategoriesListRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistRequestType;
import com.sinodynamic.hkgta.integration.spa.request.TherapistsAvailableForServiceRequest;
import com.sinodynamic.hkgta.integration.spa.request.UpdateGuestRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDatum;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentServiceCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AvailableRoomsResponse;
import com.sinodynamic.hkgta.integration.spa.response.BookAppointmentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CCPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CashPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.response.CountryListResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAppointmentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTimeSlotsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetInvoiceRevenueResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetSubcategoriesResponse;
import com.sinodynamic.hkgta.integration.spa.response.GuestAppointmentsListResponse;
import com.sinodynamic.hkgta.integration.spa.response.InvoiceVoidResponse;
import com.sinodynamic.hkgta.integration.spa.response.Payment;
import com.sinodynamic.hkgta.integration.spa.response.PaymentByCustomResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCategoryResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCenterTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryServiceListResponse;
import com.sinodynamic.hkgta.integration.spa.response.RegistGuestResponse;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.SetInvoiceDetails;
import com.sinodynamic.hkgta.integration.spa.response.StatePKResponse;
import com.sinodynamic.hkgta.integration.spa.response.UpdateGuestResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.integration.util.MMSAPIException;
import com.sinodynamic.hkgta.integration.util.SetInvoiceDetailsDeserializer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:config/context-test.xml"})
public class SpaServcieManagerTest {
	
	@Autowired
	private SpaServcieManager spaServcieManager;
	
	@Test
	public void testRegistGuest() throws MMSAPIException {
		AddGuestRequest guest = new AddGuestRequest();
		guest.setFirstName("Miranda");
		guest.setLastName("test for250");
		guest.setMobilePhone("5548972251");
		guest.setEmail("abc2@250.com");
		guest.setGender(Gender.MALE.getCode());
		//guest.setGuestCode("0000046");
		RegistGuestResponse result = spaServcieManager.registGuest(guest);
		System.out.println(result);
	}

	
	@Test
	public void testUpdateGuest() throws MMSAPIException {
		UpdateGuestRequest guest = new UpdateGuestRequest();
		guest.setFirstName("Nick1");
		guest.setLastName("Xiong");
		guest.setMobilePhone("9874 561");
		guest.setEmail("xls@163.com");
		guest.setGender(Gender.MALE.getCode());
		guest.setGuestId("8b955ee1-701b-46ad-9551-6210f2768274");
		guest.setZipCode("518000");
		guest.setAddress1("add1");
		guest.setAddress2("addr2");
		guest.setCity("sz");
		guest.setCountryId("45");
		guest.setStateId("812");
		/*guest.setPasswordOld("");
		guest.setPasswordNew1("11111111");
		guest.setPasswordNew2("11111111");*/
		UpdateGuestResponse result = spaServcieManager.updateGuest(guest);
		System.out.println(result);
	}
	
	
	
	@Test
	public void categoriesList() throws MMSAPIException {
		QueryCategoryResponse result = spaServcieManager.getCategoriesList();
		System.out.println(result);
	}
	
	@Test
	public void testqueryServiceList() throws MMSAPIException {
		ServicesListByCategoryCodeRequest request = new ServicesListByCategoryCodeRequest();
		request.setCategoryCode("1");
		QueryServiceListResponse result = spaServcieManager.getServicesListByCategoryCode(request);
		System.out.println(result);
	}
	
	@Test
	public void testQueyrServiceDetails() throws MMSAPIException {
		GetServiceDetailsRequest request = new GetServiceDetailsRequest();
		request.setServiceCode("Service1");
		ServiceDetailsResponse result = spaServcieManager.queryServiceDetails(request);
		System.out.println(result);
	}
	
	@Test
	public void queryCountryList()throws MMSAPIException {
		CountryListResponse result = spaServcieManager.queryCountryList();
		System.out.println(result);
	}
	
	@Test
	public void queryStatePK()throws MMSAPIException {
		QueryStatePKRequest request = new QueryStatePKRequest();
		request.setCountryCode("CN");
		request.setStatename("Hubei Province");
		StatePKResponse result = spaServcieManager.queryStatePK(request);
		System.out.println(result);
	}
	
	@Test
	public void queryAvailableRooms() throws Exception {
		AvailableRoomsRequest request = new AvailableRoomsRequest();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate = df.parse("2015-09-09 10:00:00");
		request.setServiceCode("Service1");
		request.setStartDate(startDate);
		AvailableRoomsResponse result = spaServcieManager.queryAvailableRooms(request);
		System.out.println(result);
	}
	
	@Test
	public void getCenterTherapists() throws Exception {
		GetCenterTherapistsRequest request = new GetCenterTherapistsRequest();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date requestDate = df.parse("2015-12-14");
		request.setRequestDate(requestDate);
		QueryCenterTherapistsResponse result = spaServcieManager.getCenterTherapists(request);
		System.out.println(result);
	}
	
	@Test
	public void bookAppointment() throws Exception {
		BookAppointmentRequest parameter = new BookAppointmentRequest();
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startTime = df1.parse("2015-12-26 18:30:00");
		Date endTime = df1.parse("2015-12-26 19:00:00");
		parameter.setGuestCode("0000001");
		parameter.setStartTime(startTime);
		
		List<ServiceInfoItem> serviceInfo = new ArrayList<ServiceInfoItem>();
		ServiceInfoItem item1 = new ServiceInfoItem();
		item1.setServiceCode("test0001");
		item1.setTherapistCode("J001");
		//item1.setStartTime(startTime);
		//item1.setEndTime(endTime);
		item1.setRoomCode("BLSR002");
		item1.setTherapistRequestType(TherapistRequestType.MALE);
		serviceInfo.add(item1);
		
		/*ServiceInfoItem item2 = new ServiceInfoItem();
		item2.setServiceCode("test0001");
		item2.setTherapistCode("J001");
		//item1.setStartTime(startTime);
		//item1.setEndTime(endTime);
		item2.setRoomCode("BLSR002");
		item2.setTherapistRequestType(TherapistRequestType.MALE);
		serviceInfo.add(item2);*/
		parameter.setServiceInfo(serviceInfo);
		//parameter.setInvoiceNo("442");
		BookAppointmentResponse result = spaServcieManager.bookAppointment(parameter);
		System.out.println(result);
	}
	
	@Test
	public void getAvailableTimeSlots() throws Exception {
		GetAvailableTimeSlotsRequest parameter = new GetAvailableTimeSlotsRequest();
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startTime = df2.parse("2015-12-19 07:00:00");
		parameter.setStartTime(startTime);
		parameter.setTherapistRequestType("2");
		parameter.setAdvanceBookingSlots(false);
		parameter.setAllAvailableSlots(false);
		parameter.setCheckRoomAvailablity(true);
		parameter.setTherapistCode("H001");
		List<String> serviceCodeList = new ArrayList<String>();
		//serviceCodeList.add("Service1");
		serviceCodeList.add("test0001");
		parameter.setServiceCodeList(serviceCodeList);
		GetAvailableTimeSlotsResponse response = spaServcieManager.getAvailableTimeSlots(parameter);
		System.out.println(response);
	}
	
	
	@Test
	public void cashPayment() throws Exception {
		CashPaymentRequest parameter = new CashPaymentRequest();
	    parameter.setInvoiceNo("553");
	    parameter.setGuestCode("mms01");
	    parameter.setAmountPaid(new BigDecimal(500));
	    //parameter.setTipAmount(new BigDecimal(0));
		CashPaymentResponse response = spaServcieManager.cashPayment(parameter);
		System.out.println(response);
	}
	
	@Test
	public void creditCardPayment() throws MMSAPIException {
		CCPaymentRequest parameters = new CCPaymentRequest();
		parameters.setInvoiceNo("550");
		parameters.setGuestCode("mms01");
		parameters.setAmountPaid(new BigDecimal(1000));
		parameters.setTipAmount(new BigDecimal(0));
		parameters.setCCNumber("8888888888");
		parameters.setExpiryDate(new Date());
		parameters.setCardType("Visa");
		parameters.setBankName("BankName");
		parameters.setSecurityCode("000");
		CCPaymentResponse response = spaServcieManager.creditCardPayment(parameters);
		System.out.println(response);
	}
	
	@Test
	public void subcategoriesList()throws MMSAPIException {
		SubCategoriesListRequest request = new SubCategoriesListRequest();
		request.setCategoryCode("1");
		GetSubcategoriesResponse response = spaServcieManager.getSubCategoriesListByCode(request);
		System.out.println(response);
	}
	
	@Test
	public void SetInvoiceDetails()throws MMSAPIException {
		InvoiceItemsList obj = new InvoiceItemsList();
    	obj.setGuid("0965805f-4478-4ee1-a8f9-c874d8d899b8");
    	obj.setFirstName("raghav");
    	obj.setLastName("g");
    	obj.setPhoneNumber("9849853248");
    	obj.setGender(Gender.MALE.getCode());
    	obj.setSaleByEmployeeId("undefined");
    	obj.setPrice(new BigDecimal(3000));
    	obj.setDiscount("0");
    	obj.setFinalSalePrice(new BigDecimal(3000));
    	obj.setTaxes(new BigDecimal(0));
    	obj.setCreatedByEmployeeGUID("d01486e6-0daf-43a6-bf9b-468923e10da0");
    	obj.setQuantity(1);
    	obj.setGuestEmailId("graghav@sohamonline.com");
    	//obj.setStartTime(new Date());
    	//obj.setEndTime(new Date());
    	
    	PaymentsList pay = new PaymentsList();
		pay.setPaymentInstrument("CashDetails");
		pay.setAmexNumber("88888888");
		pay.setAmexExpiry("12/25");
		pay.setAmexRcptNumber("123456");
		pay.setAmount(new BigDecimal(3000));
		
		InvoiceDetails invoice = new InvoiceDetails();
		invoice.setCenterId("7737a338-89e4-4492-9cd1-3dca4f6c9e72");
		invoice.setInvoiceNo("1f97ddaa");
		invoice.setInvoiceItemsList(obj);
		invoice.setPaymentsList(pay);
		
		InvoiceDetailsList invoiceDetailsList = new InvoiceDetailsList();
		List<InvoiceDetails> list = new ArrayList<InvoiceDetails>();
		list.add(invoice);
		invoiceDetailsList.setInvoiceDetailsList(list);
		
		List<SetInvoiceDetails> response = spaServcieManager.setInvoiceDetails(invoiceDetailsList);
		
		System.out.println("response=" + response);
	}
	
	@Test
	public void therapistsAvailableForService() throws MMSAPIException, ParseException{
		TherapistsAvailableForServiceRequest request = new TherapistsAvailableForServiceRequest();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date requestDate = df.parse("2015-12-14");
		request.setRequestDate(requestDate);
		request.setServiceCode("Service1");
		request.setType(TherapistRequestType.ANY);
		GetAvailableTherapistsResponse response = spaServcieManager.therapistsAvailableForService(request);
		System.out.println(response);
	}
	
	@Test
	public void invoiceVoid() {
		InvoiceVoidRequest parameters = new InvoiceVoidRequest();
		parameters.setComments("my comments");
		parameters.setInvoiceNo("24");
		parameters.setUserCode("userCode");
		InvoiceVoidResponse response = spaServcieManager.invoiceVoid(parameters);
		System.out.println(response);
	}
	
	@Test
	public void guestAppointmentsList() {
		AppointmentListRequest request = new AppointmentListRequest();
		request.setGuestCode("mms01");
		request.setStart(0);
		request.setLimit(10);
		GuestAppointmentsListResponse response = spaServcieManager.guestAppointmentsList(request);
		System.out.println(response);
	}
	
	@Test
	public void appointmentCancel() {
		InvoiceCancelRequest request = new InvoiceCancelRequest();
		request.setInvoiceNo("141");
		AppointmentCancelResponse response = spaServcieManager.appointmentCancel(request);
		System.out.println(response);
	}
	
	@Test
	public void appointmentDetails() {
		AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo("141");
		AppointmentDetailsResponse response = spaServcieManager.appointmentDetails(request);
		System.out.println(response);
	}
	
	@Test
	public void getInvoiceRevenue() throws ParseException{
		//for(int i=0; i <=100;i++) {
		InvoiceRevenueRequest request = new InvoiceRevenueRequest();
			DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			Date startTime = df1.parse("2015-12-03");
			Date endTime = df1.parse("2015-12-04");
			request.setFromDate(startTime);
			request.setToDate(endTime);
			GetInvoiceRevenueResponse response = spaServcieManager.getInvoiceRevenue(request);
			System.out.println( response);
		//}
		
	}
	
	@Test
	public void getAppointments()throws ParseException{
		try{
			GetAppointmentsRequest request = new GetAppointmentsRequest();
			DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			Date startTime = df1.parse("2015-12-25");
			Date endTime = df1.parse("2015-12-25");
			request.setFromDate(startTime);
			request.setToDate(endTime);
			request.setStatus(AppointmentStatus.OPEN);
			GetAppointmentsResponse response = spaServcieManager.getAppointments(request);
			List<AppointmentDatum> list = response.getAppointmentData();
			System.out.println("total:"+list.size());
			for(AppointmentDatum appointmentDatum :list){
				System.out.print(appointmentDatum.getInvoiceNo()+"  "+appointmentDatum.getStatus()+ "  "+appointmentDatum.getServiceCode());
				System.out.print(" start "+appointmentDatum.getStartTime());
				System.out.print(" end "+appointmentDatum.getEndTime());
				System.out.print(" create "+appointmentDatum.getCreationDate());
				System.out.print(" checkin "+appointmentDatum.getCheckInTime());
				System.out.println("");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void paymentByCustom() {
		PaymentByCustomRequest parameters = new PaymentByCustomRequest();
		parameters.setInvoiceNo("308");
		parameters.setAmountPaid(new BigDecimal(100));
		parameters.setTipAmount(new BigDecimal(0));
		parameters.setPaymentMethod(CustomPaymentMethod.CASHVALUE);
		PaymentByCustomResponse response = spaServcieManager.paymentByCustom(parameters);
		System.out.println(response);
	}
	
	@Test
	public void getPayments() throws ParseException {
		GetPaymentsRequest request = new GetPaymentsRequest();
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		Date startTime = df1.parse("2015-12-18");
		Date endTime = df1.parse("2015-12-18");
		request.setFromDate(startTime);
		request.setToDate(endTime);
		GetPaymentsResponse response = spaServcieManager.getPayments(request);
		List<Payment> list = response.getPayments();
		System.out.println("total:"+list.size());
		for(Payment payment :list){
			System.out.println(payment.getInvoiceNo()+"  "+payment.getPaymentDate()+ "  "+payment.getPaymentType() +"  "+payment.getTipAmount()+"  "+payment.getCustomName());
		}
		System.out.println(response);
	}
	
	
	@Test
	public void test() {
		String json = "[{\"Invoice-No\":\"1f97ddaa\",\"status\":\"1\"},{\"Invoice-No\":\"1f97ddaa\",\"status\":\"1\"}]";
		System.out.println(json);
		List<SetInvoiceDetails> response = null;
		Type listType = new TypeToken<List<SetInvoiceDetails>>(){}.getType();
		//response = GsonUtil.fromJson(json, listType);
		final GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(SetInvoiceDetails.class, new SetInvoiceDetailsDeserializer());
		Gson gson = gsonBuilder.create();
		response = gson.fromJson(json, listType);
		System.out.println(response);
		
	}
	
	@Test
	public void appointmentServiceCancel() {
		AppointmentCancelRequest request = new AppointmentCancelRequest();
		String appointmentId ="308ea017-c00e-414b-8267-88289599ad66";
		String comments ="yeyeye";
		request.setAppointmentId(appointmentId);
		request.setComments(comments);
		AppointmentServiceCancelResponse response = spaServcieManager.appointmentServiceCancel(request);
		System.out.println(response);
	}
	
	@Test
	public void test2() {
		String desc = " CSH-566611799990";
		String transactionType = StringUtils.substring(desc.trim(), 0, 3);
		System.out.println(transactionType);
	}
}
