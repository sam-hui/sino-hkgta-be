package test;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.CancelReservationDto;
import com.sinodynamic.hkgta.dto.pms.ChargePostingDto;
import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
import com.sinodynamic.hkgta.dto.pms.FoodItemDto;
import com.sinodynamic.hkgta.dto.pms.HotelPaymentDto;
import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.response.MessageResult;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml"})
public class PMSServiceMangerTest {

	@Autowired
	private PMSApiService PMSApiService;
	
	
	
	@Test
	public void testQueryAvailableHotels() throws Exception {
		CheckHotelAvailableDto checkHotelAvailableDto = new CheckHotelAvailableDto();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		checkHotelAvailableDto.setStayBeginDate("2015-07-25");
		checkHotelAvailableDto.setStayEndDate("2015-07-26");
		checkHotelAvailableDto.setAdultCount(1);
		MessageResult  result = PMSApiService.getkHotelAvailable(checkHotelAvailableDto);
		System.out.println(result);
		System.out.println(result.getObject());
	}
	
	@Test
	public void testhotelReservation() throws Exception {
		RoomResDto dto = new RoomResDto();
		dto.setRoomTypeCode("STND");
		dto.setRoomRateCode("BPIC");
		dto.setAdultCount(2L);
		dto.setChildCount(1L);
		dto.setStartDate("2015-09-25");
		dto.setEndDate("2015-09-26");
		dto.setGender("Male");
		dto.setNamePrefix("Mr.");
		dto.setGivenName("TEST CASE 004");
		dto.setSurname("Chan");
		dto.setPhoneNumber("77778888");
		dto.setMembershipID("121111");
		System.out.println(PMSApiService.hotelReservation(dto));
	}
	
	
	
	@Test
	public void checkHotelAvailable() throws Exception {
		CheckHotelAvailableDto dto =  new CheckHotelAvailableDto();
		dto.setAdultCount(8);
		dto.setChildCount(1);
		dto.setStayBeginDate("2015-07-20");
		dto.setStayEndDate("2015-07-20");
		MessageResult result = PMSApiService.getkHotelAvailable(dto);
		System.out.println(result);
	}
	
	@Test
	public void hotelReservation() throws Exception {
		RoomResDto dto =  new RoomResDto();
		dto.setAddress1("address1");
		dto.setAddress2("address2");
		dto.setAddress3("address3");
		dto.setAddress4("address4");
		dto.setAdultCount(1L);
		dto.setChildCount(0L);
		dto.setEmail("12@12.com");
		dto.setEndDate("2015-09-29");
		dto.setStartDate("2015-09-29");
		dto.setSurname("hhh4");
		dto.setGender("Male");
		dto.setGivenName("hhh4");
		dto.setMembershipID("998");
		dto.setNamePrefix("Mr");
		dto.setPhoneNumber("12345672");
		dto.setRoomRateCode("BPIC");
		dto.setRoomTypeCode("STND");
		MessageResult result = PMSApiService.hotelReservation(dto);
		System.out.println(result);
	}
	@Test
	public void ignoreReservation() throws Exception {
		MessageResult result = PMSApiService.ignoreReservation("295");
		System.out.println(result);
	}
	
	@Test
	public void payment() throws Exception {
		HotelPaymentDto dto = new HotelPaymentDto();
		dto.setAmount(new BigDecimal("6930"));
		dto.setAmountAfterTax(new BigDecimal("6930"));
		dto.setCardCode("MC");
		dto.setCardHolderName("John Q Smith");
		dto.setCertificateNumber("1231231");
		dto.setExpireDate("1614");
		dto.setMaskedCardNumber("*************99");
		dto.setMemberNumber("999");
		dto.setPaymentType(PaymentMethod.VISA.name());
		dto.setResID("296");
		MessageResult result = PMSApiService.payment(dto);
		System.out.println(result);
	}
	@Test
	public void chargePostingReserved() throws Exception {
		ChargePostingDto dto = new ChargePostingDto();
        dto.setAmount(new BigDecimal("128"));
        dto.setDescription("string");
        dto.setPmsRevenueCode("4800");
        dto.setResvID("296");
        dto.setTicketID("123123");
		MessageResult result = PMSApiService.chargePostingReserved(dto);
		System.out.println(result);
	}
	@Test
	public void cancelReservation() throws Exception {
		CancelReservationDto dto =  new CancelReservationDto();
		dto.setResId("296");
		dto.setMemberName("hhh4");
		dto.setMembershipID("998");
		dto.setReservationTimeSpanEnd("2015-06-29");
		dto.setReservationTimeSpanStart("2015-06-28");
		MessageResult result = PMSApiService.cancelReservation(dto);
		System.out.println(result);
	}
	
	@Test
	public void updateRoomStatus() throws Exception {
		UpdateRoomStatusDto dto =  new UpdateRoomStatusDto();
		dto.setReasonCode("OOTH");
		dto.setRoleName("ROOM_ATTENDANT");
		dto.setRoomNo("135");
		dto.setStatus("OUT_OF_ORDER");
		dto.setUserId("101");
		MessageResult result = PMSApiService.updateRoomStatus(dto);
		System.out.println(result);
	}
	
	
	@Test
	public void testQueryRoomStatus() throws ClientProtocolException, IOException {
		System.out.println(PMSApiService.queryHouseKeepingStatusByRoomId("122"));
	}
	
	@Test
	public void testqueryOrdersByOutletId() throws Exception {
		for(FoodItemDto item :PMSApiService.queryOrdersByOutletId("0001")) {
			System.out.println(item);
		}
		
	}
	
	@Test
	public void testmodifyReservation() throws Exception {
		RoomResDto dto =  new RoomResDto();
		dto.setAddress1("address1");
		dto.setAddress2("address2");
		dto.setAddress3("address3");
		dto.setAddress4("address4");
		dto.setAdultCount(1L);
		dto.setChildCount(0L);
		dto.setEmail("12sss@12.com");
		dto.setEndDate("2015-09-29");
		dto.setStartDate("2015-09-29");
		dto.setSurname("hhh4");
		dto.setGender("Male");
		dto.setGivenName("hhh4");
		dto.setMembershipID("998");
		dto.setNamePrefix("Mr");
		dto.setPhoneNumber("12345672");
		dto.setRoomRateCode("BPIC");
		dto.setRoomTypeCode("STND");
		dto.setReservationID("297");
		System.out.println(PMSApiService.modifyReservation(dto));
	}
}
