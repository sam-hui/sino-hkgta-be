package com.sinodynamic.hkgta.util.constant;

public enum RoomHousekeepTaskStatus {
	
	PND("Pending"),OPN("Open task"), RTI("ready to inspect"), IRJ("inspector rejected"), CMP("task completed"), CAN("cancelled"), DND("Do not disturb");

		private String desc;

		private RoomHousekeepTaskStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}
		
		public void setDesc(String desc) {
			this.desc = desc;
		}
}

