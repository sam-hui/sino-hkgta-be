package com.sinodynamic.hkgta.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.sinodynamic.hkgta.util.constant.Constant;

/**
 * @desc connect to a remote machine with security ssh. Note, after the file operation with a instance of this class,
 * you have to invoke the disconnect to release the session
 * @author Mianping_Wu
 *
 */
public class SftpUtil {

    private static final Logger logger = Logger.getLogger(SftpUtil.class.getName());
    
    private Session session;
    private Channel channel;
    private static Properties appProps;
    
    
    static {
    	
    	try {
    		
    		Resource resource = new ClassPathResource("placeholder/app.properties");
    		appProps = PropertiesLoaderUtils.loadProperties(resource);
    		
        	/**
        	 * This section is just for test
        	Properties appProps = new Properties();
        	appProps.load(new FileReader("D:\\GTA\\code\\sino-hkgta\\src\\main\\resources\\placeholder\\app.properties"));
        	*/
    		
    	} catch (IOException e) {
    		
    		e.printStackTrace();
    		logger.error("placeholder/app.properties cannot be found in classpath!");
    	}
    }
    
    public enum Mode {
    	
    	OVERWRITE(ChannelSftp.OVERWRITE),
    	RESUME(ChannelSftp.RESUME),
    	APPEND(ChannelSftp.APPEND);
    	
    	int value;
    	private Mode(int value) {
    		this.value = value;
    	}
    	
    	public int getValue() {
    		return value;
    	}
    }

    
    public SftpUtil() throws Exception {

        String ftpHost = appProps.getProperty(Constant.SFTP_HOST, null);
        String port = appProps.getProperty(Constant.SFTP_PORT, null);
        String ftpUserName = appProps.getProperty(Constant.SFTP_USERNAME, null);
        String ftpPassword = appProps.getProperty(Constant.SFTP_PASSWORD, null);
        String timeout = appProps.getProperty(Constant.SFTP_TIMEOUT, null);

        int ftpPort = Constant.SFTP_DEFAULT_PORT;
        if (CommUtil.nvl(port).length() != 0) {
            ftpPort = Integer.valueOf(port);
        }
        
        int ftpTimeout = Constant.SFTP_DEFAULT_TIMEOUT;
        if (CommUtil.nvl(timeout).length() != 0) {
        	ftpTimeout = Integer.valueOf(timeout);
        }

        JSch jsch = new JSch();
        session = jsch.getSession(ftpUserName, ftpHost, ftpPort);
        logger.info("Sftp session created");
        
        if (ftpPassword != null) {
            session.setPassword(ftpPassword);
        }
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.setTimeout(ftpTimeout);
        session.connect();
        logger.info("Sftp session connected.");

        channel = session.openChannel("sftp");
        channel.connect();
        
        logger.info("Connected successfully to ftpHost = " + ftpHost + ", with ftpUserName = " + ftpUserName
                + ", with returning: " + channel);
    }
    
    
    /**
     * @desc  fetch a remote file as an InputStream
     * @param remoteFileName	must be a file filename on remote
     * @throws Exception
     * @return InputStream
     */
    public InputStream download(String remoteFileName) throws Exception {
    	
    	if (remoteFileName == null) return null;
    	
    	InputStream is =  null;
    	try {
    		ChannelSftp sftp = (ChannelSftp) channel;
        	is = sftp.get(remoteFileName);
    	} catch (Exception e) {
    		throw e;
    	} 
    	
    	return is;
    }
    
    
    /**
     * @desc download file from remote to localhost, default mode is overwrite
     * @param remoteFileName	must be a filename on remote
     * @param localFilePath	   a file or a directory on localhost, it will be overwrite when the localFilePath is a exsisting file
     * @throws Exception
     */
    public void download(String remoteFileName, String localFilePath) throws Exception {
    	
    	if (remoteFileName == null) return;
    	if (!isExist(localFilePath)) return;
    	
    	try {
    		ChannelSftp sftp = (ChannelSftp) channel;
    		sftp.get(remoteFileName, localFilePath);
    	} catch (Exception e) {
    		throw e;
    	}
    }
    
    
    /**
     * @desc  download file from remote to localhost, with mode can be configured
     * @param remoteFileName 	must be a filename on remote
     * @param localFilePath 	a file or a directory on localhost, it will be overwrite when localFilePath is a existing file
     * @param mode with three mode of OVERWRITE, RESUME and APPEND
     * @throws Exception
     */
    public void download(String remoteFileName, String localFilePath, Mode mode) throws Exception {
    	
    	if (mode == null) {
    		download(remoteFileName, localFilePath);
    		return;
    	}
    	
    	if (remoteFileName == null) return;
    	if (!isExist(localFilePath)) return;
    	
    	try {
        	ChannelSftp sftp = (ChannelSftp) channel;
        	sftp.get(remoteFileName, localFilePath, null, mode.getValue());
    	} catch (Exception e) {
    		throw e;
    	}
    	
    }
    
    
    /**
     * @desc write a stream into a remote file, default mode is overwirte
     * @param localInputStream 	  local InputStream waiting to be write into remote file
     * @param remoteFileName	must to be a file name on remote
     * @throws Exception
     */
    public void upload(InputStream localInputStream, String remoteFileName) throws Exception {
    	
    	if (localInputStream == null) return;
    	if (remoteFileName == null) return;
    	
    	try {
    		ChannelSftp sftp = (ChannelSftp) channel;
    		sftp.put(localInputStream, remoteFileName);
    	} catch (Exception e) {
    		throw e;
    	} 
    	
    }
    
    
    /**
     * @desc upload with default mode overwrite
     * @param localFilePath    full name standing for localFile which is waited to be uploaded
     * @param remoteFilePath    a file or directory to be write into
     * @throws Exception
     */
    public void upload(String localFilePath, String remoteFilePath) throws Exception {
    	
    	if (!isFileExist(localFilePath)) return;
    	if (!isValid(remoteFilePath)) return;

    	try {
        	ChannelSftp sftp = (ChannelSftp) channel;
        	sftp.put(localFilePath, remoteFilePath);
    	} catch (Exception e) {
    		
    		throw e;
    	} 
    }
    
    
    /**
     * @param localFilePath    full name standing for localFile which is waited to be uploaded
     * @param remoteFilePath    a file or directory to be write into
     * @param mode with three mode of OVERWRITE, RESUME and APPEND
     * @throws Exception
     */
    public void upload(String localFilePath, String remoteFilePath, Mode mode) throws Exception {
    	
    	if (mode == null) {
    		upload(localFilePath, remoteFilePath);
    		return;
    	}
    	
    	if (!isFileExist(localFilePath)) return;
    	if (!isValid(remoteFilePath)) return;
    	
    	try {
        	ChannelSftp sftp = (ChannelSftp) channel;
        	sftp.put(localFilePath, remoteFilePath, mode.getValue());
    	} catch (Exception e) {
    		throw e;
    	}
    }
    
    /**
     * @desc move the file on sftp server from one directory to another
     * @param currentPath
     * @param targetPath
     * @param fileName
     * @throws Exception
     */
	public void move(String fileName, String currentPath, String targetPath) throws Exception {

		String receivePath = appProps.getProperty(Constant.SFTP_RECEIVE_PATH, "");
		String receiveBackPath = appProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, "");
		
		if (isValid(currentPath)) receivePath = currentPath;
		if (isValid(targetPath)) receiveBackPath = targetPath;
		
//		String currentFilePath = receivePath + File.separator + fileName;
//		String targeFilePath = receiveBackPath + File.separator + fileName;
		
		String currentFilePath = null;
		String targeFilePath = null;
		if (currentPath.split("\\\\").length > 1) {
			
			currentFilePath = receivePath + "\\" + fileName;
			targeFilePath = receiveBackPath + "\\" + fileName;
			
		} else if (currentPath.split("/").length > 1){
			
			currentFilePath = receivePath + "/" + fileName;
			targeFilePath = receiveBackPath + "/" + fileName;
		}
    	
    	try {
    		
        	ChannelSftp sftp = (ChannelSftp) channel;
        	sftp.rename(currentFilePath, targeFilePath);
    	} catch (Exception e) {
    		
    		throw e;
    	} 
	}
	
	
	/**
	 * @desc  this function is to list all the files in subPath based on sftp configured path
	 * @param remoteFilePath the sub directory based on configured path
	 * @return filename list
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<String> list(String remoteFilePath) throws Exception {

	    List<String> fileList = new ArrayList<String>();
    	
    	try {
    		
        	ChannelSftp sftp = (ChannelSftp) channel;
        	sftp.cd(remoteFilePath);
        	Vector<LsEntry> fileVector = sftp.ls("*");
            for(int i=0; i < fileVector.size(); i++){
            	LsEntry file = fileVector.get(i);
            	fileList.add(file.getFilename());
            }
    	} catch (Exception e) {
    		throw e;
    	} 
    	
		return fileList;
	}
	
	
	/**
	 * @param remoteFilePath   the remote file which you want to delete
	 * @throws Exception
	 */
	public void delete(String remoteFilePath) throws Exception {  
		
    	try {
        	ChannelSftp sftp = (ChannelSftp) channel;
        	sftp.rm(remoteFilePath);  
    	} catch (Exception e) {
    		throw e;
    	} 
	}

    

    public void disconnect() throws Exception {
    	
        if (channel != null) {
            channel.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
        
        logger.info("Sftp session disconnected.");
    }
    
    private boolean isValid(String path) {
    	
    	if (CommUtil.nvl(path).length() == 0) return false;
    	return true;
    }
    
    private boolean isExist(String fileName) {
    	
    	File file = new File(fileName);
    	return file.exists();
    	
    }
    
    private boolean isFileExist(String fileName) {
    	
    	File file = new File(fileName);
    	return file.exists() && file.isFile();
    	
    }
    
    
//    public static void main(String[] args) throws Exception {
//    	
//    	String fromPath = "D:\\DailyNote\\2015-06-08\\VA Transaction Rpt.csv";
//    	SftpUtil sftp = null;
//    	try {
//    		sftp = new SftpUtil();
//    		
//    		//list
//    		//List<String> fileNames = sftp.list("/home/sftp_root/shared");
//    		//for (String fileName : fileNames) System.out.println(fileName);
//    		
//    		//move
//    		//sftp.move("some", "/home/sftp_root/shared", "/home/sftp_root/shared/testdir");
//    		
//    		//delete
//    		//sftp.delete("/home/sftp_root/shared/testdir/some");
//    		
//    		//upload
//    		sftp.upload(fromPath, "/RECEIVE");
//    		
//    		//download
//    		//sftp.download("/home/sftp_root/shared/swagger.txt", "C:\\Users\\Mianping_Wu\\Desktop");
//    		
//    	} catch(Exception e) {
//    		throw e;
//    	} finally {
//    		sftp.disconnect();
//    	}
//    	
//    }
}
