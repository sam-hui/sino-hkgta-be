package com.sinodynamic.hkgta.util.constant;

import java.util.HashMap;

/**
 * 
 * @author Mianping_Wu
 * @since 5/11/2015
 */
public enum PermitCardMasterEnumStatus {

	OH("OH", "onhand"), ISS("ISS", "issued"), CAN("CAN", "cancelled"), DPS("DPS", "disposal"), RTN("RTN", "In Store");

	private String code;
	private String description;
	private static HashMap<String, PermitCardMasterEnumStatus> map;
	
	static {
		
		map = new HashMap<String, PermitCardMasterEnumStatus>();
		for (PermitCardMasterEnumStatus status : PermitCardMasterEnumStatus.values()) {
			map.put(status.getCode(), status);
		}
	}

	private PermitCardMasterEnumStatus(String code, String desc) {

		this.code = code;
		this.description = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static boolean isValid(String code) {
		
		PermitCardMasterEnumStatus status = map.get(code);
		if (status == null) return false; 
		return true;
	}

}
