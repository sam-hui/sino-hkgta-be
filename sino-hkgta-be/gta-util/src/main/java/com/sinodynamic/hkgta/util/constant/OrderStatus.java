package com.sinodynamic.hkgta.util.constant;

public enum OrderStatus {
	
	OPN("OPN"), CAN("CAN"), CMP("CMP"), REJ("REJ");

		private String desc;

		private OrderStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

