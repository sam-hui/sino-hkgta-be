package com.sinodynamic.hkgta.util.constant;

/**
 * @author Vian_Tang
 *
 */
public enum OrderByType {  
	PresentationName, CreateBy, UpdateDate;  
   
} 