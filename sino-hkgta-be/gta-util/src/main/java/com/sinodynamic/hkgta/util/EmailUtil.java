package com.sinodynamic.hkgta.util;

import com.sinodynamic.hkgta.util.constant.EmailType;

public class EmailUtil {

	/**
	 * This method has moved into com.sinodynamic.hkgta.util.CommUtil,<br>
	 * Use CommUtil.generatePdfName(String emailType) instead.
	 * **/
	@Deprecated
	public static String generatePdfName(String emailType){

		if(EmailType.INVOICE.toString().equals(emailType)){
			return "HKGTA-INVOICE-SP00001.pdf";
		}else if(EmailType.RECEIPT.toString().equals(emailType)){
			return "HKGTA-RECEIPT-SP00001.pdf";
		}

		return "HKGTA-RECEIPT-SP00001.pdf";
	}


}
