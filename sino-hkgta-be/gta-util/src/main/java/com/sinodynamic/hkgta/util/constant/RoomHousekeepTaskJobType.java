package com.sinodynamic.hkgta.util.constant;

public enum RoomHousekeepTaskJobType {

	ADH("Ad Hoc"), RUT("Routine"), SCH("Scheduled"), MTN("Maintenance");

	private String desc;

	private RoomHousekeepTaskJobType(String d) {
		this.desc = d;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean equalsWith(String jobType) {
		return jobType != null && jobType.toUpperCase().equals(this.name());
	}

	public static String getDesc(String jobType) {
		for(RoomHousekeepTaskJobType type : RoomHousekeepTaskJobType.values()){
			if(type.equalsWith(jobType)) {return type.desc;}
		}
		return "";
	}
}
