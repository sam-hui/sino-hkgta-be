package com.sinodynamic.hkgta.util.constant;

public enum MessageTemplateParams {
			UserName("username"),
			FromName("fromname"),
			
			RegistrationUrl("registrationurl"),
			LoginId("loginid"),
			Password("password"),
			
			Letf_Symbol("{"),
			Right_Symbol("}"),
			MemberEmail("Member’s Email"),
			Salutation("salutation"),
			FirstName("Member First Name"),
			LastName("lastName"),
			FullName("fullName"),
			Confirmation("confirmationId"),
			StartTime("startTime"),
			EndTime("endTime"),
			CoachName("coachFullName"),
			FacilityName("facilityName"),
			FacilityType("facilityType"),
			FacilityTypeQty("facilityTypeQty"),
			TennisType("tennisCourtType"),
			BayType("golfBayType"),
			FacilityNo("facilityNo"),
			//for service refund & cash value refund begin
			RefundStatus("refundServiceStatus"),
			ServiceType("refundServiceType"),
			RefundedDate("updateDate"),
			RefundAmount("approvedAmt"),
			RefundMethod("refundMoneyType"),
			RefundNo("refundNo"),
			ResultMessage("refundStatus"),
			//for service refund & cash value refund end
			BookingDate("bookingDate"),
			TimeLeft("timeLeft"),
			TopupMethod("topupMethod"),
			CurrentBalance("currentBalance"),
			
			HKPublicHolidayURL("HKPublicHolidayURL"),
			;
			
			private String param;
			
			private MessageTemplateParams(String param)
			{
				this.param = param;
			}
			
			public String getParam()
			{
				return "(?i)\\{"+this.param+"\\}";
			}
}
