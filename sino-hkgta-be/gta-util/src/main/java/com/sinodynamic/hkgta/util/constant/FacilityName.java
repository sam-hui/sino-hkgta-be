package com.sinodynamic.hkgta.util.constant;

public enum FacilityName {
	GOLF("Golf "),
	TENNIS("Tennis");
	
	private FacilityName(String desc){
		this.desc = desc;
	}
	
	private String desc;
	
	public String getDesc(){
		return this.desc;
	}
	
	@Override
	public String toString() {
		return this.desc;
	}
}
