package com.sinodynamic.hkgta.util;

import java.util.Properties;

import javax.annotation.Resource;


public abstract  class AbstractUploadFilter {
	abstract public void filter(String originalFilename) ;
	String getSuffix(String originalFilename) {
		if(!CommUtil.notEmpty(originalFilename)){
			throw new RuntimeException("Empty origin file name.");
		}
		
		int start = originalFilename.lastIndexOf(".");
		
		if(start == -1){
			throw new RuntimeException("Can not identify suffix.");
		}
		
		String suffix = originalFilename.substring(start+1, originalFilename.length());
		
		if(!CommUtil.notEmpty(suffix)){
			throw new RuntimeException("Can not identify suffix.");
		}
		return suffix;
	}
}
