package com.sinodynamic.hkgta.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateCalcUtil {
	private final static DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public static Date RemoveTimePart(Date date) throws Exception {
		return dateFormat.parse(dateFormat.format(date));
	}
	
	public static Date parseDateTime(String inputDate) throws Exception {
		return timeFormat.parse(inputDate);
	}
	
	public static Date parseDate(String inputDate) throws Exception {
		return dateFormat.parse(inputDate);
	}
	
	public static String formatDate(Date date) throws Exception {
		return dateFormat.format(date);
	}
	
	public static String formatDate(Date date, DateFormat format) {
	    
	    if (format == null) return null;
	    return format.format(date);
	}
	
	public static String formatDatetime(Date date) {
	    
	    return formatDate(date, timeFormat);

	}
	
	public static Timestamp parseTimestamp(String inputDate) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(parseDate(inputDate));
		return new Timestamp(calendar.getTimeInMillis());
	}
	
	public static String getDayOfWeek(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return new Integer(calendar.get(Calendar.DAY_OF_WEEK)).toString();
	}
	
	public static Integer getHourOfDay(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return new Integer(calendar.get(Calendar.HOUR_OF_DAY));
	}

	public static String getHourAndMinuteOfDay(Date date){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		String time = dateFormat.format(date);
		return time;
	}
	
	public static Date getNearDay(Date date, int offset){
		if (offset == 0)
			return date;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int target = offset;
		int step = 1;
		if (target < 0) {
			target *= -1;
			step = -1;
		}
		while(target>0){
			calendar.add(Calendar.DAY_OF_YEAR, step);
			target--;
		}
		return calendar.getTime();
	}
	
	public static Date getNearDateTime(Date date, int offset, int type){
		if (offset == 0)
			return date;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int target = offset;
		int step = 1;
		if (target < 0) {
			target *= -1;
			step = -1;
		}
		while(target>0){
			calendar.add(type, step);
			target--;
		}
		return calendar.getTime();
	}
	
	public static int GetDateDifferenceByType(Date date1, Date date2, int CalendarType){
		Date earlier;
		Date later;
		Boolean switched = false;
		Calendar calendarEarlier = Calendar.getInstance();
		Calendar calendarLater = Calendar.getInstance();
		if (date1.equals(date2)){
			return 0;
		}
		if (date1.after(date2)){
			earlier = date2;
			later = date1;
			switched = true;
		}else{
			earlier = date1;
			later = date2;
			switched = false;
		}
		calendarEarlier.setTime(earlier);
		calendarLater.setTime(later);
		int count = 0;
		while(calendarEarlier.before(calendarLater)){
			calendarEarlier.add(CalendarType, 1);
			count++;
		}
		if (switched == true) {
			count *= -1;
		}
		return count;
	}
	
	public static Date GetNextHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, 1);
		return calendar.getTime();
	}
	
	public static Date GetEndTime(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, 59);
		calendar.add(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static Date getDatePart(Date date) throws ParseException {
	    
	    String dateStr = dateFormat.format(date);
	    return dateFormat.parse(dateStr);
	}
	
	public static Date getBeginDateTime(Date date) {
	    
    	    Calendar cal = Calendar.getInstance();
    	    cal.setTime(date);
    	    cal.set(Calendar.HOUR_OF_DAY, 0);
    	    cal.set(Calendar.MINUTE, 0);
    	    cal.set(Calendar.SECOND, 0);
    	    return cal.getTime();
	}
	
	public static Date getEndDateTime(Date date) {
	    
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static Date getNextHalfHour(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, 30);		
		return calendar.getTime();
	}
}
