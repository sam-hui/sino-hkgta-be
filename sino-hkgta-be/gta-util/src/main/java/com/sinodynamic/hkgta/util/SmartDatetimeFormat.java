package com.sinodynamic.hkgta.util;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Extend from simpleDateFormat, based on the date length to determine datetime and date format 

 */
public class SmartDatetimeFormat extends SimpleDateFormat {  
// @Value("${appProperties['format.datetime']}") String DATETIME; //"yyyy-MM-dd HH:mm:ss";
// @Value("${appProperties['format.date']}") String DATE; //"yyyy-MM-dd";  
// @Value("${appProperties['format.date.jqgrid.default']}") String JQGDATE;//"MMM dd, yyyy h:mm:ss a"; // JQGrid default format   
   
   final String DATETIME = "yyyy-MM-dd HH:mm:ss";
   final String DATE = "yyyy-MM-dd";     
   final String JQGDATE = "MMM dd, yyyy h:mm:ss a"; // JQGrid default format
   
   final SimpleDateFormat sdtime = new SimpleDateFormat(DATETIME);
   final SimpleDateFormat sdf = new SimpleDateFormat(DATE);

   //@Resource(name="appProperties") Properties appProps; // refer to applicationContext-properties.xml & placeholder/app.properties
   public SmartDatetimeFormat() {
      super("yyyy-MM-dd HH:mm:ss");
   }

   public SmartDatetimeFormat(String format) {
//   @Autowired
//   public SmartDatetimeFormat(@Value("${appProperties['format.datetime']}") String format) {
      super(format);     
   }   
   
  
   @Override
   public Date parse(String source, ParsePosition pos) {     
	   System.out.print("smart indate: " + source + "  ");
      //System.out.print("TEST : " + DATETIME + "  ");   
       if (source.length() - pos.getIndex() == DATETIME.length()) {
           System.out.println(DATETIME);
           return sdtime.parse(source, pos);
       } else if (source.length() - pos.getIndex() == DATE.length()) {
          System.out.println(DATE);       
          return sdf.parse(source, pos);
//       } else if (source.length() - pos.getIndex() == JQGDATE.length()) {
//          System.out.println(JQGDATE);       
//          return sdf.parse(source, pos);
       }

       
       return super.parse(source, pos);
   }   

}
