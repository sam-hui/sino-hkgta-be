package com.sinodynamic.hkgta.util.constant;

public enum RefundPeriodType {
    
    	REFPERIOD_GOLFINGBAY,//RefundPeriod Golf Bay
    	REFPERIOD_TENNISCOURT,//RefundPeriod Tennis Court
    	REFPERIOD_GOLFCOACHING,//RefundPeriod Golf Coach
    	REFPERIOD_TENNISCOACHING,//RefundPeriod Tennis Coach
    	REFPERIOD_GOLFCOURSE, //RefundPeriod Golf Period
    	REFPERIOD_TENNISCOURSE, //RefundPeriod Tennis Period
    	REFPERIOD_DAYPASS,//RefundPeriod Purchase daypass
    	REFPERIOD_GUESTROOM;//RefundPeriod Purchase guestroom
}
