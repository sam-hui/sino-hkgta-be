package com.sinodynamic.hkgta.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class CollectionUtil {
	/**
	 * It perform the retainAll method like {@link Collection#retainAll(Collection)}
	 * the only difference is to compare elements through @param elementProperty and @param keptElementProperty
	 * @param elements the elements to be iterated
	 * @param keptElements the elements to be kept
	 * @param elementProperty the left side element property name
	 * @param keptElementProperty the right side element property name
	 * @param callback the callback method to handle deleted elements
	 * @return
	 */
	public static <T1, T2> Collection<T1> retainAll(Collection<T1> elements,Collection<T2> keptElements, String elementProperty, String keptElementProperty, PairCallBack<T1,T2> callback) {
		if(elements == null)
			return null;
		if(keptElements == null){
			return new ArrayList<T1>();
		}
		List<T1> removedItems = new ArrayList<T1>();
		int retainIndex=0;
		for (T1 left : elements){
			BeanWrapper leftWrapper = new BeanWrapperImpl(left);
			Object l = leftWrapper.getPropertyValue(elementProperty);
			boolean isFound = false; 
			for(T2 right : keptElements){
				BeanWrapper rightWrapper = new BeanWrapperImpl(right);
				Object r = rightWrapper.getPropertyValue(keptElementProperty);
				if(l==r){//both null , or the same reference
					isFound = true;
					break;
				}
				
				if(l!=null && l.equals(r)){//if left side element is null , then should be removed
					if(callback!=null){
						callback.execute(left, right, retainIndex, false);
						retainIndex++;
					}
					isFound = true;
					break;
				}
			}
			
			if(!isFound){
				removedItems.add(left);
			}
		}
		
		elements.removeAll(removedItems);
		if(callback != null){
			int i = 0;
			for (T1 removed : removedItems){
				callback.execute(removed, null, i , true);
				i++;
			}
		}
		return elements;
	}
	
	/**
	 * It perform the retainAll method like {@link Collection#retainAll(Collection)}
	 * the only difference is to compare elements through @param elementProperty and @param keptElementProperty
	 * @param elements the elements to be iterated
	 * @param keptElements the elements to be kept
	 * @param elementProperty the left side element property name
	 * @param keptElementProperty the right side element property name
	 * @return
	 */
	public static <T1, T2> Collection<T1> retainAll(Collection<T1> elements,Collection<T2> keptElements, String elementProperty, String keptElementProperty){
		return retainAll(elements, keptElements, elementProperty, keptElementProperty, null);
	}
	
	/**
	 * It perform the removeAll method like {@link Collection#removeAll(Collection)}
	 * the only difference is to compare elements through @param elementProperty and @param deletedElementProperty
	 * @param elements the elements to be iterated
	 * @param deletes the elements to be deleted 
	 * @param elementProperty the left side element property name
	 * @param deletedElementProperty the right side element property name
	 * @param callback the callback method to handle deleted elements
	 * @return
	 */
	public static <T1, T2> Collection<T1> removeAll(Collection<T1> elements,Collection<T2> deletes, String elementProperty, String deletedElementProperty, PairCallBack<T1,T2> callback) {
		if(elements == null)
			return null;
		if(deletes == null){
			return elements;
		}
		List<T1> removedItems = new ArrayList<T1>();
		int removeIndex=0;
		for (T1 left : elements){
			BeanWrapper leftWrapper = new BeanWrapperImpl(left);
			Object l = leftWrapper.getPropertyValue(elementProperty);
			boolean isFound = false; 
			for(T2 right : deletes){
				BeanWrapper rightWrapper = new BeanWrapperImpl(right);
				Object r = rightWrapper.getPropertyValue(deletedElementProperty);
				if(l==r){//both null , or the same reference
					isFound = true;
					break;
				}
				
				if(l!=null && l.equals(r)){//if left side element is null , then should be removed
					if(callback!=null){
						callback.execute(left, right, removeIndex, true);
						removeIndex++;
					}
					isFound = true;
					break;
				}
			}
			
			if(isFound){
				removedItems.add(left);
			}
		}
		
		elements.removeAll(removedItems);
		if(callback != null){
			int i = 0;
			for (T1 retained : elements){
				callback.execute(retained,null, i , false);
				i++;
			}
		}
		
		return elements;
	}
	
	/**
	 * It perform the removeAll method like {@link Collection#removeAll(Collection)}
	 * the only difference is to compare elements through @param elementProperty and @param deletedElementProperty
	 * @param elements the elements to be iterated
	 * @param deletes the elements to be deleted 
	 * @param elementProperty the left side element property name
	 * @param deletedElementProperty the right side element property name
	 * @return
	 */
	public static <T1, T2> Collection<T1> removeAll(Collection<T1> elements,Collection<T2> deletes, String elementProperty, String deletedElementProperty) {
		return removeAll(elements, deletes, elementProperty, deletedElementProperty, null);
	}
	
	
	public static interface CallBack<T,R>{
		R execute(T t, int index);
	}
	
	public static interface NoResultCallBack<T>{
		void execute(T t, int index);
	}
	
	public static interface PairCallBack<T1,T2>{
		void execute(T1 t1, T2 t2 , int index , boolean isRemoved);
	}
	public static <T,R> Collection<R> map(Collection<T> elements, CallBack<T,R> callback){
		if(elements == null)
			return null;
		if(elements.isEmpty())
			return new ArrayList<R>();
		
		int index = 0;
		List<R> result = new ArrayList<R>();
		for (T t : elements){
			result.add(callback.execute(t, index));
			index++;
		}
		return result;
	}
	public static <T,R> Collection<R> map(T[] elements, CallBack<T,R> callback){
		if(elements==null){
			return map(new ArrayList<T>(),callback);
		}
		List<T> list = Arrays.asList(elements);
		return map(list,callback);
	}
	
	public static <T> void loop(Collection<T> elements, NoResultCallBack<T> callback){
		if(elements == null)
			return;
		if(elements.isEmpty())
			return;
		
		int index = 0;
		for (T t : elements){
			callback.execute(t, index);
			index++;
		}
	}
	
	public static <T> void loop(T[] elements, NoResultCallBack<T> callback){
		if(elements==null){
			return;
		}
		List<T> list = Arrays.asList(elements);
		loop(list,callback);
	}
}
