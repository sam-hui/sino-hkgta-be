
package com.sinodynamic.hkgta.util.constant;

public enum RefundServiceType
{
	GMS("GMS","Golf bay"),
	TSS("TSS","Tennis course"),
	GCH("GCH","Golf private coaching"),
	GSS("GSS","Golf course"),
	TMS("TMS","Tennis court"),
	TCH("TCH","Tennis private coaching"),
	CVL("CVL","Cash value refund"),
	SRV("SRV","purchase daypass refund"),
	ROOM("ROOM","Room")
	;
	
	private RefundServiceType(String name, String desc){
		this.name = name;
		this.desc = desc;
	}
	
	private String desc;
	private String name;
	
	public String getDesc(){
		return this.desc;
	}
	
	public String getName()
	{
		return this.name;
	}
}

