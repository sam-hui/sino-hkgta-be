package com.sinodynamic.hkgta.util.constant;

public enum PositionTitle
{
	SP("Sales person"),
	SO("Sales Officer"),
	SRG("Senior Golf Coach"),
	SRT("Senior Tennis Coach"),
	GC("Golf Coach"), 
	TC("Tennis Coach");
	
    private String desc;
	
	private PositionTitle(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}

