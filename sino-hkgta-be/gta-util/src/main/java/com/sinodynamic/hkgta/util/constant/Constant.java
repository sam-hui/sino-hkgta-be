
package com.sinodynamic.hkgta.util.constant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Constant {

	// NoticeType for email
	public static final String NOTICE_TYPE_PRESENTATION = "P";
	public static final String NOTICE_TYPE_ENROLLMENT = "E";
	public static final String NOTICE_TYPE_RENEWAL = "R";
	public static final String NOTICE_TYPE_TRANSACTION = "T";
	public static final String NOTICE_TYPE_EXTENSION = "X";
	public static final String NOTICE_TYPE_TEMP = "M";
	public static final String NOTICE_TYPE_STAFF = "S";
	public static final String NOTICE_TYPE_FACILITY = "F";
	public static final String NOTICE_TYPE_CENTER = "CENTER";
	public static final String NOTICE_TYPE_COURSE = "COURSE";
	public static final String NOTICE_TYPE_WELLNESS = "wellness";
	
	// Service Plan offer code
	public static final String SERVICE_PLAN_OFFER_CODE_RENEWAL = "RENEW";
	public static final String SERVICE_PLAN_TYPE_RENEWAL = "Renewal";
	public static final String SERVICE_PLAN_TYPE_ENROLLMENT = "Enrollment";

	// permit card master status literal
	public static final String PERMIT_CARD_MASTER_OH = "OH";
	public static final String PERMIT_CARD_MASTER_ISS = "ISS";
	public static final String PERMIT_CARD_MASTER_CAN = "CAN";
	public static final String PERMIT_CARD_MASTER_LOST = "LOST";
	//daypass
	public static final String ENROLL_PRICE_PREFIX_DP = "DP_000000";
	public static final String ENROLL_PRICE_ITEM_PREFIX_DP = "DP_";
	public static final String SRV_HI_PREFIX_DP = "DP_HI000000";
	public static final String SRV_HI_RATE_ITEM_PREFIX_DP = "DP_HI";
	//serviceplan
	public static final String ENROLL_PRICE_PREFIX = "SRV000000";
	public static final String ENROLL_PRICE_ITEM_PREFIX = "SRV";
	public static final String RENEW_PRICE_PREFIX = "RENEW000000";
	public static final String RENEW_PRICE_ITEM_PREFIX = "RENEW";
	public static final String TOPUP_ITEM_NO = "CVT0000001";
	public static final String SRV_REFUND_ITEM_NO = "SRR00000001";
	public static final String SRV_HI_PREFIX = "SRVHI000000";
	public static final String SRV_HI_RATE_ITEM_PREFIX = "SRVHI";
	public static final String GSSHI_RATE_ITEM_PREFIX = "GSSHI";
	public static final String TSSHI_RATE_ITEM_PREFIX = "TSSHI";
	public static final String GSSLO_RATE_ITEM_PREFIX = "GSSLO";
	public static final String TSSLO_RATE_ITEM_PREFIX = "TSSLO";
	public static final String FACILITY_PRICE_PREFIX = "FMS";
	public static final String PMS_ROOMTYPE_ITEM_PREFIX = "PMS";
	public static final String GOLF_COURSE_PRICE_PREFIX = "GSSCOS";
	public static final String TENNIS_COURSE_PRICE_PREFIX = "TSSCOS";
	public static final String RENEW_ITEM_NO_PREFIX = "RENEW";

	public static final String ENROLL_PRICE_DESC_SUFFIX = " - Enrollment";
	public static final String RENEW_PRICE_DESC_SUFFIX = " - Renewal";
	public static final String SPECIAL_PRICE_DESC_SUFFIX = " - Special";

	public static final String TRANSACTION_BALANCEDUE_ALL = "All";

	public static final String TRANSACTION_BALANCEDUE_PENDING = "PENDING";// PendingPayment
	public static final String TRANSACTION_BALANCEDUE_COMPLETED = "COMPLETED";// CompletedPayment

	public static final String DELETE_NO = "N";
	public static final String DELETE_YES = "Y";
	
	public static final String NO = "N";
	public static final String YES = "Y";
	public static final String BLOCK = "B";

	public static final String Member_Status_ACT = "ACT";
	public static final String Member_Status_NACT = "NACT";

	public static final String General_Status_ACT = "ACT";
	public static final String General_Status_NACT = "NACT";
	public static final String General_Status_EXP = "EXP";
	
	public static final String HK_PUBLIC_HOLODAYS_URL="hk_government_holidays_ical_url";

	// SFTP literal define
	public static final String SFTP_HOST = "hkgta.virtual.account.sftp.host";
	public static final String SFTP_PORT = "hkgta.virtual.account.sftp.port";
	public static final String SFTP_USERNAME = "hkgta.virtual.account.sftp.username";
	public static final String SFTP_PASSWORD = "hkgta.virtual.account.sftp.password";
	public static final String SFTP_RECEIVE_PATH = "hkgta.virtual.account.sftp.receive.path";
	public static final String SFTP_RECEIVE_BACK_PATH = "hkgta.virtual.account.sftp.receive.back.path";
	public static final String SFTP_TIMEOUT = "hkgta.virtual.account.sftp.timeout";
	public static final String SFTP_SNED_PATH = "hkgta.virtual.account.sftp.upload.path";
	public static final int SFTP_DEFAULT_PORT = 22;
	public static final int SFTP_DEFAULT_TIMEOUT = 60000;
	
	

	// CSV file name format
	public static final String CSV_UPLOAD_PATH = "csv.upload.path";
	public static final String CSV_UPLOAD_BACK_PATH = "csv.upload.back.path";
	public static final String CSV_DOWNLOAD_PATH = "csv.download.path";
	public static final String CSV_DOWNLOAD_BACK_PATH = "csv.download.back.path";

	// search sysconfig from global_parameter
	public static final String CLIENT_ID = "ClientID";
	public static final String PHYSICAL_ACCOUNT = "PhysicalAccount";
	public static final String RECORD_CODE = "RecordCode";
	public static final String PAYER_NAME = "PayerName";

	public static final String ROLE_OFFICER = "Manager";
	public static final String ROLE_SALES = "SALES";

	public static final String ADVANCEDRESPERIOD_GOLF = "GolfingBayAdvancedResPeriod";
	public static final String ADVANCEDRESPERIOD_TENNIS = "TennisCourtAdvancedResPeriod";
	public static final String FACILITY_TYPE_GOLF = "GOLF";
	public static final String FACILITY_TYPE_TENNIS = "TENNIS";


	// sort by column for contractor help
	public static final String COL_TEMP_USER = "tempUser";
	public static final String COL_PASS_TYPE = "temporaryPassType";
	public static final String COL_CARD_ID = "cardId";
	public static final String COL_ACT_DATE = "activationDate";
	public static final String COL_DEACT_DATE = "deactivationDate";
	public static final String COL_REFERRAL = "referral";
	public static final String COL_STATUS = "status";

	public static final String LOGIN_FAIL_TRY_TIME = "login.failed.try.times";
	public static final String LOGIN_FAIL_TRY_TIME_LOCK = "login.failed.try.times.lock";
	public static final String LOGIN_FAIL_TRY_PERIOD = "login.failed.try.period";
	public static final String LOGIN_FAIL_LOCK_PERIOD = "login.failed.lock.period";
	public static final String LOGIN_DEVICE_CHECK_LIST = "login.device.check.list";
	public static final String LOCATION_CHECK_DEVICE_LIST = "location.check.device.list";

	public static final String BALLFEEDSWITCH = "BALLFEEDSWITCH";
	public static final int MAX_FACILITY_ADVANCEDRESPERIOD = 1000;
	public static final int MAX_FACILITY_ITEMPRICE = 1000000;

	// sort by column for course list
	public static final String COL_COURSE_ID = "courseId";
	public static final String COL_COURSE_NAME = "courseName";
	public static final String COL_REG_DATE = "regDate";
	public static final String COL_COURSE_PERIOD = "periodStart";
	public static final String COL_COURSE_STATUS = "status";
	public static final String COL_ENROLLMENT = "enrollment";

	public static final String GOLFBAYTYPE = "GOLFBAYTYPE";
	public static final String TENNISCRT = "TENNISCRT";
	public static final String GOLFBAYTYPE_CAR = "GOLFBAYTYPE-CAR";
	
	public static final String ZONE_ATTRIBUTE = "ZONE";
	
	public static final String RESV_TYPE = "resvType";
	
	public static final String RESV_TYPE_PRIVATE_COACHING = "Private Coaching";

	public static final String RESV_TYPE_MEMBER_BOOKING = "Member Booking";
	
	public static final String RESV_TYPE_GUEST_ROOM_BUNDLED = "Guest room bundled";

	public static final String RESV_TYPE_MAINTENANCE_OFFER = "Maintenance Offer";

	public static final String RESV_TYPE_COURSE = "Course";
	
	public static final String COURSE_NAME = "courseName";

	public static final String COACH_NAME = "coachName";
	public static final String HOUSEKEEP_PUSHMESSAGE_ASSIGNED = "taskAssigned";
	public static final String HOUSEKEEP_PUSHMESSAGE_UPDATED = "taskUpdated";
	public static final String HOUSEKEEP_PUSHMESSAGE_EXPIRED = "taskExpired";
	public static final String HOUSEKEEP_PUSHMESSAGE_COMPLETED = "taskCompleted";
	public static final String HOUSEKEEP_PUSHMESSAGE_REASSIGNED = "taskReassigned";
	public static final String HOUSEKEEP_PUSHMESSAGE_CANCELED = "taskCanceled";
	public static final String HOUSEKEEP_PUSHMESSAGE_DND = "DNDState";
	public static final String HOUSEKEEP_PUSHMESSAGE_CHANGESTATUS = "statusChanged";
	
	public static final String UPDATE_CUSTOMER_PROFILE_SMS_TEMPLETE = "Your profile is updated --HKGTA";
	public static final String UPDATE_CUSTOMER_STATUS_SMS_TEMPLETE = "Your Member status is updated --HKGTA";
	public static final String UPDATE_CUSTOMER_CREDIT_LIMIT_SMS_TEMPLETE = "Your credit limit is updated --HKGTA";
	public static final String UPDATE_CUSTOMER_SPENING_LIMIT_SMS_TEMPLETE = "Your spending limit is updated --HKGTA";
	
	public static final String SALESKIT_PUSH_APPLICATION = "SalesKit";
	public static final String STAFFAPP_PUSH_APPLICATION = "StaffApp";
	
	public static final int THREAD_NUM = 3;
	
	public static final String MEMBER_SYNCHRONIZE_INSERT = "INSERT";
	public static final String MEMBER_SYNCHRONIZE_UPDATE = "UPDATE";
	public static final String STATUS_WAITING_TO_SYNCHRONIZE = "OPN";
	public static final String STATUS_FINISHED_SYNCHRONIZE = "CMP";
	
	public static final String RESTAURANT_BOOK_VIA_OL = "OL";
	public static final String RESTAURANT_BOOK_VIA_FD = "FD";
	public static final String RESTAURANT_BOOK_VIA_PH = "PH";
	
	public static ExecutorService getThreadPool(){
		return Executors.newFixedThreadPool(THREAD_NUM);
	}
	
	public static ExecutorService getThreadPool(Integer num){
		return Executors.newFixedThreadPool(num);
	}
	
	public enum PriceCatagory {
		Enrollment, RENEW, SRV, TOPUP, REFUND, PMS_ROOM,DP
	}

	public enum RateType {
		HI("HI"),
		LO("LO"),
		OFF("OFF"),
		BLK("BLK");
		
		private RateType(String name)
		{
			this.name = name;
		}
		
		private String name;
		
		public String getName()
		{
			return this.name;
		}
	}
	
	public enum StaffType {
		FTR("Tennis Coach"),
		FTG("Golf Coach"),
		HK("Housekeep"),
		CONCG("Concierge"),
		MGNT("Management"),
		SALESMBR("Sales(membership/Service)"),
		RETAIL("Sales(Retail)"),
		WTR("Waiter"),
		HR("Human Resources"),
		NGR("Engineer"),
		BDA("Bell/Door attendant"),
		SEC("Security"),
		LNDSCP("Landscape"),
		ACC("Account/Audit"),
		IT("IT"),
		PROC("Procurement"),
		CUSTS("Customer service"),
		DRV("Driver"),
		RFE("Recreation Facilities & Events"),
		OTH("OTHER(general staff)");
		
		public String getTypeName()
		{
			return typeName;
		}

		private String typeName;

		private StaffType(String typeName)
		{
			this.typeName = typeName;
		}
	}
	
	

	public enum Purchase_Daypass_Type {
		Staff, Member
	}

	public enum Purchase_Flag_Type {
		OK, FAIL
	}
	
	public enum Subscriber_Type {
		STF, M
	}
	
	public enum Status {
		ACT, NACT,
		//for table service_plan
		//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled
		EXP,
		// for table customer_order_hd. OPN-open, CAN-cancelled, CMP-paid and
		// transaction completed, REJ - Rejected
		OPN, CAN, CMP, REJ,
		// for table customer_order_permit_card. ACT-activated; RA - ready to
		// activate (card_no not mapped yet); PND-pending
		RA, PND,
		// for table customer_order_trans. NULL/empty-paid but not confirmed;
		/*
		 *  NULL/empty-paid but not confirmed; SUC - payment success; FAIL - payment not success; 
		 *  VOID- paid and success but cancelled finally; PND-pending for transaction (oline payment gateway) ; 
		 *  RFU-refund; PR-paid but need to refund\n
		 */
		// success but cancelled finally \n
		SUC, FAIL, VOID,
		RFU,PR, // refund
		//for table staff_timeslot OP-Occupied; LV-on leave; SL-sick leave
		OP,LV,SL,
		//for member_facility_type_booking RSV-resevered; ATN-attended;  NAT-not attended; CAN-cancel booking
		RSV,ATN,NAT,
		//course Absent or Absented
		ABS,ATD,
		//for table permit_card_master OH- onhand; ISS-issued; CAN-cancelled; DPS-Disposal
		OH,ISS,DPS,CFM
	}
	public enum CustomerRefundRequest {
		/*
		 * for table customer_refund_request 
		 * FMS - facility; SRV-service plan; TSS-tennis course; GSS-golf course
		 * OPN - open case; APR-approved; REJ- rejected; PND-pending; NRF-no refund
		 * CASH-cash; CASHVALUE-cash value
		 */
		
		FMS("FMS","facility"),
		SRV("SRV","service plan"),
		TSS("TSS","tennis course"),
		GSS("GSS","golf course"),
		OPN("OPN","open case"),
		APR("APR","approved"),
		REJ("REJ","rejected"),
		PND("PND","pending"),
		NRF("NRF","no refund"),
		CASH("CASH","cash"),
		CV("CV","cash value"),
		;
		
		private CustomerRefundRequest(String name, String desc){
			this.name = name;
			this.desc = desc;
		}
		
		private String desc;
		private String name;
		
		public String getDesc(){
			return this.desc;
		}
		
		public String getName()
		{
			return this.name;
		}
	}
	public enum duty_category{
		//for table staff_timeslot NULL-no category; PVCOACH-private coach; TSS - Trainer for a tennis course; GSS - Trainer for a golf course; CLNROOM- Room cleaning; MEET-meeting; .....etc
		PVTCOACH,PVGCOACH,TSS,GSS,CLNROOM,MEET
	}
	
	public enum  reserve_via{
		//for table member_facility_type_booking reservation method: APS-mobile apps; WP-web portal; FD-front desk request
		APS,WP,FD
	}
	public enum AnnualRepeat {
		Y, N
	}

	public enum ServiceplanType {
		SERVICEPLAN, DAYPASS
	}

	public enum memberType {
		/*
		 * IPM – Individual Primary Member;IDM – Individual Dependent Member;
		 * CPM – Corporate Primary Member; CDM – Corporate Dependent Member; MG
		 * – Member Guest;HG – House Gues
		 */
		IPM, IDM, CPM, CDM, MG,HG
	}
	
	// spring security authorization filter access right for specific
	// url/program_master
	public static final String SECURITY_ACCESS_RIGHT_ALL = "*";
	public static final String SECURITY_ACCESS_RIGHT_READ = "R";
	public static final String SECURITY_ACCESS_RIGHT_CREATE_UPDATE = "U"; // create
																			// +
																			// update
																			// +
																			// read;
																			// all(*)
																			// except
																			// delete.
	// public static final String SECURITY_ACCESS_RIGHT_DELETE = "D"; //
	// equivalent to ALL(*)
	public static final String DAY_PASS_PURCHASING = "Day Pass Purchasing";

	// payment account type
	public static final String VIRTUAL_ACCOUNT = "VA";
	public static final String BANK_ACCOUNT = "BNK";

	// topup method type
	public static final String VIRTUAL = "VA";
	public static final String CASH = "CASH";
	public static final String CASH_Value = "CV";
	public static final String CREDIT_CARD = "VISA";
	public static final String PRE_AUTH = "PREAUTH";

	// virtual acc status
	// AV-available; US-used; NR-Not ready; DEL-deleted
	public static final String VIR_ACC_STATUS_VA = "VA";
	public static final String VIR_ACC_STATUS_US = "US";
	public static final String VIR_ACC_STATUS_NR = "NR";
	public static final String VIR_ACC_STATUS_DEL = "DEL";

	// member payment account status
	public static final String PAYMENT_ACC_STATUS_ACT = "ACT";
	public static final String PAYMENT_ACC_STATUS_NACT = "NACT";

	// staff timeslot status
	// OP-Occupied; LV-on leave; SL-sick leave
	public static final String STAFF_TIMESLOT_OP = "OP";
	public static final String STAFF_TIMESLOT_LV = "LV";
	public static final String STAFF_TIMESLOT_SL = "SL";

	// staff timeslot dutyCategory
	// NULL-no category; TRAINER-Trainer for a course; CLNROOM- Room cleaning;
	// MEET-meeting; .....etc
	public static final String STAFF_TIMESLOT_TRAINER = "TRAINER";
	public static final String STAFF_TIMESLOT_CLNROOM = "CLNROOM";
	public static final String STAFF_TIMESLOT_MEET = "MEET";

	// staff timeslot dutyDescription
	public static final String STAFF_TIMESLOT_CATEGORY_DESCRIPTION = "Primary TRAINER";
	
	public static final String COL_CREATE_DATE = "createDate";
	
	// Common placeholder for userName and fromName in email content
	public static final String PLACE_HOLDER_FROM_USER = "{fromname}";
	public static final String PLACE_HOLDER_TO_CUSTOMER = "{username}";
	public static final String HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER = "HKGTA";

	public static final String INTERNAL_REMARK_REFUND = "refund to cash value";
	
	public static final int REFUND_PERIOD_DEFAULT_HOURS = 96;
	
	public static final String CATEGORY_4_SECURITY_QUESTION = "pwdquestion";
	
	public static final String SYSCODE_CREW_ROLE = "HK-CREW-ROLE";

	public static final String SYSCODE_STAFF_TYPE = "staffType";
	
	public static final String ZONE_COACHING = "ZONE-COACHING";
	
	public static final String ZONE_PRACTICE = "ZONE-PRACTICE";
	
	/**
	 * private coach training
	 */
	public static final String RESERVE_TYPE_PC = "PC";
	
	/**
	 * course session
	 */
	public static final String RESERVE_TYPE_CS = "CS";
	
	/**
	 * member reserve
	 */
	public static final String RESERVE_TYPE_MR = "MR";
	
	/**
	 * maintenance
	 */
	public static final String RESERVE_TYPE_MT = "MT";
	
	public enum MessageTemplate {
		
		//common 
		Letf_Symbol("{"),
		Right_Symbol("}"),
		MemberEmail("(?i)Member’s Email"),
		Salutation("(?i)Salutation"),
		FirstName("(?i)Member First Name"),
		LastName("(?i)Member Last Name"),
		FullName("(?i)\\{Member Full Name\\}"),
		//for Private Coaching Reservation Confirmation Template begin
		/*
		To: <Member’s Email>
		Dear <Salutation><Member’s Last Name>,
		Confirmation ID: {Confirmation #}   -- orderNo
		Reserved Person: <Member’s Full Name>
		Reservation Period: {Start Time} to {End Time}
		Coach: <Coach Full Name>
		Facility Name: <Facility’s Name>
		Tennis Court Type: <Tennis Court Type>
		
		Bay Type: <Bay Type>
		 */
		Confirmation("(?i)\\{Confirmation #\\}"),
		StartTime("(?i)\\{startTime\\}"),
		EndTime("(?i)\\{endTime\\}"),
		CoachName("(?i)Coach Full Name"),
		FacilityName("(?i)FacilityName"),
		TennisType("(?i)Tennis Court Type"),
		BayType("(?i)Golf Bay Type"),
		//for Private Coaching Reservation Confirmation Template end
		
	
		
		
		//for private coach auto push message begin 
		BookingDate("(?i)bookingDate"),
		//for private coach auto push message end
		
		TimeLeft("(?i)timeLeft"),
		UserName("(?i)\\{username\\}"),
		TopupMethod("(?i)\\{topupMethod\\}"),
		CurrentBalance("(?i)\\{currentBalance\\}"),
		
		//for purchase daypass
		/**
		Dear {username},
		This is to confirm your reservation request. Please find your reservation details below. We have approved your payment and you can find the receipt in the attachment. For reservation cancelation, please go to HKGTA front-desk or via Service Hotline.
		
		Order ID:{Confirmation #}
		Reserved Date:{Start Time} to {End Time}
		No of Day Pass: {no of day pass}
		Day Pass Type: {type}
		If you have any questions, please call us. We are looking forward to your arrival.
		Best Regards,
		Sales & Marketing Office of HKGTA
		 */
		Type("(?i)\\{type\\}"),
		OffPass("(?i)\\{no of day pass\\}"),
		;
		
		private MessageTemplate(String name)
		{
			this.name = name;
		}
		
		private String name;
		
		public String getName()
		{
			return this.name;
		}
	}
	
	public static final String REFUND_SERVICE_REMARK = "Refund for service";
	
	public static final String REFUND_CASH_VALUE_REMARK = "Refund for cashvalue";
	
	public static final String REFUND_ITEM_NO = "CVR00000001";
	public static final String REFUND_SERVICE_ITEM_NO = "SRR00000001";
	
	public enum PaymentMethodCode{
		VISA("VISA"), MASTER("MASTER"), CASH("CASH"), CASHVALUE("CV"), CHEQUE("CHEQUE"), UPAY("UPAY"), REFUND("REFUND");
		private String desc;
		private PaymentMethodCode(String desc){
			this.desc = desc;
		}
		@Override
		public String toString(){
			return desc;
		}
	}
	
	public enum BizParties{
		CUSTOMER, HKGTA
	}
	
	public enum UserType{
		CUSTOMER, ADMIN, STAFF
	}
	
	public enum AdvertiseAppType { // advertise_image.application_type
		MAPP, MPORT
	}
	
	public enum AdvertiseDispLoc { // advertise_image.display_location
		MSCR, TOPUP
	}
	
	public enum ServiceItem{

		GFBK("Golfing Bay"), TFBK("Tennis Court"), TS("Tennis Private Coaching"), GS("Golf Private Coaching"),SRV("Service Plan"),DP("Day Pass Purchasing"), 
		GSS("Golf Course Enrollment"), TSS("Tennis Course Enrollment"), REFUND("Refund"),TOPUP("Top up"),PMS_ROOM("Guest Room"),MMS_SPA("Wellness Center");
		private String desc;
		private ServiceItem(String desc){
			this.desc = desc;
		}
		@Override
		public String toString(){
			return desc;
		}
	}
	
	public enum RoomReservationStatus
	{
		SUC, //both oasis system and our system successfully pay the reservation
		IGN, // ?
		CAN, //those order not yet paid by our system , and cancel manually or automatically
		RSV, //the initial status
		PAY, //paid by oasis system, not yet paid by our system
		EXP, // ?
		RFU  //both oasis system and our system successfully pay the reservation, and cancel manually
	}
	
	public enum RoomFrontdeskStatus
	{
		O,V,OOO
	}

	public static final String PROGRAM_SALES_REPORT_OVERVIEW = "m_salesreport_overview";
	public static final String PROGRAM_TYPE_MENU = "M";
	public static final String PROGRAM_TYPE_REPORT = "R";

	public enum Terminal{
		APP, //iPhone , Android, WindowsPhone ...
		WEB  //Website
	}

	public enum AppType{
		STAFF, //For HKGTA all staff(front desk, admin , and so on)
		MEMBER //For member who use HKGTA service
	}
	
	public enum RepeatMode{
		ONCE, DAILY, WEEKLY, MONTHLY
	}
}
