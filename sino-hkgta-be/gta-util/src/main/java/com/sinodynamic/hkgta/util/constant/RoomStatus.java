package com.sinodynamic.hkgta.util.constant;

public enum RoomStatus {
	
	 	D("VACANT_DIRTY"),R("READY_TO_INSPECT"),OC("OCCUPIED_CLEAN"),VC("VACANT_CLEAN");
	
		private String desc;

		private RoomStatus(String desc) {
			this.desc = desc;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

