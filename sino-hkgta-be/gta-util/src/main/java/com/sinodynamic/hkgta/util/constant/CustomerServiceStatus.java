package com.sinodynamic.hkgta.util.constant;

public enum CustomerServiceStatus {
	
	
	
	NACT("NACT"), ACT("ACT"), EXP("EXP"), PND("PND");
	
	private String desc;

	private CustomerServiceStatus(String d) {
		this.desc = d;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
