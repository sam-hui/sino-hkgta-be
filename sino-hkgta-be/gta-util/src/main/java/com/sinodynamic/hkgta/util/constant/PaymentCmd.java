package com.sinodynamic.hkgta.util.constant;

public enum PaymentCmd {
	PAY("PAY"),QUERYDR("QUERYDR"),REFUND("REFUND"),VOIDPURCHASE("VOIDPURCHASE"),CAPTURE("CAPTURE");
	private String desc;

	private PaymentCmd(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
