package com.sinodynamic.hkgta.util;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.validator.internal.util.privilegedactions.NewInstance;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.sinodynamic.hkgta.util.constant.AccessBatCmdType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.CommonException;

public class AccessDatabaseUtil {

	private static Logger logger = Logger.getLogger(AccessDatabaseUtil.class);
	
	private final static String Access_MDB_PATH = "access.path";
	
	private final static String TABLE_BATCMD = "BatCmd";
	
	private final static String TABLE_BATINFO = "BatInfo";
	
	private static File accessFile = null;
	
	private static SmartDatetimeFormat smartDatetimeFormat = new SmartDatetimeFormat();
	
	public static File getAccessFile()throws IOException{
		
		if(null != accessFile){
			return accessFile;
		}
		Resource resource = new ClassPathResource("placeholder/app.properties");
		Properties appProps = PropertiesLoaderUtils.loadProperties(resource);
		String path = appProps.getProperty(Access_MDB_PATH);
		accessFile = new File(path);
		return accessFile;
	}

	
	public static Boolean changeTeeupMachineStatsbyBatNo(Long batNo, String ballFeedingStatus, File accessFile)throws IOException{
		
		try {
			Database database = new DatabaseBuilder().open(accessFile);
			Table cmdtable = database.getTable(TABLE_BATCMD);
			String currentTime = smartDatetimeFormat.format(new Date());
			if (ballFeedingStatus.equals("OFF") ) {
				cmdtable.addRow(currentTime, batNo, currentTime, 0, 0, 0,
						AccessBatCmdType.Finish.getCmd());
				} 
			else {
				cmdtable.addRow(currentTime, batNo, currentTime, 9999, 9999, 0,
						AccessBatCmdType.Checkin.getCmd());
				}
			database.flush();
			database.close();
			return true;

		} catch (IOException ex) {
			logger.error(ex.getMessage());
			ex.printStackTrace();
			return false;
			// TODO: handle exception
		}	
	}
	
	public static void setTeeupMachineStatsbyCheckinTime(Long batNo, Date startTime, Date endTime) throws Exception {
		
		File accessFile = AccessDatabaseUtil.getAccessFile();
		if(!accessFile.exists() || accessFile.isDirectory()){
			throw new CommonException(GTAError.FacilityError.ACCESSDATABASE_ISNULL,"");
		}	
		Database database = DatabaseBuilder.open(accessFile);
		Table cmdTable = database.getTable(TABLE_BATCMD);
		Date currentTime = new Date();
		String currentTimeStr = smartDatetimeFormat.format(currentTime);
		Long timeDiff;

		if (currentTime.after(startTime)) {
			timeDiff = endTime.getTime() - currentTime.getTime();
			Long min = timeDiff / (1000 * 60);
			cmdTable.addRow(currentTimeStr, batNo, currentTimeStr, min, 9999, 0, AccessBatCmdType.Checkin.getCmd());
		} 
		else {
			timeDiff = endTime.getTime() - startTime.getTime();
			Long min = timeDiff / (1000 * 60);
			String startTimeStr = smartDatetimeFormat.format(startTime);
			cmdTable.addRow(currentTimeStr, batNo, startTimeStr, min, 9999, 0, AccessBatCmdType.Checkin.getCmd());
		}

		database.flush();
		database.close();

	}
	
	public static Boolean setTeeupMachineCommand(Long batNo, String startTime, Long timeCount, Long ballCount, Long useType, Long cmdType, File accessFile)throws IOException{
		
		try {
			Database database = new DatabaseBuilder().open(accessFile);
			Table cmdTable = database.getTable(TABLE_BATCMD);
			String currentTime = smartDatetimeFormat.format(new Date());
			cmdTable.addRow(currentTime, batNo, startTime, timeCount,
					ballCount, useType, cmdType);
			database.flush();
			database.close();
			return true;
		} catch (IOException ex) {
			// TODO: handle exception
			logger.error(ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}
	
	public Map<String, String> getTeeupMachineStatus()throws Exception{
		
		
		File accessFile = AccessDatabaseUtil.getAccessFile();
		if(!accessFile.exists() || accessFile.isDirectory()){
			throw new CommonException(GTAError.FacilityError.ACCESSDATABASE_ISNULL,"");
		}	
		Database database = DatabaseBuilder.open(accessFile);
		Table cmdTable = database.getTable(TABLE_BATINFO);
		Map<String, String> statsMap = new HashMap<String, String>();
		
		for(Row row : cmdTable) {
			statsMap.put(row.get("BatNo").toString(), row.get("BatStat").toString());
		}
		
		return statsMap;
	}
}
