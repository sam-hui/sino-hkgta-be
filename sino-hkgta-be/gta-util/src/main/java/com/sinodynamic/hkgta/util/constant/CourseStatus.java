package com.sinodynamic.hkgta.util.constant;

/**
 * @author Miranda_Zhang
 *
 */
public enum CourseStatus {	
	OPEN("OPN"),
	CLOSE("CLD"),
	FULL("FUL"),
	CANCEL("CAN");
		
	private CourseStatus(String type){
		this.type = type;
	}
	
	private String type;
	
	public String getType(){
		return this.type;
	}
	
	@Override
	public String toString() {
		return this.type;
	}
}
