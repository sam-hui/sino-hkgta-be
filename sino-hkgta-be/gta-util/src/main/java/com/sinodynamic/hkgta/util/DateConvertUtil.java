package com.sinodynamic.hkgta.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;

public class DateConvertUtil {
	public static Date d = new Date();
	public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String defaultformat = "yyyy/MM/dd";

	public static String getStrFromDate(Date d) {
		df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = df.format(d);
		return strDate;
	}

	public static String getYMDFormatDate(Date d) {
		df = new SimpleDateFormat("yyyy-MM-dd");
		if (d == null)
			return null;
		return df.format(d);
	}

	/**
	 * @author Junfeng_Yan
	 * @param d
	 * @param format
	 *            such as "yyyy-MM-dd or yyyy/MM/dd"
	 * @return
	 */
	public static String date2String(Date d, String format) {
		if (null == d)
			return "";
		if (StringUtils.isEmpty(format))
			format = defaultformat;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(d);
	}

	public static Date getDateFromStr(String strDate) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return dateFormat.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @author Vineela_Jyothi This method returns the no.of days/weeks/months
	 *         between the given date and today. If the given date is after
	 *         today and no.of days in between <30, returns --> 'xxd left' (xx -
	 *         no.of days) If the given date is after today and no.of days in
	 *         between >30 & <90, returns --> 'xxw left' (xx - no.of weeks) If
	 *         the given date is after today and no.of days in between >90,
	 *         returns --> 'xxm left' (xx - no.of months) If the given date is
	 *         before today and no.of days in between <30, returns --> 'xxd ago'
	 *         (xx - no.of days) If the given date is before today and no.of
	 *         days in between >30 & <90, returns --> 'xxw ago' (xx - no.of
	 *         weeks) If the given date is before today and no.of days in
	 *         between >90, returns --> 'xxm ago' (xx - no.of months) Else
	 *         returns (Today)
	 * @param givenDate
	 * @return no.of days/weeks/months
	 */
	public static String getNoOfDays(Date givenDate) {

		if (null == givenDate)
			return "";

		Date today = new Date();
		DateTime start = new DateTime(givenDate);
		DateTime end = new DateTime(today);

		int noOfDays = Days.daysBetween(start.withTimeAtStartOfDay(),
				end.withTimeAtStartOfDay()).getDays();
		/*
		 * System.out.println("No.of days: "+noOfDays); String returnString =
		 * "";
		 * 
		 * if(noOfDays > 0){
		 * 
		 * if(noOfDays < 30){ returnString = String.valueOf(noOfDays)+"d ago";
		 * 
		 * }else if (noOfDays >= 30 && noOfDays < 90) { returnString =
		 * String.valueOf(noOfDays/7)+"w ago";
		 * 
		 * }else if (noOfDays >= 90) { returnString =
		 * String.valueOf(noOfDays/30)+"m ago";
		 * 
		 * }
		 * 
		 * }else if(noOfDays < 0){ noOfDays = Math.abs(noOfDays);
		 * 
		 * if(noOfDays < 30){ returnString = String.valueOf(noOfDays)+"d left";
		 * 
		 * }else if (noOfDays >= 30 && noOfDays < 90) { returnString =
		 * String.valueOf(noOfDays/7)+"w left";
		 * 
		 * }else if (noOfDays >= 90) { returnString =
		 * String.valueOf(noOfDays/30)+"m left";
		 * 
		 * }
		 * 
		 * }else{ return "(Today)";
		 * 
		 * }
		 * 
		 * return "("+returnString+")";
		 */
		if (noOfDays < 0 && noOfDays > -30) {
			return "(" + Math.abs(noOfDays) + "d left)";
		} else if (noOfDays > -90 && noOfDays <= -30) {
			return "(" + Math.abs(noOfDays) / 7 + "w left)";
		} else if (noOfDays <= -90) {
			return "(" + Math.abs(noOfDays) / 30 + "m left)";
		} else if (noOfDays == 0) {
			return "(Today)";
		} else if (noOfDays > 0 && noOfDays < 30) {
			return "(" + noOfDays + "d ago)";
		} else if (noOfDays >= 30 && noOfDays < 90) {
			return "(" + noOfDays / 7 + "w ago)";
		} else {
			return "(" + noOfDays / 30 + "m ago)";
		}
	}
	
	/**
	 * Used for tablet to show the time difference
	 * @author Liky_Pan
	 * @param givenDate
	 * @return A formatted string
	 */
	public static String getTimeDifferenceForPast(Date givenDate) {

		if (null == givenDate)
			return "";
		Date today = new Date();
		DateTime start = new DateTime(givenDate);
		DateTime end = new DateTime(today);

		int noOfMins = Minutes.minutesBetween(start,
				end).getMinutes();
		if (noOfMins == 0) {
			return "Now";
		}else if(noOfMins>0&&noOfMins<60){
			return noOfMins+" Min(s)";
		}else if(noOfMins>=60&&noOfMins<1440){
			return noOfMins/60+" Hour(s)";
		}else if(noOfMins>=1440&&noOfMins<43200){
			return noOfMins/1440+" Day(s)";
		}else if(noOfMins>=43200&&noOfMins<525600){
			return noOfMins/43200+" Month(s)";
		}else{
			return noOfMins/525600+ " Year(s)";
		}
	}
	
	/**
	 * Used for calculating the remaining time for my booking
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return A formatted string
	 */
	public static String calcRemainingTime(Date beginDate, Date endDate) {
		String remainingTime = "";
		if (beginDate == null || endDate == null) {
			return remainingTime;
		}
		Date todayDate = new Date();
		DateTime start = new DateTime(beginDate);
		DateTime end = new DateTime(endDate);
		DateTime today = new DateTime(todayDate);

		// noOfMinsStart>0, (start>today) for the Date Left Part; <=0,
		// (start<=today) for Ongoing
		int noOfMinsStart = Minutes.minutesBetween(today, start).getMinutes();
		// noOfMinsEnd>0, (today>end) for Complete; <=0, (today<=end) for
		// Ongoing
		int noOfMinsEnd = Minutes.minutesBetween(end, today).getMinutes();

		int hour = 60;
		int day = hour * 24;// 1440
		int month = day * 30;// 43200
		int year = day * 365;// 525600

		if (noOfMinsStart > 0) {
			if (noOfMinsStart > 0 && noOfMinsStart < hour) {
				remainingTime = noOfMinsStart + (noOfMinsStart == 1 ? " min" : " mins");
			} else if (noOfMinsStart >= hour && noOfMinsStart < day) {
				Integer hrCount = noOfMinsStart / hour;
				remainingTime = hrCount + (hrCount == 1 ? " hr" : " hrs");
			} else if (noOfMinsStart >= day && noOfMinsStart < month) {
				Integer dayCount = noOfMinsStart / day;
				if(noOfMinsStart % day > 0){
					dayCount++;
				}
				remainingTime = dayCount + (dayCount == 1 ? " day" : " days");
			} else if (noOfMinsStart >= month && noOfMinsStart < year) {
				Integer monthCount = noOfMinsStart / month;
				remainingTime = monthCount + (monthCount == 1 ? " mth" : " mths");
			} else {
				Integer yearCount = noOfMinsStart / year;
				remainingTime = yearCount + (yearCount == 1 ? " yr" : " yrs");
			}
			remainingTime = remainingTime + " left";
		}

		else if (noOfMinsStart <= 0 && noOfMinsEnd <= 0) {
			remainingTime = "Ongoing";
		}

		else if (noOfMinsEnd > 0) {
			remainingTime = "Complete";
		}
		return remainingTime;
	}
	
	/**
	 * 
	 * Used to count the activation days for enrollment
	 * @author Liky_Pan
	 * @param givenDate
	 */
	public static String getNoOfDaysInfuture(Date givenDate){
		if (null == givenDate)
			return "";

		DateTime start = new DateTime(new Date());
		DateTime end = new DateTime(givenDate);

		int noOfDays = Days.daysBetween(start.withTimeAtStartOfDay(),
				end.withTimeAtStartOfDay()).getDays();
		if(noOfDays==1){
			return "(" + Math.abs(noOfDays) + " day)";
		}
		else if(noOfDays>0){
			return "(" + Math.abs(noOfDays) + " days)";
		}else{
			return "";
		}
	}
	
	/**
	 * 
	 * Used to count the activation days for enrollment
	 * @author Liky_Pan
	 * @param givenDate
	 */
	public static Integer getNoOfDaysOnlyInfuture(Date givenDate){
		if (null == givenDate)
			return null;

		DateTime start = new DateTime(new Date());
		DateTime end = new DateTime(givenDate);

		Integer noOfDays = Days.daysBetween(start.withTimeAtStartOfDay(),
				end.withTimeAtStartOfDay()).getDays();
		if(noOfDays<0){
			return 0;
		}else{
			return noOfDays;	
		}
	}

	/**
	 * @author Allen_Yu
	 * @param givenDate
	 * @return ymdAndDateDiff String such as 2015/05/05(Today)
	 */
	public static String getYMDDateAndDateDiff(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		df = new SimpleDateFormat("yyyy-MMM-dd");
		return df.format(givenDate) + " " + getNoOfDays(givenDate);
	}

	public static String getYMDDate(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		df = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
		return df.format(givenDate);
	}

	public static String getFomattedDate(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		df = new SimpleDateFormat("ddMMMHHmmss");
		return df.format(givenDate);
	}

	/**
	 * @author Li_Chen
	 * @param givenDateStr
	 * @return getObliqueYMDDateAndDateDiff String such as 2015/04/22
	 *         17:01:55(16d ago)
	 */
	public static String getObliqueYMDDateAndDateDiff(Date givenDate) {
		df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return df.format(givenDate) + getNoOfDays(givenDate);
	}

	public static String getObliqueYMDDateAndDateDiff(Date givenDate,
			String format) {
		df = new SimpleDateFormat(format);
		return df.format(givenDate) + getNoOfDays(givenDate);
	}

	public static Date parseString2Date(String src, String format) {

		if (StringUtils.isEmpty(src))
			return null;

		if (StringUtils.isEmpty(format))
			format = defaultformat;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = simpleDateFormat.parse(src);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * @param givenDate
	 * @return ymdAndDateTimeDiff String such as 2015/05/05(Today)
	 * @author Vineela_Jyothi
	 */
	public static String getYMDDateTimeAndDateDiff(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		df = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		return df.format(givenDate) + " " + getNoOfDays(givenDate);
	}

	
   
   public static String parseDate2String(Date startDate,String format) {
		df = new SimpleDateFormat(format);
		String strDate = df.format(startDate);
		return strDate;
	}
   
       public static boolean isSameDay(Date day1, Date day2) {
           SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
           String ds1 = sdf.format(day1);
           String ds2 = sdf.format(day2);
           if (ds1.equals(ds2)) {
               return true;
           } else {
               return false;
           }
       }
   
   
	public static void main(String[] args) {
//		System.out.println(getStrFromDate(new Date()));
//		System.out.println(getDateFromStr("2013-01-22 17:01:55"));
//		System.out.println(getNoOfDays(getDateFromStr("2015-05-18 17:01:55")));
//		System.out.println(getObliqueYMDDateAndDateDiff(new Date()));
		
//		System.out.println(parseString2Date("2014/02/12", "yyyy/MM/dd"));
//		System.out.println(getDateFromStr("2013-01-22"));
//		System.out.println(getYMDDateAndDateDiff(getDateFromStr("2015-08-29 17:01:55")));
//		Date date = new Date();
//		System.out.println(date.getTime());
//		System.out.println(getFomattedDate(date));
//		Timestamp time = new Timestamp(new Date().getTime());
//		System.out.println(time);
		System.out.println(parseDate2String(new Date(),"yyyy MMM dd"));
		System.out.println(parseDate2String(new Date(),"MMM dd"));
		
		
		System.out.println(DateConvertUtil.date2String(new Date(), "yyyy-MM-dd"));
		String str = DateConvertUtil.date2String(new Date(), "yyyy-MM-dd") + " 23:59:59";
		System.out.println(str);
		
		System.out.println(DateConvertUtil.df);
		System.out.println(getDateFromStr(str));
		
		
		System.out.println(getDateFromStr(DateConvertUtil.date2String(new Date(), "yyyy-MM-dd")+" 23:59:59"));
		
		System.out.println(getTimeDifferenceForPast(parseString2Date("2011/11/13 10:59", "yyyy/MM/dd HH:mm")));
	}

}
