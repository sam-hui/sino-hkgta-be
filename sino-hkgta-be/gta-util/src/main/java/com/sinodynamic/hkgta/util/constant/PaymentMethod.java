package com.sinodynamic.hkgta.util.constant;

public enum PaymentMethod {
	VISA("VISA"),MASTER("MASTER"),CASH("Cash"),CV("Cash Value"),BT("Bank Transfer"),CHEQUE("Cheque"),VAC("Virtual Account Transfer"),UPAY("union pay"),OTH("Other"), PREAUTH("Pre-Authorization"); 
	
	private String desc;

	private PaymentMethod(String desc) {
		this.desc = desc;
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
