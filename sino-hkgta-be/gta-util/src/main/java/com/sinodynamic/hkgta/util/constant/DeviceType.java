package com.sinodynamic.hkgta.util.constant;

public enum DeviceType {

	WP("Web Portal"), AP("Apps");

	private String desc;

	private DeviceType(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return this.desc;
	}

}
