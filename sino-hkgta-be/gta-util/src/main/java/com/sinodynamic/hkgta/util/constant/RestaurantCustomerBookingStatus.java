package com.sinodynamic.hkgta.util.constant;

public enum RestaurantCustomerBookingStatus {
	
	CFM("confirmed"), EXP("expired and not attend"), DEL("mark deleted"), ATN("attended and completed");

		private String desc;

		private RestaurantCustomerBookingStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

