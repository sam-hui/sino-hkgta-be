package com.sinodynamic.hkgta.util.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Phone constraint annotation implementation 
 * @author Sam Hui
 *
 */
public class PhoneConstraintImp implements ConstraintValidator<Phone, String> {
   
   public void initialize(Phone phone) { }

   public boolean isValid(String phoneField, ConstraintValidatorContext cxt) {
       if(phoneField == null) {
           return false;
       }
       return phoneField.matches("[0-9()-]*") && phoneField.trim().length() >= 8 ;
   }

}