package com.sinodynamic.hkgta.util.constant;

public enum EmailType {
    /*
     * EmailType("functionId")
     */
	PRESENT("present"),
	RENEWAL("renewal"),
	INVOICE("invoice"),
	RECEIPT("receipt"),
	ACTIVATION("activation"),
	DAYPASSRECEIPT("daypass_receipt"),
	GUESTROOMBOOKRECEIPT("guest_room_book_confirm"),
	Purchase_Daypass("daypass_purchase_notify"),
	REJECT("reject"),
	
	UPDATE_MEMBER_STATUS_EMAIL("update_member_status_email"),
	UPDATE_CORPORATE_STATUS_EMAIL("update_corporate_status_email"),
	UPDATE_CORPORATE_PROFILE_EMAIL("update_corporate_profile_email")
	;
	
	private String functionId;
	
	private EmailType(String functionId){
		this.functionId = functionId;
	}
	
	public String getFunctionId(){
		return this.functionId;
	}
	
}
