package com.sinodynamic.hkgta.util.exception;

import com.sinodynamic.hkgta.util.constant.GTAError;

/**
 * This is a common exception for the whole GTA project. it will be handled by ControllerBase.
 * @author Vian Tang
 * 
 * */
public class GTACommonException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private GTAError error;
	
	private String errorMsg;

	private Object[] args;

	public GTACommonException()
	{
		super();
	}
	
	
	public GTACommonException(GTAError error)
	{
		super();
		this.error = error;
	}
	
	public GTACommonException(GTAError error, String errorMsg)
	{
		super(errorMsg);
		this.error = error;
		this.errorMsg = errorMsg;
	}

	public GTACommonException(GTAError error, Object[] args)
	{
		super();
		this.error = error;
		this.args = args;
	}

		
	public GTACommonException(Exception e)
	{
		super(e);
	}


	public GTAError getError()
	{
		return error;
	}

	public void setError(GTAError error)
	{
		this.error = error;
	}

	public Object[] getArgs()
	{
		return args;
	}

	public void setArgs(Object[] args)
	{
		this.args = args;
	}


	public String getErrorMsg()
	{
		return errorMsg;
	}


	public void setErrorMsg(String errorMsg)
	{
		this.errorMsg = errorMsg;
	}

}
