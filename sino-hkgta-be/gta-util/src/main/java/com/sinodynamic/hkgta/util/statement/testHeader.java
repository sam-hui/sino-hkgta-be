package com.sinodynamic.hkgta.util.statement;

import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

public class testHeader {

	public static void main(String[] args) {
		Document document = new Document(PageSize.A4, 50, 50, 150, 50);
		try {
			
			
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\Allen_Yu\\Desktop\\ss.pdf"));
			Image logo = Image.getInstance("C:\\Users\\Allen_Yu\\Desktop\\logo_header.jpg");
			logo.setAlignment(Image.MIDDLE);
			logo.setAbsolutePosition(0, 0);
			
//			logo.scaleAbsoluteHeight(500);
//			logo.scaleAbsoluteWidth(2000);
			logo.scalePercent(25);
			Chunk chunk = new Chunk(logo, 0, 0,true);
			Phrase phrase = new Phrase();
			phrase.add(chunk);
			Rectangle rect = new Rectangle(36, 54, 559, 810);
//			writer.setBoxSize("logo_header", rect);
//			rect = writer.getBoxSize("logo_header");
			CustomHeaderFooter header = new CustomHeaderFooter();
			writer.setPageEvent(header);
			document.open();
			header.onEndPage(writer, document,rect,phrase);
			Font titleFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
			Paragraph title = new Paragraph("Patron Account Statement",titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			title.setLeading(15f);
			title.setSpacingBefore(20f);
			document.add(title);
			Paragraph par = new Paragraph(
					"First Page. It has also requested the names of companies suspected of involvement in causing the haze,"
					+ " who can be punished under Singapore laws. Indonesia has previously pointed out that some palm oil, "
					+ "paper and pulp companies which operate in its territory have Singapore and Malaysian stakeholders.");
			document.add(par);
			document.newPage();
			
			Paragraph par2 = new Paragraph(
					"Second Page. It has also requested the names of companies suspected of involvement in causing the haze,"
					+ " who can be punished under Singapore laws. Indonesia has previously pointed out that some palm oil, "
					+ "paper and pulp companies which operate in its territory have Singapore and Malaysian stakeholders.");
			document.add(par2);
			document.newPage();
			
			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
