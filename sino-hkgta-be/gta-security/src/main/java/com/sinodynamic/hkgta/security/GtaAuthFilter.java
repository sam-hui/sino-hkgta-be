package com.sinodynamic.hkgta.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.transaction.TransactionException;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

public class GtaAuthFilter extends GenericFilterBean
{
	private static final Logger LOG = LogManager.getLogger(GtaAuthFilter.class);
	
	@Autowired
	private ResponseMsg responseMsg;

	@Autowired
	@Resource(name="authUserDetailService")
	private AuthUserDetailService authUserDetailService;

	@Autowired
	private UserMasterService userMasterService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		if (!(request instanceof HttpServletRequest))
			throw new RuntimeException("Expecting a http request");

		HttpServletRequest httpRequest = (HttpServletRequest) request;

		LOG.debug(httpRequest.getRequestURI());

		boolean valid = false;
		boolean noCheck = false;
		boolean pass = true;
		String errorMsg = "";
		GTAError errorCode = null;

		
		noCheck =  httpRequest.getRequestURI().contains("/home/login")
				|| httpRequest.getRequestURI().contains("/docs")
				|| httpRequest.getRequestURI().contains("/rest/api-docs")
				|| httpRequest.getRequestURI().contains("get_media") 
				|| httpRequest.getRequestURI().contains("get_thumbnail") 
				|| httpRequest.getRequestURI().endsWith("hkgtaCoreservices/")
				|| httpRequest.getRequestURI().contains("getCustomerFile")
				|| httpRequest.getRequestURI().contains("getReport")
				|| httpRequest.getRequestURI().contains("addVistor")
				|| httpRequest.getRequestURI().contains("/verifyMemberInfo/")
				|| httpRequest.getRequestURI().contains("/validaterAnswer")
				|| httpRequest.getRequestURI().contains("/updatePwd")
				|| httpRequest.getRequestURI().contains("/membership/pospayment/callback")
				|| httpRequest.getRequestURI().contains("/getStatementPDF")
				|| httpRequest.getRequestURI().contains("/getSattlementReportExport")
		        || httpRequest.getRequestURI().contains("/getDailyEnrollmentAttach")
				|| httpRequest.getRequestURI().contains("/getMonthlyEnrollmentAttach")
				|| httpRequest.getRequestURI().contains("/getDailyLeadsAttach")
				|| httpRequest.getRequestURI().contains("/getDailyFacilityUsageAttach")
				|| httpRequest.getRequestURI().contains("/getMonthlyFacilityUsageAttach")
				|| httpRequest.getRequestURI().contains("/monthlyCourseCompletionReportAttach")
				|| httpRequest.getRequestURI().contains("/monthlyCourseRevenueReportAttach")
				|| httpRequest.getRequestURI().contains("/getEnrollmentRenewalAttach")
				||httpRequest.getRequestURI().contains("/academyNoQrCode")
		;
		


		String base64Token = httpRequest.getHeader("token");
		
		if (null==base64Token || StringUtils.isEmpty(base64Token))
		{
			base64Token = httpRequest.getParameter("token");
		}
		
		if (!noCheck)
		{
			if (!StringUtils.isEmpty(base64Token))
			{
				byte[] t = Base64.decodeBase64(base64Token);
				String authToken = new String(t);

				valid = TokenUtils.validateUUIDToken(authToken);
				String userId = null;
				if (valid)
				{
					LOG.debug("token:[" + authToken + "]");
					userId = TokenUtils.getUserNameFromToken(authToken);
				}
				else
				{
					errorCode = GTAError.LoginError.TOKEN_INVALID;
				}

				if (userId != null)
				{
					boolean shouldUpdateToken = shouldUpdateToken(httpRequest);

					try {
						errorCode = userMasterService.updateSessionToken(authToken, shouldUpdateToken);
					} catch (TransactionException e) {
						logger.error(e.getMessage(), e);
						// update token failure should continue
						errorCode = GTAError.LoginError.SAVE_SESSION_FAILED;
					}

					if (valid && (GTAError.Success.SUCCESS.equals(errorCode) || GTAError.LoginError.SAVE_SESSION_FAILED.equals(errorCode)))
					{
						try
						{
							LoginUser userDetails = null;
							UsernamePasswordAuthenticationToken authenticationToken = null;
							userDetails = (LoginUser) authUserDetailService.loadUserByUsername(userId);
							authenticationToken = new GtaAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());

							authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
							SecurityContextHolder.getContext().setAuthentication(authenticationToken);

							request.setAttribute("User", userDetails);
							SessionMap.getInstance().addLocation(userId, TokenUtils.getLocationFromUUIDToken(authToken));
						}
						catch (Exception e)
						{
							pass = false;
							if (e.getClass().equals(GTACommonException.class))
							{
								errorCode = ((GTACommonException)e).getError();
							}
							else
							{
								errorCode = GTAError.LoginError.AUTHENTICATE_REQRIRED;
							}
						}

					}
					else
					{
						pass = false;
						LOG.debug("validateToken:" + valid);
						LOG.debug(errorMsg);
					}
				}
			}
			else
			{
				errorCode = GTAError.LoginError.AUTHENTICATE_REQRIRED;
			}
		}

		try
		{
			if ((valid && pass) || noCheck)
			{
				chain.doFilter(request, response);
			}
			else
			{
				setResponse(httpRequest, response, errorCode);
			}
		}
		finally
		{
		}
	}


	private boolean shouldUpdateToken(HttpServletRequest httpRequest) {
		boolean shouldUpdateToken = true;
		String refresh = httpRequest.getHeader("refresh");
		
		if ("true".equals(refresh)) {
			shouldUpdateToken = false;
		}
		return shouldUpdateToken;
	}


	private void setResponse(HttpServletRequest request, ServletResponse response, GTAError error) throws JsonGenerationException, JsonMappingException, IOException
	{
		LOG.debug("responseTo:" + request.getRequestURI());
		LOG.debug("auth_token:" + request.getHeader("token"));
		
		responseMsg.initResult(error);
		response.getOutputStream().write((new ObjectMapper().writeValueAsString(responseMsg)).getBytes());
		response.setContentType("application/json");
	}
}
