package com.sinodynamic.hkgta.security;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class LoginUser extends User
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4565267201666023250L;
	
	private String role;  
	private String userId;  
	private String defaultModule;
	private String userType;
	private String userName;
	private String fullname;
	private String portraitPhoto;
	private String dateOfBirth;
	private String positionTitle;
	private String firstName;
	private String lastName;
	private String memberType;
	
	
	
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getFullname()
	{
		return fullname;
	}

	public void setFullname(String fullname)
	{
		this.fullname = fullname;
	}

	private String staffType;
	
	public String getStaffType()
	{
		return staffType;
	}

	public void setStaffType(String staffType)
	{
		this.staffType = staffType;
	}

	public String getPortraitPhoto()
	{
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto)
	{
		this.portraitPhoto = portraitPhoto;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getUserNo()
	{
		return userNo;
	}

	public void setUserNo(String userNo)
	{
		this.userNo = userNo;
	}

	private String userNo;
	
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getRole()
	{
		return role;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	public String getDefaultModule()
	{
		return defaultModule;
	}

	public void setDefaultModule(String defaultModule)
	{
		this.defaultModule = defaultModule;
	}

	public String getUserType()
	{
		return userType;
	}

	public void setUserType(String userType)
	{
		this.userType = userType;
	}

	public String getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	public String getPositionTitle()
	{
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle)
	{
		this.positionTitle = positionTitle;
	}

	public String getMemberType()
	{
		return memberType;
	}

	public void setMemberType(String memberType)
	{
		this.memberType = memberType;
	}

	public LoginUser(String userid, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Collection<GrantedAuthority> authorities) {
		super(userid, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		// TODO Auto-generated constructor stub
	}

	public LoginUser(String userid, String password, Collection<? extends GrantedAuthority> authorities)
	{
		super(userid, password, authorities);
		// TODO Auto-generated constructor stub
	}
	


}
