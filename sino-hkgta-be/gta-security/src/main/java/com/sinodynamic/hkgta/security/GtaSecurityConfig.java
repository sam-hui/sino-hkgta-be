package com.sinodynamic.hkgta.security;

import org.springframework.security.access.SecurityConfig;

public class GtaSecurityConfig extends SecurityConfig {

	private static final long serialVersionUID = 4677025681970505610L;

	private String accessRight;

	/** use {@link #GtaSecurityConfig(String, String)} instead. */
	@Deprecated
	public GtaSecurityConfig(String config) {
		super(config);
	}

	public GtaSecurityConfig(String config, String accessRight) {
		super(config);
		this.accessRight = accessRight;
	}

	public String getAccessRight() {
		return accessRight;
	}

	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}

}
