package com.sinodynamic.hkgta.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dto.sys.RoleProgramURLDto;
import com.sinodynamic.hkgta.entity.crm.RoleProgram;
import com.sinodynamic.hkgta.security.service.RoleProgramService;
import com.sinodynamic.hkgta.service.sys.UserRoleMap;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.urlmatcher.AntUrlPathMatcher;

public class GtaInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	@Autowired
	private RoleProgramService roleProgramService;

	private AntUrlPathMatcher urlMatcher = new AntUrlPathMatcher();

	private static Map<String, Collection<ConfigAttribute>> resourceMap = null;

	public static Map<String, Collection<ConfigAttribute>> getResourceMap()
	{
		return resourceMap;
	}

	public GtaInvocationSecurityMetadataSource() {
	}

	/** init-method of GtaInvocationSecurityMetadataSource */
	
	//@PostConstruct
	public void loadProgramRoles() {
		
		UserRoleMap.getInstance().setRoleMenu(roleProgramService.initRoleMenu());
		resourceMap = new HashMap<String, Collection<ConfigAttribute>>();

		// fetch role-programs from database, and put them in this.resourceMap.
		String programUrlPath = null;
		String roleName = null;
		Collection<ConfigAttribute> atts = null;
		List<RoleProgramURLDto> rolePrograms = roleProgramService.getRoleProgramsWithUrlPath();
		if (rolePrograms != null && !rolePrograms.isEmpty()) {
			for (RoleProgramURLDto roleProgram : rolePrograms) {
				programUrlPath = roleProgram.getUriPath();
				roleName = roleProgram.getRoleName();
				if (CommUtil.notEmpty(programUrlPath) && CommUtil.notEmpty(roleName)) {
					atts = resourceMap.get(programUrlPath);
					if (atts == null) {
						atts = new ArrayList<ConfigAttribute>();
					}
					atts.add(new GtaSecurityConfig(roleName, roleProgram.getAccessRight()));
					resourceMap.put(programUrlPath, atts);
				}
			}
		}
		// TODO programMaster.urlPath == null --> no authorization required?
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return null;
	}

	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		// param "object" --> request (url).
		String url = ((FilterInvocation) object).getRequestUrl();
		
		if ("/".equals(url)) {
			return null;
		}
		int firstQuestionMarkIndex = url.indexOf(".");
		// (/index.do --> /index)
		if (firstQuestionMarkIndex != -1) {
			url = url.substring(0, firstQuestionMarkIndex);
		}

		String urlPath = ((FilterInvocation) object).getHttpRequest().getPathInfo();
		if (!StringUtils.isEmpty(urlPath))
		{
			url = urlPath;
		}
		Iterator<String> ite = resourceMap.keySet().iterator();
		// compare / pattern-actualUrl matching
		while (ite.hasNext()) {
			String resURL = ite.next(); // url-pattern
			if (urlMatcher.pathMatchesUrl(resURL, url)) {
				return resourceMap.get(resURL);
			}
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

}
