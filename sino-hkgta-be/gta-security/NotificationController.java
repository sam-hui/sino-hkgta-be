package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.NotificationService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError.CommonError;
import com.sinodynamic.hkgta.util.exception.ErrorCodeException;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/notification")
@Scope("prototype") 
public class NotificationController extends ControllerBase{
	@Autowired
	private NotificationService notificationService;
	
	public Logger logger = Logger.getLogger(NotificationController.class);

	@RequestMapping(value = "/email", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseMsg sendNotificationByEmail(@ModelAttribute NotificationDto notificationDto){
		try{
			
			return null;
		}catch(ErrorCodeException e){
			responseMsg.initResult(e.getErrorCode(),e.getArgs());
			return responseMsg;
		}
		catch(Exception e){
			logger.error(e);
			responseMsg.initResult(CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
	
	@RequestMapping(value = "/sms", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseMsg sendNotificationBySMS(@ModelAttribute NotificationDto notificationDto){
		try{
			
			return null;
		}catch(ErrorCodeException e){
			responseMsg.initResult(e.getErrorCode(),e.getArgs());
			return responseMsg;
		}
		catch(Exception e){
			logger.error(e);
			responseMsg.initResult(CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
	

	
}
