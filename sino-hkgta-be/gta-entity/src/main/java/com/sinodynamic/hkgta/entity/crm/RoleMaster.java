package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the role_master database table.
 * 
 */
@Entity
@Table(name="role_master")
@NamedQuery(name="RoleMaster.findAll", query="SELECT r FROM RoleMaster r")
public class RoleMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="role_id", unique=true, nullable=false)
	private Long roleId;

	@Column(length=300)
	private String description;

	@Column(name="for_user_type", length=10)
	private String forUserType;

	@Column(name="role_name", nullable=false, length=100)
	private String roleName;

	@Column(length=10)
	private String status;


	public RoleMaster(Long roleId, String status, String forUserType)
	{
		super();
		this.roleId = roleId;
		this.forUserType = forUserType;
		this.status = status;
	}
	
	public RoleMaster(Long roleId, String status, String forUserType, String roleName)
	{
		super();
		this.roleId = roleId;
		this.forUserType = forUserType;
		this.status = status;
		this.roleName = roleName;
	}


	public RoleMaster() {
	}

	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Object roleId) {
		this.roleId = (roleId!=null ? NumberUtils.toLong(roleId.toString()):null);
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getForUserType() {
		return this.forUserType;
	}

	public void setForUserType(String forUserType) {
		this.forUserType = forUserType;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}