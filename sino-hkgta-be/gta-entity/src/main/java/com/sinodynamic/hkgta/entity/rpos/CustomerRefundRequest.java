package com.sinodynamic.hkgta.entity.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "customer_refund_request")
public class CustomerRefundRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "refund_id")
	private Long refundId;
	
	@Column(name = "requester_type")
	private String requesterType;
	
	@Column(name = "refund_transaction_no")
	private Long refundTransactionNo;
	
	@Column(name = "order_det_id")
	private Long orderDetailId;
	
	@Column(name = "refund_service_type")
	private String refundServiceType;
	
	@Column(name = "customer_reason")
	private String customerReason;
	
	@Column(name = "internal_remark")
	private String internalRemark;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "request_amt")
	private BigDecimal requestAmount;
	
	@Column(name = "approved_amt")
	private BigDecimal approvedAmount;
	
	@Column(name = "refund_money_type")
	private String refundMoneyType;
	
	@Column(name = "auditor_user_id")
	private String auditorUserId;
	
	@Column(name = "audit_date")
	private String auditDate;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "create_by")
	private String createBy;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "update_by")
	private String updateBy;
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	public String getRequesterType() {
		return requesterType;
	}

	public void setRequesterType(String requesterType) {
		this.requesterType = requesterType;
	}

	public Long getRefundTransactionNo() {
		return refundTransactionNo;
	}

	public void setRefundTransactionNo(Long refundTransactionNo) {
		this.refundTransactionNo = refundTransactionNo;
	}

	public Long getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(Long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public String getRefundServiceType() {
		return refundServiceType;
	}

	public void setRefundServiceType(String refundServiceType) {
		this.refundServiceType = refundServiceType;
	}

	public String getCustomerReason() {
		return customerReason;
	}

	public void setCustomerReason(String customerReason) {
		this.customerReason = customerReason;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getRequestAmount() {
		return requestAmount;
	}

	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}

	public BigDecimal getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(BigDecimal approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public String getRefundMoneyType() {
		return refundMoneyType;
	}

	public void setRefundMoneyType(String refundMoneyType) {
		this.refundMoneyType = refundMoneyType;
	}

	public String getAuditorUserId() {
		return auditorUserId;
	}

	public void setAuditorUserId(String auditorUserId) {
		this.auditorUserId = auditorUserId;
	}

	public String getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
}