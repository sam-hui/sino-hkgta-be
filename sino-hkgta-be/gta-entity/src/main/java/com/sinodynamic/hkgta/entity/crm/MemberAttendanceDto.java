package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Date;

public class MemberAttendanceDto implements Serializable {
	
	private Long customerId;
	
	private String customerName;
	
	private Date traingDate;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getTraingDate() {
		return traingDate;
	}

	public void setTraingDate(Date traingDate) {
		this.traingDate = traingDate;
	}

	
}
