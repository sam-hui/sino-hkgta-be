package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the program_master database table.
 * 
 */
@Entity
@Table(name="program_master")
@NamedQuery(name="ProgramMaster.findAll", query="SELECT p FROM ProgramMaster p")
/*@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable*/
public class ProgramMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="program_id", unique=true, nullable=false, length=40)
	private String programId;

	@Column(length=120)
	private String icon;

	@Column(name="menu_name", length=120)
	private String menuName;

	@Column(name="module_id", length=10)
	private String moduleId;

	@Column(name="parent_id", length=40)
	private String parentId;

	@Column(name="program_desc", length=200)
	private String programDesc;

	@Column(name="program_type", length=10)
	private String programType;

	@Column(name="sort_seq")
	private Long sortSeq;

	@Column(length=10)
	private String status;

	@Column(name="url_path", length=250)
	private String urlPath;

	public ProgramMaster() {
	}

	public String getProgramId() {
		return this.programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getParentId() {
		return this.parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getProgramDesc() {
		return this.programDesc;
	}

	public void setProgramDesc(String programDesc) {
		this.programDesc = programDesc;
	}

	public String getProgramType() {
		return this.programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}

	public Long getSortSeq() {
		return this.sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrlPath() {
		return this.urlPath;
	}

	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}

}