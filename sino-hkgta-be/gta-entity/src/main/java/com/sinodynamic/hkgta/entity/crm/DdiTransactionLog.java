package com.sinodynamic.hkgta.entity.crm;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ddi_transaction_log")
public class DdiTransactionLog implements java.io.Serializable {

	private Long sysId;
	private Long transactionNo;

	private DdInterfaceFileLog ddInterfaceFileLog;

	public DdiTransactionLog() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "process_id", nullable = false)
	public DdInterfaceFileLog getDdInterfaceFileLog() {
		return this.ddInterfaceFileLog;
	}

	public void setDdInterfaceFileLog(DdInterfaceFileLog ddInterfaceFileLog) {
		this.ddInterfaceFileLog = ddInterfaceFileLog;
	}

	@Column(name = "transaction_no", nullable = false)
	public long getTransactionNo() {
		return this.transactionNo;
	}

	public void setTransactionNo(long transactionNo) {
		this.transactionNo = transactionNo;
	}

}
