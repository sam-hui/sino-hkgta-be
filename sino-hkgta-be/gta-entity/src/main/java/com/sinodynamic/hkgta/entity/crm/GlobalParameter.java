package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the global_parameter database table.
 * 
 */
@Entity
@Table(name="global_parameter")
@NamedQuery(name="GlobalParameter.findAll", query="SELECT g FROM GlobalParameter g")
public class GlobalParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="param_id", unique=true, nullable=false, length=40)
	private String paramId;

	@Column(name="chg_permission", length=10)
	private String chgPermission;

	@Column(name="create_by", length=50)
	private String createBy;
	@JsonIgnore
	@Column(name="create_date", nullable=false)
	private Timestamp createDate;

	@Column(length=200)
	private String description;

	@Column(name="display_order")
	private Long displayOrder;

	@Column(name="param_cat", length=50)
	private String paramCat;

	@Column(name="param_value", length=50)
	private String paramValue;

	@Column(length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	//bi-directional many-to-one association to UserPreferenceSetting
	@OneToMany(mappedBy="globalParameter")
	private List<UserPreferenceSetting> userPreferenceSettings;

	public GlobalParameter() {
	}

	public String getParamId() {
		return this.paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getChgPermission() {
		return this.chgPermission;
	}

	public void setChgPermission(String chgPermission) {
		this.chgPermission = chgPermission;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getParamCat() {
		return this.paramCat;
	}

	public void setParamCat(String paramCat) {
		this.paramCat = paramCat;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<UserPreferenceSetting> getUserPreferenceSettings() {
		return this.userPreferenceSettings;
	}

	public void setUserPreferenceSettings(List<UserPreferenceSetting> userPreferenceSettings) {
		this.userPreferenceSettings = userPreferenceSettings;
	}

	public UserPreferenceSetting addUserPreferenceSetting(UserPreferenceSetting userPreferenceSetting) {
		getUserPreferenceSettings().add(userPreferenceSetting);
		userPreferenceSetting.setGlobalParameter(this);

		return userPreferenceSetting;
	}

	public UserPreferenceSetting removeUserPreferenceSetting(UserPreferenceSetting userPreferenceSetting) {
		getUserPreferenceSettings().remove(userPreferenceSetting);
		userPreferenceSetting.setGlobalParameter(null);

		return userPreferenceSetting;
	}

}