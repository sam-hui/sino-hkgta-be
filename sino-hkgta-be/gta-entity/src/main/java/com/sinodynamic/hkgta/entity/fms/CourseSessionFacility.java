package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the course_session_facility database table.
 * 
 */
@Entity
@Table(name = "course_session_facility")
@NamedQuery(name = "CourseSessionFacility.findAll", query = "SELECT f FROM CourseSessionFacility f")
public class CourseSessionFacility implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false, length = 20)
	private Long sysId;

	@Column(name = "course_session_id")
	private Long courseSessionId;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="facility_timeslot_id", nullable=false, insertable=true, updatable=true)
	private FacilityTimeslot facilityTimeslot;
	
	@Column(name = "facility_no", length = 50)
	private String facilityNo;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public CourseSessionFacility() {
	}

	public Long getSysId()
	{
		return sysId;
	}

	public void setSysId(Long sysId)
	{
		this.sysId = sysId;
	}

	public Long getCourseSessionId()
	{
		return courseSessionId;
	}

	public void setCourseSessionId(Long courseSessionId)
	{
		this.courseSessionId = courseSessionId;
	}

	public String getFacilityNo()
	{
		return facilityNo;
	}

	public void setFacilityNo(String facilityNo)
	{
		this.facilityNo = facilityNo;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public FacilityTimeslot getFacilityTimeslot()
	{
		return facilityTimeslot;
	}

	public void setFacilityTimeslot(FacilityTimeslot facilityTimeslot)
	{
		this.facilityTimeslot = facilityTimeslot;
	}
}