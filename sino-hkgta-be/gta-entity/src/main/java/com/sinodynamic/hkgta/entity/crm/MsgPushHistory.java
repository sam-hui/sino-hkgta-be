package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="msg_push_history")
public class MsgPushHistory implements Serializable {
    
    	private static final long serialVersionUID = 1L;
    	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false, length=15)
	private Long sysId;
	
	@Column(name="msg_type")
	private String msgType;
	
	@Column(name="ref_key")
	private Long refKey;

	@Column(name="recipient_device_id")
	private Long recipientDeviceId;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="content")
	private String content;
	
	@Column(name="status")
	private String status;
	
	@Column(name="response_code")
	private String responseCode;
	
	@Column(name="retry_count")
	private Long retryCount;
	
	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="status_update_date")
	private Date statusUpdateDate;

	public Long getSysId() {
	    return sysId;
	}

	public void setSysId(Long sysId) {
	    this.sysId = sysId;
	}

	public String getMsgType() {
	    return msgType;
	}

	public void setMsgType(String msgType) {
	    this.msgType = msgType;
	}

	public Long getRefKey() {
	    return refKey;
	}

	public void setRefKey(Long refKey) {
	    this.refKey = refKey;
	}

	public Long getRecipientDeviceId() {
	    return recipientDeviceId;
	}

	public void setRecipientDeviceId(Long recipientDeviceId) {
	    this.recipientDeviceId = recipientDeviceId;
	}

	public String getContent() {
	    return content;
	}

	public void setContent(String content) {
	    this.content = content;
	}

	public String getStatus() {
	    return status;
	}

	public void setStatus(String status) {
	    this.status = status;
	}

	public String getResponseCode() {
	    return responseCode;
	}

	public void setResponseCode(String responseCode) {
	    this.responseCode = responseCode;
	}

	public Long getRetryCount() {
	    return retryCount;
	}

	public void setRetryCount(Long retryCount) {
	    this.retryCount = retryCount;
	}

	public Date getCreateDate() {
	    return createDate;
	}

	public void setCreateDate(Date createDate) {
	    this.createDate = createDate;
	}

	public String getCreateBy() {
	    return createBy;
	}

	public void setCreateBy(String createBy) {
	    this.createBy = createBy;
	}

	public Date getStatusUpdateDate() {
	    return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
	    this.statusUpdateDate = statusUpdateDate;
	}

	public String getSubject() {
	    return subject;
	}

	public void setSubject(String subject) {
	    this.subject = subject;
	}
	
}
