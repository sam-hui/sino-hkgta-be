package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the member_facility_book_addition_attr database table.
 * 
 */
@Entity
@Table(name="member_facility_book_addition_attr")
@NamedQuery(name="MemberFacilityBookAdditionAttr.findAll", query="SELECT f FROM MemberFacilityBookAdditionAttr f")
public class MemberFacilityBookAdditionAttr implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MemberFacilityBookAdditionAttrPK id;
	
	@Column(name="facility_type",length=10)
	private String facilityType;
	
	@Column(name="attr_value",length=250)
	private String attrValue;
	
	
	public MemberFacilityBookAdditionAttr() {
	}


	public MemberFacilityBookAdditionAttrPK getId()
	{
		return id;
	}


	public void setId(MemberFacilityBookAdditionAttrPK id)
	{
		this.id = id;
	}


	public String getFacilityType()
	{
		return facilityType;
	}


	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}


	public String getAttrValue()
	{
		return attrValue;
	}


	public void setAttrValue(String attrValue)
	{
		this.attrValue = attrValue;
	}




}