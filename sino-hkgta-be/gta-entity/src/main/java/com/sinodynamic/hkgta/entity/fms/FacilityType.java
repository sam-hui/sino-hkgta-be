package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the facility_type database table.
 * 
 */
@Entity
@Table(name="facility_type")
@NamedQuery(name="FacilityType.findAll", query="SELECT f FROM FacilityType f")
public class FacilityType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="type_code", unique=true, nullable=false, length=10)
	private String typeCode;

	@Column(length=250)
	private String description;
	
	@Column(name="default_quota")
	private Long defaultQuota;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	//bi-directional many-to-one association to FacilityMaster
	@OneToMany(mappedBy="facilityTypeBean")
	private List<FacilityMaster> facilityMasters;

	public FacilityType() {
	}

	public String getTypeCode() {
		return this.typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<FacilityMaster> getFacilityMasters() {
		return this.facilityMasters;
	}

	public void setFacilityMasters(List<FacilityMaster> facilityMasters) {
		this.facilityMasters = facilityMasters;
	}

	public FacilityMaster addFacilityMaster(FacilityMaster facilityMaster) {
		getFacilityMasters().add(facilityMaster);
		facilityMaster.setFacilityTypeBean(this);

		return facilityMaster;
	}

	public FacilityMaster removeFacilityMaster(FacilityMaster facilityMaster) {
		getFacilityMasters().remove(facilityMaster);
		facilityMaster.setFacilityTypeBean(null);

		return facilityMaster;
	}

	public Long getDefaultQuota()
	{
		return defaultQuota;
	}

	public void setDefaultQuota(Object defaultQuota)
	{
		this.defaultQuota = (defaultQuota!=null ? NumberUtils.toLong(defaultQuota.toString()):null);
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

}