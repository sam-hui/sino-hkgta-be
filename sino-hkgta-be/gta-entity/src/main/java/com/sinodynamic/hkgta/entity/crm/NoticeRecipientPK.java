package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the notice_recipient database table.
 * 
 */
@Embeddable
public class NoticeRecipientPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="notice_id", insertable=false, updatable=false, unique=true, nullable=false)
	private String noticeId;

	@Column(name="recipient_user_id", insertable=false, updatable=false, unique=true, nullable=false, length=50)
	private String recipientUserId;

	public NoticeRecipientPK() {
	}
	public String getNoticeId() {
		return this.noticeId;
	}
	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}
	public String getRecipientUserId() {
		return this.recipientUserId;
	}
	public void setRecipientUserId(String recipientUserId) {
		this.recipientUserId = recipientUserId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof NoticeRecipientPK)) {
			return false;
		}
		NoticeRecipientPK castOther = (NoticeRecipientPK)other;
		return 
			this.noticeId.equals(castOther.noticeId)
			&& this.recipientUserId.equals(castOther.recipientUserId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.noticeId.hashCode();
		hash = hash * prime + this.recipientUserId.hashCode();
		
		return hash;
	}
}