package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the room_pic_path database table.
 * 
 */
@Entity
@Table(name="room_pic_path")
@NamedQuery(name="RoomPicPath.findAll", query="SELECT p FROM RoomPicPath p")
public class RoomPicPath implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "pic_id", unique = true, nullable = false, length = 11)
	private Long id;

	@Column(name="room_type_code", length=10)
	private String roomTypeCode;
	
	@Column(name="room_id", length=11, nullable = true)
	private Long roomId;
	
	@Column(name="pic_type", length=10)
	private String picType;
	
	@Column(name="server_filename", length=255)
	private String serverFilename;
	
	@Column(name="display_order", length=11, nullable = true)
	private Long displayOrder;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getRoomTypeCode()
	{
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode)
	{
		this.roomTypeCode = roomTypeCode;
	}

	public Long getRoomId()
	{
		return roomId;
	}

	public void setRoomId(Long roomId)
	{
		this.roomId = roomId;
	}

	public String getPicType()
	{
		return picType;
	}

	public void setPicType(String picType)
	{
		this.picType = picType;
	}

	public String getServerFilename()
	{
		return serverFilename;
	}

	public void setServerFilename(String serverFilename)
	{
		this.serverFilename = serverFilename;
	}

	public Long getDisplayOrder()
	{
		return displayOrder;
	}

	public void setDisplayOrder(Long displayOrder)
	{
		this.displayOrder = displayOrder;
	}
	
}