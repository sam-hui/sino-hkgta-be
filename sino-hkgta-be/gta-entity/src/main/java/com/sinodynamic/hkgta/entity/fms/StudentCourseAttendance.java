package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the student_course_attendance database table.
 * 
 */
@Entity
@Table(name="student_course_attendance")
@NamedQuery(name="StudentCourseAttendance.findAll", query="SELECT s FROM StudentCourseAttendance s")
public class StudentCourseAttendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "attend_id", unique = true, nullable = false)
	private Long attendId;

	@Column(name="course_session_id")
	private Long courseSessionId;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="enroll_id")
	private Long enrollId;

	private String status;

	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public StudentCourseAttendance() {
	}

	public Long getAttendId() {
		return this.attendId;
	}

	public void setAttendId(Long attendId) {
		this.attendId = attendId;
	}

	public Long getCourseSessionId() {
		return this.courseSessionId;
	}

	public void setCourseSessionId(Long courseSessionId) {
		this.courseSessionId = courseSessionId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Long getEnrollId() {
		return this.enrollId;
	}

	public void setEnrollId(Object enrollId) {
		this.enrollId = (enrollId!=null ? NumberUtils.toLong(enrollId.toString()):null);
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

}