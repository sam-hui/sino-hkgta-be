package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class CustomerActionContentPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerActionContentPK() {
		// TODO Auto-generated constructor stub
	}
	
	@Column(name="customer_id")
	private Long customerId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="action_content")
	private String actionContent;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}
	

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getActionContent() {
		return actionContent;
	}

	public void setActionContent(String actionContent) {
		this.actionContent = actionContent;
	}
	
	
}
