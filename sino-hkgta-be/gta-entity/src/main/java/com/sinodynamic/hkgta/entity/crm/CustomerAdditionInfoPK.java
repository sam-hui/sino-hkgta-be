package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the customer_addition_info database table.
 * 
 */
@Embeddable
public class CustomerAdditionInfoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="customer_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long customerId;

	@Column(name="caption_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long captionId;

	public CustomerAdditionInfoPK() {
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getCaptionId() {
		return captionId;
	}

	public void setCaptionId(Long captionId) {
		this.captionId = captionId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerAdditionInfoPK)) {
			return false;
		}
		CustomerAdditionInfoPK castOther = (CustomerAdditionInfoPK)other;
		return 
			this.customerId.equals(castOther.customerId)
			&& (this.captionId.equals(castOther.captionId));
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + (this.customerId == null ? 0: this.customerId.intValue());
		hash = hash * prime + (this.captionId == null ? 0: this.captionId.intValue());
		
		return hash;
	}
}