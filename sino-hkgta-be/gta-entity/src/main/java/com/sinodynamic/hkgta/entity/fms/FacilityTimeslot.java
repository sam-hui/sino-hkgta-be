package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

/**
 * The persistent class for the facility_timeslot database table.
 * 
 */
@Entity
@Table(name = "facility_timeslot")
@NamedQuery(name = "FacilityTimeslot.findAll", query = "SELECT f FROM FacilityTimeslot f")
public class FacilityTimeslot implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "facility_timeslot_id", unique = true, nullable = false, length = 20)
	private Long facilityTimeslotId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "begin_datetime", nullable = false)
	private Date beginDatetime;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_datetime")
	private Date endDatetime;
	
	@Column(name = "transfer_from_timeslot_id")
	private Long transferFromTimeslotId;

	@Column(length = 10)
	private String status;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "internal_remark", length = 250)
	private String internalRemark;

	// bi-directional many-to-one association to FacilityMaster
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "facility_no")
	private FacilityMaster facilityMaster;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="facility_timeslot_id", nullable=false)
	private FacilityTimeslotAddition facilityTimeslotAddition;

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinTable(
            name="member_reserved_facility",
            joinColumns = @JoinColumn( name="facility_timeslot_id"),
            inverseJoinColumns = @JoinColumn( name="resv_id")
     )
	private MemberFacilityTypeBooking memberFacilityTypeBooking;
	
	@OneToOne(cascade = CascadeType.REMOVE, fetch=FetchType.LAZY, orphanRemoval = true, mappedBy="facilityTimeslot")
	private StaffFacilitySchedule staffFacilitySchedule;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@Column(name = "reserve_type")
	private String reserveType;
	
	public FacilityTimeslot() {
	}

	public Date getBeginDatetime() {
		return this.beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public FacilityMaster getFacilityMaster() {
		return this.facilityMaster;
	}

	public void setFacilityMaster(FacilityMaster facilityMaster) {
		this.facilityMaster = facilityMaster;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public FacilityTimeslotAddition getFacilityTimeslotAddition() {
		return facilityTimeslotAddition;
	}

	public void setFacilityTimeslotAddition(
			FacilityTimeslotAddition facilityTimeslotAddition) {
		this.facilityTimeslotAddition = facilityTimeslotAddition;
	}

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Object facilityTimeslotId) {
		this.facilityTimeslotId = (facilityTimeslotId!=null ? NumberUtils.toLong(facilityTimeslotId.toString()):null);
	}

	public MemberFacilityTypeBooking getMemberFacilityTypeBooking() {
		return memberFacilityTypeBooking;
	}

	public void setMemberFacilityTypeBooking(MemberFacilityTypeBooking memberFacilityTypeBooking) {
		this.memberFacilityTypeBooking = memberFacilityTypeBooking;
	}

	public StaffFacilitySchedule getStaffFacilitySchedule() {
		return staffFacilitySchedule;
	}

	public void setStaffFacilitySchedule(StaffFacilitySchedule staffFacilitySchedule) {
		this.staffFacilitySchedule = staffFacilitySchedule;
	}
	public Long getTransferFromTimeslotId()
	{
		return transferFromTimeslotId;
	}

	public void setTransferFromTimeslotId(Object transferFromTimeslotId)
	{
		this.transferFromTimeslotId = (transferFromTimeslotId!=null ? NumberUtils.toLong(transferFromTimeslotId.toString()):null);
	}

	public String getReserveType() {
		return reserveType;
	}

	public void setReserveType(String reserveType) {
		this.reserveType = reserveType;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Object verNo) {
		this.verNo = (verNo!=null ? NumberUtils.toLong(verNo.toString()):null);
	}
}