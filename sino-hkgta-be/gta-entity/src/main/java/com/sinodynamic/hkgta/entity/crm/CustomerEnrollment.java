package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the customer_enrollment database table.
 * 
 */
@Entity
@Table(name="customer_enrollment")
@NamedQuery(name="CustomerEnrollment.findAll", query="SELECT c FROM CustomerEnrollment c")
public class CustomerEnrollment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="enroll_id", unique=true, nullable=false)
	private Long enrollId;

	@Column(name="customer_id")
	private Long customerId;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="enroll_date")
	private Date enrollDate;

	@Column(name="enroll_type", length=10)
	private String enrollType;

	@Column(name="internal_remark", length=300)
	private String internalRemark;

	@Column(name="sales_follow_by", length=50)
	private String salesFollowBy;

	@Column(length=10)
	private String status;

	@Column(name="subscribe_contract_length")
	private Long subscribeContractLength;

	@Column(name="subscribe_plan_no")
	private Long subscribePlanNo;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	//bi-directional many-to-one association to CustomerEnrollPo
	//@OneToMany(mappedBy="customerEnrollment")
	@Transient
	private List<CustomerEnrollPo> customerEnrollPos;

	public CustomerEnrollment() {
	}

	public Long getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(Long enrollId) {
		this.enrollId = enrollId;
	}
	
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getEnrollDate() {
		return this.enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getEnrollType() {
		return this.enrollType;
	}

	public void setEnrollType(String enrollType) {
		this.enrollType = enrollType;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getSalesFollowBy() {
		return this.salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getSubscribeContractLength() {
		return subscribeContractLength;
	}

	public void setSubscribeContractLength(Long subscribeContractLength) {
		this.subscribeContractLength = subscribeContractLength;
	}

	public Long getSubscribePlanNo() {
		return subscribePlanNo;
	}

	public void setSubscribePlanNo(Long subscribePlanNo) {
		this.subscribePlanNo = subscribePlanNo;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public List<CustomerEnrollPo> getCustomerEnrollPos() {
		return this.customerEnrollPos;
	}

	public void setCustomerEnrollPos(List<CustomerEnrollPo> customerEnrollPos) {
		this.customerEnrollPos = customerEnrollPos;
	}

	public CustomerEnrollPo addCustomerEnrollPo(CustomerEnrollPo customerEnrollPo) {
		getCustomerEnrollPos().add(customerEnrollPo);
		customerEnrollPo.setCustomerEnrollment(this);

		return customerEnrollPo;
	}

	public CustomerEnrollPo removeCustomerEnrollPo(CustomerEnrollPo customerEnrollPo) {
		getCustomerEnrollPos().remove(customerEnrollPo);
		customerEnrollPo.setCustomerEnrollment(null);

		return customerEnrollPo;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		/*if(customerId instanceof Long){
			this.customerId = ((Long) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}*/
		
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}

}