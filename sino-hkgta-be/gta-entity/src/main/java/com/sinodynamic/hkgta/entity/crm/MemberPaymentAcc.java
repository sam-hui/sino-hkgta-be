package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * The persistent class for the member_payment_acc database table.
 * 
 */
@Entity
@Table(name = "member_payment_acc")
@NamedQuery(name = "MemberPaymentAcc.findAll", query = "SELECT m FROM MemberPaymentAcc m")
public class MemberPaymentAcc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "acc_id", unique = true, nullable = false)
	private String accId;

	@Column(name = "customer_id", insertable = false, updatable = false)
	private Long customerId;

	@Column(name = "acc_type", length = 10)
	private String accType;

	@Column(name = "account_no", length = 50)
	private String accountNo;

	@Column(name = "bank_id", length = 20)
	private String bankId;

	@Column(name = "status", length = 10)
	private String status;

	@Column(length = 300)
	private String remark;
	
	@Column(name = "originator_bank_code", length = 10)
	private String originatorBankCode;
	
	@Column(name = "drawer_bank_code", length = 10)
	private String drawerBankCode;
	
	@Column(name = "bank_customer_id", length = 50)
	private String bankCustomerId;
	
	@Column(name = "bank_account_name", length = 40)
	private String bankAccountName;

	// bi-directional many-to-one association to Member
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "customer_id", nullable = false)
	@JsonIgnore
	private Member member;

	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	public MemberPaymentAcc() {
	}

	public String getAccId() {
		return this.accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	public String getAccType() {
		return this.accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public String getAccountNo() {
		return this.accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getBankId() {
		return this.bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOriginatorBankCode() {
		return originatorBankCode;
	}

	public void setOriginatorBankCode(String originatorBankCode) {
		this.originatorBankCode = originatorBankCode;
	}

	public String getDrawerBankCode() {
		return drawerBankCode;
	}

	public void setDrawerBankCode(String drawerBankCode) {
		this.drawerBankCode = drawerBankCode;
	}

	public String getBankCustomerId() {
		return bankCustomerId;
	}

	public void setBankCustomerId(String bankCustomerId) {
		this.bankCustomerId = bankCustomerId;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	

}