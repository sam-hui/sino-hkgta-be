package com.sinodynamic.hkgta.entity.crm;

import java.util.Date;

import com.sinodynamic.hkgta.entity.EntityHelper;

public class EnrollmentHistoryDto {
	private Date joinDate;
	private Date expiryDate;
	public String getJoinDate() {
		if (joinDate == null) {
			return "";
		}
		return EntityHelper.date2String(joinDate, "yyyy-MMM-dd");
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public String getExpiryDate() {
		if (expiryDate == null) {
			return "";
		}
		return EntityHelper.date2String(expiryDate, "yyyy-MMM-dd");
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
}
