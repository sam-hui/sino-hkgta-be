package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the member_reserved_staff database table.
 * 
 */
@Entity
@Table(name="member_reserved_staff")
@NamedQuery(name="MemberReservedStaff.findAll", query="SELECT f FROM MemberReservedStaff f")
public class MemberReservedStaff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="staff_timeslot_id", unique=true, nullable=false, length=20)
	private String staffTimeslotId;

	@Column(name="resv_id", unique=true, nullable=false, length=20)
	private Long resvId;
	
	public MemberReservedStaff() {
	}
	public String getStaffTimeslotId() {
		return staffTimeslotId;
	}


	public void setStaffTimeslotId(String staffTimeslotId) {
		this.staffTimeslotId = staffTimeslotId;
	}



	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

}