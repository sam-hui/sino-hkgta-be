package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the login_session database table.
 * 
 */
@Embeddable
public class LoginSessionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="user_id", insertable=false, updatable=false, unique=true, nullable=false)
	private String userId;

	@Column(name="device_access", insertable=false, updatable=false, unique=true, nullable=false)
	private String deviceAccess;

	public LoginSessionPK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LoginSessionPK)) {
			return false;
		}
		LoginSessionPK castOther = (LoginSessionPK)other;
		return 
			(this.userId == castOther.userId)
			&& (this.deviceAccess == castOther.deviceAccess);
	}

	public int hashCode() {
		final Long prime = 31L;
		Long hash = 17L;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.deviceAccess.hashCode();
		
		return hash.intValue();
	}
	
	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getDeviceAccess() {
		return deviceAccess;
	}


	public void setDeviceAccess(String accessDevice) {
		this.deviceAccess = accessDevice;
	}
}