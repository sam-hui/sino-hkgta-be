package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the sys_code database table.
 * 
 */
@Entity
@Table(name="sys_code")
@NamedQuery(name="SysCode.findAll", query="SELECT s FROM SysCode s")
public class SysCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false)
	private Long sysId;

	@Column(nullable=false, length=20)
	private String category;

	@Column(name="code_display_nls", length=100)
	private String codeDisplayNls;
	
	@Column(name="code_display", length=100)
	private String codeDisplay;

	@Column(name="code_value", nullable=false, length=10)
	private String codeValue;

	@Column(length=200)
	private String description;
	
	@Column(name="description_nls",length=200)
	private String descriptionNls;

	@Column(name="display_order")
	private Long displayOrder;

	public SysCode() {
	}
	
	public SysCode(String codeValue, String codeDisplay,String codeDisplayNls) {
		this.codeValue = codeValue;
		this.codeDisplay = codeDisplay;
		this.codeDisplayNls = codeDisplayNls;
	}
	
	public SysCode(String codeValue, String category) {
		this.codeValue = codeValue;
		this.category = category;
	}
	
	public SysCode(String category) {
		this.category = category;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCodeDisplay() {
		return this.codeDisplay;
	}

	public void setCodeDisplay(String codeDisplay) {
		this.codeDisplay = codeDisplay;
	}

	public String getCodeValue() {
		return this.codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getCodeDisplayNls() {
		return codeDisplayNls;
	}

	public void setCodeDisplayNls(String codeDisplayNls) {
		this.codeDisplayNls = codeDisplayNls;
	}

	public String getDescriptionNls() {
		return descriptionNls;
	}

	public void setDescriptionNls(String descriptionNls) {
		this.descriptionNls = descriptionNls;
	}
	
	@Override
	public int hashCode()
	{
		return category.hashCode() + codeValue.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null)
		{
			return false;
		}

		if (obj != null && obj instanceof SysCode)
		{
			if (((SysCode) obj).category.equals(this.category) && ((SysCode) obj).codeValue.equals(this.codeValue))
				return true;
		}

		return false;
	}

}