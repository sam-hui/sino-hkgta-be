package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name="spa_category")
public class SpaCategory implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="category_id", unique=true, nullable=false)
	private String categoryId;
	
	@Column(name="category_name")
	private String categoryName;
	
	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="display_order")
	private Long displayOrder;
	
	@OneToMany(mappedBy="spaCategory")
	private List<SpaPicPath> spaPicPaths;

	
	public SpaCategory(){
		
	}
	
	public Long getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getCategoryId() {
		return categoryId;
	}



	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}



	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public List<SpaPicPath> getSpaPicPaths() {
		return spaPicPaths;
	}

	public void setSpaPicPaths(List<SpaPicPath> spaPicPaths) {
		this.spaPicPaths = spaPicPaths;
	}
	
	
}
