package com.sinodynamic.hkgta.entity.pos;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "restaurant_master")
public class RestaurantMaster implements Serializable
{
	private static final long serialVersionUID = 5911666142185479367L;

	@Id
	@Column(name = "restaurant_id", unique = true, nullable = false)
	private String restaurantId;
	
	@Column(name = "restaurant_name")
	private String restaurantName;
	
	@Column(name = "cuisine_type")
	private String cuisineType;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "venue_code")
	private String venueCode;
	
	@Type(type="yes_no")
	@Column(name = "allow_online_order")
	private Boolean allowOnlineOrder;
	
	@Column(name = "open_hour_text")
	private String openHourText;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "tab_occupy_duration")
	private Long tabOccupyDuration;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@OneToMany(mappedBy = "restaurantMaster", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<RestaurantOpenHour> restaurantOpenHours;

	@OneToMany(mappedBy = "restaurantMaster", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<RestaurantBookingQuota> restaurantBookingQuotas;

	@OneToMany(mappedBy = "restaurantMaster", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<RestaurantImage> restaurantImages;
	
	@ManyToMany(fetch = FetchType.LAZY)
	private List<RestaurantMenuCat> restaurantMenuCats;
	
	public String getRestaurantId()
	{
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId)
	{
		this.restaurantId = restaurantId;
	}

	public String getRestaurantName()
	{
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName)
	{
		this.restaurantName = restaurantName;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getVenueCode()
	{
		return venueCode;
	}

	public void setVenueCode(String venueCode)
	{
		this.venueCode = venueCode;
	}

	public Boolean getAllowOnlineOrder() {
		return allowOnlineOrder;
	}

	public void setAllowOnlineOrder(Boolean allowOnlineOrder) {
		this.allowOnlineOrder = allowOnlineOrder;
	}

	public String getOpenHourText()
	{
		return openHourText;
	}

	public void setOpenHourText(String openHourText)
	{
		this.openHourText = openHourText;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public Long getTabOccupyDuration()
	{
		return tabOccupyDuration;
	}

	public void setTabOccupyDuration(Long tabOccupyDuration)
	{
		this.tabOccupyDuration = tabOccupyDuration;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public Long getVerNo()
	{
		return verNo;
	}

	public void setVerNo(Long verNo)
	{
		this.verNo = verNo;
	}

	public List<RestaurantOpenHour> getRestaurantOpenHours()
	{
		return restaurantOpenHours;
	}

	public void setRestaurantOpenHours(List<RestaurantOpenHour> restaurantOpenHours)
	{
		this.restaurantOpenHours = restaurantOpenHours;
	}

	public String getCuisineType()
	{
		return cuisineType;
	}

	public void setCuisineType(String cuisineType)
	{
		this.cuisineType = cuisineType;
	}

	public List<RestaurantImage> getRestaurantImages() {
		return restaurantImages;
	}

	public void setRestaurantImages(List<RestaurantImage> restaurantImages) {
		this.restaurantImages = restaurantImages;
	}

	public List<RestaurantMenuCat> getRestaurantMenuCats() {
		return restaurantMenuCats;
	}

	public void setRestaurantMenuCats(List<RestaurantMenuCat> restaurantMenuCats) {
		this.restaurantMenuCats = restaurantMenuCats;
	}

	public List<RestaurantBookingQuota> getRestaurantBookingQuotas() {
		return restaurantBookingQuotas;
	}

	public void addRestaurantBookingQuota(RestaurantBookingQuota restaurantBookingQuota) {
		this.restaurantBookingQuotas.add(restaurantBookingQuota);
	}

	public void removeRestaurantBookingQuota(RestaurantBookingQuota restaurantBookingQuota) {
		this.restaurantBookingQuotas.remove(restaurantBookingQuota);
	}

}
