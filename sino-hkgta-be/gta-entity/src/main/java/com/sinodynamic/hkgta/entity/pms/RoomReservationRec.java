package com.sinodynamic.hkgta.entity.pms;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name="room_reservation_rec")
public class RoomReservationRec  implements java.io.Serializable {
     private static final long serialVersionUID = 1L;
     
	 private Long resvId;
     private String confirmId;
     private String roomTypeCode;
     private Long roomId;
     private Long customerId;
     private Date requestDate;
     private Date arrivalDate;
     private Date departureDate;
     private Date checkinTimestamp;
     private Long night;
     private Date checkoutTimestamp;
     private String checkinMethod;
     private Date createDate;
     private String createBy;
     private Date updateDate;
     private String updateBy;
     private Long verNo;
     private Long orderNo;
     private String status;

    public RoomReservationRec() {
    }
    
    @Id 
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="resv_id", unique=true, nullable=false)
    public Long getResvId() {
        return this.resvId;
    }
    
    public void setResvId(Long resvId) {
        this.resvId = resvId;
    }
    
    @Column(name="confirm_id")
    public String getConfirmId() {
        return this.confirmId;
    }
    
    public void setConfirmId(String confirmId) {
        this.confirmId = confirmId;
    }
    
    @Column(name="room_type_code", length=10)
    public String getRoomTypeCode() {
        return this.roomTypeCode;
    }
    
    public void setRoomTypeCode(String roomTypeCode) {
        this.roomTypeCode = roomTypeCode;
    }
    
    @Column(name="room_id")
    public Long getRoomId() {
        return this.roomId;
    }
    
    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }
    
    @Column(name="customer_id")
    public Long getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="request_date", length=19)
    public Date getRequestDate() {
        return this.requestDate;
    }
    
    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="arrival_date", length=10)
    public Date getArrivalDate() {
        return this.arrivalDate;
    }
    
    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="departure_date", length=10)
    public Date getDepartureDate() {
        return this.departureDate;
    }
    
    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="checkin_timestamp", length=19)
    public Date getCheckinTimestamp() {
        return this.checkinTimestamp;
    }
    
    public void setCheckinTimestamp(Date checkinTimestamp) {
        this.checkinTimestamp = checkinTimestamp;
    }
    
    @Column(name="night")
    public Long getNight() {
        return this.night;
    }
    
    public void setNight(Long night) {
        this.night = night;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="checkout_timestamp", length=19)
    public Date getCheckoutTimestamp() {
        return this.checkoutTimestamp;
    }
    
    public void setCheckoutTimestamp(Date checkoutTimestamp) {
        this.checkoutTimestamp = checkoutTimestamp;
    }
    
    @Column(name="checkin_method", length=10)
    public String getCheckinMethod() {
        return this.checkinMethod;
    }
    
    public void setCheckinMethod(String checkinMethod) {
        this.checkinMethod = checkinMethod;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=19)
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="create_by", length=50)
    public String getCreateBy() {
        return this.createBy;
    }
    
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="update_date", length=19)
    public Date getUpdateDate() {
        return this.updateDate;
    }
    
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    @Column(name="update_by", length=50)
    public String getUpdateBy() {
        return this.updateBy;
    }
    
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    
    @Version
    @Column(name="ver_no")
    public Long getVerNo() {
        return this.verNo;
    }
    
    public void setVerNo(Long verNo) {
        this.verNo = verNo;
    }
    
    @Column(name="order_no")
	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="status")
	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}
}


