package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the facility_item_attribute database table.
 * 
 */
@Embeddable
public class FacilityItemAttributePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="attribute_no", unique=true, nullable=false, length=40)
	private String attributeNo;

	@Column(name="facility_no", insertable=false, updatable=false, unique=true, nullable=false, length=20)
	private Long facilityNo;

	public FacilityItemAttributePK() {
	}
	public String getAttributeNo() {
		return this.attributeNo;
	}
	public void setAttributeNo(String attributeNo) {
		this.attributeNo = attributeNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FacilityItemAttributePK)) {
			return false;
		}
		FacilityItemAttributePK castOther = (FacilityItemAttributePK)other;
		return 
			this.attributeNo.equals(castOther.attributeNo)
			&& this.facilityNo==castOther.facilityNo;
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.attributeNo.hashCode();
		hash = hash * prime + this.facilityNo.intValue();
		
		return hash;
	}
	public Long getFacilityNo() {
		return facilityNo;
	}
	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}
}