package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="cashvalue_topup_history")
public class CashvalueTopupHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;

	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "topup_method")
	private String topupMethod;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "cashvalue_amt")
	private BigDecimal cashvalueAmt;

	@Column(name = "topup_date")
	private Date topupDate;

	@Column(name = "success_date")
	private Date successDate;

	@Column(name = "bank_file_return")
	private String bankFileReturn;
	
	@Column(name = "create_date")
	private Date createDate;

	@Transient
	private String accNo;
	
	@Column(name="status")
	private String status;
	
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ref_history_id")
	private CashvalueTopupHistory cashvalueTopupHistory;
	 
	
	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getTopupMethod() {
		return topupMethod;
	}

	public void setTopupMethod(String topupMethod) {
		this.topupMethod = topupMethod;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCashvalueAmt() {
		return cashvalueAmt;
	}

	public void setCashvalueAmt(BigDecimal cashvalueAmt) {
		this.cashvalueAmt = cashvalueAmt;
	}

	public Date getTopupDate() {
		return topupDate;
	}

	public void setTopupDate(Date topupDate) {
		this.topupDate = topupDate;
	}

	public Date getSuccessDate() {
		return successDate;
	}

	public void setSuccessDate(Date successDate) {
		this.successDate = successDate;
	}

	public String getBankFileReturn() {
		return bankFileReturn;
	}

	public void setBankFileReturn(String bankFileReturn) {
		this.bankFileReturn = bankFileReturn;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CashvalueTopupHistory getCashvalueTopupHistory() {
		return cashvalueTopupHistory;
	}

	public void setCashvalueTopupHistory(CashvalueTopupHistory cashvalueTopupHistory) {
		this.cashvalueTopupHistory = cashvalueTopupHistory;
	}
	
	

}
