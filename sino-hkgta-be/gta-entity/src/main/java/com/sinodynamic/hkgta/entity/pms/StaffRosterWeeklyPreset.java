package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "staff_roster_weekly_preset")
public class StaffRosterWeeklyPreset  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "preset_id", unique = true, nullable = false)
	private Long presetId;

	@Column(name = "preset_name", nullable = false)
	private String presetName;

	@Column(name = "week_day", length = 2)
	private Long weekDay;

	@Type(type="yes_no")
	@Column(name = "off_duty")
	private boolean offDuty;
	
	@Column(name = "begin_time")
	private Long beginTime;

	@Column(name = "end_time")
	private Long endTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "create_by", length = 50)
	private String createBy;

	public Long getPresetId() {
		return presetId;
	}

	public void setPresetId(Long presetId) {
		this.presetId = presetId;
	}

	public String getPresetName() {
		return presetName;
	}

	public void setPresetName(String presetName) {
		this.presetName = presetName;
	}

	public Long getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(Long weekDay) {
		this.weekDay = weekDay;
	}

	public boolean getOffDuty() {
		return offDuty;
	}

	public void setOffDuty(boolean offDuty) {
		this.offDuty = offDuty;
	}

	public Long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Long beginTime) {
		this.beginTime = beginTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}
