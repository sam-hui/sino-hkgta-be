package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;



/**
 * The persistent class for the course_session database table.
 * 
 */
@Entity
@Table(name="course_session")
@NamedQuery(name="CourseSession.findAll", query="SELECT c FROM CourseSession c")
public class CourseSession  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false)
	private Long sysId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="begin_datetime")
	private Date beginDatetime;

	@Column(name="coach_timeslot_id")
	private Long coachTimeslotId;

	@Column(name="create_by", nullable=false, length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_datetime")
	private Date endDatetime;

	@Column(name="session_no")
	private Long sessionNo;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="course_id")
	private Long courseId;
	
	@Column(name="gather_location")
	private String gatherLocation;
	
	@Column(name="status")
	private String status;

	@Column(name="coach_user_id")
	private String coachUserId;
	
	@Column(name="other_train_location")
	private String otherTrainLocation;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Object version) {
		this.version = (version!=null ? NumberUtils.toLong(version.toString()):null);
	}
	
	public CourseSession() {
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Object sysId) {
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}

	public Date getBeginDatetime() {
		return this.beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public Long getCoachTimeslotId() {
		return this.coachTimeslotId;
	}

	public void setCoachTimeslotId(Object coachTimeslotId) {
		this.coachTimeslotId = NumberUtils.toLong(coachTimeslotId.toString());
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public Long getSessionNo() {
		return this.sessionNo;
	}

	public void setSessionNo(Object sessionNo) {
		this.sessionNo = (sessionNo!=null ? NumberUtils.toLong(sessionNo.toString()):null);
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getCourseId() {
	    return courseId;
	}

	public void setCourseId(Object courseId) {
	    this.courseId = (courseId!=null ? NumberUtils.toLong(courseId.toString()):null);
	}

	public String getGatherLocation() {
	    return gatherLocation;
	}

	public void setGatherLocation(String gatherLocation) {
	    this.gatherLocation = gatherLocation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCoachUserId() {
	    return coachUserId;
	}

	public void setCoachUserId(String coachUserId) {
	    this.coachUserId = coachUserId;
	}

	public String getOtherTrainLocation() {
	    return otherTrainLocation;
	}

	public void setOtherTrainLocation(String otherTrainLocation) {
	    this.otherTrainLocation = otherTrainLocation;
	}
	
}