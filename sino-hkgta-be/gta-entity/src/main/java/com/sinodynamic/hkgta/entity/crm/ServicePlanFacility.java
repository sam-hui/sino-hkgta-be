package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the service_plan_facility database table.
 * 
 */
@Entity
@Table(name="service_plan_facility")
@NamedQuery(name="ServicePlanFacility.findAll", query="SELECT s FROM ServicePlanFacility s")
public class ServicePlanFacility implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ServicePlanFacilityPK id;

	@Column(length=2)
	private String permission;


	@Column(name="sys_id", nullable=false)
	private Long sysId;

	@Column(name="facility_type_code", nullable=false, insertable=false, updatable=false)
	private String facilityTypeCode;

	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="service_plan_id", nullable=false, insertable=false, updatable=false)
	private ServicePlan servicePlan;
	
	public ServicePlanFacility() {
	}

	public ServicePlanFacilityPK getId() {
		return this.id;
	}

	public void setId(ServicePlanFacilityPK id) {
		this.id = id;
	}

	public String getPermission() {
		return this.permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getFacilityTypeCode() {
		return facilityTypeCode;
	}

	public void setFacilityTypeCode(String facilityTypeCode) {
		this.facilityTypeCode = facilityTypeCode;
	}

	public ServicePlan getServicePlan() {
		return this.servicePlan;
	}

	public void setServicePlan(ServicePlan servicePlan) {
		this.servicePlan = servicePlan;
	}
}