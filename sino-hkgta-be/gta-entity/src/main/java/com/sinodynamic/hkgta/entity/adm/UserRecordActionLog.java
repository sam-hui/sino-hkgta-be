package com.sinodynamic.hkgta.entity.adm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_record_action_log")
public class UserRecordActionLog implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue()
	@Column(name = "sys_id")
	private Long sysId;

	@Column(name = "table_name")
	private String tableName;

	@Column(name = "action")
	private String action;

	@Column(name = "primary_key_value")
	private String primaryKeyValue;

	@Column(name = "before_image")
	private String beforeImage;

	@Column(name = "action_timestamp")
	private Date actionTimestamp;

	@Column(name = "action_user_id")
	private String actionUserId;

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPrimaryKeyValue() {
		return primaryKeyValue;
	}

	public void setPrimaryKeyValue(String primaryKeyValue) {
		this.primaryKeyValue = primaryKeyValue;
	}

	public String getBeforeImage() {
		return beforeImage;
	}

	public void setBeforeImage(String beforeImage) {
		this.beforeImage = beforeImage;
	}

	public Date getActionTimestamp() {
		return actionTimestamp;
	}

	public void setActionTimestamp(Date actionTimestamp) {
		this.actionTimestamp = actionTimestamp;
	}

	public String getActionUserId() {
		return actionUserId;
	}

	public void setActionUserId(String actionUserId) {
		this.actionUserId = actionUserId;
	}

}
