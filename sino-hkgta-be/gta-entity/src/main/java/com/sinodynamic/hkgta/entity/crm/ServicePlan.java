package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the service_plan database table.
 * 
 */
@Entity
@Table(name="service_plan")
@NamedQuery(name="ServicePlan.findAll", query="SELECT s FROM ServicePlan s")
public class ServicePlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="plan_no", unique=true, nullable=false)
	private Long  planNo;

	@Column(name="contract_length_month")
	private Long contractLengthMonth;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_end_date")
	private Date effectiveEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_start_date")
	private Date effectiveStartDate;

	@Column(name="pass_nature_code", length=10)
	private String passNatureCode;

	@Column(name="pass_period_type", length=10)
	private String passPeriodType;

	@Column(name="plan_name", nullable=false, length=50)
	private String planName;

	@Column(length=50)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="subscriber_type", length=10)
	private String subscriberType;
	//bi-directional many-to-one association to ServicePlanAdditionRule
	@OneToMany(cascade=CascadeType.ALL, mappedBy="servicePlan" )
	@OrderBy(value = "ruleId asc")
	private List<ServicePlanAdditionRule> servicePlanAdditionRules;

	//bi-directional many-to-one association to ServicePlanFacility
	@OneToMany(mappedBy="servicePlan", cascade=CascadeType.ALL)
	private List<ServicePlanFacility> servicePlanFacilities;

	//bi-directional many-to-one association to ServicePlanPo
	@OneToMany(cascade=CascadeType.ALL, mappedBy="servicePlan")
	private List<ServicePlanPos> servicePlanPoses;
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public ServicePlan() {
	}

	public ServicePlan(Long planNo, Long contractLengthMonth, String createBy,
			Date createDate, Date effectiveEndDate, Date effectiveStartDate,
			String passNatureCode, String passPeriodType, String planName,
			String status, String updateBy, Date updateDate) {
		this.planNo = planNo;
		this.contractLengthMonth = contractLengthMonth;
		this.createBy = createBy;
		this.createDate = createDate;
		this.effectiveEndDate = effectiveEndDate;
		this.effectiveStartDate = effectiveStartDate;
		this.passNatureCode = passNatureCode;
		this.passPeriodType = passPeriodType;
		this.planName = planName;
		this.status = status;
		this.updateBy = updateBy;
		this.updateDate = updateDate;
	}
	
	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	
	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}

	public Long getContractLengthMonth() {
		return contractLengthMonth;
	}

	public void setContractLengthMonth(Long contractLengthMonth) {
		this.contractLengthMonth = contractLengthMonth;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getEffectiveEndDate() {
		return this.effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Date getEffectiveStartDate() {
		return this.effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getPassNatureCode() {
		return this.passNatureCode;
	}

	public void setPassNatureCode(String passNatureCode) {
		this.passNatureCode = passNatureCode;
	}

	public String getPassPeriodType() {
		return this.passPeriodType;
	}

	public void setPassPeriodType(String passPeriodType) {
		this.passPeriodType = passPeriodType;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public List<ServicePlanAdditionRule> getServicePlanAdditionRules() {
		return this.servicePlanAdditionRules;
	}

	public void setServicePlanAdditionRules(List<ServicePlanAdditionRule> servicePlanAdditionRules) {
		this.servicePlanAdditionRules = servicePlanAdditionRules;
	}

	public ServicePlanAdditionRule addServicePlanAdditionRule(ServicePlanAdditionRule servicePlanAdditionRule) {
		getServicePlanAdditionRules().add(servicePlanAdditionRule);
		servicePlanAdditionRule.setServicePlan(this);

		return servicePlanAdditionRule;
	}

	public ServicePlanAdditionRule removeServicePlanAdditionRule(ServicePlanAdditionRule servicePlanAdditionRule) {
		getServicePlanAdditionRules().remove(servicePlanAdditionRule);
		servicePlanAdditionRule.setServicePlan(null);

		return servicePlanAdditionRule;
	}

	public List<ServicePlanFacility> getServicePlanFacilities() {
		return this.servicePlanFacilities;
	}

	public void setServicePlanFacilities(List<ServicePlanFacility> servicePlanFacilities) {
		this.servicePlanFacilities = servicePlanFacilities;
	}

	public ServicePlanFacility addServicePlanFacility(ServicePlanFacility servicePlanFacility) {
		getServicePlanFacilities().add(servicePlanFacility);
		servicePlanFacility.setServicePlan(this);

		return servicePlanFacility;
	}

	public ServicePlanFacility removeServicePlanFacility(ServicePlanFacility servicePlanFacility) {
		getServicePlanFacilities().remove(servicePlanFacility);
		servicePlanFacility.setServicePlan(null);

		return servicePlanFacility;
	}

	public List<ServicePlanPos> getServicePlanPoses() {
		return this.servicePlanPoses;
	}

	public void setServicePlanPoses(List<ServicePlanPos> servicePlanPoses) {
		this.servicePlanPoses = servicePlanPoses;
	}

	public ServicePlanPos addServicePlanPos(ServicePlanPos servicePlanPos) {
		getServicePlanPoses().add(servicePlanPos);
		servicePlanPos.setServicePlan(this);

		return servicePlanPos;
	}

	public ServicePlanPos removeServicePlanPos(ServicePlanPos servicePlanPos) {
		getServicePlanPoses().remove(servicePlanPos);
		servicePlanPos.setServicePlan(null);

		return servicePlanPos;
	}
	
}