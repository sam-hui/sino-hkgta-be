package com.sinodynamic.hkgta.entity.ibeacon;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ibeacon_nearby_person database table.
 * 
 */
@Embeddable
public class IbeaconNearbyPersonPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="user_id")
	private String userId;

	@Column(name="ibeacon_id")
	private Long ibeaconId;

	public IbeaconNearbyPersonPK() {
	}

	public Long getIbeaconId() {
		return this.ibeaconId;
	}
	public void setIbeaconId(Long ibeaconId) {
		this.ibeaconId = ibeaconId;
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof IbeaconNearbyPersonPK)) {
			return false;
		}
		IbeaconNearbyPersonPK castOther = (IbeaconNearbyPersonPK)other;
		return 
			this.userId.equals(castOther.userId)
			&& (this.ibeaconId == castOther.ibeaconId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.ibeaconId.intValue();
		
		return hash;
	}
}