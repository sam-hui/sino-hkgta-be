package com.sinodynamic.hkgta.entity.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPo;


/**
 * The persistent class for the customer_order_hd database table.
 * 
 */
@Entity
@Table(name="customer_order_hd")
@NamedQuery(name="CustomerOrderHd.findAll", query="SELECT c FROM CustomerOrderHd c")
public class CustomerOrderHd implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_no", unique=true, nullable=false)
	private Long orderNo;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="customer_id")
	private Long customerId;

	@Temporal(TemporalType.DATE)
	@Column(name="order_date")
	private Date orderDate;

	@Column(name="order_remark", length=250)
	private String orderRemark;

	@Column(name="order_status", length=10)
	private String orderStatus;

	@Column(name="order_total_amount", precision=10, scale=2)
	private BigDecimal orderTotalAmount;

	@Column(name="purchased_location_code", length=50)
	private String purchasedLocationCode;

	@Column(name="purchaser_name", length=80)
	private String purchaserName;

	@Column(name="staff_user_id", length=50)
	private String staffUserId;

	@Column(name="update_by", length=50)
	private String updateBy;
	
	@Column(name="vendor_reference_code", length=50)
	private String vendorRefCode;
	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;
	//bi-directional many-to-one association to CustomerEnrollPo
	//@OneToMany(mappedBy="customerOrderHd")
	@Transient
	@JsonIgnore
	private List<CustomerEnrollPo> customerEnrollPos;

	//bi-directional many-to-one association to CustomerOrderDet
	@OneToMany(mappedBy="customerOrderHd")
	@JsonIgnore
	private List<CustomerOrderDet> customerOrderDets;

	//bi-directional many-to-one association to CustomerOrderPermitCard
	@OneToMany(mappedBy="customerOrderHd")
	private List<CustomerOrderPermitCard> customerOrderPermitCards;

	//bi-directional many-to-one association to CustomerOrderTran
	@OneToMany(mappedBy="customerOrderHd",cascade = CascadeType.MERGE)
	private List<CustomerOrderTrans> customerOrderTranses;

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public CustomerOrderHd() {
	}

	public Long getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(Object orderNo) {
		this.orderNo = (orderNo!=null ? NumberUtils.toLong(orderNo.toString()):null);
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderRemark() {
		return this.orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public BigDecimal getOrderTotalAmount() {
		return this.orderTotalAmount;
	}

	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	public String getPurchasedLocationCode() {
		return purchasedLocationCode;
	}

	public void setPurchasedLocationCode(String purchasedLocationCode) {
		this.purchasedLocationCode = purchasedLocationCode;
	}

	public String getPurchaserName() {
		return this.purchaserName;
	}

	public void setPurchaserName(String purchaserName) {
		this.purchaserName = purchaserName;
	}

	public String getStaffUserId() {
		return this.staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<CustomerEnrollPo> getCustomerEnrollPos() {
		return this.customerEnrollPos;
	}

	public void setCustomerEnrollPos(List<CustomerEnrollPo> customerEnrollPos) {
		this.customerEnrollPos = customerEnrollPos;
	}
	
	

	public String getVendorRefCode() {
		return vendorRefCode;
	}

	public void setVendorRefCode(String vendorRefCode) {
		this.vendorRefCode = vendorRefCode;
	}

	public CustomerEnrollPo addCustomerEnrollPo(CustomerEnrollPo customerEnrollPo) {
		getCustomerEnrollPos().add(customerEnrollPo);
		customerEnrollPo.setCustomerOrderHd(this);

		return customerEnrollPo;
	}

	public CustomerEnrollPo removeCustomerEnrollPo(CustomerEnrollPo customerEnrollPo) {
		getCustomerEnrollPos().remove(customerEnrollPo);
		customerEnrollPo.setCustomerOrderHd(null);

		return customerEnrollPo;
	}

	public List<CustomerOrderDet> getCustomerOrderDets() {
		return this.customerOrderDets;
	}

	public void setCustomerOrderDets(List<CustomerOrderDet> customerOrderDets) {
		this.customerOrderDets = customerOrderDets;
	}

	public CustomerOrderDet addCustomerOrderDet(CustomerOrderDet customerOrderDet) {
		getCustomerOrderDets().add(customerOrderDet);
		customerOrderDet.setCustomerOrderHd(this);

		return customerOrderDet;
	}

	public CustomerOrderDet removeCustomerOrderDet(CustomerOrderDet customerOrderDet) {
		getCustomerOrderDets().remove(customerOrderDet);
		customerOrderDet.setCustomerOrderHd(null);

		return customerOrderDet;
	}

	public List<CustomerOrderPermitCard> getCustomerOrderPermitCards() {
		return this.customerOrderPermitCards;
	}

	public void setCustomerOrderPermitCards(List<CustomerOrderPermitCard> customerOrderPermitCards) {
		this.customerOrderPermitCards = customerOrderPermitCards;
	}

	public CustomerOrderPermitCard addCustomerOrderPermitCard(CustomerOrderPermitCard customerOrderPermitCard) {
		getCustomerOrderPermitCards().add(customerOrderPermitCard);
		customerOrderPermitCard.setCustomerOrderHd(this);

		return customerOrderPermitCard;
	}

	public CustomerOrderPermitCard removeCustomerOrderPermitCard(CustomerOrderPermitCard customerOrderPermitCard) {
		getCustomerOrderPermitCards().remove(customerOrderPermitCard);
		customerOrderPermitCard.setCustomerOrderHd(null);

		return customerOrderPermitCard;
	}

	public List<CustomerOrderTrans> getCustomerOrderTrans() {
		return this.customerOrderTranses;
	}

	public void setCustomerOrderTrans(List<CustomerOrderTrans> customerOrderTranses) {
		this.customerOrderTranses = customerOrderTranses;
	}

	public CustomerOrderTrans addCustomerOrderTrans(CustomerOrderTrans customerOrderTrans) {
		getCustomerOrderTrans().add(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(this);

		return customerOrderTrans;
	}

	public CustomerOrderTrans removeCustomerOrderTrans(CustomerOrderTrans customerOrderTrans) {
		getCustomerOrderTrans().remove(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(null);

		return customerOrderTrans;
	}

}