package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the notice database table.
 * 
 */
@Entity
@Table(name="notice")
@NamedQuery(name="Notice.findAll", query="SELECT n FROM Notice n")
public class Notice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notice_id", unique=true, nullable=false)
	private Long noticeId;

	@Column(length=250)
	private String subject;
	
	@Column(length=3000)
	private String content;

	@Column(name="notice_type", length=3)
	private String noticeType;

	@Column(name="recipient_type", length=10)
	private String recipientType;
	
	@Temporal(TemporalType.DATE)
	@Column(name="notice_begin")
	private Date noticeBegin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notice_end")
	private Date noticeEnd;

	@Column(length=10)
	private String status;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	//bi-directional many-to-one association to NoticeRecipient
	@OneToMany(mappedBy="notice")
	private List<NoticeRecipient> noticeRecipients;

	@OneToMany(mappedBy="notice", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<NoticeFile> noticeFiles = new ArrayList<>();
	
	public Notice() {
	}

	
	public Long getVerNo() {
		return verNo;
	}


	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}


	public Long getNoticeId() {
		return this.noticeId;
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getNoticeBegin() {
		return this.noticeBegin;
	}

	public void setNoticeBegin(Date noticeBegin) {
		this.noticeBegin = noticeBegin;
	}

	public Date getNoticeEnd() {
		return this.noticeEnd;
	}

	public void setNoticeEnd(Date noticeEnd) {
		this.noticeEnd = noticeEnd;
	}

	public String getNoticeType() {
		return this.noticeType;
	}

	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}

	public String getRecipientType() {
		return this.recipientType;
	}

	public void setRecipientType(String recipientType) {
		this.recipientType = recipientType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<NoticeRecipient> getNoticeRecipients() {
		return this.noticeRecipients;
	}

	public void setNoticeRecipients(List<NoticeRecipient> noticeRecipients) {
		this.noticeRecipients = noticeRecipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public NoticeRecipient addNoticeRecipient(NoticeRecipient noticeRecipient) {
		getNoticeRecipients().add(noticeRecipient);
		noticeRecipient.setNotice(this);

		return noticeRecipient;
	}

	public NoticeRecipient removeNoticeRecipient(NoticeRecipient noticeRecipient) {
		getNoticeRecipients().remove(noticeRecipient);
		noticeRecipient.setNotice(null);

		return noticeRecipient;
	}

	public List<NoticeFile> getNoticeFiles() {
		return noticeFiles;
	}

	public void addNoticeFiles(NoticeFile noticeFiles) {
		this.noticeFiles.add(noticeFiles);
	}

	public void removeNoticeFiles(NoticeFile noticeFiles) {
		this.noticeFiles.remove(noticeFiles);
	}
}