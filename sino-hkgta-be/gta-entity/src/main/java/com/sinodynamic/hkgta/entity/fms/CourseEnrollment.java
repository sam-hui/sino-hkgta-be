package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the course_enrollment database table.
 * 
 */
@Entity
@Table(name="course_enrollment")
@NamedQuery(name="CourseEnrollment.findAll", query="SELECT c FROM CourseEnrollment c")
public class CourseEnrollment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="enroll_id", unique=true, nullable=false)
	private String enrollId;

	@Column(name="course_id")
	private Long courseId;
	
	@Transient
	private Long courseIdValue;
	
	@Transient
	private Long customerIdValue;

	@Column(name="create_by", nullable=false, length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="cust_order_no")
	private Long custOrderDetId;

	@Column(name="customer_id")
	private Long customerId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="enroll_date")
	private Date enrollDate;

	@Column(name="internal_remark", length=300)
	private String internalRemark;

	@Column(length=10)
	private String status;

	@Column(name="status_update_by", length=50)
	private String statusUpdateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_update_date")
	private Date statusUpdateDate;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="cancel_requester_type")
	private String cancelRequesterType;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public CourseEnrollment() {
	}

	public String getEnrollId() {
		return this.enrollId;
	}

	public void setEnrollId(String enrollId) {
		this.enrollId = enrollId;
	}

	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Object courseId) {
		this.courseId = (courseId!=null ? NumberUtils.toLong(courseId.toString()):null);
	}

	public Long getCourseIdValue()
	{
		return courseIdValue;
	}

	public void setCourseIdValue(Object courseIdValue)
	{
		this.courseIdValue = (courseIdValue!=null ? NumberUtils.toLong(courseIdValue.toString()):null);
	}

	public Long getCustomerIdValue()
	{
		return customerIdValue;
	}

	public void setCustomerIdValue(Object customerIdValue)
	{
		this.customerIdValue = (customerIdValue!=null ? NumberUtils.toLong(customerIdValue.toString()):null);
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Long getCustOrderDetId() {
		return this.custOrderDetId;
	}

	public void setCustOrderDetId(Long custOrderDetId) {
		this.custOrderDetId = custOrderDetId;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getEnrollDate() {
		return this.enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusUpdateBy() {
		return this.statusUpdateBy;
	}

	public void setStatusUpdateBy(String statusUpdateBy) {
		this.statusUpdateBy = statusUpdateBy;
	}

	public Date getStatusUpdateDate() {
		return this.statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCancelRequesterType() {
	    return cancelRequesterType;
	}

	public void setCancelRequesterType(String cancelRequesterType) {
	    this.cancelRequesterType = cancelRequesterType;
	}
	
}