package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

/**
 * The persistent class for the facility_master database table.
 * 
 */
@Entity
@Table(name = "facility_master")
@NamedQuery(name = "FacilityMaster.findAll", query = "SELECT f FROM FacilityMaster f")
public class FacilityMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "facility_no", unique = true, nullable = false, length = 20)
	private Long facilityNo;

	@Column(name = "capacity" )
	private Long capacity;

	@Column(name = "venue_floor")
	private Long venueFloor;

	@OneToOne
	@JoinColumn(name = "venue_code")
	private VenueMaster venueMaster;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(name = "factory_serial_no", length = 50)
	private String factorySerialNo;
	
	@Column(name = "facility_description", length = 250)
	private String facilityDescription;

	@Column(name = "facility_name", length = 100)
	private String facilityName;

	@Column(length = 10)
	private String status;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	// bi-directional many-to-one association to FacilityType
	@ManyToOne
	@JoinColumn(name = "facility_type")
	private FacilityType facilityTypeBean;
	
	@Column(name = "facility_subtype_id", length = 50)
	private String facilitySubtypeId;

	// bi-directional many-to-one association to FacilityTimeslot
	@OneToMany(mappedBy = "facilityMaster", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<FacilityTimeslot> facilityTimeslots;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public FacilityMaster() {
	}

	public FacilityMaster(Long venueFloor) {
		super();
		this.venueFloor = venueFloor;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getFactorySerialNo(){
		return this.factorySerialNo;
	}
	
	public void setFactorySerialNo(String factorySerialNo){
		this.factorySerialNo = factorySerialNo;
	}
	
	public String getFacilityDescription() {
		return this.facilityDescription;
	}

	public void setFacilityDescription(String facilityDescription) {
		this.facilityDescription = facilityDescription;
	}

	public String getFacilityName() {
		return this.facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public FacilityType getFacilityTypeBean() {
		return this.facilityTypeBean;
	}

	public void setFacilityTypeBean(FacilityType facilityTypeBean) {
		this.facilityTypeBean = facilityTypeBean;
	}

	public List<FacilityTimeslot> getFacilityTimeslots() {
		return this.facilityTimeslots;
	}

	public void setFacilityTimeslots(List<FacilityTimeslot> facilityTimeslots) {
		this.facilityTimeslots = facilityTimeslots;
	}

	public FacilityTimeslot addFacilityTimeslot(FacilityTimeslot facilityTimeslot) {
		getFacilityTimeslots().add(facilityTimeslot);
		facilityTimeslot.setFacilityMaster(this);

		return facilityTimeslot;
	}

	public FacilityTimeslot removeFacilityTimeslot(FacilityTimeslot facilityTimeslot) {
		getFacilityTimeslots().remove(facilityTimeslot);
		facilityTimeslot.setFacilityMaster(null);

		return facilityTimeslot;
	}


	public Long getVenueFloor() {
		return venueFloor;
	}

	public void setVenueFloor(Object venueFloor) {
		this.venueFloor = venueFloor != null ? NumberUtils.toLong(venueFloor.toString()) : null;
	}

	public Long getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Object facilityNo) {
		this.facilityNo = (facilityNo!=null ? NumberUtils.toLong(facilityNo.toString()):null);
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Object capacity) {
		this.capacity = (capacity!=null ? NumberUtils.toLong(capacity.toString()):null);
	}

	public VenueMaster getVenueMaster() {
		return venueMaster;
	}

	public void setVenueMaster(VenueMaster venueMaster) {
		this.venueMaster = venueMaster;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Object verNo) {
		this.verNo = verNo != null ? NumberUtils.toLong(verNo.toString()) : null;
	}

	public String getFacilitySubtypeId()
	{
		return facilitySubtypeId;
	}

	public void setFacilitySubtypeId(String facilitySubtypeId)
	{
		this.facilitySubtypeId = facilitySubtypeId;
	}

}