package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the member_cashvalue_bal_history database table.
 * 
 */
@Entity
@Table(name="member_cashvalue_bal_history")
@NamedQuery(name="MemberCashvalueBalHistory.findAll", query="SELECT m FROM MemberCashvalueBalHistory m")
public class MemberCashvalueBalHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="process_id", unique=true, nullable=false)
	private Long processId;
	
	@Column(name="customer_id")
	private Long customerId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="cutoff_date")
	private Date cutoffDate;

	@Column(name="last_cashvalue_bal")
	private BigDecimal lastCashvalueBal;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_cashvalue_date")
	private Date lastCashvalueDate;

	@Column(name="process_date")
	private Timestamp processDate;

	@Column(name="recal_balance")
	private BigDecimal recalBalance;

	//bi-directional many-to-one association to MemberTransactionLog
	@OneToMany(mappedBy="memberCashvalueBalHistory")
	private List<MemberTransactionLog> memberTransactionLogs;

	public MemberCashvalueBalHistory() {
	}

	public Long getProcessId() {
		return this.processId;
	}

	public void setProcessId(Long processId) {
		this.processId = processId;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getCutoffDate() {
		return this.cutoffDate;
	}

	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	public BigDecimal getLastCashvalueBal() {
		return this.lastCashvalueBal;
	}

	public void setLastCashvalueBal(BigDecimal lastCashvalueBal) {
		this.lastCashvalueBal = lastCashvalueBal;
	}

	public Date getLastCashvalueDate() {
		return this.lastCashvalueDate;
	}

	public void setLastCashvalueDate(Date lastCashvalueDate) {
		this.lastCashvalueDate = lastCashvalueDate;
	}

	public Timestamp getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Timestamp processDate) {
		this.processDate = processDate;
	}

	public BigDecimal getRecalBalance() {
		return this.recalBalance;
	}

	public void setRecalBalance(BigDecimal recalBalance) {
		this.recalBalance = recalBalance;
	}

	public List<MemberTransactionLog> getMemberTransactionLogs() {
		return this.memberTransactionLogs;
	}

	public void setMemberTransactionLogs(List<MemberTransactionLog> memberTransactionLogs) {
		this.memberTransactionLogs = memberTransactionLogs;
	}

	public MemberTransactionLog addMemberTransactionLog(MemberTransactionLog memberTransactionLog) {
		getMemberTransactionLogs().add(memberTransactionLog);
		memberTransactionLog.setMemberCashvalueBalHistory(this);

		return memberTransactionLog;
	}

	public MemberTransactionLog removeMemberTransactionLog(MemberTransactionLog memberTransactionLog) {
		getMemberTransactionLogs().remove(memberTransactionLog);
		memberTransactionLog.setMemberCashvalueBalHistory(null);

		return memberTransactionLog;
	}

}