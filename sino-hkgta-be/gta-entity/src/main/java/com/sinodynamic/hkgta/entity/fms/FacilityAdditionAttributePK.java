package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the facility_addition_attribute database table.
 * 
 */
@Embeddable
public class FacilityAdditionAttributePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="attribute_id", unique=true, nullable=false, length=40)
	private String attributeId;

	@Column(name="facility_no", insertable=false, updatable=false, unique=true, nullable=false, length=20)
	private Long facilityNo;

	public FacilityAdditionAttributePK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FacilityAdditionAttributePK)) {
			return false;
		}
		FacilityAdditionAttributePK castOther = (FacilityAdditionAttributePK)other;
		return 
			this.attributeId.equals(castOther.attributeId)
			&& this.facilityNo== castOther.facilityNo;
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.attributeId.hashCode();
		hash = hash * prime + this.facilityNo.intValue();
		
		return hash;
	}
	
	public Long getFacilityNo() {
		return facilityNo;
	}
	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}

	public String getAttributeId()
	{
		return attributeId;
	}

	public void setAttributeId(String attributeId)
	{
		this.attributeId = attributeId;
	}
}