package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the member_reserved_facility database table.
 * 
 */
@Entity
@Table(name="member_reserved_facility")
@NamedQuery(name="MemberReservedFacility.findAll", query="SELECT f FROM MemberReservedFacility f")
public class MemberReservedFacility implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="facility_timeslot_id", unique=true, nullable=false, length=20)
	private Long facilityTimeslotId;

	@Column(name="resv_id", unique=true, nullable=false, length=20)
	private Long resvId;
	

	public MemberReservedFacility() {
	}

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

}