package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the member_plan_facility_right database table.
 * 
 */
@Entity
@Table(name="member_plan_facility_right")
@NamedQuery(name="MemberPlanFacilityRight.findAll", query="SELECT m FROM MemberPlanFacilityRight m")
public class MemberPlanFacilityRight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false)
	private Long sysId;
	
	@Column(name="customer_id",nullable=false)
	private Long customerId;
	
	@Column(name="service_plan",nullable=false)
	private Long servicePlan;
	
	@Column(name="facility_type_code",length=10)
	private String facilityTypeCode;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Column(length=2)
	private String permission;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_date")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="expiry_date")
	private Date expiryDate;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long version;
	
	//bi-directional many-to-one association to Member
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="customer_id", nullable=false, insertable=false, updatable=false)
	@JsonIgnore
	private Member member;

	public MemberPlanFacilityRight() {
	}
	
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPermission() {
		return this.permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Long  getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getFacilityTypeCode() {
		return facilityTypeCode;
	}

	public void setFacilityTypeCode(String facilityTypeCode) {
		this.facilityTypeCode = facilityTypeCode;
	}

	public Long getServicePlan() {
		return servicePlan;
	}

	public void setServicePlan(Long servicePlan) {
		this.servicePlan = servicePlan;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Object version) {
		/*if(version instanceof BigInteger){
			this.version = ((BigInteger) version).longValue();
		}else if(version instanceof Integer){
			this.version = ((Integer) version).longValue();
		}else if(version instanceof String){
			this.version = Long.valueOf((String) version);
		}else{
			this.version = (Long) version;
		}*/
		this.version = (version!=null ? NumberUtils.toLong(version.toString()):null);
	}

}