package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;

@Entity
@Table(name = "facility_sub_type_pos")
public class FacilitySubTypePos  implements Serializable {

	private static final long serialVersionUID = 5463152874212170115L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subtype_pos_id" , unique=true, nullable=false, length=50)
	private Long subtypePosId;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "subtype_id")
	private FacilitySubType facilitySubType;
	
	@OneToOne
	@JoinColumn(name = "pos_item_no")
	private PosServiceItemPrice posItemPrice;
	
	@Column(name="rate_type", length=10)
	private String rateType;
	
	@Column(name="age_range_code", length=10)
	private String ageRangeCode;
	
	private String description;
	
	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="update_by")
	private String updateBy;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public Long getSubtypePosId()
	{
		return subtypePosId;
	}

	public void setSubtypePosId(Long subtypePosId)
	{
		this.subtypePosId = subtypePosId;
	}

	public String getRateType()
	{
		return rateType;
	}

	public void setRateType(String rateType)
	{
		this.rateType = rateType;
	}

	public String getAgeRangeCode()
	{
		return ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode)
	{
		this.ageRangeCode = ageRangeCode;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public FacilitySubType getFacilitySubType()
	{
		return facilitySubType;
	}

	public void setFacilitySubType(FacilitySubType facilitySubType)
	{
		this.facilitySubType = facilitySubType;
	}

	public PosServiceItemPrice getPosItemPrice()
	{
		return posItemPrice;
	}

	public void setPosItemPrice(PosServiceItemPrice posItemPrice)
	{
		this.posItemPrice = posItemPrice;
	}
}
