package com.sinodynamic.hkgta.entity.crm;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="dd_interface_file_log")
public class DdInterfaceFileLog  implements java.io.Serializable {


     private Long processId;
     private String filename;
     private String transactionType;
     private String status;
     private String bankMsg;
     private Date processTimestamp;
     private Date ackTimestamp;
     private String statusUpdateBy;
     private Date statusUpdateDate;
     private Set<DdiTransactionLog> ddiTransactionLogs = new HashSet<DdiTransactionLog>(0);
    public DdInterfaceFileLog() {
    }

    public DdInterfaceFileLog(String filename, String transactionType, String status, String bankMsg, Date processTimestamp, Date ackTimestamp, String statusUpdateBy, Date statusUpdateDate) {
       this.filename = filename;
       this.transactionType = transactionType;
       this.status = status;
       this.bankMsg = bankMsg;
       this.processTimestamp = processTimestamp;
       this.ackTimestamp = ackTimestamp;
       this.statusUpdateBy = statusUpdateBy;
       this.statusUpdateDate = statusUpdateDate;
    }
   
    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="process_id", unique=true, nullable=false)
    public Long getProcessId() {
        return this.processId;
    }
    
    public void setProcessId(Long processId) {
        this.processId = processId;
    }
    
    @Column(name="filename", length=200)
    public String getFilename() {
        return this.filename;
    }
    
    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    @Column(name="transaction_type", length=5)
    public String getTransactionType() {
        return this.transactionType;
    }
    
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    
    @Column(name="status", length=10)
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    @Column(name="bank_msg", length=300)
    public String getBankMsg() {
        return this.bankMsg;
    }
    
    public void setBankMsg(String bankMsg) {
        this.bankMsg = bankMsg;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="process_timestamp", length=19)
    public Date getProcessTimestamp() {
        return this.processTimestamp;
    }
    
    public void setProcessTimestamp(Date processTimestamp) {
        this.processTimestamp = processTimestamp;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="ack_timestamp", length=19)
    public Date getAckTimestamp() {
        return this.ackTimestamp;
    }
    
    public void setAckTimestamp(Date ackTimestamp) {
        this.ackTimestamp = ackTimestamp;
    }
    
    @Column(name="status_update_by", length=50)
    public String getStatusUpdateBy() {
        return this.statusUpdateBy;
    }
    
    public void setStatusUpdateBy(String statusUpdateBy) {
        this.statusUpdateBy = statusUpdateBy;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="status_update_date", length=19)
    public Date getStatusUpdateDate() {
        return this.statusUpdateDate;
    }
    
    public void setStatusUpdateDate(Date statusUpdateDate) {
        this.statusUpdateDate = statusUpdateDate;
    }

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="ddInterfaceFileLog")
    public Set<DdiTransactionLog> getDdiTransactionLogs() {
        return this.ddiTransactionLogs;
    }
    
    public void setDdiTransactionLogs(Set<DdiTransactionLog> ddiTransactionLogs) {
        this.ddiTransactionLogs = ddiTransactionLogs;
    }



}


