package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the staff_profile database table.
 * 
 */
@Entity
@Table(name="staff_profile")
@NamedQuery(name="StaffProfile.findAll", query="SELECT s FROM StaffProfile s")
public class StaffProfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")     
	@GenericGenerator(name = "paymentableGenerator", strategy = "assigned")   
	@Column(name="user_id", unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="contact_email", length=50)
	private String contactEmail;

	@Column(name="create_by", length=50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth", nullable=false)
	private Date dateOfBirth;

	@Column(length=1)
	private String gender;

	@Column(name="given_name", length=50)
	private String givenName;

	@Column(nullable=false, length=50)
	private String nationality;

	@Column(name="passport_no", nullable=false, length=40)
	private String passportNo;

	@Column(name="passport_type", nullable=false, length=20)
	private String passportType;

	@Column(name="phone_home", nullable=false, length=50)
	private String phoneHome;

	@Column(name="phone_mobile", nullable=false, length=50)
	private String phoneMobile;
	
	@Column(name="phone_office", nullable=false, length=50)
	private String phoneOffice;

	@Column(name="portrait_photo", length=300)
	private String portraitPhoto;

	@Column(name="postal_address1", nullable=false, length=100)
	private String postalAddress1;
	
	@Column(name="postal_address2", nullable=false, length=100)
	private String postalAddress2;

	@Column(name="postal_area", nullable=false, length=3)
	private String postalArea;

	@Column(name="postal_district", nullable=false, length=30)
	private String postalDistrict;

	@Column(length=50)
	private String surname;
	
	@Column(name="surname_nls", length=50)
	private String surnameNls;
	
	@Column(name="given_name_nls", length=50)
	private String givenNameNls;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	//bi-directional one-to-one association to StaffMaster
	@JsonIgnore
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id", nullable=false, insertable=false, updatable=false)
	private StaffMaster staffMaster;

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getPhoneOffice()
	{
		return phoneOffice;
	}

	public void setPhoneOffice(String phoneOffice)
	{
		this.phoneOffice = phoneOffice;
	}

	public StaffProfile() {
	}
	
	public StaffProfile(String userId, String givenName, String surname) {
		
		this.userId = userId;
		this.givenName = givenName;
		this.surname = surname;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGivenName() {
		return this.givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportType() {
		return this.passportType;
	}

	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}

	public String getPhoneHome() {
		return this.phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getPhoneMobile() {
		return this.phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getPortraitPhoto() {
		return this.portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public String getPostalArea() {
		return this.postalArea;
	}

	public String getPostalAddress1()
	{
		return postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1)
	{
		this.postalAddress1 = postalAddress1;
	}

	public String getPostalAddress2()
	{
		return postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2)
	{
		this.postalAddress2 = postalAddress2;
	}

	public void setPostalArea(String postalArea) {
		this.postalArea = postalArea;
	}

	public String getPostalDistrict() {
		return this.postalDistrict;
	}

	public void setPostalDistrict(String postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getSurnameNls() {
		return surnameNls;
	}

	public void setSurnameNls(String surnameNls) {
		this.surnameNls = surnameNls;
	}

	public String getGivenNameNls() {
		return givenNameNls;
	}

	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = givenNameNls;
	}

	public StaffMaster getStaffMaster() {
		return this.staffMaster;
	}

	public void setStaffMaster(StaffMaster staffMaster) {
		this.staffMaster = staffMaster;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StaffProfile other = (StaffProfile) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}