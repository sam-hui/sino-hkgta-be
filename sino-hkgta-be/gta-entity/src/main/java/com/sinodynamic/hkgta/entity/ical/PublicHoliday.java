package com.sinodynamic.hkgta.entity.ical;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="public_holiday")
@NamedQuery(name="PublicHoliday.findAll", query="SELECT p FROM PublicHoliday p")
public class PublicHoliday implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="holiday_id", unique=true, nullable=false)
	private Long holidayId;
	
	@Column(name="holiday_date")
	private Date holidayDate;
	
	@Column(name="description_en")
	private String descriptionEn;
	
	@Column(name="description_tc")
	private String descriptionTc;
	
	@Column(name="description_sc")
	private String descriptionSc;
	
	@Column(name="holiday_type")
	private String holidayType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date", nullable=false)
	private Date createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="update_by")
	private String updateBy;
	
	@Column(name="ver_no", nullable=false)
	private Long verNo;

	
	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	public Long getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(Object holidayId) {
		if (holidayId != null){
			this.holidayId = Long.valueOf(holidayId.toString());
		}	
	}

	public Date getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	public String getDescriptionEn() {
		return descriptionEn;
	}

	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}

	public String getDescriptionTc() {
		return descriptionTc;
	}

	public void setDescriptionTc(String descriptionTc) {
		this.descriptionTc = descriptionTc;
	}

	public String getDescriptionSc() {
		return descriptionSc;
	}

	public void setDescriptionSc(String descriptionSc) {
		this.descriptionSc = descriptionSc;
	}

	public String getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Object verNo) {
		if (verNo != null){
		   this.verNo = Long.valueOf(verNo.toString());
		}
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
