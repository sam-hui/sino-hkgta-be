package com.sinodynamic.hkgta.entity.pos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "restaurant_menu_item")
public class RestaurantMenuItem implements Serializable {

	private static final long serialVersionUID = -926657136971276607L;

	@Id
	@ManyToOne
    @JoinColumn(name = "item_no", referencedColumnName = "item_no")
	private RestaurantItemMaster restaurantItemMaster;
	
	@Id
	@ManyToOne
    @JoinColumns({
        @JoinColumn(name = "restaurant_id", referencedColumnName = "restaurant_id"),
        @JoinColumn(name = "cat_id", referencedColumnName = "cat_id")
    })
	private RestaurantMenuCat restaurantMenuCat;
	
	@Column(name = "status")
	private String status;

	@Column(name = "online_price")
	private BigDecimal onlinePrice;
	
	@Column(name = "image_filename")
	private String imageFileName;
	
	@Column(name = "display_order")
	private Integer displayOrder;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Version
	@Column(name = "ver_no")
	private Long verNo;
	
	public RestaurantMenuCat getRestaurantMenuCat() {
		return restaurantMenuCat;
	}

	public void setRestaurantMenuCat(RestaurantMenuCat restaurantMenuCat) {
		this.restaurantMenuCat = restaurantMenuCat;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getOnlinePrice() {
		return onlinePrice;
	}

	public void setOnlinePrice(BigDecimal onlinePrice) {
		this.onlinePrice = onlinePrice;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public RestaurantItemMaster getRestaurantItemMaster() {
		return restaurantItemMaster;
	}

	public void setRestaurantItemMaster(RestaurantItemMaster restaurantItemMaster) {
		this.restaurantItemMaster = restaurantItemMaster;
	}

}
