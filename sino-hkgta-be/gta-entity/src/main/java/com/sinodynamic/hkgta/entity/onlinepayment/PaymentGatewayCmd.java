package com.sinodynamic.hkgta.entity.onlinepayment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the payment_gateway_cmd database table.
 * 
 */
@Entity
@Table(name="payment_gateway_cmd")
@NamedQuery(name="PaymentGatewayCmd.findAll", query="SELECT p FROM PaymentGatewayCmd p")
public class PaymentGatewayCmd implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PaymentGatewayCmdPK id;

	@Column(name="command_type")
	private String commandType;

	private String description;

	@Column(name="return_to_page")
	private String returnToPage;

	@Column(name="url_cmd")
	private String urlCmd;

	//bi-directional many-to-one association to PaymentGateway
	@ManyToOne
	@JoinColumn(name="gateway_id", insertable=false, updatable=false)
	private PaymentGateway paymentGateway;

	public PaymentGatewayCmd() {
	}

	public PaymentGatewayCmdPK getId() {
		return this.id;
	}

	public void setId(PaymentGatewayCmdPK id) {
		this.id = id;
	}

	public String getCommandType() {
		return this.commandType;
	}

	public void setCommandType(String commandType) {
		this.commandType = commandType;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnToPage() {
		return this.returnToPage;
	}

	public void setReturnToPage(String returnToPage) {
		this.returnToPage = returnToPage;
	}

	public String getUrlCmd() {
		return this.urlCmd;
	}

	public void setUrlCmd(String urlCmd) {
		this.urlCmd = urlCmd;
	}

	public PaymentGateway getPaymentGateway() {
		return this.paymentGateway;
	}

	public void setPaymentGateway(PaymentGateway paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

}