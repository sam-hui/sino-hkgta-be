package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the member_limit_rule database table.
 * 
 */
@Entity
@Table(name="member_limit_rule")
@NamedQuery(name="MemberLimitRule.findAll", query="SELECT m FROM MemberLimitRule m")
public class MemberLimitRule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="limit_id", unique=true, nullable=false)
	private Long limitId;

	@Column(name="applied_scope", length=10)
	private String appliedScope;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Timestamp createDate;

	@Column(length=250)
	private String description;

	@Column(name="limit_type", nullable=false, length=10)
	private String limitType;

	@Column(name="limit_unit", length=10)
	private String limitUnit;

	@Column(name="num_value", precision=10, scale=2)
	private BigDecimal numValue;

	@Column(name="text_value", length=50)
	private String textValue;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="customer_id")
	private Long customerId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="effective_date")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="expiry_date")
	private Date expiryDate;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	public MemberLimitRule() {
	}

	public Long getLimitId() {
		return limitId;
	}

	public void setLimitId(Object limitId) {
		/*if(limitId instanceof Long){
			this.limitId = ((Long) limitId).longValue();
		}else if(limitId instanceof Integer){
			this.limitId = ((Integer) limitId).longValue();
		}else if(limitId instanceof String){
			this.limitId = Long.valueOf((String) limitId);
		}else{
			this.limitId = (Long) limitId;
		}*/
		this.limitId = (limitId!=null ? NumberUtils.toLong(limitId.toString()):null);
	}

	public String getAppliedScope() {
		return this.appliedScope;
	}

	public void setAppliedScope(String appliedScope) {
		this.appliedScope = appliedScope;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLimitType() {
		return this.limitType;
	}

	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}

	public String getLimitUnit() {
		return this.limitUnit;
	}

	public void setLimitUnit(String limitUnit) {
		this.limitUnit = limitUnit;
	}

	public BigDecimal getNumValue() {
		return this.numValue;
	}

	public void setNumValue(BigDecimal numValue) {
		this.numValue = numValue;
	}

	public String getTextValue() {
		return this.textValue;
	}

	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}