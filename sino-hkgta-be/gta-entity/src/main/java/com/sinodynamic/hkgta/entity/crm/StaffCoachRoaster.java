package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="staff_coach_roster")
public class StaffCoachRoaster implements Serializable, Comparable<StaffCoachRoaster>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id")
	private Long id;
	
	@Column(name="coach_user_id")
	private String coachId;
	
	@Column(name="rate_type")
	private String rateType;
	
	@Column(name="off_duty")
	private String offDuty;
	
	@Column(name="week_day")
	private String weekDay;
	
	@Column(name="on_date")
	private Date onDate;
	
	@Column(name="begin_time")
	private Long beginTime;
	
	@Column(name="end_time")
	private Long endTime;
	
	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="create_by")
	private String createBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCoachId() {
		return coachId;
	}

	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getOffDuty() {
		return offDuty;
	}

	public void setOffDuty(String offDuty) {
		this.offDuty = offDuty;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}

	public Date getOnDate() {
		return onDate;
	}

	public void setOnDate(Date onDate) {
		this.onDate = onDate;
	}

	public Long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Long beginTime) {
		this.beginTime = beginTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public int compareTo(StaffCoachRoaster o) {
		if(o == null)
			return 1;
		
		if(this.onDate!=null && o.onDate!=null){
			int compareDate = this.onDate.compareTo(o.onDate);
			if(compareDate == 0)
			{
				if(this.beginTime > o.beginTime)
					return 1;
				else if(this.beginTime==o.beginTime)
					return 0;
				else
					return -1;
			}
			else
				return compareDate;
			
		}else if (this.weekDay!=null && o.weekDay!=null){
			int compareWeekday = this.weekDay.compareTo(o.weekDay); 
			if(compareWeekday == 0)
			{
				if(this.beginTime > o.beginTime)
					return 1;
				else if(this.beginTime==o.beginTime)
					return 0;
				else
					return -1;
			}
			else
				return compareWeekday;
		}
		return 0;
		
		
		//throw new GTACommonException(GTAError.CoachMgrError.FAIL_LOAD_ROSTER_DATA);
	}
}
