package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the member_facility_attendance database table.
 * 
 */
@Embeddable
public class MemberFacilityAttendancePK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="facility_timeslot_id", unique=true, nullable=false, length=40)
	private Long facilityTimeslotId;

	@Column(name="customer_id", insertable=false, updatable=false, nullable=false, length=20)
	private Long customerId;

	public MemberFacilityAttendancePK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MemberFacilityAttendancePK)) {
			return false;
		}
		MemberFacilityAttendancePK castOther = (MemberFacilityAttendancePK)other;
		return 
			this.facilityTimeslotId==castOther.facilityTimeslotId
			&& this.customerId==castOther.customerId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (customerId ^ (customerId >>> 32));
		result = prime * result + (int) (facilityTimeslotId ^ (facilityTimeslotId >>> 32));
		return result;
	}


	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}
}