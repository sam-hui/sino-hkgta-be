package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;


/**
 * The persistent class for the customer_addition_info database table.
 * 
 */
@Entity
@Table(name="customer_addition_info")
@NamedQuery(name="CustomerAdditionInfo.findAll", query="SELECT c FROM CustomerAdditionInfo c")
public class CustomerAdditionInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CustomerAdditionInfoPK id;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Column(name="customer_input", length=300)
	private String customerInput;

	@Column(name="sys_id", nullable=false)
	private Long sysId;

	@Column(name="update_by", length=50)
	private String updateBy;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

//	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
//	@JoinColumn(name="customer_id", nullable=false, insertable=false, updatable=false)
//	@JsonIgnore
	@Transient
	private CustomerProfile customerProfile;

	@Transient
	private String captionId;
	
	public CustomerAdditionInfo() {
	}

	public CustomerAdditionInfoPK getId() {
		return this.id;
	}

	public void setId(CustomerAdditionInfoPK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCustomerInput() {
		return this.customerInput;
	}

	public void setCustomerInput(String customerInput) {
		this.customerInput = customerInput;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public CustomerProfile getCustomerProfile() {
		return this.customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	public String getCaptionId() {
		return captionId;
	}

	public void setCaptionId(String captionId) {
		this.captionId = captionId;
	}

}