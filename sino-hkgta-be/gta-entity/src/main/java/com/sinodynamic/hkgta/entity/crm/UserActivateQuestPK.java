package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the user_activate_quest database table.
 * 
 */
@Embeddable
public class UserActivateQuestPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="user_id", insertable=false, updatable=false, unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="question_no", unique=true, nullable=false)
	private Long questionNo;

	public UserActivateQuestPK() {
	}
	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getQuestionNo() {
		return this.questionNo;
	}
	public void setQuestionNo(Long questionNo) {
		this.questionNo = questionNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserActivateQuestPK)) {
			return false;
		}
		UserActivateQuestPK castOther = (UserActivateQuestPK)other;
		return 
			this.userId.equals(castOther.userId)
			&& (this.questionNo == castOther.questionNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.questionNo.intValue();
		
		return hash;
	}
}