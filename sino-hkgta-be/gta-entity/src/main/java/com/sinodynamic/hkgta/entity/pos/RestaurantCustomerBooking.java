package com.sinodynamic.hkgta.entity.pos;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.crm.CustomerProfile;

@Entity
@Table(name = "restaurant_customer_booking")
public class RestaurantCustomerBooking implements Serializable
{
	private static final long serialVersionUID = -4113511459979858738L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "resv_id", unique = true, nullable = false, length = 20)
	private Long resvId;
	
	@OneToOne
	@JoinColumn(name = "customer_id")
	private CustomerProfile customerProfile;
	
	@Column(name = "book_time")
	private Date bookTime;
	
	@Column(name = "party_size")
	private Long partySize;
	
	private String status;
	
	@Column(name = "book_via")
	private String bookVia;
	
	private String remark;
	
	@Column(name = "actual_arrival_time")
	private Date actualArrivalTime;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "sync_date")
	private Date syncDate;
	
	@Version
	@Column(name = "ver_no")
	private Long verNo;
	
	@ManyToOne
	@JoinColumn(name = "restaurant_id")
	private RestaurantMaster restaurantMaster;	

	public Long getResvId()
	{
		return resvId;
	}

	public void setResvId(Long resvId)
	{
		this.resvId = resvId;
	}

	public CustomerProfile getCustomerProfile()
	{
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile)
	{
		this.customerProfile = customerProfile;
	}

	public Date getBookTime()
	{
		return bookTime;
	}

	public void setBookTime(Date bookTime)
	{
		this.bookTime = bookTime;
	}

	public Long getPartySize()
	{
		return partySize;
	}

	public void setPartySize(Long partySize)
	{
		this.partySize = partySize;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getBookVia()
	{
		return bookVia;
	}

	public void setBookVia(String bookVia)
	{
		this.bookVia = bookVia;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getActualArrivalTime()
	{
		return actualArrivalTime;
	}

	public void setActualArrivalTime(Date actualArrivalTime)
	{
		this.actualArrivalTime = actualArrivalTime;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public Date getSyncDate()
	{
		return syncDate;
	}

	public void setSyncDate(Date syncDate)
	{
		this.syncDate = syncDate;
	}

	public Long getVerNo()
	{
		return verNo;
	}

	public void setVerNo(Long verNo)
	{
		this.verNo = verNo;
	}

	public RestaurantMaster getRestaurantMaster()
	{
		return restaurantMaster;
	}

	public void setRestaurantMaster(RestaurantMaster restaurantMaster)
	{
		this.restaurantMaster = restaurantMaster;
	}
	
	
}
