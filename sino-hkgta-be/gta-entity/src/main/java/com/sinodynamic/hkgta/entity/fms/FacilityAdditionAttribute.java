package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the facility_addition_attribute database table.
 * 
 */
@Entity
@Table(name="facility_addition_attribute")
@NamedQuery(name="FacilityAdditionAttribute.findAll", query="SELECT f FROM FacilityAdditionAttribute f")
public class FacilityAdditionAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FacilityAdditionAttributePK id;

	@Column(name="input_value", length=250)
	private String inputValue;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "attribute_id",insertable=false, updatable=false)
	private FacilityAttributeCaption attributeCaption;
	
	public FacilityAdditionAttribute() {
	}

	public FacilityAdditionAttributePK getId() {
		return this.id;
	}

	public void setId(FacilityAdditionAttributePK id) {
		this.id = id;
	}

	public String getInputValue()
	{
		return inputValue;
	}

	public void setInputValue(String inputValue)
	{
		this.inputValue = inputValue;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public FacilityAttributeCaption getAttributeCaption()
	{
		return attributeCaption;
	}

	public void setAttributeCaption(FacilityAttributeCaption attributeCaption)
	{
		this.attributeCaption = attributeCaption;
	}

}