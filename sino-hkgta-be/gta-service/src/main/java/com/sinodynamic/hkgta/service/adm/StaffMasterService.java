package com.sinodynamic.hkgta.service.adm;

import com.sinodynamic.hkgta.dto.staff.RequestStaffDto;
import com.sinodynamic.hkgta.dto.staff.StaffCoachInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffListDto;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.dto.staff.UserDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.StaffMasterOrderByCol;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * @author Junfeng_Yan
 * @since May 28 2015
 */
public interface StaffMasterService extends IServiceBase<StaffMasterInfoDto>{

	public ListPage<StaffMasterInfoDto> getStaffMasterList(ListPage<StaffMasterInfoDto> pListPage, StaffMasterOrderByCol orderCol,String sortType, StaffListDto dto) throws Exception;
	
	public ResponseResult getStaffMasterByUserId(String userId) throws Exception;
	
	public ResponseMsg saveStaffMaster(RequestStaffDto staffDto , String createBy) throws Exception;
	
	public ResponseMsg saveCoachDescription(StaffCoachInfoDto coachDto , String createBy) throws Exception;
	
	public ResponseMsg updateStaffMasterOld(RequestStaffDto staffDto , String updateBy) throws Exception;
	
	public ResponseMsg saveStaffMasterOld(UserDto staffDto , String createBy) throws Exception;
	
	public ResponseMsg saveStaffMaster(UserDto staffDto , String createBy) throws Exception;
	
	public ResponseMsg updateStaffMaster(UserDto staffDto , String updateBy) throws Exception;
	
	public ResponseMsg updateStaff(UserDto staffDto , String updateBy) throws Exception;
	
	public ResponseMsg updateProfilePhoto(StaffProfile p) throws Exception;
	
	public ResponseMsg changeStaffMasterStatus(RequestStaffDto staffDto , String updateBy) throws Exception;
	
	public ResponseResult staffNoIsUsed(String staffNo) throws Exception;
	
	public ResponseResult validatePassport(String loginId, String passportType, String passportNo) throws Exception;
	
	public ResponseResult emailIsUsed(String email) throws Exception;
	
	public ResponseResult changePsw(StaffPasswordDto staffPsw) throws Exception;
	
	public void changeExpiredStaffMasterStatus() throws Exception;
	
	public ResponseResult getStaffMasterOnlyByUserId(String userId);
	
	public StaffMaster getStaffMaster(String userId);

	public ResponseResult getCoachInfoByUserId(String userId);
	
	public String getStaffs();
	
	public String getCoachs();

	public void autoQuitExpireStaff();

	public void sendEmail2Staff(String userId, UserDto staffDto)  throws Exception;
}
