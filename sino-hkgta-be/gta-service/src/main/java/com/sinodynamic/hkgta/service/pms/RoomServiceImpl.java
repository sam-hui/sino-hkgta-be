package com.sinodynamic.hkgta.service.pms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.UserDeviceDao;
import com.sinodynamic.hkgta.dao.pms.RoomCrewDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dao.pms.RoomHousekeepTaskDao;
import com.sinodynamic.hkgta.dao.sys.UserRoleDao;
import com.sinodynamic.hkgta.dto.pms.HousekeepTaskFileDto;
import com.sinodynamic.hkgta.dto.pms.RoomCrewDto;
import com.sinodynamic.hkgta.dto.pms.RoomDetailDto;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.dto.pms.RoomTaskDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.dto.push.RegisterResultDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.integration.push.service.PushService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RoomCrewRoleType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RoomServiceImpl extends ServiceBase<Room> implements RoomService {
	@Autowired
	private RoomDao roomDao;
	
	@Autowired
	private RoomHousekeepTaskDao roomHousekeepTaskDao;

	@Autowired
	private RoomCrewDao roomCrewDao;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	
	@Autowired
	private PMSApiService pMSService;

	@Autowired
	private UserRoleDao userRoleDao;
	
	@Autowired
	private PushService pushService;
	
	
	@Autowired
	private UserDeviceDao userDeviceDao;
	
	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;
	
	
	@Deprecated
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomDto> getRoomList(String updateBy) throws Exception {
		List<RoomDto> rooms = new ArrayList<RoomDto>();
		List<Room> roomList = roomDao.getRoomList();
		if(null != roomList && roomList.size() > 0){
			RoomDto roomDto = null;
			for(Room room : roomList){
				roomDto = new RoomDto();
				roomDto.setRoomId(room.getRoomId());
				roomDto.setRoomNo(room.getRoomNo());
				roomDto.setStatus(room.getStatus());
				roomDto.setFrontdeskStatus(room.getFrontdeskStatus());
				List<RoomHousekeepTask> roomHousekeepTasks = roomHousekeepTaskDao.getRoomHousekeepTaskList(room.getRoomId());
				setRoomHousekeepTaskDto(roomDto, roomHousekeepTasks);
				if(roomDto.getRoomId()>0)rooms.add(roomDto);
				if(roomDto.getNoResponse()==null)
					roomDto.setNoResponse(false);
			}
		}
		return rooms;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomDto> getRoomListWithTaskCounts(String updateBy) throws Exception {

		List<RoomDto> dtos = roomDao.getRoomListWithTaskCounts();

		GlobalParameter assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
		int raRespTime = assistant != null ? Integer.parseInt(assistant.getParamValue()) : 0;

		List<Long> noRespRoomIds = roomDao.getNoResponseRoomIdx(raRespTime, new Date());

		for (RoomDto dto : dtos) {
			if (noRespRoomIds.indexOf(dto.getRoomId()) > -1) {
				dto.setNoResponse(Boolean.TRUE);
			} else {
				dto.setNoResponse(Boolean.FALSE);
			}
		}
		return dtos;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public RoomDetailDto getRoomDetail(Long roomId){
		RoomDetailDto roomDetail = new RoomDetailDto();
		Room room = roomDao.get(Room.class, roomId);
		if(null != room){
			roomDetail.setRoomId(room.getRoomId());
			roomDetail.setRoomNo(room.getRoomNo());
			roomDetail.setStatus(room.getStatus());
			roomDetail.setFrontdeskStatus(room.getFrontdeskStatus());
			List<RoomCrew> roomCrewList = roomCrewDao.getRoomCrewList(roomId);
			List<RoomCrewDto> roomCrewDtoList = null; 
			List<RoomHousekeepTaskDto> roomHousekeepTaskDtoList = null;
			if(null != roomCrewList && roomCrewList.size() >0){
				roomCrewDtoList = new ArrayList<RoomCrewDto>();
				for(RoomCrew roomCrew : roomCrewList){
					RoomCrewDto roomCrewDto = new RoomCrewDto();
					roomCrewDto.setUserId(roomCrew.getStaffProfile().getUserId());
					SysCode sysCode = sysCodeDao.getByCategoryAndCodeValue("HK-CREW-ROLE", roomCrew.getCrewRoleId());
					roomCrewDto.setTitle(null == sysCode?"":sysCode.getCodeDisplay());
					roomCrewDto.setUserName(roomCrew.getStaffProfile().getGivenName() + " " + roomCrew.getStaffProfile().getSurname());
					roomCrewDtoList.add(roomCrewDto);
				}
			}
			List<RoomHousekeepTask> roomHousekeepTasks = roomHousekeepTaskDao.getRoomHousekeepTaskList(roomId);
			if(null != roomHousekeepTasks && roomHousekeepTasks.size() >0){
				roomHousekeepTaskDtoList = new ArrayList<RoomHousekeepTaskDto>();
				GlobalParameter  assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
				Integer assistantResTime = assistant!=null?Integer.parseInt(assistant.getParamValue()):0;
				for(RoomHousekeepTask task : roomHousekeepTasks){
					RoomHousekeepTaskDto taskDto = setRoomHousekeepTaskDetailDto(assistantResTime, task);
					roomHousekeepTaskDtoList.add(taskDto);
				}
			}
			roomDetail.setRoomCrewList(roomCrewDtoList);
			roomDetail.setTaskList(roomHousekeepTaskDtoList);
		}
		return roomDetail;
	}

	private RoomHousekeepTaskDto setRoomHousekeepTaskDetailDto(Integer assistantResTime, RoomHousekeepTask task)
	{
		RoomHousekeepTaskDto taskDto = new RoomHousekeepTaskDto();
		taskDto.setTaskId(task.getTaskId());
		taskDto.setJobType(task.getJobType());
		taskDto.setStatus(task.getStatus());
		taskDto.setBeginDate(task.getScheduleDatetime());
		taskDto.setEndDate(task.getTargetCompleteDatetime());
		taskDto.setStartTimestamp(task.getStartTimestamp());
		taskDto.setFinishTimestamp(task.getFinishTimestamp());
		taskDto.setTaskDescription(task.getTaskDescription());
		taskDto.setRemark(task.getRemark());
		if (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString()) && task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp())
		{
			Calendar cal = Calendar.getInstance();
			cal.setTime(task.getScheduleDatetime());
			cal.add(Calendar.MINUTE, assistantResTime);
			taskDto.setExpireTime(cal.getTime());
			if (taskDto.getExpireTime().before(new Date()))
				taskDto.setNoResponse(true);
		}
		else if (task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString()) && task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp() && null != task.getTargetCompleteDatetime())
		{
			if (task.getTargetCompleteDatetime().before(new Date()))
				taskDto.setNoResponse(true);
		}
		else if (task.getJobType().equalsIgnoreCase(RoomHousekeepTaskJobType.RUT.toString()) && taskDto.getStatus().equalsIgnoreCase(RoomHousekeepTaskStatus.DND.toString()))
		{
			taskDto.setDndTimestamp(task.getStatusDate());
		}
		else
			taskDto.setNoResponse(false);
		setHousekeepTaskFileList(task, taskDto);
		return taskDto;
	}

	private void setHousekeepTaskFileList(RoomHousekeepTask task, RoomHousekeepTaskDto taskDto)
	{
		List<HousekeepTaskFile> taskFiles = task.getHousekeepTaskFiles();
		List<HousekeepTaskFileDto> taskFileList = null;
		if (null != taskFiles && taskFiles.size() > 0)
		{
			taskFileList = new ArrayList<HousekeepTaskFileDto>();
			for (HousekeepTaskFile taskFile : taskFiles)
			{
				if (!StringUtils.isEmpty(taskFile.getServerFile()))
				{
					HousekeepTaskFileDto taskFileDto = new HousekeepTaskFileDto();
					taskFileDto.setFileId(taskFile.getFileId());
					if ("AAC".equalsIgnoreCase(getFileType(taskFile.getServerFile())))
					{
						taskFileDto.setServerFile("/room/task/get_aacfile?fileId=" + taskFile.getFileId());
					}
					else if ("AMR".equalsIgnoreCase(getFileType(taskFile.getServerFile())))
					{
						taskFileDto.setServerFile("/room/task/get_amrfile?fileId=" + taskFile.getFileId());
					}
					else
					{
						taskFileDto.setServerFile("/room/task/get_thumbnail?fileId=" + taskFile.getFileId());
					}
					taskFileDto.setFileType(getFileType(taskFile.getServerFile()));
					taskFileList.add(taskFileDto);
				}
			}
			taskDto.setTaskFileList(taskFileList);
		}
	}
	
	public String getFileType(String fileUri){
		String fileType = "";
		if(fileUri.lastIndexOf(".") > 0){
			fileType = fileUri.substring(fileUri.lastIndexOf(".")+1,fileUri.length()).toUpperCase();
		}
		 return fileType;
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult changeRoomStatus(Long roomId,String status, String userId)
			throws Exception {
		Room room = roomDao.get(Room.class, roomId);
		RoomDto roomDto = null;
		if(null != room){
			//update oasis room status
			UpdateRoomStatusDto statusDto = new UpdateRoomStatusDto();
			statusDto.setRoomNo(room.getRoomNo()); // TODO
			statusDto.setUserId(userId);
			for( RoomStatus roomStatus : RoomStatus.values()){
				if(roomStatus.name().equals(status)){
					statusDto.setStatus(roomStatus.getDesc());
					break;
				}
			}
			List<UserRoleDto> userRoles = userRoleDao.getUserRoleListByUserId(userId);
			if(null !=userRoles && userRoles.size() >0)statusDto.setRoleName(userRoles.get(0).getRoleName());
			MessageResult  messageResult  = null;
			try {
				messageResult = pMSService.updateRoomStatus(statusDto);
			} catch (Exception e) {
				logger.error(GTAError.HouseKeepError.FAIL_GET_ROOM_LIST, e);
			}
			if(null != messageResult && messageResult.isSuccess()){
				room.setStatus(status);
				room.setUpdateBy(userId);
				room.setUpdateDate(new Date());
				roomDao.update(room);
				roomDto = new RoomDto();
				roomDto.setRoomId(roomId);
				roomDto.setRoomNo(room.getRoomNo());
				roomDto.setStatus(room.getStatus());
				
				roomHousekeepTaskService.autoRemindRoomAssistant(Constant.HOUSEKEEP_PUSHMESSAGE_CHANGESTATUS,RoomCrewRoleType.RA.name(),room,null);
				responseResult.initResult(GTAError.Success.SUCCESS,roomDto);
				
			}else{
				responseResult.initResult(GTAError.HouseKeepError.CHANGE_STATUS_FAIL);
			}
				
		}
		return responseResult;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Room> getRoomListByStatus(String[] status) throws Exception {
		return roomDao.getRoomListByStatus(status);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateRoom(Room room) {
		roomDao.update(room);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomDto> getRoomCrewList(String crewRoleId)
	{
		return roomDao.getRoomCrewList(crewRoleId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult getRoomTaskList(String userId,Boolean isMyRoom,String roomNo)
	{
		try {
			StaffMaster staffMaster = staffMasterDao.getByUserId(userId);
			if(staffMaster==null){
				responseResult.initResult(GTAError.HouseKeepError.NOT_FOUND_STAFF_TYPE);
				return responseResult;
			}
			List<RoomMemberDto> roomMemberDtos =isMyRoom ? roomHousekeepTaskDao.getMyRoomListForApp(staffMaster.getUserId(),staffMaster.getStaffType(),roomNo):
				roomHousekeepTaskDao.getRoomListForApp(staffMaster.getUserId(),staffMaster.getStaffType(),roomNo);
			if(null != roomMemberDtos && roomMemberDtos.size() >0){
				List<RoomHousekeepTaskQueryDto>  queryTaskDtos = isMyRoom ? roomHousekeepTaskDao.getMyRoomTaskListForApp(staffMaster.getUserId(),staffMaster.getStaffType(),roomNo) 
						: roomHousekeepTaskDao.getRoomTaskListForApp(staffMaster.getUserId(),staffMaster.getStaffType(),roomNo);
				setRoomMemberDtoList(staffMaster, roomMemberDtos,  queryTaskDtos);
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS,roomMemberDtos);
			
		} catch (Exception e) {
			logger.error(GTAError.HouseKeepError.FAIL_GET_ROOM_LIST, e);
			System.out.println(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.FAIL_GET_ROOM_LIST);
		}
		return responseResult;
	}
	
	private void setRoomMemberDtoList(StaffMaster staffMaster, List<RoomMemberDto> roomMemberDtos, List<RoomHousekeepTaskQueryDto> queryTaskDtos)
	{
		Map<String,List<RoomTaskDto>> roomNoMap = new HashMap<String,List<RoomTaskDto>>(); 
		GlobalParameter  assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
		GlobalParameter  inspector = globalParameterService.getGlobalParameter("HK-MAXRESP-INSPECTOR");
		GlobalParameter  supervisor = globalParameterService.getGlobalParameter("HK-MAXRESP-SUPERVISOR");
		Integer assistantResTime = assistant!=null?Integer.parseInt(assistant.getParamValue()):0;
		Integer inspectorResTime = inspector!=null?Integer.parseInt(inspector.getParamValue()):0;
		Integer supervisorResTime = supervisor!=null?Integer.parseInt(supervisor.getParamValue()):0;
		
		if(null != queryTaskDtos && queryTaskDtos.size()>0){
			for(RoomHousekeepTaskQueryDto taskDto : queryTaskDtos){
				List<RoomTaskDto> roomTasks = roomNoMap.get(taskDto.getRoomNo());
				if(null == roomTasks){
					roomTasks = new ArrayList<RoomTaskDto>();
					setRoomTaskDtoData(staffMaster, assistantResTime, inspectorResTime, supervisorResTime, taskDto, roomTasks);
					if(null != taskDto.getTaskId() && taskDto.getTaskId().longValue() > 0){
						roomNoMap.put(taskDto.getRoomNo(), roomTasks);
					}
				}else {
					setRoomTaskDtoData(staffMaster, assistantResTime, inspectorResTime, supervisorResTime, taskDto, roomTasks);
				}
			}
			for(RoomMemberDto memberDto : roomMemberDtos){
				memberDto.setTasks(roomNoMap.get(memberDto.getRoomNo()));
			}
		}
		
		
	}
	
	private void setRoomTaskDtoData(StaffMaster staffMaster, Integer assistantResTime, Integer inspectorResTime, Integer supervisorResTime, RoomHousekeepTaskQueryDto taskDto, List<RoomTaskDto> roomTasks)
	{
		if(null != taskDto.getTaskId() && taskDto.getTaskId().longValue() > 0){
			RoomTaskDto toomTaskDto = buildRoomTaskDto(staffMaster, assistantResTime, inspectorResTime, supervisorResTime, taskDto);
			roomTasks.add(toomTaskDto);
		}
	}

	private RoomTaskDto buildRoomTaskDto(StaffMaster staffMaster, Integer assistantResTime, Integer inspectorResTime, Integer supervisorResTime, RoomHousekeepTaskQueryDto taskDto)
	{
		RoomTaskDto toomTaskDto = new RoomTaskDto();
		toomTaskDto.setJobType(taskDto.getJobType());
		toomTaskDto.setStatus(taskDto.getTaskStatus());
		toomTaskDto.setTaskId(taskDto.getTaskId());
		toomTaskDto.setStartTimestamp(taskDto.getStartTimestamp());
		toomTaskDto.setFinishTimestamp(taskDto.getFinishTimestamp());
		toomTaskDto.setNoResponse(getTaskNoResponse(taskDto,assistantResTime,inspectorResTime,supervisorResTime,staffMaster.getStaffType()));
		return toomTaskDto;
	}

	private void setRoomHousekeepTaskDto(RoomDto roomDto,List<RoomHousekeepTask> roomHousekeepTasks)
	{
		if(null != roomHousekeepTasks && roomHousekeepTasks.size() > 0){
			GlobalParameter  assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
			Integer assistantResTime = null != assistant?Integer.parseInt(assistant.getParamValue()):0;
			for(RoomHousekeepTask task  : roomHousekeepTasks){
				if(task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString()))
					roomDto.setAdHocCount(null == roomDto.getAdHocCount()? 0l : roomDto.getAdHocCount());
				if(task.getJobType().equals(RoomHousekeepTaskJobType.RUT.toString()))
					roomDto.setRoutineCount(null == roomDto.getRoutineCount()? 0l : roomDto.getRoutineCount());
				if(task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString()))
					roomDto.setScheduleCount(null == roomDto.getScheduleCount()? 0l : roomDto.getScheduleCount());
				if(task.getJobType().equals(RoomHousekeepTaskJobType.MTN.toString()))
					roomDto.setMaintenanceCount(null == roomDto.getMaintenanceCount()? 0l : roomDto.getMaintenanceCount());
				
				if(task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString()) && task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp()){
					Calendar cal = Calendar.getInstance();
					cal.setTime(task.getScheduleDatetime());
					cal.add(Calendar.MINUTE, assistantResTime);
					if(cal.getTime().before(new Date()))
						roomDto.setNoResponse(true);
				}else if(task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString()) && task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp() && null != task.getTargetCompleteDatetime()){
					if(task.getTargetCompleteDatetime().before(new Date()))
						roomDto.setNoResponse(true);
				}
			}
		}
	}

	public boolean getTaskNoResponse(RoomHousekeepTaskQueryDto task,Integer assistantResTime,Integer inspectorResTime,Integer supervisorResTime,String roleType){
		if(task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString())){
			if(task.getTaskStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null && null != task.getEndDate() && task.getEndDate().before(new Date()) )
					return true;
		}else if(task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString())){
			if(task.getTaskStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null){
				if(roleType.equals(RoomCrewRoleType.SPV.name())){
					Calendar calInspector = Calendar.getInstance();
					calInspector.setTime(task.getBeginDate());
					calInspector.add(Calendar.MINUTE, (assistantResTime+inspectorResTime));
					if(calInspector.getTime().before(new Date()))
						return true;
				}
				else if(roleType.equals(RoomCrewRoleType.MGR.name())){
					Calendar calSpv = Calendar.getInstance();
					calSpv.setTime(task.getBeginDate());
					calSpv.add(Calendar.MINUTE, (assistantResTime+inspectorResTime+supervisorResTime));
					if(calSpv.getTime().before(new Date()))
						return true;
				}else{
					Calendar calAssistant = Calendar.getInstance();
					calAssistant.setTime(task.getBeginDate());
					calAssistant.add(Calendar.MINUTE, assistantResTime);
					if(calAssistant.getTime().before(new Date()))
						return true;
				
				}
			}
		}
		return false;
	}
	
	public boolean getTaskNoResponse(RoomHousekeepTask task,Integer assistantResTime,Integer inspectorResTime,Integer supervisorResTime,String roleType){
		if(task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString())){
			if(task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null && null != task.getTargetCompleteDatetime() && task.getTargetCompleteDatetime().before(new Date()) )
					return true;
		}else if(task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString())){
			if(task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null){
				if(roleType.equals(RoomCrewRoleType.ISP.name())){
					Calendar calAssistant = Calendar.getInstance();
					calAssistant.setTime(task.getScheduleDatetime());
					calAssistant.add(Calendar.MINUTE, assistantResTime);
					if(calAssistant.getTime().before(new Date()))
						return true;
				}
				else if(roleType.equals(RoomCrewRoleType.SPV.name())){
					Calendar calInspector = Calendar.getInstance();
					calInspector.setTime(task.getScheduleDatetime());
					calInspector.add(Calendar.MINUTE, (assistantResTime+inspectorResTime));
					if(calInspector.getTime().before(new Date()))
						return true;
				}
				else if(roleType.equals(RoomCrewRoleType.MGR.name())){
					Calendar calSpv = Calendar.getInstance();
					calSpv.setTime(task.getScheduleDatetime());
					calSpv.add(Calendar.MINUTE, (assistantResTime+inspectorResTime+supervisorResTime));
					if(calSpv.getTime().before(new Date()))
						return true;
				}
			}
		}
		return false;
	}

	public static long getDiffMinutes(Date startDate,Date endDate) {  
        long time1 = startDate.getTime();  
        long time2 = endDate.getTime();  
        long diff;  
        if(time1<time2)
            diff = time2 - time1;  
         else 
            diff = time1 - time2;  
        long day = diff / (24 * 60 * 60 * 1000);  
        long hour = (diff / (60 * 60 * 1000) - day * 24);  
        long min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);  
        long sec = (diff/1000-day*24*60*60-hour*60*60-min*60);  
        return day*24*60+hour*60+min+sec/60;  
	 }

	@Override
	public MessageResult updateRoomStatus(UpdateRoomStatusDto dto) throws Exception
	{
		if(null != dto && !StringUtils.isEmpty(dto.getRoomNo()) && !StringUtils.isEmpty(dto.getStatus())){
			return pMSService.updateRoomStatus(dto);
		}else 
			throw new GTACommonException(GTAError.HouseKeepError.REQUESTBODY_ISNULL );
	}

	@Override
	@Transactional
	public ResponseResult pushRegister(RegisterDto dto,String userId) {
		try {
			RegisterResultDto resultDto = pushService.register(dto);
			if(resultDto!=null){
				if("success".equals(resultDto.getStatus().toLowerCase())){
					String endpointArn = resultDto.getEndpointArn();
					UserDevice ud =  new UserDevice();
					ud.setArnApplication("housekeeper");
					ud.setArnEndpoint(endpointArn);
					ud.setCreateBy(userId);
					Date date = new Date();
					ud.setCreateDate(date);
					ud.setDeviceLanguage(dto.getLang());
					ud.setDeviceToken(dto.getToken());
					ud.setPlatform(dto.getPlatform());
					ud.setStatus("ACT");
					ud.setSystemVersion(dto.getSystemVersion());
					ud.setUpdateBy(userId);
					ud.setUserId(userId);
					ud.setUpdateDate(date);
					userDeviceDao.saveOrUpdate(ud);
					responseResult.initResult(GTAError.Success.SUCCESS);
				}else{
					responseResult.initResult(GTAError.HouseKeepError.FAIL_PUSH_REG,resultDto.getErrorMessageEn());
				}
			}else{
				responseResult.initResult(GTAError.HouseKeepError.FAIL_PUSH_REG);
			}
		} catch (Exception e) {
			logger.error(GTAError.HouseKeepError.FAIL_PUSH_REG, e);
			responseResult.initResult(GTAError.HouseKeepError.FAIL_PUSH_REG);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public List<Room> getRoomList()
	{
		return roomDao.getRoomList();
	}
	
	@Transactional
	public List<Room> getLimitedInfoRoomList(){
		return roomDao.getLimitedInfoRoomList();
	}

	@Override
	@Transactional
	public void staffPushRegister(String staffId, String deviceArn, String token, String platform, String version, String updatedBy)
	{
		List<UserDevice> userDeviceList = userDeviceDao.getByCol(UserDevice.class, "userId", staffId, null);
		UserDevice ud;
		if (userDeviceList!=null && userDeviceList.size() > 0) {
			ud = userDeviceList.get(0);
			setUserDevice(ud, staffId, deviceArn, token, platform, version, updatedBy, userDeviceList);
			userDeviceDao.update(ud);
		}
		else
		{
			ud =  new UserDevice();
			setUserDevice(ud, staffId, deviceArn, token, platform, version, updatedBy, userDeviceList);
			userDeviceDao.saveOrUpdate(ud);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
	}

	private void setUserDevice(UserDevice ud, String staffId, String deviceArn, String token, String platform, String version, String updatedBy, List<UserDevice> userDeviceList)
	{
		ud.setArnApplication("housekeeper");
		ud.setArnEndpoint(deviceArn);
		ud.setCreateBy(updatedBy);
		Date date = new Date();
		ud.setCreateDate(date);
		ud.setDeviceToken(token);
		ud.setPlatform(platform);
		ud.setStatus("ACT");
		ud.setSystemVersion(version);
		ud.setUpdateBy(updatedBy);
		ud.setUserId(staffId);
		ud.setUpdateDate(date);
	} 
}
