package com.sinodynamic.hkgta.service.fms;

public interface FacilityEmailService {

	public void sendReceiptConfirmationEmail(long resvId,String userId);
	
	public void sendRefundEmail(long resvId);
}
