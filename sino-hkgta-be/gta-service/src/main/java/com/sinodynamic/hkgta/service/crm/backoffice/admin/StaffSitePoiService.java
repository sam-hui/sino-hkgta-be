package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

public interface StaffSitePoiService extends IServiceBase<StaffSitePoiMonitor>{
	
     public void saveStaffSitePoiMonitor(StaffSitePoiMonitor staffSitePoiMonitor) throws GTACommonException;

     public List<StaffSitePoiMonitor> listBySiteId(Long sitePoiId) throws GTACommonException;
     
     public List<StaffSitePoiMonitor> listByUserId(String sitePoiId) throws GTACommonException;
     
     public void deleteById(Long sysId) throws GTACommonException;
     
     public List<SitePoi> listLocations() throws GTACommonException;
}
