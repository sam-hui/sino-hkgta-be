package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dto.fms.AvailableTimelotDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityTimeslotService extends IServiceBase<FacilityTimeslot> {

	public List<FacilityTimeslot> getFacilityTimeslotList(String typeCode, Date beginDatetime, Integer venueFloor);
	
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String facilityType,Date date, String bayType);
	
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String coachId, String facilityType,Date date, String bayType);

	public List<FacilityTimeslot> getFacilityTimeslotListByRange(String typeCode, Date beginDatetime, Date endDatetime, Integer venueFloor);

	public boolean updateFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException;

	public Serializable addFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException;

	public FacilityTimeslot getFacilityTimeslot(Long facility_no, String typeCode, int venueFloor, Date beginDatetime, Date endDatetime);
}
