package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.InternalPermitCardDao;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;

@Service
public class InternalPermitCardServiceImpl extends ServiceBase<InternalPermitCard> implements InternalPermitCardService {
    
    @Autowired
    private InternalPermitCardDao internalPermitCardDao;

    @Override
    @Transactional
    public InternalPermitCard getCardById(String cardNO) throws Exception {
	
	if (StringUtils.isEmpty(cardNO)) return null;
	
	String cardId = CommUtil.cardNoTransfer(cardNO);
	return internalPermitCardDao.getCardById(Long.parseLong(cardId));
    }

    @Override
    @Transactional
    public void saveCard(InternalPermitCard card) throws Exception {
	
	if (card == null) return;
	internalPermitCardDao.save(card);
    }

    @Override
    @Transactional
    public void updateCard(InternalPermitCard card) throws Exception {
	
	if (card == null) return;
	internalPermitCardDao.updateCardInfo(card);
    }

    
}
