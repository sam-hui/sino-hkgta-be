package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface SysCodeService extends IServiceBase<SysCode> {
	
	public List<SysCode> getSysCodeByCategory(String category) throws Exception;
	
	public List<SysCode> getSysCodeByMutipleCategory(String category) throws Exception;
	
	public List<SysCode> getAllSysCodeCategory() throws Exception;
}
