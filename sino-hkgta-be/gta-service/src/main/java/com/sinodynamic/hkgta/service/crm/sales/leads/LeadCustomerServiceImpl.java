package com.sinodynamic.hkgta.service.crm.sales.leads;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jaxen.function.ConcatFunction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerPreEnrollStageDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.LeadCustomerDao;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class LeadCustomerServiceImpl extends ServiceBase<CustomerProfile> implements LeadCustomerService {

	@Autowired
	private LeadCustomerDao leadCustomerDao;
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerPreEnrollStageDao customerPreEnrollStageDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	/**
	 * Method to add the potential customer and create related table, e.g.
	 * customer enrollment
	 * 
	 * @return ResponseMessage
	 * @throws Exception 
	 */
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult saveOrUpdatePotentialCustomer(
			CustomerProfileDto dto) throws Exception {
		
		String salutation  = dto.getSalutation();
		String surname = dto.getSurname();
		String givenName = dto.getGivenName();
		String phoneMobile = dto.getPhoneMobile();
		String note =dto.getInternalRemark();
		
		if(StringUtils.isEmpty(phoneMobile)) {
			responseResult.initResult(GTAError.LeadError.MOBILE_REQUIRED);
			return responseResult;
		}
		
		if(StringUtils.isEmpty(salutation)) {
			responseResult.initResult(GTAError.LeadError.SALUTATION_REQUIRED);
			return responseResult;
		}
		if(StringUtils.isEmpty(surname)) {
			responseResult.initResult(GTAError.LeadError.SURNAME_REQUIRED);
			return responseResult;
		}
		if(StringUtils.isEmpty(givenName)) {
			responseResult.initResult(GTAError.LeadError.GIVENNAME_REQUIRED);
			return responseResult;
		}
		if(StringUtils.isEmpty(phoneMobile)) {
			responseResult.initResult(GTAError.LeadError.MOBILE_REQUIRED);
			return responseResult;
		}
		
//		if(!StringUtils.isEmpty(note)){
//			int length = note.length();
//			if(length>300){
//				responseResult.initResult(GTAError.LeadError.NOTE_EXCEED_LIMIT);
//				return responseResult;
//			}
//		}
		
		String passportType = dto.getPassportType();
		if(StringUtils.isEmpty(passportType)) {
			dto.setPassportType(null);
			passportType = dto.getPassportType();
		}
		String passportNo = dto.getPassportNo();
		if(StringUtils.isEmpty(passportNo)) {
			dto.setPassportNo(null);
			passportNo = dto.getPassportNo();
		}
		String contactEmail = dto.getContactEmail();
		if(StringUtils.isEmpty(contactEmail)) {
			dto.setContactEmail(null);
			contactEmail = dto.getContactEmail();
		}
		Long customerId = dto.getCustomerId();
		String loginUserId = dto.getLoginUserId();
		Date currentDate = new Date();
			
		if (passportType != null && !"HKID".equals(passportType)
				&& !"VISA".equals(passportType)) {
			responseResult.initResult(GTAError.LeadError.PASSPORT_TYPE_INVALID);
			return responseResult;
		}
		
		if ("HKID".equals(passportType)&&passportNo!=null&&!CommUtil.validateHKID(passportNo)){
				responseResult.initResult(GTAError.LeadError.HKID_NO_INVALID);
				return responseResult;
			}
		else if ("VISA".equals(passportType)&&passportNo!=null&&!CommUtil.validateVISA(passportNo)){
				responseResult.initResult(GTAError.LeadError.VISA_NO_INVALID);
				return responseResult;
		}
		
		if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
			responseResult.initResult(GTAError.LeadError.MOBILE_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
			responseResult.initResult(GTAError.LeadError.BUSINESS_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
			responseResult.initResult(GTAError.LeadError.HOME_PHONE_INVALID);
			return responseResult;
		}
		
		if (contactEmail!=null&&!CommUtil.validateEmail(contactEmail)) {
			responseResult.initResult(GTAError.LeadError.EMAIL_ADDRESS_INVALID);
			return responseResult;
		}
	
		if (dto.getCustomerId()!=null) {
			
			CustomerProfile cp = leadCustomerDao
					.getByCustomerId(dto.getCustomerId());
			CustomerEnrollment ce =  customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
			if (cp == null||ce==null) {
				responseResult.initResult(GTAError.LeadError.UPDATED_FAILED);
				return responseResult;
			} else {
				if(!dto.getLoginUserId().equals(ce.getSalesFollowBy())) {
					responseResult.initResult(GTAError.LeadError.EDIT_PERMISSION_REJ);
					return responseResult;
				}
				if(passportNo!=null&&passportType!=null&&!customerProfileDao.checkAvailablePassportNo(customerId,passportType, passportNo)){
					responseResult.initResult(GTAError.LeadError.PASSPORT_IS_EXIST);
					return responseResult;
				}
				String[] ignoreFileds = new String[]{"isDeleted","createDate","createBy","version"};
				if (StringUtils.isEmpty(dto.getSignature())) {
					dto.setSignature(cp.getSignature());
				}
				if(StringUtils.isEmpty(dto.getPortraitPhoto())){
					dto.setPortraitPhoto(cp.getPortraitPhoto());
				}
				saveOrUpdateOptionalOrFavorForPositionTitleAndCompanyNameOnly(dto.getPositionTitle(),Long.valueOf(1), dto.getCompanyName(), Long.valueOf(14), dto.getCustomerId(),loginUserId);
				BeanUtils.copyProperties(dto, cp,ignoreFileds);
				leadCustomerDao.getCurrentSession().evict(cp);
				cp.setUpdateDate(currentDate);
				cp.setUpdateBy(loginUserId);
				cp.setPositionTitle(null);
				leadCustomerDao.update(cp);
				
				
			}

		} else {
			
			if(passportNo!=null&&passportType!=null&&!customerProfileDao.checkAvailablePassportNo(null,passportType, passportNo)){
				responseResult.initResult(GTAError.LeadError.PASSPORT_IS_EXIST);
				return responseResult;
			}
			
			CustomerProfile newCustomerProfile = new CustomerProfile();
			
			BeanUtils.copyProperties(dto, newCustomerProfile);
			newCustomerProfile.setIsDeleted("N");
			newCustomerProfile.setCreateDate(currentDate);
			newCustomerProfile.setCreateBy(loginUserId);
			newCustomerProfile.setUpdateDate(currentDate);
			newCustomerProfile.setUpdateBy(loginUserId);
			Long idSerial = (Long) leadCustomerDao.savePotentialCustomerProfile(newCustomerProfile);
			saveOrUpdateOptionalOrFavorForPositionTitleAndCompanyNameOnly(dto.getPositionTitle(),Long.valueOf(1),dto.getCompanyName(),Long.valueOf(14),idSerial,loginUserId);
			CustomerEnrollment customerEnrollment = new CustomerEnrollment();
			customerEnrollment.setStatus(EnrollStatus.OPN.name());
			customerEnrollment.setInternalRemark(dto.getInternalRemark());
			customerEnrollment.setCustomerId(idSerial);
			customerEnrollment.setSalesFollowBy(dto.getSalesFollowBy());
			customerEnrollment.setCreateDate(currentDate);
			customerEnrollment.setCreateBy(loginUserId);
			customerEnrollment.setUpdateDate(currentDate);
			customerEnrollment.setUpdateBy(loginUserId);
			customerEnrollmentDao.save(customerEnrollment);
			customerId = idSerial;

			customerEnrollmentService.recordCustomerEnrollmentUpdate(null,EnrollStatus.OPN.name(),idSerial);
			
		}
		CustomerProfile cpRetrieve = customerProfileDao.getById(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS,cpRetrieve);
		return responseResult;
	}

	private void saveOrUpdateOptionalOrFavorForPositionTitleAndCompanyNameOnly(String positionTitle,Long positionCaptionId, String companyName, Long companyNameCaptionId, Long customerId,String createBy){
				CustomerAdditionInfo  cai= customerAdditionInfoDao.getByCustomerIdAndCaptionId(customerId, Long.valueOf(positionCaptionId));
				if(cai==null){
					 cai = new CustomerAdditionInfo();
					CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
					cpk.setCaptionId(positionCaptionId);
					cpk.setCustomerId(customerId);
					cai.setId(cpk);	
					cai.setCreateDate(new Date());
					cai.setCreateBy(createBy);
					
				}else{
					cai.setUpdateBy(createBy);
					cai.setUpdateDate(new Date());
				}
				cai.setCustomerInput(positionTitle);
				customerAdditionInfoDao.saveOrUpdate(cai);
				
				CustomerAdditionInfo companyNameCai = customerAdditionInfoDao.getByCustomerIdAndCaptionId(customerId, Long.valueOf(companyNameCaptionId));
				if(companyNameCai==null){
					companyNameCai = new CustomerAdditionInfo();
					CustomerAdditionInfoPK companyNamecpk = new CustomerAdditionInfoPK();
					companyNamecpk.setCaptionId(companyNameCaptionId);
					companyNamecpk.setCustomerId(customerId);
					companyNameCai.setId(companyNamecpk);
					companyNameCai.setCreateDate(new Date());
					companyNameCai.setCreateBy(createBy);
				}else{
					companyNameCai.setUpdateBy(createBy);
					companyNameCai.setUpdateDate(new Date());
				}
				
				companyNameCai.setCustomerInput(companyName);
				customerAdditionInfoDao.saveOrUpdate(companyNameCai);
	}
	
	/**
	 * Method used to delete selected lead's record(reflected in several
	 * table,e.g. customer_profile, customer_enrollment)
	 * 
	 * @return ResponseMessage
	 */
	@Transactional
	public ResponseResult deleteLead(Long customerId,String salesLoginId) {

		CustomerProfile customerProfile = leadCustomerDao.getByCustomerId(customerId);
		CustomerEnrollment customerEnrollment = customerEnrollmentDao
				.getCustomerEnrollmentByCustomerId(customerId);
		
		if (customerEnrollment != null && customerProfile != null) {
			if(!customerEnrollment.getSalesFollowBy().equals(salesLoginId)) {
				responseResult.initResult(GTAError.LeadError.DELETE_PERMISSION_REJ);
				return responseResult;
			}
			if(!"OPN".equals(customerEnrollment.getStatus())) {
				responseResult.initResult(GTAError.LeadError.DELETE__FAIL_ONLY_OPN);
				return responseResult;
			}
			
			List<CustomerPreEnrollStage> customerPreEnrollStage = customerPreEnrollStageDao.getByCustomerId(customerId);
			
			if(customerPreEnrollStage.size()==0&&customerEnrollment.getStatus().equals(EnrollStatus.OPN.name())){
				customerAdditionInfoDao.deleteByCustomerId(customerId); 
				customerPreEnrollStageDao.deleteCustomerPreEnrollStage(customerPreEnrollStage);
				leadCustomerDao.deleteLeadCustomerProfile(customerProfile);
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}else{
				responseResult.initResult(GTAError.LeadError.DELETE_FAILED);
				return responseResult;
			}
			
		} else {
			responseResult.initResult(GTAError.LeadError.CUSTOMER_PROFILE_NOT_EXIST);
			return responseResult;
		}

	}

	/**
	 * Method to enroll Lead, result will be transfered in front-end
	 * @return ResponseResult
	 */
	@Transactional
	public ResponseResult enrollLead(Long customerId) {
		CustomerProfile customerProfile = leadCustomerDao.getByCustomerId(customerId);
		Data data = new Data();
		List<CustomerProfile> list = new ArrayList<CustomerProfile>();
		if (customerProfile != null) {
			customerProfile.setCustomerAdditionInfos(null);
			customerProfile.setCustomerAddresses(null);
			customerProfile.setCustomerEnrollments(null);
			list.add(customerProfile);
			data.setList(list);
			data.setLastPage(true);
			responseResult.initResult(GTAError.Success.SUCCESS, data);
		}else{
			responseResult.initResult(GTAError.LeadError.RETRIEVE_PROFILE_FAILED);
		}
		return responseResult;
	}
	
	@Transactional
	public ResponseResult movePortraitToCustomerFolder(CustomerProfile customerProfile) {
		Long customerId = customerProfile.getCustomerId();
		String portraitPhoto = customerProfile.getPortraitPhoto();
		
		if (portraitPhoto.equals("") || portraitPhoto == null) {

		} else {
			String basePath = "";
			try {
				basePath = FileUpload.getBasePath(FileUpload.FileCategory.USER);
			} catch (Exception e) {
				logger.debug("Upload Portrait: Cannot get the base path of the portrait.Contact backoffice!");
			}
			File customerFile = new File(basePath + File.separator
					+ customerId.longValue());
			if (!customerFile.exists()) {
				boolean isCreated = customerFile.mkdirs();
				if (!isCreated) {
					logger.error("Upload Portrait: Cannot create folder!Contact backoffice!");
				}
			}
			boolean isSucc1 = FileUtil.moveFile(basePath + portraitPhoto,
					basePath + File.separator + customerId.longValue()
							+ portraitPhoto);
			if (isSucc1) {
				customerProfile.setPortraitPhoto("/" + customerId.longValue()
						+  portraitPhoto);
			}
			customerProfileDao.update(customerProfile);
		}
		CustomerProfile cpRetrieve = customerProfileDao.getById(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS, cpRetrieve);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getCustomerProfileDetail(Long customerId){
		CustomerProfile cp = customerProfileDao.getById(customerId);
		/*Used to get the Position Tile in Table Customer Addition Info*/
		CustomerAdditionInfo customerAdditionInfoPositionTitle = customerAdditionInfoDao.getByCustomerIdAndCaptionId(customerId,Long.valueOf(1));
		if(customerAdditionInfoPositionTitle!=null){
			cp.setPositionTitle(customerAdditionInfoPositionTitle.getCustomerInput());
		}
		CustomerAdditionInfo customerAdditionInfoCompanyName = customerAdditionInfoDao.getByCustomerIdAndCaptionId(customerId, Long.valueOf(14));
		if(customerAdditionInfoCompanyName!=null){
			cp.setCompanyName(customerAdditionInfoCompanyName.getCustomerInput());
		}
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		List<Serializable> list  = new ArrayList<Serializable>();
		cp.setSalesFollowBy(customerEnrollment.getSalesFollowBy());
		list.add(cp);
		Data data = new Data();
		data.setList(list);
		data.setLastPage(true);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}

}
