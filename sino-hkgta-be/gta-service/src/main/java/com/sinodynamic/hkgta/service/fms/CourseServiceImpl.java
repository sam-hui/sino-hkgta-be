package com.sinodynamic.hkgta.service.fms;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.crm.*;
import com.sinodynamic.hkgta.dao.fms.*;
import com.sinodynamic.hkgta.dao.rpos.*;
import com.sinodynamic.hkgta.dto.ReportRequestDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.fms.*;
import com.sinodynamic.hkgta.dto.fms.MemberAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.fms.MemberInfoDto;
import com.sinodynamic.hkgta.dto.fms.NewCourseDto;
import com.sinodynamic.hkgta.dto.fms.StudentCourseAttendanceDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.staff.StaffCoachInfoDto;
import com.sinodynamic.hkgta.entity.crm.*;
import com.sinodynamic.hkgta.entity.fms.*;
import com.sinodynamic.hkgta.entity.rpos.*;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.common.ReportService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.*;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

@Service
@Scope("prototype")
public class CourseServiceImpl extends ServiceBase<CourseMaster> implements CourseService {

    private Logger logger = Logger.getLogger(CourseServiceImpl.class);

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private FacilityTimeslotDao facilityTimeslotDao;

    @Autowired
    private CourseSessionDao courseSessionDao;

    @Autowired
    private CourseMasterDao courseMasterDao;

    @Autowired
    private StaffTimeslotDao staffTimeslotDao;

    @Autowired
    private FacilityMasterDao facilityMasterDao;

    @Autowired
    private StaffMasterInfoDtoDao staffMasterDao;

    @Autowired
    private CourseSessionFacilityDao courseSessionFacilityDao;

    @Autowired
    private CourseEnrollmentDao courseEnrollmentDao;

    @Autowired
    private PosServiceItemPriceDao posServiceItemPriceDao;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private StaffProfileDao staffProfileDao;

    @Autowired
    private MessageTemplateDao messageTemplateDao;

    @Autowired
    private CustomerRefundRequestDao customerRefundRequestDao;

    @Autowired
    private CustomerEmailContentDao customerEmailContentDao;

    @Autowired
    private MemberFacilityAttendanceDao memberFacilityAttendanceDao;

    @Autowired
    private FacilityTimeslotAdditionDao facilityTimeslotAdditionDao;

    @Autowired
    private StaffFacilityScheduleDao staffFacilityScheduleDao;

    @Autowired
    private MemberReservedFacilityDao memberReservedFacilityDao;

    @Autowired
    private MemberCashvalueService memberCashvalueService;

    @Autowired
    private StudentCourseAttendanceDao studentCourseAttendanceDao;

    @Autowired
    private MemberCashValueDao memberCashValueDao;

    @Autowired
    private MemberLimitRuleDao memberLimitRuleDao;

    @Autowired
    private CustomerOrderTransDao customerOrderTransDao;

    @Autowired
    private CustomerOrderHdDao customerOrderHdDao;

    @Autowired
    private CustomerOrderDetDao customerOrderDetDao;

    @Autowired
    private CustomerProfileDao customerProfileDao;

    @Autowired
    private StaffCoachRoasterDao staffCoachRoasterDao;

    @Autowired
    private MemberLimitRuleDao limitRuleDao;

    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @Autowired
    private MemberFacilityTypeBookingService memberFacilityTypeBookingService;

    @Autowired
    private CourseEnrollmentService courseEnrollmentService;

    @Autowired
    private MailThreadService mailThreadService;

    @Autowired
    private CustomerServiceAccDao customerServiceAccDao;

    @Autowired
    private CustomerServiceAccService customerServiceAccService;

    //@Autowired
    private ServletContext servletContext;

    @Autowired
    private ReportService reportService;

    @Transactional
    public synchronized ResponseResult createCourseSession(String coachNo, String[] facilityNos, CourseSessionDto sessionDto, String userName) throws Exception {

        String otherTrainLocation = sessionDto.getOtherTrainLocation();
        String courseId = sessionDto.getCourseId();
        Date now = new Date();
        Timestamp createDate = new Timestamp(now.getTime());
        String createBy = userName;
        Date beginDatetime = DateCalcUtil.parseDateTime(sessionDto.getBeginDatetime());
        Date endDatetime = DateCalcUtil.parseDateTime(sessionDto.getEndDatetime());
        CourseMaster course = courseDao.getCourseMasterById(Long.parseLong(courseId));
        course.setUpdateBy(createBy);
        course.setUpdateDate(createDate);

        if (CommUtil.nvl(courseId).length() == 0 || beginDatetime == null || endDatetime == null || beginDatetime.after(endDatetime) || beginDatetime.before(course.getRegistDueDate())) {
            responseResult.initResult(GTAError.CourseError.SESSION_DTO_PARAMETER_ERROR);
            return responseResult;
        }

        if ((facilityNos != null && facilityNos.length > 0 && !StringUtils.isEmpty(otherTrainLocation)) ||
                ((facilityNos == null || facilityNos.length == 0) && StringUtils.isEmpty(otherTrainLocation))) {

            responseResult.initResult(GTAError.CourseError.COURSE_FACILITY_NOT_EMPTY);
            return responseResult;
        }

        // check if the coach exists
        StaffMaster staff = staffMasterDao.getByUserId(coachNo);
        if (staff == null) {
            responseResult.initResult(GTAError.CourseError.NO_COACH_EXISTS, new String[]{coachNo});
            return responseResult;
        }

        // check if the coach is occupied
        Boolean isCoachAvailble = staffTimeslotDao.isCoachVailable(coachNo, beginDatetime, endDatetime);
        if (!isCoachAvailble) {
            responseResult.initResult(GTAError.CourseError.SELECTED_COACH_UNAVAILABLE);
            return responseResult;
        }

        List<FacilityTimeslot> facilityTimeslots = new ArrayList<FacilityTimeslot>();
        if (facilityNos != null && facilityNos.length > 0) {

            // check if the facilities exists
            List<FacilityMaster> facilities = facilityMasterDao.getFacilityMasterByIdBatch(facilityNos);
            if (facilities == null || facilities.size() != facilityNos.length) {
                StringBuilder sb = new StringBuilder("{");
                for (String facilityNo : facilityNos) {
                    sb.append(facilityNo).append(",");
                }
                String tempInfo = sb.toString();
                String info = tempInfo.substring(0, tempInfo.length() - 1) + "}";

                responseResult.initResult(GTAError.CourseError.NO_FACILITY_EXISTS, new String[]{info.toString()});
                return responseResult;
            }

            // check if the facility is occupied
            for (String facilityNo : facilityNos) {
                Boolean isFacilityAvailable = facilityTimeslotDao.isFacilityAvailable(facilityNo, beginDatetime, endDatetime);
                if (!isFacilityAvailable) {
                    responseResult.initResult(GTAError.CourseError.SELECTED_FACILITY_UNVAILABLE, new String[]{facilityNo});
                    return responseResult;
                }
            }

            // save FacilityTimeslot
            for (FacilityMaster facility : facilities) {

                List<HashMap<String, Date>> hourSections = saperatePeriodByHour(beginDatetime, endDatetime);
                if (hourSections == null || hourSections.size() == 0)
                    continue;

                for (HashMap<String, Date> timeMap : hourSections) {

                    Date startTime = timeMap.get("startTime");
                    Date endTime = timeMap.get("endTime");

                    FacilityTimeslot facilityTimeslot = new FacilityTimeslot();
                    facilityTimeslot.setBeginDatetime(startTime);
                    facilityTimeslot.setEndDatetime(endTime);
                    facilityTimeslot.setCreateBy(createBy);
                    facilityTimeslot.setCreateDate(createDate);
                    facilityTimeslot.setFacilityMaster(facility);
                    facilityTimeslot.setStatus(FacilityStatus.OP.getDesc());
                    Long facilityTimeslotId = (Long) facilityTimeslotDao.save(facilityTimeslot);
                    facilityTimeslot.setFacilityTimeslotId(facilityTimeslotId);
                    facilityTimeslots.add(facilityTimeslot);
                }
            }
        }

        // save StaffTimeslot
        StaffTimeslot staffTimeslot = new StaffTimeslot();
        staffTimeslot.setBeginDatetime(beginDatetime);
        staffTimeslot.setEndDatetime(reduceOneSecond(endDatetime));
        staffTimeslot.setStaffUserId(coachNo);
        staffTimeslot.setCreateBy(createBy);
        staffTimeslot.setCreateDate(createDate);
        staffTimeslot.setStatus(Constant.STAFF_TIMESLOT_OP);
        staffTimeslot.setDutyCategory(Constant.STAFF_TIMESLOT_TRAINER);
        staffTimeslot.setDutyDescription(Constant.STAFF_TIMESLOT_CATEGORY_DESCRIPTION);
        String staffTimeslotId = (String) staffTimeslotDao.save(staffTimeslot);

        // save CourseSession
        CourseSession courseSession = new CourseSession();
        courseSession.setBeginDatetime(beginDatetime);
        courseSession.setEndDatetime(endDatetime);
        courseSession.setCoachTimeslotId(Long.parseLong(staffTimeslotId));
        courseSession.setCreateBy(createBy);
        courseSession.setOtherTrainLocation(otherTrainLocation);
        courseSession.setCreateDate(createDate);
        courseSession.setStatus("ACT");
        courseSession.setGatherLocation(sessionDto.getGatherLocation());
        courseSession.setCourseId(Long.parseLong(sessionDto.getCourseId()));
        courseSession.setCoachUserId(coachNo);
        courseSession.setUpdateBy(createBy);
        courseSession.setUpdateDate(createDate);
        Long sessionNo = courseSessionDao.getNextSessionNo(Long.parseLong(courseId));
        courseSession.setSessionNo(sessionNo);
        Long courseSessionId = (Long) courseSessionDao.addCourseSession(courseSession);

        //update CourseMaster
        courseDao.saveOrUpdate(course);
        
        // save CourseSessionFacility
        if (facilityTimeslots.size() > 0) {

            for (FacilityTimeslot facilityTimeslot : facilityTimeslots) {

                CourseSessionFacility courseSessionFacility = new CourseSessionFacility();
                courseSessionFacility.setCourseSessionId(courseSessionId);
                courseSessionFacility.setCreateBy(createBy);
                courseSessionFacility.setCreateDate(createDate);
                courseSessionFacility.setFacilityNo(String.valueOf(facilityTimeslot.getFacilityMaster().getFacilityNo()));
                courseSessionFacility.setFacilityTimeslot(facilityTimeslot);
                courseSessionFacilityDao.addCourseSessionFacility(courseSessionFacility);
            }
        }

        CourseMaster courseMaster = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", Long.parseLong(sessionDto.getCourseId()));
        Map<String, String> appMsgInfo = new HashMap<String, String>();
        String message = getSmsMessage(SceneType.COURSE_REMIND_COACH_JOIN, courseMaster, courseSession);

        if (logger.isDebugEnabled()) {
            logger.debug("message:" + message);
            logger.debug("coachUserId:" + coachNo);
        }
        appMsgInfo.put("message", message);
        appMsgInfo.put("coachUserId", coachNo);
        responseResult.initResult(GTAError.Success.SUCCESS, appMsgInfo);
        return responseResult;

    }

    /**
     * This method is used to retrieve list of available Courses.
     *
     * @param page
     * @param dto
     * @return ResponseResult (list of courses, pagination details etc)
     * @author Vineela_Jyothi
     */
    @Override
    @Transactional
    public ResponseResult getCourseList(ListPage<CourseMaster> page, CourseListDto dto) {
        try {

            Data data = new Data();
            ListPage<CourseMaster> courses = courseDao.getCourseList(page, dto);
            int count = courses.getDtoList().size();
            if (count == 0) {
                data.setList(new ArrayList<>());
                responseResult.initResult(GTAError.Success.SUCCESS, data);
                return responseResult;

            }

            data.setList(courses.getDtoList());
            data.setTotalPage(page.getAllPage());
            data.setCurrentPage(page.getNumber());
            data.setPageSize(page.getSize());
            data.setRecordCount(page.getAllSize());
            data.setLastPage(page.isLast());

            responseResult.initResult(GTAError.Success.SUCCESS, data);
            return responseResult;

        } catch (Exception e) {
            logger.error("CourseServiceImpl : getCourseList method ", e);
            responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_COURSE_LIST);
            return responseResult;

        }
    }

	@Override
	@Transactional
	public ResponseResult createCourse(NewCourseDto dto) {
		logger.info("CourseServiceImpl.createCourse invocation start ...");

		if (StringUtils.isEmpty(dto.getCourseName())) {
			responseResult.initResult(GTAError.CourseError.COURSE_NAME_EMPTY);
			return responseResult;

		}
		
		if (!StringUtils.isEmpty(dto.getCourseDescription()) && dto.getCourseDescription().length() > 1000) {
			responseResult.initResult(GTAError.CourseError.COURSE_DESCRIPTION_TOO_LONG);
			return responseResult;

		}

		CourseMaster existingCourse = courseMasterDao.getCourseMaseterByTypeAndName(dto.getCourseType(), dto.getCourseName());
		if (null != existingCourse) {
			responseResult.initResult(GTAError.CourseError.COURSE_NAME_ALREADY_EXISTS);
			return responseResult;

		}

		if (null == dto.getRegistBeginDate() || null == dto.getRegistDueDate()) {
			responseResult.initResult(GTAError.CourseError.REGIST_DATE_NULL);
			return responseResult;
		}
		
		if (dto.getRegistBeginDate().after(dto.getRegistDueDate())) {
			responseResult.initResult(GTAError.CourseError.COURSE_REG_DATE_ERROR);
			return responseResult;
		}

		CourseMaster newCourse = new CourseMaster();
		newCourse.setCourseName(dto.getCourseName());
		newCourse.setRegistBeginDate(dto.getRegistBeginDate());
		newCourse.setRegistDueDate(dto.getRegistDueDate());
		newCourse.setAgeRangeCode(dto.getAgeRangeCode());
		newCourse.setCapacity(dto.getCapacity());
		newCourse.setMemberAcceptance(dto.getMemberAcceptance());
		newCourse.setCourseDescription(dto.getCourseDescription());
		newCourse.setCourseType(dto.getCourseType());
		newCourse.setPosterFilename(dto.getPosterFilename());

		newCourse.setStatus("OPN");
		newCourse.setCreateBy(dto.getCreateBy());
		newCourse.setUpdateBy(dto.getUpdateBy());
		newCourse.setCreateDate(new Timestamp(System.currentTimeMillis()));
		newCourse.setUpdateDate(new Timestamp(System.currentTimeMillis()));

		Long courseId = (Long) courseMasterDao.save(newCourse);
		CourseMaster course = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", courseId);
		String posItemNo = dto.getCourseType() + "COS" + String.format("%08d", courseId);
		course.setPosItemNo(posItemNo);

		PosServiceItemPrice posItem = new PosServiceItemPrice();
		posItem.setItemNo(posItemNo);
		posItem.setItemCatagory(dto.getCourseType());
		
		String posDesc = null;
		if (CourseType.GOLFCOURSE.getDesc().equals(dto.getCourseType())) posDesc = "Golf Course - " + dto.getCourseName();
		if (CourseType.TENNISCOURSE.getDesc().equals(dto.getCourseType())) posDesc = "Tennis Course - " + dto.getCourseName();
		posItem.setDescription(posDesc);
		posItem.setItemPrice(dto.getPrice());
		posItem.setStatus("ACT");
		posServiceItemPriceDao.save(posItem);

		logger.debug("CourseServiceImpl.createCourse invocation end ...");

		Map<String, Long> newId = new HashMap<String, Long>();
		newId.put("courseId", courseId);
		responseResult.initResult(GTAError.Success.SUCCESS, newId);
		return responseResult;
	}

	private List<HashMap<String, Date>> saperatePeriodByHour(Date startTime, Date endTime) {

		if (startTime == null || endTime == null)
			return null;
		List<HashMap<String, Date>> timeMapList = new ArrayList<HashMap<String, Date>>();
		Calendar cal = Calendar.getInstance();
		Date tempStartTime = startTime;
		Date tempEndTime = null;
		while (tempStartTime.before(endTime)) {

			cal.setTime(tempStartTime);
			cal.add(Calendar.HOUR_OF_DAY, 1);
			Date midTime = cal.getTime();
			cal.add(Calendar.SECOND, -1);
			tempEndTime = cal.getTime();
			HashMap<String, Date> map = new HashMap<String, Date>();
			map.put("startTime", tempStartTime);
			map.put("endTime", tempEndTime);
			timeMapList.add(map);
			tempStartTime = midTime;
		}

		return timeMapList;
	}

	private Date reduceOneSecond(Date param) {

		if (param == null)
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(param);
		cal.add(Calendar.SECOND, -1);
		return cal.getTime();
	}

	private List<StaffProfile> _getAvailableCoachList(String type, String beginDatetime, String endDatetime) throws Exception {
		List<StaffProfile> cpList = courseSessionDao.selectAvailableCoach(type, beginDatetime, endDatetime);
		List<StaffProfile> result = removeOnDutyCoach(cpList, beginDatetime, endDatetime, null);
		
		return result;
	}
	
	private Map<String, List<FacilityItemDto>> _getAvailableFacilities(String type, String beginDatetime, String endDatetime) {
		Map<String, List<FacilityItemDto>> result = new TreeMap<String, List<FacilityItemDto>>();

		if ("GOLF".equals(type)) {

			// normal facility in certain floor
			List<FacilityMaster> venueFloors = facilityMasterDao.getAllVenueFloor();
			if (venueFloors != null && venueFloors.size() > 0) {
				for (FacilityMaster facilityMaster : venueFloors) {
					Long floor = facilityMaster.getVenueFloor();
					List<FacilityItemDto> facilityItemDtos = facilityMasterDao.getFacilityStatusByFloorAndTime(type, beginDatetime, endDatetime, floor);
					if (facilityItemDtos != null && facilityItemDtos.size() > 0)
						result.put(floor.toString(), facilityItemDtos);
				}
			}

//			// addition facility with no floor, just for GOLF
//			List<FacilityItemDto> additionFacilityItemDtos = facilityMasterDao.getAdditionFacilityStatusByTime(type, beginDatetime, endDatetime);
//			if (additionFacilityItemDtos != null && additionFacilityItemDtos.size() > 0)
//				result.put("A", additionFacilityItemDtos);

		} else if ("TENNIS".equals(type)) {

			// normal facility for tennis
			List<FacilityItemDto> facilityItemDtos = facilityMasterDao.getFacilityStatusByFloorAndTime(type, beginDatetime, endDatetime, null);
			if (facilityItemDtos != null && facilityItemDtos.size() > 0)
				result.put("T", facilityItemDtos);
		}
		return result;
	}
	
	@Override
	@Transactional
	public List<StaffProfile> getAvailableCoachList(String type,
			List<String[]> periodList) {
		List<StaffProfile> result = new ArrayList<StaffProfile>();
		try{
			Map<StaffProfile, Integer> counter = new HashMap<StaffProfile, Integer>();
			
			for (String[] period : periodList){
				List<StaffProfile> profiles = _getAvailableCoachList(type, period[0], period[1]);
				for (StaffProfile profile : profiles){
					if(counter.containsKey(profile)){
						Integer count = counter.get(profile);
						counter.put(profile, count + 1);
					}else{
						counter.put(profile, 1);
					}
				}
			}
			
			
			for(StaffProfile profile : counter.keySet()){
				if(counter.get(profile).intValue() == periodList.size()){
					result.add(profile);
				}
			}
			return result;
		}catch(Exception e){
			logger.debug("getAvailableCoachList(String, List<String[]>) error", e);
			return result;
		}
		
	}
	
	@Override
	@Transactional
	public Map<String, List<FacilityItemDto>> getAvailableFacilities(
			String type, List<String[]> periodList) {
		Map<String, List<FacilityItemDto>> result = new TreeMap<String, List<FacilityItemDto>>();
		
		for (String[] period : periodList){
			Map<String, List<FacilityItemDto>> facilities = _getAvailableFacilities(type, period[0], period[1]);
			
			for(String key : facilities.keySet()){//which floor ?
				if(result.get(key) == null){//init counter
					result.put(key, new ArrayList<FacilityItemDto>());
				}
				
				List<FacilityItemDto> facilityDtoes = facilities.get(key);//get facilities by floor
				List<FacilityItemDto> newFacilityDtoes = result.get(key);//get the counter for floor
				
				for(FacilityItemDto dto : facilityDtoes){
					if(newFacilityDtoes.contains(dto)){
						int index = newFacilityDtoes.indexOf(dto);
						FacilityItemDto newFacilityDto = newFacilityDtoes.get(index);
						if(!StringUtils.equalsIgnoreCase(FacilityStatus.VC.name(), dto.getStatus())
								&& StringUtils.equalsIgnoreCase(FacilityStatus.VC.name(), newFacilityDto.getStatus()) ){
							
							
							newFacilityDto.setStatus(dto.getStatus());
						}
					}else{
						newFacilityDtoes.add(dto);
					}
				}
			}
		}
		
		
		return result;
	}

	@Override
	@Transactional
	public ResponseResult getAvailableCoachList(String type, String beginDatetime, String endDatetime) throws Exception {
		responseResult.initResult(GTAError.Success.SUCCESS, _getAvailableCoachList(type, beginDatetime, endDatetime));
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getAvailableFacilities(String type, String beginDatetime, String endDatetime) {
		responseResult.initResult(GTAError.Success.SUCCESS, _getAvailableFacilities(type, beginDatetime, endDatetime));
		return responseResult;
	}

	/**
	 * This method is used to get the details of a course
	 * 
	 * @param courseId
	 * @return ResponseResult (Course Info)
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult getCourseInfo(String courseId, String device) {

		try {
			if (StringUtils.isEmpty(courseId)) {
				responseResult.initResult(GTAError.CourseError.COURSE_ID_EMPTY);
				return responseResult;
			}

			CourseListDto coursePrimaryInfo = courseMasterDao.getCourseInfo(courseId);
			
			if (null == coursePrimaryInfo) {
				responseResult.initResult(GTAError.CourseError.COURSE_DOESNT_EXIST);
				return responseResult;
			}

			List<CourseSessionDetailsDto> sessions = courseSessionDao.getCourseSessionDetails(courseId, device);

			coursePrimaryInfo.setSessions(sessions);
			
			responseResult.initResult(GTAError.Success.SUCCESS, coursePrimaryInfo);
			return responseResult;

		} catch (Exception e) {
			logger.error("CourseServiceImpl : getCourseInfo method ", e);
			responseResult.initResult(GTAError.CourseError.GET_COURSE_DETAILS_FAIL);
			return responseResult;
		}
	}

	/**
	 * This method is used to view list of enrolled members info for a course
	 * 
	 * @param page
	 * @param courseId
	 * @param status
	 * @return ResponseResult (Primary course info + members info)
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult getMemberInfo(ListPage<CourseEnrollment> page, String courseId, String attendance, String status,String sessionId) {

		try {
			if (CommUtil.notEmpty(courseId)) {

				CourseMaster courseMaster = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", new Long(courseId));

				if (null == courseMaster) {
					responseResult.initResult(GTAError.CourseError.COURSE_DOESNT_EXIST);
				} else {

					CourseMembersDto course = new CourseMembersDto();
					CourseListDto coursePrimayInfo = courseMasterDao.getCourseInfo(courseId);

					course.setCapacity(coursePrimayInfo.getCapacity());
					course.setEnrollment(coursePrimayInfo.getEnrollment());
					course.setCourseName(coursePrimayInfo.getCourseName());
					course.setMemberAcceptance(coursePrimayInfo.getMemberAcceptance());

					List<MemberInfoDto> members = courseEnrollmentDao.getMemberInfoList(page, attendance, courseId, status, course.getMemberAcceptance());
					List<MemberAttendanceDto> attendanceList;
					for (MemberInfoDto member : members) {
						
						attendanceList = courseEnrollmentDao.getMemberAttendance(courseId, member.getEnrollId(),sessionId);
						member.setAttendance(attendanceList);
						
					}
					

					Data data = new Data();
					course.setMembers(members);

					List<CourseMembersDto> courseData = new ArrayList<CourseMembersDto>();
					courseData.add(course);

					data.setList(courseData);
					data.setTotalPage(page.getAllPage());
					data.setCurrentPage(page.getNumber());
					data.setPageSize(page.getSize());
					data.setRecordCount(members.size());
					data.setLastPage(page.isLast());

					responseResult.initResult(GTAError.Success.SUCCESS, data);

				}

			}
			return responseResult;

		} catch (Exception e) {
			logger.error("CourseServiceImpl : getMemberInfo method ", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_MEMBER_LIST);
			return responseResult;
		}
	}

	/*
	@Override
	@Transactional
	public ResponseResult updateCourseStatus(CourseListDto dto) {
		try {
			CourseMaster course = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", dto.getCourseId());

			if (null == course) {
				responseResult.initResult(GTAError.CourseError.COURSE_DOESNT_EXIST);
				return responseResult;
			}

			if (CommUtil.notEmpty(dto.getStatus())) {
				course.setStatus(dto.getStatus());
				course.setUpdateBy(dto.getUpdateBy());
				course.setUpdateDate(dto.getUpdateDate());

			}

			courseMasterDao.update(course);

			responseResult.initResult(GTAError.Success.SUCCESS, course);
			return responseResult;

		} catch (Exception e) {
			logger.error("CourseServiceImpl : updateCourseStatus method ", e);
			responseResult.initResult(GTAError.CourseError.FAIL_TO_UPDATE_COURSE_STATUS);
			return responseResult;
		}
	}
*/
	@Override
	@Transactional
	public ResponseResult modifyCourseSession(String coachNo,
			String[] facilityNos, CourseSessionDto sessionDto, String username)
			throws Exception {
	    
		Date now = new Date();
		Timestamp updateDate = new Timestamp(now.getTime());
		Timestamp createDate = updateDate;
		String updateBy = username;
		String createBy = updateBy;
		Date beginDatetime = DateCalcUtil.parseDateTime(sessionDto.getBeginDatetime());
		Date endDatetime = DateCalcUtil.parseDateTime(sessionDto.getEndDatetime());
		String otherTrainLocation = sessionDto.getOtherTrainLocation();
		
	    	if ((facilityNos != null && facilityNos.length > 0 && !StringUtils.isEmpty(otherTrainLocation)) ||
	    		((facilityNos == null || facilityNos.length == 0) && StringUtils.isEmpty(otherTrainLocation))) {
	    	    
			responseResult.initResult(GTAError.CourseError.COURSE_FACILITY_NOT_EMPTY);
			return responseResult;
	    	}

		// check if the courseSession exists
		CourseSession session = courseSessionDao.getCourseSessionById(sessionDto.getSysId());
		if (session == null) {
			String sessionId = String.valueOf(sessionDto.getSysId());
			responseResult.initResult(GTAError.CourseError.NO_COURSE_SESSION_EXISTS, new String[] { sessionId });
			throw new CourseSessionCustomizeException(responseResult);
		}
		
		//added by vicky wang 2015-11-03, 		
		String existingCoachNo = session.getCoachUserId();
		if(logger.isDebugEnabled()){
			logger.debug("existing coachUserId:" + existingCoachNo);
		}
				
		// check if the coach exists
		StaffMaster staff = staffMasterDao.getByUserId(coachNo);
		if (staff == null) {
			responseResult.initResult(GTAError.CourseError.NO_COACH_EXISTS, new String[] { coachNo });
			throw new CourseSessionCustomizeException(responseResult);
		}

		// check if the facilities exists
		List<FacilityMaster> facilities = null;
		if (facilityNos != null && facilityNos.length > 0) {
		    
		    facilities = facilityMasterDao.getFacilityMasterByIdBatch(facilityNos);
		    if (facilities == null || facilities.size() != facilityNos.length) {
			StringBuilder sb = new StringBuilder("{");
			for (String facilityNo : facilityNos) {
			    sb.append(facilityNo).append(",");
			}
			String tempInfo = sb.toString();
			String info = tempInfo.substring(0, tempInfo.length() - 1) + "}";
			responseResult.initResult(GTAError.CourseError.NO_FACILITY_EXISTS, new String[] { info.toString() });
			throw new CourseSessionCustomizeException(responseResult);
		    }
		}

		CourseMaster course = courseDao.getCourseMasterById(session.getCourseId());
		if (beginDatetime == null
			|| endDatetime == null || beginDatetime.after(endDatetime)
			|| beginDatetime.before(course.getRegistDueDate())) {
		    responseResult.initResult(GTAError.CourseError.COURSE_SESSION_TIME_ERROR);
		    throw new CourseSessionCustomizeException(responseResult);
		}

		// release coach time check-in
		staffTimeslotDao.deleteStaffTimeslotById(String.valueOf(session.getCoachTimeslotId()));

		// release facility time check-in, delete old facilities for current
		// course session
		List<CourseSessionFacility> courseSessionFacilities = courseSessionFacilityDao.selectCourseSessionFacilityBySessionId(session.getSysId());
		if (courseSessionFacilities != null && courseSessionFacilities.size() > 0) {
		    
		    for (CourseSessionFacility csf : courseSessionFacilities) {
			FacilityTimeslot facilityTimeslot = csf.getFacilityTimeslot();
			facilityTimeslot.getFacilityMaster().getFacilityTimeslots().remove(facilityTimeslot);
			facilityTimeslot.setFacilityMaster(null);
			facilityTimeslot.setMemberFacilityTypeBooking(null);
			facilityTimeslot.setStaffFacilitySchedule(null);
			long facilityTimeslotId = facilityTimeslot.getFacilityTimeslotId();
			courseSessionFacilityDao.deleteCourseSessionFacility(csf);
			facilityTimeslotDao.deleteFacilityTimeslotById(facilityTimeslotId);
		    }
		}

		// check if the coach is occupied
		Boolean isCoachAvailble = staffTimeslotDao.isCoachVailable(coachNo,beginDatetime, endDatetime);
		if (!isCoachAvailble) {
			responseResult.initResult(GTAError.CourseError.SELECTED_COACH_UNAVAILABLE);
			throw new CourseSessionCustomizeException(responseResult);
		}

		// check if the facility is occupied
		if (facilityNos != null && facilityNos.length > 0) {
		    for (String facilityNo : facilityNos) {
			Boolean isFacilityAvailable = facilityTimeslotDao.isFacilityAvailable(facilityNo, beginDatetime, endDatetime);
			if (!isFacilityAvailable) {
			    responseResult.initResult(GTAError.CourseError.SELECTED_FACILITY_UNVAILABLE,new String[] { facilityNo });
			    throw new CourseSessionCustomizeException(responseResult);
			}
		    }
		}

		// add new coach time check-in
		StaffTimeslot staffTimeslot = new StaffTimeslot();
		staffTimeslot.setBeginDatetime(beginDatetime);
		staffTimeslot.setEndDatetime(reduceOneSecond(endDatetime));
		staffTimeslot.setStaffUserId(coachNo);
		staffTimeslot.setCreateBy(createBy);
		staffTimeslot.setCreateDate(createDate);
		staffTimeslot.setStatus(Constant.STAFF_TIMESLOT_OP);
		staffTimeslot.setDutyCategory(Constant.STAFF_TIMESLOT_TRAINER);
		staffTimeslot.setDutyDescription(Constant.STAFF_TIMESLOT_CATEGORY_DESCRIPTION);
		String staffTimeslotId = (String) staffTimeslotDao.save(staffTimeslot);

		// add new facility time check-in
		if (facilities != null && facilities.size() > 0) {
		    
		    List<FacilityTimeslot> facilityTimeslots = new ArrayList<FacilityTimeslot>();
		    for (FacilityMaster facility : facilities) {
			
			List<HashMap<String, Date>> hourSections = saperatePeriodByHour(beginDatetime, endDatetime);
			if (hourSections == null || hourSections.size() == 0) continue;
			
			for (HashMap<String, Date> timeMap : hourSections) {
			    
			    Date startTime = timeMap.get("startTime");
			    Date endTime = timeMap.get("endTime");
			    FacilityTimeslot facilityTimeslot = new FacilityTimeslot();
			    facilityTimeslot.setBeginDatetime(startTime);
			    facilityTimeslot.setEndDatetime(endTime);
			    facilityTimeslot.setCreateBy(createBy);
			    facilityTimeslot.setCreateDate(createDate);
			    facilityTimeslot.setFacilityMaster(facility);
			    facilityTimeslot.setStatus(FacilityStatus.OP.getDesc());
			    Long facilityTimeslotId = (Long) facilityTimeslotDao.save(facilityTimeslot);
			    facilityTimeslot.setFacilityTimeslotId(facilityTimeslotId);
			    facilityTimeslots.add(facilityTimeslot);
			}
		    }
		    
		    // add new facilities for current session
		    for (FacilityTimeslot facilityTimeslot : facilityTimeslots) {
			
			CourseSessionFacility courseSessionFacility = new CourseSessionFacility();
			courseSessionFacility.setCourseSessionId(sessionDto.getSysId());
			courseSessionFacility.setCreateBy(createBy);
			courseSessionFacility.setCreateDate(createDate);
			courseSessionFacility.setFacilityNo(String.valueOf(facilityTimeslot.getFacilityMaster().getFacilityNo()));
			courseSessionFacility.setFacilityTimeslot(facilityTimeslot);
			courseSessionFacilityDao.addCourseSessionFacility(courseSessionFacility);
		    }
		}

		// update course ssession info
		session.setBeginDatetime(beginDatetime);
		session.setEndDatetime(endDatetime);
		session.setCoachTimeslotId(Long.parseLong(staffTimeslotId));
		session.setUpdateBy(updateBy);
		session.setUpdateDate(updateDate);
		session.setGatherLocation(sessionDto.getGatherLocation());
		session.setCoachUserId(coachNo);
		session.setOtherTrainLocation(otherTrainLocation);
		courseSessionDao.updateCourseSession(session);
		
        //update CourseMaster
        course.setUpdateBy(updateBy);
        course.setUpdateDate(updateDate);
        courseDao.saveOrUpdate(course);
		
		//get sms info to send
		Map<String, Object> smsInfo = notifyCourseMembers(SceneType.COURSE_CHANGE, course);		
		smsInfo = notifyCourseCoach(smsInfo,existingCoachNo, coachNo, course,session);
		
		//responseResult.initResult(GTAError.Success.SUCCESS);
		if(logger.isDebugEnabled()){
			logger.debug("smsInfo:" + smsInfo.toString());			
		}
		responseResult.initResult(GTAError.Success.SUCCESS, smsInfo);
		return responseResult;
	}

	//add this method to add the coach reminder message into the response result.
	private Map<String, Object> notifyCourseCoach(Map<String, Object> smsInfo, String oldCoachNo, String newCoachNo, CourseMaster course, CourseSession session) {

		// add by vicky wang, when session update, coach should receive the
		// mobile message
		
		if(logger.isDebugEnabled()){
			logger.debug("notifyCoach() start." );
			logger.debug("oldCoachNo:" + oldCoachNo!=null? oldCoachNo : "");
			logger.debug("newCoachNo:" + newCoachNo!=null? newCoachNo : "");
		}
		
		if(oldCoachNo == null || "".equals(oldCoachNo)){
			logger.error("invalid coach no.");
			return null;
		}
		if(smsInfo==null || smsInfo.isEmpty()){
			smsInfo = new HashMap<String, Object>(); 
		}
		String[] coachUserId = new String[1];
		String[] newCoachUserId = new String[1];
		if (newCoachNo != null && !oldCoachNo.equals(newCoachNo)) 
		{

				// if coach changed, both original coach and new coach should
				// receive the message.
				// StaffProfile oldCoachProfile =
				// staffProfileDao.getStaffProfileByUserId(oldCoachNo);
				// StaffProfile newCoachProfile =
				// staffProfileDao.getStaffProfileByUserId(newCoachNo);
				String messageForOldCoach = getSmsMessage(SceneType.COURSE_REMIND_COACH_MOVE, course, session);
				String messageForNewCoach = getSmsMessage(SceneType.COURSE_REMIND_COACH_JOIN, course, session);

				if (logger.isDebugEnabled()) {
					logger.debug("messageForOldCoach:" + messageForOldCoach);
					logger.debug("messageForNewCoach:" + messageForNewCoach);
				}
				coachUserId[0] = oldCoachNo;
				smsInfo.put("coachUserId", coachUserId);
				smsInfo.put("coachMessage", messageForOldCoach);

				newCoachUserId[0] = newCoachNo;
				smsInfo.put("newCoachUserId", newCoachUserId);
				smsInfo.put("newCoachMessage", messageForNewCoach);

			} else {
				// if coach not changed, send the message to coach.
				// StaffProfile coachProfile =
				// staffProfileDao.getStaffProfileByUserId(newCoachNo);

				String message = getSmsMessage(SceneType.COURSE_REMIND_COACH_UPDATE, course, session);
				if (logger.isDebugEnabled()) {
					logger.debug("message:" + message);
				}
				coachUserId[0] = oldCoachNo;
				smsInfo.put("coachUserId", coachUserId);
				smsInfo.put("coachMessage", message);

			}
		return smsInfo;
	}
	
	
	@Override
	@Transactional
	public ResponseResult getCourseSessionInfo(Long sysId, String type) throws Exception {

		CourseSession session = courseSessionDao.getCourseSessionById(sysId);
		Date begin = session.getBeginDatetime();
		Date end = session.getEndDatetime();
		String beginDatetime = DateCalcUtil.formatDatetime(begin);
		String endDatetime = DateCalcUtil.formatDatetime(end);
		List<CourseSessionFacility> currentFacilities = courseSessionFacilityDao.selectCourseSessionFacilityBySessionId(sysId);
		StaffTimeslot currentStaff = staffTimeslotDao.getStaffTimeslotById(String.valueOf(session.getCoachTimeslotId()));

		Map<String, List<FacilityItemDto>> avaialbeFacilities = new TreeMap<String, List<FacilityItemDto>>();

		if ("GOLF".equals(type)) {

			// normal facility in certain floor
			List<FacilityMaster> venueFloors = facilityMasterDao.getAllVenueFloor();
			if (venueFloors != null && venueFloors.size() > 0) {
				for (FacilityMaster facilityMaster : venueFloors) {
					Long floor = facilityMaster.getVenueFloor();
					List<FacilityItemDto> facilityItemDtos = facilityMasterDao.getFacilityStatusByFloorAndTime(type, beginDatetime, endDatetime, floor);
					if (facilityItemDtos != null && facilityItemDtos.size() > 0)
						avaialbeFacilities.put(floor.toString(), facilityItemDtos);
				}
			}

//			// addition facility with no floor, just for GOLF
//			List<FacilityItemDto> additionFacilityItemDtos = facilityMasterDao.getAdditionFacilityStatusByTime(type, beginDatetime, endDatetime);
//			if (additionFacilityItemDtos != null && additionFacilityItemDtos.size() > 0)
//				avaialbeFacilities.put("A", additionFacilityItemDtos);

		} else if ("TENNIS".equals(type)) {

			// normal facility for tennis
			List<FacilityItemDto> facilityItemDtos = facilityMasterDao.getFacilityStatusByFloorAndTime(type, beginDatetime, endDatetime, null);
			if (facilityItemDtos != null && facilityItemDtos.size() > 0)
				avaialbeFacilities.put("T", facilityItemDtos);
		}

		List<StaffProfile> staffUsers = courseSessionDao.selectAvailableCoachContainCurrent(type, beginDatetime, endDatetime, (currentStaff != null ? currentStaff.getStaffUserId() : null));
		List<StaffProfile> availableCoaches = removeOnDutyCoach(staffUsers, beginDatetime, endDatetime, (currentStaff != null ? currentStaff.getStaffUserId() : null));
		
		// mark current facilities
		if (avaialbeFacilities.size() > 0 && currentFacilities != null && currentFacilities.size() > 0) {

			Iterator<Map.Entry<String, List<FacilityItemDto>>> iterator = avaialbeFacilities.entrySet().iterator();
			while (iterator.hasNext()) {

				Map.Entry<String, List<FacilityItemDto>> entry = (Map.Entry<String, List<FacilityItemDto>>) iterator.next();
				List<FacilityItemDto> facilityItemDtos = (List<FacilityItemDto>) entry.getValue();
				for (FacilityItemDto item : facilityItemDtos) {
					if (isContained(item, currentFacilities))
						item.setStatus("ST");
				}
			}
		}

		// mark current coach
		if (availableCoaches != null && availableCoaches.size() > 0 && currentStaff != null) {
			for (StaffProfile coach : availableCoaches) {
				if (coach.getUserId().equals(currentStaff.getStaffUserId()))
					coach.setPostalArea("ST");
			}
		}

		// set final data
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("session", session);
		data.put("facilities", avaialbeFacilities);
		data.put("coaches", availableCoaches);

		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	private boolean isContained(FacilityItemDto item, List<CourseSessionFacility> facilities) {

		if (item == null || facilities == null || facilities.size() == 0)
			return false;
		for (CourseSessionFacility facility : facilities) {
			if (facility.getFacilityNo().equals(String.valueOf(item.getFacilityNo())))
				return true;
		}
		return false;
	}

	@Override
	@Transactional
	public ResponseResult changeCourseSessionStatus(String sysId, String status, String userId) throws Exception {
		Date upTime = new Timestamp(new Date().getTime());	
		CourseSession courseSession = courseSessionDao.get(CourseSession.class, Long.parseLong(sysId));		
		courseSession.setStatus(status.toUpperCase());
		courseSession.setUpdateBy(userId);
		courseSession.setUpdateDate(upTime);
		courseSessionDao.update(courseSession);

        //update CourseMaster
        CourseMaster course = courseDao.getCourseMasterById(courseSession.getCourseId());
        course.setUpdateBy(userId);
        course.setUpdateDate(upTime);
        courseDao.saveOrUpdate(course);
		
		Map<String, Object> smsInfo = null;
		
		if (status.toUpperCase().equals("DEL") || status.toUpperCase().equals("CAN")) {
			
			// release facility time check-in, delete old facilities for current
			// course session
			List<CourseSessionFacility> courseSessionFacilities = courseSessionFacilityDao
					.selectCourseSessionFacilityBySessionId(courseSession.getSysId());
			for (CourseSessionFacility csf : courseSessionFacilities) {

				FacilityTimeslot facilityTimeslot = csf.getFacilityTimeslot();
				facilityTimeslot.getFacilityMaster().getFacilityTimeslots()
						.remove(facilityTimeslot);
				facilityTimeslot.setFacilityMaster(null);
				facilityTimeslot.setMemberFacilityTypeBooking(null);
				facilityTimeslot.setStaffFacilitySchedule(null);
				long facilityTimeslotId = facilityTimeslot.getFacilityTimeslotId();
				courseSessionFacilityDao.deleteCourseSessionFacility(csf);
				facilityTimeslotDao.deleteFacilityTimeslotById(facilityTimeslotId);
			}
			
			//if status is DEL, then change the session_no adding 1 for the nexting part
			if (status.toUpperCase().equals("DEL")) changeSessionNoAddingOne(sysId);
			
			smsInfo = notifyCourseMembers(SceneType.COURSE_CHANGE, course);
			
			String messageForCoach = getSmsMessage(SceneType.COURSE_SESSON_CANCEL, course,courseSession);
			String coachUserId = courseSession.getCoachUserId();
			
			if(smsInfo==null)
				smsInfo = new HashMap<String, Object>();
			smsInfo.put("coachUserId", coachUserId);
			smsInfo.put("coachMessage", messageForCoach);
		}
		if (null != courseSession.getCoachTimeslotId()) {
			staffTimeslotDao.deleteById(StaffTimeslot.class, courseSession.getCoachTimeslotId().toString());
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, smsInfo);
		return responseResult;
	}

	
	@Override
	@Transactional
	public ResponseResult updateCourse(NewCourseDto dto) {
		try {
			if (null == dto.getCourseId()) {
				responseResult.initResult(GTAError.CourseError.PROVIDE_COURSE_ID_AND_OTHER_DETAILS);
				return responseResult;

			}
			
			if (!StringUtils.isEmpty(dto.getCourseDescription()) && dto.getCourseDescription().length() > 1000) {
				responseResult.initResult(GTAError.CourseError.COURSE_DESCRIPTION_TOO_LONG);
				return responseResult;

			}
			
			CourseMaster course = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", dto.getCourseId());

			if (null == course) {
				responseResult.initResult(GTAError.CourseError.COURSE_DOESNT_EXIST);
				return responseResult;
			}
			
			if (!StringUtils.isEmpty(course.getCourseName()) && !course.getCourseName().equalsIgnoreCase(dto.getCourseName())) {
			    
				CourseMaster existingCourse = courseMasterDao.getCourseMaseterByTypeAndName(dto.getCourseType(), dto.getCourseName());
				if (null != existingCourse) {
					responseResult.initResult(GTAError.CourseError.COURSE_NAME_ALREADY_EXISTS);
					return responseResult;

				}
			}

			
			//Method decide if the course status should be change to open/closed
			ResponseResult result = changeCourseStatus(dto, course);
			if (result != null) return result; 

			if (null != dto.getCourseName())
				course.setCourseName(dto.getCourseName());
			if (null != dto.getRegistBeginDate())
				course.setRegistBeginDate(dto.getRegistBeginDate());
			if (null != dto.getRegistDueDate())
				course.setRegistDueDate(dto.getRegistDueDate());
			if (null != dto.getAgeRangeCode())
				course.setAgeRangeCode(dto.getAgeRangeCode());
			if (null != dto.getCapacity())
				course.setCapacity(dto.getCapacity());
			if (null != dto.getMemberAcceptance())
				course.setMemberAcceptance(dto.getMemberAcceptance());
			if (null != dto.getCourseDescription())
				course.setCourseDescription(dto.getCourseDescription());
			if (null != dto.getPosterFilename())
				course.setPosterFilename(dto.getPosterFilename());
			
			course.setUpdateBy(dto.getUpdateBy());
			course.setUpdateDate(dto.getUpdateDate());
			courseMasterDao.update(course);

			if(null != dto.getPrice() && null != course.getPosItemNo()){
				PosServiceItemPrice posItem = posServiceItemPriceDao.getUniqueByCol(PosServiceItemPrice.class, "itemNo", course.getPosItemNo());
				posItem.setItemPrice(dto.getPrice());
				posServiceItemPriceDao.save(posItem);
			}
			
			//get sms info to send
			Map<String, Object> smsInfo = notifyCourseMembers(SceneType.COURSE_CHANGE, course);
			
			//added by vicky wang to send app message for coach when create a new course.
			String editType = dto.getEditType();
			if("New".equals(editType))
			{
				//notify coach about the new course
				smsInfo = notifyCourseCoachCreate(course);
			}
			//notify coach end.
			
			responseResult.initResult(GTAError.Success.SUCCESS, smsInfo);
			return responseResult;

		} catch (Exception e) {
			logger.error("CourseServiceImpl : updateCourse method ", e);
			responseResult.initResult(GTAError.CourseError.FAIL_TO_UPDATE_COURSE_INFO);
			return responseResult;
		}
	}

	//when create a new course, after save, should push app message to all coach for the same message content, each coach will receive one app message.
	private Map<String, Object> notifyCourseCoachCreate(CourseMaster course) throws Exception
	{
		Map<String, Object> smsInfo = new HashMap<String,Object>();

		List<CourseSession> sessionList = courseSessionDao.getByCol(CourseSession.class, "courseId", course.getCourseId(), null);
		Date periodStartDate = null;
		Date periodEndDate = null;
		Set<String> coachList = new HashSet<String>();
		if(sessionList!=null && sessionList.size()>0)
		{
			for(int index =0; index < sessionList.size(); index++)
			{
				CourseSession session = sessionList.get(index);
				if(periodStartDate == null)
				{
					periodStartDate = session.getBeginDatetime();
				}else if(session.getBeginDatetime().before(periodStartDate))
				{
					periodStartDate = session.getBeginDatetime();
				}
				if(periodEndDate == null)
				{
					periodEndDate = session.getEndDatetime();
				}else if(session.getEndDatetime().after(periodEndDate))
				{
					periodEndDate = session.getEndDatetime();
				}
				String coachNo = session.getCoachUserId();
				coachList.add(coachNo);
			}

			String functionId = "";
			if (CourseType.GOLFCOURSE.getDesc().equals(course.getCourseType()))
			{
				functionId = "golf_course_create";
			}
			if (CourseType.TENNISCOURSE.getDesc().equals(course.getCourseType()))
			{
				functionId = "tennis_course_create";
			}

			String courseStartDate =  DateCalcUtil.formatDate(periodStartDate);
			String courseEndDate = DateCalcUtil.formatDate(periodEndDate);
			String courseDate = courseStartDate + " to " + courseEndDate;
			MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(functionId);
			String message = mt.getFullContent(course.getCourseName(), courseDate);

			smsInfo.put("coachUserList",coachList);
			smsInfo.put("coachMessage", message);
		}

		return smsInfo;
	}


	@Override
	@Transactional
	public ResponseResult uploadFeatureImage(MultipartFile image) {
		if (image == null || image.isEmpty()) {
			logger.error("Image is empty!");
			responseResult.initResult(GTAError.CourseError.UPLOAD_IMAGE_EMPTY);
			return responseResult;
		}
		String saveFilePath = "";
		try {
			saveFilePath = FileUpload.upload(image, FileUpload.FileCategory.COURSE);
		} catch (Exception e) {
			logger.error(CourseServiceImpl.class.getName() + " uploadFeatureImage Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_TO_UPLOAD_IMAGE);

			return responseResult;

		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/" + saveFilePath);

		responseResult.setData(imgPathMap);
		responseResult.initResult(GTAError.Success.SUCCESS, imgPathMap);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult getCourseAndMemberInfo(String customerId, String courseId) {
	    
	    MemberCashvalueDto memberCashvalueDto = getMemberInfo(customerId);

		CourseEnrollment ce = courseEnrollmentDao.getCourseEnrollmentByCourseIdAndCustomerId(Long.parseLong(courseId), Long.parseLong(customerId));
		if (ce != null) {
			if (ce.getCustOrderDetId() != null) {
			    List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(ce.getCustOrderDetId());
			    if (customerOrderTranses != null && customerOrderTranses.size() > 0) {
				memberCashvalueDto.setTransactionId(customerOrderTranses.get(0).getTransactionNo());
			    }
			}
		}
	    
	    CourseSimpleInfoDto courseInfoDto = null;
	    if (CommUtil.notEmpty(courseId)) {
			courseInfoDto = courseDao.getCourseInfo(courseId);
			//add for web portal display
			CourseMaster courseMaster = courseMasterDao.get(CourseMaster.class, courseInfoDto.getCourseId());
			courseInfoDto.setAgeRange(courseMaster.getAgeRangeCode());
			courseInfoDto.setCourseType(courseMaster.getCourseType());
			List<CourseSessionDetailsDto> courseSessions = courseSessionDao.getCourseSessionDetails(String.valueOf(courseInfoDto.getCourseId()));
			if(courseSessions!=null && courseSessions.size()>0){
				courseInfoDto.setTime(courseSessions.get(0).getBeginDatetime());
			}
			//end
	    }
	    
	    Map<String, Object> result = new HashMap<String, Object>();
	    result.put("memberInfo", memberCashvalueDto);
	    result.put("courseInfo", courseInfoDto);
	    
	    responseResult.initResult(GTAError.Success.SUCCESS, result);
	    return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getCourseAndMemberInfo(String customerId, String courseId, Long refundId) {

	    MemberCashvalueDto memberCashvalueDto = getMemberInfo(customerId);

	    CourseSimpleInfoDto courseInfoDto = null;
	    if (CommUtil.notEmpty(courseId)) {
			courseInfoDto = courseDao.getCourseInfo(courseId);
			//add for web portal display
			CourseMaster courseMaster = courseMasterDao.get(CourseMaster.class, courseInfoDto.getCourseId());
			courseInfoDto.setAgeRange(courseMaster.getAgeRangeCode());
			courseInfoDto.setCourseType(courseMaster.getCourseType());
			List<CourseSessionDetailsDto> courseSessions = courseSessionDao.getCourseSessionDetails(String.valueOf(courseInfoDto.getCourseId()));
			if(courseSessions!=null && courseSessions.size()>0){
				courseInfoDto.setTime(courseSessions.get(0).getBeginDatetime());
			}

			//add payment amount when order generate
			CustomerRefundRequest refundRequest = customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
			if (refundRequest != null) {
			    CustomerOrderHd orderHd = customerOrderHdDao.get(CustomerOrderHd.class, refundRequest.getOrderDetailId());
			    if (orderHd != null) courseInfoDto.setPrice(orderHd.getOrderTotalAmount());
			}

	    }

	    Map<String, Object> result = new HashMap<String, Object>();
	    result.put("memberInfo", memberCashvalueDto);
	    result.put("courseInfo", courseInfoDto);

	    responseResult.initResult(GTAError.Success.SUCCESS, result);
	    return responseResult;
	}

	@Override
	@Transactional
	public MemberCashvalueDto getMemberInfo(String customerId) {
		if (!CommUtil.notEmpty(customerId) && !CommUtil.notEmpty(customerId)) return null;
	    
	    MemberCashvalueDto memberCashvalueDto = null;
	    if (CommUtil.notEmpty(customerId)) {
		memberCashvalueDto = memberDao.getMemberCashvlueInfo(customerId);
	    }
	    
	    //Extral process for dependency member
	    Member member = memberDao.getMemberById(Long.parseLong(customerId));
	    if(member==null) {
	    	throw new GTACommonException(GTAError.MemberError.MEMBER_NOT_FOUND);
	    }
	    String memberType = member.getMemberType();
	    if("IDM".equals(memberType) || "CDM".equals(memberType)){
		Long tempCustomerId = member.getSuperiorMemberId();//superior customer id
		if (tempCustomerId != null) {
		    MemberCashvalue memberCashvalue = memberCashValueDao.getByCustomerId(tempCustomerId);
		    if (memberCashvalue != null) memberCashvalueDto.setCashValue(memberCashvalue.getAvailableBalance());
		}
            }

    	    //get creditLimit, spendLimit, and transactionId
		String limitType = "";
		MemberLimitRule limitRule = null;
		if (memberType.equals(MemberType.CPM.getType()) || memberType.equals(MemberType.IPM.getType()))
		{
		    	memberCashvalueDto.setSpendingLimit(null);
			limitType = LimitType.CR.getName();
			limitRule = limitRuleDao.getEffectiveMemberLimitRule(Long.parseLong(customerId), limitType);
			if (limitRule != null) memberCashvalueDto.setCreditLimit(limitRule.getNumValue());
		}
		else
		{
			Member primaryMember = memberDao.getMemberByCustomerId(member.getSuperiorMemberId());
			MemberLimitRule memberLimitRule = null;
			if (primaryMember != null) {
			    memberLimitRule = limitRuleDao.getEffectiveMemberLimitRule(primaryMember.getCustomerId(), LimitType.CR.getName());
			}
			MemberLimitRule dependentMemberLimitRule = limitRuleDao.getEffectiveMemberLimitRule(member.getCustomerId(), LimitType.TRN.getName());
			if (memberLimitRule != null) memberCashvalueDto.setCreditLimit(memberLimitRule.getNumValue());
			if (dependentMemberLimitRule != null) memberCashvalueDto.setSpendingLimit(dependentMemberLimitRule.getNumValue());
		}
		return memberCashvalueDto;
	}
	
//	private List<CustomerEmailContent> notifyCoachAndMember(String courseId, String oldCoachId) {
//	    
//	    List<CustomerEmailContent> retMap = new ArrayList<CustomerEmailContent>();
//	    List<MemberInfoDto> customers = null;
//	    StaffProfile staffProfile = null; 
//	    Date now = new Date();
//	    
//	    //save members which will be send email to
//	    if (StringUtils.isNotEmpty(courseId)) {
//		customers = courseEnrollmentDao.getMemberInfo(null,courseId, null, null);
//		if (customers != null && customers.size() > 0) {
//		    for (MemberInfoDto customer : customers) {
//			
//			CustomerEmailContent cec = new CustomerEmailContent();
//			cec.setRecipientCustomerId(String.valueOf(customer.getCustomerId()));
//			cec.setNoticeType("N");
//			cec.setSendDate(now);
//			cec.setRecipientEmail(customer.getEmailAddress());
//			
//			MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("csnc");
//			if (mt != null) {
//			    cec.setSubject(mt.getMessageSubject());
//			    String templateContent = mt.getContent();
//			    String content = templateContent.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, customer.getMemberName())
//				    .replace(Constant.PLACE_HOLDER_FROM_USER, Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER);
//			    cec.setContent(content);
//			}
//			String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
//			cec.setSendId(sendId);
//			retMap.add(cec);
//		    }
//		}
//	    }
//	    
//	    //save coach which will be send email to
//	    if (StringUtils.isNotEmpty(oldCoachId)) {
//		staffProfile = staffProfileDao.getStaffProfileByUserId(oldCoachId);
//		if (staffProfile != null) {
//		    
//			CustomerEmailContent cec = new CustomerEmailContent();
//			cec.setRecipientCustomerId(staffProfile.getUserId());
//			cec.setNoticeType("N");
//			cec.setSendDate(now);
//			cec.setRecipientEmail(staffProfile.getContactEmail());
//			
//			MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("csnt");
//			if (mt != null) {
//			    cec.setSubject(mt.getMessageSubject());
//			    String templateContent = mt.getContent();
//			    String content = templateContent.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, staffProfile.getGivenName() + " " + staffProfile.getSurname())
//				    .replace(Constant.PLACE_HOLDER_FROM_USER, Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER);
//			    cec.setContent(content);
//			}
//			
//			String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
//			cec.setSendId(sendId);
//			retMap.add(cec);
//		}
//	    }
//	    
//	    return retMap;
//	}
	
	@Transactional
	public List<Map<String, Object>> sendReminderMsg(Date begin, Date end) {
		List<CourseMaster> courses = courseDao.getTomorrowCourses(begin, end);
		List<Map<String, Object>> senderInfos = new ArrayList<Map<String, Object>>();
		for(CourseMaster course:courses){
		    
		    Map<String, Object> senderInfo = notifyCourseMembers(SceneType.COURSE_REMIND,course);
		    if (senderInfo != null) senderInfos.add(senderInfo);
		}
		return senderInfos;
	}
	
	public Map<String, Object> notifyCourseMembers(SceneType sceneType, CourseMaster course) {

		if(logger.isDebugEnabled()){
			logger.debug("notifyCourseMembers() start.");
		}
		if (course == null) return null;
		List<CourseEnrollment> customers = courseEnrollmentDao.getCourseEnrollmentByCourseId(course.getCourseId());
		String message = getSmsMessage(sceneType, course, null);

		if (StringUtils.isEmpty(message) || customers == null || customers.size() == 0) return null;
		
		List<String> phoneNumbers = new ArrayList<String>();
		for (CourseEnrollment enroll : customers) {
		    	
		    	if (!EnrollCourseStatus.REG.getDesc().equalsIgnoreCase(enroll.getStatus())) continue;
			CustomerProfile cp = customerProfileDao.getById(enroll.getCustomerId().longValue());
			if (cp != null && !StringUtils.isEmpty(cp.getPhoneMobile()))
				phoneNumbers.add(cp.getPhoneMobile());
		}
		
		if(logger.isDebugEnabled()){
			logger.debug("phonenumbers size:" + phoneNumbers.size());
			logger.debug("message:" + message);
		}
		if (phoneNumbers.size() == 0) return null; 

		Map<String, Object> senderInfo = new HashMap<String, Object>();
		senderInfo.put("phonenumbers", phoneNumbers);
		senderInfo.put("message", message);

		return senderInfo;
	}
	
	public Map<String, Object> notifyEnrollMember(SceneType sceneType, CourseEnrollment ce, CourseMaster course) {

		if (course == null || ce == null || ce.getCustomerId() == null) return null;

		CustomerProfile customer = customerProfileDao.getById(ce.getCustomerId());
		if (customer == null || StringUtils.isEmpty(customer.getPhoneMobile())) return null;
		
		String message = getSmsMessage(sceneType, course, null);
		if (StringUtils.isEmpty(message)) return null;
		
		List<String> phoneNumbers = new ArrayList<String>();
		phoneNumbers.add(customer.getPhoneMobile());

		Map<String, Object> senderInfo = new HashMap<String, Object>();
		senderInfo.put("phonenumbers", phoneNumbers);
		senderInfo.put("message", message);

		return senderInfo;
	}
	
	/**
	 * Get message to send
	 * @Author Mianping_Wu
	 * @Date Sep 23, 2015
	 * @Param @param sceneType
	 * @Param @param courseType
	 * @Param @return
	 * @return String
	 */
	private String getSmsMessage(SceneType sceneType, CourseMaster course, CourseSession session) {
		
		if(logger.isDebugEnabled())
		{
			logger.debug("getSmsMessage:" + " sceneType:" + sceneType);
		}
		if (sceneType == null || course == null) return null;
		
		String courseType = course.getCourseType();
		if (StringUtils.isEmpty(courseType)) return null;
		
		String functionId = null;
		
		if (SceneType.COURSE_CANCEL == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_cancel";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_cancel";
			
		} else if (SceneType.COURSE_CANCEL_APP == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "app_golf_course_cancel";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "app_tennis_course_cancel";
			
		}else if (SceneType.COURSE_CREATE == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_create";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_create";
			
		} else if (SceneType.COURSE_REMIND == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_remind";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_remind";
			
		} else if (SceneType.COURSE_CHANGE == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_change";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_change";
			
		} else if (SceneType.COURSE_ENROLL_MANUAL_RECEIVE == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_m_enroll_receive";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_m_enroll_receive";
			
		} else if (SceneType.COURSE_ENROLL_MANUAL_ACCEPT == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_m_enroll_accept";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_m_enroll_accept";
			
		} else if (SceneType.COURSE_ENROLL_MANUAL_CONFIRM == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_m_enroll_confim";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_m_enroll_confirm";
			
		} else if (SceneType.COURSE_ENROLL_AUTO_CONFIRM == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_a_enroll_confirm";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_a_enroll_confirm";
			
		} else if (SceneType.COURSE_ENROLL_REJECT == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_enroll_reject";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_enroll_reject";
			
		} else if (SceneType.COURSE_ENROLL_CANCEL == sceneType) {
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "golf_course_enroll_cancel";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "tennis_course_enroll_cancel";
			
		}else if(SceneType.COURSE_REMIND_COACH_UPDATE == sceneType){  //added by vicky wang to add the reminder message for course session change.
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "app_golf_course_change_oldcoach";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "app_tennis_course_change_oldcoach";
			
		}else if(SceneType.COURSE_REMIND_COACH_MOVE == sceneType){
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "app_golf_course_change_oldcoach";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "app_tennis_course_change_oldcoach";
			
		}else if(SceneType.COURSE_REMIND_COACH_JOIN == sceneType){
			
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "app_golf_course_change_newcoach";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "app_tennis_course_change_newcoach";			
		}else if(SceneType.COURSE_SESSON_CANCEL == sceneType)
		{
			if (CourseType.GOLFCOURSE.getDesc().equals(courseType)) functionId = "app_golf_course_session_cancel";
			if (CourseType.TENNISCOURSE.getDesc().equals(courseType)) functionId = "app_tennis_course_session_cancel";			
		}
		
		if(logger.isDebugEnabled())
		{
			logger.debug("functionId:" + functionId);
		}
		if (functionId == null) return null;
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(functionId);
		if (mt == null || StringUtils.isEmpty(mt.getContent())) return null;
		
//		String content = mt.getContent();
		String message = null;
		if (SceneType.COURSE_REMIND == sceneType || SceneType.COURSE_REMIND_COACH_UPDATE == sceneType ||SceneType.COURSE_CREATE == sceneType
				|| SceneType.COURSE_REMIND_COACH_MOVE == sceneType ||SceneType.COURSE_REMIND_COACH_JOIN == sceneType || SceneType.COURSE_SESSON_CANCEL == sceneType) {
		    
		    if(session == null && SceneType.COURSE_REMIND == sceneType)
		    {
		    	session = courseSessionDao.getLatestCourseSessionByCourseId(String.valueOf(course.getCourseId()));
		    }
		    String facility = null;
		    String sessionDateTime = null;
		    if (session != null) {
			try {
				String sessionDate = DateCalcUtil.formatDate(session.getBeginDatetime());
				String startTime = DateCalcUtil.getHourAndMinuteOfDay(session.getBeginDatetime());
				String endTime = DateCalcUtil.getHourAndMinuteOfDay(session.getEndDatetime());
				sessionDateTime = sessionDate + " " + startTime + "-" + endTime;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CourseSessionSmsParamDto dto = courseSessionDao.getCourseSessionReminderInfo(session.getSysId());
			if (dto != null) facility = dto.getFacility();
//			List<CourseSessionFacility> sessionFacilities = courseSessionFacilityDao.selectCourseSessionFacilityBySessionId(session.getSysId());
//			if (sessionFacilities != null && sessionFacilities.size() > 0) {
//			    CourseSessionFacility sessionFacility = sessionFacilities.get(0);
//			    facilityNO = "#" + sessionFacility.getFacilityNo();
//			}
		    }
//		    message = content.replace("{courseName}", course.getCourseName())
//			    .replace("{courseSessionDate}", sessionBeginDatetime)
//			    .replace("{sessionTime}", "")
//			    .replace("{facility}", facilityNO)0;
		    message = mt.getFullContent(course.getCourseName(), sessionDateTime, "", facility);
		    
		} else {
//		    message = content.replace("{courseName}", course.getCourseName());
		    message = mt.getFullContent(course.getCourseName());
		}
			
		if(logger.isDebugEnabled())
		{
			logger.debug("message:" + message);
		}
		return message;
	}

	@Override
	@Transactional
	public ResponseResult changeCourseStatus(CourseInfoDto dto, String userId) throws Exception{
		if (StringUtils.isEmpty(dto.getCourseId())) {
			responseResult.initResult(GTAError.CourseError.COURSE_ID_ISNULL);
			return responseResult;
		}
		
		if(StringUtils.isEmpty(dto.getStatus())){ 
			responseResult.initResult(GTAError.CourseError.COURSE_STATUS_ISNULL);
			return responseResult;
		}
		
		if (!(dto.getStatus().toUpperCase().equals("OPN") || dto.getStatus().toUpperCase().equals("CAN") || dto.getStatus().toUpperCase().equals("CLD") || dto.getStatus().toUpperCase().equals("FUL"))){
			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_STATUS_ERROR);
			return responseResult;
		}
		CourseMaster courseMaster = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", Long.valueOf(dto.getCourseId()));
		
		if(null == courseMaster){
			responseResult .initResult(GTAError.CourseError.COURSE_DOESNT_EXIST);
			return responseResult;
		}
		if("CAN".equals(courseMaster.getStatus().toUpperCase())){
			responseResult .initResult(GTAError.CourseError.COURSE_IS_CANCELED);
			return responseResult;
		}
		if("CAN".equals(dto.getStatus().toUpperCase())){
			courseMaster.setCancelRequesterType(dto.getCancelRequesterType());
			courseMaster.setInternalRemark(dto.getInternalRemark());
		}
		
		courseMaster.setStatus(dto.getStatus());
		if ("OPN".equals(dto.getStatus().toUpperCase())) {
		    int capacity = courseEnrollmentDao.countByCourseId(courseMaster.getCourseId());
		    if (capacity >= courseMaster.getCapacity()) courseMaster.setStatus("FUL");
		}

		courseMaster.setUpdateBy(userId);
		courseMaster.setUpdateDate(new Date());
		courseMasterDao.update(courseMaster);
		
		//get sms info to send
		Map<String, Object> smsInfo = null;
			
		Set<String> coachList = new HashSet<String>();
		
		if("CAN".equals(courseMaster.getStatus())){
			List<CourseSession> courseSessionList = courseSessionDao.selectCourseSessionByCourseId(Integer.parseInt(dto.getCourseId()));
			if(null != courseSessionList && courseSessionList.size() > 0){
				
				for(CourseSession courseSession : courseSessionList){
					if(null != courseSession){
						// release facility time check-in, delete old facilities for current
						// course session
						List<CourseSessionFacility> courseSessionFacilities = courseSessionFacilityDao
								.selectCourseSessionFacilityBySessionId(courseSession.getSysId());
						for (CourseSessionFacility csf : courseSessionFacilities) {

							FacilityTimeslot facilityTimeslot = csf.getFacilityTimeslot();
							facilityTimeslot.getFacilityMaster().getFacilityTimeslots()
									.remove(facilityTimeslot);
							facilityTimeslot.setFacilityMaster(null);
							facilityTimeslot.setMemberFacilityTypeBooking(null);
							facilityTimeslot.setStaffFacilitySchedule(null);
							long facilityTimeslotId = facilityTimeslot.getFacilityTimeslotId();
							courseSessionFacilityDao.deleteCourseSessionFacility(csf);
							facilityTimeslotDao.deleteFacilityTimeslotById(facilityTimeslotId);
						}
						
						courseSession.setStatus("CAN");
						courseSession.setUpdateBy(dto.getUpdateBy());
						courseSession.setUpdateDate(new Date());
						courseSessionDao.update(courseSession);
						
						String coachId = courseSession.getCoachUserId();
						coachList.add(coachId);
						if(logger.isDebugEnabled())
						{
							logger.debug("coachUserId:" + coachId);
						}
					}

					if (null != courseSession.getCoachTimeslotId()) {
						staffTimeslotDao.deleteById(StaffTimeslot.class, courseSession.getCoachTimeslotId().toString());
					}
				}
				
			}			
			smsInfo = notifyCourseMembers(SceneType.COURSE_CANCEL, courseMaster);	
			
			if(smsInfo==null)
				smsInfo = new HashMap<String, Object>();
			String messageForCoach = getSmsMessage(SceneType.COURSE_CANCEL_APP, courseMaster, null);
			smsInfo.put("coachList", coachList);
			smsInfo.put("coachMessage", messageForCoach);
			
//			smsInfo = notifyCourseCoach(smsInfo, courseMaster.ge);
			List<CourseEnrollment> courseEnrollmentList = courseEnrollmentDao.getCourseEnrollmentByCourseId(Long.parseLong(dto.getCourseId()));
			
			if(null != courseEnrollmentList && courseEnrollmentList.size() > 0){
				
				for(CourseEnrollment c : courseEnrollmentList){
					
					if("REG".equals(c.getStatus())){
						processRefundRequest(dto, userId, c, courseMaster);
					}
						
					c.setCancelRequesterType(dto.getCancelRequesterType());
					c.setInternalRemark(dto.getInternalRemark());
					c.setStatus("CAN");
					c.setUpdateBy(userId);
					c.setStatusUpdateBy(userId);
					c.setUpdateDate(new Date());
					c.setStatusUpdateDate(new Date());
					courseEnrollmentDao.updateCourseEnrollment(c);

				}
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS, smsInfo);
		return responseResult;
	}
	

	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 19, 2015
	 * @Param @param dto
	 * @Param @param userId
	 * @Param @param ce
	 * @Param @param cm
	 * @return void
	 */
	public void processRefundRequest(CourseInfoDto dto, String userId, CourseEnrollment ce, CourseMaster cm) throws Exception {
		
		
		if ("HKGTA".equalsIgnoreCase(dto.getCancelRequesterType())) {
			
			generateRefundRequester(dto, userId, ce, cm, "PND");
			
		} else if ("CUSTOMER".equalsIgnoreCase(dto.getCancelRequesterType())) {
			
			boolean inRefundPeriod = false;
			Date courseStartDate = courseMasterDao.getCourseSessionStartDate(cm.getCourseId());
			if(courseStartDate!=null){
				Timestamp startTime = new Timestamp(courseStartDate.getTime());
				inRefundPeriod = memberFacilityTypeBookingService.checkRefundDate(startTime, cm.getCourseType(), "course");
			}
			
			if (inRefundPeriod) {
				
				generateRefundRequester(dto, userId, ce, cm, "PND");
				
			} else if (!inRefundPeriod && dto.isRefundFlag()) {
				
				generateRefundRequester(dto, userId, ce, cm, "NRF");
			}
			
		}

	}
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 19, 2015
	 * @Param @param dto
	 * @Param @param userId
	 * @Param @param ce
	 * @Param @param cm
	 * @Param @param refundStatus
	 * @return void
	 */
	private void generateRefundRequester(CourseInfoDto dto, String userId, CourseEnrollment ce, CourseMaster cm, String refundStatus) {
		
		CustomerRefundRequest customerRefundRequest = new CustomerRefundRequest();
		customerRefundRequest.setRequesterType(dto.getCancelRequesterType());
		customerRefundRequest.setStatus(refundStatus);
		customerRefundRequest.setCustomerReason(dto.getInternalRemark());
		customerRefundRequest.setCreateBy(userId);
		customerRefundRequest.setCreateDate(new Date());
		customerRefundRequest.setRefundServiceType(cm.getCourseType());
		customerRefundRequest.setOrderDetailId(ce.getCustOrderDetId());
		customerRefundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
		customerRefundRequest.setUpdateBy(userId);
		customerRefundRequest.setUpdateDate(new Date());
		CustomerOrderHd order = customerOrderHdDao.getUniqueByCol( CustomerOrderHd.class, "orderNo", ce.getCustOrderDetId());
		List<CustomerOrderTrans> txns = order.getCustomerOrderTrans();
		if (txns.size() > 0) {
			customerRefundRequest.setRefundTransactionNo(txns.get(0).getTransactionNo());
			customerRefundRequest.setRequestAmount(txns.get(0).getPaidAmount());
		}
		customerRefundRequestDao.save(customerRefundRequest);
	}
	
	/**
	 * @discription This method is used to change course status to close when register due date come
	 * @author Mianping_Wu
	 * @date 7/14/2015
	 */
	@Transactional
	public void closeCourseOnDueDate() throws Exception {
	    
	    Date now = new Date();
	    Date invokeDate = DateCalcUtil.getNearDay(now, -1);
	    String curDate = DateCalcUtil.formatDate(invokeDate);
	    String hql = "from CourseMaster where status in ('OPN', 'FUL') and registDueDate <= '" + curDate + "'";
	    List<CourseMaster> courses = courseDao.getByHql(hql);
	    if (courses != null && courses.size() > 0) {
		
		for (CourseMaster course : courses) {
		    
		    course.setStatus("CLD");
		    course.setUpdateBy("Admin");
		    course.setUpdateDate(now);
		    courseDao.updateCourseMaster(course);
		}
	    }
	    
	}
	

	/**
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult changeMemberAttendanceStatus(String sessionId,
			String enrollId, String attendId, String status, String userId) {

		StudentCourseAttendance attendanceRecord = null;
		if (StringUtils.isEmpty(attendId)) {
			attendanceRecord = new StudentCourseAttendance();
			attendanceRecord.setCourseSessionId(Long.parseLong(sessionId));
			attendanceRecord.setEnrollId(new Long(enrollId));
			attendanceRecord.setCreateBy(userId);
			attendanceRecord.setCreateDate(new Timestamp(new Date().getTime()));
			attendanceRecord.setUpdateBy(userId);
			attendanceRecord.setUpdateDate(new Date());
			attendanceRecord.setStatus(status);
			Long pk = (Long) studentCourseAttendanceDao.save(attendanceRecord);
			attendanceRecord.setAttendId(pk);

		} else {
		    
			attendanceRecord = studentCourseAttendanceDao.getUniqueByCol(StudentCourseAttendance.class, "attendId", Long.valueOf(attendId));
			attendanceRecord.setStatus(status);
			attendanceRecord.setUpdateBy(userId);
			attendanceRecord.setUpdateDate(new Date());
		}
		
		StudentCourseAttendanceDto result = getReturnData(attendanceRecord);
		if (result == null) throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;

	}
	
	private StudentCourseAttendanceDto getReturnData(StudentCourseAttendance attendance) {
	    
	    if (attendance == null || attendance.getEnrollId() == null) return null;
	    
	    StudentCourseAttendanceDto dto = courseEnrollmentDao.getMemberInfoByEnrollId(String.valueOf(attendance.getEnrollId()));
	    if (dto == null) return null; 
	    dto.setAttendId(attendance.getAttendId());
	    dto.setCourseSessionId(attendance.getCourseSessionId());
	    dto.setEnrollId(attendance.getEnrollId());
	    dto.setStatus(attendance.getStatus());
	    dto.setCreateBy(attendance.getCreateBy());
	    dto.setCreateDate(attendance.getCreateDate());
	    dto.setUpdateBy(attendance.getUpdateBy());
	    dto.setUpdateDate(attendance.getUpdateDate());
	    
	    return dto;
	}


	/**
	 * 输入参数有效性判断 validate the input parameter
	 * @param customerId
	 * @param courseId
	 * @param isPay
	 * @return
	 * @author Miranda_Zhang
	 * @date Aug 7, 2015
	 */
	private boolean checkInputParemter(Long customerId, CourseMaster course, Boolean isPay) throws Exception {
		if (customerId == null || course == null || isPay == null) {
		    responseResult.initResult(GTAError.CourseError.BAD_REQUEST_PARAMETER);
		    return false;
		}
		
		CustomerProfile cp = customerProfileDao.getById(customerId);
		if (cp == null) {		    
		    responseResult.initResult(GTAError.CourseError.NO_CUSTOMER_EXISTS, new String[]{ String.valueOf(customerId)} );
		    return false;
		}
		
		boolean isActiveMember = memberDao.isMemberActive(customerId);
		if(!isActiveMember) {
		    responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_NACT_MEMBER);
		    return false;
		}
		
		boolean isValid = validateAgeRange(cp, course);
		if (!isValid) {		    
		    responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_BAD_AGE_RANGE);
		    return false;
		}
		
		//check if current course has available session
		ContractHelper obj = courseSessionDao.getSessionPeriodOfCourse(String.valueOf(course.getCourseId()));
		if (obj == null || obj.getPeriodFrom() == null || obj.getPeriodTo() == null) {
		    responseResult.initResult(GTAError.CourseError.COURSE_NO_SESSION_AVAILABLE);
		    return false;
		}
		
		//check if service account expired
		boolean yes = customerServiceAccService.isCustomerServiceAccValid(customerId, obj);
		if (!yes) {
		    responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_EXPIRED_SERVICE_ACC);
		    return false;
		}
		
		return true;
	}
	
	
	/**
	 * @Author Mianping_Wu
	 * @Date Sep 9, 2015
	 * @Param @param cp
	 * @Param @param course
	 * @Param @return
	 * @return boolean
	 */
	private boolean validateAgeRange(CustomerProfile cp, CourseMaster course) throws Exception {
		
		String ageRange = course.getAgeRangeCode();
		String birthDate = cp.getDateOfBirth();
		if (StringUtils.isEmpty(birthDate) || StringUtils.isEmpty(ageRange) || AgeRangeCode.ALL.getDesc().equalsIgnoreCase(ageRange)) {
			return true;
		}
		
		int age = DateCalcUtil.GetDateDifferenceByType(DateCalcUtil.parseDate(birthDate), new Date(), Calendar.YEAR);
		AgeRangeCode ageRangeCode = CommUtil.getAgeRange(age);
		if (ageRangeCode != null && ageRange.equalsIgnoreCase(ageRangeCode.getDesc())) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * member是否能注册本课程 check whether member can enroll this course
	 * @param courseId
	 * @param customerId
	 * @return
	 * @author Miranda_Zhang
	 * @date Aug 7, 2015
	 */
	private boolean checkMemberCanEnrollCourse(Long courseId,Long customerId,String courseType) {
		
		String hasRight = courseEnrollmentDao.checkCustomerServicePlanRight(customerId, courseType);
		if (!"true".equals(hasRight)) {
			String retType = null;
			if ("GSS".equals(courseType)) {
				retType = "golf course right";
			} else if ("TSS".equals(courseType)) {
				retType = "tennis course right";
			}
		    
		    responseResult.initResult(GTAError.CourseError.NO_COURSE_RIGHT_EXISTS, new String[]{ retType } );
		    return false;
		}
		
		return true;
	}
	
	/**
	 * 检查course是否可以注册
	 * @param course
	 * @param customerId
	 * @return
	 * @author Miranda_Zhang
	 * @date Aug 7, 2015
	 */
	private boolean isValidCourse (CourseMaster course,Long customerId) {
		Date curDate = new Date();
		//注册时间不对不能注册 check enroll is during the register period 
		if(curDate.before(course.getRegistBeginDate()) || curDate.after(DateCalcUtil.getNearDay(course.getRegistDueDate(), 1))){
			responseResult .initResult(GTAError.CourseError.ENROLL_COURSE_DATE_END);
			return false;
		}
		
		//状态不为open不能注册 check the course status is open
		if (!CourseStatus.OPEN.getType().equals(course.getStatus())) {
			responseResult.initResult(GTAError.CourseError.NOT_OPENED_COURSE);
			return false;
		}
		
		//判断是否重复注册 check has already enrolled 
	    CourseEnrollment ce = courseEnrollmentDao.getCourseEnrollmentByCourseIdAndCustomerId(course.getCourseId(), customerId);
		if (ce != null) {
			responseResult.initResult(GTAError.CourseError.COURSE_ENROLL_REPEAT);
			return false;
		}
		
		return true;
	}
	
	/**
	 * course 支付
	 * @param customerId
	 * @param itemNo
	 * @param paymentMethod
	 * @param createBy
	 * @param device
	 * @return
	 * @throws Exception
	 * @author Miranda_Zhang
	 * @date Aug 7, 2015
	 */
	private CustomerOrderTransDto coursePayment(Long customerId,String itemNo,String paymentMethod,String createBy,String device, String paymentLocation) throws Exception {
	    
		CustomerOrderTransDto orderTransDto = null;
		if (Constant.CASH_Value.equals(paymentMethod)) {
		    orderTransDto = payByCashValue(customerId, itemNo, paymentMethod, createBy, paymentLocation);
		} else if (Constant.CREDIT_CARD.equals(paymentMethod)) {
		    orderTransDto = payByCreditCard(customerId, itemNo, paymentMethod, createBy, device, paymentLocation);
		} else if (Constant.CASH.equals(paymentMethod)) {
		    orderTransDto = payByCash(customerId, itemNo, paymentMethod, createBy, paymentLocation);
		}
		
		if (orderTransDto == null) {
		    throw new RuntimeException("Error in generate orderNo!");
		}
		
		return orderTransDto;
	}
	
	@Override
	@Transactional
	public ResponseResult enrollCourseFinal(CourseEnrollmentDto courseEnrollmentDto, String createBy) throws Exception {
	    
	    Long customerId = courseEnrollmentDto.getCustomerId();
	    Long courseId = courseEnrollmentDto.getCourseId();
		String paymentMethod = courseEnrollmentDto.getPaymentMethod();
		String device = courseEnrollmentDto.getDevice();
		String remark = courseEnrollmentDto.getRemark();		
		Boolean isPay= courseEnrollmentDto.getIsPay();
		
		if(StringUtils.isEmpty(paymentMethod)) {
			paymentMethod = Constant.CASH_Value;
		}
		
		CourseMaster course = courseMasterDao.get(CourseMaster.class, courseId);
		if (course == null) {
		    responseResult.initResult(GTAError.CourseError.NO_COURSE_EXISTS, new String[]{ String.valueOf(courseId)} );
		    return responseResult;
		}
		
		String memberAcceptance = course.getMemberAcceptance();
		Long capacity = course.getCapacity();
		String itemNo = course.getPosItemNo();
		String courseType = course.getCourseType();
		
		Date date = new Date();
		
		// 1.输入参数有效性判断 validate the input parameter
		if(!checkInputParemter(customerId, course, isPay)){
			return responseResult;
		}
		
		// 2.member是否能注册本课程 check whether member can enroll this course
		if(!checkMemberCanEnrollCourse(courseId,customerId,courseType)){
			return responseResult;
		}
			
		try {
		    
			//Auto: check parameter for generate course_enrollment
	    	//state1: Manu && false
	    	//state2: Manu && true
	    	//state3: Auto && true
			if (MemberAcceptance.AUTO.getType().equals(memberAcceptance) && isPay) {
			    //3.查看course 是否可以注册
				if(!isValidCourse(course,customerId)){
	    			return responseResult;
	    		}
				
				//3.1 如果满员了改变course status change to full when the reach the capacity 
				int currentEnrollMembers = courseEnrollmentDao.countByCourseId(courseId);
				if(capacity <= (currentEnrollMembers + 1)){
					course.setStatus(CourseStatus.FULL.getType());
					course.setUpdateBy(createBy);
					course.setUpdateDate(new Date());
					courseDao.updateCourseMaster(course);
				}
				
				//4.支付
				CourseEnrollment courseEnrollment = new CourseEnrollment();
				CustomerOrderTransDto orderTransDto = coursePayment(customerId, itemNo, paymentMethod, createBy,device, courseEnrollmentDto.getPaymentLocation());
				courseEnrollment.setCustOrderDetId(orderTransDto.getOrderNo());
				
				//5.插入enroll记录
				if (!Constant.CREDIT_CARD.equals(paymentMethod)){ 
					courseEnrollment.setStatus(EnrollCourseStatus.REG.getDesc());
				} else {
				    	courseEnrollment.setStatus(EnrollCourseStatus.ACT.getDesc());
				}
				
				courseEnrollment.setCourseId(courseId);
				courseEnrollment.setCustomerId(customerId);
				courseEnrollment.setEnrollDate(date);		
				courseEnrollment.setInternalRemark(remark);
				courseEnrollment.setCreateBy(createBy);
				courseEnrollment.setCreateDate(new Timestamp(System.currentTimeMillis()));
    			String enrollId = (String)courseEnrollmentDao.save(courseEnrollment);
    			courseEnrollment.setEnrollId(enrollId);
    			
    			if(!Constant.CREDIT_CARD.equals(paymentMethod)){
					sendCourseEmailAfterPayment(orderTransDto.getOrderNo());
				}
				
    			//get sms info
				Map<String, Object> smsInfo = null;
				if (!Constant.CREDIT_CARD.equals(paymentMethod)){ 
					smsInfo = notifyEnrollMember(SceneType.COURSE_ENROLL_AUTO_CONFIRM, courseEnrollment, course);
				} else {
					smsInfo = notifyEnrollMember(SceneType.COURSE_ENROLL_MANUAL_RECEIVE, courseEnrollment, course);
				}
    				
    			Map<String, Object> map = new HashMap<String, Object>();
    			map.put("enrollMent", courseEnrollment);
    			map.put("orderTrans", orderTransDto);
    			map.put("smsInfo", smsInfo);
				
				responseResult.initResult(GTAError.Success.SUCCESS, map);
				
			} else if (MemberAcceptance.MANUAL.getType().equals(memberAcceptance)) {
			    
		    	CourseEnrollment courseEnrollment = courseEnrollmentDao.getCourseEnrollmentByCourseIdAndCustomerId(courseId, customerId);
		    	//1) 如果是支付
		    	if (isPay) {
		    	    
			    	if (courseEnrollment == null) {
			    		responseResult.initResult(GTAError.CourseError.NO_COURSE_ENROLLMENT_EXISTS, new String[]{ String.valueOf(courseId), String.valueOf(customerId)} );
    					return responseResult;
			    	}
    			    	
    				if(EnrollCourseStatus.REG.getDesc().equals(courseEnrollment.getStatus())) {
    				    
						responseResult.initResult(GTAError.CourseError.PYAMENT_REPEAT);
						return responseResult;
    				}
    				
    				CustomerOrderTransDto orderTransDto = coursePayment(customerId, itemNo, paymentMethod, createBy, device, courseEnrollmentDto.getPaymentLocation());
    				
    				Map<String, Object> smsInfo = null;
    				if (!Constant.CREDIT_CARD.equals(paymentMethod)){ 
    					
    					courseEnrollment.setStatus(EnrollCourseStatus.REG.toString());
    					courseEnrollment.setStatusUpdateBy(createBy);
    					courseEnrollment.setStatusUpdateDate(date);
    					smsInfo = notifyEnrollMember(SceneType.COURSE_ENROLL_MANUAL_CONFIRM, courseEnrollment, course);
    					
    				} 
    				
    				courseEnrollment.setCustOrderDetId(orderTransDto.getOrderNo());
    				courseEnrollment.setUpdateBy(createBy);
    				courseEnrollment.setUpdateDate(date);
    				courseEnrollmentDao.updateCourseEnrollment(courseEnrollment);
    				if(!Constant.CREDIT_CARD.equals(paymentMethod)){
    					sendCourseEmailAfterPayment(orderTransDto.getOrderNo());
    				}
    				
    				Map<String, Object> map = new HashMap<String, Object>();
    				map.put("enrollMent", courseEnrollment);
    				map.put("orderTrans", orderTransDto);
    				map.put("smsInfo", smsInfo);
    				responseResult.initResult(GTAError.Success.SUCCESS, map);
		    	    
		    	} else {
		    	    //2) 查看当前course是否能注册
		    		if(!isValidCourse(course,customerId)){
		    			return responseResult;
		    		}
		    		courseEnrollment = new CourseEnrollment();
			    	courseEnrollment.setCourseId(courseId);
			    	courseEnrollment.setStatus(EnrollCourseStatus.PND.toString());
        			courseEnrollment.setCustomerId(customerId);
        			courseEnrollment.setEnrollDate(date);		
        			courseEnrollment.setInternalRemark(remark);
        			courseEnrollment.setCreateBy(createBy);
        			courseEnrollment.setCreateDate(new Timestamp(System.currentTimeMillis()));
    				String enrollId = (String)courseEnrollmentDao.save(courseEnrollment);
    				courseEnrollment.setEnrollId(enrollId);
    				
    				Map<String, Object> smsInfo = notifyEnrollMember(SceneType.COURSE_ENROLL_MANUAL_RECEIVE, courseEnrollment, course);
    				
    				Map<String, Object> map = new HashMap<String, Object>();
    				map.put("enrollMent", courseEnrollment);  
    				map.put("smsInfo", smsInfo);
    				responseResult.initResult(GTAError.Success.SUCCESS, map);
		    	}
		    
			}

			return responseResult;
			
		} catch (GTACommonException e) {
		    
		    e.printStackTrace();
		    throw e;
		    
		} catch (Exception e) {
		    
		    e.printStackTrace();
		    throw new RuntimeException(e);
		}
	}
	
	/**
	 * Used to send the course enrollment email after payment.
	 * (This method is using the thread pool)
	 * @author Liky_Pan
	 * @param orderNo Order No. of customer order
	 */
	public void sendCourseEmailAfterPayment(Long orderNo){
	    
	    	try {
		    
			CustomerEmailContent customerEmailContent = courseEnrollmentService.getCourseEnrollEmailContent(orderNo);
			if (customerEmailContent != null) {
				List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(orderNo);
				CustomerOrderTrans customerOrderTrans = customerOrderTranses.get(0);//Course Payment Order and transaction is one to one.
				byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, customerOrderTrans.getTransactionNo().toString(), "course");
				List<byte[]> attachmentList = Arrays.asList(attachment);
				List<String> mineTypeList = Arrays.asList("application/pdf");
				List<String> fileNameList = Arrays.asList("CourseEnrollmentReceipt-"+customerOrderTrans.getTransactionNo().toString()+".pdf");
				mailThreadService.sendWithResponse(customerEmailContent, attachmentList, mineTypeList, fileNameList);
			}
			
		} catch (Exception e) {
		    
		    e.printStackTrace();
		}

	}
	/**
	 * 
	 * @param customerId
	 * @param itemNo
	 * @param paymentMethod
	 * @param createBy
	 * @return
	 * @author Miranda_Zhang
	 */
	private CustomerOrderTransDto payByCashValue(Long customerId,String itemNo,String paymentMethod,String createBy, String paymentLocation) throws Exception {
		MemberCashValuePaymentDto paymentDto = new MemberCashValuePaymentDto();
		paymentDto.setCustomerId(customerId);
		paymentDto.setItemNos(new String[]{itemNo});
		Integer amount = 1;
		paymentDto.setOrderQty(amount);
		paymentDto.setPaymentMethod(paymentMethod);
		PosServiceItemPrice posItem = posServiceItemPriceDao.getByItemNo(itemNo);
		BigDecimal price = posItem.getItemPrice();
		paymentDto.setTotalAmount(price);
		paymentDto.setUserId(createBy);
		paymentDto.setLocation(paymentLocation);
		Map<String,Integer> itemNoMap = new HashMap<String,Integer>();
		itemNoMap.put(posItem.getItemNo(), amount);
		paymentDto.setItemNoMap(itemNoMap);
		CustomerOrderTransDto orderTrans = memberCashvalueService.paymentByMemberCashvalue(paymentDto);
		return orderTrans;

	}
	
	
	private CustomerOrderTransDto payByCreditCard(Long customerId, String itemNo, String paymentMethod, String createBy, String device, String paymentLocation) throws Exception {
	    
		MemberCashValuePaymentDto paymentDto = new MemberCashValuePaymentDto();
		paymentDto.setCustomerId(customerId);
		paymentDto.setItemNos(new String[]{itemNo});
		Integer amount = 1;
		paymentDto.setOrderQty(amount);
		paymentDto.setPaymentMethod(paymentMethod);
		PosServiceItemPrice posItem = posServiceItemPriceDao.getByItemNo(itemNo);
		BigDecimal price = posItem.getItemPrice();
		paymentDto.setTotalAmount(price);
		paymentDto.setUserId(createBy);
		paymentDto.setLocation(paymentLocation);
		CustomerOrderTransDto orderTrans = creditCardHandler(paymentDto, device);
		return orderTrans;

	}
	
	private CustomerOrderTransDto payByCash(Long customerId, String itemNo, String paymentMethod, String createBy, String paymentLocation) throws Exception {
	    
		MemberCashValuePaymentDto paymentDto = new MemberCashValuePaymentDto();
		paymentDto.setCustomerId(customerId);
		paymentDto.setItemNos(new String[]{itemNo});
		Integer amount = 1;
		paymentDto.setOrderQty(amount);
		paymentDto.setPaymentMethod(paymentMethod);
		PosServiceItemPrice posItem = posServiceItemPriceDao.getByItemNo(itemNo);
		BigDecimal price = posItem.getItemPrice();
		paymentDto.setTotalAmount(price);
		paymentDto.setUserId(createBy);
		paymentDto.setLocation(paymentLocation);
		CustomerOrderTransDto orderTrans = cashHandler(paymentDto);
		return orderTrans;

	}
	
	
	@Override
	@Transactional
	public String getCommonQueryByCourseType(String courseType,
			String dateRange, String expired, String customerId) {
		return courseMasterDao.getCommonQueryByCourseType(courseType, dateRange, expired, customerId);
	}
	
	@Override
	@Transactional
	public String getCommonQueryByCourseTypeAndroid(String courseType, String dateRange, String expired, String customerId) {
		return courseMasterDao.getCommonQueryByCourseTypeAndroid(courseType, dateRange, expired, customerId);
	}
	
	
	private CustomerOrderTransDto creditCardHandler(MemberCashValuePaymentDto memberCashValuePaymentDto, String device)
	{
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date());
		customerOrderHd.setOrderStatus(Constant.Status.OPN.toString());
		customerOrderHd.setStaffUserId(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCustomerId(memberCashValuePaymentDto.getCustomerId());
		customerOrderHd.setOrderTotalAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderHd.setCreateBy(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCreateDate(currentTime);
		customerOrderHdDao.save(customerOrderHd);


		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		customerOrderDet.setCustomerOrderHd(customerOrderHd);
		customerOrderDet.setItemNo(memberCashValuePaymentDto.getItemNos()[0]);
		customerOrderDet.setOrderQty(new Long(memberCashValuePaymentDto.getOrderQty()));
		customerOrderDet.setItemTotalAmout(memberCashValuePaymentDto.getTotalAmount());
		customerOrderDet.setCreateBy(memberCashValuePaymentDto.getUserId());
		customerOrderDet.setCreateDate(currentTime);
		customerOrderDetDao.save(customerOrderDet);


		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
//		customerOrderTrans.setPaymentMethodCode(Constant.CREDIT_CARD);
		customerOrderTrans.setTransactionTimestamp(currentTime);
		customerOrderTrans.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTrans.setStatus(Constant.Status.PND.toString());
		customerOrderTrans.setPaymentRecvBy(memberCashValuePaymentDto.getUserId());
		customerOrderTrans.setPaymentLocationCode(memberCashValuePaymentDto.getLocation());
		String redirectUrl = null;
		if ("WP".equals(device)) {
		    customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		} else {
		    customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
		}
		Long transactionNo = (Long) customerOrderTransDao.save(customerOrderTrans);
		customerOrderTrans.setTransactionNo(transactionNo);
		if ("WP".equals(device)) redirectUrl = paymentGatewayService.payment(customerOrderTrans);

		CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
		customerOrderTransDto.setOrderNo(customerOrderHd.getOrderNo());
		customerOrderTransDto.setTransactionNo(transactionNo);
		customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(currentTime));
		customerOrderTransDto.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTransDto.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
		customerOrderTransDto.setRedirectUrl(redirectUrl);
		customerOrderTransDto.setPosItemNo(memberCashValuePaymentDto.getItemNos()[0]);
		
		return customerOrderTransDto;
	}
	
	
	private List<StaffProfile> removeOnDutyCoach(List<StaffProfile> cpList, String beginDatetime, String endDatetime, String currentCoachId) throws Exception {
	    
	    if (StringUtils.isEmpty(beginDatetime) || StringUtils.isEmpty(endDatetime) || cpList == null || cpList.size() == 0) return cpList;
	    List<StaffCoachInfoDto> offDutyCoaches = staffCoachRoasterDao.getOnDutyCoachByTimeslot(beginDatetime, endDatetime);
	    if (offDutyCoaches == null || offDutyCoaches.size() == 0) return cpList;
	    
	    List<StaffProfile> result = new ArrayList<StaffProfile>();
	    
	    for (StaffProfile sp : cpList) {
		
		if (sp.getUserId().equals(currentCoachId)) {
		    result.add(sp);
		    continue;
		}
		
		boolean exist = false;
		for (StaffCoachInfoDto coach : offDutyCoaches) {
		    if (coach.getUserId().equals(sp.getUserId())) {
			exist = true;
			break;
		    }
		}
		
		if (!exist) result.add(sp);
	    }
	    
	    return result;
	}
	
	private CustomerOrderTransDto cashHandler(MemberCashValuePaymentDto memberCashValuePaymentDto)
	{
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date());
		customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
		customerOrderHd.setStaffUserId(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCustomerId(memberCashValuePaymentDto.getCustomerId());
		customerOrderHd.setOrderTotalAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderHd.setCreateBy(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCreateDate(currentTime);
		customerOrderHdDao.save(customerOrderHd);


		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		customerOrderDet.setCustomerOrderHd(customerOrderHd);
		customerOrderDet.setItemNo(memberCashValuePaymentDto.getItemNos()[0]);
		customerOrderDet.setOrderQty(new Long(memberCashValuePaymentDto.getOrderQty()));
		customerOrderDet.setItemTotalAmout(memberCashValuePaymentDto.getTotalAmount());
		customerOrderDet.setCreateBy(memberCashValuePaymentDto.getUserId());
		customerOrderDet.setCreateDate(currentTime);
		customerOrderDetDao.save(customerOrderDet);


		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setPaymentMethodCode(Constant.CASH);
		customerOrderTrans.setTransactionTimestamp(currentTime);
		customerOrderTrans.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		customerOrderTrans.setPaymentRecvBy(memberCashValuePaymentDto.getUserId());
		customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		customerOrderTrans.setPaymentLocationCode(memberCashValuePaymentDto.getLocation());
		customerOrderTransDao.save(customerOrderTrans);
		
		CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
		customerOrderTransDto.setOrderNo(customerOrderHd.getOrderNo());
		customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(currentTime));
		customerOrderTransDto.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTransDto.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
		
		return customerOrderTransDto;
	}

	@Override
	public String getCommonQueryForMembersList(String courseId,
			String attendance, String status) {
		return courseEnrollmentDao.getCommonQueryForMembersList(courseId, attendance, status);
		
	}


	@Override
	@Transactional
	public List<MemberAttendanceDto> getMembersAttendance(String courseId,
			List<MemberInfoDto> members,String sessionId) {
		List<MemberAttendanceDto> attendanceList = null;
		for (MemberInfoDto member : members) {
			attendanceList = courseEnrollmentDao.getMemberAttendance(courseId,
					member.getEnrollId(),sessionId);
			member.setAttendance(attendanceList);

		}
		return attendanceList;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public ResponseResult prepareMemberListResponse(String courseId,
			ResponseResult tmpResult, String attendance, String status, ListPage<CourseMaster> page,String sessionId) {
		
			
		try {
			if (CommUtil.notEmpty(courseId)) {

				CourseMaster courseMaster = courseMasterDao.getUniqueByCol(CourseMaster.class, "courseId", new Long(courseId));

				if (null == courseMaster) {
					responseResult.initResult(GTAError.CourseError.COURSE_DOESNT_EXIST);
				} else {

					CourseMembersDto course = new CourseMembersDto();
					CourseListDto coursePrimayInfo = courseMasterDao.getCourseInfo(courseId);

					course.setCapacity(coursePrimayInfo.getCapacity());
					course.setEnrollment(coursePrimayInfo.getEnrollment());
					course.setCourseName(coursePrimayInfo.getCourseName());
					course.setMemberAcceptance(coursePrimayInfo.getMemberAcceptance());

					if(null == tmpResult.getListData()){
						return tmpResult;
					}
					List<MemberInfoDto> members = (List<MemberInfoDto>) tmpResult.getListData().getList();
					List<MemberAttendanceDto> attendanceList;
					for (MemberInfoDto member : members) {
						
						attendanceList = courseEnrollmentDao.getMemberAttendance(courseId, member.getEnrollId(),sessionId);
						member.setAttendance(attendanceList);
						
					}
					

					Data data = new Data();
					course.setMembers(members);

					List<CourseMembersDto> courseData = new ArrayList<CourseMembersDto>();
					courseData.add(course);

					data.setList(courseData);
					data.setTotalPage(page.getAllPage());
					data.setCurrentPage(page.getNumber());
					data.setPageSize(page.getSize());
					data.setRecordCount(members.size());
					data.setLastPage(page.isLast());

					responseResult.initResult(GTAError.Success.SUCCESS, data);
					
				}

			}
			return responseResult;

		} catch (Exception e) {
			logger.error("CourseServiceImpl : prepareMemberListResponse method ", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_MEMBER_LIST);
			return responseResult;
		}
	}
	
	/**
	 * This method is to change course status when capacity, registBeginDate, registDueDate change
	 * @Author Mianping_Wu
	 * @Date Aug 11, 2015
	 * @Param @param dto
	 * @Param @param course
	 * @Param @return
	 * @return ResponseResult
	 */
	private ResponseResult changeCourseStatus(NewCourseDto dto, CourseMaster course) throws Exception {
	    
	    	int currentEnrollMembers = courseEnrollmentDao.countByCourseId(course.getCourseId());
		Long newCapacity = dto.getCapacity();
		if (newCapacity < currentEnrollMembers) {
		    responseResult.initResult(GTAError.CourseError.COURSE_CAPACITY_ERROR, new String[] { String.valueOf(currentEnrollMembers) });
		    return responseResult;
		}
		
		String maxAndMinDateString = courseEnrollmentDao.getMaxAndMinEnrollDate(course.getCourseId());
		Date maxEnrollDate = null;
		Date minEnrollDate = null;
		if (!StringUtils.isEmpty(maxAndMinDateString)) {
		    
		    String[] dataArray = maxAndMinDateString.split("#");
		    if (dataArray.length > 0 && !StringUtils.isEmpty(dataArray[0])) {
			maxEnrollDate = DateCalcUtil.parseDateTime(dataArray[0]);
		    }
		    
		    if (dataArray.length > 1 && !StringUtils.isEmpty(dataArray[1])) {
			minEnrollDate = DateCalcUtil.parseDateTime(dataArray[1]);
		    }
		    
		}
		
		if (dto.getRegistBeginDate() != null && minEnrollDate != null && DateCalcUtil.getDatePart(dto.getRegistBeginDate()).after(DateCalcUtil.getDatePart(minEnrollDate))) {
		    
		    responseResult.initResult(GTAError.CourseError.COURSE_REGIST_BEGIN_ERROR, new String[] { DateConvertUtil.getStrFromDate(minEnrollDate) });
		    return responseResult;
		}
		
		if (dto.getRegistDueDate() != null && maxEnrollDate != null && DateCalcUtil.getDatePart(dto.getRegistDueDate()).before(DateCalcUtil.getDatePart(maxEnrollDate))) {
		    
		    responseResult.initResult(GTAError.CourseError.COURSE_REGIST_END_ERROR, new String[] { DateConvertUtil.getStrFromDate(maxEnrollDate) });
		    return responseResult;
		}
		
		
		Long oldCapacity = (course.getCapacity() == null ? 0L : course.getCapacity());
		Long capacity = (newCapacity != null ? newCapacity : oldCapacity);
		Date dueDate = (dto.getRegistDueDate() != null ? dto.getRegistDueDate() : course.getRegistDueDate());
		Date now = new Date();
		
		if (newCapacity != null 
			&& newCapacity > oldCapacity 
			&& CourseStatus.FULL.getType().equalsIgnoreCase(course.getStatus())
			&& (dueDate.after(now))) {
		    course.setStatus(CourseStatus.OPEN.getType());
		}

		if (currentEnrollMembers < capacity 
			&& CourseStatus.CLOSE.getType().equalsIgnoreCase(course.getStatus())
			&& dueDate.after(now)) {
		    
		    course.setStatus(CourseStatus.OPEN.getType());
		} else if (capacity.intValue() == currentEnrollMembers 
			&& CourseStatus.OPEN.getType().equalsIgnoreCase(course.getStatus())) {
		    
		    course.setStatus(CourseStatus.FULL.getType());
		}
		
		return null;
	}
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 13, 2015
	 * @Param @param sysId
	 * @return void
	 */
	private void changeSessionNoAddingOne(String sysId) {
	    
	    CourseSession session = courseSessionDao.getCourseSessionById(Long.parseLong(sysId));
	    if (session == null || session.getSessionNo() == null) return;
	    courseSessionDao.moveSessionNoForword(session.getCourseId(), session.getSessionNo());
	}

	
	@Transactional
	public ResponseResult repaymentCourse(CourseEnrollmentDto courseEnrollmentDto, String createBy) throws Exception {
		
	    Long customerId = courseEnrollmentDto.getCustomerId();
	    Long courseId = courseEnrollmentDto.getCourseId();
		String paymentMethod = courseEnrollmentDto.getPaymentMethod();
		String device = courseEnrollmentDto.getDevice();
		
		CourseMaster course = courseMasterDao.get(CourseMaster.class, courseId);
		CourseEnrollment courseEnrollment = courseEnrollmentDao.getCourseEnrollmentByCourseIdAndCustomerId(courseId, customerId);
		CustomerOrderTransDto orderTransDto = coursePayment(customerId, course.getPosItemNo(), paymentMethod, createBy, device, courseEnrollmentDto.getPaymentLocation());

		Map<String, Object> smsInfo = null;
		Date now = new Date();
		if (!Constant.CREDIT_CARD.equals(paymentMethod)){ 
			
			courseEnrollment.setStatus(EnrollCourseStatus.REG.toString());
			courseEnrollment.setStatusUpdateBy(createBy);
			courseEnrollment.setStatusUpdateDate(now);
			smsInfo = notifyCourseMembers(SceneType.COURSE_ENROLL_MANUAL_CONFIRM, course);
			
		} 

		courseEnrollment.setCustOrderDetId(orderTransDto.getOrderNo());
		courseEnrollment.setUpdateBy(createBy);
		courseEnrollment.setUpdateDate(now);
		courseEnrollmentDao.updateCourseEnrollment(courseEnrollment);
		if(!Constant.CREDIT_CARD.equals(paymentMethod)){
			sendCourseEmailAfterPayment(orderTransDto.getOrderNo());
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("enrollMent", courseEnrollment);
		map.put("orderTrans", orderTransDto);
		map.put("smsInfo", smsInfo);
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}
	
	/**
	 * This method is to check if current operation is repayment for auto and credit card case
	 * @Author Mianping_Wu
	 * @Date Aug 27, 2015
	 * @Param @param courseEnrollmentDto
	 * @Param @return
	 * @return boolean
	 */
	@Transactional
	public boolean isRepayCourse(CourseEnrollmentDto courseEnrollmentDto) {
		
	    Long customerId = courseEnrollmentDto.getCustomerId();
	    Long courseId = courseEnrollmentDto.getCourseId();
//		String paymentMethod = courseEnrollmentDto.getPaymentMethod();
		
//		if (!Constant.CREDIT_CARD.equals(paymentMethod)) {
//			return false;
//		}

        CourseMaster course = courseMasterDao.get(CourseMaster.class, courseId);
        if (course == null) return false;

        String memberAcceptance = course.getMemberAcceptance();
        if (!MemberAcceptance.AUTO.getType().equals(memberAcceptance)) {
            return false;
        }

        CourseEnrollment courseEnrollment = courseEnrollmentDao.getCourseEnrollmentByCourseIdAndCustomerId(courseId, customerId);
        if (courseEnrollment == null) {
            return false;
        }

        if (!EnrollCourseStatus.ACT.getDesc().equalsIgnoreCase(courseEnrollment.getStatus())) {
            return false;
        }

        return true;
    }

    @Transactional
    @Override
    public ResponseResult getMonthlyCourseCompletionReport(ListPage<CourseMaster> page, String closedDate, String courseType) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM").parseDateTime(closedDate);
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();
        ListPage<?> listPage = courseMasterDao.getMonthlyCourseCompletionReport(page, year, month, courseType);
        List<Object> listBooking = listPage.getDtoList();
        Data data = new Data();
        data.setLastPage(listPage.isLast());
        data.setRecordCount(listPage.getAllSize());
        data.setPageSize(listPage.getSize());
        data.setTotalPage(listPage.getAllPage());
        data.setCurrentPage(listPage.getNumber());
        data.setList(listBooking);
        responseResult.initResult(GTAError.Success.SUCCESS, data);
        return responseResult;
    }

    @Transactional
    @Override
    public byte[] getMonthlyCourseCompletionReportAttach(String closedDate, String fileType, String courseType, String sortBy, String isAscending) {

        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM").parseDateTime(closedDate);
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("closedMonth", month);
        parameters.put("closedYear", year);
        parameters.put("courseType", courseType);
        parameters.put("fileType", fileType);
        parameters.put("selectedMonth", year + "-" + String.format("%0" + 2 + "d", month) + "-01");

        ReportRequestDto reportRequestDto = new ReportRequestDto();
        reportRequestDto.setParameters(parameters);
        reportRequestDto.setFileType(fileType);
        reportRequestDto.setIsAscending(isAscending);
        reportRequestDto.setReportFile("MonthlyCourseCompletion.jasper");
        reportRequestDto.setSortBy(sortBy);
        return reportService.getReportAttach(reportRequestDto);

    }

    @Transactional
    @Override
    public ResponseResult getMonthlyCourseRevenueReport(ListPage page, String transactionDate, String facilityType) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM").parseDateTime(transactionDate);
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();
        ListPage<?> listPage = courseMasterDao.getMonthlyCourseRevenueReport(page, year, month, facilityType);
        List<Object> listBooking = listPage.getDtoList();
        Data data = new Data();
        data.setLastPage(listPage.isLast());
        data.setRecordCount(listPage.getAllSize());
        data.setPageSize(listPage.getSize());
        data.setTotalPage(listPage.getAllPage());
        data.setCurrentPage(listPage.getNumber());
        data.setList(listBooking);
        responseResult.initResult(GTAError.Success.SUCCESS, data);
        return responseResult;
    }

    @Override
    public byte[] getMonthlyCourseRevenueReportAttach(String transactionDate, String fileType, String facilityType, String sortBy, String isAscending) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM").parseDateTime(transactionDate);
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("month", month);
        parameters.put("year", year);
        parameters.put("facilityType", facilityType);
        parameters.put("fileType", fileType);
        parameters.put("selectedMonth", year + "-" + String.format("%0" + 2 + "d", month) + "-01");

        ReportRequestDto reportRequestDto = new ReportRequestDto();
        reportRequestDto.setParameters(parameters);
        reportRequestDto.setFileType(fileType);
        reportRequestDto.setIsAscending(isAscending);
        reportRequestDto.setReportFile("MonthlyCourseRevenue.jasper");
        reportRequestDto.setSortBy(sortBy);
        return reportService.getReportAttach(reportRequestDto);
    }
}

