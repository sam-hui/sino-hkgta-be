package com.sinodynamic.hkgta.service.pos;

import java.util.Date;

import com.sinodynamic.hkgta.entity.pos.RestaurantOpenHour;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RestaurantOpenHourService extends IServiceBase<RestaurantOpenHour> {
	
	public boolean isAvailableRestaurant(String restaurantId,Integer partySize, Date bookTime,String bookVia) throws Exception;
}
