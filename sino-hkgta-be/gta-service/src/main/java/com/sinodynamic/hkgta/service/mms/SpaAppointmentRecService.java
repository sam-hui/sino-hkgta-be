package com.sinodynamic.hkgta.service.mms;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface SpaAppointmentRecService extends IServiceBase<Serializable> {
    
    public String getMmsAdvanceSql(String beginDate, String endDate, String customerId, String status) throws Exception;
    
    public List<MmsOrderItemDto> getSpaAppointmentRecByExtinvoiceNo(String extInvoiceNo);
    
    public void sendSmsWhenCancelSpaOrder(String extInvoiceNo) throws Exception;
    
    public void sendSmsWhenCancelSpaOrderItem(String extAppointmentId) throws Exception;
    
    public void spaAppointmentBeginNotification() throws Exception;
    
    public void spaAppointmentEndNotification() throws Exception;
}
