package com.sinodynamic.hkgta.service.pms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.pms.RoomCrewDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dto.pms.RoomCrewDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class RoomCrewServiceImpl extends ServiceBase<RoomCrew> implements RoomCrewService {

	@Autowired
	private RoomCrewDao roomCrewDao;

	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private RoomDao roomDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomCrewDto> getRoomCrewList(Long roomId)
	{
		List<RoomCrew> roomCrewList = roomCrewDao.getRoomCrewList(roomId);
		List<RoomCrewDto> roomCrewDtos = null;
		if(null != roomCrewList && roomCrewList.size() > 0){
			roomCrewDtos = new ArrayList<RoomCrewDto>();
			for(RoomCrew roomCrew : roomCrewList){
				RoomCrewDto crewDto = new RoomCrewDto();
				crewDto.setCrewId(roomCrew.getCrewId());
				crewDto.setUserId(roomCrew.getStaffProfile().getUserId());
				crewDto.setUserName(roomCrew.getStaffProfile().getGivenName() + " " + roomCrew.getStaffProfile().getSurname());
				SysCode sysCode = sysCodeDao.getByCategoryAndCodeValue("HK-CREW-ROLE", roomCrew.getCrewRoleId());
				if(null != sysCode){
					crewDto.setTitle(sysCode.getCodeDisplay());
				}
				roomCrewDtos.add(crewDto);
			}
		}
		return roomCrewDtos;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeRoomCrew(Long crewId)
	{
		RoomCrew roomCrew = roomCrewDao.get(RoomCrew.class, crewId);
		if(null != roomCrew){
			roomCrewDao.delete(roomCrew);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void assignRoomCrew(RoomCrewDto roomCrewDto)
	{
		StaffProfile staffProfile = staffProfileDao.get(StaffProfile.class, roomCrewDto.getUserId());
		StaffMaster staffMaster = staffMasterDao.getByUserId(roomCrewDto.getUserId());
		if( null != staffMaster && staffProfile != null){
			for(int i=0; i<roomCrewDto.getRoomIds().length ;i++){
				Room room = roomDao.get(Room.class, roomCrewDto.getRoomIds()[i]);
				if(null != room){
					RoomCrew roomCrew = roomCrewDao.getRoomCrew(room.getRoomId(), roomCrewDto.getUserId(), staffMaster.getStaffType());
					if(null == roomCrew){
						roomCrew = new RoomCrew();
						roomCrew.setRoom(room);
						roomCrew.setStaffProfile(staffProfile);
						roomCrew.setCrewRoleId(null == staffMaster?"":staffMaster.getStaffType());
						roomCrew.setUpdateBy(roomCrewDto.getUpdateBy());
						roomCrew.setUpdateDate(new Date());
						roomCrewDao.save(roomCrew);
					}
				}
			}
		}
	}

	@Override
	@Transactional
	public List<RoomCrew> getRoomCrewList(Long roomId, String crewRoleId)
	{
		return roomCrewDao.getRoomCrewListByCrewRoleId(roomId, crewRoleId);
	}
	
}
