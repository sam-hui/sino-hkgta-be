package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationRateDateDao;
import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationRateTimeDao;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateDateDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeDto;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateDate;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;

@Service
public class FacilityUtilizationRateDateServiceImpl extends ServiceBase<FacilityUtilizationRateDate> implements FacilityUtilizationRateDateService
{

	@Autowired
	private FacilityUtilizationRateDateDao facilityUtilizationRateDateDao;

	@Autowired
	private FacilityUtilizationRateTimeDao facilityUtilizationRateTimeDao;


	/* (non-Javadoc)
	 * @see com.sinodynamic.hkgta.service.fms.FacilityUtilizationRateDateService#getSpecialRateDateList(java.lang.String, int)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityUtilizationRateDateDto> getSpecialRateDateList(String facilityType, int year, String sortBy, String ascending, String subType) throws Exception
	{
		List<FacilityUtilizationRateDate> facilityUtilizationRateDateList =  facilityUtilizationRateDateDao.getSpecialRateDateList(facilityType, year, subType);
		List<FacilityUtilizationRateDateDto> dtoReturnList = convertRateDateDtoList(facilityType, facilityUtilizationRateDateList);
		sortByField(dtoReturnList, sortBy, ascending);
		return dtoReturnList; 
	}

	private List<FacilityUtilizationRateDateDto> convertRateDateDtoList(String facilityType, List<FacilityUtilizationRateDate> facilityUtilizationRateDateList) throws Exception
	{
		List<FacilityUtilizationRateDateDto> dtoReturnList = new ArrayList<FacilityUtilizationRateDateDto>();
		for(FacilityUtilizationRateDate facilityUtilizationRateDate:facilityUtilizationRateDateList){
			FacilityUtilizationRateDateDto facilityUtilizationRateDateDto = new FacilityUtilizationRateDateDto();
			facilityUtilizationRateDateDto.setDateId(facilityUtilizationRateDate.getDateId());
			facilityUtilizationRateDateDto.setSpecialDate(DateConvertUtil.getYMDFormatDate(facilityUtilizationRateDate.getSpecialDate()));
			facilityUtilizationRateDateDto.setDateDesc(facilityUtilizationRateDateDto.getDateDesc());
			facilityUtilizationRateDateDto.setFacilityType(facilityType);
			facilityUtilizationRateDateDto.setFacilitySubTypeId(facilityUtilizationRateDate.getFacilitySubtypeId());
			facilityUtilizationRateDateDto.setUpdatedBy(facilityUtilizationRateDate.getCreateBy());
			facilityUtilizationRateDateDto.setWeekDay(null);
			facilityUtilizationRateDateDto.setDateDesc(facilityUtilizationRateDate.getDateDesc());
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(facilityUtilizationRateDate.getCreateDate().getTime());
			facilityUtilizationRateDateDto.setUpdatedTime(DateConvertUtil.getYMDDateAndDateDiff(calendar.getTime()));
			List<FacilityUtilizationRateTimeDto> rateTimeDtoList = convertRateTimeDtoList(facilityUtilizationRateDate);
			sortRateTimeDtoList(rateTimeDtoList);
			facilityUtilizationRateDateDto.setRateList(rateTimeDtoList);
			dtoReturnList.add(facilityUtilizationRateDateDto);
		}
		return dtoReturnList;
	}

	private List<FacilityUtilizationRateTimeDto> convertRateTimeDtoList(FacilityUtilizationRateDate facilityUtilizationRateDate)
	{
		List<Integer> hourList = Arrays.asList(7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22);
		List<Integer> hourBit = Arrays.asList(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		List<FacilityUtilizationRateTimeDto> rateTimeDtoList = new ArrayList<FacilityUtilizationRateTimeDto>();
		for(FacilityUtilizationRateTime facilityUtilizationRateTime: facilityUtilizationRateDate.getFacilityUtilizationRateTimes()){
			FacilityUtilizationRateTimeDto rateTimeDto = new FacilityUtilizationRateTimeDto();
			rateTimeDto.setBeginTime(facilityUtilizationRateTime.getBeginTime());
			rateTimeDto.setRateType(facilityUtilizationRateTime.getRateType());
			rateTimeDto.setTimeId(facilityUtilizationRateTime.getTimeId());
			rateTimeDtoList.add(rateTimeDto);
			if (hourList.indexOf(facilityUtilizationRateTime.getBeginTime())>=0){
				hourBit.set(hourList.indexOf(facilityUtilizationRateTime.getBeginTime()), 1);
			}
		}
		int index = 0;
		for(Integer bit : hourBit){
			if (bit.intValue() == 0) {
				FacilityUtilizationRateTimeDto rateTimeDto = new FacilityUtilizationRateTimeDto();
				rateTimeDto.setBeginTime(hourList.get(index));
				rateTimeDto.setRateType("LO");
				rateTimeDto.setTimeId(-1l);
				rateTimeDtoList.add(rateTimeDto);
			}
			index++;
		}
		return rateTimeDtoList;
	}
	
	private void sortRateTimeDtoList(List<FacilityUtilizationRateTimeDto> facilityUtilizationRateTimeDtoList) {
		if (facilityUtilizationRateTimeDtoList.size()>0){
			for(int i=0;i< facilityUtilizationRateTimeDtoList.size() - 1;i++) {
				for(int j=i+1;j< facilityUtilizationRateTimeDtoList.size();j++) {
					if (facilityUtilizationRateTimeDtoList.get(i).getBeginTime() > facilityUtilizationRateTimeDtoList.get(j).getBeginTime()) {
						FacilityUtilizationRateTimeDto swap = facilityUtilizationRateTimeDtoList.get(j);
						facilityUtilizationRateTimeDtoList.set(j, facilityUtilizationRateTimeDtoList.get(i));
						facilityUtilizationRateTimeDtoList.set(i, swap);
					}
				}
			}
		}
	}
	
	private void sortByField(List<FacilityUtilizationRateDateDto> facilityUtilizationRateDateDtoList, String sortBy, String ascending) throws Exception {
		List<Method> methods =	Arrays.asList(FacilityUtilizationRateDateDto.class.getMethods());
		Boolean found = false;
		int methodIndex = 0;
		for(Method method: methods){
			if (method.getName().startsWith("get") && method.getName().substring(3).toUpperCase().equals(sortBy.toUpperCase())){
				found = true;
				break;
			}
			methodIndex++;
		}
		if (facilityUtilizationRateDateDtoList.size()>0 && found) {
			for(int i=0;i< facilityUtilizationRateDateDtoList.size() - 1;i++) {
				for(int j=i+1;j< facilityUtilizationRateDateDtoList.size();j++) {
					if (ascending.toUpperCase().equals("TRUE")){
						if (((Comparable)methods.get(methodIndex).invoke(facilityUtilizationRateDateDtoList.get(i), null)).compareTo(
								((Comparable)methods.get(methodIndex).invoke(facilityUtilizationRateDateDtoList.get(j), null))) > 0){
							swapRateDateDto(facilityUtilizationRateDateDtoList, i, j);
						}
					}else{
						if (((Comparable)methods.get(methodIndex).invoke(facilityUtilizationRateDateDtoList.get(i), null)).compareTo(
								((Comparable)methods.get(methodIndex).invoke(facilityUtilizationRateDateDtoList.get(j), null))) < 0){
							swapRateDateDto(facilityUtilizationRateDateDtoList, i, j);
						}
					}
				}
			}
		}
	}

	private void swapRateDateDto(List<FacilityUtilizationRateDateDto> facilityUtilizationRateDateDtoList, int i, int j)
	{
		FacilityUtilizationRateDateDto swap = facilityUtilizationRateDateDtoList.get(i);
		facilityUtilizationRateDateDtoList.set(i, facilityUtilizationRateDateDtoList.get(j));
		facilityUtilizationRateDateDtoList.set(j, swap);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public FacilityUtilizationRateDateDto getSpecialRateDateListByDate(String facilityType, String specialDate, String subType) throws Exception
	{
		List<FacilityUtilizationRateDate> facilityUtilizationRateDateList = facilityUtilizationRateDateDao.getSpecialRateDateListByDate(facilityType, DateCalcUtil.parseDate(specialDate), subType);
		if (facilityUtilizationRateDateList.size()==0){
			String weekDay = DateCalcUtil.getDayOfWeek(DateCalcUtil.parseDate(specialDate));
			List<FacilityUtilizationRateTime> facilityUtilizationRateTimeList = facilityUtilizationRateTimeDao.getFacilityUtilizationRateTimeList(facilityType, weekDay, subType);
			FacilityUtilizationRateDate facilityUtilizationRateDate = new FacilityUtilizationRateDate();
			facilityUtilizationRateDate.setCreateBy("");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			facilityUtilizationRateDate.setCreateDate(new Timestamp(calendar.getTimeInMillis()));
			facilityUtilizationRateDate.setDateDesc("");
			facilityUtilizationRateDate.setDateId(-1L);
			facilityUtilizationRateDate.setSpecialDate(DateCalcUtil.parseDate(specialDate));
			if (facilityUtilizationRateTimeList != null && facilityUtilizationRateTimeList.size() > 0){
				facilityUtilizationRateDate.setFacilityUtilizationRateTimes(facilityUtilizationRateTimeList);
			}else{
				facilityUtilizationRateDate.setFacilityUtilizationRateTimes(new ArrayList<FacilityUtilizationRateTime>());
			}
			facilityUtilizationRateDateList.add(facilityUtilizationRateDate);
		}
		List<FacilityUtilizationRateDateDto> dtoReturnList = convertRateDateDtoList(facilityType, facilityUtilizationRateDateList);
		if (dtoReturnList != null && dtoReturnList.size() > 0) {
			return dtoReturnList.get(0);
		} else {
			return null;
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveSpecialRateDate(String facilityType, FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) throws Exception
	{
		if (facilityUtilizationRateDateDto != null){
			FacilityUtilizationRateDate facilityUtilizationRateDate = convertDtoToRateDate(facilityType, facilityUtilizationRateDateDto);
			facilityUtilizationRateDateDao.save(facilityUtilizationRateDate);
			List<FacilityUtilizationRateTime> facilityUtilizationRateTimeList = convertDtoToRateTime(facilityType, facilityUtilizationRateDateDto.getRateList(), facilityUtilizationRateDate);
			for(FacilityUtilizationRateTime facilityUtilizationRateTime : facilityUtilizationRateTimeList) {
				facilityUtilizationRateTimeDao.save(facilityUtilizationRateTime);
			}
		}
	}

	private FacilityUtilizationRateDate convertDtoToRateDate(String facilityType, FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) throws Exception
	{
		FacilityUtilizationRateDate facilityUtilizationRateDate = new FacilityUtilizationRateDate();
		facilityUtilizationRateDate.setCreateBy(facilityUtilizationRateDateDto.getUpdatedBy());
		facilityUtilizationRateDate.setCreateDate(DateCalcUtil.parseTimestamp(DateCalcUtil.formatDate(new Date())));
		facilityUtilizationRateDate.setDateDesc(facilityUtilizationRateDateDto.getDateDesc());
		facilityUtilizationRateDate.setSpecialDate(DateCalcUtil.parseDate(facilityUtilizationRateDateDto.getSpecialDate()));
		facilityUtilizationRateDate.setFacilityType(facilityUtilizationRateDateDto.getFacilityType());
		facilityUtilizationRateDate.setFacilitySubtypeId(facilityUtilizationRateDateDto.getFacilitySubTypeId());
		return facilityUtilizationRateDate;
	}

	private FacilityUtilizationRateDate convertDtoToRateDateForUpdate(String facilityType, FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) throws Exception
	{
		FacilityUtilizationRateDate facilityUtilizationRateDate = convertDtoToRateDate(facilityType, facilityUtilizationRateDateDto);
		facilityUtilizationRateDate.setDateId(facilityUtilizationRateDateDto.getDateId());
		return facilityUtilizationRateDate;
	}

	private List<FacilityUtilizationRateTime> convertDtoToRateTime(String facilityType, List<FacilityUtilizationRateTimeDto> rateList, FacilityUtilizationRateDate facilityUtilizationRateDate)
	{
		List<FacilityUtilizationRateTime> facilityUtilizationRateTimeList = new ArrayList<FacilityUtilizationRateTime>();
		for(FacilityUtilizationRateTimeDto facilityUtilizationRateTimeDto : rateList){
			FacilityUtilizationRateTime facilityUtilizationRateTime = new FacilityUtilizationRateTime();
			facilityUtilizationRateTime.setBeginTime(facilityUtilizationRateTimeDto.getBeginTime());
			facilityUtilizationRateTime.setEndTime(facilityUtilizationRateTimeDto.getBeginTime() + 1);
			facilityUtilizationRateTime.setFacilityDateId(facilityUtilizationRateDate.getDateId());
			facilityUtilizationRateTime.setFacilityNo(null);;
			facilityUtilizationRateTime.setFacilityType(facilityType);
			facilityUtilizationRateTime.setFacilitySubtypeId(facilityUtilizationRateDate.getFacilitySubtypeId());
			facilityUtilizationRateTime.setRateType(facilityUtilizationRateTimeDto.getRateType());
			facilityUtilizationRateTime.setCreateBy(facilityUtilizationRateDate.getCreateBy());
			facilityUtilizationRateTime.setCreateDate(facilityUtilizationRateDate.getCreateDate());
			facilityUtilizationRateTimeList.add(facilityUtilizationRateTime);
		}
		return facilityUtilizationRateTimeList;
	}

	private List<FacilityUtilizationRateTime> convertDtoToRateTimeForUpdate(String facilityType, List<FacilityUtilizationRateTimeDto> rateList, FacilityUtilizationRateDate facilityUtilizationRateDate)
	{
		List<FacilityUtilizationRateTime> facilityUtilizationRateTimeList = convertDtoToRateTime(facilityType, rateList, facilityUtilizationRateDate);
		for(int i=0;i< rateList.size();i++) {
			if (rateList.get(i).getTimeId().compareTo(0l) > 0) {
				facilityUtilizationRateTimeList.get(i).setTimeId(rateList.get(i).getTimeId());
			}
		}
		return facilityUtilizationRateTimeList;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateSpecialRateDate(String facilityType, FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) throws Exception
	{
		if (facilityUtilizationRateDateDto != null){
			FacilityUtilizationRateDate facilityUtilizationRateDate = convertDtoToRateDateForUpdate(facilityType, facilityUtilizationRateDateDto);
			facilityUtilizationRateDateDao.update(facilityUtilizationRateDate);
			List<FacilityUtilizationRateTime> facilityUtilizationRateTimeList = convertDtoToRateTimeForUpdate(facilityType, facilityUtilizationRateDateDto.getRateList(), facilityUtilizationRateDate);
			for(FacilityUtilizationRateTime facilityUtilizationRateTime : facilityUtilizationRateTimeList) {
				if (facilityUtilizationRateTime.getTimeId() <= 0) {
					facilityUtilizationRateTimeDao.save(facilityUtilizationRateTime);
				} else {
					facilityUtilizationRateTimeDao.update(facilityUtilizationRateTime);
				}
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteSpecialRateDate(long dateId) throws Exception
	{
		FacilityUtilizationRateDate facilityUtilizationRateDate = facilityUtilizationRateDateDao.get(FacilityUtilizationRateDate.class, dateId);
		List<FacilityUtilizationRateTime> facilityUtilizationRateTimeList = facilityUtilizationRateTimeDao.getByCol(FacilityUtilizationRateTime.class, "facilityDateId", dateId, null);
		if (facilityUtilizationRateDate != null) {
			facilityUtilizationRateDateDao.delete(facilityUtilizationRateDate);
			if (facilityUtilizationRateTimeList != null && facilityUtilizationRateTimeList.size() > 0) {
				for(FacilityUtilizationRateTime facilityUtilizationRateTime : facilityUtilizationRateTimeList) {
					facilityUtilizationRateTimeDao.delete(facilityUtilizationRateTime);
				}
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Boolean checkSpecialDateExistance(String specialDateStr, String facilityType, String subType) throws Exception
	{
		Date specialDate = DateCalcUtil.parseDate(specialDateStr);
		List<FacilityUtilizationRateDate> facilityUtilizationRateDateList = facilityUtilizationRateDateDao.getByCols(FacilityUtilizationRateDate.class, new String[] {"specialDate", "facilityType", "facilitySubtypeId"}, new Serializable[] {specialDate, facilityType, subType}, null);
		if (facilityUtilizationRateDateList != null && facilityUtilizationRateDateList.size() > 0){
			return true;
		}
		return false;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Boolean checkSpecialDateExistanceById(long dateId) throws Exception
	{
		List<FacilityUtilizationRateDate> facilityUtilizationRateDateList = facilityUtilizationRateDateDao.getByCols(FacilityUtilizationRateDate.class, new String[] {"dateId"}, new Serializable[] {dateId}, null);
		if (facilityUtilizationRateDateList != null && facilityUtilizationRateDateList.size() > 0){
			return true;
		}
		return false;
	}
}
