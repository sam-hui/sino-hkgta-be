package com.sinodynamic.hkgta.service.mms;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentSynDto;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.spa.em.AppointmentStatus;
import com.sinodynamic.hkgta.integration.spa.request.GetAppointmentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDatum;
import com.sinodynamic.hkgta.integration.spa.response.GetAppointmentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.Payment;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.OrderStatus;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SceneType;

@Service
public class SynchronizeSpaDataImpl implements SynchronizeSpaData {
    
    private Logger logger = Logger.getLogger(SynchronizeSpaDataImpl.class);
    
//    private static final int APPOINTMENT_DAYS_FORWORD = 0;
//    private static final int PAYMENT_DAYS_FORWORD = 0;
    private static final int APPOINTMENT_DAYS_AFTERWORD = 14;
    private static final String CUSTOMIZE_TYPE = "Custom";
    
    @Autowired
    private SpaServcieManager spaServcieManager;
    
    @Autowired
    private SpaAppointmentRecDao spaAppointmentRecDao;
    
    @Autowired
    private CustomerOrderDetDao customerOrderDetDao;
    
    @Autowired
    private CustomerOrderHdDao customerOrderHdDao;
    
    @Autowired
    private CustomerOrderTransDao customerOrderTransDao;
    
    @Autowired
    private MemberDao memberDao;
    
    @Autowired 
    private SpaAppointmentRecService spaAppointmentRecService;
    
    @Autowired
    private MMSService mMSService;
    
    @SuppressWarnings("serial")
    private static final HashMap<String, String> APPOINTMENT_STATUS = new HashMap<String, String>() {
	    {
	        put("Open", "RSV");  
	        put("Confirmed", "RSV");
	        put("Checkin", "ATN");
	        put("No Show", "NAT");
	        put("Cancelled", "CAN");
	        put("Closed", "CLD");
	    }
	};
	
	    @SuppressWarnings("serial")
	    private static final HashMap<String, PaymentMethod> PAYMENT_METHOD = new HashMap<String, PaymentMethod>() {
		    {
		        put("Cash", PaymentMethod.CASH);
		        put("Cash Value", PaymentMethod.CV);
		        put("Visa", PaymentMethod.VISA);
		        put("Mastercard", PaymentMethod.MASTER);
		        put("Check", PaymentMethod.CHEQUE);
		        put("Debit Card", PaymentMethod.OTH);
		    }
	    };
    
	    
    @Override
    @Transactional
    public void synchronizePayments() {
	
	logger.info("payment synchronize invocation start ...");
	try {
	    
	    Date toTime = new Date();
	    Date fromTime = getPaymentsLatestSynSuccessTime(toTime);
	    paymentSynchronize(fromTime, toTime);
	    
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
	
	logger.info("payment synchronize invocation end ...");
	
    }


    @Override
    @Transactional
    public void synchronizeAppointments() {
	
	logger.info("appointment synchronize invocation start ...");
	try {
	    
	    Date now = new Date();
	    Date fromTime = getAppointmentsLatestSynSuccessTime(now);
	    Date toTime = DateCalcUtil.getNearDay(now, APPOINTMENT_DAYS_AFTERWORD);
	    appointmentSynchronize(fromTime, toTime);
	    
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
	
	logger.info("appointment synchronize invocation end ...");
	
    }

    private Map<String, List<SpaAppointmentSynDto>> groupingAppointments(List<SpaAppointmentSynDto> result) {
	
	if (result == null || result.size() == 0) return null;
	
	Map<String, List<SpaAppointmentSynDto>> groupResult =  new HashMap<String, List<SpaAppointmentSynDto>>();
	for (SpaAppointmentSynDto dto : result) {
	    
	    String key = dto.getExtInvoiceNo();
	    if (StringUtils.isEmpty(key)) continue;
	    List<SpaAppointmentSynDto> value = groupResult.get(key);
	    if (value == null) {
		value = new ArrayList<SpaAppointmentSynDto>();
		value.add(dto);
		groupResult.put(key, value);
	    } else {
		value.add(dto);
	    }
	}
	
	return groupResult;
    }
    
    private Map<String, List<Payment>> groupingPayments(List<Payment> result) {
	
	if (result == null || result.size() == 0) return null;
	Map<String, List<Payment>> groupResult =  new HashMap<String, List<Payment>>();
	for (Payment p : result) {
	    String key = p.getInvoiceNo();
	    if (StringUtils.isEmpty(key)) continue;
	    List<Payment> value = groupResult.get(key);
	    if (value == null) {
		value = new ArrayList<Payment>();
		value.add(p);
		groupResult.put(key, value);
	    } else {
		value.add(p);
	    }
	}
	
	return groupResult;
    }
    
    
    private List<SpaAppointmentSynDto> formatAppointmentData(GetAppointmentsResponse appointmentsResponse) {
	
	if (appointmentsResponse == null || appointmentsResponse.getAppointmentData() == null || 
		appointmentsResponse.getAppointmentData().size() == 0) return null;
	
	List<AppointmentDatum> appointmentData = appointmentsResponse.getAppointmentData();
	List<SpaAppointmentSynDto> appointmentResult = new ArrayList<SpaAppointmentSynDto>();
	for (AppointmentDatum appointment : appointmentData) {
	    
	    SpaAppointmentSynDto dto = new SpaAppointmentSynDto();
	    dto.setExtAppointmentId(appointment.getAppointmentId());
	    dto.setExtServiceCode(appointment.getServiceCode());
	    dto.setExtInvoiceNo(appointment.getInvoiceNo());
	    dto.setGuestCode(appointment.getGuestCode());
	    dto.setServicePrice(appointment.getFinalSalePrice());
	    dto.setServiceInternalCost(appointment.getServiceInternalCost());
	    dto.setTherapistNo(appointment.getEmployeeCode());
	    dto.setServiceTime(appointment.getServiceTime());
	    dto.setServiceDescription(null);
	    dto.setStatus(appointment.getStatus());
	    dto.setStartTime(appointment.getStartTime());
	    dto.setEndTime(appointment.getEndTime());
	    dto.setServiceName(appointment.getServiceName());
	    dto.setActualTime(appointment.getActualTime());
	    dto.setRebooked(appointment.getRebooked());
	    dto.setCheckInTime(appointment.getCheckInTime());
	    dto.setCreatedBy(appointment.getCreatedby());
	    dto.setCreationDate(appointment.getCreationDate());
	    dto.setExtNote(appointment.getNote());
	    dto.setEmployeeFName(appointment.getEmployeeFName());
	    dto.setEmployeeLName(appointment.getEmployeeLName());
	    appointmentResult.add(dto);
	}
	
	return appointmentResult;
    }
    
    
    private void appointmentSynchronize(Date fromDate, Date toDate) throws Exception {

	// fetch appointment item data from remote
        GetAppointmentsRequest request = new GetAppointmentsRequest();
        request.setFromDate(fromDate);
        request.setToDate(toDate);
        request.setStatus(AppointmentStatus.ALL);
	GetAppointmentsResponse appointmentsResponse = spaServcieManager.getAppointments(request);
	
	List<SpaAppointmentSynDto> appointmentResult = formatAppointmentData(appointmentsResponse);
	if (appointmentResult == null || appointmentResult.size() == 0) return;

	// grouping result
	Map<String, List<SpaAppointmentSynDto>> appointmentGroup = groupingAppointments(appointmentResult);
	if (appointmentGroup == null || appointmentGroup.isEmpty()) return;

	//iterator for synchronize
	for (Map.Entry<String, List<SpaAppointmentSynDto>> entry : appointmentGroup.entrySet()) {
	    
	    // update spa appointments start
	    String extInvoiceNo = entry.getKey();
	    List<SpaAppointmentSynDto> newItems = entry.getValue();
	    List<SpaAppointmentRec> oldItems = spaAppointmentRecDao.getSpaAppointmentRecByExtinvoiceNo(extInvoiceNo);
	    
	    //when current appointment list closed, then stop current and continue
	    boolean isClosed = isAppointmentClosed(oldItems);
	    if (isClosed) continue;

	    // ignore items when guestCode is empty
	    String academyNo = getAcademyNo(newItems);
	    if (StringUtils.isEmpty(academyNo)) continue;
	    
	    // ignore items when guest not exists in gta db
	    Long customerId = memberDao.getMemberCustomerId(academyNo);
	    if (customerId == null) continue;
		
	    // logic for if a reservation is canceled by mms
	    boolean doCanceled = doesCancelHapppened(oldItems, newItems);
	    
	    // delete old items info
	    if (oldItems != null && oldItems.size() > 0) {
		for (SpaAppointmentRec entity : oldItems) {
		    spaAppointmentRecDao.delete(entity);
		}
	    }
	    
	    CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
	    if (order != null) {
		List<CustomerOrderDet> oldOrderDets = customerOrderDetDao.getCustomerOrderDetsByOrderNo(order.getOrderNo());
		if (oldOrderDets != null && oldOrderDets.size() > 0) {
		    for (CustomerOrderDet entity : oldOrderDets) {
			entity.setCustomerOrderHd(null);
			customerOrderDetDao.delete(entity);
		    }
		}
	    }
	    
	    // insert new items info
	    Date curDate = new Date();
	    Timestamp curTimestamp = new Timestamp(curDate.getTime());
	    if (newItems != null && newItems.size() > 0) {
		
		if (order == null) {
		    order = new CustomerOrderHd();
		    order.setOrderDate(curDate);
		    order.setOrderStatus(OrderStatus.OPN.getDesc());
		    order.setCustomerId(customerId);
		    order.setOrderRemark("");
		    order.setCreateDate(curTimestamp);		
		    order.setVendorRefCode(extInvoiceNo);
		    Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(order);
		    order.setOrderNo(orderNo);
		}
		
		BigDecimal orderAmount = BigDecimal.ZERO;
		boolean isOrderCanceled = true;
		for (SpaAppointmentSynDto item : newItems) {
		    
		    String status = APPOINTMENT_STATUS.get(item.getStatus());
		    if (StringUtils.isEmpty(status)) continue;
		    if (!"CAN".equals(status)) isOrderCanceled = false;
		    
		    //regenerate customer_order_det
		    CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		    order.setCustomerOrderDets(null);
		    customerOrderDet.setCustomerOrderHd(order);
		    customerOrderDet.setItemNo("MMS2BRS");
		    customerOrderDet.setItemRemark("");
		    customerOrderDet.setOrderQty(1);
		    BigDecimal amount = new BigDecimal(StringUtils.isEmpty(item.getServicePrice()) ? "0.00" : item.getServicePrice());
		    if (!"CAN".equals(status)) orderAmount = orderAmount.add(amount);
		    customerOrderDet.setItemTotalAmout(amount);
		    customerOrderDet.setExtRefNo(item.getExtServiceCode());
		    customerOrderDet.setUpdateDate(curDate);
		    customerOrderDet.setCreateDate(curTimestamp);
		    customerOrderDet.setUpdateBy("spa_sync");
		    customerOrderDet.setCreateBy("spa_sync");
		    Long orderDetId = (Long) customerOrderDetDao.saveOrderDet(customerOrderDet);
		    
		    //regenerate SpaAppointmentRec
		    SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
		    spaAppointmentRec.setCustomerId(order.getCustomerId());
		    spaAppointmentRec.setServiceName(item.getServiceName());
		    spaAppointmentRec.setRecoveryTime(StringUtils.isEmpty(item.getRecoveryTime()) ? 0L : Long.parseLong(item.getRecoveryTime()));
		    spaAppointmentRec.setActualTime(StringUtils.isEmpty(item.getActualTime()) ? 0L : Long.parseLong(item.getActualTime()));
		    spaAppointmentRec.setExtNote(item.getExtNote());
		    spaAppointmentRec.setRebooked(StringUtils.isEmpty(item.getRebooked()) ? 0L : Long.parseLong(item.getRebooked()));
		    //spaAppointmentRec.setCreateBy(item.getCreatedBy());
		    //spaAppointmentRec.setCreateDate(StringUtils.isEmpty(item.getCreationDate()) ? null : Timestamp.valueOf(item.getCreationDate()));
		    spaAppointmentRec.setCreateBy("spa_sync");
		    spaAppointmentRec.setCreateDate(curTimestamp);
		    spaAppointmentRec.setCheckinDatetime(StringUtils.isEmpty(item.getCheckInTime()) ? null : Timestamp.valueOf(item.getCheckInTime()));
		    spaAppointmentRec.setTherapistFirstname(item.getEmployeeFName());
		    spaAppointmentRec.setTherapistLastname(item.getEmployeeLName());
		    spaAppointmentRec.setServiceTime(Long.parseLong(item.getServiceTime()));
		    BigDecimal serviceInternalCost = new BigDecimal(StringUtils.isEmpty(item.getServiceInternalCost()) ? "0.00" : item.getServiceInternalCost());
		    spaAppointmentRec.setServiceInternalCost(serviceInternalCost);
		    spaAppointmentRec.setExtAppointmentId(item.getExtAppointmentId());
		    spaAppointmentRec.setExtInvoiceNo(item.getExtInvoiceNo());
		    spaAppointmentRec.setExtServiceCode(item.getExtServiceCode());
		    spaAppointmentRec.setServiceDescription(item.getServiceDescription());
		    spaAppointmentRec.setOrderDetId(orderDetId);		
		    String startTime = item.getStartTime();
		    String endTime = item.getEndTime();
		    spaAppointmentRec.setStatus(status);
		    spaAppointmentRec.setStartDatetime(Timestamp.valueOf(startTime));					
		    spaAppointmentRec.setEndDatetime(Timestamp.valueOf(endTime));
		    spaAppointmentRec.setTherapistCode(item.getTherapistNo());
		    spaAppointmentRec.setUpdateBy("spa_sync");
		    spaAppointmentRec.setUpdateDate(curTimestamp);
		    spaAppointmentRec.setExtSyncTimestamp(curTimestamp);
		    spaAppointmentRecDao.saveSpaAppointmenRec(spaAppointmentRec);
		}
		
		//update order amount info
		if (isOrderCanceled) order.setOrderStatus(OrderStatus.CAN.getDesc());
		order.setOrderTotalAmount(orderAmount);
		order.setUpdateBy("spa_sync");
		order.setUpdateDate(curDate);
		customerOrderHdDao.update(order);
	    }
	    
	    //send reservation cancel notification
	    if (doCanceled) {
		spaAppointmentRecService.sendSmsWhenCancelSpaOrder(extInvoiceNo);
	    }
	}

    }
    
    
    private void paymentSynchronize(Date fromDate, Date toDate) throws Exception {
	
        GetPaymentsRequest request = new GetPaymentsRequest();
        request.setFromDate(fromDate);
        request.setToDate(toDate);
	GetPaymentsResponse response = spaServcieManager.getPayments(request);
	if (response == null || response.getPayments() == null || response.getPayments().size() == 0) return;
	List<Payment> payments = response.getPayments();
	Map<String, List<Payment>> groupingResult = groupingPayments(payments);
	if (groupingResult == null || groupingResult.size() == 0) return;
	
	for (Map.Entry<String, List<Payment>> entry : groupingResult.entrySet()) {
	    
	    String extInvoiceNo = entry.getKey();
	    List<Payment> transactionSet = entry.getValue();
	    
	    // when payment is not complete (without receiptNo), then ignore it
	    if (!isPaymentsComplete(transactionSet)) continue;

	    //when order is null, means appointmet in such order synchronize failed
	    //when status is cmp, means order process finished, and no need to handle again
	    CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
	    if (order == null || OrderStatus.CMP.getDesc().equals(order.getOrderStatus())) continue;
	    
	    //delete old customer order transaction info
	    List<CustomerOrderTrans> orderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(order.getOrderNo());
	    for (CustomerOrderTrans cot : orderTranses) {
		customerOrderTransDao.delete(cot);
	    }
	    
	    // add new transactions info
	    Date curDate = new Date();
	    for (Payment payment : transactionSet) {
		
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setExtRefNo(payment.getTransactionNo());
		customerOrderTrans.setCustomerOrderHd(order);
		customerOrderTrans.setTransactionTimestamp(curDate);
		BigDecimal paidAmount = new BigDecimal(StringUtils.isEmpty(payment.getPaymentAmount()) ? "0.00" : payment.getPaymentAmount());
		customerOrderTrans.setPaidAmount(paidAmount);
		customerOrderTrans.setStatus(Constant.Status.SUC.name());
		String paymentMethod = null;
		String paymentType = payment.getPaymentType();
		if (CUSTOMIZE_TYPE.equals(paymentType)) paymentType = payment.getCustomName();
		PaymentMethod method = PAYMENT_METHOD.get(paymentType);
		if (method != null) {
		    paymentMethod = method.name();
		} else {
		    paymentMethod = PaymentMethod.OTH.name();
		}
		customerOrderTrans.setPaymentMethodCode(paymentMethod);
		customerOrderTrans.setPaymentMedia("");
		customerOrderTrans.setCreateBy("spa_sync");
		customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);	    
	    }
	    
	    order.setOrderStatus(OrderStatus.CMP.getDesc());
	    order.setUpdateBy("spa_sync");
	    order.setUpdateDate(curDate);
	    customerOrderHdDao.update(order);
	    sendPaymentReceipt(order);
	}
	
    }
    
    
    private boolean isPaymentsComplete(List<Payment> transactions) {
	
	if (transactions == null || transactions.size() == 0) return false;
	for (Payment p : transactions) {
	    if (!StringUtils.isEmpty(p.getReceiptNo())) return true;
	}
	return false;
    }
    
    private String getAcademyNo(List<SpaAppointmentSynDto> items) {
	
	if (items == null || items.size() == 0) return null;
	for (SpaAppointmentSynDto dto : items) {
	    String guestCode = dto.getGuestCode();
	    if (!StringUtils.isEmpty(guestCode)) return guestCode;
	}
	return null;
    }
    
    private Date getAppointmentsLatestSynSuccessTime(Date current) {
	
	Date date = spaAppointmentRecDao.getLastestSpaSyncTime();
	if (date == null) return current;
	return date;
    }
    
    private Date getPaymentsLatestSynSuccessTime(Date current) {
	
	Date date = customerOrderTransDao.getLastestSpaSyncTime();
	if (date == null) return current;
	return date;
    }
    
    private void sendPaymentReceipt(CustomerOrderHd order) throws Exception {
	
	if (order == null) return;
	
	//send email recepit
	MMSPaymentDto dto = new MMSPaymentDto();
	dto.setOrderNo(String.valueOf(order.getOrderNo()));
	dto.setInvoiceNo(order.getVendorRefCode());
	mMSService.sentPaymentReceipt(dto, true);
	
	//send sms confirm
	List<SpaAppointmentRec> spaAppointmentRecs = spaAppointmentRecDao.getAppointmentRecsByInvoiceNo(order.getVendorRefCode());
	List<SpaAppointmentRec> appointmentList = new ArrayList<SpaAppointmentRec>();
	for (SpaAppointmentRec entity : spaAppointmentRecs) {
	    SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
	    spaAppointmentRec.setExtAppointmentId(entity.getExtAppointmentId());
	    spaAppointmentRec.setCustomerId(entity.getCustomerId());
	    spaAppointmentRec.setExtInvoiceNo(entity.getExtInvoiceNo());
	    appointmentList.add(spaAppointmentRec);
	}
	mMSService.sentMessage(appointmentList, SceneType.WELLNESS_PAYMENT_SETTLE);
    }
    
    private boolean doesCancelHapppened(List<SpaAppointmentRec> oldItems, List<SpaAppointmentSynDto> newItems) {
	
	if (newItems == null || newItems.size() == 0) return false;
	
	if (oldItems == null || oldItems.size() == 0) {
	    
	    int i = 0;
	    for (SpaAppointmentSynDto dto : newItems) if ("Cancelled".equals(dto.getStatus())) i++;
	    if (i == newItems.size()) return true;
	    
	} else {
	    
	    int i = 0;
	    int j = 0;
	    for (SpaAppointmentSynDto dto : newItems) if ("Cancelled".equals(dto.getStatus())) i++;
	    for (SpaAppointmentRec entity : oldItems) if ("CAN".equals(entity.getStatus())) j++;
	    if (i == newItems.size() && i > j) return true;
	}
	
	return false;
    }
    
    private boolean isAppointmentClosed(List<SpaAppointmentRec> oldItems) {
	
	if (oldItems == null || oldItems.size() == 0) return false;
	
	int i = 0;
	for (SpaAppointmentRec appointment : oldItems) {
	    if ("CLD".equals(appointment.getStatus())) i++;
	}
	
	return  oldItems.size() == i ? true : false;
    }

}
