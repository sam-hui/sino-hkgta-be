package com.sinodynamic.hkgta.service.common;

import java.util.List;
import java.util.Map;

import org.hibernate.type.Type;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface AdvanceQueryService extends IServiceBase {
	
	
	/**
	 *  
	 *  
	 * @param queryDto   the advance search condition ,including sort  info.
	 * @param joinSQL      HQL (not contain  advance search condition)
	 * @param page           Page object ,set your page data into it before invoke this method.
	 * @return   ResponseResult   if you there is no  special ,you can return this to front directly.  
	 */
	public ResponseResult getAdvanceQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page);
	
    /**
     * 
	 * @param queryDto        no advance search condition but including sort  info.
	 * @param joinSQL           HQL ,you can directly do search
	 * @param page                page data,set pageNumber,pageSize before you invoke this method 
	 * @return  ResponseResult			if you there is no  special ,you can return this to front directly.  
     */
    public ResponseResult getAdvanceQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto);
    
	/**
	 * for initial query ,that is you can do simple query,without writing service and writing dao.
	 *   you can reference :   CustomerMemberController.getMemberList(xx
	 *   
	 * @param queryDto        no advance search condition but including sort  info.
	 * @param joinSQL           HQL ,you can directly do search
	 * @param page                page data,set pageNumber,pageSize before you invoke this method 
	 * @return  ResponseResult			if you there is no  special ,you can return this to front directly.  
	 */
	public ResponseResult getInitialQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page);

	/**
	 *
	 * @param queryDto advance query condition mapping to this dto
	 * @param joinSQL the main query sql ,exclude advance condition
	 * @param page
	 * @return
	 */
	public ResponseResult getAdvanceQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class dto);
	/**
	 *
	 * @param queryDto advance query condition mapping to this dto
	 * @param joinSQL the main query sql ,exclude advance condition
	 * @param page
	 * @param dto      The object to transfer data.
	 * @param paramList the parameters for the joinSQL
	 * @return        ResponseResult
	 */
	public ResponseResult getAdvanceQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class dto, List<Object> paramList);
	/**
	 *
	 * @param queryDto  advance query condition mapping to this dto
	 * @param joinSQL   the main query sql ,exclude advance condition
	 * @param page
	 * @param dtoClass  after query ,mapping data to specific class
	 * @return
	 */
	public ResponseResult getAdvanceQuerySpecificResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class dtoClass);

	/**
	 *	this method can be  only invoked when  implements by database configuration
	 * @param category    every advance query ,have unique category
	 * @return   offer to frontend and this list assembled advance seach condition
	 * @throws Exception
	 */
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category) throws Exception;

	/**
	 *
	 * @param category
	 * @param advanceQueryDao
	 * @return
	 * @throws Exception
	 */
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category, AdvanceQueryConditionDao advanceQueryDao) throws Exception;

	/**
	 *
	 * @param category
	 * @return
	 * @throws Exception
	 */
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category, String type) throws Exception;

	/**
	 * get member info by physical cardNo
	 * @param cardNo
	 * @return
	 */
	public ResponseResult mappingCustomerByCardNo(Long cardNo);

	/**
	 *  get result by sql ,no advance condition contains.
	 * @param queryDto
	 * @param sql
	 * @param page
	 * @param dto
	 * @return
	 */
	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto);
	
	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto, List<Object> paramList);

	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page);

	/**
	* @param
	* @return List<AdvanceQueryConditionDto>
	* @author Vineela_Jyothi
	* Date: Jul 29, 2015
	*/
	List<AdvanceQueryConditionDto> assembleCourseQueryConditions(String courseType) throws Exception;
	
	List<AdvanceQueryConditionDto> assembleTemppassQueryConditions() throws Exception;
	
	List<AdvanceQueryConditionDto> assembleHelpPassTypeQueryConditions() throws Exception;
	
	List<AdvanceQueryConditionDto> assembleMmsOrderQueryConditions() throws Exception;
	
	List<AdvanceQueryConditionDto> assemblePermitCardQueryConditions() throws Exception;
	
	String getSearchCondition(AdvanceQueryDto queryDto,String joinSQL);
	
	/**
	* @param
	* @return List<AdvanceQueryConditionDto>
	* @author Vineela_Jyothi
	* Date: Aug 6, 2015
	*/
	public List<AdvanceQueryConditionDto> assembleCourseMembersQueryConditions()throws Exception;

	/**
	* @param
	* @return ResponseResult
	* @author Vineela_Jyothi
	* Date: Aug 7, 2015
	*/
	public ResponseResult getAdvanceQueryCustomizedResultBySQL(AdvanceQueryDto queryDto,
			String joinSQL, ListPage page, Class dto);
	
	
	public ResponseResult getInitialQueryCustomizedResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto);

	List<AdvanceQueryConditionDto> assembleRestaurantMenuQueryConditions() throws Exception;

	public ResponseResult getAdvanceQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap);

	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap);
	
	public List<AdvanceQueryConditionDto> assembleSpaRetreatConditions()throws Exception ;
	public List<AdvanceQueryConditionDto> assembleSpaRetreatItemConditions()throws Exception;
	
	public List<AdvanceQueryConditionDto> assembleTemppassHistoryQueryConditions() throws Exception;
}

