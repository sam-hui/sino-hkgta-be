package com.sinodynamic.hkgta.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.SmartDatetimeFormat;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * @author refactor by Johnmax_Wang
 * @date Apr 28, 2015
 * @param <T>
 */
public class ServiceBase<T extends Serializable> implements IServiceBase<T>
{
	protected final Logger logger = Logger.getLogger(ServiceBase.class);
	protected CommUtil uc = new CommUtil();

	@Resource(name = "appProperties")
	protected Properties appProps;

	@Resource(name = "messageSource")
	protected MessageSource messageSource;

	@Autowired
	protected ResponseMsg responseMsg;

	@Autowired
	protected ResponseResult responseResult;

	/*@Autowired
	private HttpServletRequest request;*/


	
	
	/**
				 * To customize the format for date parameter within this controller. 
				 * To avoid error when passing empty date string parameter when submit from view page  
				 * @param binder
				 */
	@InitBinder public void initBinder(WebDataBinder binder)
	{
		SimpleDateFormat dateFormat = new SmartDatetimeFormat(appProps.getProperty("format.datetime"));
		dateFormat.setLenient(false);
		// true passed to CustomDateEditor constructor means convert empty
		// String to null
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * get i18n message
	 * 
	 * @param msgCode
	 * @return
	 */
	public String getI18nMessge(String msgCode)
	{
		return this.getI18nMessge(msgCode, null, LocaleContextHolder.getLocale());
	}

	/**
	 * get i18 message with parameters, just like {0} {1}
	 * 
	 * @param msgCode
	 * @param args
	 * @return
	 */
	public String getI18nMessge(String msgCode, Object[] args)
	{
		return this.getI18nMessge(msgCode, args, LocaleContextHolder.getLocale());
	}

	public String getI18nMessge(String msgCode, Locale locale)
	{
		return this.getI18nMessge(msgCode, null, locale);
	}

	public String getI18nMessge(String msgCode, Object[] args, Locale locale)
	{
		return messageSource.getMessage(msgCode, args, locale);
	}
}
