package com.sinodynamic.hkgta.service.crm.sales.presentation;

import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface PresentMaterialService extends IServiceBase<PresentMaterial> {
	PresentMaterial getById(Long materialId);
	void save();
}
