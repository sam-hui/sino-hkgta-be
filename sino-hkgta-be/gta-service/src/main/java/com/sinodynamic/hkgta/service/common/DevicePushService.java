package com.sinodynamic.hkgta.service.common;

public interface DevicePushService {

	public void pushRegister(String userId, String deviceArn, String token, String platform, String application, String version, String updatedBy);

	public int deleteUserDevice(String userId,String arnApplication);
	
	public void pushMessage(String[] userIds,String message, String application);
	
	public void pushMessage(String[] userIds, String functionId, String[] replaceParams, String application);

	public void pushMessage(String[] userIds,String subject, String message, String application);
	
	public void broadcastMessage(String topicArn, String message);
}