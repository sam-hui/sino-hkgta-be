package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.RoomDetailDto;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RoomService extends IServiceBase<Room> {

	String DEFAULT_PATH_PREFIX_KEY = "image.server.material";
	
	@Deprecated
	public List<RoomDto> getRoomList(String updateBy) throws Exception;

	public List<RoomDto> getRoomListWithTaskCounts(String updateBy) throws Exception;

	public RoomDetailDto getRoomDetail(Long roomId) throws Exception;
	
	public ResponseResult changeRoomStatus(Long roomId,String status,String userId) throws Exception;
	
	public List<Room> getRoomListByStatus(String[] status) throws Exception;
	
	public void updateRoom(Room room);
	
	public List<RoomDto> getRoomCrewList(String crewRoleId);
	
	public ResponseResult getRoomTaskList(String userId,Boolean isMyRoom,String roomNo);
	
	public MessageResult updateRoomStatus(UpdateRoomStatusDto dto) throws Exception;

	public ResponseResult pushRegister(RegisterDto dto,String userId);
	
	public List<Room> getRoomList();
	
	public void staffPushRegister(String staffId, String deviceArn, String token, String platform, String version, String updatedBy);
	
	public List<Room> getLimitedInfoRoomList();
	
}
