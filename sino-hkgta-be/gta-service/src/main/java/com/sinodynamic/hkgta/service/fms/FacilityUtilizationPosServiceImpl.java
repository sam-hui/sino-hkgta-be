package com.sinodynamic.hkgta.service.fms;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationPosDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationPosDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationPos;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class FacilityUtilizationPosServiceImpl extends ServiceBase<FacilityUtilizationPos> implements FacilityUtilizationPosService {

	@Autowired
	private FacilityUtilizationPosDao facilityUtilizationPosDao;

	@Autowired
	private GlobalParameterDao globalParameterDao;

	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityUtilizationPos> getFacilityUtilizationPosList(String facilityType) {
		return facilityUtilizationPosDao.getFacilityUtilizationPosList(facilityType);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveFacilityUtilizationPos(FacilityUtilizationPosDto facilityUtilizationPosDto) {
		String facTypeConst = Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityUtilizationPosDto.getFacilityType()) ? Constant.ADVANCEDRESPERIOD_GOLF : Constant.ADVANCEDRESPERIOD_TENNIS;
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, facTypeConst);
		if (globalParameter != null) {
			globalParameter.setParamValue(facilityUtilizationPosDto.getAdvancedResPeriod().toString());
			globalParameterDao.saveOrUpdate(globalParameter);
		}else{
			globalParameter = new GlobalParameter();
			globalParameter.setParamId(facTypeConst);
			globalParameter.setParamValue(facilityUtilizationPosDto.getAdvancedResPeriod().toString());
			globalParameter.setCreateBy(facilityUtilizationPosDto.getUpdateBy());
			globalParameter.setCreateDate(new Timestamp(new Date().getTime()));
			globalParameter.setStatus(Constant.General_Status_ACT);
			globalParameter.setParamCat("BR");
			globalParameterDao.save(globalParameter);
		}

		List<FacilityUtilizationPosDto> facilityUtilizationPosDtos = facilityUtilizationPosDto.getPriceList();
		for (FacilityUtilizationPosDto posDto : facilityUtilizationPosDtos) {
			FacilityUtilizationPos facilityUtilizationPos = facilityUtilizationPosDao.getFacilityUtilizationPos(facilityUtilizationPosDto.getFacilityType(), posDto.getAgeRangeCode(), posDto.getRateType());
			/*if (null != posDto.getFacilityPosId() && posDto.getFacilityPosId() > 0) {
				facilityUtilizationPos = facilityUtilizationPosDao.get(FacilityUtilizationPos.class, posDto.getFacilityPosId());
			}*/
			if (null != facilityUtilizationPos) {
				PosServiceItemPrice price = posServiceItemPriceDao.get(PosServiceItemPrice.class, facilityUtilizationPos.getPosItemNo());
				if (null != price) {
					price.setItemPrice(posDto.getItemPrice());
					posServiceItemPriceDao.update(price);
				}else{
					price = new PosServiceItemPrice();
					price.setItemNo(Constant.FACILITY_PRICE_PREFIX + addZeroForNum("" + facilityUtilizationPos.getFacilityPosId(), 8));
					price.setItemCatagory(Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityUtilizationPosDto.getFacilityType())?"GFBK":"TFBK");
					price.setStatus(Constant.General_Status_ACT);
					price.setItemPrice(posDto.getItemPrice());
					posServiceItemPriceDao.save(price);
				}
			}
			else {
				PosServiceItemPrice price = posServiceItemPriceDao.get(PosServiceItemPrice.class, Constant.FACILITY_PRICE_PREFIX + addZeroForNum("" + (facilityUtilizationPosDao.getMaxFacilityUtilizationPosId()+1), 8));
				if(null == price){
					price = new PosServiceItemPrice();
					price.setItemNo(Constant.FACILITY_PRICE_PREFIX + addZeroForNum("" + (facilityUtilizationPosDao.getMaxFacilityUtilizationPosId()+1), 8));
					price.setItemCatagory(Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityUtilizationPosDto.getFacilityType())?"GFBK":"TFBK");
					price.setStatus(Constant.General_Status_ACT);
				}
				price.setItemPrice(posDto.getItemPrice());
				posServiceItemPriceDao.save(price);
				
				facilityUtilizationPos = new FacilityUtilizationPos();
				facilityUtilizationPos.setPosItemNo(price.getItemNo());
				facilityUtilizationPos.setAgeRangeCode(posDto.getAgeRangeCode());
				facilityUtilizationPos.setRateType(posDto.getRateType());
				facilityUtilizationPos.setFacilityType(facilityUtilizationPosDto.getFacilityType());
			}
			if(null ==facilityUtilizationPos.getCreateBy()){
				facilityUtilizationPos.setCreateBy(facilityUtilizationPosDto.getUpdateBy());
				facilityUtilizationPos.setCreateDate(new Timestamp(new Date().getTime()));
			}
			facilityUtilizationPos.setUpdateDate(new Date());
			facilityUtilizationPos.setUpdateBy(facilityUtilizationPosDto.getUpdateBy());
			facilityUtilizationPosDao.save(facilityUtilizationPos);
		}
	}

	public static String addZeroForNum(String str, int strLength) {
		int strLen = str.length();
		if (strLen < strLength) {
			while (strLen < strLength) {
				StringBuffer sb = new StringBuffer();
				sb.append("0").append(str);
				str = sb.toString();
				strLen = str.length();
			}
		}
		return str;
	}

}
