package com.sinodynamic.hkgta.service.fms;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.dto.membership.FacilityTransactionInfo;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberFacilityTypeBookingService extends IServiceBase<MemberFacilityTypeBooking> {
	
	public MemberFacilityTypeBooking getMemberFacilityTypeBooking(long resvId);
	
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingIncludeAllStatus(long resvId);
	
	public MemberFacilityBookingDto createMemberFacilityTypeBooking(MemberFacilityBookingDto memberFacilityBookingDto,boolean isMemberApp,List<Long> facilityNos)  throws Exception;
	
	public MemberFacilityBookingDto getMemberFacilityTypeBookingPrice(MemberFacilityBookingDto memberFacilityBookingDto,boolean isMemberApp)  throws Exception; 
	
	public Long finalizeMemberFacilityTypeBooking(Long transactionNo,String updateBy, String responseCode, String traceNumber, String remark, String cardType);
	
	public Long voidMemberFacilityTypeBooking(Long orderNo,String userId);
	
	public boolean cancelMemberFacilityTypeBooking(long resvId, String userId,String remark,String requesterType, String createRefundRequest);
	
	public boolean cancelMemberFacilityTypeBookingCheck(long resvId);

	public String getAllMemberFacilityTypeBooking(String facilityType, String dateRange, String customerId,boolean isPushMessage) throws Exception;
	
	public List<FacilityReservationDto> getAllTodayMemberFacilityTypeBooking(boolean isPushMessage);
	
	public String  getPrivateCoachBookList(String status, Integer show,String facilityType, String customerId);
	
	public MemberFacilityBookingDto doPay(Map<String,Object> map) throws Exception;
	
	public Map<String,Object> saveReservationOrder(MemberFacilityBookingDto bookingDto) throws Exception;
	
	public ResponseResult doCancel(Long resvId, String userId,String remark,String requesterType,boolean refundFlag);
	
	public boolean getRightByCustomerIdAndFacilityType(Long customerId, String facilityType);
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingByOrderNo(Long orderNo);

	public void sendPrivateCoachingReservationEmail(long resvId,String userId);
	
	public ResponseResult getBookingRecordsByCustomerId(ListPage<MemberFacilityTypeBooking> page,Long customerId,String appType,String year,String month,String day,String selectedStartDate,String selectedEndDate)throws Exception;

	public boolean sendCustomerRefundRequest(CustomerOrderHd customerOrderHd, String refundServiceType,String customerReason,
			String requesterType, String userId,String status);

	/**   
	* @author: Zero_Wang
	 * @throws Exception 
	* @since: Aug 4, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	public ResponseResult preCancel(Long resvId,String type) throws Exception;
	
	public Member checkMemberStatus(Long customerId);
	
	public boolean checkRefundDate(Timestamp beginDatetimeBook,String facilityType,String type);
	
	public FacilityTransactionInfo getFacilityTransactionInfo(long resvId);

	/**   
	* @author: Zero_Wang
	* @since: Sep 8, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	public MemberFacilityTypeBooking getCancelBooking(Long resvId);

	public int getbookingTimes(MemberFacilityBookingDto booking);

	public MemberFacilityBookingDto saveAndPayBooking(
			MemberFacilityBookingDto dto)  throws Exception;

	public void update(MemberFacilityTypeBooking booking);
	
	public int getMemberFacilityBookedCountForQuota(String facilityType,Date bookingDateTime)throws Exception;
	
	public void sendRefundRequestEmail(CustomerOrderHd customerOrderHd,
			String refundServiceType,Date refundDate);
	
	public ResponseResult getDailyMonthlyFacilityUsage(String timePeriodType,ListPage page, String selectedDate, String facilityType);
	
	public byte[] getDailyFacilityUsageAttach(String timePeriodType,String selectedDate, String fileType, String sortBy, String isAscending, String facilityType);

	public Long getresvIdByFacilityTimeslotId(long facilityTimeslotId,long customerId);

	public ResponseResult getDailyMonthlyPrivateCoaching(String timePeriodType,ListPage page, String selectedDate, String facilityType);

	public byte[] getDailyMonthlyPrivateCoachingAttach(String timePeriodType,String selectedDate, String fileType, String sortBy, String isAscending, String facilityType);
}
