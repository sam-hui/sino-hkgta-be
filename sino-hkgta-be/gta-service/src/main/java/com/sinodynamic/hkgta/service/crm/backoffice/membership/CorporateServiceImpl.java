package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CorporateAdditionAddressDao;
import com.sinodynamic.hkgta.dao.crm.CorporateAdditionContactDao;
import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateProfileDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAddressDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollPoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentLogDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.VirtualAccPoolDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.CorporateAdditionContactDto;
import com.sinodynamic.hkgta.dto.crm.CorporateMemberDto;
import com.sinodynamic.hkgta.dto.crm.CorporateProfileDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.DropDownDto;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.entity.crm.CorporateAdditionAddress;
import com.sinodynamic.hkgta.entity.crm.CorporateAdditionContact;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CorporateProfile;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceSubscribePK;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;
import com.sinodynamic.hkgta.entity.crm.CustomerAddressPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPo;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollmentLog;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.crm.VirtualAccPool;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.notification.SMSService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.AddressType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.constant.ServicePlanRightTypeStatus;
//import com.sinodynamic.hkgta.util.exception.CommonException;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
@Scope("prototype")
public class CorporateServiceImpl extends ServiceBase<CorporateProfile> implements CorporateService{
	
	@Autowired
	private CorporateProfileDao corporateProfileDao;
	
	@Autowired
	private CorporateServiceAccDao corporateServiceAccDao;
	
	@Autowired
	private CorporateServiceSubscribeDao corpSerSubDao;
	
	@Autowired
	private CorporateAdditionAddressDao corpAdditionAddressDao;
	
	@Autowired
	private ServicePlanDao servicePlanDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerAddressDao customerAddressDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CorporateMemberDao corporateMemberDao;
	
	@Autowired
	private UserMasterDao userMasterDao;
	
	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;
	
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private VirtualAccPoolDao virtualAccPoolDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private SMSService smsService;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired 
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao; 
	
	@Autowired
	private UserMasterService userMasterService;
	
	@Autowired
	private CorporateAdditionContactDao corporateAdditionContactDao;
	
	@Autowired
	private CustomerEnrollPoDao customerEnrollPoDao;

	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao cOrderDetDao;
	
	@Autowired
	private CustomerEnrollmentLogDao customerEnrollmentLogDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private SpaMemberSyncDao spaMemberSyncDao;
	
	@Autowired
	private CustomerServiceDao customerServiceDao;
	
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult editCorporateAcc(CorporateProfileDto corporateProfileDto) throws Exception {
		boolean isUpdate = false;
		String loginUserId = corporateProfileDto.getLoginUserId();
		Date currentDate = new Date();
		Long corporateId = corporateProfileDto.getCorporateId();
		CorporateProfile corporateProfile = corporateProfileDao.getByCorporateId(corporateId);
//		isUpdate = compareProfile(corporateProfileDto, corporateProfile);

		if (corporateProfile == null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_PROFILE_NOT_FOUND);
			return responseResult;
		}

		boolean status_company_name = corporateProfileDao.checkAvailableCompanyName(corporateProfileDto.getCompanyName(), corporateId);
		if (!status_company_name) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_NAME_REGISTERED);
			return responseResult;
		}

		boolean status = corporateProfileDao.checkAvailableBrNo(corporateProfileDto.getBrNo(), corporateId);
		if (!status) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_BRNO_REGISTERED);
			return responseResult;
		}

		BigDecimal creditLimit = corporateProfileDto.getCreditLimit();
		int check = creditLimit.compareTo(new BigDecimal(99999999.99).setScale(2, BigDecimal.ROUND_HALF_UP));
		if (!StringUtils.isEmpty(creditLimit) && check == 1) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_CR_LIMIT_INVALID);
			return responseResult;
		}

		BigDecimal allocatedCreditAmount = corporateMemberDao.getAllocatedCreditAmountByCorporateId(corporateId);
		if (creditLimit.compareTo(allocatedCreditAmount) < 0) {
			Object[] object = new Object[] { allocatedCreditAmount.setScale(2, BigDecimal.ROUND_HALF_UP) };
			responseResult.initResult(GTAError.CorporateManagementError.CORP_CR_LIMIT_ABOVE, object);
			return responseResult;
		}

		if (!StringUtils.isEmpty(corporateProfileDto.getContactPhone()) && !CommUtil.validatePhoneNo(corporateProfileDto.getContactPhone())) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_PHONE_INVALID);
			return responseResult;
		}
		
		Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateId);
		if (accNo == null) {
			throw new GTACommonException(GTAError.CorporateManagementError.CORP_SERVICE_ACC_NOT_FOUND);
		}
		
		String[] ignoreFiledsForCorporateProfile= new String[]{"version","createDate","createBy","status"};
		BeanUtils.copyProperties(corporateProfileDto, corporateProfile,ignoreFiledsForCorporateProfile);
		corporateProfileDao.getCurrentSession().evict(corporateProfile);
		corporateProfile.setUpdateBy(loginUserId);
		corporateProfile.setUpdateDate(currentDate);
		corporateProfileDao.update(corporateProfile);
		
		CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
		corporateServiceAcc.setRemark(corporateProfileDto.getRemark());
		corporateServiceAcc.setTotalCreditLimit(corporateProfileDto.getCreditLimit());
		corporateServiceAcc.setContractRefNo(corporateProfileDto.getContractRefNo());
		corporateServiceAccDao.update(corporateServiceAcc);

		List<CorporateAdditionContactDto> corporateAdditionContactDtos = corporateProfileDto.getCorporateAdditionContactDtos();
		for (CorporateAdditionContactDto tempContactDto : corporateAdditionContactDtos) {
			CorporateAdditionContact corporateAdditionContact = corporateAdditionContactDao.getByContactId(tempContactDto.getContactId());
			if (corporateAdditionContact == null) {
				corporateAdditionContact = new CorporateAdditionContact();	
				if(tempContactDto.getIsSameAsInCharge()){
					corporateAdditionContact.setContactType(tempContactDto.getContactType());
					corporateAdditionContact.setContactPersonFirstname(corporateProfileDto.getContactPersonFirstname());
					corporateAdditionContact.setContactPersonLastname(corporateProfileDto.getContactPersonLastname());
					corporateAdditionContact.setContactPhone(corporateProfileDto.getContactPhone());
					corporateAdditionContact.setContactPhoneMobile(corporateProfileDto.getContactPhoneMobile());
					corporateAdditionContact.setContactEmail(corporateProfileDto.getContactEmail());
					corporateAdditionContact.setPostalAddress1(corporateProfileDto.getAddress1());
					corporateAdditionContact.setPostalAddress2(corporateProfileDto.getAddress2());
					corporateAdditionContact.setDistrict(corporateProfileDto.getDistrict());
				}else{
					BeanUtils.copyProperties(tempContactDto, corporateAdditionContact);
				}
				corporateAdditionContact.setCreateBy(loginUserId);
				corporateAdditionContact.setCreateDate(currentDate);
				corporateAdditionContact.setCorporateId(corporateId);
				corporateAdditionContactDao.save(corporateAdditionContact);
			} else {
				if(tempContactDto.getIsSameAsInCharge()){
					corporateAdditionContact.setContactPersonFirstname(corporateProfileDto.getContactPersonFirstname());
					corporateAdditionContact.setContactPersonLastname(corporateProfileDto.getContactPersonLastname());
					corporateAdditionContact.setContactPhone(corporateProfileDto.getContactPhone());
					corporateAdditionContact.setContactPhoneMobile(corporateProfileDto.getContactPhoneMobile());
					corporateAdditionContact.setContactEmail(corporateProfileDto.getContactEmail());
					corporateAdditionContact.setPostalAddress1(corporateProfileDto.getAddress1());
					corporateAdditionContact.setPostalAddress2(corporateProfileDto.getAddress2());
					corporateAdditionContact.setDistrict(corporateProfileDto.getDistrict());
				}else{
					String[] ignoreFiledsForCorporateAdditionContact= new String[]{"version","createDate","createBy"};
					BeanUtils.copyProperties(tempContactDto, corporateAdditionContact,ignoreFiledsForCorporateAdditionContact);
				}
				
				corporateAdditionContactDao.getCurrentSession().evict(corporateAdditionContact);
				corporateAdditionContact.setUpdateBy(loginUserId);
				corporateAdditionContact.setUpdateDate(currentDate);
				corporateAdditionContactDao.update(corporateAdditionContact);
			}
		}

		// if (isUpdate &&
		// Constant.Status.ACT.name().equalsIgnoreCase(corporateProfile.getStatus()))
		// {
		// try {
		// CustomerEmailContent customerEmailContent =
		// assembleCorporateAccountChangedEmail(loginUserId,
		// corporateProfileDto.getLoginUserName(),
		// EmailType.UPDATE_CORPORATE_PROFILE_EMAIL.getFunctionId(),
		// corporateProfile);
		// mailThreadService.sendWithResponse(customerEmailContent, null, null,
		// null);
		// } catch (Exception e) {
		// logger.debug("Send SMS failed!", e);
		// }
		// }

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	
	/**
	 * Judge whether the  corporateProfile is updated. If the  corporateProfile is updated return true, else return false.
	 * @param corporateProfileDto
	 * @param corporateProfile
	 * @return isUpdate.
	 * 
	 */
	@Transactional
	private boolean  compareProfile(CorporateProfileDto corporateProfileDto, CorporateProfile corporateProfile) {
		boolean isUpdate = false;
		
		Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateProfileDto.getCorporateId());
		if(accNo == null) {
			throw new GTACommonException(GTAError.CorporateManagementError.CORP_SERVICE_ACC_NOT_FOUND);
		}
		CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
		if (0 != corporateProfileDto.getCreditLimit().compareTo(corporateServiceAcc.getTotalCreditLimit())) {//Update credit limit
			return true;
		}
		if (corporateProfileDto.getContractRefNo() != null && !corporateProfileDto.getContractRefNo().equals(corporateServiceAcc.getContractRefNo())) {//Update Contract Reference 
			return true;
		}
		if (!compare(corporateServiceAcc.getRemark(),corporateProfileDto.getRemark())) {
			return true;
		}
		CorporateProfile sourceCorporateProfile = new CorporateProfile();
		sourceCorporateProfile.setAddress1(corporateProfileDto.getAddress1());
		sourceCorporateProfile.setAddress2(corporateProfileDto.getAddress2());
		sourceCorporateProfile.setBrNo(corporateProfileDto.getBrNo());
		sourceCorporateProfile.setBusinessNatureCode(corporateProfileDto.getBusinessNatureCode());
		sourceCorporateProfile.setCompanyName(corporateProfileDto.getCompanyName());
		sourceCorporateProfile.setCompanyNameNls(corporateProfileDto.getCompanyNameNls());
		sourceCorporateProfile.setContactEmail(corporateProfileDto.getContactEmail());
		sourceCorporateProfile.setContactPhone(corporateProfileDto.getContactPhone());
		
		CorporateAdditionAddress corporateAdditionAddress = new CorporateAdditionAddress();
		corporateAdditionAddress.setAddressType("BILL");
		corporateAdditionAddress.setAddress1(corporateProfileDto.getBillingAddress1());
		corporateAdditionAddress.setAddress2(corporateProfileDto.getBillingAddress2());
		corporateAdditionAddress.setHkDistrict(corporateProfileDto.getBillingHKDistrict());
		sourceCorporateProfile.setCorporateAdditionAddress(corporateAdditionAddress);
		sourceCorporateProfile.setDistrict(corporateProfileDto.getDistrict());
		
		if (!sourceCorporateProfile.equals(corporateProfile)) {//Profile is updated
			isUpdate = true;
		}
		return isUpdate;
	}
	public boolean compare(Object source,Object target){
		if (null != source) {
			if(!source.equals(target)){
				return false;
			}
		}else {
			if (null != target){
				return false;
			}
		}
		return true;
	}
	@Transactional
	public ResponseResult createCorporateAcc(CorporateProfileDto corporateProfileDto) {
		String loginUserId = corporateProfileDto.getLoginUserId();
		//CORPORATE Profile
		CorporateProfile corporateProfile = new CorporateProfile();
		if(corporateProfileDto.getBrNo()==null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_BRNO_REQ);
			return responseResult;
		}
		
		boolean status_company_name = corporateProfileDao.checkAvailableCompanyName(corporateProfileDto.getCompanyName());
		if(!status_company_name) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_NAME_REGISTERED_IN_CREATE);
			return responseResult;
		}
		
		boolean status = corporateProfileDao.checkAvailableBRNo(corporateProfileDto.getBrNo());
		if(!status) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_BRNO_REGISTERED_IN_CREATE);
			return responseResult;
		}
	
		BigDecimal creditLimit = corporateProfileDto.getCreditLimit();
		int check = creditLimit.compareTo(new BigDecimal(99999999.99).setScale(2,BigDecimal.ROUND_HALF_UP));
		if(!StringUtils.isEmpty(creditLimit)&&check==1) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_CR_LIMIT_INVALID_IN_CREATE);
			return responseResult;
		}
		
		if (!StringUtils.isEmpty(corporateProfileDto.getContactPhone()) && !CommUtil.validatePhoneNo(corporateProfileDto.getContactPhone())) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_PHONE_INVALID_IN_CREATE);
			return responseResult;
		}
		
		BeanUtils.copyProperties(corporateProfileDto, corporateProfile);
		corporateProfile.setStatus("ACT");
		corporateProfile.setCreateDate(new Date());
		corporateProfile.setCreateBy(loginUserId);
		Long corporateId = (Long) corporateProfileDao.save(corporateProfile);
		
		List<CorporateAdditionContactDto> contactDtos = corporateProfileDto.getCorporateAdditionContactDtos();
		if(contactDtos !=null){
			for(CorporateAdditionContactDto tempContactDto: contactDtos){
				CorporateAdditionContact corporateAdditionContact = new CorporateAdditionContact();
				BeanUtils.copyProperties(tempContactDto, corporateAdditionContact);
				corporateAdditionContact.setCorporateId(corporateId);
				if(tempContactDto.getIsSameAsInCharge()){
					corporateAdditionContact.setContactPersonFirstname(corporateProfileDto.getContactPersonFirstname());
					corporateAdditionContact.setContactPersonLastname(corporateProfileDto.getContactPersonLastname());
					corporateAdditionContact.setContactPhone(corporateProfileDto.getContactPhone());
					corporateAdditionContact.setContactPhoneMobile(corporateProfileDto.getContactPhoneMobile());
					corporateAdditionContact.setContactEmail(corporateProfileDto.getContactEmail());
					corporateAdditionContact.setPostalAddress1(corporateProfileDto.getAddress1());
					corporateAdditionContact.setPostalAddress2(corporateProfileDto.getAddress2());
					corporateAdditionContact.setDistrict(corporateProfileDto.getDistrict());
				}
				
				corporateAdditionContact.setCreateBy(loginUserId);
				corporateAdditionContact.setCreateDate(new Date());
				corporateAdditionContactDao.save(corporateAdditionContact);
			}
		}
		
		//CORPORATE SERVICE ACC
		CorporateServiceAcc corpSerAcc = new CorporateServiceAcc();
		corpSerAcc.setCorporateId(corporateId);
		corpSerAcc.setAccCat("COP");
		corpSerAcc.setStatus("ACT");
		corpSerAcc.setRemark(corporateProfileDto.getRemark());
		corpSerAcc.setTotalCreditLimit(corporateProfileDto.getCreditLimit());
		corpSerAcc.setStatementDeliveryBy("EMAIL");
		corpSerAcc.setResetCreditPeriod("YEAR");
		corpSerAcc.setContractRefNo(corporateProfileDto.getContractRefNo());
		corpSerAcc.setFollowByStaff(loginUserId);
		corporateServiceAccDao.save(corpSerAcc);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult viewCorporateAcc(Long corporateId){
		
		CorporateProfile corporateProfile = corporateProfileDao.getByCorporateId(corporateId);
		if(corporateProfile==null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_PROFILE_NOT_FOUND_IN_VIEW);
			return responseResult;
		}
		CorporateProfileDto corporateProfileDto = new CorporateProfileDto();
		BeanUtils.copyProperties(corporateProfile,corporateProfileDto);
		
		Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateId);
		if(accNo == null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_SERVICE_ACC_NOT_FOUND_IN_VIEW);
			return responseResult;
		}
		CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
		if(corporateServiceAcc==null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_NO_SERVICE_ACC);
			return responseResult;
		}
		corporateProfileDto.setContractRefNo(corporateServiceAcc.getContractRefNo());
		corporateProfileDto.setCreditLimit(corporateServiceAcc.getTotalCreditLimit());
		corporateProfileDto.setRemark(corporateServiceAcc.getRemark());
		
		
		List<CorporateAdditionContact> corporateAdditionContacts = corporateAdditionContactDao.getCorporateAdditionContactByCorporateId(corporateId);
		List<CorporateAdditionContactDto> corporateAdditionContactDtos = new ArrayList<CorporateAdditionContactDto>();
		for(CorporateAdditionContact tempContact: corporateAdditionContacts){
			CorporateAdditionContactDto tempContactDto = new CorporateAdditionContactDto();
			BeanUtils.copyProperties(tempContact,tempContactDto);
			Boolean isSameAsInCharge = tempContact.equals(corporateProfile);
			tempContactDto.setIsSameAsInCharge(isSameAsInCharge);
			corporateAdditionContactDtos.add(tempContactDto);
		}
		corporateProfileDto.setCorporateAdditionContactDtos(corporateAdditionContactDtos);
		responseResult.initResult(GTAError.Success.SUCCESS,corporateProfileDto);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult deactivateCorporateAcc(Long corporateId,String loginUserId,String status,Long accNo,String userName, Integer corporateProfileVersion, Integer corporateServiceAccVersion){
		CorporateProfile corporateProfile = corporateProfileDao.getByCorporateId(corporateId);
		if(corporateProfile==null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_PROFILE_NOT_FOUND_IN_DEACTIVATE);
			return responseResult;
		}
		if(corporateProfile.getStatus().equalsIgnoreCase("NACT")&&status.equals("NACT")) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_BEEN_DEACTIVATED);
			return responseResult;
		}
		if(corporateProfile.getStatus().equalsIgnoreCase("ACT")&&status.equals("ACT")) {
			responseResult.initResult(GTAError.CorporateManagementError.CORP_BEEN_ACTIVATED);
			return responseResult;
		}
		
		corporateProfile.setStatus(status);
		corporateProfile.setUpdateDate(new Date());
		corporateProfile.setUpdateBy(loginUserId);
//		corporateProfile.setVersion(new Long(corporateProfileVersion));
		corporateProfileDao.update(corporateProfile);
		CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
		if(corporateServiceAcc==null) {
			throw new GTACommonException(GTAError.CorporateManagementError.CORP_SERVICE_ACC_NOT_FOUND_IN_DEACTIVATE);
		}
		corporateServiceAcc.setStatus(status);
//		corporateServiceAcc.setVersion(new Long(corporateServiceAccVersion));
		corporateServiceAccDao.update(corporateServiceAcc);
		if (status.equals("NACT")) {//Only when deactivate corporate account's status, update the member's status of this account.
			List<CorporateMember> listCorporateMembers = corporateMemberDao.getAllCorporateMember(corporateId);
			for(CorporateMember corporateMember: listCorporateMembers){
				Long tempCustomerId = corporateMember.getCustomerId();
				corporateMemberDao.updateStatus(status, tempCustomerId,loginUserId);
				memberDao.updateStatus(status, tempCustomerId,loginUserId);
				Member primaryMember = memberDao.get(Member.class, tempCustomerId);
				UserMaster userMaster = userMasterDao.get(UserMaster.class, primaryMember.getUserId());
				userMaster.setStatus(Constant.Status.NACT.name());
				userMaster.setUpdateBy(loginUserId);
				userMaster.setUpdateDate(new Date());
				userMasterDao.update(userMaster);
				List<Member> tempMembers = memberDao.getListMemberBySuperiorId(tempCustomerId);
				//Discussed with Nicky and Moby, It does not need send SMS to member when member's Status and Credit Limit be changed.
				/*try {
					MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("update_status");
					String content = mt.getContent();
					if (null == content) {
						content = Constant.UPDATE_CUSTOMER_STATUS_SMS_TEMPLETE;
					}
					smsService.sendSMS(Arrays.asList(customerProfile.getPhoneMobile()), content, new Date());
				} catch (Exception e) {
					logger.debug("Send SMS failed!", e);
				}*/
				for(Member member: tempMembers){
					Long tempDepCustomerId = member.getCustomerId();
					memberDao.updateStatus(status, tempDepCustomerId,loginUserId);
					UserMaster denUserMaster = userMasterDao.get(UserMaster.class, member.getUserId());
					denUserMaster.setStatus(Constant.Status.NACT.name());
					denUserMaster.setUpdateBy(loginUserId);
					denUserMaster.setUpdateDate(new Date());
					userMasterDao.update(denUserMaster);
				}
				
			}
		}
		
		CustomerEmailContent customerEmailContent = assembleCorporateAccountChangedEmail(loginUserId, userName, EmailType.UPDATE_CORPORATE_STATUS_EMAIL.getFunctionId(), corporateProfile);
		mailThreadService.sendWithResponse(customerEmailContent, null, null, null);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult changeMemberStatus(Long customerId, String loginUserId,String status,String fromName){
		Member member = memberDao.getMemberById(customerId);
		if(member == null) {
			responseResult.initResult(GTAError.CorporateManagementError.MEMBER_NOT_FOUND);
			return responseResult;
		}
		String memberType = member.getMemberType();
		Long checkEnrollCustomerId =customerId;
		if(MemberType.IDM.name().equals(memberType)||MemberType.CDM.name().equals(memberType)){
			checkEnrollCustomerId = member.getSuperiorMemberId();
		}
		CustomerServiceAcc customerServiceAcc = customerServiceDao.getCurrentActiveAcc(checkEnrollCustomerId);
		if (null == customerServiceAcc && "ACT".equalsIgnoreCase(status) && member.getStatus().equalsIgnoreCase("NACT")) {
			responseResult.initResult(GTAError.CorporateManagementError.NO_ACTIVE_SERVICE_ACC);
			return responseResult;
		}
		CustomerEnrollment primaryEnroll = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(checkEnrollCustomerId);
		if(primaryEnroll!=null&&EnrollStatus.OPN.name().equals(primaryEnroll.getStatus())
				||EnrollStatus.NEW.name().equals(primaryEnroll.getStatus())||EnrollStatus.APV.name().equals(primaryEnroll.getStatus())){
			responseResult.initResult(GTAError.CorporateManagementError.ACTIVATE_PM_FIRST);
			return responseResult;
		}
		
		if(member.getStatus().equalsIgnoreCase("NACT")&&status.equals("NACT")) {
			responseResult.initResult(GTAError.CorporateManagementError.PM_BEEN_DEACTIVATED);
			return responseResult;
		}
		if(member.getStatus().equalsIgnoreCase("ACT")&&status.equals("ACT")) {
			responseResult.initResult(GTAError.CorporateManagementError.PM_BEEN_ACTIVATED);
			return responseResult;
		}
		
		
		if(memberType.equals(MemberType.CPM.name())){
			CorporateMember corporateMember = corporateMemberDao.get(CorporateMember.class, customerId);
			if(corporateMember == null) {
				responseResult.initResult(GTAError.CorporateManagementError.CORP_MEMBER_NOT_FOUND);
				return responseResult;
			}
			CorporateProfile corporateProfile = corporateProfileDao.getByCorporateId(corporateMember.getCorporateProfile().getCorporateId());
			
			if (corporateProfile != null && !corporateProfile.getStatus().equalsIgnoreCase(status) && status.equalsIgnoreCase("ACT")) {
				responseResult.initResult(GTAError.CorporateManagementError.CORP_ACC_INACTIVE);
				return responseResult;
			}
			corporateMemberDao.updateStatus(status, corporateMember.getCustomerId(),loginUserId);
		}
		if(memberType.equals(MemberType.IDM.name())||memberType.equals(MemberType.CDM.name())){
			Long superiorID = member.getSuperiorMemberId();
			Member superiorMember = memberDao.getMemberById(superiorID);
			if (superiorMember != null && !superiorMember.getStatus().equalsIgnoreCase(status) && status.equalsIgnoreCase("ACT")) {
				responseResult.initResult(GTAError.CorporateManagementError.PM_INACTIVE);
				return responseResult;
			}
		}
		boolean isSuccess = memberDao.updateStatus(status, customerId, loginUserId);
		if (isSuccess) {
			UserMaster userMaster = userMasterDao.get(UserMaster.class, member.getUserId());
			userMaster.setStatus(status);
			userMaster.setUpdateBy(loginUserId);
			userMaster.setUpdateDate(new Date());
			userMasterDao.update(userMaster);
		}
		
		//If active IPM or CPM, their dependent member need not to be active automatically, it should be active manually.
		if((memberType.equals(MemberType.CPM.name())||memberType.equals(MemberType.IPM.name())) && status.equals("NACT")){ 
			List<Member> tempMembers = memberDao.getListMemberBySuperiorId(customerId);
			for(Member denMember: tempMembers){
				Long tempCustomerId = denMember.getCustomerId();
				boolean success = memberDao.updateStatus(status, tempCustomerId,loginUserId);
				if (success) {
					UserMaster denUserMaster = userMasterDao.get(UserMaster.class, denMember.getUserId());
					denUserMaster.setStatus(status);
					denUserMaster.setUpdateBy(loginUserId);
					denUserMaster.setUpdateDate(new Date());
					userMasterDao.update(denUserMaster);
				}
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	
	@Transactional
	public ResponseResult listCorporateAcc(ListPage<CorporateProfile> page){
		ListPage<CorporateProfile> listPage = corporateProfileDao.getCorpoateProfileList(page);
		List<Object> listCorporateDto = listPage.getDtoList();
		
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listCorporateDto);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}

	/**
	 * @param corporateId
	 * @param planNo
	 * @param loginUserId
	 * @author Li_Chen
	 * Date:2015-5-28
	 * 
	 * Description:
	 * 1 set the original corporate service account status "NACT"
	 * 2 create a new corporate service account
	 * 3 create a new corporate_service_subcribe
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public ResponseResult editCorporateServicePlan(Long corporateId, Long planNo,String loginUserId){
//		ResponseResult result = new ResponseResult();
		Date currentDate = new Date();
		//set the original corporate service account status "NACT"
		CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getUniqueByCols(CorporateServiceAcc.class, 
				new String[]{"status","corporateProfile.corporateId"}, new Serializable[]{Status.ACT.name(),corporateId});
		
		ServicePlan servicePlan = servicePlanDao.getServicePlanById(planNo);
		if(servicePlan==null) {
			responseResult.initResult(GTAError.CorporateManagementError.MISSING_SERVICE_PLAN);
			return responseResult;
		}
		
		if (null == corporateServiceAcc) {
			responseResult.initResult(GTAError.CorporateManagementError.CORPORATE_SERVICE_ACC_INACTIVE);
			return responseResult;
		}
		Long currentCorporateServiceAccVersion = corporateServiceAcc.getVersion();
		corporateServiceAcc.setStatus(Status.NACT.name());
		corporateServiceAcc.setVersion(currentCorporateServiceAccVersion);
		//transform persistent object to detached object for version control
		corporateServiceAccDao.getCurrentSession().evict(corporateServiceAcc);
		corporateServiceAccDao.update(corporateServiceAcc);
		
		//create a new corporate service account
		CorporateServiceAcc corpSerAcc = new CorporateServiceAcc();
		//ServicePlan servicePlan = servicePlanDao.getServicePlanById(planNo);
		BeanUtils.copyProperties(corporateServiceAcc,corpSerAcc);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		/*if(servicePlan.getContractLengthMonth()!=null) {
			calendar.add(Calendar.MONTH, servicePlan.getContractLengthMonth().intValue());
		}*/
		// set corporate account's expiryDate is forever
		calendar.add(Calendar.YEAR, 50);
		corpSerAcc.setEffectiveDate(currentDate);
		corpSerAcc.setExpiryDate(calendar.getTime());
		corpSerAcc.setStatus(Status.ACT.name());
		corpSerAcc.setFollowByStaff(loginUserId);
		corpSerAcc.setPeriodCode(servicePlan.getPassPeriodType());
		//set new version No for new corpSerAcc
		Integer newCorpSerAccVersion = 0;
		//update version no
		corpSerAcc.setVersion(new Long(newCorpSerAccVersion));
		Long serialAccNo = (Long) corporateServiceAccDao.saveCorpSerAcc(corpSerAcc);
		
		//create a new corporate_service_subcribe
		CorporateServiceSubscribe corpSerSub = new CorporateServiceSubscribe();
		CorporateServiceSubscribePK corpSerSubPK = new CorporateServiceSubscribePK();
		corpSerSubPK.setAccNo(serialAccNo);
		corpSerSubPK.setServicePlanNo(planNo);
		corpSerSub.setId(corpSerSubPK);
		corpSerSub.setSubscribeDate(currentDate);
		corpSerSub.setCreateDate(currentDate);
		corpSerSub.setCreateBy(loginUserId);
		corpSerSub.setUpdateDate(currentDate);
		corpSerSub.setUpdateBy(loginUserId);
		corpSerSub.setSubscribeDate(currentDate);
		corpSerSub.setCreateDate(currentDate);
		corpSerSub.setCreateBy(loginUserId);
		corpSerSub.setUpdateDate(currentDate);
		corpSerSub.setUpdateBy(loginUserId);
		corpSerSubDao.saveCorpSerSub(corpSerSub);
		List<ServicePlanFacility> sPlanFacility = servicePlanFacilityDao.getListByServicePlanNo(planNo);
		List<CorporateMember> corporateMember = corporateMemberDao.getAllCorporateMember(corporateId);
		List servicePlanAdditionRuleList = servicePlanAdditionRuleDao.getServicePlanAdditionRuleByPlanNo(planNo);
		for(CorporateMember tempMember: corporateMember){
			updateMemberFacilityLimitRule(planNo, servicePlan, tempMember.getCustomerId(), sPlanFacility,servicePlanAdditionRuleList,loginUserId);	
			List<Member> superiorMemberList = memberDao.getListMemberBySuperiorId(tempMember.getCustomerId());
			for(Member member: superiorMemberList){
				updateMemberFacilityLimitRule(planNo, servicePlan, member.getCustomerId(), sPlanFacility,servicePlanAdditionRuleList,loginUserId);
			}
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult createCorporateMember(CustomerProfile dto, String loginUserId,String fromName) throws Exception{
		Date currentDate = new Date();
		Long corporateId = dto.getCorporateId();
		CorporateProfile corporateProfile = corporateProfileDao.getByCorporateId(corporateId);
		if(corporateProfile==null) {
			responseResult.initResult(GTAError.CorporateManagementError.CORPORATE_HAS_NO_PROFILE);
			return responseResult;
		}
		ResponseResult validateResult = validateInput(new StringBuilder(),dto);
		if(!validateResult.getReturnCode().equals("0")){
			return validateResult;
		}
		if(dto.getPassportNo()!=null&&dto.getPassportType()!=null&&!customerProfileDao.checkAvailablePassportNo(null,dto.getPassportType(), dto.getPassportNo())){
			responseResult.initResult(GTAError.CorporateManagementError.ID_NUMBER_ALREADY_EXISTING);
			return responseResult;
		}
		Long servicePlanNo = dto.getPlanNo();
			
		if(servicePlanNo==null){
			throw new GTACommonException(GTAError.CorporateManagementError.CORPORATE_HAS_NO_SERVICE_PLAN);
		}
		ServicePlan plan = servicePlanDao.getServicePlanById(servicePlanNo);
		if(plan==null) {
			throw new GTACommonException(GTAError.CorporateManagementError.CANNOT_FIND_SERVICE_PLAN);
		}
		//Check if the age is compatible with the age in service plan
		if (dto.getCheckAge()){
			ResponseResult validateBirthDate = validateAge(dto.getDateOfBirth(), servicePlanNo);
			if(!validateBirthDate.getReturnCode().equals("0")){
				return validateBirthDate;
			}
		}
		
		//----------------Images will be saved by using individual method(in Controller)--------------
		String portraitPhoto = dto.getPortraitPhoto();
		String signature = dto.getSignature();
		dto.setPortraitPhoto(null);
		dto.setSignature(null);
		//-------------------------------------------------------------------------------
		dto.setCreateDate(currentDate);
		dto.setCreateBy(loginUserId);
		dto.setIsDeleted(Constant.DELETE_NO);
		Long customerId = (Long)customerProfileDao.addCustomerProfile(dto);
		
		//Create addition info
		if(dto.getCustomerAdditionInfos()!=null && dto.getCustomerAdditionInfos().size() >0){
			for(CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()){
				if(cust.getId().getCaptionId()==7||cust.getId().getCaptionId()==13){
					continue;
				}
				cust.getId().setCustomerId(customerId);
				cust.setCreateDate(currentDate);
				cust.setCreateBy(loginUserId);
				customerAdditionInfoDao.saveCustomerAdditionInfo(cust);
			}
		}
		
		//Create customer enrollment
		CustomerEnrollment enrollment = new CustomerEnrollment();
		enrollment.setCustomerId(customerId);
		enrollment.setCreateDate(currentDate);
		enrollment.setCreateBy(loginUserId);
		enrollment.setEnrollDate(currentDate);
		enrollment.setEnrollType(MemberType.CPM.name());
		enrollment.setStatus(EnrollStatus.APV.name());
		enrollment.setSubscribePlanNo(servicePlanNo);
		
		if (plan.getContractLengthMonth() != null) {
			enrollment.setSubscribeContractLength(plan.getContractLengthMonth());
		}
		enrollment.setSalesFollowBy(loginUserId);
		customerEnrollmentDao.addCustomerEnrollment(enrollment);
		
		//Create customer billing addresses
		if(dto.getCustomerAddresses()!=null && dto.getCustomerAddresses().get(0) != null){
			CustomerAddress address = dto.getCustomerAddresses().get(0);
			CustomerAddressPK apk = new CustomerAddressPK();
			apk.setAddressType(AddressType.BILL.name());
			apk.setCustomerId(customerId);
			address.setId(apk);
			if("true".equalsIgnoreCase(dto.getCheckBillingAddress())){
				address.setAddress1(dto.getPostalAddress1());
				address.setAddress2(dto.getPostalAddress2());
				address.setHkDistrict(dto.getPostalDistrict());
			}
			customerAddressDao.save(address);
		}
		
		String stringAcademyNo = memberDao.findLargestAcademyNo();
		if(stringAcademyNo==null) stringAcademyNo = "0000000";
		Integer tempAcademyNo = Integer.parseInt(stringAcademyNo)+1;
		String academyNo = CommUtil.formatAcademyNo(tempAcademyNo);
		if(!memberDao.validateAcademyID(academyNo)){
			throw new GTACommonException(GTAError.CorporateManagementError.ACADEMY_NUMBER_UNAVAILABLE);
		}
		
		//Create Member
		Member member = new Member();
		member.setCustomerId(customerId);
		member.setAcademyNo(academyNo);
		member.setCustomerId(customerId);
		member.setMemberType(MemberType.CPM.getType());
		member.setStatus(Constant.Member_Status_NACT);
		member.setFirstJoinDate(currentDate);
		member.setEffectiveDate(null);
		member.setCreateDate(currentDate);
		member.setCreateBy(loginUserId);
		memberDao.addMember(member);
		spaMemberSyncDao.addSpaMemberSyncWhenInsert(member);
		CorporateMember corporateMember = new CorporateMember();
		corporateMember.setCustomerId(customerId);
		corporateMember.setCorporateProfile(corporateProfile);
		corporateMember.setStatus(Constant.General_Status_NACT);
		corporateMember.setAuthorizedPositionTitle(dto.getPositionTitle());
		corporateMember.setCreateDate(currentDate);
		corporateMember.setCreateBy(loginUserId);
		corporateMemberDao.addCorporateMember(corporateMember);
		PosServiceItemPrice pServiceItemPrice = getPosServiceItemPrice(servicePlanNo);
		Long orderNo = setCustomerOrderHd(customerId, pServiceItemPrice, "OPN", null, loginUserId);
		Long orderDetId = setCustomerOderDet(orderNo, pServiceItemPrice, null, 1L, loginUserId);
		setCustomerEnrollPo(enrollment, orderDetId, orderNo);
		recordCustomerEnrollmentUpdate(null, EnrollStatus.APV.name(), customerId);
		
		//For Moving Portrait Photo in Controller
		CustomerProfile returnCustomerProfile = new CustomerProfile();
		returnCustomerProfile.setCustomerId(customerId);
		returnCustomerProfile.setPortraitPhoto(portraitPhoto);
		returnCustomerProfile.setSignature(signature);
				
		responseResult.initResult(GTAError.Success.SUCCESS,returnCustomerProfile);
		return responseResult;
	}

	public Long setCustomerOrderHd(Long customerId, PosServiceItemPrice pServiceItemPrice, String status,
			String orderRemark, String userId) {
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		Date currentDate = new Date();
		customerOrderHd.setOrderDate(currentDate);
		customerOrderHd.setOrderStatus(status);
		customerOrderHd.setStaffUserId(userId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(pServiceItemPrice.getItemPrice());
		customerOrderHd.setOrderRemark(orderRemark);
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(userId);
		return (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
	}

	public Long setCustomerOderDet(Long orderNo, PosServiceItemPrice pServiceItemPrice, String orderRemark,
			Long orderQty, String userId) {
		CustomerOrderDet cOrderDet = new CustomerOrderDet();
		Date currentDate = new Date();
		CustomerOrderHd cOrderHd = customerOrderHdDao.getOrderById(orderNo);
		cOrderDet.setCustomerOrderHd(cOrderHd);
		cOrderDet.setItemNo(pServiceItemPrice.getItemNo());
		cOrderDet.setItemRemark(orderRemark);
		cOrderDet.setOrderQty(orderQty);
		cOrderDet.setItemTotalAmout(pServiceItemPrice.getItemPrice());
		cOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		cOrderDet.setCreateBy(userId);
		return (Long) cOrderDetDao.saveOrderDet(cOrderDet);
	}

	public Serializable setCustomerEnrollPo(CustomerEnrollment customerEnrollment, Long orderDetId, Long orderNo) {
		CustomerEnrollPo customerEnrollPo = new CustomerEnrollPo();
		CustomerEnrollPoPK cEnrollPoPK = new CustomerEnrollPoPK();
		cEnrollPoPK.setEnrollId(customerEnrollment.getEnrollId());
		cEnrollPoPK.setOrderDetId(orderDetId);
		customerEnrollPo.setId(cEnrollPoPK);
		customerEnrollPo.setOrderNo(orderNo);
		return customerEnrollPoDao.saveCustomerEnrollPo(customerEnrollPo);
	}
	
	public PosServiceItemPrice getPosServiceItemPrice(Long servicePlanNo) {
		PosServiceItemPrice pServiceItemPrice = null;
		List<PosServiceItemPrice> itemNos = servicePlanDao.getDaypassPriceItemNo(servicePlanNo,
				Constant.ServiceplanType.SERVICEPLAN.name());
		for (PosServiceItemPrice psip : itemNos) {
			if (psip.getItemNo().startsWith("SRV")) {
				pServiceItemPrice = psip;
			}
		}
		return pServiceItemPrice;
	}
	
	public Long recordCustomerEnrollmentUpdate(String statusFrom,String statusTo,Long customerId){
		CustomerEnrollmentLog enrollmentLog = new CustomerEnrollmentLog();
		CustomerEnrollment enrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if(enrollment!=null){
			enrollmentLog.setEnrollId(enrollment.getEnrollId());
			enrollmentLog.setStatusFrom(statusFrom);
			enrollmentLog.setStatusTo(statusTo);
			enrollmentLog.setStatusUpdateDate(new Date());
			enrollmentLog.setSubscribePlanNo(enrollment.getSubscribePlanNo());
			enrollmentLog.setSubscribeContractLength(enrollment.getSubscribeContractLength());
			enrollmentLog.setSalesFollowBy(enrollment.getSalesFollowBy());
			return (Long) customerEnrollmentLogDao.save(enrollmentLog);
		}else {
			return null;
		}
		
	}
	
	private VirtualAccPool allocateVirtualAccount() {
		VirtualAccPool virtualAccount = null;
		virtualAccount = virtualAccPoolDao.queryOneAvailableVirtualAccount();
		if (virtualAccount == null) {
			throw new GTACommonException(GTAError.EnrollError.NO_VIRTUAL_ACCOUNT);
		}
		virtualAccount.setStatus(Constant.VIR_ACC_STATUS_US);
		virtualAccPoolDao.update(virtualAccount);

		return virtualAccount;
	}
	
	@Transactional
	public ResponseResult viewMember(CustomerProfile profile,Long customerId){
		CustomerProfile corporateMemberProfile = customerProfileDao.getCorporateMemberDetailByCustomerId(customerId);
		profile.setCorporateId(corporateMemberProfile.getCorporateId());
		profile.setMemberType(corporateMemberProfile.getMemberType());
		profile.setSuperiorMemberId(corporateMemberProfile.getSuperiorMemberId());
		profile.setTotalCreditLimit(corporateMemberProfile.getTotalCreditLimit());
		
		responseResult.initResult(GTAError.Success.SUCCESS,profile);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult editCorporateMember(CustomerProfile dto, String loginUserId){
		Date currentDate = new Date();
		Long corporateId = dto.getCorporateId();
		Long customerId  = dto.getCustomerId();
		StringBuilder ms = new StringBuilder();
		if(StringUtils.isEmpty(customerId)) ms.append(" Customer ID,");
		if (StringUtils.isEmpty(dto.getMemberType())) dto.setMemberType(MemberType.CPM.getType());
		ResponseResult validateResult = validateInput(ms,dto);
		if(!validateResult.getReturnCode().equals("0")){
			return validateResult;
		}
		CorporateMember corporateMember = corporateMemberDao.get(CorporateMember.class, corporateId);
		if(corporateMember==null){
			//return new ResponseResult("1", "Can not find the related corporate information for this member!");
			responseResult.initResult(GTAError.CorporateManagementError.NO_RELATED_CORPORATE_FOR_MEMBER);
			return responseResult;
		}
		CustomerProfile dBProfile = customerProfileDao.getById(customerId);
		if(dBProfile==null){
			//return new ResponseResult("1", "Can not find the profile for this member!");
			responseResult.initResult(GTAError.CorporateManagementError.NO_PROFILE_FOR_MEMBER);
			return responseResult;
		}
		dto.setUpdateBy(loginUserId);
		dto.setUpdateDate(currentDate);
		dto.setIsDeleted(dBProfile.getIsDeleted());
		dto.setCreateDate(dBProfile.getCreateDate());
		dto.setCreateBy(dBProfile.getCreateBy());
		dto.setSignature(dBProfile.getSignature());
		BeanUtils.copyProperties(dto, dBProfile);
		customerProfileDao.updateCustomerProfile(dBProfile);
		
		//create customer addresses
		if(dto.getCustomerAddresses()!=null && dto.getCustomerAddresses().get(0) != null){
			CustomerAddress address = dto.getCustomerAddresses().get(0);
			CustomerAddress temp = customerAddressDao.getByCustomerIDAddressType(customerId, AddressType.BILL.name());
			if(temp==null){
				CustomerAddressPK apk = new CustomerAddressPK();
				apk.setAddressType(AddressType.BILL.name());
				apk.setCustomerId(customerId);
				address.setId(apk);
				if("true".equalsIgnoreCase(dto.getCheckBillingAddress())){
					address.setAddress1(dto.getPostalAddress1());
					address.setAddress2(dto.getPostalAddress2());
					address.setHkDistrict(dto.getPostalDistrict());
				}
				customerAddressDao.save(address);
			}else{
				if("true".equalsIgnoreCase(dto.getCheckBillingAddress())){
					temp.setAddress1(dto.getPostalAddress1());
					temp.setAddress2(dto.getPostalAddress2());
					temp.setHkDistrict(dto.getPostalDistrict());
				}else{
					temp.setAddress1(address.getAddress1());
					temp.setAddress2(address.getAddress1());
					temp.setHkDistrict(address.getHkDistrict());
				}
				customerAddressDao.update(temp);
			}
		}
		
		//member limit rule
		MemberLimitRule memberLimitRule = memberLimitRuleDao.getEffectiveByCustomerId(customerId);
		if(memberLimitRule==null){
			//return new ResponseResult("1", "Can not find the member limit rule for this customer!Please contact backoffice!");
			responseResult.initResult(GTAError.CorporateManagementError.NO_MEMBER_LIMIT_RULES);
			return responseResult;
		}
		memberLimitRule.setNumValue(dto.getTotalCreditLimit());
		memberLimitRule.setUpdateBy(loginUserId);
		memberLimitRule.setUpdateDate(currentDate);
		memberLimitRuleDao.update(memberLimitRule);
		
		//customer additioninfos
		CustomerAdditionInfo temp;
		if(dto.getCustomerAdditionInfos()!=null && dto.getCustomerAdditionInfos().size() >0){
			for(CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()){
				temp=customerAdditionInfoDao.getByCustomerIdAndCaptionId(customerId,cust.getId().getCaptionId());
				if(temp==null){
					cust.getId().setCustomerId(customerId);
					cust.setCreateDate(currentDate);
					cust.setCreateBy(loginUserId);
					cust.setUpdateBy(loginUserId);
					cust.setUpdateDate(currentDate);
					customerAdditionInfoDao.saveCustomerAdditionInfo(cust);
				}else{
					temp.setUpdateBy(loginUserId);
					temp.setUpdateDate(currentDate);
					temp.setCustomerInput(cust.getCustomerInput());
					customerAdditionInfoDao.update(temp);
				}
			}
		}
		
		//Update Corporate Member
		if (MemberType.CPM.getType().equals(dto.getMemberType())) {
			corporateMember.setAuthorizedPositionTitle(dto.getPositionTitle());
			corporateMemberDao.update(corporateMember);
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getMemberList(ListPage<CorporateProfile> page,String byAccount,String status,Long filterByCustomerId){
		ListPage<CorporateProfile> listPage = corporateProfileDao.getMemberList(page,byAccount,status,filterByCustomerId);
		List<Object> listCorporateDto = listPage.getDtoList();
		
		for (Object dto : listCorporateDto) {
			CorporateMemberDto corporateMemberDto = (CorporateMemberDto) dto;
			if (MemberType.CPM.name().equals(corporateMemberDto.getMemberType())) {

				MemberLimitRule memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(corporateMemberDto.getCustomerId(), "G1");
				if (memberLimitRule != null) {
					if ("true".equalsIgnoreCase(memberLimitRule.getTextValue())) {
						corporateMemberDto.setDependentCreationRight(true);
					} else {
						corporateMemberDto.setDependentCreationRight(false);
					}
				}

				if (Constant.General_Status_NACT.equals(corporateMemberDto.getStatus())) {
					corporateMemberDto.setDependentCreationRight(false);
				}
			} else {
				corporateMemberDto.setDependentCreationRight(false);
			}
		}
		
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listCorporateDto);
		
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}
	
	
	@Override
	@Transactional
	public ResponseResult getMemberListByAccount(ListPage page, Long corporateId, String status) {
		ListPage<CorporateProfile> listPage = corporateProfileDao.getMemberList(page,corporateId.toString(),status,null);
		List<Object> listCorporateDto = listPage.getDtoList();
		
		CorporateProfile corporateProfile = corporateProfileDao.get(CorporateProfile.class, corporateId);
		List<CorporateMember> activeCorporateMembers = corporateMemberDao.getAllCorporateMemberByStatus(corporateId, Constant.Status.ACT.name());
		
		BigDecimal availableCreditLimit = corporateMemberDao.getRemainAvailableCreditLimit(corporateId, null);
		Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateId);
		CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
		
		BigDecimal totalCreditLimt = corporateServiceAcc.getTotalCreditLimit();
		DecimalFormat format = new DecimalFormat("##,##0.00");
		
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listCorporateDto);
		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("list", data);
		responseMap.put("availableLimit", null == availableCreditLimit ?"":("$" + format.format(availableCreditLimit.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())));
		responseMap.put("totalLimt", null == totalCreditLimt ?"":"$" + format.format(totalCreditLimt.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
		responseMap.put("activeCount", null == activeCorporateMembers ? 0:activeCorporateMembers.size());
		responseMap.put("corporateName", null == corporateProfile ? "":corporateProfile.getCompanyName());
		responseResult.initResult(GTAError.Success.SUCCESS, responseMap);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getCorporateAccountDropList() {
		List<CorporateProfile> list = corporateProfileDao.getCorporateAccountDropList();
		
		responseResult.initResult(GTAError.Success.SUCCESS,list);
		return responseResult;
	}

	@Transactional
	public ResponseResult getMemberDropList(Long corporateId) {
		List<CorporateMemberDto> list = corporateMemberDao.getMemberDropList(corporateId);
		Map<String, String> map = new HashMap<String, String>();
		if(list != null && list.size() > 0){
			for(CorporateMemberDto p : list){
				map.put(p.getCustomerId()+"", p.getMemberName());
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS,map);
		return responseResult;
	}

	@Transactional
	public ResponseResult checkCreditLimit(Long corporateId) {
		BigDecimal availableCreditLimit = corporateMemberDao.getRemainAvailableCreditLimit(corporateId, null);
		CorporateMemberDto dto = new CorporateMemberDto();
		if(availableCreditLimit!=null){
			dto.setRemainSum(availableCreditLimit);
		}
		responseResult.initResult(GTAError.Success.SUCCESS,dto);
		return responseResult;
	}
	
	public List<MemberPlanFacilityRight> setMemberPlanFacilityRights(
			Member tempMember, List<ServicePlanFacility> sPlanFacility,
			List<MemberPlanFacilityRight> mFacilityRight, Long servicePlanNo,
			Long customerId,String userId) {
		// member_plan_facility_right
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.YEAR, 50);

		MemberPlanFacilityRight temp;

		for (ServicePlanFacility spf : sPlanFacility) {
			temp = new MemberPlanFacilityRight();
			temp.setCreateBy(userId);
			temp.setCreateDate(currentDate);
			temp.setCustomerId(customerId);
			temp.setMember(tempMember);
			temp.setServicePlan(servicePlanNo);
			temp.setFacilityTypeCode(spf.getFacilityTypeCode());
			temp.setPermission(spf.getPermission());
			temp.setServicePlan(servicePlanNo);
			temp.setEffectiveDate(currentDate);
			temp.setExpiryDate(calendar.getTime());
			mFacilityRight.add(temp);
		}

		return mFacilityRight;
	}
	
	public List<MemberLimitRule> setMemberLimitRule(List<MemberLimitRule> list,ServicePlan servicePlan,Long servicePlanNo,List servicePlanAdditionRuleList,Long customerId,String userId){
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		if(servicePlan.getContractLengthMonth()!=null) {
			calendar.add(Calendar.MONTH, servicePlan.getContractLengthMonth().intValue());
		}
		if ( null != servicePlanAdditionRuleList && servicePlanAdditionRuleList.size()>0) {
			for (int i=0;i<servicePlanAdditionRuleList.size();i++) {
				Object[] objectArry = (Object[])servicePlanAdditionRuleList.get(i);
				MemberLimitRule memberLimitRule = new MemberLimitRule();
				memberLimitRule.setCustomerId(customerId);
				memberLimitRule.setLimitType( (String)objectArry[0]);
				String inputValueType = (String)objectArry[1];
				if (!StringUtils.isEmpty(inputValueType))
					inputValueType = inputValueType.trim();
				if (ServicePlanRightTypeStatus.TXT.getDesc().equals(inputValueType)) 
					memberLimitRule.setTextValue((String)objectArry[2]);
				else if (ServicePlanRightTypeStatus.INT.getDesc().equals(inputValueType)) {
					memberLimitRule.setLimitUnit("MONTH");
					memberLimitRule.setNumValue(BigDecimal.valueOf(Long.valueOf((String) objectArry[2])) );
				}	
				memberLimitRule.setDescription((String)objectArry[3]);
				memberLimitRule.setCreateDate(new Timestamp(currentDate.getTime()));
				memberLimitRule.setCreateBy(userId);
				memberLimitRule.setUpdateDate(currentDate);
				memberLimitRule.setUpdateBy(userId);
				memberLimitRule.setEffectiveDate(currentDate);
				memberLimitRule.setExpiryDate(calendar.getTime());
				list.add(memberLimitRule);
			}
			return list;
		}
		return null;
	}
	
	public void updateMemberFacilityLimitRule(Long planNo,ServicePlan servicePlan,Long customerId,List<ServicePlanFacility> sPlanFacility,List servicePlanAdditionRuleList,String userId){
		
		Member tempMember = memberDao.getMemberById(customerId);
		customerEnrollmentDao.updateServicePlanNo(planNo, servicePlan.getContractLengthMonth(), customerId, userId);
		List<MemberPlanFacilityRight> memberPlanFacilityRight = memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(customerId);
		if ( memberPlanFacilityRight != null && memberPlanFacilityRight.size() > 0) {
			memberPlanFacilityRightDao.deletePlanFacilityRight(customerId);
		}
		List<MemberLimitRule> oldLimitRules = memberLimitRuleDao.getEffectiveListByCustomerId(customerId);
		if ( oldLimitRules != null && oldLimitRules.size() > 0) {
			if(tempMember.getMemberType().equalsIgnoreCase("CPM")){
				memberLimitRuleDao.deleteMemberLimitRule(customerId, "CR");
			}else if (tempMember.getMemberType().equalsIgnoreCase("CDM")){
				memberLimitRuleDao.deleteMemberLimitRule(customerId, "TRN");
			}
		}
		List<MemberPlanFacilityRight> tempMemberPlanFacilityRight = setMemberPlanFacilityRights(tempMember,sPlanFacility,new ArrayList<MemberPlanFacilityRight>(), planNo, customerId,userId);
		memberPlanFacilityRightDao.savePlanFacilityRight(tempMemberPlanFacilityRight);
		
		List<MemberLimitRule> tempMemberLimitRules = setMemberLimitRule(new ArrayList<MemberLimitRule>(), servicePlan, planNo, servicePlanAdditionRuleList, customerId,userId);
		memberLimitRuleDao.saveMemberLimitRule(tempMemberLimitRules);
	}
	
	public ResponseResult validateAge(String dateOfBirth, Long planNo) throws Exception {
		
		if(!StringUtils.isEmpty(dateOfBirth)) {
			Integer age = CommUtil.getAge(dateOfBirth);
			String ageRangeCode = CommUtil.getAgeRange(age).name();
			AgeRange ageRange = customerProfileDao.getServicePlanAgeRange(planNo);
			
			if (ageRange == null) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
				return responseResult;
			}
			
			if (!ageRange.getAgeRangeCode().equalsIgnoreCase("ALL") && !ageRangeCode.equals(ageRange.getAgeRangeCode())){
				switch (ageRange.getAgeRangeCode()) {
				case "ADULT":
//					responseResult.initResult(GTAError.MemberShipError.CREATED_MEMBER_AGE_INCOMPATIBLE_WITH_SERVICEPLAN, new String[]{"under the age of 18 or above the age of 60"});
//					return responseResult;
					if (age < 18){
						//return new ResponseResult("92301","The member is under the age of 18, are you sure?"); 
						responseResult.initResult(GTAError.CorporateManagementError.UNDER_AGE_OF_18);
						return responseResult;
					}
					else if (age > 60){
						//return new ResponseResult("92301","The member is above the age of 60, are you sure?");
						responseResult.initResult(GTAError.CorporateManagementError.ABOVE_AGE_OF_60);
						return responseResult;
					}
				case "SENIOR":
//					responseResult.initResult(GTAError.MemberShipError.CREATED_MEMBER_AGE_INCOMPATIBLE_WITH_SERVICEPLAN, new String[]{"under the age of 60"});
//					return responseResult;
					//return new ResponseResult("92301","The member is under the age of 60, are you sure?");
					responseResult.initResult(GTAError.CorporateManagementError.UNDER_AGE_OF_60);
					return responseResult;
				case "CHILD":
//					responseResult.initResult(GTAError.MemberShipError.CREATED_MEMBER_AGE_INCOMPATIBLE_WITH_SERVICEPLAN, new String[]{"under the age of 11 or above the age of 17"});
//					return responseResult;
					if (age < 11){
						//return new ResponseResult("92301","The member is under the age of 11, are you sure?");
						responseResult.initResult(GTAError.CorporateManagementError.UNDER_AGE_OF_11);
						return responseResult;
					}
					else if (age > 17){
						//return new ResponseResult("92301","The member is above the age of 17, are you sure?");
						responseResult.initResult(GTAError.CorporateManagementError.ABOVE_AGE_OF_17);
						return responseResult;
					}
				case "KID":
//					responseResult.initResult(GTAError.MemberShipError.CREATED_MEMBER_AGE_INCOMPATIBLE_WITH_SERVICEPLAN, new String[]{"above the age of 10"});
//					return responseResult;
					//return new ResponseResult("92301","The member is above the age of 10, are you sure?");
					responseResult.initResult(GTAError.CorporateManagementError.ABOVE_AGE_OF_10);
					return responseResult;
				default:
					break;
				}
			}
			
		}
		else {
			//return new ResponseResult("1","Date of Birth is required parameters!"); 
			responseResult.initResult(GTAError.CorporateManagementError.DATE_OF_BIRTH_IS_EMPTY);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	public ResponseResult validateInput(StringBuilder ms, CustomerProfile dto){

		if(StringUtils.isEmpty(dto.getCorporateId())){
			ms.append(" Corporate ID,");
		}
		if(StringUtils.isEmpty(dto.getPassportType())){
			ms.append(" ID Type,");
		}
		if(StringUtils.isEmpty(dto.getPassportNo())){
			ms.append(" ID No,");
		}
		if(StringUtils.isEmpty(dto.getTotalCreditLimit())){
			ms.append(" Credit Limit,");
		}
		if(StringUtils.isEmpty(dto.getSalutation())){
			ms.append(" Salutation,");
		}
		if(StringUtils.isEmpty(dto.getSurname())){
			ms.append(" Surname,");
		}
		if(StringUtils.isEmpty(dto.getGivenName())){
			ms.append(" Given Name,");
		}
		if(StringUtils.isEmpty(dto.getContactEmail())){
			ms.append(" Contact Email,");
		}
		if(StringUtils.isEmpty(dto.getCheckBillingAddress())){
			ms.append(" Check billing address,");
		}
		if(StringUtils.isEmpty(dto.getDateOfBirth())){
			ms.append(" Date of Birth,");
		}
		if(ms.length()>0){
			//return new ResponseResult("1",ms.substring(0,ms.length()-1).toString()+" are required parameters!");
			responseResult.initResult(GTAError.CorporateManagementError.MS_PARAMETER_IS_REQUIRED);
			return responseResult;
		}
		
		if(!PassportType.HKID.name().equals(dto.getPassportType()) && !PassportType.VISA.name().equals(dto.getPassportType())){
			//return new ResponseResult("1", "ID Type: invalid! Please input HKID or VISA!");
			responseResult.initResult(GTAError.CorporateManagementError.ID_TYPE_INVALID);
			return responseResult;
		}

		if(PassportType.HKID.name().equals(dto.getPassportType())&& !CommUtil.validateHKID(dto.getPassportNo())){
			//return new ResponseResult("1", "HKID Number: Invalid! Please input a new HKID Number!");
			responseResult.initResult(GTAError.CorporateManagementError.HK_ID_NUMBER_INVALID);
			return responseResult;
		}
		if(PassportType.VISA.name().equals(dto.getPassportType())&& !CommUtil.validateVISA(dto.getPassportNo())){
			//return new ResponseResult("1", "Passport Number: Invalid! Please input a new Passport Number!");
			responseResult.initResult(GTAError.CorporateManagementError.PASSPORT_NUMBER_INVALID);
			return responseResult;
		}
		
		if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
			responseResult.initResult(GTAError.EnrollError.MOBILE_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
			responseResult.initResult(GTAError.EnrollError.BUSINESS_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
			responseResult.initResult(GTAError.EnrollError.HOME_PHONE_INVALID);
			return responseResult;
		}
		
		if(!CommUtil.validateEmail(dto.getContactEmail())){
			//return new ResponseResult("1", "Email Address  : Invalid! Please input a new Email address!");
			responseResult.initResult(GTAError.CorporateManagementError.EMAIL_ADDRESS_INVALID);
			return responseResult;
		}
		if (!MemberType.CPM.getType().equals(dto.getMemberType())&&!dto.getMemberType().equals(MemberType.CDM.getType())){
			//return new ResponseResult("1","Please select Corporate Primary Member (CPM) or Corporate Dependent Member (CDM)!");
			responseResult.initResult(GTAError.CorporateManagementError.NEED_TO_SELECT_CPM_OR_CDM);
			return responseResult;
		}	
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
		
	}
	
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String module){
		if ("corporateAccount".equalsIgnoreCase(module)) {
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Corporate ID", "corporateId", Integer.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Corporate Name", "companyName", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("# of Active Members", "noOfActiveMember", Integer.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Membership Officer", "followByStaff", String.class.getName(), "", 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Credit Limit", "creditLimit", BigDecimal.class.getName(), "", 5);

			return Arrays.asList(condition1, condition2, condition3, condition4, condition5);
		} else if ("corporateMember".equalsIgnoreCase(module)) {
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyNo", String.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member", "memberName", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Corporate Name", "companyName", String.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Service Plan", "planName", String.class.getName(), "", 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Activation Date", "DATE_FORMAT(activationDate,'%Y-%m-%d')", Date.class.getName(), "", 5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Membership Officer", "officer", String.class.getName(), "", 6);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Credit Limit", "totalCreditLimit", BigDecimal.class.getName(), "", 7);
			AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Status", "statusValue", String.class.getName(), getMemberStatusForAdvanceSearch(), 8);

			
			return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6, condition7,condition8);
		} else if ("spendingSummary".equalsIgnoreCase(module)){
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date", "DATE_FORMAT(coh.order_date,'%Y-%m-%d')", Date.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Academy ID", "m.academy_no", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Member", "CONCAT(cp.salutation,' ',cp.given_name,' ',cp.surname)", String.class.getName(), "",3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Spending Description", "pos.description", String.class.getName(), "", 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Spending", "cot.paid_amount", BigDecimal.class.getName(), "", 5);
			return Arrays.asList(condition1, condition2, condition3,condition4, condition5);
		} else if ("enrollmentSettlement".equalsIgnoreCase(module)){
			List<SysCode> enrollStatus = sysCodeDao.selectSysCodeByCategoryWithoutEmptyCodeValue("enrollStatus");
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Enrollment Date", "enrollDateDB", Date.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Academy ID", "academyNo", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Company", "companyName", String.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Customer Name", "memberName", String.class.getName(), "",4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Enroll Status", "status", String.class.getName(), enrollStatus, 5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Member Type", "passPeriodType", String.class.getName(), getMemberTypeForAdvanceSearch(), 6);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Sales Person", "salesFollowBy", String.class.getName(), "", 7);
			AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("# of Pay. Failed", "failTransNo", Integer.class.getName(), "", 8);
			AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Balance Due", "balanceDue", BigDecimal.class.getName(), "", 9);
			
			return Arrays.asList(condition1, condition2, condition3,condition4, condition5,condition6,condition7,condition8,condition9);
		}
		return new ArrayList<AdvanceQueryConditionDto>();
		
	}
	
	
	private List<SysCode> getMemberStatusForAdvanceSearch(){
		List<SysCode> memberStatus=new ArrayList<>();

		SysCode s1=new SysCode();
		s1.setCodeDisplay("Active");
		s1.setCodeValue("Active");
		memberStatus.add(s1);
		
		SysCode s2=new SysCode();
		s2.setCodeDisplay("Inactive");
		s2.setCodeValue("Inactive");
		memberStatus.add(s2);
		return memberStatus;
	}
	
	private List<SysCode> getMemberTypeForAdvanceSearch(){
		List<SysCode> memberType=new ArrayList<>();

		SysCode s1=new SysCode();
		s1.setCodeDisplay("All Days");
		s1.setCodeValue("FULL");
		memberType.add(s1);
		
		SysCode s2=new SysCode();
		s2.setCodeDisplay("Weekdays Only");
		s2.setCodeValue("WD");
		memberType.add(s2);
		return memberType;
	}
	
	@Transactional
	public CustomerEmailContent assembleMemberStatusChangedEmail(String sendUserId,String fromName,String functionId,CustomerProfile customerProfile){
		Long customerId = customerProfile.getCustomerId();
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		MessageTemplate messageTemplate = messageTemplateDao.getTemplateByFunctionId(functionId);
		
		String content= messageTemplate.getFullContentHtml(customerProfile.getSalutation()+ " "+ customerProfile.getGivenName()+" "+customerProfile.getSurname(),fromName);
		customerEmailContent.setContent(content);
		customerEmailContent.setRecipientEmail(customerProfile.getContactEmail());
		customerEmailContent.setSubject(messageTemplate.getMessageSubject());
		customerEmailContent.setStatus(EmailStatus.PND.name());
		customerEmailContent.setSenderUserId(sendUserId);
		customerEmailContent.setRecipientCustomerId(customerId.toString());
		customerEmailContent.setSendDate(new Date());
		String sendId = (String)customerEmailContentDao.save(customerEmailContent);
		customerEmailContent.setSendId(sendId);
		return customerEmailContent;
	}
	@Transactional
	public CustomerEmailContent assembleCorporateAccountChangedEmail(String sendUserId,String fromName,String functionId,CorporateProfile corporateProfile){
//		Long customerId = customerProfile.getCustomerId();
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		MessageTemplate messageTemplate = messageTemplateDao.getTemplateByFunctionId(functionId);
		
		String content= messageTemplate.getFullContentHtml(corporateProfile.getContactPersonFirstname()+" "+corporateProfile.getContactPersonLastname(),fromName);
		customerEmailContent.setContent(content);
		customerEmailContent.setRecipientEmail(corporateProfile.getContactEmail());
		customerEmailContent.setSubject(messageTemplate.getMessageSubject());
		customerEmailContent.setStatus(EmailStatus.PND.name());
		customerEmailContent.setSenderUserId(sendUserId);
		customerEmailContent.setRecipientCustomerId(null);
		customerEmailContent.setSendDate(new Date());
		String sendId = (String)customerEmailContentDao.save(customerEmailContent);
		customerEmailContent.setSendId(sendId);
		return customerEmailContent;
	}
	
	@Transactional
	public ResponseResult getValidServicePlanIncludeItself(Long corporateId){
		List<ServicePlan> servicePlans = servicePlanDao.getValidCorporateServicPlan();
		Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateId);
		CorporateServiceSubscribe subscribe = corpSerSubDao.getByAccountNo(accNo);
		List<DropDownDto> dropdownList = new ArrayList<DropDownDto>();
		Boolean isFound = false;
		for (ServicePlan plan : servicePlans) {
			if (Constant.General_Status_ACT.equals(plan.getStatus())) {
				DropDownDto dto = new DropDownDto();
				dto.setCodeDisplay(plan.getPlanName());
				dto.setCodeValue(String.valueOf(plan.getPlanNo()));
				if (corporateId!=null&&plan.getPlanNo().equals(subscribe.getId().getServicePlanNo())) {
					isFound = true;
				}
				dropdownList.add(dto);
			}
		}
		
		if(!isFound&&corporateId!=null){
			ServicePlan currentServicePlan = servicePlanDao.getServicePlanById(subscribe.getId().getServicePlanNo());
			if(currentServicePlan!=null){
				DropDownDto currentDto = new DropDownDto();
				currentDto.setCodeDisplay(currentServicePlan.getPlanName());
				currentDto.setCodeValue(String.valueOf(currentServicePlan.getPlanNo()));
				dropdownList.add(currentDto);
			}
		}
		Data data = new Data();
		data.setList(dropdownList);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getCorporateMembershipSettlement(ListPage<CustomerOrderTrans> page,String status,Long filterByCustomerId){
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getCorporateMembershipSettlement(page,status,filterByCustomerId);
		List<Object> listCorporateMembershipSettlement = listPage.getDtoList();
		for(Object tempDto: listCorporateMembershipSettlement){
			CustomerEnrollmentDto convertedDto = (CustomerEnrollmentDto)tempDto;
			if(!EnrollStatus.TOA.name().equals(convertedDto.getStatus())){
				continue;
			}
			convertedDto.setNoOfDays(DateConvertUtil.getNoOfDaysOnlyInfuture(convertedDto.getEffectiveDateDB()));
		}
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listCorporateMembershipSettlement);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getCorporateContactByCorporateIdAndContactType(Long corporateId,String contactType){
		CorporateProfileDto corporateProfileDto = corporateProfileDao.getCorporateContactByCorporateIdAndContactType(corporateId, contactType);
		if( corporateProfileDto != null ){
			responseResult.initResult(GTAError.Success.SUCCESS,corporateProfileDto);
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS);
		}
		return responseResult;
	}
}
