package com.sinodynamic.hkgta.service.crm.sales.presentation;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.PresentMaterialDao;
import com.sinodynamic.hkgta.dao.crm.PresentMaterialSeqDao;
import com.sinodynamic.hkgta.dao.crm.PresentationDao;
import com.sinodynamic.hkgta.dao.crm.PresentationFolderDao;
import com.sinodynamic.hkgta.dao.crm.PresentationFolderDetailsDao;
import com.sinodynamic.hkgta.dao.crm.PresentationMaterialsSeqDao;
import com.sinodynamic.hkgta.dao.crm.ServerFolderMappingDao;
import com.sinodynamic.hkgta.dto.crm.PresentMaterialDto;
import com.sinodynamic.hkgta.dto.crm.PresentationWithMaterialDto;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeqPK;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.PdfConvertor;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.OrderByType;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.exception.PresentationException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class PresentationServiceImpl extends ServiceBase<PresentationBatch> implements PresentationService
{
	private Logger logger = Logger.getLogger(PresentationServiceImpl.class);
	@Autowired
	private PresentationDao presentationDao;

	@Autowired
	private PresentMaterialDao presentMaterialDao;

	@Autowired
	private PresentationMaterialsSeqDao presentationMaterialsSeqDao;

	@Autowired
	private ServerFolderMappingDao serverFolderMappingDao;

	@Autowired
	private PresentationFolderDao presentationFolderDao;

	@Autowired
	private PresentMaterialSeqDao pmsDao;

	@Autowired
	private PresentationFolderDetailsDao presentationFolderDetailsDao;

	public PresentationBatch getPresentation(PresentationBatch t) throws Exception
	{
		return null;
	}

	@Transactional
	public ResponseMsg savePresentation(PresentationWithMaterialDto t) throws Exception
	{
		logger.info("PresentationServiceImpl.savePresentation invocation start ...");
		if (null == t)
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE,  ResponseMsgConstant.ERRORMSG_EN);

		PresentationBatch p = new PresentationBatch();

		p.setCreateBy(t.getCreateBy());
		p.setCreateDate(new Timestamp(System.currentTimeMillis()));
		p.setUpdateDate(new Timestamp(System.currentTimeMillis()));

		if (StringUtils.isEmpty(t.getPresentName()))
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE_PRESENTNAME_NULL, ResponseMsgConstant.ERRORMSG_PRESENTNAME_NULL_EN);

		p.setPresentName(t.getPresentName());

		p.setCreateDate(new Timestamp(System.currentTimeMillis()));
		 p.setDescription(t.getDescription());
		p.setUpdateBy(t.getCreateBy());

		if (presentationDao.findPresentationByName(t.getPresentName()))
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE_PRESENTATION_EXIST, ResponseMsgConstant.ERRORMSG_PRESENTATION_EXIST_EN);

		Long presentationId = (Long) presentationDao.savePresentation(p);

		List<PresentMaterialDto> materialist = t.getPresentMaterials();

		if (null == presentationId)
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_EN);

		if (null != materialist)
		{
			List<PresentMaterialSeq> seqs = new ArrayList<PresentMaterialSeq>();
			for (int i = 0; i < materialist.size(); i++)
			{
				PresentMaterialDto pdo = materialist.get(i);

				PresentMaterialSeqPK pk = new PresentMaterialSeqPK();
				pk.setPresentId(presentationId);
				pk.setMaterialId(pdo.getMaterialId());

				PresentMaterialSeq pms = new PresentMaterialSeq();
				pms.setId(pk);
				pms.setPresentSeq(pdo.getMaterialSeq());
				seqs.add(pms);
			}

			p.setPresentMaterialSeqs(seqs);
		}
		
		logger.info("PresentationServiceImpl.savePresentation invocation end ...");
		return new ResponseResult(ResponseMsgConstant.SUCESSCODE,"", new PresentationBatch(presentationId));

	}

	/**
	 * @author Li_Chen 
	 * Date:2015-4-28
	 * Description:  
	 *  1. delete the presentBatch's data
	 *  2. update the new data on this presentBatch
	 * Revision history: 
	 * 2015-4-28  Li_Chen  The initial implementation 
	 */
	@Transactional
	@Override
	public ResponseResult updatePresentation(PresentationWithMaterialDto presentationDto) throws Exception
	{
		logger.info("PresentationServiceImpl.updatePresentation invocation start ...");
		
		/*
		 * this variable is not sure,then change later
		 *
		 */
//		String updateBy = "Li_Chen"; //get this by session user
		ResponseResult result = new ResponseResult();
		PresentationBatch present = presentationDao.getById(presentationDto.getPresentId());

		if (null == present) {
			result.setReturnCode(ResponseMsgConstant.ERRORCODE);
			result.setErrorMessageEN("The presentation Id doesn't exist,please check again");

			return result;
		}
		
		PresentationBatch presentationBatch = presentationDao.getById(presentationDto.getPresentId());
		if(!presentationDto.getUpdateBy().equals(presentationBatch.getUpdateBy())){
			return new ResponseResult("1", "Can't modify the other's presentation!");
		}
		present.setPresentName(presentationDto.getPresentName());
		present.setDescription(presentationDto.getDescription());
		present.setUpdateDate(new Date());
		present.setUpdateBy(presentationDto.getUpdateBy());
		present.getPresentMaterialSeqs().clear();
        presentationDao.getCurrentSession().flush();
		List<PresentMaterialDto> presentMaterials = presentationDto.getPresentMaterials();
		List<PresentMaterialSeq> seqs = new ArrayList<PresentMaterialSeq>();
		PresentMaterialSeq seq = null;
		PresentMaterialSeqPK pk = null;
		if (null != presentMaterials && presentMaterials.size() > 0) {
			
			for (PresentMaterialDto material : presentMaterials) {
				seq = new PresentMaterialSeq();
				pk = new PresentMaterialSeqPK();
				pk.setMaterialId(Long.valueOf(material.getMaterialId()));
				pk.setPresentId(present.getPresentId());
				seq.setId(pk);
				seq.setPresentSeq(Long.valueOf(material.getMaterialSeq()));
				seqs.add(seq);
			}
			present.getPresentMaterialSeqs().addAll(seqs);
		}
		result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
		result.setErrorMessageEN("success");
		result.setData(present);
		result.setData(new PresentationBatch(presentationDto.getPresentId()));
		
		logger.info("PresentationServiceImpl.updatePresentation invocation end ...");
		return result;

	}

	@Transactional
	public ResponseMsg deletePresentation(PresentationBatch t) throws Exception
	{
		logger.info("PresentationServiceImpl.deletePresentation invocation start ...");
		
		if (null == t.getPresentId())
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE,  ResponseMsgConstant.ERRORMSG_EN);

		boolean deletePresentation = presentationDao.deletePresentation(t);
		if (!deletePresentation)
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_EN);
		
		logger.info("PresentationServiceImpl.deletePresentation invocation end ...");
		return new ResponseMsg(ResponseMsgConstant.SUCESSCODE);
	}

	@Override
	@Transactional
	public PresentationBatch getPresentationById(Long presentId) throws Exception
	{
		return presentationDao.get(PresentationBatch.class, presentId);
	}

	@Override
	@Transactional
	public String convert(Long presentId) {
		logger.info("PresentationServiceImpl.convert invocation start ...");
		
		String saveDir = appProps.getProperty("presentation.attach.dir");
		
		logger.info("presentation.attach.dir=" + saveDir);
		File file = new File(saveDir);
		if (!file.exists()) file.mkdir();
		
		PresentationBatch p = presentationDao.getById(presentId);
		if (p == null) return null;
		
		List<PresentMaterialSeq> materials = p.getPresentMaterialSeqs();
		if (materials == null || materials.size() == 0) return null;
		
		logger.info("materials=" + materials);
		Collections.sort(materials, new Comparator<PresentMaterialSeq>() {
			@Override
			public int compare(PresentMaterialSeq o1, PresentMaterialSeq o2) {
				int seq1 = o1.getPresentSeq().intValue();
				int seq2 = o2.getPresentSeq().intValue();
				return  (seq1 - seq2);
			}
		});
		
		List<String> photoUrls =  new ArrayList<String>();
		for (PresentMaterialSeq seq : materials) {

			Long materialId = seq.getId().getMaterialId();
			PresentMaterial presentMaterial = presentMaterialDao.getById(materialId);
			if (presentMaterial == null) return null;
			
			//filter present material by .jpg and .png and .JPG and .PNG
			String materialFilename = presentMaterial.getMaterialFilename();
			if (!materialFilename.endsWith(".jpg") && !materialFilename.endsWith(".JPG")
					&& !materialFilename.endsWith(".png") && !materialFilename.endsWith(".PNG")) continue;
			
			Long folderId = presentMaterial.getFolderId();
			if(null == folderId) continue;
			ServerFolderMapping folderMapping = getServerFolderMappingById(folderId);
			if (folderMapping == null) continue;
			String pathPrefixKey = folderMapping.getPathPrefixKey();
			
			String prefixPath = appProps.getProperty(pathPrefixKey, "");
			String imagePreUrl = prefixPath + File.separator + folderId + File.separator + materialFilename;
			
			logger.info("imagePreUrl=" + imagePreUrl);
			photoUrls.add(imagePreUrl);

		}
		
		logger.info("Image Urls = " + materials);
		String pdfName = p.getPresentName();
		String pdfPath = saveDir + pdfName + ".pdf";
		
		PdfConvertor pdfConvertor = new PdfConvertor();
		
		logger.info("PresentationServiceImpl.convert invocation end ...");
		return pdfConvertor.createPresentationPdf(pdfPath, photoUrls);

	}

	@Transactional(propagation = Propagation.REQUIRED)
	public ListPage<PresentationBatch> getPresentationList(String createBy, OrderByType orderByType, boolean isDesc, String presentName, ListPage<PresentationBatch> pListPage) throws Exception
	{
		logger.info("PresentationServiceImpl.getPresentationList invocation start ...");

		StringBuffer hql = new StringBuffer();
		StringBuffer hqlCount = new StringBuffer();
		hql.append("select p from PresentationBatch p where p.presentId > 0");
		hqlCount.append("select count(*) from PresentationBatch p where p.presentId > 0");
		List<Serializable> param = new ArrayList<Serializable>();
		if (createBy != null && !createBy.isEmpty())
		{
			hql.append(" and p.createBy = ?");
			hqlCount.append(" and p.createBy = ?");
			param.add(createBy);
		}

		if (presentName != null && !presentName.isEmpty())
		{
			hql.append(" and p.presentName like ? ");
			hqlCount.append(" and p.presentName like ? ");
			param.add("%" + presentName + "%");
		}

		String orderByCol = "";

		switch (orderByType)
		{
		case PresentationName:
			orderByCol = "p.presentName";
			break;
		case CreateBy:
			orderByCol = "p.createBy";
			break;
		case UpdateDate:
			orderByCol = "p.updateDate";
			break;
		}

		if (!isDesc)
		{
			pListPage.addDescending(orderByCol);
		} else
		{
			pListPage.addAscending(orderByCol);
		}

		pListPage = presentationDao.getPresentationBatchList(pListPage, hql.toString(), hqlCount.toString(), param);

		logger.info("PresentationServiceImpl.getPresentationList invocation end ...");
		
		return pListPage;
	}

	/**
	 * This method is used to retrieve list of presentation folders
	 * @return List of presentation folders
	 * @throws Exception
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public List<ServerFolderMapping> getTemplateFolderList() throws Exception
	{
		return presentationFolderDao.getTemplateFolderList();

	}

	@Override
	@Transactional
	public List<PresentMaterial> getFolderDetails(Long folderId) throws Exception
	{
		return presentationFolderDetailsDao.getFolderDetails(folderId);

	}

	@Override
	@Transactional
	public int getPresentSeq(Long materialId, Long presentId)
	{
		int presentSeq = presentationMaterialsSeqDao.getPresentSeq(materialId, presentId);

		return presentSeq;
	}

	@Override
	@Transactional
	public Long cloneById(Long presentId, String presentName, String description, String createBy) throws PresentationException
	{
		logger.info("PresentationServiceImpl.cloneById invocation start ...");
		
		PresentationBatch p = presentationDao.getById(presentId);

		if (null == p)
		{
			throw new GTACommonException(GTAError.PresentationError.PRESENTATION_NOT_EXIST, new Object[] { presentId.toString() });
		}

		PresentationBatch newP = new PresentationBatch();
		
		if(StringUtils.isEmpty(createBy)){
			newP.setCreateBy(p.getCreateBy());
		}else{
			newP.setCreateBy(createBy);
		}
		
		Date date = new Date();
		Timestamp ts = new Timestamp(date.getTime());
		newP.setCreateDate(ts);
		
		if(StringUtils.isEmpty(description)){
			newP.setDescription(p.getDescription());
		}else{
			newP.setDescription(description);
		}

		if(StringUtils.isEmpty(presentName)){
			newP.setPresentName(p.getPresentName());
		}else{
			newP.setPresentName(presentName);
		}
		
		
		if(StringUtils.isEmpty(createBy)){
			newP.setUpdateBy(p.getUpdateBy());
		}else{
			newP.setUpdateBy(createBy);
		}
		newP.setUpdateDate(date);
		List<HashMap<String, Long>> oldseqs = new ArrayList<HashMap<String, Long>>();
		for (PresentMaterialSeq seq : p.getPresentMaterialSeqs())
		{
			HashMap<String, Long> m_seq = new HashMap<String, Long>();
			m_seq.put("m_id", seq.getId().getMaterialId());
			m_seq.put("seq", (long) seq.getPresentSeq());
			oldseqs.add(m_seq);
		}

		presentationDao.save(newP);

		List<PresentMaterialSeq> seqs = new ArrayList<PresentMaterialSeq>();
		for (HashMap<String, Long> tmp_m_seq : oldseqs)
		{
			PresentMaterialSeq tmp_seq = new PresentMaterialSeq();
			PresentMaterialSeqPK tmp_pk = new PresentMaterialSeqPK();
			tmp_pk.setMaterialId(tmp_m_seq.get("m_id"));
			tmp_pk.setPresentId(newP.getPresentId());
			tmp_seq.setId(tmp_pk);
			tmp_seq.setPresentationBatch(p);
			tmp_seq.setPresentSeq(tmp_m_seq.get("seq"));
			seqs.add(tmp_seq);
		}

		newP.setPresentMaterialSeqs(seqs);

		logger.info("PresentationServiceImpl.cloneById invocation end ...");
		return newP.getPresentId();
	}

	@Override
	@Transactional
	public ServerFolderMapping getServerFolderMappingById(Long folderId)
	{
		return serverFolderMappingDao.findById(folderId);
	}

	@Transactional
	public List<PresentMaterialDto> getPresentMaterials(List<PresentMaterialSeq> seqs) throws Exception
	{
		logger.info("PresentationServiceImpl.getPresentMaterials invocation start ...");

		List<HashMap<Long, PresentMaterial>> mlist = new ArrayList<HashMap<Long, PresentMaterial>>();

		for (PresentMaterialSeq seq : seqs)
		{
			PresentMaterial material = presentMaterialDao.getById(seq.getId().getMaterialId());// findById(seq.getId().getMaterialId());
			HashMap<Long, PresentMaterial> material_seq = new HashMap<Long, PresentMaterial>();
			material_seq.put(seq.getPresentSeq(), material);
			mlist.add(material_seq);
		}

		List<PresentMaterialDto> presentMaterials = getPresentMaterialDto(mlist);
		
		logger.info("PresentationServiceImpl.getPresentMaterials invocation end ...");
		
		return presentMaterials;

	}

	@Transactional
	private List<PresentMaterialDto> getPresentMaterialDto(List<HashMap<Long, PresentMaterial>> materialList)
	{
		logger.info("PresentationServiceImpl.getPresentMaterialDto invocation start ...");
		
		List<PresentMaterialDto> presentMaterials = new ArrayList<PresentMaterialDto>();
		//ServerFolderMapping serverFolderMapping = null;
		for (HashMap<Long, PresentMaterial> m : materialList)
		{
			PresentMaterialDto material = new PresentMaterialDto();
			Long seq = m.keySet().iterator().next();
			material.setMaterialId(m.get(seq).getMaterialId());
			//String filename = m.get(seq).getMaterialFilename();
			//String extname = filename.substring(filename.lastIndexOf("."));
			material.setFolderId(m.get(seq).getFolderId());
			material.setMaterialType(m.get(seq).getMaterialType());

			//serverFolderMapping = this.getServerFolderMappingById(m.get(seq).getFolderId());
			//String pathOnServer = appProps.getProperty(serverFolderMapping.getPathPrefixKey());
			//String materialFilename = pathOnServer + File.separator + m.get(seq).getFolderId() + File.separator + m.get(seq).getMaterialFilename();
			//material.setMaterialFilename(materialFilename);
			//material.setThumbnail(materialFilename.replace(extname, "") + "_small" + extname);
			
			material.setMaterialFilename("/presentation/get_media?material_id="+material.getMaterialId()+"&media_type="+material.getMaterialType());
			material.setThumbnail("/presentation/get_thumbnail?material_id="+material.getMaterialId()+"&media_type="+material.getMaterialType());
			
			material.setMaterialSeq(new Long(seq));
			material.setStatus(m.get(seq).getStatus());
			presentMaterials.add(material);
		}
		
		logger.info("PresentationServiceImpl.getPresentMaterialDto invocation end ...");
		return presentMaterials;

	}

	/**
	 * if the presentMaterials is null then the returned Present Dto will not includes the materials 
	 * @return PresentationWithMaterialDto
	 */
	@Transactional
	public PresentationWithMaterialDto getPresentDto(PresentationBatch present, List<PresentMaterialDto> presentMaterials)
	{
		logger.info("PresentationServiceImpl.getPresentDto invocation start ...");
		
		PresentationWithMaterialDto pDetail = new PresentationWithMaterialDto();
		pDetail.setPresentName(present.getPresentName());
		pDetail.setCreateDate(present.getCreateDate().toString());
		pDetail.setDescription(present.getDescription());
		pDetail.setUpdateDate(present.getUpdateDate().toString());
		pDetail.setPresentId(present.getPresentId());
		if (presentMaterials != null)
		{
			/*for(PresentMaterialDto pm : presentMaterials){
				if(null != pm.getFolderId()){
					Long folderId = pm.getFolderId();
					ServerFolderMapping folderMapping = getServerFolderMappingById(folderId);
					if(null != folderMapping){
						if(StringUtils.isNotEmpty(folderMapping.getPathPrefixKey())){
							pm.setThumbnail(folderMapping.getPathPrefixKey()+pm.getMaterialFilename());
						}
					}
				}
			}*/
			Collections.sort(presentMaterials, new Comparator<PresentMaterialDto>(){
				@Override
				public int compare(PresentMaterialDto o1, PresentMaterialDto o2) {
					// TODO Auto-generated method stub
					if(o1.getMaterialSeq() < o2.getMaterialSeq()){
						return -1;
					}else if(o1.getMaterialSeq() > o2.getMaterialSeq()){
						return 1;
					}else{
						return 0;
					}
				}});
			
			pDetail.setPresentMaterials(presentMaterials);
		}
		
		logger.info("PresentationServiceImpl.getPresentDto invocation end ...");
		return pDetail;
	}
	
	@Transactional
	public List<PresentMaterialSeq> getPresentMaterialSeqByPresentId(Long presentId){
		return presentationMaterialsSeqDao.getListByPresentId(presentId);
	}

}