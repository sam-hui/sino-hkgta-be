package com.sinodynamic.hkgta.service.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.MsgPushHistoryDao;
import com.sinodynamic.hkgta.dao.crm.UserDeviceDao;
import com.sinodynamic.hkgta.dto.push.PushDto;
import com.sinodynamic.hkgta.dto.push.PushResultDto;
import com.sinodynamic.hkgta.dto.push.PushTopicDto;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.MsgPushHistory;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.integration.push.service.PushService;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service("synchronizedPushService")
public class DevicePushServiceImpl extends ServiceBase<UserDevice>implements DevicePushService {
	
	@Autowired
	private UserDeviceDao userDeviceDao;

	@Autowired
	private PushService pushService;
	
	@Autowired
	private MsgPushHistoryDao msgPushHistoryDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Override
	@Transactional
	public void pushRegister(String userId, String deviceArn, String token, String platform, String application, String version,
			String updatedBy) {
		
		UserDevice userDevice = userDeviceDao.getUserDeviceByApplication(userId, application);
		
		if (userDevice != null) {
			setUserDevice(userDevice, userId, deviceArn, token, platform, version, updatedBy, application);
			userDeviceDao.update(userDevice);
		} else {
			userDevice = new UserDevice();
			setUserDevice(userDevice, userId, deviceArn, token, platform, version, updatedBy,  application);
			userDeviceDao.saveOrUpdate(userDevice);
		}
	}

	private void setUserDevice(UserDevice ud, String userId, String deviceArn, String token, String platform,
			String version, String updatedBy, String application) {
		ud.setArnApplication(application);
		ud.setArnEndpoint(deviceArn);
		ud.setCreateBy(updatedBy);
		Date date = new Date();
		ud.setCreateDate(date);
		ud.setDeviceToken(token);
		ud.setPlatform(platform);
		ud.setStatus("ACT");
		ud.setSystemVersion(version);
		ud.setUpdateBy(updatedBy);
		ud.setUserId(userId);
		ud.setUpdateDate(date);
	}

	@Override
	@Transactional
	public int deleteUserDevice(String userId,String arnApplication)
	{
		return userDeviceDao.deleteUserDevice(userId,arnApplication);
	} 

	@Deprecated
	@Override
	@Transactional
	public void pushMessage(String[] userIds, String message, String application) {
		pushMessage(userIds, null, message, application);
	}
	
	@Override
	@Transactional
	public void pushMessage(String[] userIds, String subject, String message, String application) {
		if (userIds == null || userIds.length == 0 || StringUtils.isBlank(message) == true)
			return;
		List<Serializable> param = new ArrayList<Serializable>();
		StringBuffer sb = new StringBuffer();
		sb.append("from UserDevice where status='ACT' and userId in(");
		for (int i = 0; i < userIds.length; i++) {
			sb.append("?,");
			param.add(userIds[i]);
		}
		String hql = sb.substring(0, sb.length() - 1) + ") and arnApplication =? ";
		param.add(application);
		List<UserDevice> ls = userDeviceDao.getByHql(hql, param);
		if (null != ls && ls.size() > 0) {
		logger.info(DevicePushServiceImpl.class.getName() + " pushMessage start!");  	
		    	String content = null;
		    	//String subject = null;
		    	if (null == subject || StringUtils.isEmpty(subject))
		    	{
		    		if (!StringUtils.isEmpty(message)) {
		    			String[] messages = message.split("\\|");
		    			if (messages.length > 0) content = messages[0];
		    			if (messages.length > 1) subject = messages[1];
		    		}
		    	}
		    	else
		    	{
		    		content = message;
		    	}
		    	
		    	
			for (UserDevice d : ls) {
				PushDto dto = new PushDto();
				dto.setDeviceArn(d.getArnEndpoint());
				dto.setMessage(content);
				try {
				    
				    MsgPushHistory msgPushHistory = new MsgPushHistory();
				    msgPushHistory.setRecipientDeviceId(d.getSysId());
				    msgPushHistory.setContent(content);
				    msgPushHistory.setSubject(subject);
				    msgPushHistory.setCreateBy("system");
				    msgPushHistory.setCreateDate(new Date());
				    msgPushHistory.setRetryCount(0L);
				    msgPushHistoryDao.addMsgPushHistory(msgPushHistory);
				    
				    PushResultDto resultDto = pushService.push(dto);
				    if (resultDto != null) {
					msgPushHistory.setResponseCode(resultDto.getErrorCode());
					msgPushHistory.setStatus(resultDto.getStatus());
					msgPushHistoryDao.modifyMsgPushHistory(msgPushHistory);
				    }
				    logger.info(DevicePushServiceImpl.class.getName() + " pushMessage end!");
				} catch (Exception e) {
					logger.error(DevicePushServiceImpl.class.getName() + content + " pushMessage Failed!", e);
				}
			}
		}
	}
	
	/**
	 * for mobile notification
	 */
	@Override
	@Transactional
	public void pushMessage(String[] userIds, String functionId, String[] replaceParams, String application) {
		try {
			if (userIds == null || userIds.length == 0 || StringUtils.isBlank(functionId) == true )
				return;
			
			MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(functionId);
			if (template == null) return;
			String macroTag = template.getMacroTag();
			String message = template.getContent();
			if(!StringUtil.isBlank(macroTag)){
				String[] tags = macroTag.split(",");
				for(int i=0;i<tags.length;i++)
				message = message.replace(tags[i], replaceParams[i]);
			}
			
			List<Serializable> param = new ArrayList<Serializable>();
			StringBuffer sb = new StringBuffer();
			sb.append("from UserDevice where status='ACT' and userId in(");
			for (int i = 0; i < userIds.length; i++) {
				sb.append("?,");
				param.add(userIds[i]);
			}
			String hql = sb.substring(0, sb.length() - 1) + ") and arnApplication =? ";
			param.add(application);
			List<UserDevice> ls = userDeviceDao.getByHql(hql, param);
			if (null != ls && ls.size() > 0) {
				for (UserDevice d : ls) {
					PushDto dto = new PushDto();
					dto.setDeviceArn(d.getArnEndpoint());
					dto.setMessage(message);
				    MsgPushHistory msgPushHistory = new MsgPushHistory();
				    msgPushHistory.setRecipientDeviceId(d.getSysId());
				    msgPushHistory.setContent(message);
				    msgPushHistory.setSubject(template.getMessageSubject());
				    msgPushHistory.setCreateBy("system");
				    msgPushHistory.setCreateDate(new Date());
				    msgPushHistory.setRetryCount(0L);
				    msgPushHistoryDao.addMsgPushHistory(msgPushHistory);
				    
				    PushResultDto resultDto = pushService.push(dto);
				    if (resultDto != null) {
						msgPushHistory.setResponseCode(resultDto.getErrorCode());
						msgPushHistory.setStatus(resultDto.getStatus());
						msgPushHistoryDao.modifyMsgPushHistory(msgPushHistory);
				    }
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(DevicePushServiceImpl.class.getName() + " pushMessage Failed!", e);
		}
	}
	@Override
	public void broadcastMessage(String topicArn, String message)
	{
		PushTopicDto dto = new PushTopicDto();
		dto.setMessage(message);
		dto.setTopicArn(topicArn);
		try{
			pushService.pushTopic(dto);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Broadcast message failed! topicArn: %s , message: %s , error message: %s", topicArn, message, e.getMessage()));
		}
	}
}
