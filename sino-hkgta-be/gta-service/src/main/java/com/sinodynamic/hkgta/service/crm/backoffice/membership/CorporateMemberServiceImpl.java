package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.runtime.DotClass;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.pms.RoomTypeDao;
import com.sinodynamic.hkgta.dto.crm.MemberTransactionDto;
import com.sinodynamic.hkgta.dto.membership.SearchSpendingSummaryDto;
import com.sinodynamic.hkgta.dto.membership.SpendingSummaryDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CorporateProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.pms.RoomType;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant.PriceCatagory;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Service
public class CorporateMemberServiceImpl extends ServiceBase<CorporateMember> implements CorporateMemberService {
	@Autowired
	private CorporateMemberDao corporateMemberDao;
	
	@Autowired
	private RoomTypeDao roomTypeDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CorporateProfileDao corporateProfileDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Override
	@Transactional
	public ResponseResult getSpendingSummary(ListPage<CorporateMember> page,
			SearchSpendingSummaryDto dto) {
		ListPage<CorporateMember> listPage = corporateMemberDao.getSpendingSummary(page,dto);
		BigDecimal totalSpending = corporateMemberDao.getTotalSpending(page,dto); 
		List<Object> spendingSummaryDtos = listPage.getDtoList();
//		for(Object obj :spendingSummaryDtos){
//			SpendingSummaryDto temp = (SpendingSummaryDto)obj;
//			if(PriceCatagory.PMS_ROOM.name().equals(temp.getItemCatagory())){
//				RoomType roomType= roomTypeDao.getRoomTypeByCode(temp.getDescription());
//				if(roomType != null){
//					temp.setDescription(roomType.getTypeName());
//				}
//			}
//		}
		for(Object object:spendingSummaryDtos){
			SpendingSummaryDto temp = (SpendingSummaryDto)object;
			for(Constant.ServiceItem item : Constant.ServiceItem.values()){
				if(item.name().equals(temp.getItemCatagory())){
					temp.setItemCatagory(Constant.ServiceItem.valueOf(temp.getItemCatagory()).toString());
				}
			}
		}
		Data data = new Data();
		data.setList(spendingSummaryDtos);
		data.setTotalPage(page.getAllPage());;
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("list", data);
		responseMap.put("totalSpending", null == totalSpending ?"":totalSpending.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
		responseResult.initResult(GTAError.Success.SUCCESS, responseMap);
		return responseResult;
	}

	@Override
	@Transactional
	public CorporateMember getCorporateMemberById(Long customerId) {
		return corporateMemberDao.getCorporateMemberById(customerId);
	}

	@Override
	@Transactional
	public ResponseResult sendSpendingSummaryReportEmail(ListPage page, SearchSpendingSummaryDto dto) {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String parentjasperPath = reportPath + "SpendingSummary.jasper";
		File reFile = new File(parentjasperPath);
		
		StringBuilder queryCondition = new StringBuilder(" 1 = 1 ");
		String dateFiled = dto.getDateField();
		String dateValue = dto.getDateVaule();
		String time = "";
		if (CommUtil.notEmpty(dateFiled)) {
			if (CommUtil.notEmpty(dateValue)) {
				if ("M".equalsIgnoreCase(dateFiled)) {
					String[] yearMonth = dateValue.split("/");
					queryCondition.append(" and ( YEAR(coh.order_date) = "+yearMonth[0]+" and MONTH(coh.order_date) = "+yearMonth[1]+" )");
					time = yearMonth[0] + "-" + String.format("%0" + 2 + "d", Integer.valueOf(yearMonth[1]));
				}else if ("Y".equals(dateFiled)) {
					queryCondition.append(" and  YEAR(coh.order_date) = "+dateValue+" ");
					time = dateValue;
				}
			}
		}
		String corporateId = dto.getCorporateId();
		String customerId = dto.getCustomerId();
		CustomerProfile customerProfile = null;
		CorporateProfile corporateProfile = null;
		String corporateName = "All";
		String memberName = "All";
		if (CommUtil.notEmpty(corporateId)) {
			if ("ALL".equals(corporateId)) {
				if (!CommUtil.notEmpty(customerId) || "ALL".equalsIgnoreCase(customerId)) {
					
				}else {
					queryCondition.append(" and m.customer_id = "+Long.parseLong(customerId)+" ");
					customerProfile = customerProfileDao.get(CustomerProfile.class, Long.parseLong(customerId));
					memberName = customerProfile.getSalutation()+" " + customerProfile.getGivenName() + " " +customerProfile.getSurname();
				}
			}else {
				if (CommUtil.notEmpty(customerId) && !"ALL".equalsIgnoreCase(customerId)) {
					queryCondition.append(" and cm.corporate_id = "+Long.parseLong(corporateId)+" and m.customer_id = " + Long.parseLong(customerId) + " ");
					corporateProfile = corporateProfileDao.get(CorporateProfile.class, Long.parseLong(corporateId));
					corporateName = corporateProfile.getCompanyName();
					customerProfile = customerProfileDao.get(CustomerProfile.class, Long.parseLong(customerId));
					memberName = customerProfile.getSalutation()+" " + customerProfile.getGivenName() + " " +customerProfile.getSurname();
				}else {
					queryCondition.append(" and cm.corporate_id = "+Long.parseLong(corporateId)+" ");
					corporateProfile = corporateProfileDao.get(CorporateProfile.class, Long.parseLong(corporateId));
					corporateName = corporateProfile.getCompanyName();
				}
			}
		}else {
			if (!CommUtil.notEmpty(customerId) || "ALL".equalsIgnoreCase(customerId)) {
				
			}else {
				queryCondition.append(" and m.customer_id = "+Long.parseLong(customerId)+" ");
				customerProfile = customerProfileDao.get(CustomerProfile.class, Long.parseLong(customerId));
				memberName = customerProfile.getSalutation()+" " + customerProfile.getGivenName() + " " +customerProfile.getSurname();
			}
		}
			
//		sql = sql.replace("<QueryCondition>", queryCondition.toString());
		
 		if(!StringUtils.isBlank(page.getCondition())){
 			queryCondition.append(" and "+page.getCondition()) ;
		}
 		
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(corporateMemberDao.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("corporateName", corporateName);
		parameters.put("memberName", memberName);
		parameters.put("time", time);
		parameters.put("fileType", dto.getFileType());
		parameters.put("QueryCondition", queryCondition.toString());
		
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(dto.getSortBy());
		if("true".equalsIgnoreCase(dto.getIsAscending())){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equalsIgnoreCase(dto.getFileType())){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		
		byte[] attachment = null;
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);
			ByteArrayOutputStream outPut = new ByteArrayOutputStream();
			JRAbstractExporter exporter = exportedByFileType(dto.getFileType(), jasperPrint, outPut);
			if (exporter == null) {
				return null;
			} else {
				exporter.exportReport();
				attachment = outPut.toByteArray();
			}
		} catch (JRException e) {
			e.printStackTrace();
		}
		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId("corporate_spending_summary");
		
		List<String> fileNameList = new ArrayList<String>();
		if ("csv".equalsIgnoreCase(dto.getFileType())) {
			fileNameList.add("SpendingSummary("+corporateName+"-"+memberName+") - "+time+".csv");
		}else {
			fileNameList.add("SpendingSummary("+corporateName+"-"+memberName+") - "+time+".pdf");
		}
		List<String> mineTypeList = new ArrayList<String>();
		if ("csv".equalsIgnoreCase(dto.getFileType())) {
			mineTypeList.add("text/csv");
		}else{
			mineTypeList.add("application/pdf");
		}
		
		List<byte[]> bytesList = new ArrayList<byte[]>();
//		bytesList.add(out.toByteArray());
		bytesList.add(attachment);
		String content = template.getFullContentHtml(dto.getEmailTo(),time,dto.getUserName());
		String subject = template.getMessageSubject();
		String email = dto.getEmail();
		String userId = dto.getUserId();
		
		
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		
		customerEmailContent.setContent(content);
		customerEmailContent.setRecipientCustomerId(customerId.toString());
		customerEmailContent.setRecipientEmail(email);
		customerEmailContent.setSendDate(new Date());
		customerEmailContent.setSenderUserId(userId);
		customerEmailContent.setSubject(subject);
		customerEmailContent.setStatus(EmailStatus.PND.name());
		String sendId = (String) customerEmailContentDao.save(customerEmailContent);
		customerEmailContent.setSendId(sendId);
		
		CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
		customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
		customerEmailAttach.setAttachmentName(fileNameList.get(0));
		customerEmailAttachDao.save(customerEmailAttach);
		mailThreadService.sendWithResponse(customerEmailContent, bytesList, mineTypeList, fileNameList);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		
		return responseResult;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JRAbstractExporter exportedByFileType(String fileType, JasperPrint jasperPrint, ByteArrayOutputStream outPut) {
		JRAbstractExporter exporter = null;
		switch (fileType) {
		case "pdf":
			exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
			break;
		case "csv":
			exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
			break;
		default:
		}

		return exporter;
	}
}
