package com.sinodynamic.hkgta.service.fms;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.fms.CourseDao;
import com.sinodynamic.hkgta.dao.fms.CourseEnrollmentDao;
import com.sinodynamic.hkgta.dao.fms.CourseMasterDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.fms.CourseInfoDto;
import com.sinodynamic.hkgta.dto.fms.CourseReservationDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionSmsParamDto;
import com.sinodynamic.hkgta.dto.fms.CourseSimpleInfoDto;
import com.sinodynamic.hkgta.dto.fms.CourseTransactionDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CourseStatus;
import com.sinodynamic.hkgta.util.constant.CourseType;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EnrollCourseStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberAcceptance;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * @description Service for functions which operation related to course
 *              enrollment
 * @author Mianping_Wu
 * @date 7/13/2015
 */
@Service
public class CourseEnrollmentServiceImpl extends ServiceBase<CourseEnrollment>
		implements CourseEnrollmentService {

	private Logger logger = Logger.getLogger(CourseEnrollmentServiceImpl.class);
	
	// email content placeholder
	private static final String OPERATION_SCENCE_USERNAME = "{username}";
	private static final String OPERATION_SCENCE_MEMBERNAME = "{membername}";
	private static final String OPERATION_SCENCE_ENROLLID = "{enrollid}";
	private static final String OPERATION_SCENCE_COURSENAME = "{coursename}";
	private static final String OPERATION_SCENCE_BEGINDATE = "{begindate}";
	private static final String OPERATION_SCENCE_DUEDATE = "{duedate}";
	private static final String OPERATION_SCENCE_PRICE = "{price}";
	private static final String MEMBER_TYPE_MEMBER = "member";
	private static final String MEMBER_TYPE_COACH = "coach";

	@Autowired
	private CourseEnrollmentDao courseEnrollmentDao;

	@Autowired
	private MessageTemplateDao templateDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;

	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;

	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;

	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;

	@Autowired
	private GlobalParameterDao globalParameterDao;

	@Autowired
	private CourseMasterDao courseMasterDao;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	// ACT-accepted; REG-registered; PND-pending;REJ-rejected;CAN-Cancelled
	private static final List<String> COURSE_ENROLL_STATUS = Arrays.asList(
			"ACT", "REG", "PND", "REJ", "CAN");

	@Transactional
	public ResponseResult modifyCourseEnrollmentStatus(String enrollId,
			String status, String userName, String customerReason,
			String cancelRequesterType, boolean refundFlag) throws Exception {

		CourseEnrollment ce = courseEnrollmentDao.getCourseEnrollmentById(enrollId);
		if (ce == null) {
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_EXIST);
			return responseResult;
		}

		if (!COURSE_ENROLL_STATUS.contains(status)) {
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_ILLEGAL_STATUS);
			return responseResult;
		}
		

		String oldStatus = ce.getStatus();
		CourseMaster cm = courseDao.getCourseMasterById(ce.getCourseId());
		if ("ACT".equals(status) && "PND".equals(oldStatus) && (CourseStatus.FULL.getType().equalsIgnoreCase(cm.getStatus()))) {
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_FULL);
			return responseResult;
		}


		// cancel operation
		if ("CAN".equals(status)) {
			
			ce.setInternalRemark(customerReason);
			ce.setCancelRequesterType(cancelRequesterType);
			
		} else {
			// if ACT can only change to REJ/PND
			if ("ACT".equals(oldStatus)
					&& !("REJ".equals(status) || "PND".equals(status))) {
				responseResult
						.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_BAD_SWITCH);
				return responseResult;
			}

			// If PND can only change to ACT/REJ
			if ("PND".equals(oldStatus)
					&& !("ACT".equals(status) || "REJ".equals(status))) {
				responseResult
						.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_BAD_SWITCH);
				return responseResult;
			}
		}
		
		//Course status change when course-enrollment status change
		if ("ACT".equals(status) && "PND".equals(oldStatus) && (CourseStatus.OPEN.getType().equalsIgnoreCase(cm.getStatus()))) {
		  //如果满员了改变course status
		    Long capacity = cm.getCapacity();
		    int currentEnrollMembers = courseEnrollmentDao.countByCourseId(ce.getCourseId());
		    if(capacity <= (currentEnrollMembers + 1)){
		    	cm.setStatus(CourseStatus.FULL.getType());
		    	cm.setUpdateBy(userName);
		    	cm.setUpdateDate(new Date());
		    	courseDao.updateCourseMaster(cm);
		    }
		} else if (("REJ".equals(status) || "CAN".equals(status) || "PND".equals(status)) &&
			("ACT".equals(oldStatus) || "REG".equals(oldStatus)) && CourseStatus.FULL.getType().equals(cm.getStatus())) {
		    //如果取消ｅｎｒｏｌｌ时，要重新置为ｏｐｅｎ
		    	cm.setStatus(CourseStatus.OPEN.getType());
		    	cm.setUpdateBy(userName);
		    	cm.setUpdateDate(new Date());
		    	courseDao.updateCourseMaster(cm);
		}

		Date date = new Date();
		ce.setStatus(status);
		ce.setUpdateBy(userName);
		ce.setStatusUpdateBy(userName);
		ce.setUpdateDate(date);
		ce.setStatusUpdateDate(date);
		courseEnrollmentDao.updateCourseEnrollment(ce);

		// REG --> REJ or REG --> CAN need to create a refund request if needed
		if ("REG".equals(oldStatus) && ("REJ".equals(status) || "CAN".equals(status))) {
			
			CourseInfoDto dto =  new CourseInfoDto();
			dto.setRefundFlag(refundFlag);
			dto.setCancelRequesterType(cancelRequesterType);
			dto.setInternalRemark(customerReason);
			courseService.processRefundRequest(dto, userName, ce, cm);

		}
		
		//send sms info when change
		Map<String, Object> smsInfo = null;
		if ("ACT".equals(status) && MemberAcceptance.MANUAL.getType().equals(cm.getMemberAcceptance())) {
			smsInfo = courseService.notifyEnrollMember(SceneType.COURSE_ENROLL_MANUAL_ACCEPT, ce, cm);
		} else if ("REJ".equals(status)) {
		    	smsInfo = courseService.notifyEnrollMember(SceneType.COURSE_ENROLL_REJECT, ce, cm);
		} else if ("CAN".equals(status)) {
		    	smsInfo = courseService.notifyEnrollMember(SceneType.COURSE_ENROLL_CANCEL, ce, cm);
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, smsInfo);
		return responseResult;
	}

	/**
	 * @author Mianping_Wu
	 * @param dto
	 * @date 7/14/2015
	 * @return
	 */
	@Transactional
	public ResponseResult confirmCourseReservation(CourseReservationDto dto)
			throws Exception {

		String enrollId = dto.getEnrollId();
		if (StringUtils.isEmpty(enrollId)) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_PARAMETER_NULL);
			return responseResult;
		}

		CourseEnrollment ce = courseEnrollmentDao
				.getCourseEnrollmentById(enrollId);
		if (ce == null) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_COURSE_ENROLLMENT);
			return responseResult;
		}

		CourseMaster cm = courseDao.getCourseMasterById(ce.getCourseId());
		if (cm == null) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_COURSE_MASTER);
			return responseResult;
		}

		String messageType = null;
		String type = cm.getMemberAcceptance();
		if ("M".equals(type))
			messageType = "meacl";
		if ("A".equals(type))
			messageType = "aeacl";
		MessageTemplate template = templateDao.getTemplateByFunctionId(messageType);
		if (template == null) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_MESSAGE_TEMPLATE);
			return responseResult;
		}

		CustomerProfile cp = customerProfileDao.getById(ce.getCustomerId());
		if (cp == null) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_CUSTOMER_PROFILE);
			return responseResult;
		}

		CourseSimpleInfoDto courseInfoDto = null;
		courseInfoDto = courseDao
				.getCourseInfo(String.valueOf(cm.getCourseId()));

		String recipientEmail = cp.getContactEmail();
		String subjectName = template.getMessageSubject();
		String templateContent = template.getContentHtml();
		String userName = getName(cp);
		String memberName = userName;
		String courseName = cm.getCourseName();
		String beginDate = null;
		String dueDate = null;
		String price = null;
		if (courseInfoDto != null) {

			beginDate = (StringUtils.isEmpty(courseInfoDto.getBeginDate()) ? ""
					: courseInfoDto.getBeginDate());
			dueDate = (StringUtils.isEmpty(courseInfoDto.getDueDate()) ? ""
					: courseInfoDto.getDueDate());
			price = (courseInfoDto.getPrice() == null ? "" : String
					.valueOf(courseInfoDto.getPrice()));
		}

		String content = templateContent
				.replace(OPERATION_SCENCE_USERNAME, userName)
				.replace(OPERATION_SCENCE_MEMBERNAME, memberName)
				.replace(OPERATION_SCENCE_ENROLLID, enrollId)
				.replace(OPERATION_SCENCE_COURSENAME, courseName)
				.replace(OPERATION_SCENCE_BEGINDATE, beginDate)
				.replace(OPERATION_SCENCE_DUEDATE, dueDate)
				.replace(OPERATION_SCENCE_PRICE, price);

		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setNoticeType("C");
		cec.setRecipientCustomerId(String.valueOf(ce.getCustomerId()));
		cec.setRecipientEmail(recipientEmail);
		cec.setSubject(subjectName);
		cec.setSendDate(new Date());

		String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendId);

		List<CustomerEmailContent> retMap = new ArrayList<CustomerEmailContent>();
		retMap.add(cec);

		responseResult.initResult(GTAError.Success.SUCCESS, retMap);
		return responseResult;
	}

	/**
	 * @author Mianping_Wu
	 * @param dto
	 * @date 7/14/2015
	 * @return
	 */
	@Transactional
	public ResponseResult sendReceiptForCoursePayment(CourseReservationDto dto)
			throws Exception {

		String enrollId = dto.getEnrollId();
		if (StringUtils.isEmpty(enrollId)) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_PARAMETER_NULL);
			return responseResult;
		}

		CourseEnrollment ce = courseEnrollmentDao.getCourseEnrollmentById(enrollId);
		if (ce == null) {
			responseResult
					.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_COURSE_ENROLLMENT);
			return responseResult;
		}
		
		CustomerEmailContent customerEmailContent = getCourseEnrollEmailContent(ce.getCustOrderDetId());
		if (customerEmailContent != null) {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(ce.getCustOrderDetId());
			CustomerOrderTrans customerOrderTrans = customerOrderTranses.get(0);//Course Payment Order and transaction is one to one.
			byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, customerOrderTrans.getTransactionNo().toString(), "course");
			List<byte[]> attachmentList = Arrays.asList(attachment);
			List<String> mineTypeList = Arrays.asList("application/pdf");
			List<String> fileNameList = Arrays.asList("CourseEnrollmentReceipt-"+customerOrderTrans.getTransactionNo().toString()+".pdf");
			mailThreadService.sendWithResponse(customerEmailContent, attachmentList, mineTypeList, fileNameList);
		}
		/*CourseSimpleInfoDto courseInfoDto = null;
		courseInfoDto = courseDao
				.getCourseInfo(String.valueOf(cm.getCourseId()));

		String recipientEmail = cp.getContactEmail();
		String subjectName = template.getMessageSubject();
		String templateContent = template.getContent();
		String userName = getName(cp);
		String memberName = userName;
		String courseName = cm.getCourseName();
		String beginDate = null;
		String dueDate = null;
		String price = null;
		if (courseInfoDto != null) {

			beginDate = (StringUtils.isEmpty(courseInfoDto.getBeginDate()) ? ""
					: courseInfoDto.getBeginDate());
			dueDate = (StringUtils.isEmpty(courseInfoDto.getDueDate()) ? ""
					: courseInfoDto.getDueDate());
			price = (courseInfoDto.getPrice() == null ? "" : String
					.valueOf(courseInfoDto.getPrice()));
		}

		String content = templateContent
				.replace(OPERATION_SCENCE_USERNAME, userName)
				.replace(OPERATION_SCENCE_MEMBERNAME, memberName)
				.replace(OPERATION_SCENCE_ENROLLID, enrollId)
				.replace(OPERATION_SCENCE_COURSENAME, courseName)
				.replace(OPERATION_SCENCE_BEGINDATE, beginDate)
				.replace(OPERATION_SCENCE_DUEDATE, dueDate)
				.replace(OPERATION_SCENCE_PRICE, price);

		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setNoticeType("R");
		cec.setRecipientCustomerId(String.valueOf(ce.getCustomerId()));
		cec.setRecipientEmail(recipientEmail);
		cec.setSubject(subjectName);
		cec.setSendDate(new Date());

		String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendId);

		List<CustomerEmailContent> retMap = new ArrayList<CustomerEmailContent>();
		retMap.add(cec);*/

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}

	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Jul 15, 2015
	 * @Param @param cp
	 * @return String
	 */
	private String getName(CustomerProfile cp) {

		if (cp == null)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName())
				.append(" ").append(cp.getSurname());
		return sb.toString();
	}

	/**
	 * @author Mianping_Wu
	 * @Date Jul 27, 2015
	 * @param orderNo
	 * @param userId
	 * @return transactionNo
	 */
	@Transactional
	public ResponseResult courseEnrollCallBackService(Long transactionNo, String userId, String cardType) throws Exception {

	    
	    CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
	    if (customerOrderTrans == null) return null;
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		if (customerOrderHd == null) return null;
		CourseEnrollment courseEnrollment = courseEnrollmentDao.getCourseEnrollmentByOrderNo(customerOrderHd.getOrderNo());
		if (courseEnrollment != null) {
		    	
		    	CourseMaster course = courseDao.getCourseMasterById(courseEnrollment.getCourseId());
		    	if (course == null) return null;
		    	
			courseEnrollment.setStatus(EnrollCourseStatus.REG.getDesc());
			courseEnrollment.setUpdateBy(userId);
			courseEnrollment.setUpdateDate(new Date());
			courseEnrollment.setStatusUpdateDate(new Date());
			courseEnrollment.setStatusUpdateBy(userId);
			courseEnrollmentDao.updateCourseEnrollment(courseEnrollment);

			customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
			customerOrderHd.setUpdateBy(userId);
			customerOrderHd.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderHdDao.update(customerOrderHd);

			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
			customerOrderTrans.setPaymentMethodCode(cardType);
			customerOrderTrans.setAuditBy(userId);
			customerOrderTrans.setAuditDate(new Timestamp(new Date().getTime()));
			customerOrderTransDao.update(customerOrderTrans);
			
			//begin to send email
			CustomerEmailContent customerEmailContent = getCourseEnrollEmailContent(customerOrderHd.getOrderNo());
			byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, customerOrderTrans.getTransactionNo().toString(), "course");
			List<byte[]> attachmentList = Arrays.asList(attachment);
			List<String> mineTypeList = Arrays.asList("application/pdf");
			List<String> fileNameList = Arrays.asList("CourseEnrollmentReceipt-"+customerOrderTrans.getTransactionNo().toString()+".pdf");
			mailThreadService.sendWithResponse(customerEmailContent, attachmentList, mineTypeList, fileNameList);

			//begin to send sms
			Map<String, Object> smsInfo = null;
			if (MemberAcceptance.AUTO.getType().equalsIgnoreCase(course.getMemberAcceptance())) {
				smsInfo = courseService.notifyEnrollMember(SceneType.COURSE_ENROLL_AUTO_CONFIRM, courseEnrollment, course);
			} else if (MemberAcceptance.MANUAL.getType().equalsIgnoreCase(course.getMemberAcceptance())) {
				smsInfo = courseService.notifyEnrollMember(SceneType.COURSE_ENROLL_MANUAL_CONFIRM, courseEnrollment, course);
			}
					
			responseResult.initResult(GTAError.Success.SUCCESS, smsInfo);
			return responseResult;
		}
		
		return null;
	}

	public static Date getFutureDate(Date date, int noOfDays)
			throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, noOfDays);
		return format.parse(format.format(calendar.getTime()));
	}

	@Override
	@Transactional
	public String getEnrollIdBySessionIdAndAcademyCardNo(String sessionId, String cardNo) {
		
		String enrollId = courseEnrollmentDao.getEnrollIdBySessionIdAndCard(sessionId, cardNo);
		return enrollId;
	}

	@Override
	@Transactional
	public CourseTransactionDto getTransactionInfoByEnrollId(String enrollId) {
		
		if (StringUtils.isEmpty(enrollId)) return null;
		CourseTransactionDto transactionInfoDto = courseEnrollmentDao.getCustomerOrderTransByEnrollId(enrollId);
		if (transactionInfoDto != null && transactionInfoDto.getPaidAmount() != null && transactionInfoDto.getTransactionTimestamp() != null) {
			
			String expense = transactionInfoDto.getPaidAmount().setScale(2).toPlainString();
			transactionInfoDto.setExpense(expense);
			DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
			transactionInfoDto.setPaymentDate(df.format(transactionInfoDto.getTransactionTimestamp()));
		}
		
		return transactionInfoDto;
	}
	
	@Transactional
	public CustomerEmailContent getCourseEnrollEmailContent(Long orderNo) throws Exception {
		
		if (orderNo ==null ) return null;

		CourseEnrollment ce = courseEnrollmentDao.getCourseEnrollmentByOrderNo(orderNo);
		if (ce == null) throw new Exception("Database data error for sending receipt!");
		
		CourseMaster cm = courseDao.getCourseMasterById(ce.getCourseId());
		if (cm == null) throw new Exception("Database data error for sending receipt!");

		CustomerProfile cp = customerProfileDao.getById(ce.getCustomerId());
		if (cp == null) throw new Exception("Database data error for sending receipt!");
		
		String functionId = null;
		if (MemberAcceptance.AUTO.getType().equals(cm.getMemberAcceptance())) {
		    
		    if (CourseType.GOLFCOURSE.getDesc().equals(cm.getCourseType())) functionId = "golf_course_a_enroll_receipt";
		    if (CourseType.TENNISCOURSE.getDesc().equals(cm.getCourseType())) functionId = "tennis_course_a_enroll_receipt";
			
		} else if (MemberAcceptance.MANUAL.getType().equals(cm.getMemberAcceptance())) {
		    
		    if (CourseType.GOLFCOURSE.getDesc().equals(cm.getCourseType())) functionId = "golf_course_m_enroll_receipt";
		    if (CourseType.TENNISCOURSE.getDesc().equals(cm.getCourseType())) functionId = "tennis_course_m_enroll_receipt";
		}
		
		MessageTemplate template = templateDao.getTemplateByFunctionId(functionId);
		if (template == null) throw new Exception("Database data error for sending receipt!");

		CourseSimpleInfoDto courseInfoDto = courseDao.getCourseInfo(String.valueOf(cm.getCourseId()));

		String recipientEmail = cp.getContactEmail();
		String subjectName = template.getMessageSubject();
		String userName = getName(cp);
		String memberName = userName;
		String courseName = cm.getCourseName();
		String beginDate = null;
		String dueDate = null;
		String price = null;
		if (courseInfoDto != null) {

			beginDate = (StringUtils.isEmpty(courseInfoDto.getBeginDate()) ? "" : courseInfoDto.getBeginDate());
			dueDate = (StringUtils.isEmpty(courseInfoDto.getDueDate()) ? "" : courseInfoDto.getDueDate());
			price = (courseInfoDto.getPrice() == null ? "" : String.valueOf(courseInfoDto.getPrice()));
		}
		
		String comfirmationID = null;
		if (CourseType.GOLFCOURSE.getDesc().equals(cm.getCourseType())) comfirmationID = "GCE-" + ce.getEnrollId();
		if (CourseType.TENNISCOURSE.getDesc().equals(cm.getCourseType())) comfirmationID = "TCE-" + ce.getEnrollId();

//		String content = templateContent
//				.replace(OPERATION_SCENCE_USERNAME, userName)
//				.replace(OPERATION_SCENCE_MEMBERNAME, memberName)
//				.replace(OPERATION_SCENCE_ENROLLID, ce.getEnrollId())
//				.replace(OPERATION_SCENCE_COURSENAME, courseName)
//				.replace(OPERATION_SCENCE_BEGINDATE, beginDate)
//				.replace(OPERATION_SCENCE_DUEDATE, dueDate)
//				.replace(OPERATION_SCENCE_PRICE, price);

		String content = template.getFullContentHtml(userName, memberName, comfirmationID, courseName, beginDate, dueDate, price);
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setStatus(EmailStatus.PND.name());
		cec.setNoticeType(Constant.NOTICE_TYPE_COURSE);
		cec.setRecipientCustomerId(String.valueOf(ce.getCustomerId()));
		cec.setRecipientEmail(recipientEmail);
		cec.setSubject(subjectName);
		cec.setSendDate(new Date());

		String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendId);
		return cec;

	}

	@Override
	@Transactional
	public Map<String, Object> notifyEnrollMember(SceneType sceneType, CourseEnrollment ce, CourseMaster course) {
		return courseService.notifyEnrollMember(sceneType, ce, course);
	}

	@Override
	@Transactional
	public String getEnrollIdBySessionIdAndAcademyNo(String sessionId, String academyNO) {
	    
		String enrollId = courseEnrollmentDao.getEnrollIdBySessionIdAndMember(sessionId, academyNO);
		return enrollId;
	}

	@Override
	@Transactional
	public Map<String, List<UserDevice>> getAppMemberAndReminderMsg(Date begin, Date end, String application) throws Exception {
	    
	    if (begin == null || end == null || StringUtils.isEmpty(application)) return null;
	    
	    List<CourseSessionSmsParamDto> sessions = courseEnrollmentDao.getSessionDuringPeriod(begin, end);
	    if (sessions == null || sessions.size() == 0) return null;
	    
	    Map<String, List<UserDevice>> container = new HashMap<String, List<UserDevice>>();
	    for (CourseSessionSmsParamDto session : sessions) {
		
		String message = getSmsContent(session, MEMBER_TYPE_MEMBER);
		if (StringUtils.isEmpty(message)) continue;
		
		if(logger.isDebugEnabled())
		{
			logger.debug("message for member:" + message);
		}
		List<UserDevice> users = courseEnrollmentDao.getAppUser2Remind(session.getSysId(), application);
		if (users == null || users.size() == 0) continue;
		if(logger.isDebugEnabled())
		{
			logger.debug("user size:" + users.size());
		}
		container.put(message, users);
	    }
	    
	    return container;
	}
	
	@Override
	@Transactional
	public Map<String, Set<String>> getAppCoachAndReminderMsg(Date begin, Date end, String application) throws Exception {
	    
	    if (begin == null || end == null || StringUtils.isEmpty(application)) return null;
	    
	    List<CourseSessionSmsParamDto> sessions = courseEnrollmentDao.getSessionDuringPeriod(begin, end);
	    
	    Map<String, Set<String>> container = new HashMap<String,Set<String>>();
	    
	    if (sessions == null || sessions.size() == 0) return null;
	    
		for (CourseSessionSmsParamDto session : sessions) {
			
			if(logger.isDebugEnabled())
			{
				logger.debug("session:" + session.getCourseName() + " coach:" + session.getCoachUserId());
			}
			  String message = getSmsContent(session,MEMBER_TYPE_COACH);
			  if (StringUtils.isEmpty(message))
					continue;
			  
			  if (logger.isDebugEnabled()) {
					logger.debug("push message for coach:" + message);
					logger.debug("coachUserId:" + session.getCoachUserId());
				}			  
				  Set<String> coachList = new HashSet<String>();
				  coachList.add(session.getCoachUserId());
				  container.put(message, coachList);		  		
		}
	    
	    return container;
	}
	
	
	private String getSmsContent(CourseSessionSmsParamDto dto, String type) throws Exception {
	    
	    String courseType = dto.getCourseType();
	    String functionId = null;
	    if(MEMBER_TYPE_COACH.equals(type))
	    {
			if (CourseType.GOLFCOURSE.getDesc().equalsIgnoreCase(courseType)) {
				functionId = "app_golf_course_reminder";
			} else if (CourseType.TENNISCOURSE.getDesc().equalsIgnoreCase(courseType)) {
				functionId = "app_tennis_course_reminder";
			}
		}else if(MEMBER_TYPE_MEMBER.equals(type))
		{
			if (CourseType.GOLFCOURSE.getDesc().equalsIgnoreCase(courseType)) {
				functionId = "golf_course_session_reminder";
			} else if (CourseType.TENNISCOURSE.getDesc().equalsIgnoreCase(courseType)) {
				functionId = "tennis_course_session_reminder";
			}
		}
	    MessageTemplate template = templateDao.getTemplateByFunctionId(functionId);
	    if (template == null) throw new Exception("Database data error for sending receipt!");
	    
	    return template.getFullContent(dto.getCourseName(), DateCalcUtil.formatDatetime(dto.getSessionDate()), dto.getFacility(), dto.getCoach()) + "|" + template.getMessageSubject();
	}
	
}