package com.sinodynamic.hkgta.service.mms;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;
import com.sinodynamic.hkgta.integration.spa.request.AddGuestRequest;
import com.sinodynamic.hkgta.integration.spa.request.UpdateGuestRequest;
import com.sinodynamic.hkgta.integration.spa.response.RegistGuestResponse;
import com.sinodynamic.hkgta.integration.spa.response.UpdateGuestResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class MemberSynchronizeServiceImpl implements MemberSynchronizeService {
    
    private Logger logger = Logger.getLogger(MemberSynchronizeServiceImpl.class);
    
    @Autowired
    private SpaMemberSyncDao spaMemberSyncDao;
    
    @Autowired
    private SpaServcieManager spaServcieManager;
    
    @Override
    @Transactional
    public void synchronizeGtaMember2Mms() {
	
	try {
	    
	    List<GuestRequestDto> guestToSyncList = spaMemberSyncDao.getMemberInfoList2Synchronize(Constant.STATUS_WAITING_TO_SYNCHRONIZE);
	    if (guestToSyncList == null || guestToSyncList.size() == 0) return;
	    for (GuestRequestDto dto : guestToSyncList) {
		
		String action = dto.getAction();
		if (StringUtils.isEmpty(action)) continue;
		
		if (Constant.MEMBER_SYNCHRONIZE_INSERT.equals(action)) {
		    
		    AddGuestRequest guestRequest = new AddGuestRequest();
//		    guestRequest.setGuestId(String.valueOf(dto.getGuestId()));
		    guestRequest.setGuestCode(dto.getGuestCode());
		    guestRequest.setGender(dto.getGender());
		    guestRequest.setFirstName(dto.getFirstName());
		    guestRequest.setLastName(dto.getLastName());
		    guestRequest.setEmail(dto.getEmail());
		    guestRequest.setMobilePhone(dto.getMobilePhone());
		    guestRequest.setAddress1(dto.getAddress1());
		    guestRequest.setAddress2(dto.getAddress2());
		    RegistGuestResponse response = spaServcieManager.registGuest(guestRequest);
		    if (response != null) {
			SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
			entity.setRetryCount(entity.getRetryCount() + 1);
			if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
			entity.setLastResponseMsg(response.getMessage());
			if (!StringUtils.isEmpty(response.getUserId())) entity.setSpaGuestId(response.getUserId());
			entity.setUpdateDate(new Date());
			spaMemberSyncDao.update(entity);
		    }
		    
		} else if (Constant.MEMBER_SYNCHRONIZE_UPDATE.equals(action)) {
		    
		    String guestId = dto.getGuestId();
		    if (StringUtils.isEmpty(guestId)) continue;
		    UpdateGuestRequest updateGuestRequest = new UpdateGuestRequest();
		    updateGuestRequest.setGuestId(guestId);
		    updateGuestRequest.setGuestCode(dto.getGuestCode());
		    updateGuestRequest.setGender(dto.getGender());
		    updateGuestRequest.setFirstName(dto.getFirstName());
		    updateGuestRequest.setLastName(dto.getLastName());
		    updateGuestRequest.setEmail(dto.getEmail());
		    updateGuestRequest.setMobilePhone(dto.getMobilePhone());
		    updateGuestRequest.setAddress1(dto.getAddress1());
		    updateGuestRequest.setAddress2(dto.getAddress2());
		    UpdateGuestResponse response = spaServcieManager.updateGuest(updateGuestRequest);
		    if (response != null) {
			SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
			entity.setRetryCount(entity.getRetryCount() + 1);
			if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
			entity.setLastResponseMsg(response.getMessage());
			entity.setUpdateDate(new Date());
			spaMemberSyncDao.update(entity);
		    }
		}
	    }
	    
	} catch (Exception e) {
	    e.printStackTrace();
	    logger.error(e.getMessage(), e);
	    throw new RuntimeException(e);
	}
	
    }
    
}
