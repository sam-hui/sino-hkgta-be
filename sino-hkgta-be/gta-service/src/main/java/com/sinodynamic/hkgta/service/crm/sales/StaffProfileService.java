package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.service.IServiceBase;

/**
 * 
 * @author Mianping_Wu
 * @since 5/4/2015
 */
public interface StaffProfileService extends IServiceBase<StaffProfile> {
	
	public List<StaffProfile> searchAllStaffProfile(Long enrollId) throws Exception;
}
