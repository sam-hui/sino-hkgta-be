package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.RemarksDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dto.crm.RemarksDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PositionTitle;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Service
public class RemarksServiceImpl extends ServiceBase<CustomerPreEnrollStage> implements RemarksService {

	@Autowired
	private RemarksDao remarksDao;
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private UserMasterDao userMasterDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Transactional
	public ResponseResult addRemark(RemarksDto remarksDto, String loginUserId,String staffType){
		CustomerPreEnrollStage customerPreEnrollStage = new CustomerPreEnrollStage();
		Long customerId = remarksDto.getCustomerId();
		String contentRemarks = remarksDto.getContentRemarks();
	
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if(customerEnrollment==null){
			responseResult.initResult(GTAError.RemarksError.NO_ENROLLMENT);
			return responseResult;
		}
		CustomerProfile customerProfile = customerProfileDao.getById(customerId);
		String salesUserId = customerEnrollment.getSalesFollowBy();
		
		if (!salesUserId.equals(loginUserId) && ! "Manager".equalsIgnoreCase(staffType)&&!PositionTitle.SO.name().equals(staffType)){
			responseResult.initResult(GTAError.RemarksError.REMARKS_REJ);
			return responseResult;
		}
		else{
			
			if(!StringUtils.isEmpty(contentRemarks)&&contentRemarks.length()>300){
				responseResult.initResult(GTAError.RemarksError.EXCEEDCONTENTLENGTH);
				return responseResult;
			}
			customerPreEnrollStage.setCommentBy(loginUserId);
			customerPreEnrollStage.setSalesUserId(salesUserId);
			customerPreEnrollStage.setCustomerId(customerProfile.getCustomerId());
			customerPreEnrollStage.setComments(contentRemarks);
			customerPreEnrollStage.setCommentDate(new Date());
			customerPreEnrollStage.setEnrollStatus(customerEnrollment.getStatus());
			customerPreEnrollStage.setCommentStatus("NR");
		}
		remarksDao.addRemark(customerPreEnrollStage);
		//add mobile notification
		StaffProfile staffProfile = staffProfileDao.getByUserId(loginUserId);
		String staffName = null;
		if ("Manager".equalsIgnoreCase(staffType) || PositionTitle.SO.name().equals(staffType)) {
			if (staffProfile != null) {
				staffName = staffProfile.getGivenName() + " " + staffProfile.getSurname();
			} else {
				staffName = "Sales Office";
			}
			devicePushService.pushMessage(new String[] { customerPreEnrollStage.getSalesUserId() }, "new_remark_notification", new String[] { staffName, contentRemarks }, Constant.SALESKIT_PUSH_APPLICATION);
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}
	
	@Transactional
	public ResponseResult getRemarkList(ListPage<CustomerPreEnrollStage> page,
			Long customerId,String userId,String staffType) {
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if(customerEnrollment==null){
			responseResult.initResult(GTAError.RemarksError.NO_ENROLLMENT);
			return responseResult;
		}
		String salesUserId = customerEnrollment.getSalesFollowBy(); 
		
		if(StringUtils.isEmpty(salesUserId)) {
			responseResult.initResult(GTAError.RemarksError.SALESFOLLOWBY_REQ);
			return responseResult;
		}
		
		if (!salesUserId.equals(userId) && ! "Manager".equalsIgnoreCase(staffType)&&! PositionTitle.SO.name().equals(staffType)){
			responseResult.initResult(GTAError.RemarksError.VIEW_PERMISSION);
			return responseResult;
		}
		
		ListPage<CustomerPreEnrollStage> listPageRemark = remarksDao.getRemarkList(page, customerId);
		
		List<CustomerPreEnrollStage> listRemark = listPageRemark.getList();
		List<RemarksDto> remarksDtos = new ArrayList<RemarksDto>();
		
		for(CustomerPreEnrollStage stage : listRemark) {
			RemarksDto dto = new RemarksDto();
			StaffProfile staffProfile  = staffProfileDao.getByUserId(stage.getCommentBy());
			String fullName;
			
			if (staffProfile!=null){
				fullName = staffProfile.getGivenName()+" "+staffProfile.getSurname();
			}else{
				UserMaster userMaster = this.userMasterDao.get(UserMaster.class, stage.getCommentBy());
				if (userMaster.getNickname()==null || userMaster.getNickname().equals("")){
					fullName = userMaster.getLoginId();
				}else{
					fullName = userMaster.getNickname();
				}
			}
			
			dto.setCommentBy(fullName);
			dto.setCommentDate(stage.getCommentDate());
			dto.setContentRemarks(stage.getComments());
			dto.setCustomerId(stage.getCustomerId());
			dto.setCommentStatus(stage.getCommentStatus());
			remarksDtos.add(dto);
			StaffMaster sm = staffMasterDao.getByUserId(stage.getCommentBy());
			if(sm==null) {
				responseResult.initResult(GTAError.RemarksError.STAFF_MASTER_NULL);
				return responseResult;
			}
			if (!StringUtils.isEmpty(stage.getCommentStatus())) {
				if (!stage.getCommentBy().equals(userId)){
					boolean checkSalesLogin1 = sm.getPositionTitle().equalsIgnoreCase(Constant.ROLE_OFFICER)||sm.getPositionTitle().equalsIgnoreCase(PositionTitle.SO.name());
					boolean checkSalesLogin2 = staffType.equalsIgnoreCase(Constant.ROLE_SALES)||staffType.equalsIgnoreCase(PositionTitle.SP.name());
					if( checkSalesLogin1 && checkSalesLogin2){
						stage.setCommentStatus(null);
						remarksDao.saveOrUpdateRemark(stage);
					}
					
					boolean checkOfficerLogin1 = sm.getPositionTitle().equalsIgnoreCase(Constant.ROLE_SALES)||sm.getPositionTitle().equalsIgnoreCase(PositionTitle.SP.name());
					boolean checkOfficerLogin2 = staffType.equalsIgnoreCase(Constant.ROLE_OFFICER)||staffType.equalsIgnoreCase(PositionTitle.SO.name());
					if( checkOfficerLogin1 && checkOfficerLogin2 ){
						stage.setCommentStatus(null);
						remarksDao.saveOrUpdateRemark(stage);
					}
					
				}
			}
		}
		if (listRemark.size() != 0) {
			Data data = new Data();
			data.setList(remarksDtos);
			data.setLastPage(listPageRemark.isLast());
			data.setRecordCount(listPageRemark.getAllSize());
			data.setPageSize(listPageRemark.getSize());
			data.setTotalPage(listPageRemark.getAllPage());
			data.setCurrentPage(listPageRemark.getNumber());
			responseResult.initResult(GTAError.Success.SUCCESS, data);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}
	
	@Transactional
	public ResponseResult changeRemarksStatus(Long customerId,String loginUserId) {
		List<CustomerPreEnrollStage> list = remarksDao.getRemarkList(customerId);
		for (CustomerPreEnrollStage stage : list) {
			if (stage.getCommentStatus()!=null) {
				if (stage.getSalesUserId().equals(loginUserId)){
					stage.setCommentStatus(null);
					remarksDao.saveOrUpdateRemark(stage);
				}
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult checkUnreadRemark(Long customerId,String userId){
		int unread = remarksDao.countUnreadRemark(customerId,userId);
		
		if(unread>0){
			responseResult.initResult(GTAError.Success.SUCCESS, unread);;
			return responseResult;
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS, 0);;
			return responseResult;
		}
		
	}

	@Transactional
	public ResponseResult getRemarkListByDevice(ListPage<CustomerPreEnrollStage> page, Long customerId,String loginUserId,String loginDevice){
		ListPage<CustomerPreEnrollStage> listPageRemark = remarksDao.getRemarkListByDto(page, customerId);
		remarksDao.readRemarkByDevice(customerId, loginUserId, loginDevice);
		List<Object> listRemark = listPageRemark.getDtoList();
		Data data = new Data();
		data.setLastPage(listPageRemark.isLast());
		data.setRecordCount(listPageRemark.getAllSize());
		data.setPageSize(listPageRemark.getSize());
		data.setTotalPage(listPageRemark.getAllPage());
		data.setCurrentPage(listPageRemark.getNumber());
		data.setList(listRemark);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult addRemarkByDevice(RemarksDto remarksDto, String loginUserId, String loginDevice) {
		CustomerPreEnrollStage customerPreEnrollStage = new CustomerPreEnrollStage();
		Long customerId = remarksDto.getCustomerId();
		String contentRemarks = remarksDto.getContentRemarks();
		if (!StringUtils.isEmpty(contentRemarks) && contentRemarks.length() > 300) {
			responseResult.initResult(GTAError.RemarksError.EXCEEDCONTENTLENGTH);
			return responseResult;
		}
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if (customerEnrollment == null) {
			responseResult.initResult(GTAError.RemarksError.NO_ENROLLMENT);
			return responseResult;
		}
		String salesFollowBy = customerEnrollment.getSalesFollowBy();

		customerPreEnrollStage.setCommentBy(loginUserId);
		customerPreEnrollStage.setSalesUserId(salesFollowBy);
		customerPreEnrollStage.setCustomerId(customerId);
		customerPreEnrollStage.setComments(contentRemarks);
		customerPreEnrollStage.setCommentDate(new Date());
		customerPreEnrollStage.setEnrollStatus(customerEnrollment.getStatus());
		if (salesFollowBy.equals(loginUserId) && "PC".equals(loginDevice)) {
			customerPreEnrollStage.setCommentStatus(null);
		} else {
			customerPreEnrollStage.setCommentStatus("NR");
		}
		
		customerPreEnrollStage.setCommentByDevice(loginDevice);
		remarksDao.save(customerPreEnrollStage);
		
		//Mobile Notification Added when Remarks are made on PC and the Login User is not the Sales Follow By
		if (!salesFollowBy.equals(loginUserId) && "PC".equals(loginDevice)) {
			String staffName = null;
			StaffProfile staffProfile = staffProfileDao.getByUserId(loginUserId);
			if (staffProfile != null) {
				staffName = staffProfile.getGivenName() + " " + staffProfile.getSurname();
			} else {
				staffName = "Sales Office";
			}
			devicePushService.pushMessage(new String[] { customerPreEnrollStage.getSalesUserId() }, "new_remark_notification", new String[] { staffName, contentRemarks }, Constant.SALESKIT_PUSH_APPLICATION);
		}
			
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Transactional
	public ResponseResult checkUnreadRemarkByDevice(Long customerId, String loginUserId, String device) {
		Integer unread = remarksDao.countUnreadRemarkByDevice(customerId,loginUserId,device);
		
		if(unread>0){
			responseResult.initResult(GTAError.Success.SUCCESS, unread);;
			return responseResult;
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS, 0);;
			return responseResult;
		}
		
	}
	
}