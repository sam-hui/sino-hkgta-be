package com.sinodynamic.hkgta.service.rpos;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;

import org.apache.commons.io.IOUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import au.com.bytecode.opencsv.CSVWriter;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollPoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.DeliveryRecordDao;
import com.sinodynamic.hkgta.dao.crm.HomePageSummaryDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashvalueBalHistoryDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MemberTransactionLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberTypeDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.RemarksDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanOfferPosDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanPosDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerTransactionDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.PatronAccountStatementDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.SettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerTransactionListDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.dto.statement.CustomerStatementBaseInfoDto;
import com.sinodynamic.hkgta.dto.statement.RefundItemDto;
import com.sinodynamic.hkgta.dto.statement.StatementDto;
import com.sinodynamic.hkgta.dto.statement.StatementParamsDto;
import com.sinodynamic.hkgta.entity.crm.BatchSendStatementHd;
import com.sinodynamic.hkgta.entity.crm.BatchSendStatementList;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MemberType;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanOfferPos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanPos;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.memberType;
import com.sinodynamic.hkgta.util.constant.CustomerServiceStatus;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.OrderStatus;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;
import com.sinodynamic.hkgta.util.statement.CustomHeaderFooter;

@Service
@Scope("prototype")
public class CustomerOrderTransServiceImpl extends
		ServiceBase<CustomerOrderTrans> implements CustomerOrderTransService {
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	@Autowired
	private CustomerTransactionDao customerTransactionDao;
	@Autowired
	CustomerOrderHdDao customerOrderHdDao;
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
	private ServicePlanPosDao servicePlanPosDao;
	@Autowired
	private RemarksDao remarkDao;
	
	@Autowired
	private HomePageSummaryDao homePageSummaryDao;
	
	@Autowired
	private ServicePlanOfferPosDao servicePlanOfferPosDao;
	
	@Autowired
	private PaymentGatewayDao paymentGatewayDao;
	
	@Autowired 
	SessionFactory sessionFactory;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerEnrollPoDao customerEnrollPoDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MemberTypeDao memberTypeDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;

	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
	@Autowired
	private MemberCashvalueBalHistoryDao memberCashvalueBalHistoryDao;
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	
	@Autowired
	private MemberTransactionLogDao memberTransactionLogDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private DeliveryRecordDao deliveryRecordDao;
	
	@Autowired
	private ServicePlanDao servicePlanDao;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private CustomerServiceDao customerServiceDao;

	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;
	
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;

	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public ResponseResult savecustomerOrderTransService(
			CustomerOrderTransDto customerOrderTransDto, Map<String, Long> transactionNoMap) throws Exception{

		String paymentMethodCode = customerOrderTransDto.getPaymentMethodCode();
		CustomerOrderTrans customerOrderTrans = null;
		boolean newOrUpdateFileFlag = true;

		if (customerOrderTransDto.getTransactionNo() != null) {
			customerOrderTrans = this.customerOrderTransDao.get(CustomerOrderTrans.class, customerOrderTransDto.getTransactionNo());
			if ((PaymentMethod.BT.name().equalsIgnoreCase(paymentMethodCode) || PaymentMethod.CHEQUE.name().equalsIgnoreCase(paymentMethodCode)) && StringUtils.isEmpty(customerOrderTrans.getTranscriptFileName())
					&& StringUtils.isEmpty(customerOrderTransDto.getTranscriptFileName())) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.ATTACH_REQ);
			}
			String transcriptFileName = customerOrderTrans.getTranscriptFileName();
			int index = transcriptFileName.lastIndexOf("/");
			if (transcriptFileName != null && index >= 0) {
				transcriptFileName = transcriptFileName.substring(index);
			}
			String dtotTanscriptFileName = customerOrderTransDto.getTranscriptFileName();
			index = dtotTanscriptFileName.lastIndexOf("/");
			if (dtotTanscriptFileName != null && index >= 0) {
				dtotTanscriptFileName = dtotTanscriptFileName.substring(index);
			}
			// if exist transcriptFileName equals FE's transcriptFileName no,change for upload transcript file
			if (!transcriptFileName.equals(dtotTanscriptFileName)) {
				customerOrderTrans.setTranscriptFileName(customerOrderTransDto.getTranscriptFileName());
			} else {
				newOrUpdateFileFlag = false;
			}
		} else {
			customerOrderTrans = new CustomerOrderTrans();
			if ((PaymentMethod.BT.name().equalsIgnoreCase(paymentMethodCode) || PaymentMethod.CHEQUE.name().equalsIgnoreCase(paymentMethodCode)) && StringUtils.isEmpty(customerOrderTransDto.getTranscriptFileName())) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.ATTACH_REQ);
			}
			customerOrderTrans.setCreateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setCreateDate(new Date());
			customerOrderTrans.setTranscriptFileName(customerOrderTransDto.getTranscriptFileName());
		}
		CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class, customerOrderTransDto.getOrderNo());
		/*if ("CMP".equals(customerOrderHd.getOrderStatus())) {
			throw new GTACommonException(GTAError.CustomerOrderTrans.TRANS_CMP);
			//Even if the order is complete, the transaction of the order can also be void and change the order status to be OPEN
		}*/

		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setPaidAmount(customerOrderTransDto.getPaidAmount());
		customerOrderTrans.setPaymentMethodCode(customerOrderTransDto.getPaymentMethodCode());
		if (!"CASH".equalsIgnoreCase(paymentMethodCode)) {
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}
		customerOrderTrans.setTerminalId(customerOrderTransDto.getTerminalId());
		customerOrderTrans.setTransactionTimestamp(new Date());
		customerOrderTrans.setPaymentRecvBy(customerOrderTransDto.getPaymentReceivedBy());
		customerOrderTrans.setAgentTransactionNo(customerOrderTransDto.getAgentTransactionNo());
		
		//Validation input
		if (customerOrderTrans.getPaidAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new GTACommonException(GTAError.CustomerOrderTrans.PAID_AMOUNT_LESS_ZERO);
		}
		List<String> validStatusList = Arrays.asList(null,"","SUC","FAIL","VOID");
		if (validStatusList.contains(customerOrderTrans.getStatus()) == false) {
			throw new GTACommonException(GTAError.CustomerOrderTrans.INACCURATE_STATUS);
		}

		//Payment total checking
		BigDecimal balanceDueSalesMan = homePageSummaryDao.countBalanceDue(customerOrderTransDto.getOrderNo());
		BigDecimal balanceDueAccountant = customerTransactionDao.countBalanceDueStatus(customerOrderTransDto.getOrderNo());
		if (!StringUtils.isEmpty(customerOrderTransDto.getTransactionNo())) {
			CustomerOrderTrans customerOrderTransOld = customerOrderTransDao.get(CustomerOrderTrans.class, customerOrderTransDto.getTransactionNo());
			if (customerOrderTransDto.getStatus().equals("SUC")) {
				if (balanceDueAccountant.subtract(customerOrderTrans.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
					throw new GTACommonException(GTAError.CustomerOrderTrans.PAID_AMOUNT_OVER,new Object[]{balanceDueSalesMan});
				}
			} else if (!StringUtils.isEmpty(customerOrderTransDto.getStatus())) {
				if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount()).add(customerOrderTransOld.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
					throw new GTACommonException(GTAError.CustomerOrderTrans.PAYMENT_AMOUNT_OVER,new Object[]{balanceDueSalesMan});
				}
			}
			customerOrderTransDao.evict(customerOrderTransOld);
		}
		if (customerOrderTransDto.getTransactionNo() == null) {// Add new payment case
			if (balanceDueSalesMan.compareTo(BigDecimal.ZERO) <= 0) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.NO_PAYMENT_REQ);
			} else if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.PAID_AMOUNT_OVER,new Object[]{balanceDueSalesMan});
			}
		}

		//Audit Action for Verify
		boolean auditAction = PaymentMethod.BT.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
				              ||PaymentMethod.CHEQUE.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
				              ||PaymentMethod.VISA.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
				              ||PaymentMethod.CASH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode());
		if(customerOrderTransDto.getTransactionNo() != null&&auditAction){
			customerOrderTrans.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderTrans.setAuditBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setAuditDate(new Timestamp(new Date().getTime()));
		}
		
		// there is no approved for CASH
		if ("CASH".equalsIgnoreCase(paymentMethodCode) && !"VOID".equals(customerOrderTransDto.getStatus())) {
			customerOrderTrans.setStatus(CustomerTransationStatus.SUC.name());
			customerOrderTrans.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderTrans.setAuditBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setAuditDate(new Timestamp(new Date().getTime()));
		} else {
			customerOrderTrans.setStatus(customerOrderTransDto.getStatus());
		}
		
		
		//Append payment location code 
		if(StringUtils.isEmpty(customerOrderTrans.getPaymentLocationCode())){
			customerOrderTrans.setPaymentLocationCode(customerOrderTransDto.getPaymentLocationCode());
		}
		
		
		// end of payment total checking
		Long transNo = null;
		if (null == customerOrderTrans.getTransactionNo() || 0 == customerOrderTrans.getTransactionNo()) {
			transNo = (Long) customerOrderTransDao.save(customerOrderTrans);
		} else {
			customerOrderTransDao.saveOrUpdate(customerOrderTrans);
			transNo = customerOrderTransDto.getTransactionNo();
		}

		// Used to change enrollment status from Approved to Payment Approved or Payment Approved to Approved
		updateEnrollmentStatusOncePayementChange(transNo);

		transactionNoMap.put("transactionNo", customerOrderTrans.getTransactionNo());
		transactionNoMap.put("customerId", customerOrderHd.getCustomerId());
		if (null != customerOrderTrans.getTransactionNo() && null != customerOrderHd.getCustomerId() && newOrUpdateFileFlag && !PaymentMethod.CASH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())) {
			this.moveTranscriptFile(customerOrderTrans.getTransactionNo(), customerOrderHd.getCustomerId());
		}

		CustomerProfile temProfile = customerProfileDao.getCustomerProfileByCustomerId(customerOrderHd.getCustomerId());
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerOrderHd.getCustomerId());
		if (CustomerTransationStatus.SUC.name().equals(customerOrderTrans.getStatus())) {
			confirmSendEmailAfterPaymentForEnrollmentAndRenewal(transNo, EmailType.RECEIPT.name(), customerOrderTransDto.getCreateByUserId(), customerOrderTransDto.getCreateByUserName());
			//add mobile notification
			devicePushService.pushMessage(new String[]{customerEnrollment.getSalesFollowBy()},"transaction_success_notification",new String[]{temProfile.getGivenName()+ " " +temProfile.getSurname()},Constant.SALESKIT_PUSH_APPLICATION);
		}else if(CustomerTransationStatus.FAIL.name().equals(customerOrderTrans.getStatus())|| CustomerTransationStatus.VOID.name().equals(customerOrderTrans.getStatus())){
			devicePushService.pushMessage(new String[]{customerEnrollment.getSalesFollowBy()},"transaction_fail_notification",new String[]{temProfile.getGivenName()+ " " +temProfile.getSurname()},Constant.SALESKIT_PUSH_APPLICATION);
		}
		//If the total amount of all transactions of CustomerOrderHd is equals to CustomerOrderHd's amount, the CustomerOrderHd's status should be changed to Complete(CMP)
		List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (CustomerOrderTrans tempTrans: customerOrderTranses) {
			if (CustomerTransationStatus.SUC.getDesc().equals(tempTrans.getStatus())) {
				totalAmount = totalAmount.add(tempTrans.getPaidAmount());
			}
		}
		
		if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount)==0) {
			customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
			customerOrderHd.setUpdateDate(new Date());
			customerOrderHd.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderHdDao.update(customerOrderHd);
			List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
			if (customerOrderDets.size() > 0 && customerOrderDets.get(0).getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)){
				handleRenewPayment(customerOrderHd,customerOrderTransDto.getCreateByUserId());
			}
		}else if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount)!=0
				&& !OrderStatus.REJ.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())
				&& !OrderStatus.CAN.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())) {
			customerOrderHd.setOrderStatus(OrderStatus.OPN.name());
			customerOrderHd.setUpdateDate(new Date());
			customerOrderHd.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderHdDao.update(customerOrderHd);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	private void confirmSendEmailAfterPaymentForEnrollmentAndRenewal(Long transNo,String emailType,String userId, String userName){
		
		TransactionEmailDto dto = new TransactionEmailDto();
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(EmailType.RECEIPT.name());
		CustomerProfile cp = null;
		if (null !=transNo) {
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transNo);
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderTrans.getCustomerOrderHd().getCustomerId());
		}
		String contentMT= mt.getContentHtml();
		dto.setEmailContent(modifyContent(contentMT, cp, userName));
		dto.setSendTo(cp.getContactEmail());
		dto.setSubject(mt.getMessageSubject());
		
		dto.setTransactionNO(transNo);
		dto.setEmailType(emailType);
		LoginUserDto userDto = new LoginUserDto();
		userDto.setUserId(userId);
		userDto.setUserName(userName);
		sentTransactionEmail(dto, userDto);
	}
	
	@Transactional
	public TransactionEmailDto getEmailDeailReady(TransactionEmailDto transactionEmailDto,String userName){
		if (EmailType.DAYPASSRECEIPT.name().equalsIgnoreCase(transactionEmailDto.getEmailType())) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			MessageTemplate daypassTemplate = messageTemplateDao.getTemplateByFunctionId(EmailType.Purchase_Daypass.getFunctionId());
			CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, transactionEmailDto.getOrderNO());
			if(null!=orderHd.getCustomerId()){
				CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, orderHd.getCustomerId());
				List<CustomerOrderPermitCard> list = orderHd.getCustomerOrderPermitCards();
				CustomerOrderPermitCard card = list.get(0);
				StringBuilder sb = new StringBuilder();
				HashSet<String> name = new HashSet<String>();
				for(CustomerOrderPermitCard c : list){
					ServicePlan d = servicePlanDao.get(ServicePlan.class, c.getServicePlanNo());
					name.add(d.getPlanName());
				}
				for(String str : name){
					sb.append(str).append("; ");
				}
				String type = "";
				if(sb.length()>0){
					type = sb.subSequence(0, sb.length()-2).toString();
				}
				logger.debug("CustomerOrderTransServiceImpl  getEmailDeailReady for purchase daypass run start ...orderNo:"+transactionEmailDto.getOrderNO());
				ServicePlan daypass = servicePlanDao.get(ServicePlan.class, card.getServicePlanNo());
				String content = daypassTemplate.getContentHtml().replaceAll(Constant.MessageTemplate.UserName.getName(), cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname())
						.replaceAll(Constant.MessageTemplate.Confirmation.getName(), transactionEmailDto.getOrderNO()+"")
						.replaceAll(Constant.MessageTemplate.StartTime.getName(), sdf.format(card.getEffectiveFrom()))
						.replaceAll(Constant.MessageTemplate.EndTime.getName(), sdf.format(card.getEffectiveTo()))
						.replaceAll(Constant.MessageTemplate.OffPass.getName(),list.size()+"")
						.replaceAll(Constant.MessageTemplate.Type.getName(),type);
				transactionEmailDto.setEmailContent(content);
				transactionEmailDto.setSendTo(cp.getContactEmail());
				transactionEmailDto.setSubject(daypassTemplate.getMessageSubject());
				logger.debug("CustomerOrderTransServiceImpl  getEmailDeailReady for purchase daypass run end ...orderNo:"+transactionEmailDto.getOrderNO());
			}
		}else {
			MessageTemplate receiptTemplate = messageTemplateDao.getTemplateByFunctionId(EmailType.RECEIPT.name());
			CustomerProfile cp = null;
			if (null !=transactionEmailDto.getTransactionNO()) {
				CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionEmailDto.getTransactionNO());
				cp = customerProfileDao.get(CustomerProfile.class, customerOrderTrans.getCustomerOrderHd().getCustomerId());
			}else if (null !=transactionEmailDto.getOrderNO()) {
				CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, transactionEmailDto.getOrderNO());
				cp = customerProfileDao.get(CustomerProfile.class, customerOrderHd.getCustomerId());
			}
			
			String contentMT= receiptTemplate.getContentHtml();
			if(StringUtils.isEmpty(transactionEmailDto.getEmailContent())){
				transactionEmailDto.setEmailContent(modifyContent(contentMT, cp, userName));
			}
			if(StringUtils.isEmpty(transactionEmailDto.getSendTo())){
				transactionEmailDto.setSendTo(cp.getContactEmail());
			}
			if(StringUtils.isEmpty(transactionEmailDto.getSubject())){
				transactionEmailDto.setSubject(receiptTemplate.getMessageSubject());
			}
		}
			
		return transactionEmailDto;
	}
	@Transactional
	@Override
	public ResponseResult handlePosPayPrivateCoach(PosResponse posResponse) {
		try {
			Long transactionNo = updateOrderTransAndOrderHd(posResponse);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
		}
		return responseResult;
	}
	@Transactional
	@Override
	public ResponseResult handlePosPayDaypass(PosResponse posResponse) {
		try {
			if (posResponse.getReferenceNo() != null) {
				Long transactionNo = updateOrderTransAndOrderHd(posResponse);
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
		}
		return responseResult;
	}
	@Transactional
	public ResponseResult handleCreditCardByPos(PosResponse posResponse){
		try {
			if (posResponse.getReferenceNo() != null) {
				Long transactionNo = updateOrderTransAndOrderHd(posResponse);
				confirmSendEmailAfterPaymentForEnrollmentAndRenewal(transactionNo,EmailType.RECEIPT.name(),"SYSTEM","HKGTA");
				//Used to change enrollment status from Approved to Payment Approved or Payment Approved to Approved
				updateEnrollmentStatusOncePayementChange(transactionNo);
				
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
		}
		return responseResult;
	}

	private Long updateOrderTransAndOrderHd(PosResponse posResponse) throws Exception {
		Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
		CustomerOrderTrans customerOrderTrans = this.customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
		customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
		customerOrderTrans.setInternalRemark(posResponse.getResponseText());
		customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
		customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));//Card Type may be Master Visa or Union pay
		if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
			customerOrderTrans.setStatus(CustomerTransationStatus.SUC.getDesc());
			CustomerOrderHd customerOrderHd= customerOrderTrans.getCustomerOrderHd();
			BigDecimal balanceDueAccountant = customerTransactionDao.countBalanceDueStatus(customerOrderHd.getOrderNo());
			if(balanceDueAccountant.compareTo(BigDecimal.ZERO) <= 0){
				customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
				List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
				if (customerOrderDets.size() > 0 && customerOrderDets.get(0).getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)){
					//if the payment is for renew, need to set effective date and expiry date for customer service account and save limit rule and facility right data.
					handleRenewPayment(customerOrderHd,"System");
				}
			}
			
			
		}else{
			customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
		}
		customerOrderTransDao.saveOrUpdate(customerOrderTrans);
		return transactionNo;
	}
	private void handleRenewPayment(CustomerOrderHd customerOrderHd,String createBy) throws Exception{
		
		CustomerServiceAcc csAcc = customerServiceDao.getLatestCustomerService(customerOrderHd.getCustomerId());
		/*
		 *setEffectiveDate
		 *1 if the status is  ACT,then set effectiveDate is getExpiryDate+1,set ExpiryDate is effectiveDate + servicePlan's contract_length_month
		 *2 if the status is  EXP,then set effectiveDate is the first of next month,set ExpiryDate is effectiveDate + servicePlan's contract_length_month
		 */
		if (null != csAcc) {
			CustomerServiceAcc newCustomerServiceAcc = customerServiceDao.getUniqueByCol(CustomerServiceAcc.class, "orderNo", customerOrderHd.getOrderNo());
			CustomerServiceSubscribe customerServiceSubscribe = customerServiceSubscribeDao.getUniqueByCol(CustomerServiceSubscribe.class, "id.accNo", newCustomerServiceAcc.getAccNo());
			ServicePlan renewalServicePlan = servicePlanDao.get(ServicePlan.class, customerServiceSubscribe.getId().getServicePlanNo());
			Calendar calendar = Calendar.getInstance();
			if (CustomerServiceStatus.ACT.getDesc().equals(csAcc.getStatus())) {
				calendar.setTime(csAcc.getExpiryDate());
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				//setEffectiveDate
				newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
				if(renewalServicePlan.getContractLengthMonth()!=null) {
					calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
				}
				//setEffectiveDate
				newCustomerServiceAcc.setExpiryDate(calendar.getTime());
			} else if (CustomerServiceStatus.EXP.getDesc().equals(csAcc.getStatus())) {
				calendar.setTime(new Date());
				calendar.set(Calendar.DAY_OF_MONTH, 1);
		        calendar.add(Calendar.MONTH, 1);
				//setEffectiveDate
				newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
				if(renewalServicePlan.getContractLengthMonth()!=null) {
					calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
				}
				//setEffectiveDate
				newCustomerServiceAcc.setExpiryDate(calendar.getTime());
			}
		}
	}
	
	
	@Transactional(rollbackFor=Exception.class)
	public void moveTranscriptFile(Long transactionNo,Long customerId){
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		String transcriptFileName = customerOrderTrans.getTranscriptFileName();
		String basePath = "";
		try {
			basePath = FileUpload.getBasePath(FileUpload.FileCategory.USER);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File transFile = new File(basePath + File.separator + customerId.longValue());
		if(!transFile.exists()){
			boolean isCreated = transFile.mkdirs();
			if (!isCreated) {
				logger.error("Can't create folder!");
			}
		}
		String fileName = "";
		int index = transcriptFileName.lastIndexOf("/");
		if (transcriptFileName != null && index >= 0) {
			fileName = transcriptFileName.substring(index);
		}
		boolean isSuccess = FileUtil.moveFile(basePath + transcriptFileName, basePath + File.separator + customerId.longValue() + fileName);
		if (isSuccess) {
			customerOrderTrans.setTranscriptFileName("/" + customerId.longValue() + fileName);
			customerOrderTransDao.update(customerOrderTrans);
		}else {
			logger.error(transcriptFileName + "move failed!");
		}
		
	}
	@Override
	@Transactional
	public ResponseResult paymentByCreditCard(
			CustomerOrderTransDto customerOrderTransDto) {
		ResponseResult responseResult = new ResponseResult();
		try {

			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			if (customerOrderTransDto.getTransactionNo() != null) {//allow update the existing
				customerOrderTrans.setTransactionNo(customerOrderTransDto.getTransactionNo());
			}
			CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class, customerOrderTransDto.getOrderNo());
			if ("CMP".equals(customerOrderHd.getOrderStatus())) {
				responseResult.setReturnCode("1");
				responseResult.setErrorMessageEN("The transaction has been completed!");
				return responseResult;
			}
			
			
			customerOrderTrans.setCustomerOrderHd(customerOrderHd);
			customerOrderTrans.setPaidAmount(customerOrderTransDto.getPaidAmount());
			String paymentMethodCode = uc.ifempty(customerOrderTransDto.getPaymentMethodCode(),"VISA");
			customerOrderTrans.setPaymentMethodCode(paymentMethodCode);
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			customerOrderTrans.setTerminalId(customerOrderTransDto.getTerminalId());
			customerOrderTrans.setStatus("PND");
			customerOrderTrans.setTranscriptFileName(customerOrderTransDto.getTranscriptFileName());
			customerOrderTrans.setTransactionTimestamp(new Date());
			customerOrderTrans.setPaymentRecvBy(customerOrderTransDto.getPaymentReceivedBy());
			customerOrderTrans.setCreateBy(customerOrderTransDto.getCreateByUserId());
			//start of validation
			if (customerOrderTrans.getPaidAmount().compareTo(BigDecimal.ZERO) <= 0){
				return new ResponseResult("1","The paid amount can not be less than or equal to 0.");
			}
			
			//start of payment total checking

			BigDecimal balanceDueSalesMan = homePageSummaryDao.countBalanceDue(customerOrderTransDto.getOrderNo());
			BigDecimal balanceDueAccountant = customerTransactionDao.countBalanceDueStatus(customerOrderTransDto.getOrderNo());
			if (customerOrderTransDto.getTransactionNo() != null
					&& customerOrderTransDto.getTransactionNo() > 0){//Change payment to success status
				CustomerOrderTrans customerOrderTransOld = customerOrderTransDao.get(CustomerOrderTrans.class, customerOrderTransDto.getTransactionNo());
				if (customerOrderTransDto.getStatus().equals("SUC")){
					if (balanceDueAccountant.subtract(customerOrderTrans.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
						return new ResponseResult("1","The sum of successful paid is over the amount due. Current due:$" + balanceDueSalesMan);
					}
				}
				else if (customerOrderTransDto.getStatus() == null || customerOrderTransDto.getStatus().equals("")) {
					if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount()).add(customerOrderTransOld.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
						return new ResponseResult("1","The sum of payment is over the amount due. Current due:$" + balanceDueSalesMan);
					}
				}
				customerOrderTransDao.evict(customerOrderTransOld);
			}
			if (customerOrderTransDto.getTransactionNo() == null) {//Add new payment case
				if (balanceDueSalesMan.compareTo(BigDecimal.ZERO) <= 0) {
					return new ResponseResult("1","No more payment is required.");
				} else if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
					return new ResponseResult("1","The sum of successful paid is over the amount due. Current due:$" + balanceDueSalesMan);
				}
			}
			//end of payment total checking
			if (null == customerOrderTrans.getTransactionNo() || 0 == customerOrderTrans.getTransactionNo() ) {
				customerOrderTransDao.save(customerOrderTrans);
			}else {
				customerOrderTransDao.saveOrUpdate(customerOrderTrans);
			}
			responseResult.setReturnCode("0");
			responseResult.setErrorMessageEN("success");
			responseResult.setData(customerOrderTrans);
			return responseResult;
		} catch (Exception e) {
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("save failed!");
			return responseResult;
		}
	}
	
	@Override
	@Transactional
	public ResponseResult getPaymentDetailsByOrderNo(Long orderNo) {
		try {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(orderNo);
			List<CustomerOrderTransDto> customerOrderTransDtos = new ArrayList<CustomerOrderTransDto>();
			for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
				CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
				customerOrderTransDto.setAgentTransactionNo(customerOrderTrans.getAgentTransactionNo());
				customerOrderTransDto.setPaidAmount(customerOrderTrans.getPaidAmount());
				customerOrderTransDto.setPaymentMethodCode(customerOrderTrans.getPaymentMethodCode());
				customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
				customerOrderTransDto.setTerminalId(customerOrderTrans.getTerminalId());
				customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
				customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(customerOrderTrans.getTransactionTimestamp()));
				customerOrderTransDto.setTranscriptFileName(customerOrderTrans.getTranscriptFileName());
				customerOrderTransDto.setOrderNo(orderNo);
				if(CommUtil.notEmpty(customerOrderTrans.getReadBy())){
					customerOrderTransDto.setReadBy("");
				}else{
					customerOrderTransDto.setReadBy(customerOrderTrans.getReadBy());
				}
				customerOrderTransDto.setUnReadCount(remarkDao.countUnreadPayment(orderNo));
				customerOrderTransDtos.add(customerOrderTransDto);
			}
			CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class, orderNo);

			Long customerId = customerOrderHd.getCustomerId().longValue();
			CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, customerId);

			BigDecimal orderTotalAmount = customerOrderHd.getOrderTotalAmount();
			String customerSurname = customerProfile.getSurname();
			String customerGivenname = customerProfile.getGivenName();
			String customerSalutation = customerProfile.getSalutation();
			BigDecimal balanceDue = homePageSummaryDao.countBalanceDue(orderNo);

			List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
			CustomerOrderDet customerOrderDet = customerOrderDets.iterator().next();
			String itemNo = customerOrderDet.getItemNo();
			String planName = "";
			if (itemNo.startsWith("RENEW")) {
				ServicePlanOfferPos servicePlanOfferPos = servicePlanOfferPosDao.getUniqueByCol(ServicePlanOfferPos.class, "posItemNo", itemNo);
				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "servPosId", servicePlanOfferPos.getServPosId());
				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlan.getPlanName();
			}else {
				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "posItemNo", itemNo);
				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlan.getPlanName();
			}

			Map<String, Object> responseMap = new HashMap<String, Object>();
			responseMap.put("customerId", customerId);
			responseMap.put("orderNo", orderNo);
			responseMap.put("customerSurname", customerSurname);
			responseMap.put("customerGivenname", customerGivenname);
			responseMap.put("customerSalutation", customerSalutation);
			responseMap.put("orderTotalAmount",orderTotalAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
			responseMap.put("balanceDue",balanceDue.setScale(2, BigDecimal.ROUND_HALF_UP));
			responseMap.put("planName", planName);
			Data data = new Data();
			data.setLastPage(true);
			data.setRecordCount(customerOrderTransDtos.size());
			data.setList(customerOrderTransDtos);
			data.setTotalPage(1);
			data.setCurrentPage(1);
			data.setPageSize(customerOrderTransDtos.size());
			responseMap.put("list", data);
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("0");
			responseResult.setErrorMessageEN("success");
			responseResult.setData(responseMap);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("no data exists!");
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult getPaymentDetailsAccountantByOrderNo(Long orderNo) {
		try {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(orderNo);
			List<CustomerOrderTransDto> customerOrderTransDtos = new ArrayList<CustomerOrderTransDto>();
			for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
				CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
				customerOrderTransDto.setAgentTransactionNo(customerOrderTrans.getAgentTransactionNo());
				customerOrderTransDto.setPaidAmount(customerOrderTrans.getPaidAmount());
				customerOrderTransDto.setPaymentMethodCode(customerOrderTrans.getPaymentMethodCode());
				customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
				customerOrderTransDto.setTerminalId(customerOrderTrans.getTerminalId());
				customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
				customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(customerOrderTrans.getTransactionTimestamp()));
				customerOrderTransDto.setTranscriptFileName(customerOrderTrans.getTranscriptFileName());
				customerOrderTransDto.setOrderNo(orderNo);
				if(CommUtil.notEmpty(customerOrderTrans.getReadBy())){
					customerOrderTransDto.setRead(true);
				}else{
					customerOrderTransDto.setRead(false);
				}
				customerOrderTransDtos.add(customerOrderTransDto);
			}
			CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class, orderNo);

			Long customerId = customerOrderHd.getCustomerId().longValue();
			CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, customerId);

			BigDecimal orderTotalAmount = customerOrderHd.getOrderTotalAmount();
			String customerSurname = customerProfile.getSurname();
			String customerGivenname = customerProfile.getGivenName();
			String customerSalutation = customerProfile.getSalutation();
			BigDecimal balanceDue = customerTransactionDao.countBalanceDueStatus(orderNo);

			List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
			CustomerOrderDet customerOrderDet = customerOrderDets.iterator().next();
			String itemNo = customerOrderDet.getItemNo();
			
			
			String planName = "";
			if (itemNo.startsWith("RENEW")) {
				ServicePlanOfferPos servicePlanOfferPos = servicePlanOfferPosDao.getUniqueByCol(ServicePlanOfferPos.class, "posItemNo", itemNo);
//				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "servPosId", servicePlanOfferPos.getServPosId());
//				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlanOfferPos.getDescription();
			}else {
				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "posItemNo", itemNo);
				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlan.getPlanName();
			}

			Map<String, Object> responseMap = new HashMap<String, Object>();
			responseMap.put("orderNo", orderNo);
			responseMap.put("customerSurname", customerSurname);
			responseMap.put("customerGivenname", customerGivenname);
			responseMap.put("customerSalutation", customerSalutation);
			responseMap.put("orderTotalAmount",orderTotalAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
			responseMap.put("balanceDue",balanceDue.setScale(2, BigDecimal.ROUND_HALF_UP));
			responseMap.put("planName", planName);
			Data data = new Data();
			data.setLastPage(true);
			data.setRecordCount(customerOrderTransDtos.size());
			data.setList(customerOrderTransDtos);
			data.setTotalPage(1);
			data.setCurrentPage(1);
			data.setPageSize(customerOrderTransDtos.size());
			responseMap.put("list", data);
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("0");
			responseResult.setErrorMessageEN("success");
			responseResult.setData(responseMap);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("no data exists!");
			return responseResult;
		}
	}
	
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String serviceType){
		
		if (serviceType.equals(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)) {
			List<SysCode> enrollStatusCodes = sysCodeDao.selectSysCodeByCategory("enrollStatusDue");
			return customerTransactionDao.assembleQueryConditions(enrollStatusCodes,serviceType);
		}
		else if (serviceType.equals(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)) {
			List<SysCode> orderStatusCodes = sysCodeDao.selectSysCodeByCategory("orderStatusRenewal");
			return customerTransactionDao.assembleQueryConditions(orderStatusCodes,serviceType);
		}
		else {
			List<SysCode> orderStatusCodes = sysCodeDao.selectSysCodeByCategory("orderStatusRejected");
			return customerTransactionDao.assembleQueryConditions(orderStatusCodes,serviceType);
		}
		
	}

	@Override
	@Transactional
	public ListPage<CustomerOrderHd> getCustomerTransactionList(
			ListPage<CustomerOrderHd> page, String balanceDue,
			String serviceType, String orderBy, boolean isAscending,
			String searchText,String filterBySalesMan,String mode,String userId, String enrollStatusFilter,String memberType,String device) {
//		String[] validValues = new String[] {"effectiveDate", "customerName", "description" ,"salesFollowBy", "orderTotalAmount", "enrollStatus", "balanceDue" , "enrollCreateDate", "orderNo", "newTransNumber", "academyNo"};
//		if (Arrays.asList(validValues).contains(orderBy) == false) {
//			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "sortBy is not valid value. Allowed values are : effectiveDate, customerName, description, salesFollowBy, orderTotalAmount, balanceDue, enrollStatus, orderNo, academyNo, newTransNumber");
//			return new ListPage<CustomerOrderHd>();
//		}
		ListPage<CustomerOrderHd> customerTransactionListDtoList = customerTransactionDao
				.getCustomerTransactionList(page,balanceDue, serviceType, orderBy,isAscending,searchText,filterBySalesMan, mode, enrollStatusFilter,memberType);
		List<CustomerTransactionListDto> customerTransactionListDtoListWithRemark = new ArrayList<CustomerTransactionListDto>();
		for (Object item : customerTransactionListDtoList.getDtoList()) {
			CustomerTransactionListDto customerTransactionListDto = ((CustomerTransactionListDto) item);
			if(remarkDao.countUnreadRemarkByDevice(((CustomerTransactionListDto) item).getCustomerId().longValue(),userId,device)>0){
				customerTransactionListDto.setHasRemark(true);
			} else {
				customerTransactionListDto.setHasRemark(false);
			}
			if(remarkDao.countUnreadPayment(customerTransactionListDto.getOrderNo().longValue()) > 0){
				customerTransactionListDto.setHasRead(true);
			}else{
				customerTransactionListDto.setHasRead(false);
			}
			customerTransactionListDtoListWithRemark.add(customerTransactionListDto);
		}
		customerTransactionListDtoList.getDtoList().clear();
		customerTransactionListDtoList.getDtoList().addAll(customerTransactionListDtoListWithRemark);
		
		return customerTransactionListDtoList;
	}
	
	@Override
	@Transactional
	public CustomerOrderTrans getCustomerOrderTrans(Long transactionNo) {
		return customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
	}
	
	
	@Override
	@Transactional
	public Long getOrderTimeoutMin() {
		Long gatewayId = 1l;
		PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		Long timeoutMin = paymentGateway.getTimeoutMin();
		return timeoutMin;
	}

	@Override
	@Transactional
	public List<CustomerOrderTrans> getTimeoutPayments() {
		List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getTimeoutPayments(getOrderTimeoutMin());
		return customerOrderTranses;
	}
	
	@Override
	@Transactional
	public void updateCustomerOrderTrans(CustomerOrderTrans customerOrderTrans) {
		// TODO Auto-generated method stub
		customerOrderTransDao.update(customerOrderTrans);
	}
	@Override
	@Transactional
	public CustomerOrderHd getCustomerOrderHdByOrderNo(Long orderNo)
	{
		return customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
	}
	@Override
	@Transactional
	public void addNewTransactionRecord(CustomerOrderTrans customerOrderTrans)
	{
		customerOrderTransDao.save(customerOrderTrans);
	}
	@Override
	@Transactional
	public List<CustomerOrderTrans> getCustomerOrderTransListByOrderNo(Long orderNo)
	{
		List<Object> param = new ArrayList<Object>();
		param.add(orderNo);
		List<CustomerOrderTrans>  orderTrans = customerOrderTransDao.getByCol(CustomerOrderTrans.class, "customerOrderHd.orderNo", orderNo, null);
		return orderTrans;
	}
	
	@Override
	@Transactional
	public CustomerOrderTransDto payment(MemberCashValuePaymentDto memberCashValuePaymentDto)
	{
		if(memberCashValuePaymentDto.getCustomerId()==null || memberCashValuePaymentDto.getItemNoMap() == null || memberCashValuePaymentDto.getTotalAmount()==null)
			throw new GTACommonException(GTAError.MemberError.MEMBER_NOT_FOUND);
		
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date());
		if (StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod()) || memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH))
			customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
		else  if (memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CREDIT_CARD))
			customerOrderHd.setOrderStatus(Constant.Status.OPN.toString());
		customerOrderHd.setStaffUserId(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCustomerId(memberCashValuePaymentDto.getCustomerId());
		customerOrderHd.setOrderTotalAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderHd.setCreateBy(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCreateDate(currentTime);
		customerOrderHdDao.save(customerOrderHd);

		for (Map.Entry<String, Integer> entry : memberCashValuePaymentDto.getItemNoMap().entrySet()) {
			PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, entry.getKey());
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(entry.getKey());
			customerOrderDet.setOrderQty(new Long(entry.getValue()));
			customerOrderDet.setItemTotalAmout(posServiceItemPrice.getItemPrice().multiply(new BigDecimal(customerOrderDet.getOrderQty())));
			customerOrderDet.setCreateBy(memberCashValuePaymentDto.getUserId());
			customerOrderDet.setCreateDate(currentTime);
			customerOrderDetDao.save(customerOrderDet);
		}
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTrans.setTransactionTimestamp(currentTime);
		customerOrderTrans.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTrans.setPaymentLocationCode(memberCashValuePaymentDto.getLocation());
		if (StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod()) || memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.FTF.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		}else if(memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH_Value)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		}
		else if (memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CREDIT_CARD)){
			if(null != memberCashValuePaymentDto.getIsOnlinePayment() && memberCashValuePaymentDto.getIsOnlinePayment())
				customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			else 
				customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
			
			customerOrderTrans.setStatus(Constant.Status.PND.toString());
		}	else if (PaymentMethod.VISA.name().equals(memberCashValuePaymentDto.getPaymentMethod())){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			
			customerOrderTrans.setStatus(Constant.Status.PND.toString());
		}
		customerOrderTrans.setPaymentRecvBy(memberCashValuePaymentDto.getUserId());
		customerOrderTransDao.save(customerOrderTrans);
		
		CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
		customerOrderTransDto.setOrderNo(customerOrderHd.getOrderNo());
		customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(currentTime));
		customerOrderTransDto.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTransDto.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
		return customerOrderTransDto;
	}
	
	@Override
	@Transactional
	public ResponseResult getAllCreatedStaff() {
		String sql = "SELECT DISTINCT sp.user_id as userId, CONCAT(sp.given_name,' ',sp.surname) as staffName from  staff_profile sp, customer_order_trans cot where cot.create_by = sp.user_id"
				+ " UNION SELECT DISTINCT cot.create_by as userId,'System' as staffName from  customer_order_trans cot where cot.create_by = 'System'";
		List<StaffDto> sales = customerOrderTransDao.getDtoBySql(sql, null, StaffDto.class);
		responseResult.initResult(GTAError.Success.SUCCESS, sales);
		return responseResult;
	}
	@Override
	@Transactional
	public ResponseResult getAllAuditedStaff() {
		String sql = "SELECT DISTINCT sp.user_id as userId, CONCAT(sp.given_name,' ',sp.surname) as staffName from  staff_profile sp, customer_order_trans cot where cot.audit_by = sp.user_id"
				+ " UNION SELECT DISTINCT cot.audit_by as userId,'System' as staffName from  customer_order_trans cot where cot.audit_by = 'System'";
		List<StaffDto> sales = customerOrderTransDao.getDtoBySql(sql, null, StaffDto.class);
		responseResult.initResult(GTAError.Success.SUCCESS, sales);
		return responseResult;
	}
	@Override
	@Transactional
	public ResponseResult getSattlementReport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto){
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getSattlementReport(page,dto);
		List<Object> spendingSummaryDtos = listPage.getDtoList();
		Data data = new Data();
		for(Object object: spendingSummaryDtos){
			SettlementReportDto temp = (SettlementReportDto) object;
			
			replaceValue(temp);
		}
		data.setList(spendingSummaryDtos);
		data.setTotalPage(page.getAllPage());;
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	@Transactional
	private void replaceValue(SettlementReportDto temp) {
		if("ENROLL".equals(temp.getEnrollType())){
			temp.setEnrollType("Enroll");
		}else if ("RENEW".equals(temp.getEnrollType())){
			temp.setEnrollType("Renew");
		}
		
		if(PaymentMethod.BT.name().equals(temp.getPaymentMethod())){
			temp.setPaymentMethod(PaymentMethod.BT.getDesc());
		} else if(PaymentMethod.CASH.name().equals(temp.getPaymentMethod())){
			temp.setPaymentMethod(PaymentMethod.CASH.getDesc());
		}else if(PaymentMethod.VISA.name().equals(temp.getPaymentMethod())){
			temp.setPaymentMethod(PaymentMethod.VISA.getDesc());
		}else if(PaymentMethod.CHEQUE.name().equalsIgnoreCase(temp.getPaymentMethod())){
			temp.setPaymentMethod(PaymentMethod.CHEQUE.getDesc());
		}else if(PaymentMethod.OTH.name().equals(temp.getPaymentMethod())){
			temp.setPaymentMethod(PaymentMethod.OTH.getDesc());
		}else if(PaymentMethod.MASTER.name().equals(temp.getPaymentMethod())){
			temp.setPaymentMethod(PaymentMethod.MASTER.getDesc());
		}else if("CC".equals(temp.getPaymentMethod())){
			temp.setPaymentMethod("Credit Card");
		}
		
		if (null == temp.getPaymentMedia()) {
			temp.setPaymentMedia("N/A");
		}else if(PaymentMediaType.OP.name().equals(temp.getPaymentMedia())){
			temp.setPaymentMedia(PaymentMediaType.OP.getDesc());
		} else if(PaymentMediaType.OTH.name().equals(temp.getPaymentMedia())){
			temp.setPaymentMedia(PaymentMediaType.OTH.getDesc());
		} else if(PaymentMediaType.ECR.name().equals(temp.getPaymentMedia())){
			temp.setPaymentMedia(PaymentMediaType.ECR.getDesc());
		}
		
		if("SUC".equals(temp.getTransStatus())){
			temp.setTransStatus("Success");
		}else if("FAIL".equals(temp.getTransStatus())){
			temp.setTransStatus("Fail");
		}else if("VOID".equals(temp.getTransStatus())){
			temp.setTransStatus("Void");
		}else if("FAIL".equals(temp.getTransStatus())){
			temp.setTransStatus("Fail");
		}else if("PND".equals(temp.getTransStatus())){
			temp.setTransStatus("Pending");
		}else if("RFU".equals(temp.getTransStatus())){
			temp.setTransStatus("Refund");
		}else if("PR".equals(temp.getTransStatus())){
			temp.setTransStatus("Payment Refund");
		}
		
		if ("OPN".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Open");
		}else if ("CMP".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Complete");
		}else if ("CAN".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Cancelled");
		}else if ("REJ".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Rejected");
		}
		
		String createBy = temp.getCreateBy();
		String createName = null;
		if ("System".equalsIgnoreCase(createBy)) {
			createName = "System";
		}else {
			createName = customerProfileDao.getNameByUserId(createBy);
		}
		temp.setCreateBy(createName);
		String updateBy = temp.getUpdateBy();
		String updateName = null;
		if ("System".equalsIgnoreCase(updateBy)) {
			updateName = "System";
		}else {
			updateName = customerProfileDao.getNameByUserId(updateBy);
		}
		temp.setUpdateBy(updateName);
		String auditBy = temp.getAuditBy();
		String auditName = null;
		if ("System".equalsIgnoreCase(auditBy)) {
			auditName = "System";
		}else {
			auditName = customerProfileDao.getNameByUserId(auditBy);
		}
		temp.setAuditBy(auditName);
		
		String locationCode = temp.getLocation();
		String hqlstr = "SELECT s.codeDisplay FROM SysCode s where s.codeValue = ? and s.category = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(locationCode);
		param.add("location");
		String locationDesc = (String)customerProfileDao.getUniqueByHql(hqlstr, param);
		temp.setLocation(locationDesc);
	}

	@Override
	@Transactional
	public ResponseResult getSattlementReportExport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto,HttpServletResponse response) throws Exception{
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getSattlementReport(page,dto);
		List<Object> spendingSummaryDtos = listPage.getDtoList();
		for(Object object: spendingSummaryDtos){
			SettlementReportDto temp = (SettlementReportDto) object;
			replaceValue(temp);
		}
		
		String timeCondition = null;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if ("0d".equalsIgnoreCase(dto.getTransactionDate())) {
			timeCondition = DateConvertUtil.getYMDFormatDate(date);
		}else if("7d".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.add(Calendar.DATE, -7);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime())+" to "+ DateConvertUtil.getYMDFormatDate(date);
		}else if("14d".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.DATE, -14);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime())+" to "+ DateConvertUtil.getYMDFormatDate(date);
		}else if("1m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -1);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime())+" to "+ DateConvertUtil.getYMDFormatDate(date);
		}else if("2m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -2);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime())+" to "+ DateConvertUtil.getYMDFormatDate(date);
		}else if("3m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -3);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime())+" to "+ DateConvertUtil.getYMDFormatDate(date);
		}else if("6m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -6);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime())+" to "+ DateConvertUtil.getYMDFormatDate(date);
		}else if("customize".equalsIgnoreCase(dto.getTransactionDate())) {
			timeCondition = dto.getStartDate() + " to "+dto.getEndDate();
		}
		
		String orderStatus = null;
		if ("OPN".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Open";
		}else if ("CMP".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Complete";
		}else if ("CAN".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Cancelled";
		}else if ("REJ".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Rejected";
		}
		
		String transStatus  = null;
		if("SUC".equals(dto.getTransStatus())){
			transStatus = "Success";
		}else if("FAIL".equals(dto.getTransStatus())){
			transStatus = "Fail";
		}else if("VOID".equals(dto.getTransStatus())){
			transStatus = "Void";
		}else if("FAIL".equals(dto.getTransStatus())){
			transStatus = "Fail";
		}else if("PND".equals(dto.getTransStatus())){
			transStatus = "Pending";
		}else if("RFU".equals(dto.getTransStatus())){
			transStatus = "Refund";
		}else if("PR".equals(dto.getTransStatus())){
			transStatus = "Payment Refund";
		}
		
		
		String enrollType = null;
		if("ENROLL".equals(dto.getEnrollType())){
			enrollType = "Enroll";
		}else if ("RENEW".equals(dto.getEnrollType())){
			enrollType = "Renew";
		}
		
		String location = null;
		String locationCode = dto.getLocation();
		String hqlstr = "SELECT s.codeDisplay FROM SysCode s where s.codeValue = ? and s.category = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(locationCode);
		param.add("location");
		location = (String)customerProfileDao.getUniqueByHql(hqlstr, param);
		
		String salesMan = customerProfileDao.getNameByUserId(dto.getSalesman());
		
		String accountant = customerProfileDao.getNameByUserId(dto.getAuditBy());
		
		String createBy = customerProfileDao.getNameByUserId(dto.getCreateBy());
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<String[]> outputList =  new ArrayList<String[]>();
		if (dto.getFileType().equalsIgnoreCase("CSV")) {
			outputList.add(new String[]{DateConvertUtil.getStrFromDate(new Date()),null, null, null, null, null, null,
					null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[21]);
			outputList.add(new String[]{"","","","","","","","Transaction Settlement("+timeCondition+")", null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[21]);
			outputList.add(new String[]{"Filter criteria:",null, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Time:",timeCondition, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Order Status:",orderStatus, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Trans Status:",transStatus, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Enroll Type:",enrollType, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Location:",location, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Salesman:",salesMan, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Accountant:",accountant, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
			outputList.add(new String[]{"Create By:",createBy, null, null, null, null, null,null, null, null, null, null, null, null, null,
					null, null,null,null,null,null});
//			outputList.add(new String[]{"Filter criteria:","Time","Order Status","Trans Status","Enroll Type","Location","Salesman","Audit By", "Create By", null, null, null, null, null, null,
//					null, null,null,null,null,null});
			outputList.add(new String[21]);
		}
		outputList.add(new String[] { "transaction Date", "Transaction ID", "Order No.", "Order Date", "Academy ID", "Member Name",
				"Service Plan", "Salesman", "Enroll Type", "Payment Method", "Payment Medium", "Location","Trans Status","Order Status","Qty", "Trans Amount", "Create By",
				"Update By","Update Date", "Audit By", "Audit Date" });
		Long totalQty = 0L;
		BigDecimal totalTransAmount = BigDecimal.ZERO;
		for (Object object : spendingSummaryDtos) {
			SettlementReportDto temp = (SettlementReportDto) object;
			
			outputList.add(new String[] { temp.getTransactionDate(), (temp.getTransactionId()==null?"":temp.getTransactionId()).toString(), (temp.getOrderNo()==null?"":temp.getOrderNo()).toString(),
					temp.getOrderDate(), temp.getAcademyId(), temp.getMemberName(), temp.getServicePlan(), temp.getSalesman(),
					temp.getEnrollType(), temp.getPaymentMethod(), temp.getPaymentMedia(), temp.getLocation(), temp.getTransStatus(),temp.getOrderStatus(), 
					(temp.getQty()==null?"":temp.getQty()).toString(),temp.getTransAmount()==null?"":temp.getTransAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString(),
					temp.getCreateBy(), temp.getUpdateBy(), temp.getUpdateDate(),temp.getAuditBy(), temp.getAuditDate() });
			totalQty=totalQty+(temp.getQty()==null?0L:temp.getQty());
			totalTransAmount = totalTransAmount.add((temp.getTransAmount()==null?BigDecimal.ZERO:temp.getTransAmount()));
		}
		outputList.add(new String[] { null, null, null, null, null, null,null, null, null, null, null, null, null,
				"Total:",totalQty.toString() , totalTransAmount.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), null, null, null,null, null});
		if (dto.getFileType().equalsIgnoreCase("CSV")) {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(baos));
			CSVWriter csvWriter = new CSVWriter(out, ',');
			csvWriter.writeAll(outputList);
			response.setHeader("Content-Disposition", "attachment; filename=" +"Transaction Settlement("+timeCondition+").csv");
			response.setContentType("text/csv");
			csvWriter.close();
		} else if (dto.getFileType().equalsIgnoreCase("PDF")) {
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=" +"Transaction Settlement("+timeCondition+").pdf");
			Document document = new Document(PageSize.A3.rotate());
			document.setMargins(-60f, -60f, 40f, 10f);
			PdfWriter.getInstance(document, baos);
			document.open();
			
			Font timeFont = new Font(Font.TIMES_ROMAN, 15, Font.NORMAL);
			Paragraph time = getParagraph(DateConvertUtil.getStrFromDate(new Date()), timeFont, 20f, 0f, 130f);
			document.add(time);
			
			Font titleFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
			Paragraph title = getParagraph("Transaction Settlement("+timeCondition+")", titleFont, 20f, 0f, 0f);
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			
			Font filterFont = new Font(Font.TIMES_ROMAN, 15, Font.NORMAL);
			Paragraph filter = getParagraph("Filter criteria:", filterFont, 20f, 0f, 130f);
			document.add(filter);
			
			filterFont.setSize(10);
			Paragraph timeFilter = getParagraph("Time:              "+timeCondition, filterFont, 8f, 0f, 130f);
			document.add(timeFilter);
			Paragraph orderfilter = getParagraph("Order Status:      "+(orderStatus == null?"":orderStatus),filterFont, 8f, 0f, 130f);
			document.add(orderfilter);
			Paragraph transFilter = getParagraph("Trans Status:      "+ (transStatus == null?"":transStatus),filterFont, 8f, 0f, 130f);
			document.add(transFilter);
			Paragraph enrollFilter = getParagraph("Enroll Type:      "+(enrollType == null?"":enrollType),filterFont, 8f, 0f, 130f);
			document.add(enrollFilter);
			Paragraph locationFilter = getParagraph("Location:      "+(location == null?"":location),filterFont, 8f, 0f, 130f);
			document.add(locationFilter);
			Paragraph salesmanFilter = getParagraph("Salesman:      "+(salesMan == null?"":salesMan),filterFont, 8f, 0f, 130f);
			document.add(salesmanFilter);
			Paragraph accountantFilter = getParagraph("Accountant:      "+(accountant == null?"":accountant),filterFont, 8f, 0f, 130f);
			document.add(accountantFilter);
			Paragraph createFilter = getParagraph("Create By:      "+(createBy == null?"":createBy),filterFont, 8f, 0f, 130f);
			document.add(createFilter);
			
			List<String[]> myEntries = outputList;
			PdfPTable my_first_table = new PdfPTable(outputList.get(0).length);
			my_first_table.setSpacingBefore(30f);
			Font font = new Font(Font.TIMES_ROMAN, 10);
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
			for (int i = 0; i < myEntries.size(); i++) {
				String[] strings = myEntries.get(i);
				for (int j = 0; j < strings.length; j++) {
					PdfPCell table_cell = new PdfPCell();
					if (i == 0) {
						table_cell.setPhrase(new Phrase(strings[j], firstColumnFont));
					} else {
						table_cell.setPhrase(new Phrase(strings[j], font));
					}
					table_cell.setNoWrap(false);
					table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					my_first_table.addCell(table_cell);
				}
			}

			document.add(my_first_table);
			document.close();
		}
		responseResult.initResult(GTAError.Success.SUCCESS, baos.toByteArray());
		
		return responseResult;
	}
	
	@Transactional
	public byte[] getEnrollmentRenewal(String fileType, String sortBy, String isAscending) throws Exception{
		return customerOrderTransDao.getRenewalEnrollmentAttachment(fileType, sortBy, isAscending);
	}
	
	@Override
	@Transactional
	public ResponseResult readTrans(CustomerOrderTransDto customerOrderTransDto){
		CustomerOrderTrans customerOrderTrans = this.customerOrderTransDao.get(CustomerOrderTrans.class, customerOrderTransDto.getTransactionNo());
		String status = customerOrderTrans.getStatus();
		if(!"FAIL".equalsIgnoreCase(status) && !"VOID".equalsIgnoreCase(status)){
			responseResult.initResult(GTAError.PaymentError.TRANSACTION_NOT_READ);
			return responseResult;
		}
		if(customerOrderTransDto.getRead()){
			customerOrderTrans.setReadBy(customerOrderTransDto.getReadBy());
		}else{
			customerOrderTrans.setReadBy(null);
		}
		customerOrderTrans.setReadDate(new Timestamp(new Date().getTime()));
		customerOrderTransDao.save(customerOrderTrans);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	
	/**
	 * Used to change enrollment status from Approved to Payment Approved or Payment Approved to Approved
	 * @param transactionNo
	 * @author Liky_Pan
	 */
	public void updateEnrollmentStatusOncePayementChange(Long transactionNo){
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		Long orderNo = null;
		if(customerOrderTrans==null){
			return;
		}else{
			orderNo = customerOrderTrans.getCustomerOrderHd().getOrderNo();
		}
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		BigDecimal validateBalanceDueAfterPayment = customerTransactionDao.countBalanceDueStatus(orderNo);
		CustomerEnrollment customerEnrollment = null;
		if (customerOrderHd != null) {
			customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerOrderHd.getCustomerId());
		}
		if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) <= 0) {
			if (customerEnrollment != null) {
				if (EnrollStatus.APV.name().equals(customerEnrollment.getStatus())) {
					
					customerEnrollmentService.recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),EnrollStatus.PYA.name(),customerEnrollment.getCustomerId());
					
					customerEnrollment.setStatus(EnrollStatus.PYA.name());
					customerEnrollmentDao.update(customerEnrollment);
					//add message notification
					CustomerProfile temProfile = customerProfileDao.getCustomerProfileByCustomerId(customerOrderHd.getCustomerId());
					devicePushService.pushMessage(new String[]{customerEnrollment.getSalesFollowBy()},"member_payment_approved",new String[]{temProfile.getGivenName()+ " " +temProfile.getSurname()},Constant.SALESKIT_PUSH_APPLICATION);
				}
			}
		} else if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) > 0) {
			if (customerEnrollment != null) {
				if (EnrollStatus.PYA.name().equals(customerEnrollment.getStatus())||EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())) {
					
					customerEnrollmentService.recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),EnrollStatus.APV.name(),customerEnrollment.getCustomerId());
					
					customerEnrollment.setStatus(EnrollStatus.APV.name());
					customerEnrollmentDao.update(customerEnrollment);
					
					if(EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())){
						Member member = memberDao.getMemberByCustomerId(customerEnrollment.getCustomerId());
						if(member != null){
							member.setEffectiveDate(null);
							memberDao.update(member);
						}
					}
					
				}
			}
		}
	}

	@Override
	@Transactional
	public void generatePatronAccountStatement() throws Exception{
		String sql = "SELECT  DISTINCT  m.customer_id as customerId  from customer_order_hd coh,member m, customer_order_det cod, pos_service_item_price pos "
				+ " where coh.customer_id = m.customer_id and coh.order_no = cod.order_no AND cod.item_no = pos.item_no "
				+ " AND ( LEFT (cod.item_no, 3) != 'SRV' AND LEFT (cod.item_no, 5) != 'RENEW' OR (LEFT (cod.item_no, 3) = 'SRV' AND pos.item_catagory = 'DP') ) "
				+ " and coh.order_date BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH)) "
				+ " and (m.member_type = 'IPM' OR m.member_type = 'CPM') "
				+ " UNION SELECT customer_id as customerId  from member me where me.`status` = 'ACT' and (me.member_type = 'IPM' OR me.member_type = 'CPM') ORDER BY customerId";
		List<MemberDto> memberDtos = memberDao.getDtoBySql(sql, null, MemberDto.class);
		calculateClosingBalance(memberDtos,null,null);
		for (MemberDto memberDto:memberDtos) {
			Long customerId = memberDto.getCustomerId();
			CustomerStatementBaseInfoDto statementInfo = getCustomerStatementBaseInfoDto(customerId,null,null);
			
			Member member = memberDao.get(Member.class, customerId);
			
			Date date = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			int month = calendar.get(Calendar.MONTH);
			calendar.add(Calendar.MONTH, -2);
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
			Date cutoffDate = calendar.getTime();  
			String strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd")+" 23:59:59";
			
			MemberCashvalueBalHistory lastMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCutoffDate)});
			BigDecimal openingBalance =null;
			if (null == lastMemberCashvalueBalHistory) {
				Date joinDate = member.getFirstJoinDate();
				calendar.setTime(date);
				calendar.add(Calendar.MONTH, -1);
				if (DateConvertUtil.date2String(joinDate, "yyyy-MM").equals(DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM"))) {
					//if the member's join date is last month, it's opening balance is it's inital cash value.
					MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
					if (null == memberCashvalue) {
						openingBalance = new BigDecimal(0);
					}else {
						openingBalance = memberCashvalue.getInitialValue();
					}
				}else {
					openingBalance = new BigDecimal(0);
				}
			}else {
				openingBalance = lastMemberCashvalueBalHistory.getRecalBalance();
			}
			
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -1);
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
			Date currCutoffDate = calendar.getTime();  
			String strCurrCutoffDate = DateConvertUtil.date2String(currCutoffDate, "yyyy-MM-dd")+" 23:59:59";
			MemberCashvalueBalHistory currMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCurrCutoffDate)});
			
			BigDecimal closingBalance = null;
			if (null != currMemberCashvalueBalHistory) {
				closingBalance = currMemberCashvalueBalHistory.getRecalBalance();
			}
			
			String basePath = appProps.getProperty("pdf.server.report");// /opt/file-server/report/statement  or  D:\\fileServer\\report\\statement
			
			File file = new File(basePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -1);
			Date generateMonthDate = calendar.getTime();  
			String generateMonth = DateConvertUtil.date2String(generateMonthDate, "yyyyMM");
			String filePath = basePath + "/Patron Account Statement_"+customerId+"_"+generateMonth+".pdf";
			OutputStream out = new FileOutputStream(filePath);
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
			Document document = new Document(PageSize.A4, 10, 10, 110, 50);
//			document.setMargins(10f, 10f, 10f, 10f);
			PdfWriter writer = PdfWriter.getInstance(document, out);
			
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD,Color.black);
			Color color = new Color(140, 0, 30);
			Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.white);
			
			List<String[]> allCashvalueList =  new ArrayList<String[]>();
			allCashvalueList.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type", "Dependent Patron's Name","Amount(HK$)","Cash Value Balance(HK$)"});
			allCashvalueList.add(new String[] {null,null,"Opening Balance",null,null,null,getFormatAmount(openingBalance)});
			
			String cashvalueSql = "SELECT DISTINCT trans.transaction_timestamp as transactionDate,trans.transaction_no as invoiceNo,(CASE hd.customer_id WHEN "+customerId+" THEN '-' ELSE CONCAT(cp.given_name,' ',cp.surname) END) as memberName,"
					+ " pos.description as description,(CASE trans.payment_method_code WHEN 'VISA' THEN 'VISA'  WHEN 'MASTER' THEN 'MASTER' WHEN 'CASH' THEN 'CASH' WHEN 'BT' THEN 'Bank Transfer' "
					+ " WHEN 'CHEQUE' THEN 'Cheque' WHEN 'CV' THEN 'Cash Value' WHEN 'VAC' THEN 'Virtual Account Transfer' ELSE 'Other' END) as paymentType,trans.paid_amount as amount,det.item_no as itemNo "
					+ " from customer_order_trans trans, customer_order_hd hd,customer_order_det det,member m,customer_profile cp,pos_service_item_price pos"
					+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no and det.item_no = pos.item_no "
					+ " and ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = 'CV') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU') "//The condition of spending or cash value refund
					+ " or (trans.`status` = 'SUC' and det.item_no = 'CVT0000001') OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001')) "//The condition of topup or Service refund
					+ " and hd.customer_id = m.customer_id and cp.customer_id = m.customer_id "
					+ " and (DATE(trans.transaction_timestamp) BETWEEN  DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH)))"
					+ " and (m.customer_id = ? or m.superior_member_id = ?) ORDER BY trans.transaction_timestamp";
			
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(customerId);
			param.add(customerId);
			List<PatronAccountStatementDto> patronAccountStatementDtos = customerOrderTransDao.getDtoBySql(cashvalueSql, param, PatronAccountStatementDto.class);
			
			BigDecimal topupTotal = new BigDecimal(0);
			BigDecimal spendingTotal = new BigDecimal(0);
			BigDecimal cashBalance	 = openingBalance;
//			for (PatronAccountStatementDto dto:patronAccountStatementDtos) {
			for (int i =0;i<patronAccountStatementDtos.size();i++) {
				PatronAccountStatementDto dto = patronAccountStatementDtos.get(i);
				if (null == statementInfo.getCreditLimit() || statementInfo.getCreditLimit().compareTo(BigDecimal.ZERO) >0) {
					dto.setPaymentType("City Ledger");
				}
//				if (dto.getItemNo().indexOf("PMS") >= 0 ) {//所有的消费的description已经修改的有意义了，因此这个地方不需要再转。
//					dto.setDescription("Accommodation");
//				}
//				if (Constant.TOPUP_ITEM_NO.equalsIgnoreCase(dto.getItemNo())) {
//					dto.setDescription("Top up for cash value");
//				}
				if (!Constant.TOPUP_ITEM_NO.equalsIgnoreCase(dto.getItemNo()) && !Constant.REFUND_SERVICE_ITEM_NO.equalsIgnoreCase(dto.getItemNo())) {
					if (statementInfo.getMemberName().equals(dto.getMemberName()) || "-".equals(dto.getMemberName())) {
						spendingTotal = spendingTotal.add(dto.getAmount());
					}
					cashBalance = cashBalance.subtract(dto.getAmount());
					dto.setCashvalueBalance(cashBalance);
					dto.setAmount(dto.getAmount().negate().setScale(2, BigDecimal.ROUND_HALF_UP));
				}else {
					if (statementInfo.getMemberName().equals(dto.getMemberName()) || "-".equals(dto.getMemberName())) {
						topupTotal = topupTotal.add(dto.getAmount());
					}
					cashBalance = cashBalance.add(dto.getAmount());
					dto.setCashvalueBalance(cashBalance);
				}
				
				allCashvalueList.add(new String[]{DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),dto.getInvoiceNo().toString(),dto.getDescription(),
						dto.getPaymentType(),dto.getMemberName(),getFormatAmount(dto.getAmount()),getFormatAmount(dto.getCashvalueBalance())});
				if (i==patronAccountStatementDtos.size()-1) {
					closingBalance = dto.getCashvalueBalance();//In case the data has been updated, so set the last PatronAccountStatementDto's CashvalueBalance for the closingBalance.
				}
			}
			allCashvalueList.add(new String[] {null,null,"Closing Balance",null,null,null,getFormatAmount(closingBalance)});
			
			statementInfo.setCashvalue(closingBalance);//In case the data is not correct because of the report is not been generated at first day of current month.
			
			addStatementBaseInfo(writer,document,statementInfo);
			
			PdfPTable allCashvaluePdfPTable = new PdfPTable(allCashvalueList.get(0).length);
			allCashvaluePdfPTable.setTotalWidth(new float[]{20f,15f,20f,20f,20f,20f,20f});
			Font font = new Font(Font.TIMES_ROMAN, 10);
			for (int i = 0; i < allCashvalueList.size(); i++) {
				String[] strings = allCashvalueList.get(i);
				for (int j = 0; j < strings.length; j++) {
					PdfPCell cell = new PdfPCell();
					if (i == 0 ) {
						cell.setBackgroundColor(color);
						cell.setPhrase(new Phrase(strings[j], tableTitleFont));
					} else if (i == 1 || i == allCashvalueList.size()-1) {
						cell.setBackgroundColor(Color.white);
						cell.setPhrase(new Phrase(strings[j], firstColumnFont));
					}else {
						cell.setBackgroundColor(Color.white);
						cell.setPhrase(new Phrase(strings[j], font));
					}
					cell.setNoWrap(false);
					if (j == strings.length -1 || j == strings.length -2) {
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}else {
						cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					}
					allCashvaluePdfPTable.addCell(cell);
				}
			}
			document.add(allCashvaluePdfPTable);
			addBalanceHistoryTable(document,closingBalance,customerId,null);
			
			Paragraph otherDetail = getParagraph("For Other Consumption Detail ",firstColumnFont,10f,0f,57f);
			document.add(otherDetail);
			
			
			Paragraph pmTitle = getParagraph("Primary Patron - "+statementInfo.getMemberName(),firstColumnFont,10f,5f,57f);
			document.add(pmTitle);
			
//			addOtherConsumptionTable(document,customerId,startDate,endDate);//Add other consumption table include primary member and dependent members.
			addOtherConsumptionTable(document,customerId,null,null);//Add other consumption table include primary member and dependent members.
			
			Paragraph summaryTitle = getParagraph("Summary",firstColumnFont,0f,10f,57f);
			document.add(summaryTitle);
			
//			addSummaryTable(document,customerId,startDate,endDate);//Add summary table include primary member and dependent members.
			addSummaryTable(document,customerId,null,null);//Add summary table include primary member and dependent members.
			
			addFootContent(document);//Add foot content.
			
			document.close();
//			InputStream inputStream = new FileInputStream(filePath);
			
			if (null!=out) {
				out.flush();
				out.close();
			}
		
		}
	}
	
	private Paragraph getParagraph(String content, Font font, float before, float after, float indentationLeft) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Phrase(content,font));
		paragraph.setAlignment(Element.ALIGN_LEFT);
//		paragraph.setLeading(15f);
		paragraph.setSpacingBefore(before);
		paragraph.setSpacingAfter(after);
		paragraph.setIndentationLeft(indentationLeft);
		return paragraph;
	}

	@Transactional
	private void calculateClosingBalance(List<MemberDto> memberDtos, String startDateString, String endDateString) {
		for (MemberDto dto:memberDtos) {
			Long customerId = dto.getCustomerId();
			Member member = memberDao.get(Member.class, customerId);
			if (memberType.CPM.name().equalsIgnoreCase(member.getMemberType()) || memberType.IPM.name().equalsIgnoreCase(member.getMemberType())) {
				MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
				Date date = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				
				MemberCashvalueBalHistory memberCashvalueBalHistory = new MemberCashvalueBalHistory();
				MemberCashvalueBalHistory existMemberCashvalueBalHistory = null;
				int month = calendar.get(Calendar.MONTH);
				calendar.add(Calendar.MONTH, -2);
				calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
				Date cutoffDate = calendar.getTime();  
				if (null != endDateString) {
					Date endDate = DateConvertUtil.parseString2Date(endDateString, "yyyy-MM-dd");
					calendar.setTime(endDate);
					calendar.add(Calendar.MONTH, -1);
					calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
					cutoffDate = calendar.getTime();
				}
				String strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd")+" 23:59:59";
				MemberCashvalueBalHistory lastMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCutoffDate)});
				
				String totalTopUpSql = "SELECT sum(trans.paid_amount) as totalTopUp from customer_order_trans trans, customer_order_hd hd, customer_order_det det, member m"
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no  ";
				if (null != startDateString) {
					totalTopUpSql = totalTopUpSql + " and (DATE(trans.transaction_timestamp) BETWEEN '" + startDateString + "' and '" + endDateString + "')";
				}else {
					totalTopUpSql = totalTopUpSql + " and (DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH)))";
				}
				
				totalTopUpSql = totalTopUpSql +  " and ((trans.`status` = 'SUC' and det.item_no = 'CVT0000001') OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001')) and hd.customer_id = m.customer_id "
						+ " and (m.customer_id = ? or m.superior_member_id = ?) ";
				
				String totalSpendingSql = "SELECT sum(trans.paid_amount) as totalSpending from customer_order_trans trans, customer_order_hd hd, customer_order_det det, member m"
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no ";
				if (null != startDateString) {
					totalSpendingSql = totalSpendingSql + " and (DATE(trans.transaction_timestamp) BETWEEN '" + startDateString + "' and '" + endDateString + "')";
				}else {
					totalSpendingSql = totalSpendingSql + " and (DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH)))";
				}
				totalSpendingSql = totalSpendingSql +  " and ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = 'CV') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU'))"
						+ " and hd.customer_id = m.customer_id and (m.customer_id = ? or m.superior_member_id = ? ) ";
				BigDecimal closingBalance = null;
				BigDecimal openingBalance =null;
				List<Serializable> param = new ArrayList<Serializable>();
				param.add(customerId);
				param.add(customerId);
				BigDecimal totalTopup = (BigDecimal)customerTransactionDao.getUniqueBySQL(totalTopUpSql, param);
				BigDecimal sumTopup = new BigDecimal(null == totalTopup ?0:totalTopup.doubleValue());
				BigDecimal totalSpending = (BigDecimal)customerTransactionDao.getUniqueBySQL(totalSpendingSql, param);
				BigDecimal sumSpending = new BigDecimal(null == totalSpending ?0:totalSpending.doubleValue());
				if (null == lastMemberCashvalueBalHistory) {
					Date joinDate = member.getFirstJoinDate();
					calendar.setTime(date);
					calendar.add(Calendar.MONTH, -1);
					String currentMonthString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM");
					if (null != startDateString) {
						currentMonthString = startDateString.substring(0, 6);
					}
					if (DateConvertUtil.date2String(joinDate, "yyyy-MM").equals(currentMonthString) && memberType.IPM.name().equalsIgnoreCase(member.getMemberType())) {
						//if the member's join date is last month, it's opening balance is it's inital cash value.
						
						if (null == memberCashvalue) {
							openingBalance = new BigDecimal(0);
						}else {
							openingBalance = memberCashvalue.getInitialValue();
						}
					}else {
						openingBalance = new BigDecimal(0);
					}
					
				}else {
					openingBalance = lastMemberCashvalueBalHistory.getRecalBalance();
				}
				closingBalance = openingBalance.add(sumTopup).subtract(sumSpending);
				
				calendar.setTime(date);
				month = calendar.get(Calendar.MONTH);
				calendar.add(Calendar.MONTH, -1);
				calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH)); 
//				calendar.set(Calendar.DAY_OF_MONTH,1);  
//				calendar.add(Calendar.DAY_OF_MONTH, -1);
				cutoffDate = calendar.getTime(); 
				if (null != endDateString) {
					cutoffDate = DateConvertUtil.parseString2Date(endDateString, "yyyy-MM-dd");
				}
				strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd")+" 23:59:59";
				existMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCutoffDate)});
				if (null == existMemberCashvalueBalHistory) {
					memberCashvalueBalHistory.setCutoffDate(DateConvertUtil.getDateFromStr(strCutoffDate));
					memberCashvalueBalHistory.setLastCashvalueBal(memberCashvalue==null ?new BigDecimal(0):memberCashvalue.getAvailableBalance());
					memberCashvalueBalHistory.setLastCashvalueDate(date);
					memberCashvalueBalHistory.setProcessDate(new Timestamp(date.getTime()));
					memberCashvalueBalHistory.setRecalBalance(closingBalance);
					memberCashvalueBalHistory.setCustomerId(customerId);
					memberCashvalueBalHistoryDao.save(memberCashvalueBalHistory);
					/*select all refer to cash value transaction records insert into member_transaction_log*/
					String insertSql = "INSERT INTO member_transaction_log(transaction_no,process_id,order_no,payment_method_code,payment_media,"
							+ " transaction_timestamp,from_transaction_no,paid_amount,status,audit_by,audit_date,internal_remark,"
							+ " payment_location_code,create_by,update_date,update_by)"
							+ " SELECT DISTINCT trans.transaction_no,"+memberCashvalueBalHistory.getProcessId()+",trans.order_no,trans.payment_method_code,trans.payment_media,"
							+ " trans.transaction_timestamp,trans.from_transaction_no,trans.paid_amount,trans.status,trans.audit_by,"
							+ " trans.audit_date,trans.internal_remark,trans.payment_location_code,trans.create_by,trans.update_date,trans.update_by "
							+ " from customer_order_trans trans,customer_order_hd hd,customer_order_det det,member m"
							+ " WHERE ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = 'CV') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU')"
							+ " OR (det.item_no = 'SRR00000001' AND trans.`status` = 'RFU')) "
							+ " and trans.order_no = hd. order_no and hd.customer_id = m.customer_id and hd.order_no = det.order_no"
							+ " and (m.customer_id = ? or m.superior_member_id = ?)";
					if (null != startDateString) {
						insertSql = insertSql + " AND (DATE(trans.transaction_timestamp) BETWEEN '" + startDateString+ "' and '" + endDateString + "')";
					}else {
						insertSql = insertSql + " AND (DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) " + " and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH)))";
					}
					
					
					memberTransactionLogDao.sqlUpdate(insertSql, param);
				}else {
					existMemberCashvalueBalHistory.setCutoffDate(DateConvertUtil.getDateFromStr(strCutoffDate));
					existMemberCashvalueBalHistory.setLastCashvalueBal(memberCashvalue==null ?new BigDecimal(0):memberCashvalue.getAvailableBalance());
					existMemberCashvalueBalHistory.setLastCashvalueDate(date);
					existMemberCashvalueBalHistory.setProcessDate(new Timestamp(date.getTime()));
					existMemberCashvalueBalHistory.setRecalBalance(closingBalance);
					existMemberCashvalueBalHistory.setCustomerId(customerId);
					memberCashvalueBalHistoryDao.update(existMemberCashvalueBalHistory);
				}
			}
		}
	}
	public static void main(String[] args) {
		/*Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		String lastMonth = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd hh:mm:ss");
		System.out.println(lastMonth);
		System.out.println();
		*/
		BigDecimal decimal = new BigDecimal(123456789.00);
		System.out.println(new DecimalFormat("###,##0.00").format(decimal));
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH);
		System.out.println(month);
		calendar.add(Calendar.MONTH, -2);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
		Date strDateTo = calendar.getTime(); 
		String cutoffDate =DateConvertUtil.date2String(strDateTo, "yyyy-MM-dd")+" 23:59:59";
		
		System.out.println(DateConvertUtil.getDateFromStr(cutoffDate));
		
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMinimum(Calendar.DAY_OF_MONTH));  
		Date strDateFrom = calendar.getTime(); 
		String cutoffDate1 =DateConvertUtil.date2String(strDateFrom, "yyyy-MM-dd")+" 23:59:59";
		
		System.out.println(DateConvertUtil.getDateFromStr(cutoffDate1));
		String cutoffDate2 =DateConvertUtil.date2String(strDateFrom, "yyyyMM");
		System.out.println(cutoffDate2);
		
		
		 Calendar calendar1 = Calendar.getInstance();
		 calendar1.set(Calendar.DAY_OF_MONTH, 1);
		 calendar1.add(Calendar.MONTH, 1);
	     System.out.println(calendar1.getTime());
	}
	
	@Transactional
	public void modifyCustomerOrderHd(CustomerOrderHd order) {
		customerOrderHdDao.update(order);
	}
	
	@Transactional
	private CustomerStatementBaseInfoDto getCustomerStatementBaseInfoDto(Long customerId, String startDateString, String endDateString) {
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, customerId);
		final String email = customerProfile.getContactEmail();
		Member member = memberDao.get(Member.class, customerId);
		String memberName = customerProfile.getGivenName()+" "+customerProfile.getSurname();
		String companyName =customerProfile.getCompanyName();
		String address1 = customerProfile.getPostalAddress1();
		String address2 = customerProfile.getPostalAddress2	();
		String address3 = customerProfile.getPostalDistrict();
		String academyId = member.getAcademyNo();
		String typeDesc = memberTypeDao.get(MemberType.class, member.getMemberType()).getTypeName();
		MemberPaymentAcc acc = memberPaymentAccDao.getMemberPaymentAccByCustomerId(customerId);
		String virtualNo = acc == null?"":acc.getAccountNo();
		MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
		BigDecimal cashvalue = memberCashvalue == null ? BigDecimal.ZERO:memberCashvalue.getAvailableBalance();
		BigDecimal creditLimit = null;
		MemberLimitRule memberLimitRule = null;
		if (Constant.memberType.CPM.name().equalsIgnoreCase(member.getMemberType()) || Constant.memberType.IPM.name().equalsIgnoreCase(member.getMemberType())) {
			memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, "CR");
		}else {
			memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, "TRN");
		}
		if (memberLimitRule != null) {
			creditLimit = memberLimitRule.getNumValue();
//			creditLimit = "$"+numValue.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		}
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMinimum(Calendar.DAY_OF_MONTH)); 
		String from = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
		if (null != startDateString) {
			from = startDateString;
		}
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
		String to = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
		if (null != endDateString) {
			to = endDateString;
		}
		String period = from + " to "+to;
		String printDate = DateConvertUtil.date2String(date, "yyyy-MM-dd");
		String statementBalance = "";
		CustomerStatementBaseInfoDto statementBaseInfoDto = new CustomerStatementBaseInfoDto(memberName, companyName, address1, address2, address3, email, academyId, typeDesc, virtualNo, creditLimit, period, printDate, statementBalance,cashvalue);
		return statementBaseInfoDto;
	}
	
	public String getFormatAmount(BigDecimal amount){
		DecimalFormat format = new DecimalFormat("##,##0.00");
		return format.format(amount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
	}
	private void addStatementBaseInfo(PdfWriter writer,Document document,CustomerStatementBaseInfoDto statementInfo) {
		try {
			String logoPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			logoPath = logoPath.substring(0, logoPath.indexOf("WEB-INF")) + "WEB-INF/"+"jasper/logo_header.jpg";
			Image logo = Image.getInstance(logoPath);
			logo.setAlignment(Image.MIDDLE);
			logo.setAbsolutePosition(0, 0);
			logo.scalePercent(25);
			Chunk chunk = new Chunk(logo, 0, 0,true);
			Phrase phrase = new Phrase();
			phrase.add(chunk);
			Rectangle rect = new Rectangle(36, 54, 559, 835);
			CustomHeaderFooter header = new CustomHeaderFooter();
			writer.setPageEvent(header);
			document.open();
			header.onEndPage(writer, document,rect,phrase);
			Font titleFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
			Color color = new Color(140, 0, 30);
			titleFont.setColor(color);
			Paragraph title = getParagraph("Patron Account Statement",titleFont,20f,0f,0f);
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			
			Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
			Paragraph leftParagraph = getParagraph("",contentFont,20f,20f,0f);
			
			PdfPTable infoPdfPTable = new PdfPTable(3);
			PdfPCell infoCell = new PdfPCell();
			infoCell.setPhrase(new Phrase(statementInfo.getMemberName(), contentFont));
			infoCell.setBorder(0);
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0.5f);
			infoCell.setPhrase(new Phrase("Academy ID", contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0.5f);
			infoCell.setPhrase(new Phrase(statementInfo.getAcademyId(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getCompanyName(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Member Type", contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase(statementInfo.getTypeDesc(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getAddress1(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Virtual Account", contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setPhrase(new Phrase(CommUtil.formatVirtualAcc(statementInfo.getVirtualNo()), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getAddress2(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Cash Value", contentFont));
			infoPdfPTable.addCell(infoCell);
			if (null == statementInfo.getCashvalue()) {
				infoCell.setPhrase(new Phrase("$0.00", contentFont));
			}else {
				infoCell.setPhrase(new Phrase("$"+getFormatAmount(statementInfo.getCashvalue()), contentFont));
			}
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getAddress3(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Credit Limit", contentFont));
			infoPdfPTable.addCell(infoCell);
			if (null == statementInfo.getCreditLimit()) {
				infoCell.setPhrase(new Phrase("Unlimited", contentFont));
			}else {
				infoCell.setPhrase(new Phrase("$"+getFormatAmount(statementInfo.getCreditLimit()), contentFont));
			}
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getEmail(), contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Statement Period", contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setPhrase(new Phrase(statementInfo.getPeriod(), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase("", contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Print Date", contentFont));
			infoPdfPTable.addCell(infoCell);
			
			infoCell.setPhrase(new Phrase(statementInfo.getPrintDate(), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);
			
			leftParagraph.add(infoPdfPTable);
			
			document.add(leftParagraph);
			
			Paragraph detailParagraph = getParagraph("For Cash Value Detail (Primary Patron and Dependent Patron)", new Font(Font.TIMES_ROMAN, 10, Font.BOLD), 0f, 5f, 57f);
			document.add(detailParagraph);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void addFootContent(Document document){
		try {
			Paragraph foot = new Paragraph();
			Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
			foot.setFont(contentFont);
			foot.setAlignment(Element.ALIGN_CENTER);
			foot.setLeading(15f);
			foot.setSpacingBefore(20f);
			foot.add(new Phrase("If you have any questions, pleasse contact our Guest Service Office."));
			foot.add(new Phrase("\n"));
			foot.add(new Phrase("Thank You For Your Patronage!",firstColumnFont));
			document.add(foot);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult getStatementPDF(StatementParamsDto statementParamsDto) throws Exception{
		String year = statementParamsDto.getYear();
		String month = statementParamsDto.getMonth();
//		Long customerId = statementParamsDto.getCustomerIds()[0];
		String cutOffDateString  = year+"-"+month+"-01 23:59:59";
		int mon = Integer.parseInt(month);
		if (mon < 10) {
			cutOffDateString  = year+"-0"+month+"-01 23:59:59";
		}
		Date date = DateConvertUtil.getDateFromStr(cutOffDateString);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date date2 = calendar.getTime();
		System.out.println(DateConvertUtil.date2String(date2, "yyyy-MM-dd HH:mm:ss"));
		String strCutoffDate = DateConvertUtil.date2String(date2, "yyyy-MM-dd HH:mm:ss");
		List<Long> customerIdList = new ArrayList<Long>();
		if (statementParamsDto.getIsAll()) {
			String sql = "SELECT  m.customer_id as customerId,m.academy_no as academyId,CONCAT(cp.salutation,' ',cp.given_name,' ',cp.surname) as memberName,"
					+ " (CASE m.member_type WHEN 'IPM' THEN 'Individual' WHEN 'CPM' THEN 'Corporate' ELSE '' END) as memberType,mcbh.recal_balance as closingBalance "
					+ " from member_cashvalue_bal_history mcbh ,customer_profile cp, member m "
					+ " where mcbh.customer_id = m.customer_id and cp.customer_id = m.customer_id "
					+ "  and mcbh.cutoff_date = ?  order by customerId";
			
			List<StatementDto> statementDtos = memberCashvalueBalHistoryDao.getDtoBySql(sql, Arrays.asList(strCutoffDate), StatementDto.class);
			for (StatementDto dto : statementDtos) {
				customerIdList.add(dto.getCustomerId());
			}
			statementParamsDto.setCustomerIds(customerIdList.toArray(new Long[customerIdList.size()]));
		}
		Long[] customerIds = statementParamsDto.getCustomerIds();
		BatchSendStatementHd statementHd = null;
		if (statementParamsDto.isSent()){
			statementHd = recordStatmentSendBatch(statementParamsDto!=null?statementParamsDto.getUserId():null,date);
		}
		for (int k = 0; k < customerIds.length; k++) {
			Long customerId = customerIds[k];
			CustomerStatementBaseInfoDto statementInfo = getCustomerStatementBaseInfoDto(customerId,null,null);
			
			String basePath = appProps.getProperty("pdf.server.report");// /opt/file-server/report/statement  or  D:\\fileServer\\report\\statement
			String strMonth = String.valueOf(mon);
			if (mon < 10) {
				strMonth  = "0"+month;
			}
			
			String filePath = basePath + "/Patron Account Statement_"+customerId+"_"+year+strMonth+".pdf";
			InputStream inputStream = null;
			try {
				inputStream = new FileInputStream(filePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new GTACommonException(GTAError.StatementError.FILE_NOT_EXIST);
			}
			if (statementParamsDto.isSent()) {
				MessageTemplate template = messageTemplateDao.getTemplateByFunctionId("monthly_statement");
				
				List<String> fileNameList = new ArrayList<String>();
				fileNameList.add("Patron Account Statement - "+statementInfo.getMemberName()+".pdf");
				List<String> mineTypeList = new ArrayList<String>();
				mineTypeList.add("application/pdf");
				List<byte[]> bytesList = new ArrayList<byte[]>();
//				bytesList.add(out.toByteArray());
				bytesList.add(IOUtils.toByteArray(inputStream));
				String content = template.getContentHtml();
				content = content.replaceAll(MessageTemplateParams.UserName.getParam(), statementInfo.getMemberName());
				String subject = template.getMessageSubject();
				String email = statementInfo.getEmail();
				String userId = statementParamsDto.getUserId();
				
				
				CustomerEmailContent customerEmailContent = new CustomerEmailContent();
				
				customerEmailContent.setContent(content);
				customerEmailContent.setRecipientCustomerId(customerId.toString());
				customerEmailContent.setRecipientEmail(email);
				customerEmailContent.setSendDate(new Date());
				customerEmailContent.setSenderUserId(userId);
				customerEmailContent.setSubject(subject);
				customerEmailContent.setStatus(EmailStatus.PND.name());
				String sendId = (String) customerEmailContentDao.save(customerEmailContent);
				customerEmailContent.setSendId(sendId);
				
				CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
				customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
				customerEmailAttach.setAttachmentName(fileNameList.get(0));
				customerEmailAttachDao.save(customerEmailAttach);
				recordStatmentSendBatchList(userId, Long.valueOf(sendId), customerId, statementHd);
				mailThreadService.sendWithResponse(statementHd.getBatchId(),customerEmailContent, bytesList, mineTypeList, fileNameList);
				
				responseResult.initResult(GTAError.Success.SUCCESS);
			}else {
				responseResult.initResult(GTAError.Success.SUCCESS, IOUtils.toByteArray(inputStream));
			}
			
//			if (null!=out) {
//				out.flush();
//				out.close();
//			}
		}
		return responseResult;
	}
	
	private void addBalanceHistoryTable(Document document, BigDecimal currentClosingBalance, Long customerId, Date startDate) {
		Font font = new Font(Font.TIMES_ROMAN, 10);
		PdfPTable balanceTable = new PdfPTable(6);
		balanceTable.setSpacingBefore(15f);
		PdfPCell balanceCell = new PdfPCell();
		balanceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		balanceCell.setPhrase(new Phrase("Current", font));
		balanceTable.addCell(balanceCell);
		
		balanceCell.setPhrase(new Phrase("31-60 Days", font));
		balanceTable.addCell(balanceCell);
		
		balanceCell.setPhrase(new Phrase("61-90 Days", font));
		balanceTable.addCell(balanceCell);
		
		balanceCell.setPhrase(new Phrase("91-120 Days", font));
		balanceTable.addCell(balanceCell);
		
		balanceCell.setPhrase(new Phrase("Over 120 Days", font));
		balanceTable.addCell(balanceCell);
		
		balanceCell.setPhrase(new Phrase("Total Balance Due", font));
		balanceTable.addCell(balanceCell);
		
//		BigDecimal last2MonthClosingBalance = getClosingBalance(customerId,2);//31-60 Days
//		BigDecimal last3MonthClosingBalance = getClosingBalance(customerId,3);//61-90 Days
//		BigDecimal last4MonthClosingBalance = getClosingBalance(customerId,4);//91-120 Days
		BigDecimal last5MonthClosingBalance = getClosingBalance(customerId,5);//Over 120 Days
		
		BigDecimal currentTotalTopup = null;
		BigDecimal currentTotalSpending = null;
		
		BigDecimal last2MonthTotalTopup = null;
		BigDecimal last2MonthTotalSpending = null;
		
		BigDecimal last3MonthTotalTopup = null;
		BigDecimal last3MonthTotalSpending = null;
		
		BigDecimal last4MonthTotalTopup = null;
		BigDecimal last4MonthTotalSpending = null;
		
//		BigDecimal last5MonthTotalTopup = getTotalTopupAmount(customerId,5);
//		BigDecimal last5MonthTotalSpending = getTotalSpendingAmount(customerId,5);
		if (null != startDate) {
			currentTotalTopup = getTotalTopupAmount(customerId,1,startDate);
			currentTotalSpending = getTotalSpendingAmount(customerId,1,startDate);
			
			last2MonthTotalTopup = getTotalTopupAmount(customerId,2,startDate);
			last2MonthTotalSpending = getTotalSpendingAmount(customerId,2,startDate);
			
			last3MonthTotalTopup = getTotalTopupAmount(customerId,3,startDate);
			last3MonthTotalSpending = getTotalSpendingAmount(customerId,3,startDate);
			
			last4MonthTotalTopup = getTotalTopupAmount(customerId,4,startDate);
			last4MonthTotalSpending = getTotalSpendingAmount(customerId,4,startDate);
		}else {
			currentTotalTopup = getTotalTopupAmount(customerId,1,null);
			currentTotalSpending = getTotalSpendingAmount(customerId,1,null);
			
			last2MonthTotalTopup = getTotalTopupAmount(customerId,2,null);
			last2MonthTotalSpending = getTotalSpendingAmount(customerId,2,null);
			
			last3MonthTotalTopup = getTotalTopupAmount(customerId,3,null);
			last3MonthTotalSpending = getTotalSpendingAmount(customerId,3,null);
			
			last4MonthTotalTopup = getTotalTopupAmount(customerId,4,null);
			last4MonthTotalSpending = getTotalSpendingAmount(customerId,4,null);
		}
		BigDecimal totalTopUpAmount = currentTotalTopup.add(last2MonthTotalTopup).add(last3MonthTotalTopup).add(last4MonthTotalTopup);
		BigDecimal last5MonthRemainBalance = totalTopUpAmount.add(last5MonthClosingBalance);
		BigDecimal last4MonthRemainBalance = last5MonthRemainBalance.subtract(last4MonthTotalSpending);
		BigDecimal last3MonthRemainBalance = last4MonthRemainBalance.subtract(last3MonthTotalSpending);
		BigDecimal last2MonthRemainBalance = last3MonthRemainBalance.subtract(last2MonthTotalSpending);
		BigDecimal currentMonthRemainBalance = last2MonthRemainBalance.subtract(currentTotalSpending);
		
		
		
		balanceCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		if (currentMonthRemainBalance.compareTo(BigDecimal.ZERO)>=0) {
			balanceCell.setPhrase(new Phrase("-", font));
		}else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(currentMonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);
		
		if (last2MonthRemainBalance.compareTo(BigDecimal.ZERO)>=0) {
			balanceCell.setPhrase(new Phrase("-", font));
		}else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last2MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);
		
		if (last3MonthRemainBalance.compareTo(BigDecimal.ZERO)>=0) {
			balanceCell.setPhrase(new Phrase("-", font));
		}else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last3MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);
		
		if (last4MonthRemainBalance.compareTo(BigDecimal.ZERO)>=0) {
			balanceCell.setPhrase(new Phrase("-", font));
		}else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last4MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);
		
		if (last5MonthRemainBalance.compareTo(BigDecimal.ZERO)>=0) {
			balanceCell.setPhrase(new Phrase("-", font));
		}else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last5MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);

		if (currentClosingBalance.compareTo(BigDecimal.ZERO)>=0) {
			balanceCell.setPhrase(new Phrase("-", font));
		}else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(currentClosingBalance), font));
		}
		balanceTable.addCell(balanceCell);
		
		try {
			document.add(balanceTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String content = "If payment by Bank Transfer, please remit to the following bank account:\n"
				+ "Account Name:\n"
				+ "Account No:\n"
				+ "Swift Code:\n"
				+ "Account Address:\n"
				+ "Bank Name:\n"
				+ "Bank Address:\n"
				+ "Please send the Remittance Advice to our Finance Department via email: ivankot@hkgta.com or Fax no. (852)xxxxxxxx\n"
				+ "If payment by cheque. Cheque should be crossed, mark \"Not Negotiable A/C Payee Only\" and made payable to \"Hong  Kong Golf & Academy Management Co., Ltd.\"\n"
				+ "If payment by credit card, please fill in the credit card authorization form and send to our Finance Department via email or fax.\n";
		Paragraph accountInfo = getParagraph(content, font, 10f, 0f, 57f);
		try {
			document.add(accountInfo);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private BigDecimal getTotalSpendingAmount(Long customerId, int monthCountAgo,Date startDate) {
		String totalSpendingSql = "SELECT sum(trans.paid_amount) as totalSpending from customer_order_trans trans, customer_order_hd hd, customer_order_det det, member m"
				+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no ";
		String startDateString = "";
		String endDateString = "";
		if (null != startDate) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			calendar.add(Calendar.MONTH, -(monthCountAgo-1));
			startDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			endDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
			totalSpendingSql = totalSpendingSql + " and (DATE(trans.transaction_timestamp) BETWEEN '"+startDateString+"' and '"+endDateString+"')";
		}else {
			totalSpendingSql = totalSpendingSql + " and (DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL "+monthCountAgo+" MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL "+monthCountAgo+" MONTH)))";
		}
		totalSpendingSql = totalSpendingSql	+ " and ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = 'CV') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU'))"
				+ " and hd.customer_id = m.customer_id and (m.customer_id = ? or m.superior_member_id = ? ) ";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		BigDecimal totalSpending = (BigDecimal)customerTransactionDao.getUniqueBySQL(totalSpendingSql, param);
		BigDecimal sumSpending = new BigDecimal(null == totalSpending ?0:totalSpending.doubleValue());
		return sumSpending;
	}

	@Transactional
	private BigDecimal getTotalTopupAmount(Long customerId, int monthCountAgo,Date startDate) {
		
		String totalTopUpSql = "SELECT sum(trans.paid_amount) as totalTopUp from customer_order_trans trans, customer_order_hd hd, customer_order_det det, member m"
				+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no ";
		String startDateString = "";
		String endDateString = "";
		if (null != startDate) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			calendar.add(Calendar.MONTH, -(monthCountAgo-1));
			startDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			endDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
			totalTopUpSql = totalTopUpSql + " and (DATE(trans.transaction_timestamp) BETWEEN '"+startDateString+"' and '"+endDateString+"')";
		}else {
			totalTopUpSql = totalTopUpSql + " and (DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL "+monthCountAgo+" MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL "+monthCountAgo+" MONTH)))";
		}
		
		totalTopUpSql = totalTopUpSql +  " and ((trans.`status` = 'SUC' and det.item_no = 'CVT0000001') OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001')) and hd.customer_id = m.customer_id "
				+ " and (m.customer_id = ? or m.superior_member_id = ?) ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		BigDecimal totalTopup = (BigDecimal)customerTransactionDao.getUniqueBySQL(totalTopUpSql, param);
		BigDecimal sumTopup = new BigDecimal(null == totalTopup ?0:totalTopup.doubleValue());
		return sumTopup;
	}

	@Transactional
	private BigDecimal getClosingBalance(Long customerId, int monthCountAgo) {
//		Member member = memberDao.get(Member.class, customerId);
//		MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH);
		calendar.add(Calendar.MONTH, -monthCountAgo);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
		Date cutoffDate = calendar.getTime();  
		String strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd")+" 23:59:59";
		MemberCashvalueBalHistory memberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCutoffDate)});
		
		return memberCashvalueBalHistory == null ?BigDecimal.ZERO:memberCashvalueBalHistory.getRecalBalance();
	}

	@SuppressWarnings("unchecked")
	private BatchSendStatementHd recordStatmentSendBatch(String userId,Date statementMonth){
		BatchSendStatementHd statementHd = new BatchSendStatementHd();
		statementHd.setCreateBy(userId);
		statementHd.setCreateDate(new Date());
		statementHd.setErrorCount(Long.valueOf(0));
		statementHd.setStatementMonth(statementMonth);
		Long batchId = (Long) deliveryRecordDao.save(statementHd);
		statementHd.setBatchId(batchId);
		return statementHd;
	}
	
	@SuppressWarnings("unchecked")
	private void recordStatmentSendBatchList(String userId,Long emailSendId,Long customerId,BatchSendStatementHd statementHd){
		BatchSendStatementList statementList  = new BatchSendStatementList();
		statementList.setEmailSendId(emailSendId);
		statementList.setBatchSendStatementHd(statementHd);
		statementList.setCreateBy(userId);
		statementList.setCreateDate(new Date());
		MemberCashvalue memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		statementList.setCashvalueBalance(memberCashvalue!=null?memberCashvalue.getAvailableBalance():null);
		deliveryRecordDao.save(statementList);
		
	}
	
	@Transactional
	private void addSummaryTable(Document document, Long customerId,String startDateString,String endDateString) {
		try {
			Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
			Color color = new Color(140, 0, 30);
			Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, Color.white);
			List<Member> allMembers = new ArrayList<Member>();
			Member primaryMember = memberDao.get(Member.class, customerId);
			allMembers.add(primaryMember);
			List<Member> dependentMembers = memberDao.getListMemberBySuperiorId(customerId);
			allMembers.addAll(dependentMembers);
			for (Member member: allMembers) {
				PdfPTable topupTotalTable = new PdfPTable(5);
				PdfPCell topupTotalCell = new PdfPCell();
				
				CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, member.getCustomerId());
				
				String totalTopUpSql = "SELECT sum(trans.paid_amount) as totalTopUp from customer_order_trans trans, customer_order_hd hd, customer_order_det det "
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no ";
				if (null != startDateString) {
					totalTopUpSql = totalTopUpSql + " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '" + endDateString + "') ";
				}else {
					totalTopUpSql = totalTopUpSql + " and (DATE(trans.transaction_timestamp) BETWEEN  DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH))) ";
				}
						
				totalTopUpSql = totalTopUpSql + " and ((trans.`status` = 'SUC' and det.item_no = 'CVT0000001') OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001')) and hd.customer_id = ? ";
				String totalSpendingSql = "SELECT sum(trans.paid_amount) as totalSpending from customer_order_trans trans, customer_order_hd hd, customer_order_det det"
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no ";
				if (null != startDateString) {
					totalSpendingSql = totalSpendingSql + " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '" + endDateString + "') ";
				}else {
					totalSpendingSql = totalSpendingSql + " and (DATE(trans.transaction_timestamp) BETWEEN  DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH))) ";
				}
						
				totalSpendingSql = totalSpendingSql + " and ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU'))"
						+ " and hd.customer_id = ? ";
				
				String refundSql = "SELECT trans.transaction_no as transactionNo,trans.paid_amount as paidAmount,trans.transaction_timestamp as transactionTimestamp from customer_order_trans trans, customer_order_hd hd, customer_order_det det "
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no  ";
				if (null != startDateString) {
					refundSql = refundSql + " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '" + endDateString + "') ";
				}else {
					refundSql = refundSql + " and (DATE(trans.transaction_timestamp) BETWEEN  DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH))) ";
				}
				refundSql = refundSql + " and trans.`status` = 'RFU' AND det.item_no = 'SRR00000001' and hd.customer_id = ? ";
				String countSql = "SELECT COUNT(tt.transactionNo) from (" + refundSql +") tt";
				
				BigDecimal totalTopup = (BigDecimal)customerTransactionDao.getUniqueBySQL(totalTopUpSql, Arrays.asList((Serializable)member.getCustomerId()));
				BigDecimal sumTopup = new BigDecimal(null == totalTopup ?0:totalTopup.doubleValue());
				
				BigDecimal totalSpending = (BigDecimal)customerTransactionDao.getUniqueBySQL(totalSpendingSql, Arrays.asList((Serializable)member.getCustomerId()));
				BigDecimal sumSpending = new BigDecimal(null == totalSpending ?0:totalSpending.doubleValue());
				
				ListPage<CustomerOrderTrans> page = new ListPage<CustomerOrderTrans>();
				
				page =  customerOrderTransDao.listBySqlDto(page, countSql, refundSql, Arrays.asList((Serializable)member.getCustomerId()), new RefundItemDto());
				
				StringBuilder builder = new StringBuilder();
				List<Object> transDtos = page.getDtoList();
				for (Object object: transDtos) {
					RefundItemDto refundDto = (RefundItemDto)object;
					builder.append("*** HK$"+getFormatAmount(refundDto.getPaidAmount())+" refund to cash value ["+DateConvertUtil.date2String(refundDto.getTransactionTimestamp(), "dd/MM/yyyy")+"]");
					builder.append("\n");
				}
				if (com.sinodynamic.hkgta.util.constant.MemberType.IPM.getType().equals(member.getMemberType()) 
						|| com.sinodynamic.hkgta.util.constant.MemberType.CPM.getType().equals(member.getMemberType())) {
					topupTotalCell.setPhrase(new Phrase("Cash Value Top Up Total of Primary Patron - "+customerProfile.getGivenName()+" "+customerProfile.getSurname(), tableTitleFont));
				}else {
					topupTotalCell.setPhrase(new Phrase("Cash Value Top Up Total of Dependent Patron - "+customerProfile.getGivenName()+" "+customerProfile.getSurname(), tableTitleFont));
				}
				topupTotalCell.setBackgroundColor(color);
				topupTotalTable.addCell(topupTotalCell);
				
				topupTotalCell.setBackgroundColor(Color.white);
				topupTotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				topupTotalCell.setPhrase(new Phrase("HK$"+getFormatAmount(sumTopup), contentFont));
				topupTotalTable.addCell(topupTotalCell);
				
				topupTotalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				topupTotalCell.setBorder(0);
				topupTotalCell.setPhrase(new Phrase(builder.toString(), contentFont));
				topupTotalTable.addCell(topupTotalCell);
				topupTotalCell.setBorderWidthBottom(0.5f);
				topupTotalCell.setBorderWidthLeft(0.5f);
				topupTotalCell.setBorderWidthRight(0.5f);
				topupTotalCell.setBorderWidthTop(0.5f);
				topupTotalCell.setBackgroundColor(color);
				if (com.sinodynamic.hkgta.util.constant.MemberType.IPM.getType().equals(member.getMemberType()) 
						|| com.sinodynamic.hkgta.util.constant.MemberType.CPM.getType().equals(member.getMemberType())) {
					topupTotalCell.setPhrase(new Phrase("Consumption Total (All Payment Type) of Primary Patron - "+customerProfile.getGivenName()+" "+customerProfile.getSurname(), tableTitleFont));
				}else {
					topupTotalCell.setPhrase(new Phrase("Consumption Total (All Payment Type) of Dependent Patron - "+customerProfile.getGivenName()+" "+customerProfile.getSurname(), tableTitleFont));
				}
				topupTotalTable.addCell(topupTotalCell);
				topupTotalCell.setBackgroundColor(Color.white);
				topupTotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				topupTotalCell.setPhrase(new Phrase("HK$"+getFormatAmount(sumSpending), contentFont));
				topupTotalCell.setBorderWidthBottom(0.5f);
				topupTotalCell.setBorderWidthLeft(0);
				topupTotalCell.setBorderWidthRight(0.5f);
				topupTotalCell.setBorderWidthTop(0.5f);
				topupTotalTable.addCell(topupTotalCell);
				
				topupTotalTable.setSpacingAfter(20f);
				document.add(topupTotalTable);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	@Transactional
	private void addOtherConsumptionTable(Document document, Long customerId,String startDateString,String endDateString) {
		try {
			Font font = new Font(Font.TIMES_ROMAN, 10);
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
			Color color = new Color(140, 0, 30);
			Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.white);
			List<String[]> pmOtherConsumptionList =  new ArrayList<String[]>();
			pmOtherConsumptionList.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type", "Amount(HK$)"});
			String otherPaymenetSql = "SELECT DISTINCT trans.transaction_timestamp as transactionDate,trans.transaction_no as 	invoiceNo,pos.description as description,"
					+ " trans.payment_method_code as paymentType,trans.paid_amount as amount,det.item_no as itemNo"
					+ " from customer_order_trans trans, customer_order_hd hd,customer_order_det det,pos_service_item_price pos"
					+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no and det.item_no = pos.item_no and trans.`status` = 'SUC' "
					+ " AND trans.payment_method_code != 'CV' AND det.item_no != 'CVT0000001' AND det.item_no != 'SRR00000001' "
			+ " AND ( LEFT (det.item_no, 3) != 'SRV' AND LEFT (det.item_no, 5) != 'RENEW' OR ( LEFT (det.item_no, 3) = 'SRV' AND pos.item_catagory = 'DP')) ";
			if (null != startDateString) {
				otherPaymenetSql = otherPaymenetSql + " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '" + endDateString + "') ";
			}else {
				otherPaymenetSql = otherPaymenetSql + " and (DATE(trans.transaction_timestamp) BETWEEN  DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH))) ";
			}
					
			otherPaymenetSql = otherPaymenetSql + " and hd.customer_id = ? ORDER BY trans.transaction_timestamp";
		
			List<Serializable> param1 = new ArrayList<Serializable>();
			param1.add(customerId);
			List<PatronAccountStatementDto> patronAccountStatementDtos = customerOrderTransDao.getDtoBySql(otherPaymenetSql, param1, PatronAccountStatementDto.class);
			for (PatronAccountStatementDto dto:patronAccountStatementDtos) {
//				if (dto.getItemNo().indexOf("PMS") >= 0 ) {//所有的消费的description已经修改的有意义了，因此这个地方不需要再转。
//					dto.setDescription("Accommodation");
//				}
//				if (Constant.TOPUP_ITEM_NO.equalsIgnoreCase(dto.getItemNo())) {
//					dto.setDescription("Top up for cash value");
//				}
//				spendingTotal = spendingTotal.add(dto.getAmount());
				pmOtherConsumptionList.add(new String[]{DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),dto.getInvoiceNo().toString(),dto.getDescription(),
							dto.getPaymentType(),getFormatAmount(dto.getAmount())});
			}
			if (pmOtherConsumptionList.size() < 2) {
				pmOtherConsumptionList.add(new String[] { " ", " ", " ", " ", " "});
			}
			PdfPTable pmOtherConsumptionTable = new PdfPTable(pmOtherConsumptionList.get(0).length);
			for (int i = 0; i < pmOtherConsumptionList.size(); i++) {
				String[] strings = pmOtherConsumptionList.get(i);
				for (int j = 0; j < strings.length; j++) {
					PdfPCell cell = new PdfPCell();
					if (i == 0) {
						cell.setBackgroundColor(color);
						cell.setPhrase(new Phrase(strings[j], tableTitleFont));
					} else {
						cell.setBackgroundColor(Color.white);
						cell.setPhrase(new Phrase(strings[j], font));
					}
					cell.setNoWrap(false);
					if (j == strings.length-1) {
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}else {
						cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					}
					pmOtherConsumptionTable.addCell(cell);
				}
			}
			pmOtherConsumptionTable.setSpacingAfter(20f);
			document.add(pmOtherConsumptionTable);
			
			
			List<Member> dependentMembers = memberDao.getListMemberBySuperiorId(customerId);
			if (null!=dependentMembers && dependentMembers.size()>0) {
				for (Member dependentMember : dependentMembers) {
					CustomerProfile dependentProfile = customerProfileDao.get(CustomerProfile.class, dependentMember.getCustomerId());
					Paragraph denOtherDetail = getParagraph("Dependent Patron - "+dependentProfile.getGivenName()+" "+dependentProfile.getSurname(), firstColumnFont, 0f, 5f, 57f);
					document.add(denOtherDetail);
					
					List<String[]> dmOtherConsumptionList =  new ArrayList<String[]>();
					dmOtherConsumptionList.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type", "Amount(HK$)"});
				
					List<Serializable> denParam = new ArrayList<Serializable>();
					denParam.add(dependentMember.getCustomerId());
					List<PatronAccountStatementDto> denPatronAccountStatementDtos = customerOrderTransDao.getDtoBySql(otherPaymenetSql, denParam, PatronAccountStatementDto.class);
					for (PatronAccountStatementDto dto:denPatronAccountStatementDtos) {
//						if (dto.getItemNo().indexOf("PMS") >= 0 ) {//所有的消费的description已经修改的有意义了，因此这个地方不需要再转。
//							dto.setDescription("Accommodation");
//						}
//						if (Constant.TOPUP_ITEM_NO.equalsIgnoreCase(dto.getItemNo())) {
//							dto.setDescription("Top up for cash value");
//						}
						dmOtherConsumptionList.add(new String[]{DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),dto.getInvoiceNo().toString(),dto.getDescription(),
									dto.getPaymentType(),getFormatAmount(dto.getAmount())});
					}
					if (dmOtherConsumptionList.size() < 2) {
						dmOtherConsumptionList.add(new String[] { " ", " ", " ", " ", " "});
					}
					PdfPTable dmOtherConsumptiontTable = new PdfPTable(dmOtherConsumptionList.get(0).length);
					for (int i = 0; i < dmOtherConsumptionList.size(); i++) {
						String[] strings = dmOtherConsumptionList.get(i);
						for (int j = 0; j < strings.length; j++) {
							PdfPCell cell = new PdfPCell();
							if (i == 0) {
								cell.setBackgroundColor(color);
								cell.setPhrase(new Phrase(strings[j], tableTitleFont));
							} else {
								cell.setBackgroundColor(Color.white);
								cell.setPhrase(new Phrase(strings[j], font));
							}
							cell.setNoWrap(false);
							if (j == strings.length-1) {
								cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							}else {
								cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							}
							dmOtherConsumptiontTable.addCell(cell);
						}
					}
					dmOtherConsumptiontTable.setSpacingAfter(20f);
					document.add(dmOtherConsumptiontTable);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@Override
	@Transactional
	public byte[] getInvoiceReceipt(Long orderNo,String transactionNo,String receiptType) {
		try {
			return customerOrderTransDao.getInvoiceReceipt(orderNo, transactionNo, receiptType);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public ResponseResult sentTransactionEmail(TransactionEmailDto dto, LoginUserDto userDto) {
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
//			File[] pdfFiles = readPdf(dto.getEmailType());
		List<byte[]> attach = new ArrayList<byte[]>();
		String fileName = "ATTACHMENT"+".pdf";
		MessageTemplate mt = null;
		if(!StringUtils.isEmpty(dto.getEmailType())){
			fileName = dto.getEmailType().toUpperCase();
		}
		
		//
		if(EmailType.RECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
			mt = messageTemplateDao.getTemplateByFunctionId(EmailType.RECEIPT.getFunctionId());
		}else if(EmailType.INVOICE.name().equalsIgnoreCase(dto.getEmailType())){
			mt = messageTemplateDao.getTemplateByFunctionId(EmailType.INVOICE.getFunctionId());
		}else if(EmailType.DAYPASSRECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
			mt = messageTemplateDao.getTemplateByFunctionId(EmailType.Purchase_Daypass.getFunctionId());
		}else if(EmailType.GUESTROOMBOOKRECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
			mt = messageTemplateDao.getTemplateByFunctionId(EmailType.GUESTROOMBOOKRECEIPT.getFunctionId());
		}else{
			mt = messageTemplateDao.getTemplateByFunctionId(dto.getEmailType());
		}
		
		
		if(EmailType.RECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
			byte[] receiptAttach = getInvoiceReceipt(null, dto.getTransactionNO().toString(),"serviceplan");
			attach.add(receiptAttach);
			fileName = fileName+"-"+dto.getTransactionNO().toString()+".pdf";
		}else if(EmailType.INVOICE.name().equalsIgnoreCase(dto.getEmailType())){
			byte[] invoiceAttach = getInvoiceReceipt(dto.getOrderNO(), null,"serviceplan");
			attach.add(invoiceAttach);
			fileName = fileName+"-"+dto.getOrderNO()+".pdf";
		}else if(EmailType.DAYPASSRECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
//			mt = messageTemplateDao.getTemplateByFunctionId(EmailType.Purchase_Daypass.getFunctionId());
			CustomerOrderTrans customerOrderTrans = getCustomerOrderTransListByOrderNo(dto.getOrderNO()).get(0);
			byte[] invoiceAttach = getInvoiceReceipt(dto.getOrderNO(), customerOrderTrans.getTransactionNo().toString(),"daypass");
			attach.add(invoiceAttach);
			fileName = "DaypassPurchaseReceipt-"+customerOrderTrans.getTransactionNo().toString()+".pdf";
		}else if(EmailType.GUESTROOMBOOKRECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
			byte[] invoiceAttach = getInvoiceReceipt(null, dto.getTransactionNO().toString(),"guestroombook");
			attach.add(invoiceAttach);
			fileName = "GuestRoomBookingReceipt-"+dto.getTransactionNO().toString()+".pdf";
		}
		
		CustomerProfile cp = null;
		if (null !=dto.getOrderNO()) {
			CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, dto.getOrderNO());
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderHd.getCustomerId());
		}else if (null !=dto.getTransactionNO()) {
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, dto.getTransactionNO());
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderTrans.getCustomerOrderHd().getCustomerId());
		}
		if(StringUtils.isEmpty(dto.getEmailContent())){
			String contentMT= mt.getContentHtml();
			if(cp!=null){
				customerEmailContent.setContent(modifyContent(contentMT, cp, userDto.getUserName()));
			}else{
				customerEmailContent.setContent(contentMT);
			}
		}else{
			customerEmailContent.setContent(dto.getEmailContent());
		}
		
		if(StringUtils.isEmpty(dto.getSendTo())){
			customerEmailContent.setRecipientEmail(cp.getContactEmail());
		}else{
			customerEmailContent.setRecipientEmail(dto.getSendTo());
		}
		
		if(StringUtils.isEmpty(dto.getSubject())){
			customerEmailContent.setSubject(mt.getMessageSubject());
		}else{
			customerEmailContent.setSubject(dto.getSubject());
		}
		
		customerEmailContent.setRecipientCustomerId(cp.getCustomerId().toString());
		customerEmailContent.setSendDate(new Date());
		customerEmailContent.setSenderUserId(userDto.getUserId());
		customerEmailContent.setCopyto(null);
		customerEmailContentDao.save(customerEmailContent);
		
		CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
		customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
		customerEmailAttach.setAttachmentName(fileName);
		customerEmailAttachDao.save(customerEmailAttach);
		
		
		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add(fileName);
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		mailThreadService.sendWithResponse(customerEmailContent, attach, mineTypeList, fileNameList);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	private String modifyContent(String content, CustomerProfile cp, String inscribe) {
		
		content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, cp.getGivenName() + " " + cp.getSurname());
		content = content.replace(Constant.PLACE_HOLDER_FROM_USER, inscribe);
		return content;
	}
	@Override
	@Transactional
	public void manualGenerateStatement(String year,String mon) throws Exception{
		String startDateString = year + "-" + mon + "-" + "01";
		Date startDate = DateConvertUtil.parseString2Date(startDateString, "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
		Date endDate = calendar.getTime();
		String endDateString = DateConvertUtil.date2String(endDate, "yyyy-MM-dd");
		
		String sql = "SELECT  DISTINCT  m.customer_id as customerId  from customer_order_hd coh, member m, customer_order_det cod, pos_service_item_price pos "
				+ " where coh.customer_id = m.customer_id and coh.order_no = cod.order_no AND cod.item_no = pos.item_no "
//				+ " and coh.order_date BETWEEN DATE_SUB(CURDATE()-DAY(CURDATE())+1,INTERVAL 1 MONTH) and LAST_DAY(DATE_SUB(CURRENT_DATE,INTERVAL 1 MONTH)) "
				+ " AND ( LEFT (cod.item_no, 3) != 'SRV' AND LEFT (cod.item_no, 5) != 'RENEW' OR (LEFT (cod.item_no, 3) = 'SRV' AND pos.item_catagory = 'DP') ) "
				+ " and coh.order_date BETWEEN '"+startDateString+"' and '"+ endDateString+"' "
				+ " and (m.member_type = 'IPM' OR m.member_type = 'CPM') "
				+ " UNION SELECT customer_id as customerId  from member me where me.`status` = 'ACT' and (me.member_type = 'IPM' OR me.member_type = 'CPM') ORDER BY customerId";
		List<MemberDto> memberDtos = memberDao.getDtoBySql(sql, null, MemberDto.class);
		calculateClosingBalance(memberDtos,startDateString,endDateString);
		for (MemberDto memberDto:memberDtos) {
			Long customerId = memberDto.getCustomerId();
			CustomerStatementBaseInfoDto statementInfo = getCustomerStatementBaseInfoDto(customerId,startDateString,endDateString);
			
			Member member = memberDao.get(Member.class, customerId);
			
			Date date = new Date();
//			Calendar calendar = Calendar.getInstance();
			
			calendar.setTime(date);
			int month = calendar.get(Calendar.MONTH);
			calendar.add(Calendar.MONTH, -2);
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
			Date cutoffDate = calendar.getTime();  
			if (null != endDateString) {
				calendar.setTime(endDate);
				calendar.add(Calendar.MONTH, -1);
				calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				cutoffDate = calendar.getTime();
			}
			String strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd")+" 23:59:59";
			MemberCashvalueBalHistory lastMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCutoffDate)});
			BigDecimal openingBalance =null;
			if (null == lastMemberCashvalueBalHistory) {
				Date joinDate = member.getFirstJoinDate();
				calendar.setTime(date);
				calendar.add(Calendar.MONTH, -1);
				String currentMonthString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM");
				if (null != startDateString) {
					currentMonthString = startDateString.substring(0, 6);
				}
				if (DateConvertUtil.date2String(joinDate, "yyyy-MM").equals(currentMonthString)) {
					//if the member's join date is last month, it's opening balance is it's inital cash value.
					MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
					if (null == memberCashvalue) {
						openingBalance = new BigDecimal(0);
					}else {
						openingBalance = memberCashvalue.getInitialValue();
					}
				}else {
					openingBalance = new BigDecimal(0);
				}
			}else {
				openingBalance = lastMemberCashvalueBalHistory.getRecalBalance();
			}
			
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -1);
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
			Date currCutoffDate = calendar.getTime();  
			String strCurrCutoffDate = DateConvertUtil.date2String(currCutoffDate, "yyyy-MM-dd")+" 23:59:59";
			if (null != endDateString) {
				strCurrCutoffDate = endDateString + " 23:59:59";
			}
			MemberCashvalueBalHistory currMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(MemberCashvalueBalHistory.class, new String[]{"customerId","cutoffDate"}, new Serializable[]{customerId,DateConvertUtil.getDateFromStr(strCurrCutoffDate)});
			
			BigDecimal closingBalance = null;
			if (null != currMemberCashvalueBalHistory) {
				closingBalance = currMemberCashvalueBalHistory.getRecalBalance();
			}
			
			String basePath = appProps.getProperty("pdf.server.report");// /opt/file-server/report/statement  or  D:\\fileServer\\report\\statement
			
			File file = new File(basePath);
			if (!file.exists()) {
				file.mkdirs();
			}
//			calendar.setTime(date);
//			calendar.add(Calendar.MONTH, -1);
//			Date generateMonthDate = calendar.getTime();  
//			String generateMonth = DateConvertUtil.date2String(generateMonthDate, "yyyyMM");
			String filePath = basePath + "/Patron Account Statement_"+customerId+"_"+year+mon+".pdf";
			OutputStream out = new FileOutputStream(filePath);
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
			Document document = new Document(PageSize.A4, 10, 10, 110, 50);
//			document.setMargins(10f, 10f, 10f, 10f);
			PdfWriter writer = PdfWriter.getInstance(document, out);
			
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD,Color.black);
			Color color = new Color(140, 0, 30);
			Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.white);
			
			List<String[]> allCashvalueList =  new ArrayList<String[]>();
			allCashvalueList.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type", "Dependent Patron's Name","Amount(HK$)","Cash Value Balance(HK$)"});
			allCashvalueList.add(new String[] {null,null,"Opening Balance",null,null,null,getFormatAmount(openingBalance)});
			
			String cashvalueSql = "SELECT DISTINCT trans.transaction_timestamp as transactionDate,trans.transaction_no as invoiceNo,(CASE hd.customer_id WHEN "+customerId+" THEN '-' ELSE CONCAT(cp.given_name,' ',cp.surname) END) as memberName,"
					+ " pos.description as description,(CASE trans.payment_method_code WHEN 'VISA' THEN 'VISA'  WHEN 'MASTER' THEN 'MASTER' WHEN 'CASH' THEN 'CASH' WHEN 'BT' THEN 'Bank Transfer' "
					+ " WHEN 'CHEQUE' THEN 'Cheque' WHEN 'CV' THEN 'Cash Value' WHEN 'VAC' THEN 'Virtual Account Transfer' ELSE 'Other' END) as paymentType,trans.paid_amount as amount,det.item_no as itemNo "
					+ " from customer_order_trans trans, customer_order_hd hd,customer_order_det det,member m,customer_profile cp,pos_service_item_price pos"
					+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no and det.item_no = pos.item_no "
					+ " and ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = 'CV') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU') "//The condition of spending or cash value refund
					+ " or (trans.`status` = 'SUC' and det.item_no = 'CVT0000001') OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001')) "//The condition of topup or Service refund
					+ " and hd.customer_id = m.customer_id and cp.customer_id = m.customer_id "
					+ " and (DATE(trans.transaction_timestamp) BETWEEN  '"+startDateString+"' and '"+endDateString+"')"
					+ " and (m.customer_id = ? or m.superior_member_id = ?) ORDER BY trans.transaction_timestamp";
			
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(customerId);
			param.add(customerId);
			List<PatronAccountStatementDto> patronAccountStatementDtos = customerOrderTransDao.getDtoBySql(cashvalueSql, param, PatronAccountStatementDto.class);
			
			BigDecimal topupTotal = new BigDecimal(0);
			BigDecimal spendingTotal = new BigDecimal(0);
			BigDecimal cashBalance	 = openingBalance;
//			for (PatronAccountStatementDto dto:patronAccountStatementDtos) {
			for (int i =0;i<patronAccountStatementDtos.size();i++) {
				PatronAccountStatementDto dto = patronAccountStatementDtos.get(i);
				if (null == statementInfo.getCreditLimit() || statementInfo.getCreditLimit().compareTo(BigDecimal.ZERO) >0) {
					dto.setPaymentType("City Ledger");
				}
//				if (dto.getItemNo().indexOf("PMS") >= 0 ) {//所有的消费的description已经修改的有意义了，因此这个地方不需要再转。
//					dto.setDescription("Accommodation");
//				}
//				if (Constant.TOPUP_ITEM_NO.equalsIgnoreCase(dto.getItemNo())) {
//					dto.setDescription("Top up for cash value");
//				}
				if (!Constant.TOPUP_ITEM_NO.equalsIgnoreCase(dto.getItemNo()) && !Constant.REFUND_SERVICE_ITEM_NO.equalsIgnoreCase(dto.getItemNo())) {
					if (statementInfo.getMemberName().equals(dto.getMemberName()) || "-".equals(dto.getMemberName())) {
						spendingTotal = spendingTotal.add(dto.getAmount());
					}
					cashBalance = cashBalance.subtract(dto.getAmount());
					dto.setCashvalueBalance(cashBalance);
					dto.setAmount(dto.getAmount().negate().setScale(2, BigDecimal.ROUND_HALF_UP));
				}else {
					if (statementInfo.getMemberName().equals(dto.getMemberName()) || "-".equals(dto.getMemberName())) {
						topupTotal = topupTotal.add(dto.getAmount());
					}
					cashBalance = cashBalance.add(dto.getAmount());
					dto.setCashvalueBalance(cashBalance);
				}
				
				allCashvalueList.add(new String[]{DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),dto.getInvoiceNo().toString(),dto.getDescription(),
						dto.getPaymentType(),dto.getMemberName(),getFormatAmount(dto.getAmount()),getFormatAmount(dto.getCashvalueBalance())});
				if (i==patronAccountStatementDtos.size()-1) {
					closingBalance = dto.getCashvalueBalance();//In case the data has been updated, so set the last PatronAccountStatementDto's CashvalueBalance for the closingBalance.
				}
			}
			allCashvalueList.add(new String[] {null,null,"Closing Balance",null,null,null,getFormatAmount(closingBalance)});
			statementInfo.setCashvalue(closingBalance);//In case the data is not correct because of the report is not been generated at first day of current month.
			
			addStatementBaseInfo(writer,document,statementInfo);
			
			PdfPTable allCashvaluePdfPTable = new PdfPTable(allCashvalueList.get(0).length);
			allCashvaluePdfPTable.setTotalWidth(new float[]{20f,15f,20f,20f,20f,20f,20f});
			Font font = new Font(Font.TIMES_ROMAN, 10);
			for (int i = 0; i < allCashvalueList.size(); i++) {
				String[] strings = allCashvalueList.get(i);
				for (int j = 0; j < strings.length; j++) {
					PdfPCell cell = new PdfPCell();
					if (i == 0 ) {
						cell.setBackgroundColor(color);
						cell.setPhrase(new Phrase(strings[j], tableTitleFont));
					} else if (i == 1 || i == allCashvalueList.size()-1) {
						cell.setBackgroundColor(Color.white);
						cell.setPhrase(new Phrase(strings[j], firstColumnFont));
					}else {
						cell.setBackgroundColor(Color.white);
						cell.setPhrase(new Phrase(strings[j], font));
					}
					cell.setNoWrap(false);
					if (j == strings.length -1 || j == strings.length -2) {
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}else {
						cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					}
					allCashvaluePdfPTable.addCell(cell);
				}
			}
			document.add(allCashvaluePdfPTable);
			addBalanceHistoryTable(document,closingBalance,customerId,startDate);
			
			Paragraph otherDetail = getParagraph("For Other Consumption Detail ",firstColumnFont,10f,0f,57f);
			document.add(otherDetail);
			
			
			Paragraph pmTitle = getParagraph("Primary Patron - "+statementInfo.getMemberName(),firstColumnFont,10f,5f,57f);
			document.add(pmTitle);
			
//			addOtherConsumptionTable(document,customerId,startDate,endDate);//Add other consumption table include primary member and dependent members.
			addOtherConsumptionTable(document,customerId,startDateString,endDateString);//Add other consumption table include primary member and dependent members.
			
			Paragraph summaryTitle = getParagraph("Summary",firstColumnFont,0f,10f,57f);
			document.add(summaryTitle);
			
			addSummaryTable(document,customerId,startDateString,endDateString);//Add summary table include primary member and dependent members.
//			addSummaryTable(document,customerId);//Add summary table include primary member and dependent members.
			
			addFootContent(document);//Add foot content.
			
			document.close();
//			InputStream inputStream = new FileInputStream(filePath);
			
			if (null!=out) {
				out.flush();
				out.close();
			}
		
		}
	}
	
	@Transactional
	public ResponseResult getMonthlyEnrollment(ListPage<CustomerOrderTrans> page, String year, String month) {
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getMonthlyEnrollment(page, year, month);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	@Transactional
	public byte[] getMonthlyEnrollmentAttachment(String year, String month,String fileType,String sortBy,String isAscending) {
		try {
			return customerOrderTransDao.getMonthlyEnrollmentAttachment(year, month,fileType,sortBy,isAscending);
		} catch (JRException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public ResponseResult getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage<?> page,
			String selectedDate, String facilityType) {

		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getDailyMonthlyPrivateCoachingRevenue(
				timePeriodType, page, selectedDate, facilityType);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Override
	@Transactional
	public byte[] getDailyMonthlyPrivateCoachingRevenueAttach(String timePeriodType, String selectedDate,
			String fileType, String sortBy, String isAscending, String facilityType) {

		try {
			return customerOrderTransDao.getDailyMonthlyPrivateCoachingRevenueAttachment(timePeriodType, selectedDate,
					fileType, sortBy, isAscending, facilityType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
