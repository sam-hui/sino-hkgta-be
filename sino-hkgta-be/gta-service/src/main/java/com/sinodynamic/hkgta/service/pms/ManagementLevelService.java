package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.ManagementLevelDto;

public interface ManagementLevelService {

	public List<ManagementLevelDto> getAllManagementLevel();
}
