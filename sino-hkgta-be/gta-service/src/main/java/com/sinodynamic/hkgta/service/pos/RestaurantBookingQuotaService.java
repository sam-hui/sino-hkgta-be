package com.sinodynamic.hkgta.service.pos;

import java.util.List;

import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RestaurantBookingQuotaService extends IServiceBase<RestaurantBookingQuota> {
	
	public List<RestaurantBookingQuota> getRestaurantBookingQuotaList(String restaurantId,Integer bookingTime);
	
}
