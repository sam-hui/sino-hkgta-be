package com.sinodynamic.hkgta.service.fms;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.sinodynamic.hkgta.entity.crm.MsgPushHistory;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class MsgPushHistoryServiceImpl extends ServiceBase<MsgPushHistory> implements MsgPushHistoryService {

    @Override
    public String getSqlForPullMessages(String userId, String deviceArn, String application) {
	
	if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(deviceArn) || StringUtils.isEmpty(application)) return null;
	
	String sql = "select "
		+ "mph.subject as subject,"
		+ "mph.content as content,"
		+ "mph.create_date as createDate "
		+ "from msg_push_history mph "
		+ "left join user_device ud on mph.recipient_device_id = ud.sys_id "
		+ "where ud.user_id = '" + userId
		+ "' and ud.arn_endpoint = '" + deviceArn
		+ "' and ud.arn_application = '" + application + "'";
	return sql;
    }
    
    
}
