package com.sinodynamic.hkgta.service.crm.sales;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.UserActivateQuestDao;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.staff.QuestionAndAnswer;
import com.sinodynamic.hkgta.dto.staff.SecurityQuestionDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuest;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuestPK;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class MemberServiceImpl extends ServiceBase<Member> implements MemberService {

	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private UserActivateQuestDao userActivateQuestDao;
	
	@Transactional
	public void addMember(Member m) throws Exception {
		memberDao.addMember(m);
	}

	@Transactional
	public void updateMember(Member m) throws Exception {
		memberDao.updateMemberer(m);
		
	}

	public void deleteMember(Member m) throws Exception {
		memberDao.addMember(m);
		
	}
	
	@Transactional
	public void getMemberList(ListPage<Member> page, Member m) throws Exception {
		String countHql = " select count(*) from Member ";
		String hql = " from Member m ";
		memberDao.getMemberList(page, countHql, hql, null);
		
	}

	@Transactional
	public Member getMemberById(Long id) throws Exception {
		return memberDao.getMemberById(id);
	}

	@Override
	@Transactional
	public ResponseResult getDependentMembers(Long customerID,ListPage page) {
		ListPage<Member> listPage = memberDao.getDependentMembers(page, customerID);
		List<Object> dependentMemberDtos = listPage.getDtoList();
		
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(dependentMemberDtos);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult checkDMCreditLimit(Long customerId) {
		try {
			Member member = memberDao.getMemberById(customerId);
			if(member ==null){
				return new ResponseResult("-1","Member is not exist!");
			}
			if(MemberType.IDM.name().equals(member.getMemberType()) || MemberType.CDM.name().equals(member.getMemberType())){
				MemberDto memberDto = memberDao.checkDMCreditLimit(member.getSuperiorMemberId());
				return new ResponseResult("0","",memberDto); 
			}
			return new ResponseResult("-1","No record Found!");
		} catch (Exception e) {
			logger.error("checkDMCreditLimit method ", e);
			return new ResponseResult("-1","System Exception!");
		}
	}
	
	@Override
	@Transactional
	public ResponseResult checkTransactionLimit(Long customerId,BigDecimal spending){
		try{
			Member member = memberDao.getMemberById(customerId);
			if(member ==null){
				responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
				return responseResult;
			}
			
			MemberDto memberDto = memberDao.getMemberPurchaseLimit(customerId);
			if(null != memberDto){
				if(memberDto.getExpiryDate()== null || memberDto.getExpiryDate().after(new Date())){
					if(memberDto.getRemainSum().compareTo(spending)!=-1){
						if(MemberType.IDM.name().equals(member.getMemberType()) || MemberType.CDM.name().equals(member.getMemberType())){
							memberDto = memberDao.getMemberTransactionLimit(customerId);
							if(null != memberDto){
								if(null != memberDto.getCreditLimit() || "".equals(memberDto.getCreditLimit())){
									if(memberDto.getCreditLimit().compareTo(spending)!=-1){
										responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_ALLOWED);
										return responseResult;
									}else{	
										responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_NOT_ALLOWED);
										return responseResult;
									}
								}
							}else{
								responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_ALLOWED);
								return responseResult;
							}
						}else if(MemberType.IPM.name().equals(member.getMemberType()) || MemberType.CPM.name().equals(member.getMemberType())){
							responseResult.initResult(GTAError.MemberShipError.BALANCE_AVAILABLE);
							return responseResult;
						}
					}else{
						responseResult.initResult(GTAError.MemberShipError.BALANCE_NOT_AVAILBALE);
						return responseResult;
					}
				}else if(memberDto.getExpiryDate().before(new Date())){
					if(memberDto.getAvailableBalance().compareTo(spending)!=-1){
						responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_ALLOWED);
						return responseResult;
						
					}else{
						responseResult.initResult(GTAError.MemberShipError.BALANCE_NOT_AVAILBALE);
						return responseResult;
					}
				}
			}
				responseResult.initResult(GTAError.MemberShipError.NO_RECORD_FOUND);
				return responseResult;
			
	}catch(Exception e) {
		logger.error("checkDMTransactionLimit method ", e);
		responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
		return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult setSecurityQuestion(SecurityQuestionDto questionDto) throws Exception
	{
		List<SysCode> questionList = sysCodeDao.getSysCodeByCategory(Constant.CATEGORY_4_SECURITY_QUESTION);

		List<QuestionAndAnswer> answers = questionDto.getAnswers();
		for(QuestionAndAnswer answer : answers)
		{
			SysCode question = new SysCode(answer.getCode(), Constant.CATEGORY_4_SECURITY_QUESTION);
			String q = questionList.get(questionList.indexOf(question)).getCodeDisplay();
			String a = answer.getAnswer();
			
			UserActivateQuestPK id = new UserActivateQuestPK();
			id.setUserId(questionDto.getUserId());
			id.setQuestionNo(answer.getQuestionNo());
			
			UserActivateQuest userQuestion = userActivateQuestDao.getById(id);
			
			if (null == userQuestion)
			{
				userQuestion = new UserActivateQuest();
				userQuestion.initUserActivateQuest(questionDto.getUserId(), answer.getQuestionNo(), q, a, true);
				userActivateQuestDao.save(userQuestion);
			}
			else
			{
				userQuestion.initUserActivateQuest(questionDto.getUserId(), answer.getQuestionNo(), q, a, false);
			}
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public List<Member> getAllMembers() throws Exception {
		
		List<Member> members = memberDao.getAllMembers();
		
		return members;
	}

	@Override
	@Transactional
	public String getMemberTypeByCustomerId(Long customerID) {
		Member member = memberDao.get(Member.class, customerID);
		return member.getMemberType();
	}

	@Override
	@Transactional
	public Member getMemberByAcademyNo(String academyNo)
	{
		return memberDao.getMemberByAcademyNo(academyNo);
	}
}
