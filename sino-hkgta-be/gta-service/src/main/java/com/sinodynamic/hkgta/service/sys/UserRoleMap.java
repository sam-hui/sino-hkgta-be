package com.sinodynamic.hkgta.service.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.sys.MenuDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;

@Component
@Scope("singleton")
public class UserRoleMap
{
	private static UserRoleMap instance = null;

	private Map<String,  List<MenuDto>> roleMenu = new HashMap<String, List<MenuDto>>();
	
	private Map<String, List<UserRoleDto>> userRole = new HashMap<String,List<UserRoleDto>>();

	public static synchronized UserRoleMap getInstance()
	{
		if (instance == null)
		{
			instance = new UserRoleMap();
		}
		return instance;
	}

	public Map<String, List<MenuDto>> getRoleMenu()
	{
		return roleMenu;
	}

	public void setRoleMenu(Map<String, List<MenuDto>> roleMenu)
	{
		this.roleMenu = roleMenu;
	}

	public List<UserRoleDto> getUserRole(String userId)
	{
		return userRole.get(userId);
	}

	public void setUserRole(String userId, List<UserRoleDto> userRoles)
	{
		userRole.put(userId, userRoles);
	}
	
}


