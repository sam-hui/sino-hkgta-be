
package com.sinodynamic.hkgta.service.crm.backoffice.admin;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.NotificationCenterDao;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.notification.SMSService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@SuppressWarnings("rawtypes")
@Service
public class NotificationCenterServiceImpl extends ServiceBase implements NotificationCenterService {
	@Autowired
	private NotificationCenterDao notificationDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	SMSService smsService;

	public Logger logger = Logger.getLogger(NotificationCenterServiceImpl.class);
	
	@Override
	@Transactional
	public ResponseResult getNotificationMemberList(NotificationDto notificationDto) {
		try {
			List<NotificationMemberDto> dtoList = notificationDao.getNotificationMemberList(notificationDto);
			responseResult.initResult(GTAError.Success.SUCCESS,dtoList);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.Error);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public void sendNotificationByEmail(NotificationDto notificationDto,
			List<NotificationMemberDto> memList, Map<String, String> map) {
        String[] paths = notificationDto.getAttachPaths();
        String basePath = appProps.getProperty("notification.server.material");
        if(paths !=null && paths.length >0){
	        File dir = new File(basePath);
	        if(!dir.exists())dir.mkdir();
        }
		if(map !=null && map.size()>0){
			for(String id : map.keySet()){
				CustomerEmailContent content = customerEmailContentDao.get(CustomerEmailContent.class, id);
				if(content.getRecipientEmail() == null || content.getRecipientEmail().trim().length() == 0){
			        	continue;
			    }
		        logger.info("Send Email Task for customer id ( " + content.getRecipientCustomerId() + ") start ...");
		        if(notificationDto.getEnableHtml()){
		        	mailThreadService.send(content, this.getFileBytes(basePath, paths), this.getMineTypes(paths), this.getFileNames(paths));
		        }else{
		        	mailThreadService.send(content);
		        }
		        logger.info("Send Email Task for customer id ( " + content.getRecipientCustomerId() + ") end ...");
			}
		}
	}
	
	@Override
	@Transactional
	public ResponseResult sendNotificationBySMS(NotificationDto notificationDto) {
		try {
			List<NotificationMemberDto> memList = notificationDao.getNotificationMemberList(notificationDto);
			List<String> phonenumbers = new ArrayList<String>();
			if(memList!= null && memList.size()>0){
				for(NotificationMemberDto dto : memList){
					if(dto.getPhoneMobile() != null)
						phonenumbers.add(dto.getPhoneMobile());
				}
			}
			smsService.sendSMS(phonenumbers, notificationDto.getContent(), new Date());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.Error);
		}
		return responseResult;
	}
	
	@Override
	@Transactional
	public Map<String, String> saveEmailRecord(
			NotificationDto notificationDto, List<NotificationMemberDto> memList)throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		if(memList!=null && memList.size()>0){
			for(NotificationMemberDto profile : memList){
				CustomerEmailContent ca = new CustomerEmailContent();
				ca.setSubject(notificationDto.getSubject());
		    	ca.setContent(notificationDto.getContent());
		    	ca.setNoticeType(notificationDto.getPromotionType().trim().toUpperCase());
		    	ca.setRecipientCustomerId(profile.getCustomerId()+"");
		    	ca.setSendDate(new Date());
		    	ca.setSenderUserId(notificationDto.getSenderUserId());
		    	ca.setRecipientEmail(profile.getEmail());
		    	ca.setStatus(EmailStatus.PND.name());
		    	String contentId = (String)customerEmailContentDao.addCustomerEmail(ca);
		    	
		    	String[] attachs = notificationDto.getAttachPaths();
		    	if(attachs != null && attachs.length >0){
		    		for(int i=0; i<attachs.length; i++){
		        		CustomerEmailAttach attach = new CustomerEmailAttach();
		        		attach.setAttachmentName(attachs[i]);
		        		attach.setAttachmentPath(appProps.getProperty("notification.server.material"));
		        		attach.setEmailSendId(ca.getSendId()+"");
		        		customerEmailAttachDao.save(attach);
		    		}
		    	}
		    	map.put(contentId,ca.getRecipientCustomerId());
			}
		}
		return map;
    }
	    
	    /**
	     * 
	     * @param basePath
	     * @param paths
	     * @return file byte list
	     * @throws Exception
	     */
	    public List<byte[]> getFileBytes(String basePath, String[] paths){
	    	List<byte[]> fileBytes = new ArrayList<byte[]>();
	    	   try {
				if(paths !=null && paths.length >0){
					   for(int i=0; i<paths.length; i++){
						   File item = new File(basePath + File.separator + paths[i]);
						   if(item.exists()){
							   InputStream inputStream = new FileInputStream(item);
							   fileBytes.add(IOUtils.toByteArray(inputStream));
						   }
					   }
				   }
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	return fileBytes;
	    }
	    
	    /**
	     * 
	     * @param paths
	     * @return file name list
	     */
	    public List<String> getFileNames(String[] paths){
	    	List<String> fileNames = new ArrayList<String>();
	    	if(paths !=null && paths.length >0){
    		   for(int i=0; i<paths.length; i++){
    			   if(paths[i] != null && paths[i].length()>0){
    				   fileNames.add(paths[i].substring(paths[i].indexOf("@")+1));
    			   }
    		   }
	    	}
	    	return fileNames;
	    }
	    /**
	     * 
	     * @param paths
	     * @return mine type list
	     */
	    public List<String> getMineTypes(String[] paths){
	    	List<String> mineTypes = new ArrayList<String>();
	    	if(paths !=null && paths.length >0){
    		   for(int i=0; i<paths.length; i++){
    			   if(paths[i] != null && paths[i].length()>0){
    				   	String fileSuffix = paths[i].substring(paths[i].lastIndexOf("."));
    				   	if(fileSuffix.endsWith("pdf")){
    						mineTypes.add("application/pdf");
    					}else if(fileSuffix.endsWith("jpeg")){
    						mineTypes.add("image/jpeg");
    					}else if(fileSuffix.endsWith("gif")){
    						mineTypes.add("image/gif");
    					}else if (fileSuffix.endsWith("png")){
    						mineTypes.add("image/png");
    					}else if(fileSuffix.endsWith("gif")){
    						mineTypes.add("image/gif");
    					}else if(fileSuffix.endsWith("html")){
    						mineTypes.add("text/html");
    					}else{
    						mineTypes.add("application/octet-stream");  
    					}
    			   }
    		   }
	    	}
	    	return mineTypes;
	    }
}


