package com.sinodynamic.hkgta.service.crm.membercash;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;

import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.SftpByJsch;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;

@Service
public class VirtualAccountTaskServiceImpl  extends ServiceBase implements VirtualAccountTaskService {
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	
	@Autowired 
	private CashvalueTopupHistoryDao cashvalueTopupHistoryDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private GtaPublicHolidayDao gtaPublicHolidayDao;
	
	@Resource(name = "ddxProperties")
	protected Properties ddxProps;
	
	private Logger ddxLogger = LoggerFactory.getLogger("ddx");
	
	private MessageTemplate template;
	
	private String ftpHost;
	private String port;
	private String ftpUserName;
	private String ftpPassword;
	private String timeout;

	private String uploadPath;
	private String receivePath;
	private String receiveBackPath;
	private String downLoadPath;//store all the remote files in local 
	private String downLoadBackPath;//store the success processed files in local
	
	
	@PostConstruct
	public void init() {
		ftpHost = ddxProps.getProperty(Constant.SFTP_HOST, null);
		port = ddxProps.getProperty(Constant.SFTP_PORT, null);
		ftpUserName = ddxProps.getProperty(Constant.SFTP_USERNAME, null);
		ftpPassword = ddxProps.getProperty(Constant.SFTP_PASSWORD, null);
		timeout = ddxProps.getProperty(Constant.SFTP_TIMEOUT, null);

		uploadPath = ddxProps.getProperty(Constant.SFTP_SNED_PATH, null);//remote upload dir
		receivePath = ddxProps.getProperty(Constant.SFTP_RECEIVE_PATH, null);//remote downloand dir
		receiveBackPath = ddxProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, null);//remote back dir, after processed move to this dir
		downLoadPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_PATH, null);//store all the remote files in local 
		downLoadBackPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_BACK_PATH, null);//store the success processed files in local

	}
	@Override
	@Transactional
	public void topupCashValueByVirtualAccInBatch(List<CashvalueTopupHistory> list) {

		for(CashvalueTopupHistory topUp: list) {
			String accNo = topUp.getAccNo();
			BigDecimal amount = topUp.getAmount();
			
			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				ddxLogger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				continue;
			}
			Assert.notNull(mpa);
			
			
			Long cusomerId = mpa.getCustomerId();
			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				ddxLogger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
			}
			Assert.notNull(memberCashValue);

			topUp.setCustomerId(cusomerId);
			cashvalueTopupHistoryDao.addCashvalueTopupHistory(topUp);

				
			BigDecimal oldBalance = memberCashValue.getAvailableBalance();
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = amount;
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance =amount.multiply(exchgFactor);
			}
			BigDecimal newBalance = oldBalance.add(addBalance);	
			memberCashValue.setAvailableBalance(newBalance.setScale(2, BigDecimal.ROUND_HALF_UP));
			memberCashValueDao.updateMemberCashValue(memberCashValue);
			
			// CUSTOMER ORDER Header
			CustomerOrderHd  customerOrderHd = new CustomerOrderHd();
			Date currentDate = new Date();
			customerOrderHd.setCustomerId(cusomerId);
			customerOrderHd.setOrderTotalAmount(addBalance);
			customerOrderHd.setOrderRemark("");
			customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderHd.setUpdateDate(currentDate);
			customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
			
			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
			customerOrderDet.setItemRemark("");
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(addBalance);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setUpdateDate(currentDate);
			
			// CUSTOMER ORDER TRANS
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			customerOrderTrans.setCustomerOrderHd(customerOrderHd);
			customerOrderTrans.setTransactionTimestamp(currentDate);
			customerOrderTrans.setPaidAmount(addBalance);
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VAC.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			
			customerOrderDetDao.saveOrderDet(customerOrderDet);
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
			
			sendTopUpEmail(topUp, memberCashValue);
		}
		
	}
	
	@Transactional
	public void sendTopUpEmail(CashvalueTopupHistory topUp, MemberCashvalue memberCashvalue){
		if (template == null) {
			template = messageTemplateDao.getTemplateByFunctionId("topup");
		}
		
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, topUp.getCustomerId());
		if (customerProfile != null) {
			
			String to = customerProfile.getContactEmail();
		
			String subject = template.getMessageSubject();
			String username = customerProfile.getSalutation()+" "+customerProfile.getGivenName()+" "+customerProfile.getSurname();
			String content = template.getContentHtml();
			content = content.replaceAll(MessageTemplateParams.UserName.getParam(), username);
			content = content.replaceAll(MessageTemplateParams.TopupMethod.getParam(), topUp.getTopupMethod());
			
			content = content.replaceAll(MessageTemplateParams.CurrentBalance.getParam(), "HK\\$"+memberCashvalue.getAvailableBalance());
			
			CustomerEmailContent customerEmailContent = new CustomerEmailContent();

			customerEmailContent.setContent(content);
			customerEmailContent.setRecipientCustomerId(topUp.getCustomerId().toString());
			customerEmailContent.setRecipientEmail(to);
			customerEmailContent.setSendDate(new Date());
			//customerEmailContent.setSenderUserId();
			customerEmailContent.setSubject(subject);
	
			customerEmailContentDao.save(customerEmailContent);

			mailThreadService.sendWithResponse(customerEmailContent, null, null, null);
		}
	}

	private boolean findByCustomerIdAndAmount(List<CashvalueTopupHistory> rejList,CashvalueTopupHistory pndTran) {
		if(null ==rejList || rejList.isEmpty()) {
			return false;
		}
		boolean flag = false;
		for(CashvalueTopupHistory rejTran:rejList) {
			if(rejTran.getCustomerId().equals(pndTran.getCustomerId()) && rejTran.getAmount().equals(pndTran.getAmount())) {
				return true;
			}
		}
		return flag;
	}
	
	@Override
	public void processRejChTransactions(List<CashvalueTopupHistory> rejList) {
		
		String hql = "from CashvalueTopupHistory where topupDate <= ? and status =?";
		List<Serializable> param = new ArrayList();
		Date twoDaysAgoWorkingDate = getTwoDaysAgoWorkingDayFromDB(new Date());
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		param.add(twoDaysAgoWorkingDate);
		param.add("PND");
		List<CashvalueTopupHistory> pendingList = cashvalueTopupHistoryDao.getByHql(hql, param);
		
		
		
		for(CashvalueTopupHistory rejTran: rejList) {
			String accNo = rejTran.getAccNo();
			
			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				ddxLogger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				continue;
			}
			Assert.notNull(mpa);
			Long cusomerId = mpa.getCustomerId();
			rejTran.setCustomerId(cusomerId);
			
			//update the related PND transaction history record
			CashvalueTopupHistory topUpRec = cashvalueTopupHistoryDao.getUniqueByCols(CashvalueTopupHistory.class, new String[]{"customerId","amount","status"}, new Serializable[]{cusomerId,rejTran.getAmount(),"PND"});
			if(null != topUpRec) {
				topUpRec.setStatus(Constant.Status.REJ.name());
				cashvalueTopupHistoryDao.update(topUpRec);
				
				rejTran.setCashvalueTopupHistory(topUpRec);
				cashvalueTopupHistoryDao.addCashvalueTopupHistory(rejTran);
			}
			
			
			
			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				ddxLogger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
			}
			Assert.notNull(memberCashValue);
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = rejTran.getAmount();
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance =rejTran.getAmount().multiply(exchgFactor);
			}
			CustomerOrderTrans orderTran = customerOrderTransDao.getUniqueByCols(CustomerOrderTrans.class, new String[]{"paidAmount","status"}, new Serializable[]{addBalance,"PND"});
			if(null != orderTran) {
				orderTran.setStatus(Constant.Status.REJ.name());
				CustomerOrderHd orderHd = orderTran.getCustomerOrderHd();
				orderHd.setOrderStatus(Constant.Status.REJ.toString());
				orderHd.setUpdateDate(new Date());
				orderHd.setUpdateBy("[System job]");
				customerOrderTransDao.update(orderTran);
			}
			
			
		}
		
		for (CashvalueTopupHistory pndTran :pendingList) {
			boolean flag = findByCustomerIdAndAmount(rejList,pndTran);
			if(!flag) {
				//no RCQ 
				pndTran.setStatus(Constant.Status.SUC.toString());
				
				MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(pndTran.getAccNo());
				if (mpa == null) {
					ddxLogger.error("No MemberPaymentAcc with accountNo = " + pndTran.getAccNo() + " can be found!");
					continue;
				}
				Assert.notNull(mpa);
				Long cusomerId = mpa.getCustomerId();
				MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
				if (memberCashValue == null) {
					ddxLogger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
				}
				Assert.notNull(memberCashValue);
				BigDecimal oldBalance = memberCashValue.getAvailableBalance();
				BigDecimal exchgFactor = memberCashValue.getExchgFactor();
				BigDecimal addBalance = pndTran.getAmount();
				if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
					addBalance =pndTran.getAmount().multiply(exchgFactor);
				}
				BigDecimal newBalance = oldBalance.add(addBalance);	
				memberCashValue.setAvailableBalance(newBalance.setScale(2, BigDecimal.ROUND_HALF_UP));
				memberCashValueDao.updateMemberCashValue(memberCashValue);
				
				CustomerOrderTrans orderTran = customerOrderTransDao.getUniqueByCols(CustomerOrderTrans.class, new String[]{"paidAmount","status"}, new Serializable[]{addBalance,"PND"});
				if(null != orderTran) {
					orderTran.setStatus(Constant.Status.SUC.name());
					orderTran.setUpdateDate(new Timestamp(new Date().getTime()));
					orderTran.setUpdateBy("[System job]");
					
					CustomerOrderHd orderHd = orderTran.getCustomerOrderHd();
					orderHd.setOrderStatus(Constant.Status.CMP.toString());
					orderHd.setUpdateDate(new Date());
					orderHd.setUpdateBy("[System job]");
					customerOrderTransDao.update(orderTran);
					sendTopUpEmail(pndTran, memberCashValue);
				}
				
			}
		}
		
	}

	@Override
	public void processPndChTransactions(List<CashvalueTopupHistory> pndList) {
		for(CashvalueTopupHistory pndTran: pndList) {
			String accNo = pndTran.getAccNo();
			BigDecimal amount = pndTran.getAmount();
			
			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				ddxLogger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				continue;
			}
			Assert.notNull(mpa);
			
			
			Long cusomerId = mpa.getCustomerId();
			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				ddxLogger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
			}
			Assert.notNull(memberCashValue);

			pndTran.setCustomerId(cusomerId);
			cashvalueTopupHistoryDao.addCashvalueTopupHistory(pndTran);

				
			BigDecimal oldBalance = memberCashValue.getAvailableBalance();
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = amount;
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance =amount.multiply(exchgFactor);
			}
			
			
			// CUSTOMER ORDER Header
			CustomerOrderHd  customerOrderHd = new CustomerOrderHd();
			Date currentDate = new Date();
			customerOrderHd.setCustomerId(cusomerId);
			customerOrderHd.setOrderTotalAmount(addBalance);
			customerOrderHd.setOrderStatus(Constant.Status.OPN.toString());
			customerOrderHd.setOrderRemark("");
			customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderHd.setUpdateDate(currentDate);
			
			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
			customerOrderDet.setItemRemark("");
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(addBalance);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setUpdateDate(currentDate);
			
			// CUSTOMER ORDER TRANS
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			customerOrderTrans.setCustomerOrderHd(customerOrderHd);
			customerOrderTrans.setTransactionTimestamp(currentDate);
			customerOrderTrans.setPaidAmount(addBalance);
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VAC.name());
			customerOrderTrans.setStatus(Constant.Status.PND.name());
			
			customerOrderDetDao.saveOrderDet(customerOrderDet);
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		}
	}

	
	private Date getTwoDaysAgoWorkingDayFromPropFile(Date date){
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		HashSet holidaySet = new HashSet();
		String holidayListStr = appProps.getProperty("hk.holiday", null);
		StringTokenizer st = new StringTokenizer(holidayListStr, ",");
		while(st.hasMoreTokens()){
			holidaySet.add(st.nextToken());
		}
			
		Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		int workingDaysCount = 0;
		while(true){
			if (c.get(Calendar.DAY_OF_WEEK)==1 || c.get(Calendar.DAY_OF_WEEK)==7 || holidaySet.contains(sdf_yyyymmdd.format(c.getTime()))) {
				c.add(Calendar.DATE, -1);
				continue;
			} else {
				workingDaysCount ++;
			}
			
			if(workingDaysCount ==3) {
				return c.getTime();
			} else {
				c.add(Calendar.DATE, -1);
			}
		}
		
		
	}
	
	private Date getTwoDaysAgoWorkingDayFromDB(Date date) {
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		List<Date> holidays = gtaPublicHolidayDao.getNearestHolidays(date);
		List<String> holidayStrs= new ArrayList<String>();
		for(Date d:holidays) {
			holidayStrs.add(sdf_yyyymmdd.format(d));
		}
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		
		int workingDaysCount = 0;
		while(true){
			if (c.get(Calendar.DAY_OF_WEEK)==1 || c.get(Calendar.DAY_OF_WEEK)==7 || holidayStrs.contains(sdf_yyyymmdd.format(c.getTime()))) {
				c.add(Calendar.DATE, -1);
				continue;
			} else {
				workingDaysCount ++;
			}
			
			if(workingDaysCount ==3) {
				return c.getTime();
			} else {
				c.add(Calendar.DATE, -1);
			}
			
		}
		
	}
	
	
	/**
	 * This method is used to download Transaction extract from remote sftp server,
	 * and parse the file to update database.
	 */
	@Transactional
	public void downloadVirtualAccountTransactionExtract() {
		logger.info("DailyScheduleJob.downloadVirtualAccountTransactionExtract() start ..");
		//file name regular 
		String remoteFileNameReg = ddxProps.getProperty("vaTransactionReportFileNameReg");
		
		if (CommUtil.nvl(receivePath).length() == 0 
				|| CommUtil.nvl(receiveBackPath).length() == 0
				|| CommUtil.nvl(downLoadPath).length() == 0
				|| CommUtil.nvl(downLoadBackPath).length() == 0) {
			
			ddxLogger.error("Schduler pause for error path configuration!");
			return;
		}
		
		File downloadDir = new File(downLoadPath);
		File downloadBackDir = new File(downLoadBackPath);
		if (!downloadDir.exists()) {
			downloadDir.mkdir();
		}
		if (!downloadBackDir.exists()) {
			downloadBackDir.mkdir();
		}
		
		
		try {
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,1000000);
			List<File> files = sftp.dowloadByFileNameRegex(receivePath, remoteFileNameReg, downLoadPath);
			List<CashvalueTopupHistory> succTopupList = new ArrayList<CashvalueTopupHistory>();
			List<CashvalueTopupHistory> rejChList = new ArrayList<CashvalueTopupHistory>();
			List<CashvalueTopupHistory> pndChList = new ArrayList<CashvalueTopupHistory>();
			File currentFile = null;
			try{
				for(File file:files) {
					currentFile = file;
					List<String> fileLines = new ArrayList<String>();
					LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(file)));
					String currentLine = reader.readLine();
					boolean firstLine = true;
					while (currentLine != null) {
						if(!firstLine) {
							fileLines.add(currentLine);
						}
						currentLine = reader.readLine();
						firstLine = false;
					}
					reader.close();
					String transactionRegex = "\\s*,\\s*";
					for(String line: fileLines) {
						String[] strs = line.split(transactionRegex);
						String transDate = strs[4];
						String valueDate = strs[2];
						String accNo = strs[0];
						String amount = strs[8];
						//Transaction Short Description
						String desc = strs[5];
						String transactionType = StringUtils.substring(desc.trim(), 0, 3);
						
						CashvalueTopupHistory topup = new CashvalueTopupHistory();
						if (CommUtil.nvl(transDate).length() != 0) {
							topup.setTopupDate(DateConvertUtil.parseString2Date(transDate, "dd/MM/yyyy"));
						}
						topup.setAmount(new BigDecimal(amount));
						topup.setBankFileReturn(file.getName());
						topup.setAccNo(accNo);
						topup.setCreateDate(new Date());
						if("CHQ".equals(transactionType)) {
							topup.setTopupMethod("CHQ");
							topup.setStatus("PND");
							pndChList.add(topup);
						} else if("RCQ".equals(transactionType)) {
							topup.setTopupMethod("RCQ");
							topup.setStatus("REJ");
							rejChList.add(topup);
						} else {
							topup.setTopupMethod("CSH");
							topup.setStatus("SUC");
							topup.setSuccessDate(DateConvertUtil.parseString2Date(transDate, "dd/MM/yyyy"));
							succTopupList.add(topup);
						}
						
					}
					//update database in batch
					topupCashValueByVirtualAccInBatch(succTopupList);
					processPndChTransactions(pndChList);
					processRejChTransactions(rejChList);
					
					//backup sftp file to bak directory
					sftp.move(file.getName(), receivePath, receiveBackPath);
					File outFile = new File(downLoadBackPath + File.separator + file.getName());
					FileCopyUtils.copy(file, outFile);
				}
			} catch(Exception e) {
				e.printStackTrace();
				ddxLogger.error("Happened Exception when process file:" + currentFile.getName());
				throw new RuntimeException(e);
			}
		} catch(Exception e) {
			e.printStackTrace();
			ddxLogger.error("Happened Exception when download files.");
			throw new RuntimeException(e);
		} 
		ddxLogger.info("DailyScheduleJob.downloadVirtualAccountTransactionExtract() end ..");
	}
	
	
}
