package com.sinodynamic.hkgta.service.rpos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
@Service
public class CustomerOrderPermitCardServiceImpl extends ServiceBase<CustomerOrderPermitCard> implements  CustomerOrderPermitCardService {

	@Autowired
	private CustomerOrderPermitCardDao customerOrderPermitCardDao;
	/**   
	* @author: Zero_Wang
	* @since: Sep 7, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public void updateCustomerOrderPermitCardStatus() {
		List<CustomerOrderPermitCard> list = this.customerOrderPermitCardDao.getUnACTList(Constant.Status.ACT.toString());
		if(list !=null && list.size()>0){
			for(CustomerOrderPermitCard c : list){
				c.setStatus(Constant.Status.ACT.toString());
			}
		}
	}
	
}
