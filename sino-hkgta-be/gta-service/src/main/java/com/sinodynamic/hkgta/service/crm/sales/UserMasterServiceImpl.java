package com.sinodynamic.hkgta.service.crm.sales;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.LoginSessionDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.entity.crm.LoginSession;
import com.sinodynamic.hkgta.entity.crm.LoginSessionPK;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Service
public class UserMasterServiceImpl extends ServiceBase<UserMaster> implements UserMasterService
{
	@Value(value ="#{appProperties['session.timeout.minute']}")
	private Integer sessionTimeoutMinute;

	@Value(value ="#{appProperties['session.updateInterval.minute']}")
	private Integer sessionUpdateInterval = 1;
	
	@Autowired
	private UserMasterDao userMasterDao;

	@Autowired
	private LoginSessionDao loginSessionDao;

	@Autowired
	private MemberDao memberDao;
	
	@Transactional
	public void addUserMaster(UserMaster m) throws Exception
	{
		userMasterDao.addUserMaster(m);
	}

	@Transactional
	public void getUserMasterList(ListPage<UserMaster> page, UserMaster m) throws Exception
	{
		String countHql = " select count(*) from UserMaster ";
		String hql = " from UserMaster m ";
		userMasterDao.getUserMasterList(page, countHql, hql, null);

	}

	@Transactional
	public void updateUserMaster(UserMaster m) throws Exception
	{
		userMasterDao.update(m);

	}

	@Transactional
	public void deleteUserMaster(UserMaster m) throws Exception
	{
		userMasterDao.delete(m);

	}

	@Transactional
	public UserMaster getUserByLoginId(String id) throws Exception
	{
		return userMasterDao.getUserByLoginId(id);
	}

	@Override
	@Transactional
	public UserMaster getUserByUserId(String userId) throws Exception
	{
		// TODO Auto-generated method stub
		return userMasterDao.getUserByUserId(userId);
	}

	@Override
	@Transactional
	public void SaveSessionToken(String userId, String authToken, String device) throws Exception
	{
		// LoginSession token = new LoginSession();

		LoginSession token = loginSessionDao.getByUserIdAndDevice(userId, device);
		if (null == token)
		{
			token = new LoginSession();
			LoginSessionPK pk = new LoginSessionPK();
			pk.setUserId(userId);
			pk.setDeviceAccess(device);
			token.setId(pk);
		}

		token.setSessionToken(authToken);
		token.setLastAccessTime(new Date());
		loginSessionDao.save(token);
	}

	/**
	 * @return Error Message.<br>
	 *         If token updated failed , it will returns the relevant error message.
	 * @author Vian Tang
	 * 
	 */
	@Override
	@Transactional
	public GTAError updateSessionToken(String authToken) {
		return updateSessionToken(authToken, true);
	}

	@Override
	@Transactional
	public GTAError updateSessionToken(String authToken, boolean shouldUpdateToken) {

		LoginSession session = loginSessionDao.getByToken(authToken);
		if (null == session) {
			return GTAError.LoginError.TOKEN_NOT_FOUND;
		}

		DateTime dateTime = new DateTime(session.getLastAccessTime());
		long currentTimeMillis = System.currentTimeMillis();
		if (dateTime.plusMinutes(sessionTimeoutMinute).getMillis() < currentTimeMillis) {
			return GTAError.LoginError.TOKEN_EXPIRED;
		} else if (shouldUpdateToken && dateTime.plusMinutes(sessionUpdateInterval).getMillis() < currentTimeMillis) {
			session.setLastAccessTime(new Date());
			loginSessionDao.save(session);
		}
		return GTAError.Success.SUCCESS;
	}
	
	@Override
	@Transactional
	public boolean deleteSessionToken(String userId, String device)
	{
		return loginSessionDao.deleteSessionToken(userId, device);
	}

	@Override
	public boolean requireValidateKaptchaCode(UserMaster usermaster)
	{
		return check(usermaster, Constant.LOGIN_FAIL_TRY_TIME);
	}

	@Override
	public boolean hasBeenLocked(UserMaster usermaster)
	{
		return check(usermaster, Constant.LOGIN_FAIL_TRY_TIME_LOCK);
	}

	private boolean check(UserMaster usermaster, String times)
	{
		int configtimes = Integer.parseInt(appProps.getProperty(times));
		if (configtimes == 0)
		{
			return false;
		}

		Calendar c = Calendar.getInstance();
		if (null == usermaster.getLastLoginFailTime())
		{
			return false;
		}
		c.setTime(usermaster.getLastLoginFailTime());
		c.add(Calendar.MINUTE, Integer.parseInt(appProps.getProperty(Constant.LOGIN_FAIL_TRY_PERIOD)));
		if (c.getTimeInMillis() < System.currentTimeMillis())
		{
			return false;
		}

		if (usermaster.getLoginFailCount() >= configtimes)
		{
			return true;
		}
		return false;
	}

	private Set<String> getDeviceCheckList(String devices)
	{
		return new HashSet<String>(Arrays.asList(devices.split(",")));
	}

	@Override
	public boolean requireValidateKaptchaCode(String device)
	{
		return getDeviceCheckList(appProps.getProperty(Constant.LOGIN_DEVICE_CHECK_LIST)).contains(device);
	}
	
	@Override
	public boolean requireValidateLocation(String device)
	{
		return getDeviceCheckList(appProps.getProperty(Constant.LOCATION_CHECK_DEVICE_LIST)).contains(device);
	}


	@Override
	@Transactional
	public void updateLoginFailTime(UserMaster usermaster) throws Exception
	{
		Long cnt = (usermaster.getLoginFailCount() == null) ? 0L : usermaster.getLoginFailCount();
		Long timeIncrement = CommUtil.getIncTimeInMillis(usermaster.getLastLoginFailTime(), Integer.parseInt(appProps.getProperty(Constant.LOGIN_FAIL_TRY_PERIOD)));
		if (null == timeIncrement || timeIncrement < System.currentTimeMillis())
			cnt = 0L;

		cnt++;
		if (cnt == 1 || cnt == Integer.parseInt(appProps.getProperty(Constant.LOGIN_FAIL_LOCK_PERIOD)))
		{
			usermaster.setLastLoginFailTime(new Date());
		}

		usermaster.setLoginFailCount(cnt);
		updateUserMaster(usermaster);
	}

	
	@Transactional
	public UserMaster getUserMasterByCustomerId(Long customerId){
		Member member = memberDao.getMemberByCustomerId(customerId);
		if(member==null) return null;
		if(StringUtils.isEmpty(member.getUserId())) return null;
		return userMasterDao.getUserByUserId(member.getUserId());
	}
	
	@Override
	@Transactional
	public void setAllLoginIdAndPassword(List<Member>members, String defaultPassword) throws Exception{
		//String sqlString = "select u from userMaster u, member m where u.userId = m.userId and u.userType = 'CUSTOMER'";
		for (Member member : members) {
			UserMaster userMaster = getUserMasterByCustomerId(member.getCustomerId());
			if (userMaster == null || !userMaster.getUserType().equals("CUSTOMER")) continue;
			userMaster.setLoginId(member.getAcademyNo());
			userMaster.setPassword(CommUtil.getMD5Password(member.getAcademyNo(), defaultPassword));
			updateUserMaster(userMaster);
		}
	}
}
