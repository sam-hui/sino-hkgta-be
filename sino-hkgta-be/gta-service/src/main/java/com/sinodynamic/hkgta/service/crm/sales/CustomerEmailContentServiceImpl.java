package com.sinodynamic.hkgta.service.crm.sales;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.crm.DaypassIssuedDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CustomerEmailContentServiceImpl extends ServiceBase<CustomerEmailContent> implements CustomerEmailContentService {

	private Logger logger = Logger.getLogger(CustomerEmailContentServiceImpl.class);

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;

	@Autowired
	private MessageTemplateDao messageTemplateDao;

	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerOrderPermitCardDao customerOrderPermitCardDao;
	
	@Resource(name = "appProperties")
	Properties appProps;

	@Transactional
	public CustomerEmailContent sendPresentationEmailToCustomer(File attach,Long customerID, String emailTitle, String emailBody)throws Exception {

		logger.info("CustomerEmailServiceImpl.sendEmailToCustomer invocation start ...");

		CustomerProfile cp = customerProfileDao.getById(customerID);
		if (cp == null)
			return null;
		String address = cp.getContactEmail();

		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setRecipientCustomerId(customerID.toString());
		cec.setRecipientEmail(address);
		cec.setSubject(emailTitle);
		cec.setNoticeType(Constant.NOTICE_TYPE_PRESENTATION);
		cec.setContent(emailBody);
		cec.setSendDate(new Date());
		String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendID);

		// add attachment records
		if (attach != null) {

			CustomerEmailAttach attachment = new CustomerEmailAttach();
			attachment.setEmailSendId(sendID);
			attachment.setAttachmentName(attach.getName());
			attachment.setAttachmentPath(attach.getAbsolutePath());

			customerEmailAttachDao.save(attachment);
		}

		logger.info("CustomerEmailServiceImpl.sendEmailToCustomer invocation end ...");
		return cec;

	}

	
	@Transactional
	public CustomerEmailContent setEmailContentForMemberActivationEmail(Long customerId,String emailType,String sendUserId,String fromName,boolean isInitial,String loginId,String password) {

		logger.info("CustomerEmailServiceImpl.setEmailContentForMemberActivationEmail invocation start ...");

		CustomerProfile cp = customerProfileDao.getById(customerId);
		if (cp == null) return null;
		
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(emailType);
		
		if(mt==null) return null;
		String content = mt.getContentHtml(); 
		if(content==null) return null;
		String domain = appProps.getProperty("domain");
		String webportalLoginPage = appProps.getProperty("webportal.login.page");
		String salutation = StringUtils.isBlank(cp.getSalutation())?"":cp.getSalutation();
		String givenName = StringUtils.isBlank(cp.getGivenName())?"":cp.getGivenName();
		String surname = StringUtils.isBlank(cp.getSurname())?"":cp.getSurname();
		fromName = StringUtils.isBlank(fromName)?"HKGTA":fromName;
		String fullName = StringUtils.isBlank(salutation)&&StringUtils.isBlank(givenName)&&StringUtils.isBlank(surname)?"Member":salutation+" "+givenName+" "+surname;
		String registrationUrl = webportalLoginPage==null||domain==null?"":" "+domain+webportalLoginPage;
		content = content.replaceAll(MessageTemplateParams.UserName.getParam(),fullName)
				.replaceAll(MessageTemplateParams.FromName.getParam(), fromName)
				.replaceAll(MessageTemplateParams.RegistrationUrl.getParam(), registrationUrl);
				
		if(isInitial){
			content = content.replaceAll(MessageTemplateParams.Password.getParam(), "Password: "+password).replaceAll(MessageTemplateParams.LoginId.getParam(),"Login ID: "+loginId);
		}else{
			content = content.replaceAll(MessageTemplateParams.Password.getParam(), "").replaceAll(MessageTemplateParams.LoginId.getParam(),"");
		}
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setRecipientCustomerId(customerId.toString());
		cec.setRecipientEmail(cp.getContactEmail());
		cec.setSubject(mt.getMessageSubject());
		cec.setContent(content);
		cec.setSendDate(new Date());
		cec.setStatus(EmailStatus.PND.name());
		cec.setSenderUserId(sendUserId);
		String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendID);

		logger.info("CustomerEmailServiceImpl.setEmailContentForMemberActivationEmail invocation end ...");
		return cec;

	}
	
	@Transactional
	public boolean modifyCustomerEmailContent(CustomerEmailContent cec) {

		return customerEmailContentDao.updateCustomerEmail(cec);
	}
	
	@Override
	@Transactional
	public CustomerEmailContent sendEmail4Transaction(TransactionEmailDto dto,String userName) throws Exception {

		logger.info("CustomerTransactionEmailServiceImpl.sendEmailToCustomer [Email Type: " + dto.getEmailType() + "] start ...");

		CustomerEmailContent cec = new CustomerEmailContent();
		CustomerProfile cp = null;
		if(dto.getNumber() != null){
			Long customerID = getCusotmerIDFromeEmailType(dto.getNumber(), dto.getEmailType());	
		    cp = customerProfileDao.getById(customerID);
			cec.setRecipientCustomerId(customerID.toString());						
			if(StringUtils.isBlank(dto.getSendTo())){dto.setSendTo(cp.getContactEmail());}		
		}
		
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(dto.getEmailType());
		
		String subject =dto.getSubject();
		String emailContent = dto.getEmailContent();
		
		cec.setRecipientEmail(dto.getSendTo());
		if(StringUtils.isBlank(subject)){
			cec.setSubject(mt.getMessageSubject());
		}else{
			cec.setSubject(dto.getSubject());
		}
		
		if(StringUtils.isBlank(emailContent)){
			String contentMT= mt.getContentHtml();
			if(cp!=null){
				cec.setContent(modifyContent(contentMT, cp, userName));
			}else{
				cec.setContent(contentMT);
			}
		}else{
			cec.setContent(dto.getEmailContent());
		}
		
		cec.setSendDate(new Date());
		String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendID);

		/** create PDF here late */
		
//		String pdfName = CommUtil.generatePdfName(dto.getEmailType());
//		String pdfPath = appProps.getProperty("pdf.remote.base.url") + pdfName;
//		CustomerEmailAttach attachment = new CustomerEmailAttach();
//		attachment.setEmailSendId(sendID);
//		attachment.setAttachmentName(pdfName);
//		attachment.setAttachmentPath(pdfPath);
//		customerEmailAttachDao.save(attachment);

		logger.info("CustomerTransactionEmailServiceImpl.sendEmailToCustomer end ...");
		return cec;

	}

	private long getCusotmerIDFromeEmailType(Long number, String emailType){
		if(EmailType.INVOICE.toString().equalsIgnoreCase(emailType) || EmailType.DAYPASSRECEIPT.toString().equalsIgnoreCase(emailType)){
			CustomerOrderHd coh = customerOrderHdDao.getOrderById(number);
			return coh.getCustomerId().longValue();
		}else if(EmailType.RECEIPT.toString().equalsIgnoreCase(emailType)){
			List<CustomerOrderTrans> list = customerOrderTransDao.getPaymentDetailsByTransactionNO(number);
			CustomerOrderTrans cot = list.get(0);
			return cot.getCustomerOrderHd().getCustomerId().longValue();		
		}else{
			return number;
		}
	}
	
	
	@Override
	@Transactional
	public TransactionEmailDto getOpenEmailTemplate(Long IDNO, String emailType, String userName) {

		long customerID = getCusotmerIDFromeEmailType(IDNO, emailType);		
		TransactionEmailDto dto = new TransactionEmailDto();
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(emailType);

		/** Query cp for modify mt content */
		CustomerProfile cp = customerProfileDao.getById(customerID);
		String content = modifyContent(mt.getContentHtml(), cp, userName);

		dto.setSubject(mt.getMessageSubject());
		dto.setEmailContent(content);
		dto.setEmailType(emailType);
		dto.setSendTo(cp.getContactEmail());

		return dto;
	}

	private String modifyContent(String content, CustomerProfile cp, String inscribe) {
		
		content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, cp.getGivenName() + " " + cp.getSurname());
		content = content.replace(Constant.PLACE_HOLDER_FROM_USER, inscribe);
		return content;
	}

	@Transactional
	public CustomerEmailContent sendRenewalEmail(Long customerID,String emailTitle, String emailBody) throws Exception {

		logger.info("CustomerEmailServiceImpl.sendRenewalEmail invocation start ...");

		CustomerProfile cp = customerProfileDao.getById(customerID);
		if (cp == null)
			return null;
		String address = cp.getContactEmail();

		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setRecipientCustomerId(customerID.toString());
		cec.setRecipientEmail(address);
		cec.setSubject(emailTitle);
		cec.setNoticeType(Constant.NOTICE_TYPE_RENEWAL);
		cec.setContent(emailBody);
		cec.setSendDate(new Date());
		String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendID);

		logger.info("CustomerEmailServiceImpl.sendRenewalEmail invocation end ...");
		return cec;
	}
	
	@Transactional
	public CustomerEmailContent sendExtensionLetter(Long customerID,String emailTitle, String emailBody) throws Exception {

		logger.info("CustomerEmailServiceImpl.sendExtensionLetter invocation start ...");

		CustomerProfile cp = customerProfileDao.getById(customerID);
		if (cp == null)
			return null;
		String address = cp.getContactEmail();

		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setRecipientCustomerId(customerID.toString());
		cec.setRecipientEmail(address);
		cec.setSubject(emailTitle);
		cec.setNoticeType(Constant.NOTICE_TYPE_EXTENSION);
		cec.setContent(emailBody);
		cec.setSendDate(new Date());
		String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendID);

		logger.info("CustomerEmailServiceImpl.sendExtensionLetter invocation end ...");
		return cec;
	}

	@Transactional
	public CustomerEmailContent sendPresentationEmailToSomebody(File attach, String emailTo, String emailTitle, String emailBody) throws Exception {
		
		logger.info("CustomerEmailContentServiceImpl.sendPresentationEmailToSomebody invocation start ...");

		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setRecipientEmail(emailTo);
		cec.setSubject(emailTitle);
		cec.setNoticeType(Constant.NOTICE_TYPE_TEMP);
		cec.setContent(emailBody);
		cec.setSendDate(new Date());
		String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendID);

		// add attachment records
		if (attach != null) {

			CustomerEmailAttach attachment = new CustomerEmailAttach();
			attachment.setEmailSendId(sendID);
			attachment.setAttachmentName(attach.getName());
			attachment.setAttachmentPath(attach.getAbsolutePath());

			customerEmailAttachDao.save(attachment);
		}

		logger.info("CustomerEmailContentServiceImpl.sendPresentationEmailToSomebody invocation end ...");
		return cec;

		
	}
	
	@Override
	@Transactional
	public void updateEmailStatusInBatch(List<CustomerEmailContent> emailList) throws Exception {
	    
	    if (emailList == null || emailList.size() == 0) return;
	    for (CustomerEmailContent cec : emailList) {
		customerEmailContentDao.updateCustomerEmail(cec);
	    }
	}
	

	@Transactional
	public ResponseResult daypassIssuedNotify(DaypassIssuedDto daypassIssuedDto) throws Exception {
		
		String customerId = daypassIssuedDto.getCustomerId();
		String dayPassId = daypassIssuedDto.getDayPassId();
		
		if (StringUtils.isEmpty(customerId) || StringUtils.isEmpty(dayPassId)) {
			responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_PARAMETER_ERROR);
			return responseResult;
		}
		
		CustomerOrderPermitCard copc = customerOrderPermitCardDao.get(CustomerOrderPermitCard.class, Long.parseLong(dayPassId));
		if (copc == null) {
			responseResult.initResult(GTAError.TemplateError.INVALID_DAYPASS_ID);
			return responseResult;
		}
		
		CustomerProfile cp = customerProfileDao.getCustomerProfileByCustomerId(customerId);
		if (cp == null) {
			responseResult.initResult(GTAError.TemplateError.INVALID_CUSTOMER_ID);
			return responseResult;
		}
		
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("daypass_issue_notify");
		if (mt == null) {
			responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_NO_FOUND);
			return responseResult;
		}
		
		CustomerEmailContent cec = composeDaypassIssueNotify(cp, copc, mt, daypassIssuedDto);
		
		responseResult.initResult(GTAError.Success.SUCCESS, cec);
		return responseResult;
	}
	
	
	/**
	 * @Author Mianping_Wu
	 * @Date Sep 2, 2015
	 * @Param @param cp
	 * @Param @param copc
	 * @Param @param mt
	 * @Param @param dto
	 * @Param @return
	 * @Param @throws Exception
	 * @return CustomerEmailContent
	 */
	private CustomerEmailContent composeDaypassIssueNotify(CustomerProfile cp, CustomerOrderPermitCard copc, MessageTemplate mt, DaypassIssuedDto dto) throws Exception {
		
		//message info
		String emailTo = cp.getContactEmail();
		String emailTitle = mt.getMessageSubject();
		String content = mt.getContentHtml();
		
		//message content factor
		String username = cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname();
		String guestname = dto.getGuest();
		String daypassid = dto.getDayPassId();
		String type = dto.getDayPassType();
		String validperiod = null;
		String issueddate = null;
		String emailBody = null;
		if (copc.getEffectiveFrom() != null && copc.getEffectiveTo() != null) {
			
			String from = DateCalcUtil.formatDate(copc.getEffectiveFrom());
			String to = DateCalcUtil.formatDate(copc.getEffectiveTo());
			if (from.equalsIgnoreCase(to)) {
				validperiod = from;
			} else {
				validperiod = from + " To " + to;
			}
		}
		if (copc.getLinkedCardDate() != null) {
			issueddate = DateCalcUtil.formatDate(copc.getLinkedCardDate());
		}
		
		if (!StringUtils.isEmpty(content)) {
			
			emailBody = content.replace("{username}", CommUtil.nvl(username))
					.replace("{daypassid}", CommUtil.nvl(daypassid))
					.replace("{validperiod}", CommUtil.nvl(validperiod))
					.replace("{guestname}", CommUtil.nvl(guestname))
					.replace("{type}", CommUtil.nvl(type))
					.replace("{issueddate}", CommUtil.nvl(issueddate));
		}
		
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setRecipientEmail(emailTo);
		cec.setSubject(emailTitle);
		cec.setNoticeType("N");
		cec.setContent(emailBody);
		cec.setSendDate(new Date());
		customerEmailContentDao.addCustomerEmail(cec);
		
		return cec;
	}

	@Override
	@Transactional
	public CustomerEmailContent getCustomerEmailContent(String id) {
		return customerEmailContentDao.get(CustomerEmailContent.class, id);
	}

	@Override
	@Transactional
	public void saveCustomerEmailContent(CustomerEmailContent customerEmailContent) {
		customerEmailContentDao.save(customerEmailContent);
	}
	
}
