package com.sinodynamic.hkgta.service.crm.account;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import com.sinodynamic.hkgta.dao.crm.DDITransactionLogDao;
import com.sinodynamic.hkgta.dao.crm.DDInterfaceFileLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dto.account.DDAFailReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDAFailReportDto;
import com.sinodynamic.hkgta.dto.account.DDASuccessReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDASuccessReportDto;
import com.sinodynamic.hkgta.entity.crm.DdInterfaceFileLog;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.SftpByJsch;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class DDATaskServiceImpl extends ServiceBase implements DDATaskService {
	
	private Logger ddxLogger = LoggerFactory.getLogger("ddx");
	@Resource(name = "ddxProperties")
	protected Properties ddxProps;
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	@Autowired
	private DDInterfaceFileLogDao ddInterfaceFileLogDao;
	@Autowired
	private DDITransactionLogDao ddiTransactionLogDao;
	@Autowired
	private MemberDao memberDao;
	
	private String ftpHost;
	private String port;
	private String ftpUserName;
	private String ftpPassword;
	private String timeout;

	private String uploadPath;
	private String receivePath;
	private String receiveBackPath;
	private String downLoadPath;//store all the remote files in local 
	private String downLoadBackPath;//store the success processed files in local
	
	private String originatorAccountNumber = "44708063295";
	
	@PostConstruct
	public void init() {
		ftpHost = ddxProps.getProperty(Constant.SFTP_HOST, null);
		port = ddxProps.getProperty(Constant.SFTP_PORT, null);
		ftpUserName = ddxProps.getProperty(Constant.SFTP_USERNAME, null);
		ftpPassword = ddxProps.getProperty(Constant.SFTP_PASSWORD, null);
		timeout = ddxProps.getProperty(Constant.SFTP_TIMEOUT, null);

		uploadPath = ddxProps.getProperty(Constant.SFTP_SNED_PATH, null);//remote upload dir
		receivePath = ddxProps.getProperty(Constant.SFTP_RECEIVE_PATH, null);//remote downloand dir
		receiveBackPath = ddxProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, null);//remote back dir, after processed move to this dir
		downLoadPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_PATH, null);//store all the remote files in local 
		downLoadBackPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_BACK_PATH, null);//store the success processed files in local

	}
	
	private  void readDDASuccessHeader(String[] csvItems, DDASuccessReportDto ddaSuccessReport) throws Exception {
		if(!"AUTH_DDA".equals(csvItems[1]))
			throw new Exception("is not AUTH_DDA report");
		ddaSuccessReport.setSystemDate(csvItems[2]);
		ddaSuccessReport.setTransactionTime(csvItems[3]);
		ddaSuccessReport.setOriginatingNo(csvItems[4]);
		ddaSuccessReport.setCustomerId(csvItems[5]);
	}

	private  void readDDASuccessDetail(String[] csvItems, DDASuccessReportDto ddaSuccessReport) {
		DDASuccessReportDetailDto detail = new DDASuccessReportDetailDto();
		detail.setAmount(csvItems[16]);
		detail.setBuyerCode(csvItems[5]);
		detail.setBuyerName(csvItems[6]);
		detail.setCurrencyCode(csvItems[29]);
		detail.setDdaType(csvItems[4]);
		detail.setDebitAccountName(csvItems[11]);
		detail.setDebitAccountNumber(csvItems[10]);
		detail.setDebtorId(csvItems[24]);
		detail.setDebtorIdNumber1(csvItems[31]);
		detail.setDebtorIdNumber2(csvItems[33]);
		detail.setDebtorIdType1(csvItems[30]);
		detail.setDebtorIdType2(csvItems[32]);
		detail.setDraweeBankCode(csvItems[7]);
		detail.setDraweeBankName(csvItems[8]);
		detail.setDraweeBranchCode(csvItems[9]);
		detail.setEndDate(csvItems[13]);
		detail.setFrequency(csvItems[15]);
		detail.setLastStatusUpdateDate(csvItems[18]);
		detail.setMandateRemarks(csvItems[22]);
		detail.setMandateSeqNo(csvItems[23]);
		detail.setMandateStatus(csvItems[27]);
		detail.setMandateStatusCode(csvItems[28]);
		detail.setMandateType(csvItems[14]);
		detail.setMicrCode(csvItems[20]);
		detail.setOriginatorAccountNumber(csvItems[3]);
		detail.setOriginatorBankCode(csvItems[2]);
		detail.setOriginatorId(csvItems[1]);
		detail.setRcmsMandateManagementId(csvItems[25]);
		detail.setReference(csvItems[26]);
		detail.setRemarks(csvItems[17]);
		detail.setStartDate(csvItems[12]);
		detail.setTransactionId(csvItems[21]);
		detail.setValueDate(csvItems[19]);
		ddaSuccessReport.getDetails().add(detail);
	}

	private  void readDDASuccessTail(String[] csvItems, DDASuccessReportDto ddaSuccessReport) {
		ddaSuccessReport.setNoOfRecord(Integer.parseInt(csvItems[1]));
		ddaSuccessReport.setTotalTransactionAmount(Integer.parseInt(csvItems[2]));
	}

	private  void readDDAFailHeader(String[] csvItems, DDAFailReportDto ddaFailReport) {
		ddaFailReport.setSystemDate(csvItems[2]);
		ddaFailReport.setTransactionTime(csvItems[3]);
		ddaFailReport.setBankNo(csvItems[4]);
		ddaFailReport.setCustomerName(csvItems[5]);
	}

	private  void readDDAFailDetail(String[] csvItems, DDAFailReportDto ddiFailReport) {
		DDAFailReportDetailDto ddaFailReportDetail = new DDAFailReportDetailDto();
		ddaFailReportDetail.setAmount(csvItems[16]);
		ddaFailReportDetail.setBuyerCode(csvItems[5]);
		ddaFailReportDetail.setBuyerName(csvItems[6]);
		ddaFailReportDetail.setDdaType(csvItems[4]);
		ddaFailReportDetail.setDebitAccountName(csvItems[11]);
		ddaFailReportDetail.setDebitAccountNumber(csvItems[10]);
		ddaFailReportDetail.setDraweeBankCode(csvItems[7]);
		ddaFailReportDetail.setDraweeBankName(csvItems[8]);
		ddaFailReportDetail.setDraweeBranchCode(csvItems[9]);
		ddaFailReportDetail.setEndDate(csvItems[13]);
		ddaFailReportDetail.setFrequency(csvItems[15]);
		ddaFailReportDetail.setMandateType(csvItems[14]);
		ddaFailReportDetail.setOriginatorAccountNumber(csvItems[3]);
		ddaFailReportDetail.setOriginatorBankCode(csvItems[2]);
		ddaFailReportDetail.setOriginatorId(csvItems[1]);
		ddaFailReportDetail.setRejectCode(csvItems[18]);
		ddaFailReportDetail.setRejectDesc(csvItems[19]);
		ddaFailReportDetail.setRemarks(csvItems[17]);
		ddaFailReportDetail.setStartDate(csvItems[12]);
		ddaFailReportDetail.setValueDate(csvItems[20]);
		ddaFailReportDetail.setMicrCode(csvItems[21]);
		ddaFailReportDetail.setDebtorId(csvItems[22]);
		ddaFailReportDetail.setReference(csvItems[23]);
		ddaFailReportDetail.setDeletionDate(csvItems[24]);
		ddaFailReportDetail.setReactivaitonDate(csvItems[25]);
		ddaFailReportDetail.setLastStatusUpdateDate(csvItems[26]);
		ddaFailReportDetail.setActionIndicator(csvItems[27]);
		ddaFailReportDetail.setResultIndicator(csvItems[28]);
		ddaFailReportDetail.setMandateStatus(csvItems[29]);
		ddaFailReportDetail.setMandateStatusCode(csvItems[30]);
		ddaFailReportDetail.setCurrencyCode(csvItems[31]);
		ddaFailReportDetail.setDebtorIdType1(csvItems[32]);
		ddaFailReportDetail.setDebtorIdNumber1(csvItems[33]);
		ddaFailReportDetail.setDebtorIdType2(csvItems[34]);
		ddaFailReportDetail.setDebtorIdNumber2(csvItems[35]);
		
		ddiFailReport.getDetails().add(ddaFailReportDetail);
	}

	private  void readDDAFailTail(String[] items, DDAFailReportDto ddiFailReport) {
		ddiFailReport.setNoOfRecord(Integer.parseInt(items[1]));
		ddiFailReport.setTotalTransctionAmount(items[2]);
	}

	
	private  DDASuccessReportDto parseDDASuccessReport(File file) throws Exception {
		DDASuccessReportDto ddaSuccessReport = new DDASuccessReportDto();
		ddaSuccessReport.setDetails(new ArrayList<DDASuccessReportDetailDto>());
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				//System.out.println(sCurrentLine);
				String[] items = sCurrentLine.split(",");
				if (items.length > 0 && "H".equals(items[0])){
					readDDASuccessHeader(items,
							ddaSuccessReport);
				} else if (items.length > 0 && "D".equals(items[0])){
					readDDASuccessDetail(items,
							ddaSuccessReport);
				} else if (items.length > 0 && "T".equals(items[0])) {
					readDDASuccessTail(items,
							ddaSuccessReport);
				}
					
			}

			if (ddaSuccessReport.getDetails().size() != ddaSuccessReport.getNoOfRecord()) {
				throw new Exception("ERROR ! no. of record not equals");
			}

		} finally {
			br.close();
		}

		return ddaSuccessReport;
	}
	
	private  DDAFailReportDto parseRejectReport(File file) throws Exception {
		DDAFailReportDto ddaFailReport = new DDAFailReportDto();
        ddaFailReport.setDetails(new ArrayList<DDAFailReportDetailDto>());
        BufferedReader br = null;
		 try {
	            br = new BufferedReader(new FileReader(file));
	            String sCurrentLine;
	            
	            while ((sCurrentLine = br.readLine()) != null) {
					System.out.println(sCurrentLine);
					String[] items = sCurrentLine.split(",");				
					if(items.length > 0 && 
							"H".equals(items[0]))
						readDDAFailHeader(items, ddaFailReport);
					else if(items.length >0 && 
							"D".equals(items[0]))
						readDDAFailDetail(items, ddaFailReport);
					else if(items.length >0 && 
							"T".equals(items[0]))
						readDDAFailTail(items, ddaFailReport);
				}
	            
	            if(ddaFailReport.getDetails().size() != ddaFailReport.getNoOfRecord())
	            	System.out.println("ERROR ! no. of record not equals");
	            
	            System.out.println(ddaFailReport);
	            for(DDAFailReportDetailDto ddaFailReportDetail : ddaFailReport.getDetails()){
	            	System.out.println(ddaFailReportDetail.toString());
	            }
	            // TODO: 
	            
	        } finally {
	        	br.close();
	        }
		 return ddaFailReport;
	}
	
	
	private void parseDDAConfirmReporter(File file) {
		try{
			DDASuccessReportDto dda = parseDDASuccessReport(file);
			for(DDASuccessReportDetailDto details :dda.getDetails()) {
				MemberPaymentAcc memberPaymentAcc = new MemberPaymentAcc();
				memberPaymentAcc.setCustomerId(Long.parseLong(dda.getCustomerId()));
				memberPaymentAcc.setAccType("DDA");
				String bankCode = details.getBuyerCode();
				if("003".equals(details.getOriginatorBankCode())) {
					memberPaymentAcc.setBankId("SCB");
				} else{
					//TODO
				}
				memberPaymentAcc.setCustomerId(Long.parseLong(dda.getCustomerId()));
				Member member = memberDao.getMemberById(Long.parseLong(dda.getCustomerId()));
				memberPaymentAcc.setAccountNo(details.getDebitAccountNumber());
				memberPaymentAcc.setStatus("ACT");//TODO
				memberPaymentAcc.setOriginatorBankCode(details.getOriginatorBankCode());
				memberPaymentAcc.setDrawerBankCode(details.getDraweeBankCode());
				memberPaymentAcc.setBankCustomerId(details.getDebitAccountNumber());
				memberPaymentAcc.setMember(member);
				memberPaymentAcc.setBankAccountName(details.getBuyerName());
				memberPaymentAccDao.addMemberPaymentAcc(memberPaymentAcc);
				DdInterfaceFileLog ddInterfaceFileLog = new DdInterfaceFileLog();
				ddInterfaceFileLog.setFilename(file.getName());
				ddInterfaceFileLog.setTransactionType("DDA");
				ddInterfaceFileLog.setStatus("SUCC");
				ddInterfaceFileLog.setProcessTimestamp(new Date());
				//ddInterfaceFileLog.setAckTimestamp(ackTimestamp); ???????
				ddInterfaceFileLogDao.save(ddInterfaceFileLog);
			}
			//backup sftp file to bak directory
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			sftp.move(file.getName(), receivePath, receiveBackPath);
			File outFile = new File(downLoadBackPath + File.separator + file.getName());
			FileCopyUtils.copy(file, outFile);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
		

	}

	
	private void parseDDARejectReporter(File file) {
		/*try{
			DDAFailReportDto dda = parseRejectReport(file);
			for(DDAFailReportDetailDto details :dda.getDetails()) {
				MemberPaymentAcc memberPaymentAcc = new MemberPaymentAcc();
				memberPaymentAcc.setAccType("DDA");
				String bankCode = details.getBuyerCode();
				if("003".equals(bankCode)) {
					memberPaymentAcc.setBankId("SCB");
				} else{
					//TODO
				}
				memberPaymentAcc.setAccountNo(details.getDebitAccountNumber());
				memberPaymentAcc.setStatus("ACT");//TODO
				memberPaymentAcc.setOriginatorBankCode(details.getOriginatorBankCode());
				memberPaymentAcc.setDrawerBankCode(details.getDraweeBankCode());
				memberPaymentAcc.setBankCustomerId(details.getDebitAccountNumber());
				memberPaymentAcc.setBankAccountName(details.getBuyerName());
				memberPaymentAccDao.save(memberPaymentAcc);
			}
			//backup sftp file to bak directory
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			sftp.move(file.getName(), receivePath, receiveBackPath);
			File outFile = new File(downLoadBackPath + File.separator + file.getName());
			FileCopyUtils.copy(file, outFile);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}*/
		
		try{
			DdInterfaceFileLog ddInterfaceFileLog = new DdInterfaceFileLog();
			ddInterfaceFileLog.setFilename(file.getName());
			ddInterfaceFileLog.setTransactionType("DDA");
			ddInterfaceFileLog.setStatus("FAIL");
			ddInterfaceFileLog.setProcessTimestamp(new Date());
			//ddInterfaceFileLog.setAckTimestamp(ackTimestamp); ???????
			ddInterfaceFileLogDao.save(ddInterfaceFileLog);
			//backup sftp file to bak directory
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			sftp.move(file.getName(), receivePath, receiveBackPath);
			File outFile = new File(downLoadBackPath + File.separator + file.getName());
			FileCopyUtils.copy(file, outFile);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
		
	}

	private void parseDDASuccReporter(File file) {
		try{
			DdInterfaceFileLog ddInterfaceFileLog = new DdInterfaceFileLog();
			ddInterfaceFileLog.setFilename(file.getName());
			ddInterfaceFileLog.setTransactionType("DDA");
			ddInterfaceFileLog.setStatus("SUCC");
			ddInterfaceFileLog.setProcessTimestamp(new Date());
			//ddInterfaceFileLog.setAckTimestamp(ackTimestamp); ???????
			ddInterfaceFileLogDao.save(ddInterfaceFileLog);
			//backup sftp file to bak directory
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			sftp.move(file.getName(), receivePath, receiveBackPath);
			File outFile = new File(downLoadBackPath + File.separator + file.getName());
			FileCopyUtils.copy(file, outFile);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	
	
	/*private void downloadDDAAckReporter() {
		//file name regular 
		//String remoteFileNameReg = "hkgta\\d*\\.H2H-HK-DDA\\.\\d*.*\\.\\d*\\.txn";
		String ddaReporterFileReg = ddxProps.getProperty("ddaReporterFileReg");
		try{
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			List<File> files = sftp.dowloadByFileNameRegex(receivePath, ddaReporterFileReg, downLoadPath);
			for(File file:files) {
				try{
					parseDDAConfirmReporter(file);
				} catch(RuntimeException e) {
					e.printStackTrace();
					ddxLogger.error("Happened Exception when parse file:" + file.getName());
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			ddxLogger.error("Happened Exception when process DDA ack reporter");
		}
	}*/
	
	private void downloadDDARejectReporter() {
		//file name regular 
		//String remoteFileNameReg = "hkgta\\d*\\.RCMS_E_60\\.\\d*.*\\.\\d*\\.txn";
		String ddaRejectReporterFileReg =ddxProps.getProperty("ddaRejectReporterFileReg");
		try{
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			List<File> files = sftp.dowloadByFileNameRegex(receivePath, ddaRejectReporterFileReg, downLoadPath);
			for(File file:files) {
				try{
					parseDDAConfirmReporter(file);
				} catch(RuntimeException e) {
					e.printStackTrace();
					ddxLogger.error("Happened Exception when parse file:" + file.getName());
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			ddxLogger.error("Happened Exception when download DDA reject reporter");
		}
	}
	
	private void downloadDDASuccReporter() {
		//file name regular 
		//String remoteFileNameReg = "hkgta\\d*\\.RCMS_E_60\\.\\d*.*\\.\\d*\\.txn";
		String ddaReporterFileReg = ddxProps.getProperty("ddaReporterFileReg");
		try{
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			List<File> files = sftp.dowloadByFileNameRegex(receivePath, ddaReporterFileReg, downLoadPath);
			for(File file:files) {
				try{
					parseDDASuccReporter(file);
				} catch(RuntimeException e) {
					e.printStackTrace();
					ddxLogger.error("Happened Exception when parse file:" + file.getName());
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			ddxLogger.error("Happened Exception when download DDA success reporter");
		}
	}
	
	@Transactional
	public void ddaTask() {
		downloadDDASuccReporter();
		downloadDDARejectReporter();
	}
	

}
