package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dto.fms.AvailableTimelotDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
public class FacilityTimeslotServiceImpl extends ServiceBase<FacilityTimeslot> implements FacilityTimeslotService {

	@Autowired
	private FacilityTimeslotDao facilityTimeslotDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityTimeslot> getFacilityTimeslotList(String typeCode, Date beginDatetime, Integer venueFloor) {
		return facilityTimeslotDao.getFacilityTimeslotList(typeCode, beginDatetime, venueFloor);
	}

	@Override
	public boolean updateFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException {
		return facilityTimeslotDao.updateFacilityTimeslot(facilityTimeslot);
	}

	@Override
	public Serializable addFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException {
		return facilityTimeslotDao.addFacilityTimeslot(facilityTimeslot);
	}

	@Override
	public FacilityTimeslot getFacilityTimeslot(Long facility_no, String typeCode, int venueFloor, Date beginDatetime,
			Date endDatetime) {
		return facilityTimeslotDao.getFacilityTimeslot(facility_no, beginDatetime, endDatetime);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityTimeslot> getFacilityTimeslotListByRange(String typeCode, Date beginDatetime, Date endDatetime, Integer venueFloor)
	{
		return facilityTimeslotDao.getFacilityTimeslotListByRange(typeCode, beginDatetime, endDatetime, venueFloor);
	}

	@Override
	@Transactional
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String facilityType, Date date, String bayType) {
		try{
			return facilityTimeslotDao.getFacilityTimeslotDtoList(facilityType, date, bayType);
		}catch(Exception e){
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{e.getMessage()});
		}
		
	}

	@Override
	@Transactional
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String coachId, String facilityType, Date date, String bayType) {
		try{
			return facilityTimeslotDao.getFacilityTimeslotDtoList(coachId, facilityType, date, bayType);
		}catch(Exception e){
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{e.getMessage()});
		}
	}
}
