package com.sinodynamic.hkgta.service.crm.backoffice.coachmanage;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.StaffCoachInfoDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.StaffCoachRatePosDao;
import com.sinodynamic.hkgta.dao.crm.StaffCoachRoasterDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityAttendanceDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedFacilityDao;
import com.sinodynamic.hkgta.dao.fms.StaffTimeslotDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo.CoachDayRateInfo;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo.TimeSliceRate;
import com.sinodynamic.hkgta.dto.fms.AvailableCoachDto;
import com.sinodynamic.hkgta.dto.fms.AvailableDateDto;
import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRatePos;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRoaster;
import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**   
* @author: Ray_Liang
* @since: Aug 4, 2015
*/

    
@Service
public class CoachManagementServiceImpl extends ServiceBase<StaffCoachRoaster> implements CoachManagementService {

	@Autowired
	StaffCoachRoasterDao staffCoachRoasterDao;
	
	@Autowired
	CustomerServiceDao customerServiceDao;
	
	@Autowired
	CustomerProfileDao customerProfileDao;
	
	@Autowired
	MemberCashValueDao cashValueDao;
	
	@Autowired
	MemberLimitRuleDao limitRuleDao;
	
	@Autowired
	MemberDao memberDao;
	
	@Autowired
	private StaffCoachRatePosDao coachRatePosDao; 
	@Autowired
	private UserMasterDao userMasterDao;
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao; 
	@Autowired
	StaffCoachInfoDao staffCoachInfoDao;
	@Autowired
	StaffTimeslotDao staffTimeslotDao;
	@Autowired
	MemberReservedFacilityDao memberReservedFacilityDao;
	
	@Autowired
	MemberFacilityAttendanceDao memberFacilityAttendanceDao;
	
	@Autowired
	MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	
	private 
	void setRateType(StaffCoachRoaster rosterItem, String rateType){
		if(Constant.RateType.OFF.toString().equals(rateType)){
			rosterItem.setRateType(null);
			rosterItem.setOffDuty(Constant.YES);
		}else if(Constant.RateType.BLK.toString().equals(rateType)){
			rosterItem.setRateType(null);
			rosterItem.setOffDuty(Constant.BLOCK);
		}else if(Constant.RateType.HI.toString().equals(rateType) || Constant.RateType.LO.toString().equals(rateType)){
			rosterItem.setRateType(rateType);
			rosterItem.setOffDuty(null);
		}else{
			new GTACommonException(GTAError.CoachMgrError.UNKNOWN_RATETYPE, new String[]{rateType});
		}
	}
	
	@Override
	@Transactional
	public boolean savePresentRoster(final String userId, final CoachRosterInfo rosterInfo) {
		List<StaffCoachRatePos>  staffCoachRatePos = this.coachRatePosDao.getStaffCoachRatePosByUserId(rosterInfo.getCoachId());
		if(null==staffCoachRatePos || 0==staffCoachRatePos.size()){
			savePrice4Coach(rosterInfo,userId);
		}else {
			updatePrice4Coach(rosterInfo,userId,staffCoachRatePos);
		}
		CallBackExecutor exe = new CallBackExecutor(CoachManagementServiceImpl.class);
		return (Boolean)exe.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				List<CoachDayRateInfo> dayRateList = rosterInfo.getDayRateList();
				List<StaffCoachRoaster> rawPresentRoster = staffCoachRoasterDao.loadRawPresentRoster(rosterInfo.getCoachId());
				Collections.sort(rawPresentRoster);
				
				Date current = new Date();
				for(CoachDayRateInfo dayRateInfo : dayRateList){
					List<TimeSliceRate> slices = dayRateInfo.getRateList();
					for(TimeSliceRate slice : slices){
						StaffCoachRoaster roster = new StaffCoachRoaster();
						roster.setCoachId(rosterInfo.getCoachId());
						
						String rateType = slice.getRateType();
						
						roster.setWeekDay(String.valueOf(dayRateInfo.getWeekDay()));
						roster.setOnDate(null);
						roster.setBeginTime(slice.getBeginTime());
						roster.setEndTime(slice.getBeginTime() + 1);
						roster.setCreateDate(current);
						roster.setCreateBy(userId);
						
						int index = Collections.binarySearch(rawPresentRoster, roster);
						if(index >= 0){
							StaffCoachRoaster rawRosterItem = rawPresentRoster.get(index);
							setRateType(rawRosterItem, rateType);
							staffCoachRoasterDao.update(rawRosterItem);
						}else{
							setRateType(roster, rateType);
							staffCoachRoasterDao.save(roster);
						}
						
					}
				}
				return Boolean.TRUE;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CoachMgrError.FAIL_SAVE_PRESENT_ROSTER);
			}
		});
	}

	private void updatePrice4Coach(CoachRosterInfo info, String userId,
			List<StaffCoachRatePos> list) {
		for(StaffCoachRatePos pos : list){
			if(Constant.RateType.HI.toString().equals(pos.getRateType())){
				PosServiceItemPrice highPrice = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, pos.getPosItemNo());
				highPrice.setItemPrice(info.getHighRatePrice());
				this.posServiceItemPriceDao.update(highPrice);
			}else {
				PosServiceItemPrice lowPrice = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, pos.getPosItemNo());
				lowPrice.setItemPrice(info.getLowRatePrice());
				this.posServiceItemPriceDao.update(lowPrice);
			}
		}
	}

	private void savePrice4Coach(CoachRosterInfo info,String userId) {
		StaffDto staffDto = this.userMasterDao.getStaffDto(info.getCoachId());
		List<String> result = null;
		if(null != info.getHighRatePrice()){
			StaffCoachRatePos highratePos = new StaffCoachRatePos();
			highratePos.setUserId(info.getCoachId());
			highratePos.setCreateBy(userId);
			highratePos.setUpdateBy(userId);
			highratePos.setCreateDate(new Timestamp(System.currentTimeMillis()));
			highratePos.setUpdateDate(new Date());
			result = obtainDescriptinAndPrefix(staffDto, "high");
			highratePos.setRateType(Constant.RateType.HI.toString());
			highratePos.setDescription(result.get(0));
			this.coachRatePosDao.save(highratePos);
			PosServiceItemPrice highPrice = new PosServiceItemPrice();
			String prefix = result.get(1);
			highPrice.setItemNo(prefix+CommUtil.formatPosServiceItemNo(highratePos.getRateId()));
			highPrice.setItemCatagory(prefix.substring(0, 2));
			highPrice.setDescription(highratePos.getDescription());
			highPrice.setStatus(Constant.Status.ACT.toString());
			highPrice.setItemPrice(info.getHighRatePrice());
			this.posServiceItemPriceDao.save(highPrice);
			highratePos.setPosItemNo(highPrice.getItemNo());
		}
		if(null != info.getLowRatePrice()){
			StaffCoachRatePos lowRatePos = new StaffCoachRatePos();
			lowRatePos.setUserId(info.getCoachId());
			lowRatePos.setCreateBy(userId);
			lowRatePos.setUpdateBy(userId);
			lowRatePos.setCreateDate(new Timestamp(System.currentTimeMillis()));
			lowRatePos.setUpdateDate(new Date());
			result = obtainDescriptinAndPrefix(staffDto, "low");
			lowRatePos.setRateType(Constant.RateType.LO.toString());
			lowRatePos.setDescription(result.get(0));
			this.coachRatePosDao.save(lowRatePos);
			PosServiceItemPrice lowPrice = new PosServiceItemPrice();
			String prefix = result.get(1);
			lowPrice.setItemNo(prefix+CommUtil.formatPosServiceItemNo(lowRatePos.getRateId()));
			lowPrice.setItemCatagory(prefix.substring(0, 2));
			lowPrice.setDescription(lowRatePos.getDescription());
			lowPrice.setStatus(Constant.Status.ACT.toString());
			lowPrice.setItemPrice(info.getLowRatePrice());
			this.posServiceItemPriceDao.save(lowPrice);
			lowRatePos.setPosItemNo(lowPrice.getItemNo());
		}
	}
	private List<String> obtainDescriptinAndPrefix(StaffDto staffDto, String type) {
		List<String> result = new ArrayList<String>();
		String coachName = "";
		
		if(StringUtils.isNotBlank(staffDto.getStaffName())){
			coachName = staffDto.getStaffName().trim();
		} else if(StringUtils.isNotBlank(staffDto.getNickname())){
			coachName = staffDto.getNickname().trim();
		}else {
			coachName = staffDto.getUserId();
		}
		
		
		String description = "";
		// FTG - full time golf trainer; 
		if(Constant.StaffType.FTG.toString().equals(staffDto.getStaffType())){
			//description = "Full time golf private coach "+coachName+" "+ type + "rate";
			if("high".equals(type)){
				result.add("Golf coach - " +  coachName + "(High Rate)");
				result.add(Constant.GSSHI_RATE_ITEM_PREFIX);
			}else{
				result.add("Golf coach - " +  coachName + "(Low Rate)");
				result.add(Constant.GSSLO_RATE_ITEM_PREFIX);
			}
		//PTG - part time golf trainer;
//		}else if(Constant.StaffType.PTG.toString().equals(staffDto.getStaffType())){
//			description = "Full time golf private coach "+coachName+" "+ type + "rate";
//			result.add(description);
//			if("high".equals(type)){
//				result.add(Constant.GSSHI_RATE_ITEM_PREFIX);
//			}else{
//				result.add(Constant.GSSLO_RATE_ITEM_PREFIX);
//			}
		//FTR- Full time Tennis Trainer Coach
		}else if(Constant.StaffType.FTR.toString().equals(staffDto.getStaffType())){
			//description = "Full time Tennis private coach "+coachName+" "+ type + "rate";
			if("high".equals(type)){
				result.add("Tennis coach - " +  coachName + "(High Rate)");
				result.add(Constant.TSSHI_RATE_ITEM_PREFIX);
			}else{
				result.add("Tennis coach - " +  coachName + "(Low Rate)");
				result.add(Constant.TSSLO_RATE_ITEM_PREFIX);
			}
		//PTR-part time tennis trainer/coach	
//		}else if(Constant.StaffType.PTR.toString().equals(staffDto.getStaffType())){
//			description = "Full time Tennis private coach "+coachName+" "+ type + "rate";
//			result.add(description);
//			if("high".equals(type)){
//				result.add(Constant.TSSHI_RATE_ITEM_PREFIX);
//			}else{
//				result.add(Constant.TSSLO_RATE_ITEM_PREFIX);
//			}
		}else{
			throw new GTACommonException(GTAError.CoachMgrError.Coach_Type_Wrong);
		}
		return result;
	}

	private int getDayOfWeek(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	/**   
	* @author: Ray_Liang
	* @since: Aug 4, 2015
	* 
	* @description
	* the duration between start and end SHOULD BE WITHIN 1 WEEK 
	* the @param start, @param end and @param isLoadRsv are available only if @isCustomized is true
	*/  
	    
	private CoachRosterInfo loadRoster(String coachId, Date start, Date end, boolean isCustomized, boolean isLoadRsv) {
		List<StaffCoachRoaster> roster = staffCoachRoasterDao.loadRawPresentRoster(coachId);
		List<StaffTimeslot> timeslots = null;
		//use customized roster to override present roster
		if(isCustomized){
			if(isLoadRsv){
				timeslots = staffTimeslotDao.loadStaffTimeslot(coachId, start, end);
			}
			Collections.sort(roster);
			List<StaffCoachRoaster> customizedRoster = staffCoachRoasterDao.loadRawCustomizedRoster(coachId,start,end);
			for(StaffCoachRoaster customizedItem : customizedRoster){
				staffCoachRoasterDao.evict(customizedItem);
				customizedItem.setWeekDay(String.valueOf(getDayOfWeek(customizedItem.getOnDate())));
				customizedItem.setOnDate(null);
				int index = Collections.binarySearch(roster, customizedItem);
				if(index >= 0){
					roster.remove(index);
					roster.add(index,customizedItem);
				}
			}
		}
		
		//convert List<StaffCoachRoaster> to CoachRosterInfo
		HashMap<String, List<StaffCoachRoaster>> counter = new HashMap<String, List<StaffCoachRoaster>>();
		
		for(StaffCoachRoaster rosterItem : roster){
			if(counter.containsKey(rosterItem.getWeekDay())){
				counter.get(rosterItem.getWeekDay()).add(rosterItem);
			}else
			{
				List<StaffCoachRoaster> weekdayRoster = new ArrayList<StaffCoachRoaster>();
				weekdayRoster.add(rosterItem);
				counter.put(rosterItem.getWeekDay(), weekdayRoster);
			}
		}
		
		List<CoachDayRateInfo> dayRateList = new ArrayList<CoachDayRateInfo>();
		for(String weekday : counter.keySet()){
			CoachDayRateInfo dayRate = new CoachDayRateInfo();
			dayRate.setWeekDay(Integer.parseInt(weekday));
			
			List<TimeSliceRate> sliceList = new ArrayList<TimeSliceRate>();
			for(StaffCoachRoaster weekdayRosterItem : counter.get(weekday)){
				TimeSliceRate sliceRate = new TimeSliceRate();
				sliceRate.setBeginTime(weekdayRosterItem.getBeginTime());
				
				String rateType = weekdayRosterItem.getRateType();
				
				if(!CollectionUtils.isEmpty(timeslots)){
					for(StaffTimeslot slot : timeslots){
						if(Integer.parseInt(DateFormatUtils.format(slot.getBeginDatetime(), "HH")) == weekdayRosterItem.getBeginTime().intValue()
								&& getDayOfWeek(slot.getBeginDatetime()) == Integer.parseInt(weekdayRosterItem.getWeekDay())){
//							rateType = "RE";
							sliceRate.setStatus("RE");
							break;
						}
					}
				}
				
				if("Y".equals(weekdayRosterItem.getOffDuty())){
					rateType = Constant.RateType.OFF.toString();
				}
				
				if("B".equals(weekdayRosterItem.getOffDuty())){
					rateType = Constant.RateType.BLK.toString();
				}
				
				sliceRate.setRateType(rateType);
				sliceList.add(sliceRate);
			}
			dayRate.setRateList(sliceList);
			dayRateList.add(dayRate);
		}
		CoachRosterInfo rosterInfo = new CoachRosterInfo();
		rosterInfo.setDayRateList(dayRateList);
		rosterInfo.setCoachId(coachId);
		return rosterInfo;
	}
	
	@Override
	@Transactional
	public CoachRosterInfo loadPresentRoster(final String coachId) {
		CallBackExecutor exe = new CallBackExecutor(CoachManagementServiceImpl.class);
		return (CoachRosterInfo)exe.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				CoachRosterInfo info =  loadRoster(coachId, null, null, false, false);
				StaffDto staffDto = userMasterDao.getStaffDto(info.getCoachId());
				setPrice4Coach(coachId, info);
				info.setCoachName(staffDto.getStaffName());
				return info;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CoachMgrError.FAIL_LOAD_PRESENT_ROSTER);
			}
			
		});
	}
	
	private String translateRateType(String rateType){
		if(rateType == null)
			return Constant.RateType.OFF.toString();
		else
			return rateType;
	}
	
	@Override
	@Transactional
	public boolean saveCustomizedRoster(final String userId, final CoachRosterInfo rosterInfo) {

		CallBackExecutor exe = new CallBackExecutor(CoachManagementServiceImpl.class);
		return (Boolean)exe.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				List<CoachDayRateInfo> dayRateList = rosterInfo.getDayRateList();
				List<StaffCoachRoaster> rawPresentRoster = staffCoachRoasterDao.loadRawPresentRoster(rosterInfo.getCoachId());
				List<StaffCoachRoaster> rawCustomizedRoster = staffCoachRoasterDao.loadRawCustomizedRoster(rosterInfo.getCoachId());
				Collections.sort(rawPresentRoster);
				Collections.sort(rawCustomizedRoster);
				Date current = new Date();
				for(CoachDayRateInfo dayRateInfo : dayRateList){
					List<TimeSliceRate> slices = dayRateInfo.getRateList();
					for(TimeSliceRate slice : slices){
						StaffCoachRoaster roster = new StaffCoachRoaster();
						roster.setCoachId(rosterInfo.getCoachId());
						roster.setWeekDay(String.valueOf(dayRateInfo.getWeekDay()));
						roster.setOnDate(dayRateInfo.getDate());
						roster.setBeginTime(slice.getBeginTime());
						roster.setEndTime(slice.getBeginTime() + 1);
						roster.setCreateDate(current);
						roster.setCreateBy(userId);
		
						String rateType = slice.getRateType();
						int customizedIndex = Collections.binarySearch(rawCustomizedRoster, roster);
						if(customizedIndex>=0){//Update the customized roster

							StaffCoachRoaster rawCustomizedRosterItem = rawCustomizedRoster.get(customizedIndex);

							// If there is a booking record on specify date, it shall not allow to change coach status
							// to be off-duty or change price
							Date beginTime = dayRateInfo.getDate();
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(beginTime);
							calendar.add(Calendar.DATE, 1);
							Date endTime = calendar.getTime();

							String preRateType = rawCustomizedRosterItem.getRateType();
							List<StaffTimeslot> timeslots = staffTimeslotDao.loadStaffTimeslot(rosterInfo.getCoachId(),
									beginTime, endTime);
							if (!CollectionUtils.isEmpty(timeslots)) {
								for (StaffTimeslot slot : timeslots) {
									if (Integer.parseInt(DateFormatUtils.format(slot.getBeginDatetime(), "HH")) == slice
											.getBeginTime().intValue()
											&& getDayOfWeek(slot.getBeginDatetime()) == dayRateInfo.getWeekDay()
													.intValue()
											&& !rateType.equals(preRateType)) {
										throw new GTACommonException(
												GTAError.CoachMgrError.FAIL_SAVE_CUSTOMIZED_ROSTER);
									}
								}
							}

							setRateType(rawCustomizedRosterItem, rateType);
							staffCoachRoasterDao.update(rawCustomizedRosterItem);
						}else{
							int index = Collections.binarySearch(rawPresentRoster, roster);
							if(index >= 0){
								StaffCoachRoaster rawRosterItem = rawPresentRoster.get(index);
								if(!slice.getRateType().equals(translateRateType(rawRosterItem.getRateType()))){
									setRateType(roster, rateType);
									roster.setWeekDay(null);
									staffCoachRoasterDao.save(roster);
								}
							}
						}
					}
				}
				return Boolean.TRUE;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CoachMgrError.FAIL_SAVE_CUSTOMIZED_ROSTER);
			}
		});
	
	}

	@Override
	@Transactional
	public CoachRosterInfo loadCustomizedRoster(final String coachId, final Date start, final Date end) {
		CallBackExecutor exe = new CallBackExecutor(CoachManagementServiceImpl.class);
		return (CoachRosterInfo)exe.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				CoachRosterInfo info =  loadRoster(coachId, start, end, true, true);
				StaffDto staffDto = userMasterDao.getStaffDto(info.getCoachId());
				setPrice4Coach(coachId, info);
				info.setCoachName(staffDto.getStaffName());
				return info;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CoachMgrError.FAIL_LOAD_CUSTOMIZED_ROSTER);
			}
			
		});
	}
	
	private List<Date[]> getSliceAsWeek(Date start, Date end){
		Calendar startCal = Calendar.getInstance();
		startCal.setTime(new DateTime(start).millisOfDay().withMinimumValue().toLocalDateTime().toDate());
		
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(new DateTime(end).millisOfDay().withMaximumValue().toLocalDateTime().toDate());
		
		
		List<Date[]> result = new ArrayList<Date[]>();
		
		while(startCal.compareTo(endCal) <= 0){
			Calendar originStart = (Calendar)startCal.clone();
			
			int weekday = startCal.get(Calendar.DAY_OF_WEEK);
			int remainDay = 7 - weekday;
			
			startCal.add(Calendar.DAY_OF_WEEK, remainDay);
			
			Date[] headTail = new Date[2];
			headTail[0] = originStart.getTime();
			if (startCal.compareTo(endCal) <= 0){
				headTail[1] = new DateTime(startCal.getTime()).millisOfDay().withMaximumValue().toLocalDateTime().toDate();
			}else{
				headTail[1] = new DateTime(endCal.getTime()).millisOfDay().withMaximumValue().toLocalDateTime().toDate();
			}
			startCal.add(Calendar.DAY_OF_WEEK, 1);
			result.add(headTail);
		}
		return result;
		
	}
	
	@Override
	@Transactional
	public List<CoachRosterInfo> loadMultipleWeekCustomizedRoster(
			String coachId, Date start, Date end) {
		List<CoachRosterInfo> list = new ArrayList<CoachRosterInfo>();
		
		List<Date[]> datePairList = getSliceAsWeek(start, end);
		for(Date[] datePair : datePairList){
			Calendar startCal = Calendar.getInstance();
			startCal.setTime(datePair[0]);
			
			Calendar endCal = Calendar.getInstance();
			endCal.setTime(datePair[1]);
			
			int startWeekday = startCal.get(Calendar.DAY_OF_WEEK);
			int endWeekday = endCal.get(Calendar.DAY_OF_WEEK);
			
			Map<Integer, Date> weekDayMap = new HashMap<Integer, Date>();
			for(int i=startWeekday ; i <= endWeekday ; i++){
				weekDayMap.put(i, startCal.getTime());
				startCal.add(Calendar.DAY_OF_WEEK, 1);
			}
			
			CoachRosterInfo info = loadRoster(coachId, datePair[0], datePair[1], true, true);
			
			for(CoachDayRateInfo rate : info.getDayRateList()){
				Date date = weekDayMap.get(rate.getWeekDay());
				if(date != null){
					rate.setDate(DateFormatUtils.format(date, "yyyy-MM-dd"));
				}
				int dutyHours = 0;
				for(TimeSliceRate slice : rate.getRateList()){
					if(StringUtils.equalsIgnoreCase("RE", slice.getStatus())){
						dutyHours ++;
					}
				}
				
				rate.setDutyHours(dutyHours);
				rate.setHasDuty(dutyHours > 0 ? true : false);
    		}
			
			list.add(info);
		}
		return list;
	}
	
	private void setPrice4Coach(String coachId,
			CoachRosterInfo info) {
		List<StaffCoachRatePos>  staffCoachRatePos = coachRatePosDao.getStaffCoachRatePosByUserId(coachId);
//		if(null==staffCoachRatePos || 0==staffCoachRatePos.size()){
//			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE);
//		}
		if(null!=staffCoachRatePos && staffCoachRatePos.size()>0){
			for(StaffCoachRatePos pos : staffCoachRatePos){
				if(Constant.RateType.HI.toString().equals(pos.getRateType())){
					PosServiceItemPrice highPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, pos.getPosItemNo());
					info.setHighRatePrice(highPrice.getItemPrice());
					info.setHighPriceItemNo(pos.getPosItemNo());
				}else {
					PosServiceItemPrice lowPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, pos.getPosItemNo());
					info.setLowRatePrice(lowPrice.getItemPrice());
					info.setLowPriceItemNo(pos.getPosItemNo());
				}
			}
		}
	}

	@Override
	@Transactional
	public List<AvailableCoachDto> loadAvailableCoach(Date date, String timeslot, String staffType) {
		try{
			return staffCoachInfoDao.getAvailableCoach(date, timeslot, staffType);
		}catch(Exception e){
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{e.getMessage()});
		}
	}

	@Override
	@Transactional
	public List<AvailableCoachDto> loadAvailableCoach(final Date date,final String timeslot,final String staffType,final boolean createOffer,final Long originResvId) {
		CallBackExecutor exe = new CallBackExecutor(CoachManagementServiceImpl.class);
		Object result = exe.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				List<AvailableCoachDto> availableCoaches = loadAvailableCoach(date, timeslot, staffType);
				
				if(createOffer){
					if(originResvId == null){
						throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{"Unknown coach for maintenance offer"});
					}
					MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(originResvId);
					List<StaffTimeslot> timeslots = booking.getStaffTimeslots();
					if(timeslots.size() == 0){
						throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{"Unknown coach for maintenance offer"});
					}else{
						String coachId = timeslots.get(0).getStaffUserId();
						
						List<AvailableCoachDto> filteredCoaches = new ArrayList<AvailableCoachDto>();
						for(AvailableCoachDto dto : availableCoaches){
							if(StringUtils.equals(dto.getCoachId(), coachId)){
								filteredCoaches.add(dto);
								break;
							}
						}
						return filteredCoaches;
					}
				}
				
				
				return availableCoaches;
			}
		});
		
		return (List<AvailableCoachDto>)result;
		
	}

	@Override
	@Transactional
	public PrivateCoachInfoDto loadCoachProfile(String coachId) {
		try{
			return staffCoachInfoDao.getCoachProfile(coachId);
		}catch(Exception e){
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{e.getMessage()});
		}
	}

	private List<Date> getDatesByWeekday(int weekday, Date begin, Date end){
		List<Date> dateList = new ArrayList<Date>();
		Calendar beginCal = Calendar.getInstance();
		beginCal.setTime(begin);
		
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(end);
		
		int min = beginCal.get(Calendar.DAY_OF_MONTH);
		int max = endCal.get(Calendar.DAY_OF_MONTH);
		 
		
		int day = min;
		while(day <= max){
			if(beginCal.get(Calendar.DAY_OF_WEEK)==weekday){
				dateList.add(beginCal.getTime());
			}
			day++;
			beginCal.set(Calendar.DAY_OF_MONTH, day);
		}
		return dateList;
	}
	
	@Override
	@Transactional
	public List<AvailableDateDto> loadInavailableRoster(String coachId, Date begin, Date end) {
		return new ArrayList<AvailableDateDto>();
		
		/**
		 * 
		 *
		 * Will be enabled in the future, DON'T remove
		List<StaffCoachRoaster> invalidRoster = staffCoachRoasterDao.loadOffdutyRoster(coachId, begin, end);
		List<Date> invalidDates = staffTimeslotDao.loadStaffOffdutyDates(coachId, begin, end);
		Map<String, Set<Integer>> weekdaysCounter = new HashMap<String, Set<Integer>>();
		Map<Date, Set<Integer>> datesCounter = new HashMap<Date, Set<Integer>>();
		
		for (StaffCoachRoaster roster : invalidRoster){
			if(roster.getOnDate() == null){//for week days
				if(weekdaysCounter.get(roster.getWeekDay()) == null){
					Set<Integer> hours = new HashSet<Integer>();
					hours.add(roster.getBeginTime());
					weekdaysCounter.put(roster.getWeekDay(), hours);
				}else{
					weekdaysCounter.get(roster.getWeekDay()).add(roster.getBeginTime());
				}
			}else{
				if(datesCounter.get(roster.getOnDate()) == null){
					Set<Integer> hours = new HashSet<Integer>();
					hours.add(roster.getBeginTime());
					datesCounter.put(roster.getOnDate(), hours);
				}else{
					datesCounter.get(roster.getOnDate()).add(roster.getBeginTime());
				}
			}
		}
		Map<Object, Set<Integer>> weekOrDatesCounter = new HashMap<Object, Set<Integer>>();
		weekOrDatesCounter.putAll(weekdaysCounter);
		weekOrDatesCounter.putAll(datesCounter);
		
		Set<AvailableDateDto> invalidDateDtos = new HashSet<AvailableDateDto>();
		
		for(Object weekOrDate: weekOrDatesCounter.keySet()){
			Set<Integer> timeslot = weekOrDatesCounter.get(weekOrDate);
			if(timeslot.size() == 16){
				if(weekOrDate instanceof String){
					List<Date> dates = getDatesByWeekday(Integer.parseInt((String)weekOrDate),begin, end);
					for(Date date:dates){
						AvailableDateDto dto = new AvailableDateDto();
						dto.setCoachId(coachId);
						dto.setDate(DateFormatUtils.format(date, "yyyy-MM-dd"));
						invalidDateDtos.add(dto);
					}
				}
				if(weekOrDate instanceof Date){
					AvailableDateDto dto = new AvailableDateDto();
					dto.setCoachId(coachId);
					dto.setDate(DateFormatUtils.format((Date)weekOrDate, "yyyy-MM-dd"));
					invalidDateDtos.add(dto);
				}
			}
		}
		for (Date off: invalidDates){
			AvailableDateDto dto = new AvailableDateDto();
			dto.setCoachId(coachId);
			dto.setDate(DateFormatUtils.format(off, "yyyy-MM-dd"));
			invalidDateDtos.add(dto);
		}*/
		//return new ArrayList<AvailableDateDto>(invalidDateDtos); //Will be enabled in the future, don't remove
		
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public ResponseResult calcBookingPrice(MemberFacilityBookingDto booking)
	{
		
		if (StringUtils.isEmpty(booking.getCoachId())
				|| StringUtils.isEmpty(booking.getBookingDate())
				|| null == booking.getBeginTime()
				|| null == booking.getEndTime()
				|| null == booking.getCustomerId())
		{
			throw new GTACommonException(GTAError.CommomError.DATA_MISSING, new String[]{"Booking information"});
		}
		
		Date bookingDate = DateConvertUtil.parseString2Date(booking.getBookingDate(), "yyyy-MM-dd");
		
		CoachRosterInfo roaster = staffCoachRoasterDao.loadCustomizedRoster(booking.getCoachId(), bookingDate, bookingDate);
		setPrice4Coach(booking.getCoachId(), roaster);
		
		BigDecimal highPrice = roaster.getHighRatePrice();
		BigDecimal lowPrice = roaster.getLowRatePrice();
		
		if (null == highPrice && null == lowPrice)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.PRICE_NOT_SET);
		}
		
		booking.setHighPriceItemNo(roaster.getHighPriceItemNo());
		booking.setLowPriceItemNo(roaster.getLowPriceItemNo());
		
		
		List<CoachDayRateInfo> dayRate = roaster.getDayRateList();
		CoachDayRateInfo rateInfo = null;
		for (CoachDayRateInfo tmpRateInfo : dayRate)
		{
			if (tmpRateInfo.getWeekDay().equals(CommUtil.getDayOfWeek(bookingDate)))
			{
				rateInfo = tmpRateInfo;
				break;
			}
		}
		
		BigDecimal totalPrice = BigDecimal.ZERO;
		Map<String,Integer> itemNoMap = new HashMap<String, Integer>();
		for (TimeSliceRate timeRate : rateInfo.getRateList())
		{
			if (!(timeRate.getBeginTime() >= booking.getBeginTime() && timeRate.getBeginTime() <= booking.getEndTime())) continue;
			if (timeRate.getRateType().equals(Constant.RateType.OFF)) continue;
			BigDecimal price = BigDecimal.ZERO;
			if(timeRate.getRateType().equals(Constant.RateType.HI.getName())){
				price = highPrice;
				if(null!=itemNoMap.get(roaster.getHighPriceItemNo())){
					itemNoMap.put(roaster.getHighPriceItemNo(), itemNoMap.get(roaster.getHighPriceItemNo())+1);
				}else{
					itemNoMap.put(roaster.getHighPriceItemNo(),1);
				}
			}else {
				price = lowPrice;
				if(null!=itemNoMap.get(roaster.getLowPriceItemNo())){
					itemNoMap.put(roaster.getLowPriceItemNo(), itemNoMap.get(roaster.getLowPriceItemNo())+1);
				}else{
					itemNoMap.put(roaster.getLowPriceItemNo(),1);
				}
			}
			
			totalPrice = totalPrice.add(price);
		}
		booking.setItemNoMap(itemNoMap);
		if(booking.isCreateOffer()){
			booking.setTotalPrice(new BigDecimal(0));
		}else{
			booking.setTotalPrice(totalPrice);
		}
		
		Member member = memberDao.getMemberById(booking.getCustomerId());
		
		
		CustomerProfile p = customerProfileDao.getById(booking.getCustomerId());
		booking.setCustomerName(p.getSalutation() + " " + p.getGivenName() + " " + p.getSurname());
		
		MemberCashvalue memberCashvalue = null;
		booking.setAcademyNo(member.getAcademyNo());
		String memberType = member.getMemberType();
		if ((memberType.equals(MemberType.CDM.getType()) || memberType.equals(MemberType.IDM.getType())) && (null != member.getSuperiorMemberId() && member.getSuperiorMemberId() > 0))
		{
			memberCashvalue = cashValueDao.getMemberCashvalueById(member.getSuperiorMemberId());
		}
		else
		{
			memberCashvalue = cashValueDao.getMemberCashvalueById(member.getCustomerId());
		}
		
		booking.setAvailableBalance(null != memberCashvalue ? memberCashvalue.getAvailableBalance() : BigDecimal.ZERO);
		
		String limitType = "";
		MemberLimitRule limitRule = null;
		if (memberType.equals(MemberType.CPM.getType()) || memberType.equals(MemberType.IPM.getType()))
		{
			booking.setSpendingLimit(null);
			booking.setSpendingLimitUnit(null);
			limitType = LimitType.CR.getName();
			limitRule = limitRuleDao.getEffectiveMemberLimitRule(booking.getCustomerId(), limitType);
			AssertIsNotNull(limitRule, "Member Limit Rule");
			booking.setPersonalCreditLimit(limitRule.getNumValue());
		}
		else
		{
			Member primaryMember = memberDao.getMemberByCustomerId(member.getSuperiorMemberId());
			AssertIsNotNull(primaryMember, "Primary Member");
			MemberLimitRule memberLimitRule = limitRuleDao.getEffectiveMemberLimitRule(primaryMember.getCustomerId(), LimitType.CR.getName());
			MemberLimitRule dependentMemberLimitRule = limitRuleDao.getEffectiveMemberLimitRule(member.getCustomerId(), LimitType.TRN.getName());
			AssertIsNotNull(memberLimitRule, "Primary Member Limit Rule");
			AssertIsNotNull(dependentMemberLimitRule, "Dependent Member Limit Rule");
			
			booking.setPersonalCreditLimit(memberLimitRule.getNumValue());
			booking.setSpendingLimit(dependentMemberLimitRule.getNumValue());
			booking.setSpendingLimitUnit(dependentMemberLimitRule.getLimitUnit());
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, booking);
		return responseResult;
	}
	
	private boolean AssertIsNotNull(Object T, String param)
	{
		if (null == T)
		{
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[] { param });
		}

		return true;
	}

	@Override
	@Transactional
	public void changeAttendanceStatus(final Long resvId, final String attendanceStatus,
			final String loginUser) {
		
		CallBackExecutor exe = new CallBackExecutor(CoachManagementServiceImpl.class);
		exe.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				List<MemberReservedFacility> reservedFacilities =  memberReservedFacilityDao.getMemberReservedFacilityList(resvId);
				Date now = new Date();
				for(MemberReservedFacility reservedFacility : reservedFacilities){
					List<MemberFacilityAttendance> attendances = memberFacilityAttendanceDao.getMemberFacilityAttendanceList(reservedFacility.getFacilityTimeslotId(), 0);
					if(attendances != null){
						for(MemberFacilityAttendance attendance : attendances){
							if(StringUtils.equalsIgnoreCase("ATN", attendanceStatus)){
								attendance.setAttendTime(new Timestamp(now.getTime()));
							}else{
								attendance.setAttendTime(null);
							}
							
						}
					}
				}
				
				
				return null;
			}
			
			
		});
		
		
		
	}
}
