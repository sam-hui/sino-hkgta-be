package com.sinodynamic.hkgta.service.ical;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dto.ical.PublicHolidayDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.ical.PublicHoliday;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;

@Service
public class GtaPublicHolidayServiceImpl  extends ServiceBase<PublicHoliday> implements GtaPublicHolidayService{

	@Autowired
	private GtaPublicHolidayDao gtaPublicHolidayDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	MessageTemplate template;
	
	
	@Transactional
	public PublicHoliday viewHolidayDetail(Long holiday_id) throws Exception {
		return gtaPublicHolidayDao.viewHolidayDetail(holiday_id);
	}

	@Transactional
	public void deleteById(Long holiday_id) throws Exception {
		gtaPublicHolidayDao.deleteById(holiday_id);
		
	}

	@Transactional
	public void saveHoliday(List<PublicHoliday> holidays) throws Exception {
		gtaPublicHolidayDao.saveHoliday(holidays);	
	}

	@Transactional
	public List<PublicHolidayDto> getHolidayList(
			PublicHolidayDto publicHolidayDto) throws Exception {
		 return null; 	
	}

	@Transactional
	public void updateHoliday(PublicHoliday holiday) throws Exception {
		gtaPublicHolidayDao.update(holiday);
	}

	@Transactional
	public void sendPublicHolidayEmail(){
		if (template == null) {
			template = messageTemplateDao.getTemplateByFunctionId("public_holiday");
		}
		
		List<CustomerEmailContent> list = this.getAdminMailList();
		
		if (template != null) {
			String subject = template.getMessageSubject();
			String content = template.getContentHtml();
			Date date = new Date();
			for (CustomerEmailContent customerEmailContent : list){
			    customerEmailContent.setContent(content);
			    customerEmailContent.setSendDate(date);
			    customerEmailContent.setSubject(subject);
			    customerEmailContentDao.save(customerEmailContent);
			    mailThreadService.sendWithResponse(customerEmailContent, null, null, null);
			}
		}
	}
	
	private List<CustomerEmailContent> getAdminMailList(){
		String sql = "SELECT staff_profile.contact_email as recipientEmail FROM staff_profile,user_role,role_master"
		+ " WHERE staff_profile.user_id = user_role.user_id"
		+ " AND role_master.role_id = user_role.role_id"
		+ " AND role_master.role_name in ('admin', 'SystemAdmin')";

		List<CustomerEmailContent> list = customerEmailContentDao
				.getDtoBySql(sql,new ArrayList(),CustomerEmailContent.class);
		
		return list;
	}
}
