package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class ChangeMemberStatusServiceImpl extends ServiceBase<Member> implements ChangeMemberStatusService {
	
	@Autowired 
	private MemberDao memberDao;
	
	
	@Transactional
	public ResponseResult updateMemberStatus(String status, Long customerId){
		
		//ResponseResult responseResult = new ResponseResult();
		if(memberDao.updateStatus(status, customerId)){
			//responseResult.setErrorMessageEN("");
			//responseResult.setReturnCode("0");
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			//responseResult.setErrorMessageEN("Update Failed!");
			//responseResult.setReturnCode("1");
			responseResult.initResult(GTAError.MemberError.MEMBER_UPDATE_FAILED);
		}
		return responseResult;
		 
	}

}
