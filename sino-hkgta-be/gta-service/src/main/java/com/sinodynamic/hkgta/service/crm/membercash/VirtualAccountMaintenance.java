package com.sinodynamic.hkgta.service.crm.membercash;


public class VirtualAccountMaintenance {
	
	public static final String[] COLUMN_ORDER = {"recordCode", "clientId", "physicalAccount", "virtualAccount",
		"payerName", "payerNameLocal", "payerNumber", "payerReference", "emailId", "mobileNumber", "action"};
	
	private String recordCode;
	private Long clientId;
	private String physicalAccount;
	private String virtualAccount;
	private String payerName;
	private String payerNameLocal;
	private String payerNumber;
	private String payerReference;
	private String emailId;
	private String mobileNumber;
	private String action;

	public String getRecordCode() {
		return recordCode;
	}

	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getPhysicalAccount() {
		return physicalAccount;
	}

	public void setPhysicalAccount(String physicalAccount) {
		this.physicalAccount = physicalAccount;
	}

	public String getVirtualAccount() {
		return virtualAccount;
	}

	public void setVirtualAccount(String virtualAccount) {
		this.virtualAccount = virtualAccount;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPayerNameLocal() {
		return payerNameLocal;
	}

	public void setPayerNameLocal(String payerNameLocal) {
		this.payerNameLocal = payerNameLocal;
	}

	public String getPayerNumber() {
		return payerNumber;
	}

	public void setPayerNumber(String payerNumber) {
		this.payerNumber = payerNumber;
	}

	public String getPayerReference() {
		return payerReference;
	}

	public void setPayerReference(String payerReference) {
		this.payerReference = payerReference;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
