package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberTransactionService extends IServiceBase<MemberCashvalue>{
	
	public ResponseResult getCurrentBalance(Long customerId);
	
	public ResponseResult topupByCashCard(TopUpDto topUpDto);

	public ResponseResult getTransactionList(Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String clientType, AdvanceQueryDto filters);

	List<AdvanceQueryConditionDto> transactionListAdvanceSearch();
	
	public ResponseResult getTopupHistory(Long customerID, String pageNumber,
			String pageSize, String sortBy, String isAscending, AdvanceQueryDto filters);

	List<AdvanceQueryConditionDto> topupHistoryAdvanceSearch();
	
	public ResponseResult topupByCard(TopUpDto topUpDto);

	public ResponseResult handlePosPaymentResult(PosResponse posResponse);
	
	public ResponseResult getMembersOverviewList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType);
	
	public List<AdvanceQueryConditionDto> assembleQueryConditions();

	public void sentTopupEmail(TopUpDto topUpDto);

	public void saveTopupHistory(TopUpDto topUpDto);

	public Data getRecentServedMemberList(ListPage<Member> page, String sortBy, String isAscending, Long[] customerId);

	public ResponseResult getTotalAmount(Long customerId, String transactionType);
		
}
