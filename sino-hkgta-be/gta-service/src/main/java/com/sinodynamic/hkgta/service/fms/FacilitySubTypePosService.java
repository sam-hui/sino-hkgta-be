package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.dto.fms.FacilitySubtypesDto;
import com.sinodynamic.hkgta.entity.fms.FacilitySubTypePos;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilitySubTypePosService extends IServiceBase<FacilitySubTypePos> {

	public void saveFacilitySubTypePos(FacilitySubtypesDto facilitySubtypesDto);
	
	FacilitySubTypePos getFacilitySubTypePos(String subtypeId,String ageRangeCode,String rateType);
}
