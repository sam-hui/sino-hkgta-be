package com.sinodynamic.hkgta.service.mms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryDto;
import com.sinodynamic.hkgta.dto.mms.SpaCenterInfoDto;
import com.sinodynamic.hkgta.dto.mms.SpaPicPathDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatItemDto;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistRequestType;
import com.sinodynamic.hkgta.integration.spa.response.Therapist;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MMSService {

	public ResponseResult queryTopCategories() throws Exception;
	
	public ResponseResult getLocalTopCategory(String device) throws Exception;
	
	public ResponseResult getCenterTherapists(Date requestDate) throws Exception;
	
	public ResponseResult therapistsAvailableForService(Date requestDate, String serviceCode, TherapistRequestType type) throws Exception;
	
	public ResponseResult getAvailableTimeSlots(GetAvailableTimeSlotsRequest requset) throws Exception;
	public ResponseResult getFormatAvailableTimeSlots(GetAvailableTimeSlotsRequest request)throws Exception;
	public ResponseResult queryServiceList(String categoryId) throws Exception;
	
	public ResponseResult bookAppointment(SpaAppointmentDto spaAppointmentDto) throws Exception;
	
	public ResponseResult queryServiceDetails(String serviceId) throws Exception;
	
	public ResponseResult getSubcategories(String categoryId) throws Exception;
	public ResponseResult getAppointmentDetail(String invoiceNo)throws Exception;
	public ResponseResult canCelAppointment(String invoiceNo, String userId) throws Exception;
	public ResponseResult canCelAppointmentService(String appointmentId, String userId) throws Exception;
	
	public ResponseResult payment(MMSPaymentDto paymentDto) throws Exception;
	
	public Therapist getTherapistInfo(String therapistCode, Date requestDate);
	
	public ResponseResult getLatestAppointmentDetail(String invoiceNo)throws Exception;
	
	public ResponseResult getMemberInfo(String customerId) throws Exception;
	
	public String aesEncode(String res, String key);
	public String aesDecode(String res, String key);
	
	public SpaCenterInfoDto getSpaCenterInfo();
	public ResponseResult updateSpaCenterInfo(SpaCenterInfoDto spaCenterInfoDto, String userId);
	
	public ResponseResult updateSpaCategoryPic(List<SpaPicPathDto> dtoList, String userId);
	public List<SpaCategoryDto> getSpaCategoryList();

	public void updateSpaRetreat(SpaRetreatDto dto, String userId);

	public void updateSpaRetreatItem(SpaRetreatItemDto dto, String userId);
	
	public String getSpaRetreatListSql();
	public String getSpaRetreatItemListSql(Long retId);
	
	public void switchRetreatOrder(Long sourceRetId, String moveType, String userId);
	public void sentPaymentReceipt(MMSPaymentDto dto, Boolean isSynchronized) throws Exception;
	public void sentMessage(List<SpaAppointmentRec> appointmentList, SceneType messageType) throws Exception;
	public ResponseResult sendReceipt(String invoiceNo)throws Exception;
	public Long getLatestUpdatedPicId();
}
