package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotLogDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityTimeslotLogService extends IServiceBase<FacilityTimeslotLog> {

	public List<FacilityTimeslotLogDto> getLogsByFacilityNo(String facilityType, Long facilityNo);

}
