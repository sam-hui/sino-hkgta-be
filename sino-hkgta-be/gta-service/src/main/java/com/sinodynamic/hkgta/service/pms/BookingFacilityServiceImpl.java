package com.sinodynamic.hkgta.service.pms;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityMasterDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedFacilityDao;
import com.sinodynamic.hkgta.dao.pms.PMSRequestProcessorDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttrPK;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;
import com.sinodynamic.hkgta.entity.fms.TLock;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.MemberFacilityTypeBookingStatus;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;

@Service
public class BookingFacilityServiceImpl  extends ServiceBase implements BookingFacilityService{
		
	
	@Autowired
	private FacilityTimeslotDao facilityTimeslotDao;
	
	@Autowired
	private FacilityMasterDao  facilityMasterDao;
	
	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	
	@Autowired
	private MemberReservedFacilityDao memberReservedFacilityDao;
	
	@Autowired
	private PMSRequestProcessorDao pmsRequestProcessorDao;
	
	@Autowired
	private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	private static Logger logger = LoggerFactory.getLogger(BookingFacilityServiceImpl.class);


	@Transactional
	public Long bookTimeslot(Long customerId,Date startTime,Date endTime,String facilityType,String attributeId,String status){
		//DistributedLock lock =null;
		Long timeslotId = null;
		//saveorupdate()
//		String from = this.appProps.getProperty("giftBundle.fromFacilityNo");
//		String end = this.appProps.getProperty("giftBundle.endFacilityNo");
//		int fromFacilityNo = Integer.parseInt(from);
//		int endFacilityNo = Integer.parseInt(end);
//		int availableFacilitys = pmsRequestProcessorDao.countSpecifiedFacility(facilityType, attributeId, status,fromFacilityNo,endFacilityNo);
		int availableFacilitys = pmsRequestProcessorDao.countSpecifiedFacility(facilityType, Constant.ZONE_PRACTICE, status);
//		//pmsRequestProcessorDao.getLock();
		System.out.println("Thread Name:" +  Thread.currentThread().getName()  + " get Lock," + "startTime=" + startTime +",endTime=" + endTime);
//		List<Integer> facilityNos = pmsRequestProcessorDao.queryAllAvailableSpecifiedFacilityByPeriod(startTime, endTime,facilityType,attributeId,status,fromFacilityNo,endFacilityNo);
		System.out.println("Thread Name:" +  Thread.currentThread().getName()  + " get Lock," + "startTime=" + startTime +",endTime=" + endTime);
		List<Integer> facilityNos = pmsRequestProcessorDao.queryAllAvailableSpecifiedFacilityByPeriod(startTime, endTime,facilityType,Constant.ZONE_PRACTICE,status);

		if (facilityNos.size() == 0) {
			// throw new
			// NoTimeslotException("Can't find available right-hand golf-bay in: ["
			// + startTime +"," + endTime +"]");
			logger.debug("Can't find available right-hand golf-bay in: [" + startTime + "," + endTime + "]");
		} else {
			for (int i = 0; i < facilityNos.size(); i++) {
				// check in one facility
				Integer facilityNo = facilityNos.get(i);

				int reservedFacilitys = pmsRequestProcessorDao.countReservedSpecifiedFacility(startTime, endTime,
								facilityType, attributeId);
				// check whether there is timeslot available
				if (reservedFacilitys >= availableFacilitys) {
					throw new NoTimeslotException(
							"Can't find available right-hand golf-bay in: ["
									+ startTime + "," + endTime + "]");
				} else {
					// check whether the facility is occupied at this time
					int count = pmsRequestProcessorDao.isoccupied(facilityNo,startTime, endTime);
					boolean flag = count > 0;
					if (flag) {
						continue;
					} else {
						FacilityTimeslot ft = new FacilityTimeslot();
						ft.setBeginDatetime(startTime);
						ft.setEndDatetime(endTime);
						FacilityMaster fm = facilityMasterDao.get(FacilityMaster.class, Long.parseLong(facilityNo.toString()));
						ft.setFacilityMaster(fm);
						ft.setStatus("RS");
						Timestamp now = new Timestamp(new Date().getTime());
						ft.setCreateDate(now);
						ft.setUpdateDate(now);
						ft.setInternalRemark(Thread.currentThread().getName());
						timeslotId = (Long) facilityTimeslotDao.save(ft);
						break;
					}
				}
			}
		}
		if (null == timeslotId) {
			logger.debug("Can't find available right-hand golf-bay in: [" + startTime + "," + endTime + "]");
		}
		System.out.println("Thread Name:" + Thread.currentThread().getName()
				+ " execute finished," + "startTime=" + startTime + ",endTime="
				+ endTime);

		return timeslotId;

	}
	
	
	@Override
	@Transactional
	public Long bookFacility(Long customerId, Date startTime, Date endTime,
			String facilityType, String attributeId, String status) {
		return bookFacility(Constant.PaymentMethodCode.CASHVALUE.toString(), customerId, startTime, endTime, facilityType, attributeId, status);
	}
	
	@Override
	@Transactional
	public Long bookFacility(String paymentMethod, Long customerId, Date startTime, Date endTime,
			String facilityType, String attributeId, String status) {
		// DistributedLock lock =null;
		Long resvId = null;
		// saveorupdate()
//		String from = this.appProps.getProperty("giftBundle.fromFacilityNo");
//		String end = this.appProps.getProperty("giftBundle.endFacilityNo");
//		int fromFacilityNo = Integer.parseInt(from);
//		int endFacilityNo = Integer.parseInt(end);
//		int availableFacilitys = pmsRequestProcessorDao.countSpecifiedFacility(facilityType, attributeId, status, fromFacilityNo,endFacilityNo);
		int availableFacilitys = pmsRequestProcessorDao.countSpecifiedFacility(facilityType, Constant.ZONE_PRACTICE, status);
		// pmsRequestProcessorDao.getLock();
		System.out.println("Thread Name:" + Thread.currentThread().getName() + " get Lock," + "startTime=" + startTime + ",endTime=" + endTime);
//		List<Integer> facilityNos = pmsRequestProcessorDao
//				.queryAllAvailableSpecifiedFacilityByPeriod(startTime, endTime,
//						facilityType, attributeId, status, fromFacilityNo,endFacilityNo);
		List<Integer> facilityNos = pmsRequestProcessorDao.queryAllAvailableSpecifiedFacilityByPeriod(startTime, endTime,facilityType,Constant.ZONE_PRACTICE,status);
		if (facilityNos.size() == 0) {
			// throw new
			// NoTimeslotException("Can't find available right-hand golf-bay in: ["
			// + startTime +"," + endTime +"]");
			logger.debug("Can't find available right-hand golf-bay in: [" + startTime + "," + endTime + "]");
		} else {
			for (int i = 0; i < facilityNos.size(); i++) {
				// check in one facility
				Integer facilityNo = facilityNos.get(i);

				int reservedFacilitys = pmsRequestProcessorDao.countReservedSpecifiedFacility(startTime, endTime,facilityType, attributeId);
				// check whether there is timeslot available
				if (reservedFacilitys >= availableFacilitys) {
					throw new NoTimeslotException(
							"Can't find available right-hand golf-bay in: ["
									+ startTime + "," + endTime + "]");
				} else {
					// check whether the facility is occupied at this time
					int count = pmsRequestProcessorDao.isoccupied(facilityNo,startTime, endTime);
					boolean flag = count > 0;
					if (flag) {
						continue;
					} else {
						
						CustomerOrderHd customerOrderHd = new CustomerOrderHd();
						customerOrderHd.setOrderDate(new Date());
						customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
						// customerOrderHd.setStaffUserId(staffUserId);
						customerOrderHd.setCustomerId(customerId);
						customerOrderHd.setOrderTotalAmount(new BigDecimal(0));
						customerOrderHd.setOrderRemark("");
						customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));
						// customerOrderHd.setCreateBy(staffUserId);
						// customerOrderHd.setUpdateBy(staffUserId);
						customerOrderHd.setUpdateDate(new Date());
						Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
						

						
						// CUSTOMER ORDER Detail
						CustomerOrderDet customerOrderDet = new CustomerOrderDet();
						customerOrderDet.setCustomerOrderHd(customerOrderHd);
						customerOrderDet.setItemNo("FMS00000004");  //???
						customerOrderDet.setItemRemark("PMS bundle golf bay");
						customerOrderDet.setOrderQty(1L);
						customerOrderDet.setItemTotalAmout(new BigDecimal(0));
						customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));
						// customerOrderDet.setCreateBy(staffUserId);
						customerOrderDet.setUpdateDate(new Date());
						// customerOrderDet.setUpdateBy(staffUserId);
						customerOrderDetDao.saveOrderDet(customerOrderDet);

						// CUSTOMER ORDER TRANS
						CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
						customerOrderTrans.setCustomerOrderHd(customerOrderHd);
						customerOrderTrans.setTransactionTimestamp(new Date());
						customerOrderTrans.setPaidAmount(new BigDecimal(0));
						customerOrderTrans.setStatus(Constant.Status.SUC.name());//???
						// customerOrderTrans.setPaymentRecvBy(staffUserId);
						// customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
						// customerOrderTrans.setTerminalId(terminalId);
						customerOrderTrans.setPaymentMethodCode(paymentMethod);//???
						customerOrderTrans.setPaymentMedia(paymentMethod.equals(Constant.PaymentMethodCode.CASHVALUE.toString()) ? PaymentMediaType.OTH.name() : PaymentMediaType.OP.name());// ???
						customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
						
						
						
						FacilityTimeslot ft = new FacilityTimeslot();
						ft.setBeginDatetime(startTime);
						ft.setEndDatetime(endTime);
						FacilityMaster fm = facilityMasterDao.get(FacilityMaster.class, Long.parseLong(facilityNo.toString()));
						ft.setFacilityMaster(fm);
						ft.setStatus(FacilityStatus.TA.getDesc());
						Timestamp now = new Timestamp(new Date().getTime());
						ft.setCreateDate(now);
						ft.setUpdateDate(now);
						ft.setInternalRemark(Thread.currentThread().getName());
						Long timesslotId = (Long)facilityTimeslotDao.save(ft);
						
						
						
						MemberFacilityTypeBooking memberFacilityTypeBooking = new MemberFacilityTypeBooking();
						memberFacilityTypeBooking.setBeginDatetimeBook(new Timestamp(startTime.getTime()));
						memberFacilityTypeBooking.setEndDatetimeBook(new Timestamp(endTime.getTime()));
						memberFacilityTypeBooking.setCustomerId(customerId);
						memberFacilityTypeBooking.setOrderNo(orderNo);
						memberFacilityTypeBooking.setResvFacilityType("GOLF");
						memberFacilityTypeBooking.setFacilityTypeQty(1L);
						memberFacilityTypeBooking.setStatus(MemberFacilityTypeBookingStatus.RSV.getDesc());
						memberFacilityTypeBooking.setReserveVia("PMS");
						memberFacilityTypeBooking.setCreateDate(new Timestamp(new Date().getTime()));
						memberFacilityTypeBooking.setUpdateDate(new Timestamp(new Date().getTime()));
				
						resvId = (Long) memberFacilityTypeBookingDao.save(memberFacilityTypeBooking);
						
						
						MemberReservedFacility memberReservedFacility = new MemberReservedFacility();
						memberReservedFacility.setFacilityTimeslotId(timesslotId);
						memberReservedFacility.setResvId(resvId);
						memberReservedFacilityDao.save(memberReservedFacility);
				
						MemberFacilityBookAdditionAttr memberFacilityBookAdditionAttr = new MemberFacilityBookAdditionAttr();
						MemberFacilityBookAdditionAttrPK pk = new MemberFacilityBookAdditionAttrPK();
						pk.setResvId(resvId);
						pk.setAttributeId(attributeId);
						memberFacilityBookAdditionAttr.setAttrValue(attributeId);
						memberFacilityBookAdditionAttr.setFacilityType(facilityType);
						memberFacilityBookAdditionAttr.setId(pk);
						memberFacilityBookAdditionAttrDao.save(memberFacilityBookAdditionAttr);
						
						break;
					}
				}
			}
		}
		if (null == resvId) {
			logger.debug("Can't find available right-hand golf-bay in: ["
					+ startTime + "," + endTime + "]");
		}
		System.out.println("Thread Name:" + Thread.currentThread().getName()
				+ " execute finished," + "startTime=" + startTime + ",endTime="
				+ endTime);

		return resvId;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void deleteReservationById(Long resvId) {
		List<MemberReservedFacility> memberReservedFacilityList = memberReservedFacilityDao.getMemberReservedFacilityList(resvId);
		for (MemberReservedFacility  memberReservedFacility:memberReservedFacilityList) {
			memberReservedFacilityDao.deleteMemberReservedFacility(memberReservedFacility);
			memberReservedFacilityDao.getCurrentSession().flush();
			facilityTimeslotDao.deleteFacilityTimeslot(memberReservedFacility.getFacilityTimeslotId());
			facilityTimeslotDao.getCurrentSession().flush();
		}
		memberFacilityBookAdditionAttrDao.deleteByReservationId(resvId);
		memberFacilityBookAdditionAttrDao.getCurrentSession().flush();
		memberFacilityTypeBookingDao.deleteById(MemberFacilityTypeBooking.class, resvId);
		memberFacilityTypeBookingDao.getCurrentSession().flush();
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void saveOrUpdateLock(TLock lock) {
		List<TLock> list = pmsRequestProcessorDao.getAllLocks();
		for(TLock l: list) {
			System.out.println(l);
		}
		pmsRequestProcessorDao.saveOrUpdate(lock);
		
	}

	@Transactional(isolation=Isolation.READ_UNCOMMITTED)
	public void testNestTransaction(TLock lock) {
		
		saveOrUpdateLock(lock);
		 /*try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		int i = 1 /0;
		System.out.println("end");
		
	}

	
	
}
