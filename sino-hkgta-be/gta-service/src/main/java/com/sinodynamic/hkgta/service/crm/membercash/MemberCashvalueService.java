package com.sinodynamic.hkgta.service.crm.membercash;

import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

public interface MemberCashvalueService extends IServiceBase<MemberCashvalue> {
	
	public boolean createMemberCashvalue(CustomerProfile profile, String userId) throws Exception;
	
	public boolean modifyMemberCashvalue(MemberCashvalue memberCashValue) throws Exception;
	
	public MemberCashvalue getMemberCashvalueByCustomerId(Long CustomerId) throws Exception;

	public MemberCashvalue getMemberCashvalueByVirtualAccNo(String virtualAccNo) throws Exception;
	
	public MemberCashvalue getMemberCashvalueById(Long customerId) throws Exception;
	
	public CustomerOrderTransDto paymentByMemberCashvalue(MemberCashValuePaymentDto memberCashValuePaymentDto) throws Exception;
	
	public MemberCashvalue updateMemberCashValueData(Member member,MemberCashValuePaymentDto memberCashValuePaymentDto) throws GTACommonException;
}
