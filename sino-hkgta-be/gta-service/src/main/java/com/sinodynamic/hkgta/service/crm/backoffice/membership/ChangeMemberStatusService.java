package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface ChangeMemberStatusService extends IServiceBase<Member>{

	public ResponseResult updateMemberStatus(String status, Long customerId);
}
