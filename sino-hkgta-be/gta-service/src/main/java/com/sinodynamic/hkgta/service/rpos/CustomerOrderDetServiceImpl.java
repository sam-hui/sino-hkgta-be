package com.sinodynamic.hkgta.service.rpos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
@Scope("prototype")
public class CustomerOrderDetServiceImpl extends
		ServiceBase<CustomerOrderDet> implements CustomerOrderDetService {
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;

	@Override
	@Transactional
	public CustomerOrderDet getCustomerOrderDet(Long orderNo)
	{
		return customerOrderDetDao.getCustomerOrderDet(orderNo);
	}
}
