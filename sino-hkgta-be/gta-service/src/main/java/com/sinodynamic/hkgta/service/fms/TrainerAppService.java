package com.sinodynamic.hkgta.service.fms;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.TrainingRecordDto;
import com.sinodynamic.hkgta.dto.staff.CoachMonthyAchivementDto;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface TrainerAppService extends IServiceBase {
	Data getReservationList(String coachId, String orderColumn, String order, int pageSize,
			int currentPage, AdvanceQueryDto filters);


	/**   
	* @author: Zero_Wang
	* @since: Aug 27, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	void autoPushMsgInAdvance2Day();
	

	CoachMonthyAchivementDto calculateCoachAchievement(Map<String, Object> params);
	
	ResponseResult rollCallforCoaching(Long resvId, String qrCode) throws Exception;
	
	ResponseResult getTrainingComments(Long timeslotId, Long customerId) throws Exception;
	
	ResponseResult updateTrainingComments(Long timeslotId, Long customerId, Double score, String comments, String assignment) throws Exception;


	/**   
	* @author: Zero_Wang
	* @since: Aug 31, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	List<CustomerProfileDto> getPrivateCoachTrainedMemberList(String staffNo, String isAsending);
	
	ListPage<TrainingRecordDto> getTrainingRecord(ListPage<TrainingRecordDto> pListPage, Long customerId, String coachId, String sortBy, String isAscending, String filterBy) throws Exception;


	/**   
	* @author: Zero_Wang
	* @since: Sep 1, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	List<CustomerProfileDto> getPrivateCoachTrainedMemberListByName(
			String staffNo, String name, String isAsending);


	/**   
	* @author: Zero_Wang
	* @since: Sep 7, 2015
	* 
	* @description
	* this method will do follow two things
	* private coach Auto send MSM remind member InAdvance1Hour
	* private coach Auto push msg remind coach InAdvance1Hour
	*/  
	    
	void autoRemindInAdvance1Hour();


	Data getReservationListByDate(String coachId, String orderColumn, String order, int pageSize, int currentPage, String resvDate);


	ResponseResult hasRollCall(Long resvId) throws Exception;

	public void sendRemindSMS2Member(MemberFacilityTypeBooking booking);
	
	public void pushRemindMsg2Coach(MemberFacilityTypeBooking booking);


	

}
