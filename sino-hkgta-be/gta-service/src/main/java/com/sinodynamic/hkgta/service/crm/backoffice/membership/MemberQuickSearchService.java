package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberQuickSearchService extends IServiceBase<CustomerProfile>{
	public ResponseResult quickSeachMember(String number);

}
