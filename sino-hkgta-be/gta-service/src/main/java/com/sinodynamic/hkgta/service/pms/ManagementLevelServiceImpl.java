package com.sinodynamic.hkgta.service.pms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.StaffDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dto.pms.ManagementLevelDto;
import com.sinodynamic.hkgta.dto.pms.ManagementStaffDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class ManagementLevelServiceImpl implements ManagementLevelService {

	@Autowired
	private SysCodeDao sysCodeDao;

	@Autowired
	private StaffDao staffDao;
	
	@Override
	@Transactional
	public List<ManagementLevelDto> getAllManagementLevel() {

		List<SysCode> managementLevelList = sysCodeDao.selectSysCodeByCategory(Constant.SYSCODE_CREW_ROLE);

		List<ManagementLevelDto> managementLevelDtoList = new ArrayList<>();
		if (managementLevelList != null) {

			for (SysCode sysCode : managementLevelList) {

				String positionTitleCode = sysCode.getCodeValue();
				String positionTitleName = sysCode.getCodeDisplay();
				ManagementLevelDto managementLevelDto = new ManagementLevelDto();
				managementLevelDto.setPositionTitleCode(positionTitleCode);
				managementLevelDto.setPositionTitleName(positionTitleName);

				List<StaffMaster> staffMasterList = staffDao.getStaffMasterByRoomCrewRoleCode(positionTitleCode);
				List<ManagementStaffDto> staffDtoList = new ArrayList<>();
				for (StaffMaster staffMaster : staffMasterList) {
					ManagementStaffDto staffDto = new ManagementStaffDto();
					staffDto.setUserId(staffMaster.getUserId());

					if (staffMaster.getStaffProfile() != null) {
						String fullName = staffMaster.getStaffProfile().getGivenName() + " " + staffMaster.getStaffProfile().getSurname();
						staffDto.setFullName(fullName);
					}

					staffDtoList.add(staffDto);
				}
				managementLevelDto.setStaffList(staffDtoList);
				managementLevelDtoList.add(managementLevelDto);
			}

		}
		return managementLevelDtoList;
	}

}
