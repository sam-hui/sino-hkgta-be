package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Spring_Zheng
 *
 */

public interface CustomerServiceAccService extends IServiceBase<CustomerServiceAcc>{
	
	 public ResponseResult updateCustomerServiceAccStatus(Long orderNo);
	 
	 public List<CustomerServiceAcc> getExpiringCustomers() throws Exception;

	public void handleExpiredCustomerServiceAcc() throws Exception;

	/**   
	* @author: Zero_Wang
	* @since: Sep 9, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	public CustomerServiceAcc getAactiveByCustomerId(Long customerId);
	
	
	public boolean isCustomerServiceAccValid(Long customerId, ContractHelper obj);

	public void startRenewServiceAccount() throws Exception;

}
