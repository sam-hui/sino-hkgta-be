package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dto.crm.CorporateServiceAccDto;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;


@Service
public class CorporateServiceAccServiceImpl extends ServiceBase<CorporateServiceAcc> implements CorporateServiceAccService {

	@Autowired
	private CorporateServiceAccDao corporateServiceAccDao;
	
	@Override
	@Transactional
	public CorporateServiceAccDto getAactiveByCustomerId(Long corporateId) {
		CorporateServiceAccDto dto = this.corporateServiceAccDao.getAactiveByCustomerId(corporateId);
		if(null==dto){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CorporateServiceAcc corporateId not right:"+corporateId});
		}
		return dto;
		
	}
}
