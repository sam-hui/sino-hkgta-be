
package com.sinodynamic.hkgta.service.mms;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.mms.SpaCategoryDao;
import com.sinodynamic.hkgta.dao.mms.SpaCenterInfoDao;
import com.sinodynamic.hkgta.dao.mms.SpaPicPathDao;
import com.sinodynamic.hkgta.dao.mms.SpaRetreatDao;
import com.sinodynamic.hkgta.dao.mms.SpaRetreatItemDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.fms.CourseSimpleInfoDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDetailDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryDto;
import com.sinodynamic.hkgta.dto.mms.SpaCenterInfoDto;
import com.sinodynamic.hkgta.dto.mms.SpaPicPathDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatItemDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;
import com.sinodynamic.hkgta.entity.mms.SpaCenterInfo;
import com.sinodynamic.hkgta.entity.mms.SpaPicPath;
import com.sinodynamic.hkgta.entity.mms.SpaRetreat;
import com.sinodynamic.hkgta.entity.mms.SpaRetreatItem;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.integration.spa.em.CustomPaymentMethod;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.AvailableRoomsRequest;
import com.sinodynamic.hkgta.integration.spa.request.BookAppointmentRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetCenterTherapistsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetServiceDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.PaymentByCustomRequest;
import com.sinodynamic.hkgta.integration.spa.request.ServiceInfoItem;
import com.sinodynamic.hkgta.integration.spa.request.ServicesListByCategoryCodeRequest;
import com.sinodynamic.hkgta.integration.spa.request.SubCategoriesListRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistRequestType;
import com.sinodynamic.hkgta.integration.spa.request.TherapistsAvailableForServiceRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetail;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentServiceCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AvailableRoomsResponse;
import com.sinodynamic.hkgta.integration.spa.response.BookAppointmentResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTimeSlotsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetSubcategoriesResponse;
import com.sinodynamic.hkgta.integration.spa.response.GuestDetails;
import com.sinodynamic.hkgta.integration.spa.response.PaymentByCustomResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCategoryResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCenterTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryServiceListResponse;
import com.sinodynamic.hkgta.integration.spa.response.Room;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetail;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.Slot;
import com.sinodynamic.hkgta.integration.spa.response.Therapist;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.integration.util.MMSAPIException;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.util.CalendarUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.AdvancePeriodType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CourseType;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberAcceptance;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.encrypt.EncryptUtil;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;
/**
 * 
 * @author Nick_Xiong
 * 
 */
@Service
public class MMSServiceImpl extends ServiceBase<SpaAppointmentRec> implements MMSService{

	@Autowired
	private SpaServcieManager spaServcieManager;

	private static Logger logger = LoggerFactory.getLogger(MMSServiceImpl.class);
	private static String WP_APP_CATEGORY="RETREATS001,RTL001,CUISINE001,SPECIALS001";
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
	private SpaAppointmentRecDao spaAppointmentRecDao;
	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	@Autowired
	private SpaCenterInfoDao spaCenterInfoDao;
	
	@Autowired
	private SpaCategoryDao spaCategoryDao;
	
	@Autowired
	private SpaPicPathDao spaPicPathDao;
	
	@Autowired
	private SpaRetreatDao spaRetreatDao;
	
	@Autowired
	private SpaRetreatItemDao spaRetreatItemDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private ServicePlanDao servicePlanDao;
	
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private MemberCashvalueService memberCashvalueService;
	
	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private MemberLimitRuleDao limitRuleDao;
	
	@Autowired
	private MessageTemplateDao templateDao;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private ShortMessageService smsService;
	
	/**
	 * This method will return the first level categories only and the category must at least has one service.
	 */
	public ResponseResult queryTopCategories() throws Exception {
	
		try{
			QueryCategoryResponse response = spaServcieManager.getCategoriesList();
			
			if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		 return responseResult;
	}

	
	@Transactional
	public ResponseResult getLocalTopCategory(String device) throws Exception
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("device:" + device);
		}
		List<SpaCategoryDto> spaCategoryList = getSpaCategoryList();
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		if(spaCategoryList!=null && spaCategoryList.size()>0)
		{
			for(SpaCategoryDto category: spaCategoryList)
			{
				List<SpaPicPathDto> picList = category.getCategoryPicList();
				List<SpaPicPathDto> tmpList = new ArrayList<SpaPicPathDto>();
				if("PC".equals(device)) //if PC, remove those catetory which apply for Webportal & APP 
				{					
					if(WP_APP_CATEGORY.indexOf(category.getCategoryId())>=0)
					    continue;
					resultList.add(category);
				}
				//for webportal and APP, need return the mapped picture.
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPathDto pic : picList)
					{
						if(("APP".equals(pic.getPicType()) && "APP".equals(device))
					        || ("WP".equals(pic.getPicType()) && "WP".equals(device)))
						{
							tmpList.add(pic);
							break;
						}
					}
				}
				category.setCategoryPicList(tmpList);
			}
			if("PC".equals(device))
			{
				responseResult.initResult(GTAError.Success.SUCCESS, resultList);
			}else
			{
				responseResult.initResult(GTAError.Success.SUCCESS, spaCategoryList);
			}
			
		}else
		{
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "Get Top Category List failed.");
		}		
		
		 return responseResult;
	}
	/**
	 * This method will return the center's all therapists, include female, male which is available 
	 * on the specified Date.
	 */
	public ResponseResult getCenterTherapists(Date requestDate)throws Exception {
		try
		{
		 GetCenterTherapistsRequest request = new GetCenterTherapistsRequest();
		 request.setRequestDate(requestDate);
		 QueryCenterTherapistsResponse response =  spaServcieManager.getCenterTherapists(request);
		 
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}

	public ResponseResult therapistsAvailableForService(Date requestDate, String serviceCode, TherapistRequestType type)throws Exception {
		try
		{
		  TherapistsAvailableForServiceRequest request = new TherapistsAvailableForServiceRequest();
		  request.setRequestDate(requestDate);
		  request.setServiceCode(serviceCode);
		  request.setType(type);
		  GetAvailableTherapistsResponse apiResponse = spaServcieManager.therapistsAvailableForService(request);
		  
		  String protocal = this.appProps.getProperty("mms.img.protocal");
		  if(protocal==null)protocal = "http";
		  if(logger.isDebugEnabled())
		  {
			  logger.debug("protocal:" + protocal);
		  }
		  
		  if(!apiResponse.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, apiResponse.getMessage());
			}else
			{
				List<Therapist> therapists = apiResponse.getTherapists();
				if(therapists!=null && therapists.size()>0)
				{
				  for(Therapist therapist : therapists)
				  {
					  String imageUrl = therapist.getImageURL();
					  if(logger.isDebugEnabled())
					  {
						  logger.debug("imageUrl before:" + imageUrl);
					  }
					  if(!StringUtils.isEmpty(imageUrl))
					  {
							String httpStr = imageUrl.substring(0,imageUrl.indexOf(":"));
							imageUrl = imageUrl.replaceFirst(httpStr, protocal);
							 if(logger.isDebugEnabled())
							  {
								  logger.debug("imageUrl after:" + imageUrl);
							  }
							therapist.setImageURL(imageUrl);
					  }
					  if(StringUtils.isEmpty(therapist.getUserCode()) && !StringUtils.isEmpty(therapist.getEmployeeCode()))
					  {
						  therapist.setUserCode(therapist.getEmployeeCode());
					  }
				  }
				}
				apiResponse.setTherapists(therapists);
			 responseResult.initResult(GTAError.Success.SUCCESS, apiResponse);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		  return responseResult;
	}

	@Override
	public ResponseResult getAvailableTimeSlots(GetAvailableTimeSlotsRequest parameter)throws Exception {
		
		try
		{
		 GetAvailableTimeSlotsResponse response =  spaServcieManager.getAvailableTimeSlots(parameter);
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}

	@Override
	public ResponseResult getFormatAvailableTimeSlots(GetAvailableTimeSlotsRequest parameter)throws Exception {
		
		try
		{
		 GetAvailableTimeSlotsResponse response =  spaServcieManager.getAvailableTimeSlots(parameter);
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
				
				List<List<Slot>> slots = response.getSlots();
				Set<String> timeSet = new TreeSet<String>();
				SimpleDateFormat sdformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
				
				
			if (slots != null && slots.size() > 0) 
			{
				for (List<Slot> subSlots : slots) 
				{
					if (subSlots != null && subSlots.size() > 0) 
					{
						for (Slot slot : subSlots) 
						{
							String startTime = slot.getStarttimestring();
							String endTime = slot.getEndtimestring();
							Date startDate = sdformat.parse(startTime);
							Date endDate = sdformat.parse(endTime);
							while (startDate.before(endDate)) 
							{
								timeSet.add(DateCalcUtil.getHourAndMinuteOfDay(startDate));
								startDate = DateCalcUtil.getNextHalfHour(startDate);
							}

						}
					}
				}
			}
			 responseResult.initResult(GTAError.Success.SUCCESS, timeSet);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}
	

	@Override
	public ResponseResult queryServiceList(String categoryCode)throws Exception {
	
	  try
	  {
		ServicesListByCategoryCodeRequest request = new ServicesListByCategoryCodeRequest();
		request.setCategoryCode(categoryCode);
		QueryServiceListResponse response = spaServcieManager.getServicesListByCategoryCode(request);
		if(!response.getSuccess()){
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
		 responseResult.initResult(GTAError.Success.SUCCESS, response);
		}
	  }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public ResponseResult bookAppointment(SpaAppointmentDto spaAppointmentDto)throws Exception {
		
		if(logger.isDebugEnabled()){
			logger.debug("bookAppointment start.");
		}		
			
		boolean permitted = false;
		MemberPlanFacilityRight memberPlanFacilityRight = memberPlanFacilityRightDao.getEffectiveRightByCustomerIdAndFacilityType(spaAppointmentDto.getCustomerId(),"WELLNESS");
		if (memberPlanFacilityRight != null) {
			Long servicePlanNo = memberPlanFacilityRight.getServicePlan();
			ServicePlan servicePlan = servicePlanDao.getServicePlanById(servicePlanNo);
			String periodType = servicePlan.getPassPeriodType();
			if ("WD".equals(periodType)) {
				String appointmentDateStr = spaAppointmentDto.getServiceDetails().get(0).getStartDatetime();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date appointmentDate = df.parse(appointmentDateStr);
				if (!CalendarUtil.isWeekend(appointmentDate)) {
					permitted = true;
				} else {
					throw new GTACommonException(GTAError.FacilityError.SERVICE_PLAN_WEEK_DAY_ONLY);
				}
			} else {
				permitted = true;
			}
		}else
		{
			permitted = false;
		}
		
		if (permitted == false) {
			throw new GTACommonException(GTAError.FacilityError.SERVICE_PLAN_NOT_INCLUDE_THIS_FACILITY);
		}
		
		checkAppointmentBookingDate(spaAppointmentDto);
	
	   try
	   {
		BookAppointmentRequest bookAppointmentParameters;		
	    bookAppointmentParameters = convertRequestToApiParam(spaAppointmentDto);
		BookAppointmentResponse response = spaServcieManager.bookAppointment(bookAppointmentParameters);	
			
		if (!response.getSuccess()) 
		{
			logger.error("book appointment via API failed:"	+ response.getMessage());
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		} else 
		{
				if (logger.isDebugEnabled()) {
					logger.debug("BookAppointmentResponse:" + response);
				}
				createLocalAppointment(spaAppointmentDto, response);
				// for PC reservation without payment

				String invoiceNo = response.getInvoiceNo();

				String paymentMethod = spaAppointmentDto.getPaymentMethod();
				if (paymentMethod != null && !"".equals(paymentMethod)) {
					// for webportal reservation include payment
					if (invoiceNo != null && !"".equals(invoiceNo)) {
						MMSPaymentDto paymentDto = new MMSPaymentDto();
						paymentDto.setCustomerId(spaAppointmentDto.getCustomerId());
						paymentDto.setInvoiceNo(invoiceNo);
						paymentDto.setPaymentMethod(paymentMethod);
						paymentDto.setIsRemote("N");
						ResponseResult paymentResult = payment(paymentDto);
						paymentDto = (MMSPaymentDto) paymentResult.getDto();					
						
						responseResult.initResult(GTAError.Success.SUCCESS,paymentDto);
					} else {
						responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION,"InvoiceNo cannot be null for payment.");
					}
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS);
					responseResult.setData(invoiceNo);
				}

			}	
	   }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
					
		return responseResult;
	}
	
	
	@Transactional
	public void sentPaymentReceipt(MMSPaymentDto dto, Boolean isSynchronized) throws Exception
	{
		CustomerEmailContent customerEmailContent = getEmailContent(dto.getInvoiceNo(),isSynchronized);
		if (customerEmailContent != null) {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(Long.valueOf(dto.getOrderNo()));
			CustomerOrderTrans customerOrderTrans = customerOrderTranses.get(0);
			byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, customerOrderTrans.getTransactionNo().toString(), "wellness");
			List<byte[]> attachmentList = Arrays.asList(attachment);
			List<String> mineTypeList = Arrays.asList("application/pdf");
			List<String> fileNameList = Arrays.asList("MMSPaymentReceipt-"+customerOrderTrans.getTransactionNo().toString()+".pdf");
			mailThreadService.sendWithResponse(customerEmailContent, attachmentList, mineTypeList, fileNameList);
		}
	}
	
	@Transactional
	public void sentMessage(List<SpaAppointmentRec> appointmentList, SceneType messageType) throws Exception
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("sentMessage start.");
		}		
			SpaAppointmentRec rec = appointmentList.get(0);
			String invoiceNo = rec.getExtInvoiceNo();			
			Long customerId = rec.getCustomerId();
			String message = getMessageContent(invoiceNo, customerId, messageType);
			if(StringUtils.isEmpty(message))return;
			CustomerProfile cp = customerProfileDao.getById(customerId);
			String phoneNumber = cp.getPhoneMobile();
			if(logger.isDebugEnabled())
			{
				logger.debug("phoneNumber:" + phoneNumber);
				logger.debug("message content:" + message);
			}
			List<String> phoneList = new ArrayList<String>();
			phoneList.add(phoneNumber);
			smsService.sendSMS(phoneList, message, new Date());		
				
		
		if(logger.isDebugEnabled())
		{
			logger.debug("sentMessage end.");
		}
	}
	
	public String getMessageContent(String invoiceNo, Long customerId, SceneType messageType)throws Exception
	{
		String message = null;
			
		String functionId = null;
		if(SceneType.WELLNESS_PAYMENT_SETTLE.equals(messageType))
		{
			functionId = "sms_wellness_payment_confirm";
		}
		
		if(logger.isDebugEnabled())
		{
			logger.debug("functionId:" + functionId);
		}
		if (functionId == null) return null;
		MessageTemplate template = templateDao.getTemplateByFunctionId(functionId);
		if (template == null)
		{
			logger.error("Database data error for sending message!");
			return null;
		}
		List<SpaAppointmentRec> recList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		if (recList == null)
		{
		   logger.error("Database data error for sending receipt!");
		   return null;
		}
		
		String detail = "";		
		for(SpaAppointmentRec rec : recList)
		{			
			String serviceName = rec.getServiceName();
			String serviceTime = DateCalcUtil.formatDatetime(rec.getStartDatetime()) + " to " + DateCalcUtil.formatDatetime(rec.getEndDatetime());
			detail += serviceName + " (" + serviceTime + " ) ";			
		}						
			message = template.getFullContent(detail);
		
		return message;
	}
	
	private  CustomerEmailContent getEmailContent(String invoiceNo,Boolean isSynchronized) throws Exception {
		
		if (invoiceNo ==null ) return null;

		List<SpaAppointmentRec> recList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		if (recList == null)
		{
		   logger.error("Database data error for sending receipt!");
		   return null;
		}
		String detail = "";
		Long customerId = null;
		double amount = 0;
		for(SpaAppointmentRec rec : recList)
		{
			customerId = rec.getCustomerId();
			String serviceName = rec.getServiceName();
			String serviceTime = DateCalcUtil.formatDatetime(rec.getStartDatetime()) + " to " + DateCalcUtil.formatDatetime(rec.getEndDatetime());
			detail += serviceName + " (" + serviceTime + " ) ";
			
			amount += rec.getServiceInternalCost().doubleValue();
		}
		
		if(customerId == null)
		{
			logger.error("CustomerId is empty.");
			return null;
		}
		CustomerProfile cp = customerProfileDao.getById(customerId);
		if (cp == null)
		{
			logger.error("Database data error for sending receipt!");
			   return null;
		}
		
		String functionId = null;
		if(isSynchronized)
			functionId = "email_wellness_payment_receipt_synchronized";
		else
		    functionId = "email_wellness_payment_receipt";
		
		MessageTemplate template = templateDao.getTemplateByFunctionId(functionId);
		if (template == null)
		{
			logger.error("Database data error for sending receipt!");
			   return null;
		}

		String recipientEmail = cp.getContactEmail();
		String subjectName = template.getMessageSubject();
//		String templateContent = template.getContent();
		String userName = getName(cp);
		String memberName = userName;			
		
		
		String content = template.getFullContent(userName, memberName,invoiceNo,detail,String.valueOf(amount));
		
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setStatus(EmailStatus.PND.name());
		cec.setNoticeType(Constant.NOTICE_TYPE_WELLNESS);
		cec.setRecipientCustomerId(String.valueOf(customerId));
		cec.setRecipientEmail(recipientEmail);
		cec.setSubject(subjectName);
		cec.setSendDate(new Date());

		String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendId);
		return cec;
	}
	
	private String getName(CustomerProfile cp) {

		if (cp == null)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName())
				.append(" ").append(cp.getSurname());
		return sb.toString();
	}
	
	private void checkAppointmentBookingDate(SpaAppointmentDto spaAppointmentDto) throws Exception
	{
		List<SpaAppointmentDetailDto> serviceList = spaAppointmentDto.getServiceDetails();	
		SpaAppointmentDetailDto detail = serviceList.get(0);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
						
		if (format.parse(detail.getStartDatetime()).before(format.parse(format.format(new Date()))))
			throw new GTACommonException(GTAError.FacilityError.DATE_LESSTHAN_TODAY );
	
		int parameterValue = 0;
		
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
		if (null != globalParameter)
		{
			if (!StringUtils.isEmpty(globalParameter.getParamValue()))
				parameterValue = Integer.parseInt(globalParameter.getParamValue());
		}
		if (parameterValue > 0)
		{
			Date futureDate = DateCalcUtil.getNearDay(new Date(), parameterValue);
			if (format.parse(detail.getStartDatetime()).after(DateCalcUtil.getEndDateTime(futureDate)))
				throw new GTACommonException(GTAError.FacilityError.DATE_OVER_ADVANCEDPERIOD,new Object[]{parameterValue});
		}
	}
	
	
	private BookAppointmentRequest convertRequestToApiParam(SpaAppointmentDto spaAppointmentDto)throws Exception{
		
		if(logger.isDebugEnabled()){
			logger.debug("convert the input param to API parameter.");
		}		
		Long customerId = spaAppointmentDto.getCustomerId();	
		Member member = memberDao.getMemberByCustomerId(customerId);
		
		
		if(!"ACT".equals(member.getStatus())){
			logger.error("The member is not active.");		
	  		throw new GTACommonException(null, "The member is not active.");
		}		
		
		String invoiceNo = spaAppointmentDto.getInvoiceNo();
		
		BookAppointmentRequest bookAppointmentParameters = new BookAppointmentRequest();
		
		if(logger.isDebugEnabled()){
			logger.debug("userCode:" + member.getAcademyNo());
		}

		if(invoiceNo!=null && !"".equals(invoiceNo)){
			AppointmentDetailsRequest request = new AppointmentDetailsRequest();
			request.setInvoiceNo(invoiceNo);
			AppointmentDetailsResponse  appointmentDetailResponse = spaServcieManager.appointmentDetails(request);
			GuestDetails guestDetail = appointmentDetailResponse.getGuestDetails();
			String existingGuestCode = guestDetail.getUsercode();
			if(!member.getAcademyNo().equals(existingGuestCode)){
				logger.error("The appointment is expected for user " + existingGuestCode);		
		  		throw new GTACommonException(null, "The appointment is not match the existing user " + existingGuestCode);
			}
		}
		bookAppointmentParameters.setGuestCode(member.getAcademyNo());
		bookAppointmentParameters.setInvoiceNo(invoiceNo);
		List<SpaAppointmentDetailDto> serviceList = spaAppointmentDto.getServiceDetails();		
		List<ServiceInfoItem> serviceInfo = new ArrayList<ServiceInfoItem>();
		
		for(SpaAppointmentDetailDto service : serviceList){
		
			ServiceInfoItem item = new ServiceInfoItem();			
		  	item.setServiceCode(service.getServiceCode());
		  	
		  try{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");			
			bookAppointmentParameters.setStartTime(df.parse(service.getStartDatetime()));	///if multiple services, which startTime should be used?	
		  	item.setStartTime(df.parse(service.getStartDatetime())); 
		  	
			if(logger.isDebugEnabled()){
		  		logger.debug("get available rooms based on the service code and time");
		  	}  	
			AvailableRoomsRequest request = new AvailableRoomsRequest();
			request.setServiceCode(service.getServiceCode());
			request.setStartDate(df.parse(service.getStartDatetime()));
			request.setTherapistCode(service.getTherapistCode());
            AvailableRoomsResponse roomResponse = spaServcieManager.queryAvailableRooms(request);
		  	if(logger.isDebugEnabled()){
		  		logger.debug("roomResponse:" + roomResponse);
		  	}
				if (roomResponse != null) {
					for (Room room : roomResponse.getRooms()) {
						Map<String, Object> additionalProperties = room.getAdditionalProperties();
						String roomCode = additionalProperties.get("RoomCode")!=null? additionalProperties.get("RoomCode").toString() : "";
						if(logger.isDebugEnabled()){
							logger.debug("roomCode:" + roomCode);
						}
						if (roomCode != null && !"".equals(roomCode)) {
							if(logger.isDebugEnabled()){
								logger.debug("selected roomCode:" + roomCode);
							}
							item.setRoomCode(roomCode);
							break;
						}
					}
				}else{
			  		logger.error("no available rooms  or get available room failed.");		
			  		throw new GTACommonException(null, "No room avalibale for the service.");
				}
				// As the roomCode from API may all null, add below check to avoid this issue.
				if(item.getRoomCode() == null || "".equals(item.getRoomCode())){					
			  		logger.error("no available rooms  or get available room failed.");		
			  		throw new GTACommonException(null, "No room avalibale for the service.");				
				}
			item.setEndTime(df.parse(service.getEndDatetime()));
			
		  }catch(ParseException e){
			  
			  logger.error("The startTime/endTime format error, the correct format should be yyyy-MM-dd HH:mm:ss");
		  }
		  
		  	item.setTherapistCode(service.getTherapistCode());
		  	
		  	if(service.getTherapistType()!=null && "0".equals(service.getTherapistType())){	  	
		  		item.setTherapistRequestType(TherapistRequestType.FEMALE);
		  	}else if(service.getTherapistType()!=null && "1".equals(service.getTherapistType())){
		  		item.setTherapistRequestType(TherapistRequestType.MALE);
		  	}else
		  	{
		  		item.setTherapistRequestType(TherapistRequestType.ANY);
		  	}
		  	serviceInfo.add(item);	
		}
	
	  	bookAppointmentParameters.setServiceInfo(serviceInfo);
	  	
	  	return bookAppointmentParameters;
	}
	
//	@Transactional
	private void createLocalAppointment(SpaAppointmentDto param, BookAppointmentResponse response){
		
		if(logger.isDebugEnabled()){
			logger.debug("createLocalAppointment start.");
		}
		String[] appointmentId = null;
		String invoiceNo = "";
		if(response!=null)
		{
		
		appointmentId = response.getAppointmentId().split(","); 
		invoiceNo = response.getInvoiceNo();		
		}else
		{
			invoiceNo = param.getInvoiceNo();
		}
		AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentDetailsResponse  appointmentDetailResponse = spaServcieManager.appointmentDetails(request);
		if(logger.isDebugEnabled())
		{
			logger.debug("AppointmentDetailsResponse from MMS:" + appointmentDetailResponse.toString());
		}
		if(appointmentDetailResponse!=null){
			List<CustomerOrderHd>  orderList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode", invoiceNo, null);
			CustomerOrderHd customerOrderHd = new CustomerOrderHd();			
					
			Long customerId = param.getCustomerId();	
			double totalAmount = 0;
			if(orderList!=null && orderList.size()>0){	
				customerOrderHd = orderList.get(0);
				totalAmount = customerOrderHd.getOrderTotalAmount().doubleValue();
			}	
			
			GuestDetails  guestDetail = appointmentDetailResponse.getGuestDetails();		

			for(int i=0; i<param.getServiceDetails().size(); i++){
				
				SpaAppointmentDetailDto detail = param.getServiceDetails().get(i);
				
				totalAmount = totalAmount + detail.getServiceCost().doubleValue();			
	        	customerOrderHd.setOrderDate(new Date());
				customerOrderHd.setOrderStatus(Constant.Status.OPN.name());			
				customerOrderHd.setCustomerId(customerId);
				customerOrderHd.setOrderTotalAmount(new BigDecimal(totalAmount));
				customerOrderHd.setOrderRemark("");
				customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));		
				customerOrderHd.setUpdateDate(new Date());
				customerOrderHd.setVendorRefCode(invoiceNo);
				customerOrderHdDao.saveOrUpdate(customerOrderHd);

				// CUSTOMER ORDER Detail				
				
				CustomerOrderDet customerOrderDet = new CustomerOrderDet();
								
				customerOrderDet.setCustomerOrderHd(customerOrderHd);
				customerOrderDet.setItemNo("MMS2BRS");   //???
				customerOrderDet.setItemRemark("");  //????
				customerOrderDet.setOrderQty(1L);
				customerOrderDet.setItemTotalAmout(detail.getServiceCost());
				customerOrderDet.setExtRefNo(detail.getServiceCode()); //set ext ref no as the service code
				customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));			
				customerOrderDet.setUpdateDate(new Date());	               
				Long orderDetId = (Long) customerOrderDetDao.saveOrderDet(customerOrderDet);
				
				if(logger.isDebugEnabled()){
					logger.debug("orderNo:" + customerOrderHd.getOrderNo()+ "orderDetId:" + orderDetId);
				}
				// SPA_APPOINTMENT_REC
				SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
				
				if(logger.isDebugEnabled()){
					logger.debug("appointment detail return from API:" + appointmentDetailResponse);
				}				
							
				spaAppointmentRec.setRebooked(guestDetail.getRebooked()? new Long(1) : new Long(0));
				
				if(appointmentId!=null){
					spaAppointmentRec.setExtAppointmentId(appointmentId[i]);
				}	    		
	    		spaAppointmentRec.setExtInvoiceNo(invoiceNo);
	    		
	    		String serviceCode = detail.getServiceCode();
	    		GetServiceDetailsRequest queryServiceDetailsReq = new GetServiceDetailsRequest();
	    		queryServiceDetailsReq.setServiceCode(serviceCode);
	    		
	    		if(logger.isDebugEnabled())
	    		{
	    			logger.debug("ServiceCode:" + serviceCode);
	    		}
	    		ServiceDetailsResponse serviceDetailResponse = spaServcieManager.queryServiceDetails(queryServiceDetailsReq);
	    		if(logger.isDebugEnabled())
	    		{
	    			logger.debug(" ServiceDetailsResponse from MMS:" + serviceDetailResponse.toString());
	    		}
	    		ServiceDetail serviceDetail = serviceDetailResponse.getServiceDetail();
	    		
	    		spaAppointmentRec.setServiceTime(detail.getServiceTime());
				spaAppointmentRec.setServiceInternalCost(detail.getServiceCost());
	    		spaAppointmentRec.setExtServiceCode(serviceCode);
	    		spaAppointmentRec.setServiceName(serviceDetail.getName());
	    		spaAppointmentRec.setServiceDescription(serviceDetail.getDescription());
//	    		spaAppointmentRec.setOrderNo(orderNo);
	    		spaAppointmentRec.setOrderDetId(orderDetId);
	    		
	    		String therapistCode = detail.getTherapistCode();
	    		if(therapistCode!=null)
	    		{
					Therapist therapist = getTherapistInfo(therapistCode,
							new Date());

					spaAppointmentRec.setTherapistCode(therapistCode);
					spaAppointmentRec.setTherapistFirstname(therapist
							.getFirstName());
					spaAppointmentRec.setTherapistLastname(therapist
							.getLastName());
				}
	    				
				String startTime = detail.getStartDatetime();
				String endTime = detail.getEndDatetime();
										
				if(logger.isDebugEnabled()){
						logger.debug("startTime:" + startTime + " endTime:" + endTime);
				}
					
				spaAppointmentRec.setStartDatetime(Timestamp.valueOf(startTime));					
				spaAppointmentRec.setEndDatetime(Timestamp.valueOf(endTime));		
	    		
	    		spaAppointmentRec.setStatus(Constant.Status.RSV.name());    		
	    		spaAppointmentRec.setCreateDate(new Timestamp(new Date().getTime()));
	    		spaAppointmentRec.setCreateBy("");
	    		spaAppointmentRec.setCustomerId(customerId);
	    		spaAppointmentRecDao.saveSpaAppointmenRec(spaAppointmentRec);    		
			}    
	        
		}
		if(logger.isDebugEnabled()){
			logger.debug("createLocalAppointment end.");
		}
	}

	@Override
	public ResponseResult queryServiceDetails(String serviceCode)throws Exception {
	
	  try
	  {
		GetServiceDetailsRequest request = new GetServiceDetailsRequest();
		request.setServiceCode(serviceCode);
		 ServiceDetailsResponse response = spaServcieManager.queryServiceDetails(request);
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
	  }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}


	@Override
	public ResponseResult getSubcategories(String categoryCode)throws Exception {
	
	 try
	 {
		SubCategoriesListRequest request = new SubCategoriesListRequest();
		request.setCategoryCode(categoryCode);
		 GetSubcategoriesResponse response =  spaServcieManager.getSubCategoriesListByCode(request);
		 
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}
	
	@Override
	public ResponseResult getAppointmentDetail(String invoiceNo)throws Exception {
	 
	 try
	 {
		AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentDetailsResponse response =  spaServcieManager.appointmentDetails(request);		
		if(!response.getSuccess()){
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
		 responseResult.initResult(GTAError.Success.SUCCESS, response);
		}
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
	}
	
	
	//this method added for PC function -> add a new service into existing invoice no, and only return the newly added servcie detail.
	@Override
	@Transactional
	public ResponseResult getLatestAppointmentDetail(String invoiceNo)throws Exception {
		if(logger.isDebugEnabled()){
			logger.debug("getLatestAppointmentDetail start.");
		}
         List<SpaAppointmentRec> resultList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo, "sysId desc");
         SpaAppointmentRec record = resultList.get(0);
         System.out.print("servicd get from local DB table:sysId=" + record.getSysId() );
         if(logger.isDebugEnabled()){
        	 logger.debug("servicd get from local DB table:sysId=" + record.getSysId() );
         }
         SpaAppointmentDto appointmentDto = new SpaAppointmentDto();
         
         List<SpaAppointmentDetailDto> detailList = new ArrayList<SpaAppointmentDetailDto>();
         
         SpaAppointmentDetailDto detailDto = new SpaAppointmentDetailDto();
         
         Long customerId = record.getCustomerId();
         Member member = this.memberDao.getMemberByCustomerId(customerId);
         appointmentDto.setAccademyNo(member.getAcademyNo());
         
         CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
                       
         appointmentDto.setMemberName(profile.getSalutation() + " " + profile.getGivenName() + " " + profile.getSurname());
         
         String serviceCode = record.getExtServiceCode();
         GetServiceDetailsRequest request = new GetServiceDetailsRequest();
         request.setServiceCode(serviceCode);
         ServiceDetailsResponse serviceResponse = spaServcieManager.queryServiceDetails(request);
         if(serviceResponse.getSuccess()){
        	 logger.debug("serviceDetail return from API:" + serviceResponse);
         }
         ServiceDetail serviceDetail = serviceResponse.getServiceDetail();
         detailDto.setServiceName(serviceDetail.getName());
         
         String therapistCode = record.getTherapistCode();
         Therapist therapist = (Therapist)getTherapistInfo(therapistCode, new Date());
         if(logger.isDebugEnabled()){
        	 logger.debug("therapist information:" + therapist);
         }
         detailDto.setTherapistName(therapist.getName());
         
         DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         
         detailDto.setStartDatetime(df.format(record.getStartDatetime()));
         detailDto.setEndDatetime(df.format(record.getEndDatetime()));
         detailDto.setServiceCost(record.getServiceInternalCost());    
         detailList.add(detailDto);            
         
         appointmentDto.setServiceDetails(detailList);
         responseResult.initResult(GTAError.Success.SUCCESS, appointmentDto);
		
		if(logger.isDebugEnabled()){
			logger.debug("getLatestAppointmentDetail end.");			
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult canCelAppointment(String invoiceNo, String userId) throws Exception{
	
	  try
	  {
		InvoiceCancelRequest request = new InvoiceCancelRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentCancelResponse response = spaServcieManager.appointmentCancel(request);
		if(!response.getSuccess()){
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
		 return cancelLocalAppointment(invoiceNo,userId, response);
		}
	  }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
		
	}
	
	@Transactional
	public ResponseResult canCelAppointmentService(String appointmentId, String userId) throws Exception
	{		
		if(logger.isDebugEnabled())
		{
			logger.debug("cancelAppointmentService start.");
			logger.debug("appointmentId:" + appointmentId);
		}
		  try
		  {
			  AppointmentCancelRequest request = new AppointmentCancelRequest();
			  request.setAppointmentId(appointmentId);
			  request.setComments("cancelled by HKGTA request");
			  AppointmentServiceCancelResponse response = spaServcieManager.appointmentServiceCancel(request);
			if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else
			{		
				responseResult = cancelLocalService(appointmentId,userId);				
			}
		  }catch(ResourceAccessException e)
			{
				if (e.getCause() instanceof java.net.ConnectException) {
					responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
				   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
					   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
				   }else
				   {
					   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
				   }
			}catch(MMSAPIException e)
			{
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			}
			return responseResult;
			
		}
	
	
	@Transactional
	private ResponseResult cancelLocalService(String appointmentId, String userId)
	{
		try
		{
			SpaAppointmentRec appointment = spaAppointmentRecDao.getUniqueByCol(SpaAppointmentRec.class,"extAppointmentId", appointmentId);
			if (appointment != null) {
				appointment.setStatus(Constant.Status.CAN.name());
				appointment.setUpdateBy(userId);
				appointment.setUpdateDate(new Timestamp(new Date().getTime()));
				spaAppointmentRecDao.update(appointment);
			}
			String invoiceNO = appointment.getExtInvoiceNo();
			List<SpaAppointmentRec> appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo",invoiceNO, null);
			boolean isCancelOrder = true;
			if (appointmentList.size() > 1) {
				for (SpaAppointmentRec record : appointmentList) {
					if (!record.getExtAppointmentId().equals(appointment.getExtAppointmentId())) {
						if (!record.getStatus().equals(Constant.Status.CAN.name())) {
							isCancelOrder = false;
							break;
						}
					}
				}
			}
			if (isCancelOrder) {
				List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNO, null);
				CustomerOrderHd customerOrderHd = new CustomerOrderHd();
				if (customerOrderHdList != null
						&& customerOrderHdList.size() > 0) {
					customerOrderHd = customerOrderHdList.get(0);
				}

				customerOrderHd.setOrderStatus(Constant.Status.CAN.name());
				customerOrderHd.setUpdateBy(userId);
				customerOrderHd.setUpdateDate(new Date());
				customerOrderHdDao.update(customerOrderHd);
			}
			responseResult.initResult(GTAError.Success.SUCCESS);
			
//			List<SpaAppointmentRec> messageAppointmentList = new ArrayList<SpaAppointmentRec>();
//			messageAppointmentList.add(appointment);
//			sentMessage(messageAppointmentList, SceneType.WELLNESS_CANCEL_SERVICE);
			
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION, "Cancel local service failed.");
		}
		return responseResult;
	}
	
	
	@Transactional
	private ResponseResult cancelLocalAppointment(String invoiceNo,String userId, AppointmentCancelResponse response) throws Exception{
		
		if(logger.isDebugEnabled()){
			logger.debug("MMS cancelLocalAppointment start.");
		}
		
		List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNo, null);
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		if (customerOrderHdList != null && customerOrderHdList.size() > 0) {
			customerOrderHd = customerOrderHdList.get(0);
		}				
		if(customerOrderHd.getOrderStatus().equals(Constant.Status.CMP.name())){
			List<CustomerOrderDet> customerOrderDetList = customerOrderHd.getCustomerOrderDets();
			for(CustomerOrderDet detail : customerOrderDetList){
				generateRefundRequester(userId,detail);
			}
		}
		customerOrderHd.setOrderStatus(Constant.Status.CAN.name());	
		customerOrderHd.setUpdateBy(userId);
		customerOrderHd.setUpdateDate(new Date());
		customerOrderHdDao.update(customerOrderHd);		
		
		
		List<SpaAppointmentRec> appointList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo, null);
		for(SpaAppointmentRec appoint : appointList)
		{
			appoint.setStatus(Constant.Status.CAN.name());
			appoint.setUpdateBy(userId);
			appoint.setUpdateDate(new Timestamp(new Date().getTime()));
			spaAppointmentRecDao.update(appoint);
		}
		
		if(logger.isDebugEnabled()){
			logger.debug("MMS cancelLocalAppointment end.");
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		responseResult.setData(invoiceNo);
		
//		sentMessage(appointList, SceneType.WELLNESS_CANCEL_APPOINTMENT);
		
		return responseResult;
	}
	
   private void generateRefundRequester(String userId, CustomerOrderDet detail) {
		
		CustomerRefundRequest customerRefundRequest = new CustomerRefundRequest();
		customerRefundRequest.setRequesterType("HKGTA");
		customerRefundRequest.setStatus("PND");
//		customerRefundRequest.
		customerRefundRequest.setCreateBy(userId);
		customerRefundRequest.setCreateDate(new Date());
		customerRefundRequest.setRefundServiceType("WELLNESS");
		customerRefundRequest.setOrderDetailId(detail.getOrderDetId());
		customerRefundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
		customerRefundRequest.setUpdateBy(userId);
		customerRefundRequest.setUpdateDate(new Date());
		CustomerOrderHd order = detail.getCustomerOrderHd();
		List<CustomerOrderTrans> txns = order.getCustomerOrderTrans();
		if (txns.size() > 0) {
			customerRefundRequest.setRefundTransactionNo(txns.get(0).getTransactionNo());
			customerRefundRequest.setRequestAmount(txns.get(0).getPaidAmount());
		}
		customerRefundRequestDao.save(customerRefundRequest);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public ResponseResult payment(MMSPaymentDto paymentDto) throws Exception{
			
	 try
	 {
		String paymentMethod = paymentDto.getPaymentMethod();
	    String invoiceNo = paymentDto.getInvoiceNo();
	    
	    if(logger.isDebugEnabled())
	    {
	    	logger.debug("invoiceNo:" + invoiceNo + " paymentMethod:" + paymentMethod);
	    }
	    //for MMS remote call, if the appointment is not exist, create local appointment.
	    List<SpaAppointmentRec> appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		if(appointmentList ==null || appointmentList.size()<=0)
		{
			synchronizeRemoteAppoint(invoiceNo);
		}
		if(StringUtils.isEmpty(paymentMethod)){
			paymentDto.setPaymentMethod(Constant.CASH_Value);
		}
        	
		MMSPaymentDto result = mmsPayment(paymentDto);
		if (result.getPaymentMethod().equals(Constant.CREDIT_CARD))
		{
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, result.getTransNo());
			String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
			result.setRedirectUrl(redirectUrl);
		}			
		if(logger.isDebugEnabled()){
			
			logger.debug("order_no:" + result.getOrderNo() + " transNo:" + result.getTransNo());
		}
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		
		if (result.getPaymentMethod().equals(Constant.CASH_Value))
		{
			// for remote payment from MMS invoke, the initial appointment list is null, need get data again.
			if("Y".equals(paymentDto.getIsRemote()))
			{
				appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
			}
		  sentPaymentReceipt(result, false);//for webporatal payment, need send receipt email to user.
		  sentMessage(appointmentList, SceneType.WELLNESS_PAYMENT_SETTLE); //send confirm message
		}
		
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		
		return responseResult;
	}
	
//	@Transactional
	private void synchronizeRemoteAppoint(String invoiceNo)throws Exception
	{
		if(logger.isDebugEnabled()){
			logger.debug("synchronizeRemoteAppoint for MMS payment by cashValue.");
		}
		AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentDetailsResponse  response = spaServcieManager.appointmentDetails(request); 		
		
		if(response!=null && response.getSuccess())
		{
		   if(logger.isDebugEnabled())
		   {
			logger.debug("AppointmentDetailsResponse from MMS:" + response.toString());
		    }
		}else
		{
			logger.error("Get appointment data from MMS failed.");
			throw new Exception("Get appointment data from MMS failed.");
		}
		GuestDetails guestDetail = response.getGuestDetails();
		String guestCode = guestDetail.getUsercode();
		List<AppointmentDetail> detailList = response.getAppointmentDetails();
		
		Member member  = new Member();
		 member = memberDao.getMemberByAcademyNo(guestCode);
		 
		 if(member== null || StringUtils.isEmpty(member.getAcademyNo()))
		 {
			 logger.error("Member not found in HKGTA.");
			 throw new Exception("Member not found in HKGTA.");
		 }
		SpaAppointmentDto localDto = new SpaAppointmentDto();
		
		List<SpaAppointmentDetailDto> detailDtoList = new ArrayList<SpaAppointmentDetailDto>();
		
		localDto.setCustomerId(member.getCustomerId());
		localDto.setInvoiceNo(invoiceNo);
		localDto.setPaymentMethod(Constant.CASH_Value);
		
		SimpleDateFormat sdformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		
		if(detailList!=null && detailList.size()>0)
		{
			for(AppointmentDetail detail : detailList)
			{
				SpaAppointmentDetailDto detailDto = new SpaAppointmentDetailDto();
				detailDto.setServiceCode(detail.getAdditionalProperties().get("servicecode").toString());
				detailDto.setServiceName(detail.getItemName());
				detailDto.setServiceTime(Long.valueOf(detail.getSrvcTime().toString()));
				detailDto.setServiceDescription(detail.getItemName());
//				detailDto.setTherapistCode();
				Date startDatetime = sdformat.parse(detail.getStarttime());
				Date endDatetime = sdformat.parse(detail.getEndtime());	
				String startTimeStr = df.format(startDatetime);
				String endTimeStr = df.format(endDatetime);
				
				if(logger.isDebugEnabled()){
					logger.debug("startTime:" + startTimeStr + "  endTime:" + endTimeStr);
				}
				detailDto.setStartDatetime(startTimeStr);
				detailDto.setEndDatetime(endTimeStr);
				
				detailDto.setServiceCost(new BigDecimal(detail.getFinalPrice().doubleValue()));
				detailDto.setTherapistName(detail.getTherapistName());
				detailDtoList.add(detailDto);							
			}
		}
		localDto.setServiceDetails(detailDtoList);
				
		createLocalAppointment(localDto, null);
	}
	
	
//	@Transactional
	private MMSPaymentDto mmsPayment(MMSPaymentDto paymentDto)throws Exception{

		if(logger.isDebugEnabled()){
			logger.debug("mmsPayment start.");
		}	
		
		Long customerId = paymentDto.getCustomerId();
		String invoiceNo = paymentDto.getInvoiceNo();
		String paymentMethod = paymentDto.getPaymentMethod();
		String isRemote = paymentDto.getIsRemote();
		AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentDetailsResponse  appointmentDetailResponse = spaServcieManager.appointmentDetails(request); 
		
		if(logger.isDebugEnabled()){
			logger.debug("appointmentResponse for payment:" + appointmentDetailResponse);
		}
		
		if(!appointmentDetailResponse.getSuccess()){
			throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION, appointmentDetailResponse.getMessage());		
		}
		List<AppointmentDetail> detailList = appointmentDetailResponse.getAppointmentDetails();
		GuestDetails guestDetail = appointmentDetailResponse.getGuestDetails();
		String guestCode = guestDetail.getUsercode();
		
		double totalAmount = 0;
		Member member  = new Member();
		if("N".equals(isRemote))//payment by hkgta webportal
		{
			for (AppointmentDetail detail : detailList) {
				totalAmount = totalAmount + detail.getFinalPrice().doubleValue();
			}
			 member = memberDao.getMemberByCustomerId(paymentDto.getCustomerId());
			 
		}else if("Y".equals(isRemote)){ //payment by MMS remote API.
			
		   totalAmount = paymentDto.getAmount().doubleValue();
		   member = memberDao.getMemberByAcademyNo(guestCode);
		   customerId = member.getCustomerId();
		}
		BigDecimal amount = new BigDecimal(totalAmount);		
		
		if(logger.isDebugEnabled())
		{
			logger.debug("payment amount:" + amount);
		}
		String remoteTransId = "";
				
		// CUSTOMER ORDER Header
		List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNo, null);
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		if (customerOrderHdList != null && customerOrderHdList.size() > 0) {
			customerOrderHd = customerOrderHdList.get(0);
		}
		
		//for cash value, update the member cash value balance.
				if(Constant.CASH_Value.equals(paymentMethod)){

					if("N".equals(isRemote))
					{
						PaymentByCustomRequest parameters = new PaymentByCustomRequest();
						parameters.setInvoiceNo(invoiceNo);
						parameters.setAmountPaid(amount);
						parameters.setTipAmount(new BigDecimal(0));
						parameters.setPaymentMethod(CustomPaymentMethod.CASHVALUE);
						
						PaymentByCustomResponse response = spaServcieManager.paymentByCustom(parameters);
						
						if(!response.getSuccess()){
							throw new GTACommonException(null, response.getMessage());				
						} 
						if (logger.isDebugEnabled()) {
							logger.debug("payment response from API:" + response);
						}		
						remoteTransId = response.getTransactionID();
					}				
					
					MemberCashValuePaymentDto cashValuePaymentDto = new MemberCashValuePaymentDto();
					cashValuePaymentDto.setCustomerId(customerId);										
					cashValuePaymentDto.setOrderQty(amount.intValue());
					cashValuePaymentDto.setPaymentMethod(paymentMethod);					
					cashValuePaymentDto.setTotalAmount(amount);
					cashValuePaymentDto.setUserId(member.getUserId());

					MemberCashvalue cashValue = memberCashvalueService.updateMemberCashValueData(member, cashValuePaymentDto);										
					
				}
				
//		if(Constant.CASH_Value.equals(paymentMethod) && amount.compareTo(customerOrderHd.getOrderTotalAmount())>=0){
			//keep the same status with MMS, the appointment only can be closed in MMS .
		customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		 
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setUpdateBy(member.getUserId());
		customerOrderHd.setUpdateDate(new Date());
		customerOrderHdDao.update(customerOrderHd);

		paymentDto.setOrderNo(customerOrderHd.getOrderNo().toString());		
				
	    CustomerOrderTrans customerOrderTrans  = new CustomerOrderTrans();
	    
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setTransactionTimestamp(new Date());
		customerOrderTrans.setPaidAmount(amount);
			
		if(Constant.CASH_Value.equals(paymentMethod)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CV.name());
		}else if(Constant.CREDIT_CARD.equals(paymentMethod)){
			customerOrderTrans.setStatus(Constant.Status.PND.name());	
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
		   }		
		customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		customerOrderTrans.setPaymentRecvBy(member.getAcademyNo());
		customerOrderTrans.setAgentTransactionNo(remoteTransId);			
		customerOrderTrans.setCreateBy(member.getUserId());
		customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		customerOrderTrans.setExtRefNo(invoiceNo);
		Long transNo = (Long)customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);	

		CustomerOrderTrans result = customerOrderTransDao.getUniqueByCol(CustomerOrderTrans.class, "transactionNo", transNo);
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		paymentDto.setTransNo(result.getTransactionNo());
		paymentDto.setSettleTime(df.format(result.getTransactionTimestamp()));
			
			
		return paymentDto;

	}
	
	@Override
	public Therapist getTherapistInfo(String therapistCode, Date requestDate){
		
	 try
	 {
		GetCenterTherapistsRequest request = new GetCenterTherapistsRequest();
		request.setRequestDate(requestDate);
		QueryCenterTherapistsResponse response =  spaServcieManager.getCenterTherapists(request);
		if(!response.getSuccess()){
			throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
			List<Therapist> therapistList = response.getTherapists(); 
			
			for(Therapist therapist : therapistList){
				
				String userCode = therapist.getUserCode();
				if(StringUtils.isEmpty(userCode)  && therapist.getEmployeeCode() !=null)
				{
                   userCode = therapist.getEmployeeCode();
				}
				if(therapistCode.equals(userCode)){
					return therapist;
				}
			}
		}			
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				throw new GTACommonException(GTAError.FacilityError.MMS_CONNECTION_FAILED);		    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   throw new GTACommonException(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		
		return null;
	}
	
	@Override
	@Transactional
	public ResponseResult getMemberInfo(String customerId) throws Exception{
	    
	    MemberCashvalueDto memberCashvalueDto = courseService.getMemberInfo(customerId);		
		responseResult.initResult(GTAError.Success.SUCCESS, memberCashvalueDto);
		return responseResult;
	
	}
	
	@Override
	public String aesEncode(String res, String key)
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("Content for encode:" + res);
		}
	 	String encrypt = EncryptUtil.getInstance().AESencode(res, key);
	 	
	 	if(logger.isDebugEnabled())
		{
			logger.debug("Content encoded:" + encrypt);
		}
	 	return encrypt;
	}
	
	@Override
	public String aesDecode(String res, String key)
	{
	 	String encrypt = EncryptUtil.getInstance().AESdecode(res, key);
	 	
	 	return encrypt;
	}
	
	@Override
	@Transactional
	public SpaCenterInfoDto getSpaCenterInfo()
	{
		SpaCenterInfo spaCenterInfo =  spaCenterInfoDao.getSpaCenterInfo();
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
		
		SpaCenterInfoDto dto = new SpaCenterInfoDto();
		dto.setAdvanceBookPeriod(globalParameter.getParamValue());
		dto.setAppPicFilename(spaCenterInfo.getAppPicFilename());
		dto.setCenterId(spaCenterInfo.getCenterId());
		dto.setCenterName(spaCenterInfo.getCenterName());
		dto.setLocation(spaCenterInfo.getLocation());
		dto.setOpenHour(spaCenterInfo.getOpenHour());
		dto.setPhone(spaCenterInfo.getPhone());
		
		return dto;
		
	}
	
	@Override
	@Transactional
	public ResponseResult updateSpaCenterInfo(SpaCenterInfoDto spaCenterInfoDto, String userId )
	{
//		SpaCenterInfo entity = new SpaCenterInfo();			
		if(logger.isDebugEnabled())
		{
			logger.debug("updateSpaCenterInfo start.");
			logger.debug("username:" + userId);
		}
		SpaCenterInfo  entity = spaCenterInfoDao.getSpaCenterInfo();
		if(entity == null)
		{
			entity = new SpaCenterInfo();
			entity.setCreateBy(userId);
			entity.setCreateDate(new Timestamp(new Date().getTime()));			
		}else
		{
			entity.setUpdateBy(userId);
			entity.setUpdateDate(new Timestamp(new Date().getTime()));
		}
		
		entity.setLocation(spaCenterInfoDto.getLocation());
		entity.setOpenHour(spaCenterInfoDto.getOpenHour());
		entity.setPhone(spaCenterInfoDto.getPhone());
		entity.setAppPicFilename(spaCenterInfoDto.getAppPicFilename());
		entity.setCenterName(spaCenterInfoDto.getCenterName());
				
		spaCenterInfoDao.saveOrUpdate(entity);		
		
		String advanceBookPeriod = spaCenterInfoDto.getAdvanceBookPeriod();
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
		globalParameter.setParamValue(advanceBookPeriod);
		globalParameter.setUpdateBy(userId);
		globalParameter.setUpdateDate(new Date());
		globalParameterDao.update(globalParameter);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult updateSpaCategoryPic(List<SpaPicPathDto> dtoList, String userId)
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("updateSpaCategoryPic start.");
		}
		if(dtoList!=null && dtoList.size()>0)
		{
			for(SpaPicPathDto dto : dtoList)
			{
				Long picId = dto.getPicId();
				SpaPicPath picPath = spaPicPathDao.getByPicId(picId);
				picPath.setCategoryFileName(dto.getPicFileName());
				picPath.setUpdateBy(userId);
				picPath.setUpdateDate(new Timestamp(new Date().getTime()));
				spaPicPathDao.update(picPath);
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public List<SpaCategoryDto> getSpaCategoryList()
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("getSpaCategoryList start.");
		}
		List<SpaCategory> categoryList = spaCategoryDao.getAllSpaCategorys();
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		
		if(categoryList!=null && categoryList.size()>0)
		{
			for(SpaCategory category : categoryList)
			{
				SpaCategoryDto dto = new SpaCategoryDto();
				dto.setCategoryId(category.getCategoryId());
				dto.setCategoryName(category.getCategoryName());
				
				List<SpaPicPath> picList = category.getSpaPicPaths();				
				
				List<SpaPicPathDto> picDtoList = new ArrayList<SpaPicPathDto>();
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPath pic : picList)
					{
						SpaPicPathDto picDto = new SpaPicPathDto();
						picDto.setPicFileName(pic.getCategoryFileName());
						picDto.setPicId(pic.getPicId());
						picDto.setPicType(pic.getPicType());
						picDtoList.add(picDto);
					}
				}
				
				dto.setCategoryPicList(picDtoList);
				
				resultList.add(dto);
			}
		}
		return resultList;
	}
	
	@Override
	@Transactional
	public Long getLatestUpdatedPicId()
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("getLatestUpdatedPicId start.");
		}
		List<SpaPicPath> picList = spaPicPathDao.getByHql("from SpaPicPath order by updateDate desc");
		if(picList!=null && picList.size()>0)
		{
			return picList.get(0).getPicId();
		}else
		{
			return null;
		}
	}
	
	@Transactional
	public void updateSpaRetreat(SpaRetreatDto dto, String userId)
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("updateSpaRetreat start.");
		}
		
		SpaRetreat retreat = null;
		if(dto.getRetId()!=null)
		    retreat = spaRetreatDao.getByRetId(dto.getRetId());
		
		if(retreat == null)
		{	 
			retreat = new SpaRetreat();		
			retreat.setCreateBy(userId);
			retreat.setStatus("Show");
			retreat.setDisplayOrder(spaRetreatDao.getMaxDisplayOrder()+1);
			retreat.setCategoryId("RETREATS001");
			retreat.setCreateDate(new Timestamp(new Date().getTime()));
		}else
		{
			retreat.setUpdateBy(userId);
			retreat.setUpdateDate(new Timestamp(new Date().getTime()));			
		}
		if(!StringUtils.isEmpty(dto.getStatus())) //for change status
		{
		  retreat.setStatus(dto.getStatus());
		}else
		{
			retreat.setDescription(dto.getDescription());
			retreat.setPicPath(dto.getPicPath());
			retreat.setRetreatName(dto.getRetName());	
		}		
		
		spaRetreatDao.saveOrUpdate(retreat);
		
		if(logger.isDebugEnabled())
		{
			logger.debug("updateSpaRetreat end.");
		}
	}
	
	@Transactional
	public void updateSpaRetreatItem(SpaRetreatItemDto dto, String userId)
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("updateSpaRetreatItem start.");
		}
		SpaRetreatItem item = null;
		if(dto.getItemId()!=null)
		 item = spaRetreatItemDao.getByItemId(dto.getItemId());
		
		if(item == null)
		{
			item = new SpaRetreatItem();
			item.setCreateBy(userId);
			item.setCreateDate(new Timestamp(new Date().getTime()));
			item.setStatus("Show");
			SpaRetreat retreat = spaRetreatDao.getByRetId(dto.getRetId());
			item.setSpaRetreat(retreat);
			
		}else
		{
			item.setUpdateBy(userId);
			item.setUpdateDate(new Timestamp(new Date().getTime()));
		}
		if(!StringUtils.isEmpty(dto.getStatus()))
		{
		   item.setStatus(dto.getStatus());
		}else
		{
			item.setDescription(dto.getDescription());
			 item.setItemName(dto.getItemName());
		}
		
		spaRetreatItemDao.saveOrUpdate(item);		
		
		if(logger.isDebugEnabled())
		{
			logger.debug("updateSpaRetreatItem end.");
		}
		
	}
	
	@Transactional
	public String getSpaRetreatListSql()
	{
		String sql = "select * from ( select a.ret_id as retId, ret_name as retName, a.status, a.description, pic_path as picPath, a.display_order as displayOrder,"
				+ " (select count(1) from spa_retreat_item b where b.ret_id = a.ret_id) as itemSize "
				+ " from spa_retreat a) m ";
		
		if(logger.isDebugEnabled())
		{
			logger.debug("sql:" + sql);
		}
		return sql;
	}
	
	@Transactional
	public String getSpaRetreatItemListSql(Long retId)
	{
		String sql = "select * from ( select item.item_id as itemId, item.item_name as itemName, "
				+ " item.status as status, item.description as description, r.ret_name as retreatName"
				+ " from spa_retreat_item item, spa_retreat r"
				+ " where item.ret_id = '" + retId + "' and item.ret_id = r.ret_id ) a";
		
		if(logger.isDebugEnabled())
		{
			logger.debug("sql:" + sql);
		}
		return sql;
	}
	
	@Transactional
	public void switchRetreatOrder(Long sourceRetId, String moveType, String userId)
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("switchRetreatStatus start.");
		}
		if("top".equals(moveType))
		{
			SpaRetreat sourceRet = spaRetreatDao.getByRetId(sourceRetId);
			String hql = "from SpaRetreat t where t.displayOrder <= ?";
			List param = new ArrayList();
			param.add(sourceRet.getDisplayOrder());
			List<SpaRetreat> retreatList = spaRetreatDao.getByHql(hql,param);
			if(retreatList!=null && retreatList.size()>0)
			{
				for(SpaRetreat retreat:retreatList)
				{
					if(retreat.getRetId().longValue() == sourceRetId.longValue())
					{
						retreat.setDisplayOrder(new Long(1));
					}else
					{
						Long displayOrder = retreat.getDisplayOrder();
						Long newOrder = Long.valueOf(displayOrder.longValue() + 1);
						retreat.setDisplayOrder(newOrder);
					}
					spaRetreatDao.update(retreat);
				}
			}
		}else
		{
			SpaRetreat source = spaRetreatDao.getByRetId(sourceRetId);
			
			Long sourceOrder = source.getDisplayOrder();
			Long targetOrder = null;
			 if("up".equals(moveType))
			 {
			    targetOrder = Long.valueOf(sourceOrder.longValue() -1);
			 }else //move down
			 {
				 targetOrder = Long.valueOf(sourceOrder.longValue() +1);
			 }
			
			SpaRetreat target = spaRetreatDao.getUniqueByCol(SpaRetreat.class, "displayOrder", targetOrder);		

			source.setDisplayOrder(targetOrder);
			target.setDisplayOrder(sourceOrder);

			spaRetreatDao.update(source);
			spaRetreatDao.update(target);
		}
		
		if(logger.isDebugEnabled())
		{
			logger.debug("switchRetreatStatus end.");
		}
	}
	
	@Transactional
	public ResponseResult sendReceipt(String invoiceNo)throws Exception
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("sendReceipt start.");
		}
		MMSPaymentDto dto = new MMSPaymentDto();
		dto.setInvoiceNo(invoiceNo);
		CustomerOrderHd order = this.customerOrderHdDao.getUniqueByCol(CustomerOrderHd.class, "vendorRefCode", invoiceNo);
		if(order!=null)
		{
			dto.setOrderNo(order.getOrderNo().toString());
			sentPaymentReceipt(dto, false);
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else
		{
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION,"No order record found for this reservation.");
		}
		if(logger.isDebugEnabled())
		{
			logger.debug("sendReceipt end.");
		}
		return responseResult;
	}
	
}
