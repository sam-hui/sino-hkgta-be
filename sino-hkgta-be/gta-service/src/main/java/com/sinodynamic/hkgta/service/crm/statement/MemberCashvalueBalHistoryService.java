package com.sinodynamic.hkgta.service.crm.statement;

import com.sinodynamic.hkgta.dto.statement.SearchStatementsDto;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberCashvalueBalHistoryService extends IServiceBase<MemberCashvalueBalHistory> {

	public ResponseResult getStatements(ListPage page, SearchStatementsDto dto);

}
