package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class SysCodeServiceImpl extends ServiceBase<SysCode> implements SysCodeService {
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Transactional
	public List<SysCode> getSysCodeByCategory(String category) throws Exception {
		
		return sysCodeDao.selectSysCodeByCategory(category);
	}

	@Transactional
	public List<SysCode> getAllSysCodeCategory() throws Exception {
		
		return sysCodeDao.selectAllSysCodeCategory();
	}

	@Override
	@Transactional
	public List<SysCode> getSysCodeByMutipleCategory(String category) throws Exception
	{
		return sysCodeDao.getSysCodeByMutiCategory(category);
	}
}
