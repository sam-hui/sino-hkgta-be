package com.sinodynamic.hkgta.service.crm.backoffice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.DepartmentDao;
import com.sinodynamic.hkgta.dao.adm.PositionTitleDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;
import com.sinodynamic.hkgta.entity.crm.PositionTitle;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class DepartmentServiceImpl extends ServiceBase implements DepartmentService {
	
	@Autowired
	private DepartmentDao departmentDao;
	
	@Autowired
	private PositionTitleDao positionTitleDao;

	@Override
	@Transactional
	public List<DepartmentBranch> getAllDepartments() throws Exception
	{
		return departmentDao.getAllDepartments();
	}

	@Override
	@Transactional
	public List<PositionTitle> getAllPositionTitle(Long departId) throws Exception
	{
		return positionTitleDao.getAllPositionTitle(departId);
	}
}
