
package com.sinodynamic.hkgta.service.crm.sales.presentation;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.PresentMaterialDto;
import com.sinodynamic.hkgta.dto.crm.PresentationWithMaterialDto;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.OrderByType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface PresentationService extends IServiceBase<PresentationBatch>{
	
	public PresentationBatch getPresentation(PresentationBatch t) throws Exception;
	
	public ResponseMsg savePresentation(PresentationWithMaterialDto t) throws Exception;
	
	public ResponseResult updatePresentation(PresentationWithMaterialDto ptDto) throws Exception;
	
	public PresentationBatch getPresentationById(Long presentId) throws Exception;
	
	public String convert(Long presentId);	
	
	/**
	 * @author Junfeng_Yan
	 * @param t
	 * @throws Exception
	 */
	public ResponseMsg deletePresentation(PresentationBatch t) throws Exception;

	/**
	 * This method is used to retrieve list of presentation folders
	 * @return List of presentation folders
	 * @throws Exception
	 * @author Vineela_Jyothi
	 */
	public List<ServerFolderMapping> getTemplateFolderList()throws Exception;
	
	public List<PresentMaterial> getFolderDetails(Long folderId) throws Exception;
	
	public ListPage<PresentationBatch> getPresentationList(String createBy, OrderByType orderbyType, boolean isDesc, String presentationName, ListPage<PresentationBatch> pListPage) throws Exception;
	
	public Long cloneById(Long presentId, String presentName,String description, String createBy) throws Exception;

	public int getPresentSeq(Long materialId,Long presentId);
	
	public ServerFolderMapping getServerFolderMappingById(Long id);
	
	public List<PresentMaterialDto> getPresentMaterials(List<PresentMaterialSeq> seqs) throws Exception;
	
	public PresentationWithMaterialDto getPresentDto(PresentationBatch present, List<PresentMaterialDto> presentMaterials);
	
	public List<PresentMaterialSeq> getPresentMaterialSeqByPresentId(Long presentId);
	
}

