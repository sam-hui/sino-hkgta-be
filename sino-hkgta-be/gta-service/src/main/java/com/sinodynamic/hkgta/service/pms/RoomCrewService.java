package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.RoomCrewDto;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RoomCrewService extends IServiceBase<RoomCrew> {

	public List<RoomCrewDto> getRoomCrewList(Long roomId);
	
	public void removeRoomCrew(Long crewId);
	
	public void assignRoomCrew(RoomCrewDto roomCrewDto);

	public List<RoomCrew> getRoomCrewList(Long roomId,String crewRoleId);
}
