package com.sinodynamic.hkgta.service.rpos;

import java.util.List;

import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CustomerOrderDetService extends IServiceBase<CustomerOrderDet> {
	
	public CustomerOrderDet getCustomerOrderDet(Long orderNo);
}
