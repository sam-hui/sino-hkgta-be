package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.CourseSessionDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDto;
import com.sinodynamic.hkgta.dto.fms.MemberCourseAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.SessionMemberDto;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CourseSessionServiceImpl extends ServiceBase<Serializable> implements CourseSessionService {
	
	@Autowired
	private CourseSessionDao courseSessionDao;
	
	@Transactional
	public ResponseResult getCoachSessionList(String staffId, Date fromDate, Date endDate) {
		
		if (StringUtils.isEmpty(staffId) || fromDate == null || endDate == null) return null;
		
		List<CourseSessionDto> result = courseSessionDao.getCoachSessionList(staffId, fromDate, endDate);
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}

	@Transactional
	public ResponseResult getAttendanceMemberList(String sessionId) {
		
		if (StringUtils.isEmpty(sessionId)) return null;
		
		List<MemberCourseAttendanceDto> result = courseSessionDao.getMemberAttendanceList(sessionId);
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
		
	}

	@Override
	@Transactional
	public ResponseResult getSessionMembers(String courseId, String sessionNo) {
	    
	    List<SessionMemberDto> result = courseSessionDao.getStudentAttendanceInfo(courseId, sessionNo);
	    responseResult.initResult(GTAError.Success.SUCCESS, result);
	    return responseResult;
	}
	
	
}
