package com.sinodynamic.hkgta.service.fms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityAvailabilityDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemStatusDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterChangeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterQueryDto;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface FacilityMasterService extends IServiceBase<FacilityMaster>
{
	public List<FacilityMaster> getFacilityMasterList(Integer venueFloor, String facilityType);

	public List<FacilityMasterQueryDto> getFacilityMasterList(String facilityType);

	public ResponseResult getFacilityStatus(FacilityItemStatusDto facilityItemStatusDto);

	public ResponseResult saveFacilityStatus(FacilityItemStatusDto facilityItemStatusDto) throws Exception;

	public List<FacilityMaster> getFacilityVenueFloorList(String facilityType, String venueCode);

	public void changeFacilityTimeslot(FacilityMasterChangeDto facilityMasterChangeDto) throws Exception;

	public ResponseResult getReservationInfoByCustomerId(long customerId);

	int countFacilityMasterByTypeAndAttribute(String facilityType, String facilityAttribute,List<Long> facilityNos);
	
	int countFacilityMasterByTypeAndFloors(String facilityType,List<Long> floors);
	
	int countFacilityMasterByTypeAndFacilityNos(String facilityType,List<Long> facilityNos);

	List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility) throws Exception;
	
	List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility,List<Long> floors) throws Exception;
	
	public List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility,  List<Long> floors,boolean checkQuota) throws Exception;
	
	public List<FacilityAvailabilityDto> getFacilityAvailabilityByFacilityNos(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility,  List<Long> facilityNos,boolean checkQuota) throws Exception;
	
	List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute) throws Exception;
	
	List<FacilityAvailabilityDto> getFacilityAvailability(String coachId, String facilityType, Date specifiedDate, String facilityAttribute) throws Exception;
	
	public List<BayTypeDto> getFacilityBayType(String facilityType);
	
	public FacilityMaster getFacilityMasterbyFacilityNo(Integer facilityNo);
	
	public List<FacilityMaster> getFacilityByAttribute(String facilityType, String facilityAttribute);
	
	public List<FacilityItemDto> getFacilityStatusByFloorAndTime(String facilityType, String beginDatetime, String endDatetime, Long floor);

	public void setBallFeedingMachineStatus(List<FacilityMasterQueryDto> facilityMasterList,Map<String, String> statusMap) throws Exception;
	
}
