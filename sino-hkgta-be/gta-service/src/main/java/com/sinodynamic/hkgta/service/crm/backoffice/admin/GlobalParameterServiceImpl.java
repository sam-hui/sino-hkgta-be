package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class GlobalParameterServiceImpl extends ServiceBase<GlobalParameter> implements GlobalParameterService {

	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	
	@Override
	@Transactional
	public ResponseResult saveGlobalParameter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public ResponseResult getGlobalParameterById(String paramId) {
		ResponseResult result = new ResponseResult();	
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, paramId);
			if(null != globalParameter)globalParameter.setUserPreferenceSettings(null);
			result = new ResponseResult("0", "success", globalParameter);
		} catch (HibernateException e) {
			e.printStackTrace();
			result = new ResponseResult("-1", "", "fail");
			
		}
		return result;
	}

	@Override
	@Transactional
	public ResponseResult setGlobalQuota(String paramValue, String userId) {
		// TODO Auto-generated method stub
		ResponseResult result = null;	
		String paramId = "DPDAYQUOTA";
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, paramId);
			globalParameter.setParamValue(paramValue);
			globalParameter.setUpdateBy(userId);
			globalParameterDao.saveOrUpdate(globalParameter);
			return result = new ResponseResult("0", "cheng gong", "success");
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return result = new ResponseResult("-1", "", "fail");
		}

	}

	@Override
	@Transactional
	public GlobalParameter getGlobalParameter(String paramId) {
		return globalParameterDao.get(GlobalParameter.class, paramId);
	}
	@Override
	@Transactional
	public List<GlobalParameter> getGlobalParameterListByParamCat(String paramCat){
		String hqlstr = "FROM GlobalParameter g where g.paramCat = ? order by g.displayOrder";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(paramCat);
		List<GlobalParameter> list = this.globalParameterDao.getByHql(hqlstr, param);
		if(list!=null&&list.size()>0){
			for(GlobalParameter g : list){
				g.setUserPreferenceSettings(null);
			}
		}else{
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"GlobalParameter "+paramCat});
		}
		return list;
	}
	@Override
	@Transactional
	public void saveOrUpdateGlobalParameters(List<GlobalParameter> globalParameters){
		for(GlobalParameter g : globalParameters){
			this.globalParameterDao.saveOrUpdate(g);
		}
	}

	@Override
	@Transactional
	public ListPage<GlobalParameter> getGlobalParameterList(String paramCat, String sortBy, Boolean isAscending, ListPage<GlobalParameter> pListPage)
	{
		StringBuffer hql = new StringBuffer();
		StringBuffer hqlCount = new StringBuffer();
		hql.append("select p from GlobalParameter p where p.paramCat = ? ");
		hqlCount.append("select count(*) from GlobalParameter p where p.paramCat = ? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(paramCat);
		String orderByCol = "";
		switch (sortBy)
		{
		case "paramId":
			orderByCol = "p.paramId";
			break;
		case "paramValue":
			orderByCol = "p.paramValue";
			break;
		}
		if (!isAscending)
		{
			pListPage.addDescending(orderByCol);
		} else
		{
			pListPage.addAscending(orderByCol);
		}
		pListPage = globalParameterDao.getGlobalParameterList(pListPage, hql.toString(), hqlCount.toString(), param);
		return pListPage;
	
	}

	@Override
	@Transactional
	public void updateGlobalParameter(GlobalParameter globalParameter)
	{
		globalParameterDao.update(globalParameter);
	}
}
