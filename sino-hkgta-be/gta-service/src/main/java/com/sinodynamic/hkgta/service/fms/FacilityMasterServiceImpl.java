package com.sinodynamic.hkgta.service.fms;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.StaffCoachRoasterDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.fms.CourseMasterDao;
import com.sinodynamic.hkgta.dao.fms.CourseSessionDao;
import com.sinodynamic.hkgta.dao.fms.CourseSessionFacilityDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAdditionAttributeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityMasterDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotAdditionDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotLogDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityAttendanceDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedFacilityDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedStaffDao;
import com.sinodynamic.hkgta.dao.fms.StaffFacilityScheduleDao;
import com.sinodynamic.hkgta.dao.fms.StaffTimeslotDao;
import com.sinodynamic.hkgta.dao.pms.RoomFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityAvailabilityDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemStatusDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterChangeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterQueryDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotQueryDto;
import com.sinodynamic.hkgta.dto.fms.IFacilityTimeslotLogDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRoaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.fms.CourseSession;
import com.sinodynamic.hkgta.entity.fms.CourseSessionFacility;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotAddition;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendancePK;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttrPK;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;
import com.sinodynamic.hkgta.entity.fms.MemberReservedStaff;
import com.sinodynamic.hkgta.entity.fms.StaffFacilitySchedule;
import com.sinodynamic.hkgta.entity.pms.RoomFacilityTypeBooking;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberFacilityTypeBookingStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class FacilityMasterServiceImpl extends ServiceBase<FacilityMaster> implements FacilityMasterService
{
	@Autowired
	private FacilityMasterDao facilityMasterDao;

	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;

	@Autowired
	private FacilityTimeslotDao facilityTimeslotDao;

	@Autowired
	private MemberReservedFacilityDao memberReservedFacilityDao;

	@Autowired
	private FacilityTimeslotAdditionDao facilityTimeslotAdditionDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private MemberFacilityAttendanceDao memberFacilityAttendanceDao;

	@Autowired
	private StaffFacilityScheduleDao staffFacilityScheduleDao;

	@Autowired
	private GlobalParameterDao globalParameterDao;

	@Autowired
	private CourseSessionFacilityDao courseSessionFacilityDao;

	@Autowired
	private FacilityAdditionAttributeDao facilityAdditionAttributeDao;

	@Autowired
	StaffCoachRoasterDao staffCoachRoasterDao;
	
	@Autowired
	StaffTimeslotDao staffTimeslotDao;

	@Autowired
	private CourseSessionDao courseSessionDao;
	
	@Autowired
	private CourseMasterDao courseMasterDao;

	@Autowired
	private RoomFacilityTypeBookingDao roomFacilityTypeBookingDao;
	
	@Autowired
	private FacilityTypeQuotaService facilityTypeQuotaService;

	@Autowired
	private FacilityTimeslotLogDao facTimeslotLogDao;

	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;
	
	@Autowired
	private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;

	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	private MemberReservedStaffDao memberReservedStaffDao;

	protected  final Logger logger = Logger.getLogger(FacilityMasterServiceImpl.class); 
	   
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityMaster> getFacilityMasterList(Integer venueFloor, String facilityType)
	{
		return facilityMasterDao.getFacilityMasterList(venueFloor, facilityType);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityMaster> getFacilityVenueFloorList(String facilityType, String venueCode)
	{
		return facilityMasterDao.getFacilityVenueFloorList(facilityType, venueCode);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult getFacilityStatus(FacilityItemStatusDto facilityItemStatusDto)
	{
		try
		{
			FacilityItemStatusDto facilityItemStatusDtoReturn = null;
			FacilityItemStatusDto currentFacilityItemStatusDto = new FacilityItemStatusDto();
			currentFacilityItemStatusDto.setFacilityNo(facilityItemStatusDto.getFacilityNo());
			currentFacilityItemStatusDto.setBeginDatetime(facilityItemStatusDto.getBeginDatetime());
			currentFacilityItemStatusDto.setEndDatetime(facilityItemStatusDto.getEndDatetime());
			int hourDifference = DateCalcUtil.GetDateDifferenceByType(facilityItemStatusDto.getBeginDatetime(), facilityItemStatusDto.getEndDatetime(), Calendar.HOUR_OF_DAY);
			if (hourDifference >= 1)
			{
				List<FacilityItemStatusDto> facilityItemStatusDtoListReturn = new ArrayList<FacilityItemStatusDto>();
				List<FacilityItemStatusDto> facilityItemStatusDtoListSameReservation = new ArrayList<FacilityItemStatusDto>();
				currentFacilityItemStatusDto.setEndDatetime(DateCalcUtil.GetEndTime(currentFacilityItemStatusDto.getBeginDatetime()));
				
				Map<String, Object> responseMap = new HashMap<String, Object>();
				while (currentFacilityItemStatusDto.getBeginDatetime().before(facilityItemStatusDto.getEndDatetime()))
				{
					facilityItemStatusDtoReturn = facilityTimeslotDao.getFacilityStatus(currentFacilityItemStatusDto);
					if (null != facilityItemStatusDtoReturn) {
						facilityItemStatusDto.setCreateDate(facilityItemStatusDtoReturn.getCreateDate());
						facilityItemStatusDtoListReturn.add(facilityItemStatusDtoReturn);
					}
					List<FacilityItemStatusDto> bookingFacilityItemStatusDtoList = getBookingInformation(facilityItemStatusDtoReturn, responseMap);
					
					facilityItemStatusDtoListSameReservation.addAll(bookingFacilityItemStatusDtoList);
					
					if (bookingFacilityItemStatusDtoList.isEmpty()) {
						
						List<FacilityItemStatusDto> courseFacilityItemStatusDtoList = getCourseSessionInformation(facilityItemStatusDtoReturn, responseMap);
						
						if (!courseFacilityItemStatusDtoList.isEmpty()) {
							responseMap.put(Constant.RESV_TYPE, Constant.RESV_TYPE_COURSE);
							appendNotRepeated(facilityItemStatusDtoListReturn, courseFacilityItemStatusDtoList, facilityItemStatusDto);
						}
					}

					currentFacilityItemStatusDto.setBeginDatetime(DateCalcUtil.GetNextHour(currentFacilityItemStatusDto.getBeginDatetime()));
					currentFacilityItemStatusDto.setEndDatetime(DateCalcUtil.GetEndTime(currentFacilityItemStatusDto.getBeginDatetime()));
					if (facilityItemStatusDtoReturn != null) {
						getFacilityName(facilityItemStatusDto, facilityItemStatusDtoReturn);
					}
				}
				if (facilityItemStatusDtoListSameReservation.size() > 0)
				{
					appendNotRepeated(facilityItemStatusDtoListReturn, facilityItemStatusDtoListSameReservation, facilityItemStatusDto);
					copyFacilityName(facilityItemStatusDtoListReturn);
				}
				
				if (facilityItemStatusDtoListReturn.size() > 0)
				{
					responseMap.put("memberName", getMemberName(facilityItemStatusDtoListReturn.get(0).getSalutation(), facilityItemStatusDtoListReturn.get(0).getGiven_name(), facilityItemStatusDtoListReturn.get(0).getSurname()));
					responseMap.put("beginDatetimeBook", facilityItemStatusDtoListReturn.get(0).getBegin_datetime_book() != null ? facilityItemStatusDtoListReturn.get(0).getBegin_datetime_book() : "");
					responseMap.put("endDatetimeBook", facilityItemStatusDtoListReturn.get(0).getEnd_datetime_book() != null ? facilityItemStatusDtoListReturn.get(0).getEnd_datetime_book() : "");
					responseMap.put("resvId", facilityItemStatusDtoListReturn.get(0).getResvId() != null ? facilityItemStatusDtoListReturn.get(0).getResvId() : "0");
					facilityItemStatusDtoListReturn.get(0).setSalutation(null);
					facilityItemStatusDtoListReturn.get(0).setGiven_name(null);
					facilityItemStatusDtoListReturn.get(0).setSurname(null);
					facilityItemStatusDtoListReturn.get(0).setBegin_datetime_book(null);
					facilityItemStatusDtoListReturn.get(0).setEnd_datetime_book(null);
				}
				else
				{
					responseMap.put("resvId", "0");
					facilityItemStatusDtoReturn = new FacilityItemStatusDto();
					facilityItemStatusDtoReturn.setFacilityNo(facilityItemStatusDto.getFacilityNo());
					facilityItemStatusDtoReturn.setBeginDatetime(facilityItemStatusDto.getBeginDatetime());
					facilityItemStatusDtoReturn.setEndDatetime(facilityItemStatusDto.getEndDatetime());
					facilityItemStatusDtoReturn.setStatus("VC");
					facilityItemStatusDtoReturn.setResvId(null);
					getFacilityName(facilityItemStatusDto, facilityItemStatusDtoReturn);
					facilityItemStatusDtoReturn.setBallFeedingStatus("OFF");
					facilityItemStatusDtoListReturn.add(facilityItemStatusDtoReturn);
				}
				clearNotSerialableProperty(facilityItemStatusDtoListReturn);
				sortByBeginDatetime(facilityItemStatusDtoListReturn);
				responseMap.put("timeslot", facilityItemStatusDtoListReturn);
				responseResult.initResult(GTAError.FacilityError.SUCCESS, responseMap);
				return responseResult;
			}
			else
			{
				responseResult.initResult(GTAError.FacilityError.END_SHOULD_BE_ONE_HOUR_LATER_THEN_BEGIN);
				return responseResult;
			}
		}
		catch (Exception e)
		{
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private void clearNotSerialableProperty(List<FacilityItemStatusDto> facilityItemStatusDtoListReturn)
	{
		for (FacilityItemStatusDto facilityItemStatusDto : facilityItemStatusDtoListReturn)
		{
			facilityItemStatusDto.setFacilityTimeslot(null);
			facilityItemStatusDto.setResvId(null);
		}
	}

	private String getMemberName(String salutation, String givenName, String surName)
	{
		if (salutation != null || givenName != null || surName != null)
		{
			return salutation + " " + givenName + " " + surName;
		}
		else
		{
			return "";
		}
	}

	private void getFacilityName(FacilityItemStatusDto facilityItemStatusDto, FacilityItemStatusDto facilityItemStatusDtoReturn)
	{
		if (facilityItemStatusDtoReturn != null)
		{
			FacilityMaster facilityMaster = facilityMasterDao.get(FacilityMaster.class, facilityItemStatusDto.getFacilityNo());
			facilityItemStatusDtoReturn.setFacilityName(facilityMaster != null ? facilityMaster.getFacilityName() : "");
		}
	}

	private void copyFacilityName(List<FacilityItemStatusDto> facilityItemStatusDtoListReturn)
	{
		if (facilityItemStatusDtoListReturn != null && facilityItemStatusDtoListReturn.size() > 0)
		{
			int index = 0;
			String facilityName = "";
			for (FacilityItemStatusDto facilityItemStatusDto : facilityItemStatusDtoListReturn)
			{
				if (index == 0)
				{
					facilityName = facilityItemStatusDto.getFacilityName();
				}
				else
				{
					facilityItemStatusDto.setFacilityName(facilityName);
				}
				index++;
			}
		}
	}

	private void sortByBeginDatetime(List<FacilityItemStatusDto> facilityItemStatusDtoListReturn)
	{
		if (facilityItemStatusDtoListReturn != null && facilityItemStatusDtoListReturn.size() > 0)
		{
			for (int i = 0; i < facilityItemStatusDtoListReturn.size() - 1; i++)
			{
				FacilityItemStatusDto facilityItemStatusDtoTemp = null;
				if (facilityItemStatusDtoListReturn.get(i).getBeginDatetime().after(facilityItemStatusDtoListReturn.get(i + 1).getBeginDatetime()))
				{
					facilityItemStatusDtoTemp = facilityItemStatusDtoListReturn.get(i);
					facilityItemStatusDtoListReturn.set(i, facilityItemStatusDtoListReturn.get(i + 1));
					facilityItemStatusDtoListReturn.set(i + 1, facilityItemStatusDtoTemp);
				}
			}
		}
	}

	private List<FacilityItemStatusDto> getBookingInformation(FacilityItemStatusDto facilityItemStatusDtoReturn,
			Map<String, Object> responseMap) {
		List<FacilityItemStatusDto> facilityItemStatusDtoListReturn = new ArrayList<>();
		if (facilityItemStatusDtoReturn != null && facilityItemStatusDtoReturn.getFacilityTimeslot() != null) {
			MemberReservedFacility memberReservedFacility = memberReservedFacilityDao
					.getMemberReservedFacilityListByFacilityTimeslotId(
							facilityItemStatusDtoReturn.getFacilityTimeslotId());

			if (memberReservedFacility != null) {
				MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
						.getMemberFacilityTypeBooking(memberReservedFacility.getResvId());
				
				List<FacilityItemStatusDto> facilityItemStatusDtoListSameReservationList = getFacilityTimeslotSameReservation(
						memberReservedFacility, facilityItemStatusDtoReturn);

				facilityItemStatusDtoListReturn.addAll(facilityItemStatusDtoListSameReservationList);

				if (memberFacilityTypeBooking != null) {
					if (memberFacilityTypeBooking.getExpCoachUserId() != null) {
						StaffProfile staffProfile = staffProfileDao.getStaffProfileByUserId(memberFacilityTypeBooking.getExpCoachUserId());
						responseMap.put(Constant.COACH_NAME, staffProfile.getGivenName() + ' ' + staffProfile.getSurname());
						responseMap.put(Constant.RESV_TYPE, Constant.RESV_TYPE_PRIVATE_COACHING);
					} else {
						
						List<RoomFacilityTypeBooking> roomBookingList = roomFacilityTypeBookingDao.getBookingByFacilityTypeBookingId(memberReservedFacility.getResvId());
						if (roomBookingList != null && !roomBookingList.isEmpty()) {
							responseMap.put(Constant.RESV_TYPE, Constant.RESV_TYPE_GUEST_ROOM_BUNDLED);
						}else if(Constant.RESERVE_TYPE_MT.equals(facilityItemStatusDtoReturn.getReserveType())){
							responseMap.put(Constant.RESV_TYPE, Constant.RESV_TYPE_MAINTENANCE_OFFER);
						} else {
							responseMap.put(Constant.RESV_TYPE, Constant.RESV_TYPE_MEMBER_BOOKING);
						}
						
					}

					MemberInfoDto memberInfoDto = customerProfileDao
							.getCustomerInfo(memberFacilityTypeBooking.getCustomerId());
					if (memberInfoDto != null) {
						facilityItemStatusDtoReturn
								.setBegin_datetime_book(memberFacilityTypeBooking.getBeginDatetimeBook());
						facilityItemStatusDtoReturn
								.setEnd_datetime_book(memberFacilityTypeBooking.getEndDatetimeBook());
						facilityItemStatusDtoReturn.setSalutation(memberInfoDto.getSalutation());
						facilityItemStatusDtoReturn.setGiven_name(memberInfoDto.getGivenName());
						facilityItemStatusDtoReturn.setSurname(memberInfoDto.getSurname());
						facilityItemStatusDtoReturn.setSalutation(memberInfoDto.getSalutation());
						facilityItemStatusDtoReturn.setResvId(memberFacilityTypeBooking.getResvId());
					}
				}

			}

		}
		return facilityItemStatusDtoListReturn;
	}

	private List<FacilityItemStatusDto> getCourseSessionInformation(FacilityItemStatusDto facilityItemStatusDtoReturn, Map<String, Object> responseMap)
	{
		List<FacilityItemStatusDto> facilityItemStatusDtoListSameReservation = new ArrayList<>();
		if (facilityItemStatusDtoReturn != null && facilityItemStatusDtoReturn.getFacilityTimeslot() != null)
		{
			CourseSessionFacility courseSessionFacility = courseSessionFacilityDao.getCourseSessionFacility(facilityItemStatusDtoReturn.getFacilityTimeslotId());
			if (null != courseSessionFacility)
			{
				List<CourseSessionFacility> courseSessionSames = courseSessionFacilityDao.getCourseSessionFacilityList(courseSessionFacility.getCourseSessionId());

				CourseSession courseSession = courseSessionDao.getCourseSessionById(courseSessionFacility.getCourseSessionId());
				
				if (courseSession != null) {
					CourseMaster courseMaster = courseMasterDao.get(CourseMaster.class, courseSession.getCourseId());
					
					if (courseMaster != null) {
						responseMap.put(Constant.COURSE_NAME, courseMaster.getCourseName());
					}
				}
				
				for (CourseSessionFacility sessionFacility : courseSessionSames)
				{
					FacilityTimeslot facilityTimeslotSameReservation = facilityTimeslotDao.getFacilityTimeslotById(sessionFacility.getFacilityTimeslot().getFacilityTimeslotId());
					if(!FacilityStatus.CAN.name().equals(facilityTimeslotSameReservation.getStatus())){
						FacilityItemStatusDto facilityItemStatusDtoSameReservation = new FacilityItemStatusDto();
						facilityItemStatusDtoSameReservation.setFacilityTimeslotId(facilityTimeslotSameReservation.getFacilityTimeslotId());
						facilityItemStatusDtoSameReservation.setFacilityNo(null == facilityTimeslotSameReservation.getFacilityMaster() ? 0 : facilityTimeslotSameReservation.getFacilityMaster().getFacilityNo());
						facilityItemStatusDtoSameReservation.setBeginDatetime(facilityTimeslotSameReservation.getBeginDatetime());
						facilityItemStatusDtoSameReservation.setEndDatetime(facilityTimeslotSameReservation.getEndDatetime());
						facilityItemStatusDtoSameReservation.setStatus(facilityTimeslotSameReservation.getStatus());
						facilityItemStatusDtoSameReservation.setRemark(facilityTimeslotSameReservation.getInternalRemark());
						facilityItemStatusDtoSameReservation.setCreateDate(facilityTimeslotSameReservation.getCreateDate());
						facilityItemStatusDtoListSameReservation.add(facilityItemStatusDtoSameReservation);
						facilityItemStatusDtoListSameReservation.add(facilityItemStatusDtoSameReservation);
					}
				}
			}
		}
		return facilityItemStatusDtoListSameReservation;
	}

	private void getBallFeedingStatus(FacilityItemStatusDto facilityItemStatusDtoReturn)
	{
		if (facilityItemStatusDtoReturn != null)
		{
			FacilityTimeslotAddition facilityTimeslotAddition = facilityTimeslotAdditionDao.getFacilityTimeslotAddition(facilityItemStatusDtoReturn.getFacilityTimeslotId());
			facilityItemStatusDtoReturn.setBallFeedingStatus(facilityTimeslotAddition != null ? facilityTimeslotAddition.getBallFeedingStatus() : "OFF");
		}
	}

	private void appendNotRepeated(List<FacilityItemStatusDto> facilityItemStatusDtoListReturn, List<FacilityItemStatusDto> facilityItemStatusDtoListSameReservation, FacilityItemStatusDto facilityItemStatusDto)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (FacilityItemStatusDto facilityItemStatusDtoB : facilityItemStatusDtoListSameReservation)
		{
			if (!FacilityStatus.CAN.name().equals(facilityItemStatusDtoB.getStatus()) && facilityItemStatusDto.getFacilityNo().equals(facilityItemStatusDtoB.getFacilityNo()) && format.format(facilityItemStatusDto.getBeginDatetime()).equals(format.format(facilityItemStatusDtoB.getBeginDatetime()))
					&& (null == facilityItemStatusDtoB.getCreateDate() || null==facilityItemStatusDto.getCreateDate() || getDistanceTime(facilityItemStatusDtoB.getCreateDate(),facilityItemStatusDto.getCreateDate()) <= 10)
					)
			{
				Boolean found = false;
				for (FacilityItemStatusDto facilityItemStatusDtoA : facilityItemStatusDtoListReturn)
				{
					if (facilityItemStatusDtoA.getFacilityTimeslotId().equals(facilityItemStatusDtoB.getFacilityTimeslotId()))
					{
						found = true;
					}
				}
				if (found == false)
				{
					getBallFeedingStatus(facilityItemStatusDtoB);
					facilityItemStatusDtoListReturn.add(facilityItemStatusDtoB);
				}
			}
		}
	}
	
    public static long getDistanceTime(Date startDate, Date endDate) {  
        long diff ;  
        long time1 = startDate.getTime();  
        long time2 = endDate.getTime();  
        if(time1<time2) 
            diff = time2 - time1;  
         else 
            diff = time1 - time2;  
        return diff/1000;  
    } 

	private List<FacilityItemStatusDto> getFacilityTimeslotSameReservation( MemberReservedFacility memberReservedFacility, FacilityItemStatusDto facilityItemStatusDtoReturn)
	{
		List<FacilityItemStatusDto> facilityItemStatusDtoListSameReservation = new ArrayList<>();
		List<MemberReservedFacility> memberReservedFacilityListSameReservation = memberReservedFacilityDao.getMemberReservedFacilityList(memberReservedFacility.getResvId());
		for (MemberReservedFacility memberReservedFacilitySameReservation : memberReservedFacilityListSameReservation)
		{
			FacilityTimeslot facilityTimeslotSameReservation = facilityTimeslotDao.getFacilityTimeslotById(memberReservedFacilitySameReservation.getFacilityTimeslotId());
			FacilityItemStatusDto facilityItemStatusDtoSameReservation = new FacilityItemStatusDto();
			
			Long facilityNo = facilityTimeslotSameReservation.getFacilityMaster().getFacilityNo();
			if (!FacilityStatus.CAN.name().equals(facilityTimeslotSameReservation.getStatus()) && facilityItemStatusDtoReturn.getFacilityNo() == facilityNo) {
				facilityItemStatusDtoSameReservation.setFacilityTimeslotId(facilityTimeslotSameReservation.getFacilityTimeslotId());
				facilityItemStatusDtoSameReservation.setFacilityNo(null == facilityTimeslotSameReservation.getFacilityMaster() ? 0 : facilityTimeslotSameReservation.getFacilityMaster().getFacilityNo());
				facilityItemStatusDtoSameReservation.setBeginDatetime(facilityTimeslotSameReservation.getBeginDatetime());
				facilityItemStatusDtoSameReservation.setEndDatetime(facilityTimeslotSameReservation.getEndDatetime());
				facilityItemStatusDtoSameReservation.setStatus(facilityTimeslotSameReservation.getStatus());
				facilityItemStatusDtoSameReservation.setRemark(facilityTimeslotSameReservation.getInternalRemark());
				facilityItemStatusDtoSameReservation.setCreateDate(facilityTimeslotSameReservation.getCreateDate());
				facilityItemStatusDtoListSameReservation.add(facilityItemStatusDtoSameReservation);
			}
		}
		return facilityItemStatusDtoListSameReservation;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GTACommonException.class)
	public void changeFacilityTimeslot(FacilityMasterChangeDto facilityMasterChangeDto) throws Exception
	{
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try
		{
			validateFacilityCondition(facilityMasterChangeDto, sf);
			FacilityMaster facilityMaster = facilityMasterDao.get(FacilityMaster.class, facilityMasterChangeDto.getTargetFacilityNo());
			if(null != facilityMaster){
				List<FacilityTimeslot> srcFacilityTimeslots = getSrcFacilityTimeslots(facilityMasterChangeDto, sf);
				if(null != srcFacilityTimeslots && srcFacilityTimeslots.size() >0){
					boolean flag = updateReservation(facilityMasterChangeDto, sf,facilityMaster,srcFacilityTimeslots);
					if(!flag)updateCourse(facilityMasterChangeDto, sf,facilityMaster,srcFacilityTimeslots);
				}
			}else
				throw new GTACommonException(GTAError.FacilityError.FACILITY_NOTFOUND);
		}
		catch (GTACommonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new GTACommonException(e);
		}
	}

	private boolean updateReservation(FacilityMasterChangeDto facilityMasterChangeDto, SimpleDateFormat sf,FacilityMaster tragetFacilityMaster,List<FacilityTimeslot> srcFacilityTimeslots) throws GTACommonException, ParseException, Exception
	{
		String srcStartdateStr = facilityMasterChangeDto.getSrcBookingDate() + " " + facilityMasterChangeDto.getSrcStartTime() + ":00:00";
		String srcEnddateStr = facilityMasterChangeDto.getSrcBookingDate() + " " + facilityMasterChangeDto.getSrcEndTime() + ":59:59";
		
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityMasterChangeDto.getFacilityType())) {
			validateZoneAttribute(facilityMasterChangeDto.getSrcFacilityNo(), facilityMasterChangeDto.getTargetFacilityNo());
		}
			
		boolean isCreateReservation = validateFacilityBooking(facilityMasterChangeDto, sf.parse(srcStartdateStr), sf.parse(srcEnddateStr), tragetFacilityMaster);
		
		long resvId = 0L;
		List<MemberReservedFacility> srcMemberReservedFacilitys = new ArrayList<MemberReservedFacility>();
		List<Long> srcTimeslotIds = new ArrayList<Long>();
		FacilityAdditionAttribute attribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(tragetFacilityMaster.getFacilityNo(), Constant.GOLFBAYTYPE);
		String attributeType = "";
		if (attribute != null) {
			attributeType = attribute.getId().getAttributeId();
		}
		String reserveType = "";
		if (srcFacilityTimeslots.size() > 0)
		{
			for(FacilityTimeslot srcFacilityTimeslot : srcFacilityTimeslots){
				
				reserveType = srcFacilityTimeslot.getReserveType();

				MemberReservedFacility memberReserved = memberReservedFacilityDao.get(MemberReservedFacility.class, srcFacilityTimeslot.getFacilityTimeslotId());
				if (null != memberReserved)
				{
					//Private Coaching not allow to change time
					if (Constant.RESERVE_TYPE_PC.equals(reserveType) && isCreateReservation)
					{
						throw new GTACommonException(GTAError.FacilityError.CANNOT_CHANGETIME);
					}
					resvId = memberReserved.getResvId();
					srcMemberReservedFacilitys.add(memberReserved);
				} else {
					if (Constant.RESERVE_TYPE_CS.equals(reserveType) && Constant.GOLFBAYTYPE_CAR.equals(attributeType))
					{
						throw new GTACommonException(GTAError.FacilityError.COACHING_STUDIO_CHANGE);
					}
					
					if (Constant.RESERVE_TYPE_CS.equals(reserveType) && isCreateReservation)
					{
						throw new GTACommonException(GTAError.FacilityError.CANNOT_CHANGETIME);
					}
					return false;
				}
				srcTimeslotIds.add(srcFacilityTimeslot.getFacilityTimeslotId());
			}
		}
		MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);
		if (null == booking)
			throw new GTACommonException(GTAError.FacilityError.BOOKING_ISNULL);
		
		if (booking.getExpCoachUserId() == null && Constant.GOLFBAYTYPE_CAR.equals(attributeType)) {
			throw new GTACommonException(GTAError.FacilityError.COACHING_STUDIO_CHANGE);
		}
		
		boolean hasAttendanceTime = getMemberFacilityHasAttendanceTime(booking);
		
		updateSrcFacilityInfo(facilityMasterChangeDto, srcFacilityTimeslots, srcMemberReservedFacilitys, booking);
		
		MemberFacilityTypeBooking newBooking = null;
		if(isCreateReservation){
			newBooking = copyMemberFacilityTypeBooking(booking, sf, facilityMasterChangeDto);
		}
		List<MemberReservedFacility> targetReservedFacilitys = saveTargetFacilityInfo(facilityMasterChangeDto, 
				tragetFacilityMaster, 
				srcTimeslotIds, 
				(isCreateReservation)?newBooking:booking, hasAttendanceTime,
				FacilityStatus.TA.toString().equals(facilityMasterChangeDto.getStatus())?reserveType:Constant.RESERVE_TYPE_MT);
		
		if (isCreateReservation)
		{
			if (null != booking && Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(booking.getResvFacilityType())){
				saveMemberFacilityBookAdditionAttr(booking.getResvFacilityType(), booking.getResvId(),newBooking.getResvId());
			}
			newBooking.setMemberReservedFacilitys(targetReservedFacilitys);
			memberFacilityTypeBookingDao.saveOrUpdate(newBooking);
			
			if (Constant.RESERVE_TYPE_MT.equals(reserveType) && booking.getFacilityTypeQty() == 1L){
				booking.setStatus(MemberFacilityTypeBookingStatus.CAN.name());
			}
			booking.setFacilityTypeQty(booking.getFacilityTypeQty()-1);
			memberFacilityTypeBookingDao.update(booking);
		}
		else
		{
			booking.getMemberReservedFacilitys().addAll(targetReservedFacilitys);
			memberFacilityTypeBookingDao.update(booking);
		}
		return true;
	}


	private List<MemberReservedFacility> saveTargetFacilityInfo(FacilityMasterChangeDto facilityMasterChangeDto, FacilityMaster tragetFacilityMaster, List<Long> srcTimeslotIds, MemberFacilityTypeBooking booking, boolean hasAttendanceTime,String reserveType) throws Exception

	{
		Timestamp currentTimestamp= new Timestamp(Calendar.getInstance().getTimeInMillis());
		List<MemberReservedFacility> targetMemberReservedFacilitys = new ArrayList<MemberReservedFacility>();
		FacilityTimeslot timeslot = null;
		int j = 0;
		Long srcTimeslotId = 0l;
		for (int i = facilityMasterChangeDto.getTargetStartTime(); i <= facilityMasterChangeDto.getTargetEndTime(); i++)
		{
			String startdateStr = facilityMasterChangeDto.getTargetBookingDate() + " " + i + ":00:00";
			String enddateStr = facilityMasterChangeDto.getTargetBookingDate() + " " + i + ":59:59";
		 	timeslot = facilityTimeslotDao.getFacilityTimeslot(tragetFacilityMaster.getFacilityNo(), DateCalcUtil.parseDateTime(startdateStr), DateCalcUtil.parseDateTime(enddateStr));
			if (null == timeslot)
			{
				if (srcTimeslotIds.size() > j)
					srcTimeslotId = srcTimeslotIds.get(j);
				
				timeslot = createTimeslot(facilityMasterChangeDto.getUpdateBy(), 
						DateCalcUtil.parseDateTime(startdateStr), 
						DateCalcUtil.parseDateTime(enddateStr), 
						tragetFacilityMaster, 
						FacilityStatus.TA.toString().equals(facilityMasterChangeDto.getStatus())?FacilityStatus.TA.toString():FacilityStatus.OP.toString(), 
								srcTimeslotId,
								currentTimestamp,
								reserveType);
				
				createMemberReservedInfo(facilityMasterChangeDto, booking, targetMemberReservedFacilitys, timeslot);

				if (j == 0 && !facilityMasterChangeDto.getStatus().equals(FacilityStatus.TA.toString())) {
					saveFacTimeslotLog(facilityMasterChangeDto, facilityMasterChangeDto.getFacilityType(), timeslot.getFacilityTimeslotId(), srcTimeslotId, reserveType);
				}

				j++;
			}
			else
				throw new GTACommonException(GTAError.FacilityError.FACILITYSTATUS_NOTVACANT);
		}
		if(!facilityMasterChangeDto.getStatus().equals(FacilityStatus.TA.toString())){
			saveMemberFacilityAttendance(booking, timeslot,hasAttendanceTime);
		}
		return targetMemberReservedFacilitys;
	}

	private void updateSrcFacilityInfo(FacilityMasterChangeDto facilityMasterChangeDto, List<FacilityTimeslot> srcFacilityTimeslots, List<MemberReservedFacility> srcMemberReservedFacilitys, MemberFacilityTypeBooking booking)
	{
		if (null != srcFacilityTimeslots && srcFacilityTimeslots.size() > 0){
			if(FacilityStatus.TA.name().equals(srcFacilityTimeslots.get(0).getStatus()) && srcMemberReservedFacilitys.size() > 0){
				booking.getMemberReservedFacilitys().removeAll(srcMemberReservedFacilitys);
			}
			updateSrcFacilityTimeslots(facilityMasterChangeDto, srcFacilityTimeslots, booking);
		}
	}

	private void validateZoneAttribute(Long srcFacilityNo, Long targetFacilityNo) {
		FacilityAdditionAttribute srcZoneAttribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(srcFacilityNo, Constant.ZONE_ATTRIBUTE);
		FacilityAdditionAttribute targetZoneAttribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(targetFacilityNo, Constant.ZONE_ATTRIBUTE);
		if (srcZoneAttribute != null && targetZoneAttribute != null && !srcZoneAttribute.getId().getAttributeId().equals(targetZoneAttribute.getId().getAttributeId())) {
			throw new GTACommonException(GTAError.FacilityError.ZONE_DIFFERENCE);
		}
	}

	private boolean getMemberFacilityHasAttendanceTime(MemberFacilityTypeBooking booking)
	{
		MemberFacilityAttendance oldAttendance = memberFacilityAttendanceDao.getAttendaneByResvId(booking.getResvId());
		if (null != oldAttendance && null != oldAttendance.getAttendTime())
			return true;
		return false;
	}
	
	private void saveMemberFacilityBookAdditionAttr(String facilityType,Long oldResvId, Long newResvId)
	{
		MemberFacilityBookAdditionAttr oldAttr = memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttr(oldResvId,facilityType);
		if (null != oldAttr)
		{
			MemberFacilityBookAdditionAttrPK pk = new MemberFacilityBookAdditionAttrPK();
			pk.setResvId(newResvId);
			pk.setAttributeId(oldAttr.getId().getAttributeId());
			MemberFacilityBookAdditionAttr memberFacilityBookAdditionAttr = new MemberFacilityBookAdditionAttr();
			memberFacilityBookAdditionAttr.setId(pk);
			memberFacilityBookAdditionAttr.setAttrValue(oldAttr.getId().getAttributeId());
			memberFacilityBookAdditionAttr.setFacilityType(facilityType);
			memberFacilityBookAdditionAttrDao.saveOrUpdate(memberFacilityBookAdditionAttr);
		}
	}

	@SuppressWarnings("unchecked")
	private void saveMemberFacilityAttendance(MemberFacilityTypeBooking booking, FacilityTimeslot timeslot,boolean hasAttendanceTime )
	{
		MemberFacilityAttendance memberFacilityAttendance = new MemberFacilityAttendance();
		MemberFacilityAttendancePK attendancePK = new MemberFacilityAttendancePK();
		attendancePK.setCustomerId(booking.getCustomerId());
		attendancePK.setFacilityTimeslotId(timeslot.getFacilityTimeslotId());
		memberFacilityAttendance.setId(attendancePK);
		if(hasAttendanceTime)memberFacilityAttendance.setAttendTime(new Timestamp(new Date().getTime()));
		memberFacilityAttendanceDao.save(memberFacilityAttendance);
	}

	private void updateSrcFacilityTimeslots(FacilityMasterChangeDto changeDto, List<FacilityTimeslot> srcFacilityTimeslots, MemberFacilityTypeBooking booking) throws GTACommonException
	{
		for (FacilityTimeslot timeslotOld : srcFacilityTimeslots) {
			if (FacilityStatus.TA.name().equals(timeslotOld.getStatus())) {
				deleteFacilityTimeslot(timeslotOld);
			} else if (FacilityStatus.OP.name().equals(timeslotOld.getStatus()) || FacilityStatus.RS.name().equals(timeslotOld.getStatus())) {

				timeslotOld.setStatus(FacilityStatus.CAN.name());
				timeslotOld.setUpdateBy(changeDto.getUpdateBy());
				timeslotOld.setUpdateDate(new Date());
				removeStaffFacilitySchedule(timeslotOld);
				facilityTimeslotDao.saveOrUpdate(timeslotOld);

				updateMemberFacilityAttendance(booking.getCustomerId(), timeslotOld.getFacilityTimeslotId());

				// add new MT facility timeslot
				FacilityTimeslot timeslotNew = new FacilityTimeslot();
				timeslotNew.setBeginDatetime(timeslotOld.getBeginDatetime());
				timeslotNew.setEndDatetime(timeslotOld.getEndDatetime());
				timeslotNew.setFacilityMaster(timeslotOld.getFacilityMaster());
				timeslotNew.setStatus(FacilityStatus.MT.name());
				timeslotNew.setUpdateBy(changeDto.getUpdateBy());
				timeslotNew.setUpdateDate(new Date());
				timeslotNew.setInternalRemark(changeDto.getRemark());
				facilityTimeslotDao.save(timeslotNew);

			} else
				throw new GTACommonException(GTAError.FacilityError.FACILITYSTATUS_NOTOCCUPIED);
		}


	}

	@SuppressWarnings("unchecked")
	private void deleteFacilityTimeslot(FacilityTimeslot timeslotOld)
	{
		MemberReservedFacility memberReservedFacility = memberReservedFacilityDao.get(MemberReservedFacility.class, timeslotOld.getFacilityTimeslotId());
		if (null != memberReservedFacility)
			memberReservedFacilityDao.delete(memberReservedFacility);
		List<MemberFacilityAttendance> memberFacilityAttendances = memberFacilityAttendanceDao.getMemberFacilityAttendanceList(timeslotOld.getFacilityTimeslotId(), 0);
		if (null != memberFacilityAttendances && memberFacilityAttendances.size() > 0)
		{
			for (MemberFacilityAttendance attend : memberFacilityAttendances)
			{
				memberFacilityAttendanceDao.delete(attend);
			}
		}
		removeStaffFacilitySchedule(timeslotOld);
		timeslotOld.getFacilityMaster().getFacilityTimeslots().remove(timeslotOld);
		timeslotOld.setFacilityTimeslotAddition(null);
		facilityTimeslotDao.delete(timeslotOld);
	}

	private List<FacilityTimeslot> getSrcFacilityTimeslots(FacilityMasterChangeDto facilityMasterChangeDto, SimpleDateFormat sf) throws Exception
	{
		List<FacilityTimeslot> srcFacilityTimeslots = new ArrayList<FacilityTimeslot>();
		for (int i = facilityMasterChangeDto.getSrcStartTime(); i <= facilityMasterChangeDto.getSrcEndTime(); i++)
		{
			String srcStartdateStr = facilityMasterChangeDto.getSrcBookingDate() + " " + i + ":00:00";
			String srcEnddateStr = facilityMasterChangeDto.getSrcBookingDate() + " " + i + ":59:59";
			FacilityTimeslot srcFacilityTimeslot = facilityTimeslotDao.getFacilityTimeslot(facilityMasterChangeDto.getSrcFacilityNo(), sf.parse(srcStartdateStr), sf.parse(srcEnddateStr));
			if (null != srcFacilityTimeslot)
			{
				srcFacilityTimeslots.add(srcFacilityTimeslot);
			}
		}
		return srcFacilityTimeslots;
	}

	private void removeStaffFacilitySchedule(FacilityTimeslot timeslotOld)
	{
		StaffFacilitySchedule staffFacilitySchedule = staffFacilityScheduleDao.getStaffFacilityScheduleByFacilityTimeslotId(timeslotOld.getFacilityTimeslotId());
		if(null !=staffFacilitySchedule){
			timeslotOld.setStaffFacilitySchedule(null);
			staffFacilityScheduleDao.delete(staffFacilitySchedule);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateMemberFacilityAttendance(long customerId, long facilityTimeslotId)
	{
		MemberFacilityAttendancePK attendancePK = new MemberFacilityAttendancePK();
		attendancePK.setCustomerId(customerId);
		attendancePK.setFacilityTimeslotId(facilityTimeslotId);
		MemberFacilityAttendance memberFacilityAttendance = (MemberFacilityAttendance) memberFacilityAttendanceDao.get(MemberFacilityAttendance.class, attendancePK);
		if (null != memberFacilityAttendance)
			memberFacilityAttendanceDao.deleteById(MemberFacilityAttendance.class, attendancePK);
	}

	private void createMemberReservedInfo(FacilityMasterChangeDto facilityMasterChangeDto, MemberFacilityTypeBooking booking, List<MemberReservedFacility> targetMemberReservedFacilitys, FacilityTimeslot timeslot)
	{
		MemberReservedFacility memberReservedFacility = new MemberReservedFacility();
		memberReservedFacility.setFacilityTimeslotId(timeslot.getFacilityTimeslotId());
		memberReservedFacility.setResvId(booking.getResvId());
		targetMemberReservedFacilitys.add(memberReservedFacility);
	}

	private FacilityTimeslot createTimeslot(String updateBy, Date startdate, Date enddate, FacilityMaster facilityMaster, 
			String facilityStatus, long oldTimeslotId, Timestamp currentTimestamp,String reserveType) throws ParseException
	{
		FacilityTimeslot timeslot = new FacilityTimeslot();
		timeslot.setBeginDatetime(startdate);
		timeslot.setFacilityMaster(facilityMaster);
		if(!FacilityStatus.TA.toString().equals(facilityStatus))
			timeslot.setTransferFromTimeslotId(oldTimeslotId);
		timeslot.setEndDatetime(enddate);
		timeslot.setStatus(facilityStatus);
		timeslot.setCreateBy(updateBy);
		timeslot.setCreateDate(currentTimestamp);
		timeslot.setReserveType(reserveType);
		facilityTimeslotDao.saveOrUpdate(timeslot);
		return timeslot;
	}

	private boolean validateFacilityBooking(FacilityMasterChangeDto facilityMasterChangeDto, Date beginDate, Date endDate, FacilityMaster facilityMaster) throws Exception
	{
		if (null != beginDate && null != endDate)
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			cal.setTime(beginDate);
			int beginTime = cal.get(Calendar.HOUR_OF_DAY);
			String date = format.format(cal.getTime());
			cal = Calendar.getInstance();
			cal.setTime(endDate);
			int endTime = cal.get(Calendar.HOUR_OF_DAY);
			if (format.parse(facilityMasterChangeDto.getTargetBookingDate()).before(format.parse(format.format(new Date()))))
				throw new GTACommonException(GTAError.FacilityError.DATE_LESSTHAN_TODAY);
			if (format.parse(facilityMasterChangeDto.getTargetBookingDate()).compareTo(format.parse(format.format(new Date()))) == 0){
				if(facilityMasterChangeDto.getStatus().equals(FacilityStatus.TA.toString())){
					if(facilityMasterChangeDto.getTargetEndTime() < getDateHour(new Date()))
					throw new GTACommonException(GTAError.FacilityError.DATETIME_LESSTHAN_TODAY, new Object[]
							{ getDateHour(new Date()) + ":00" });
				}else {
					if(facilityMasterChangeDto.getTargetStartTime() < getDateHour(new Date()))
					throw new GTACommonException(GTAError.FacilityError.DATETIME_LESSTHAN_TODAY,  new Object[]
							{ getDateHour(new Date()) + ":00" });
				}
				
			}
			if (date.equals(facilityMasterChangeDto.getTargetBookingDate()) && facilityMasterChangeDto.getTargetFacilityNo() == facilityMasterChangeDto.getSrcFacilityNo())
			{
				if ((beginTime <= facilityMasterChangeDto.getTargetStartTime() && facilityMasterChangeDto.getTargetStartTime() <= endTime) || (beginTime <= facilityMasterChangeDto.getTargetEndTime() && facilityMasterChangeDto.getTargetEndTime() <= endTime))
					throw new GTACommonException(GTAError.FacilityError.FACILITY_TIMESLOTCONFLICT);
			}
			if (null != facilityMaster && null != facilityMaster.getFacilityTypeBean())
			{
				int parameterValue = getGlobalParameterValue(facilityMaster.getFacilityTypeBean().getTypeCode());
				if (parameterValue > 0)
				{
					Date futureDate = getFutureDate(new Date(), parameterValue);
					if (format.parse(facilityMasterChangeDto.getTargetBookingDate()).after(futureDate))
						throw new GTACommonException(GTAError.FacilityError.DATE_OVER_ADVANCEDPERIOD, new Object[]
						{ parameterValue });
				}
			}
			if (facilityMasterChangeDto.getStatus().equals(FacilityStatus.TA.toString()))
			{
				if (!(facilityMasterChangeDto.getSrcStartTime() == facilityMasterChangeDto.getTargetStartTime() && facilityMasterChangeDto.getSrcEndTime() == facilityMasterChangeDto.getTargetEndTime()))
					throw new GTACommonException(GTAError.FacilityError.FACILITY_SELECTEDTIME_NOTSAME);
			}
			
			if (facilityMasterChangeDto.getTargetStartTime() == beginTime && facilityMasterChangeDto.getTargetEndTime() == endTime && date.equals(facilityMasterChangeDto.getTargetBookingDate()))
			{
				return false;
			}
		}
		return true;
	}

	@Transactional
	public int getGlobalParameterValue(String facilityType)
	{
		if (!StringUtils.isEmpty(facilityType))
		{
			String facTypeConst = Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityType) ? Constant.ADVANCEDRESPERIOD_GOLF : Constant.ADVANCEDRESPERIOD_TENNIS;
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, facTypeConst);
			if (null != globalParameter)
			{
				if (!StringUtils.isEmpty(globalParameter.getParamValue()))
					return Integer.parseInt(globalParameter.getParamValue());
			}
		}
		return 0;
	}

	public MemberFacilityTypeBooking copyMemberFacilityTypeBooking(MemberFacilityTypeBooking booking, SimpleDateFormat sf, FacilityMasterChangeDto facilityMasterChangeDto) throws ParseException
	{
		MemberFacilityTypeBooking newBooking = new MemberFacilityTypeBooking();
		newBooking.setCustomerId(booking.getCustomerId());
		newBooking.setResvFacilityType(booking.getResvFacilityType());
		newBooking.setFacilityTypeQty(1L);
		newBooking.setExpCoachUserId(booking.getExpCoachUserId());
		newBooking.setReserveVia(booking.getReserveVia());
		newBooking.setCreateDate(new Timestamp(new Date().getTime()));
		newBooking.setCreateBy(facilityMasterChangeDto.getUpdateBy());
		String startdateStr = facilityMasterChangeDto.getTargetBookingDate() + " " + facilityMasterChangeDto.getTargetStartTime() + ":00:00";
		String enddateStr = facilityMasterChangeDto.getTargetBookingDate() + " " + facilityMasterChangeDto.getTargetEndTime() + ":59:59";
		newBooking.setBeginDatetimeBook(new Timestamp(sf.parse(startdateStr).getTime()));
		newBooking.setEndDatetimeBook(new Timestamp(sf.parse(enddateStr).getTime()));
		newBooking.setStatus(MemberFacilityTypeBookingStatus.ATN.name());
		memberFacilityTypeBookingDao.save(newBooking);
		return newBooking;
	}

	public static Date getFutureDate(Date date, int day) throws ParseException
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return format.parse(format.format(calendar.getTime()));
	}

	public static int getDateHour(Date date) throws ParseException
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static Date getDateByFormat(Date date, SimpleDateFormat format) throws ParseException
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return format.parse(format.format(calendar.getTime()));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult saveFacilityStatus(FacilityItemStatusDto facilityItemStatusDto) throws Exception
	{
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (format.parse(format.format(facilityItemStatusDto.getBeginDatetime())).before(format.parse(format.format(now))))
			throw new GTACommonException(GTAError.FacilityError.DATE_LESSTHAN_TODAY);
		if (format.parse(format.format(facilityItemStatusDto.getBeginDatetime())).compareTo(format.parse(format.format(now))) == 0 && getDateHour(facilityItemStatusDto.getBeginDatetime()) < getDateHour(now))
			throw new GTACommonException(GTAError.FacilityError.DATETIME_LESSTHAN_TODAY);
		FacilityMaster facilityMaster = facilityMasterDao.get(FacilityMaster.class, facilityItemStatusDto.getFacilityNo());
		FacilityTimeslot timeslot = facilityTimeslotDao.getFacilityTimeslot(facilityItemStatusDto.getFacilityNo(), facilityItemStatusDto.getBeginDatetime(), null);
		if (null != facilityMaster)
		{
			if (null == timeslot)
			{
				if (!facilityItemStatusDto.getStatus().equals(FacilityStatus.VC.toString()))
				{
					timeslot = addFacilityTimeslot(facilityItemStatusDto, facilityMaster);
				}
			}
			else
			{
				if (facilityItemStatusDto.getStatus().equals(FacilityStatus.VC.toString()))
				{
					//deleteFacilityTimeslot(timeslot);
					timeslot.setStatus(FacilityStatus.CAN.name());
				}
				else
				{
					timeslot.setStatus(facilityItemStatusDto.getStatus());
				}
				timeslot.setUpdateBy(facilityItemStatusDto.getUpdateBy());
				timeslot.setUpdateDate(now);
				timeslot.setInternalRemark(facilityItemStatusDto.getRemark());
				facilityTimeslotDao.saveOrUpdate(timeslot);
			}
			
			if (timeslot != null && timeslot.getFacilityTimeslotId() != null
					&& FacilityStatus.MT.getDesc().equals(facilityItemStatusDto.getStatus())) {
				saveFacTimeslotLog(facilityItemStatusDto, facilityMaster.getFacilityTypeBean().getTypeCode(),
						timeslot.getFacilityTimeslotId(), null, null);
			}
 
		}
		else
		{
			throw new GTACommonException(GTAError.FacilityError.FACILITY_NOTFOUND);
		}
		responseResult.initResult(GTAError.FacilityError.SUCCESS);
		return responseResult;
	}
	
	private void saveFacTimeslotLog(IFacilityTimeslotLogDto dto, String facType, Long timeslotId, Long fromTimeslotId, String resvType) {
		if (dto == null) {
			return;
		}
		FacilityTimeslotLog log = new FacilityTimeslotLog();
		log.setBeginDatetime(dto.getBeginDatetime());
		log.setEndDatetime(dto.getEndDatetime() == null ? new Date(dto.getBeginDatetime().getTime() + 3600l * 1000l)
				: dto.getEndDatetime());
		log.setFacilityNo(dto.getFacilityNo());
		log.setFacilityTimeslotId(timeslotId);
		log.setFacilityType(facType);
		log.setInternalRemark(dto.getRemark());
		log.setReserveType(resvType);
		log.setStatus(FacilityStatus.MT.getDesc());
		log.setTransferFromTimeslotId(fromTimeslotId);
		Date now = new Date();
		log.setCreateDate(now);
		log.setCreateBy(dto.getUpdateBy());
		log.setUpdateDate(now);
		log.setUpdateBy(dto.getUpdateBy());
		facTimeslotLogDao.save(log);
	}

	private FacilityTimeslot addFacilityTimeslot(FacilityItemStatusDto facilityItemStatusDto, FacilityMaster facilityMaster)
	{
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		FacilityTimeslot timeslot = new FacilityTimeslot();
		timeslot.setBeginDatetime(facilityItemStatusDto.getBeginDatetime());
		timeslot.setFacilityMaster(facilityMaster);
		timeslot.setEndDatetime(DateCalcUtil.GetEndTime(facilityItemStatusDto.getBeginDatetime()));
		timeslot.setStatus(facilityItemStatusDto.getStatus());
		timeslot.setCreateBy(facilityItemStatusDto.getUpdateBy());
		timeslot.setUpdateBy(facilityItemStatusDto.getUpdateBy());
		timeslot.setCreateDate(new Timestamp(now.getTime()));
		timeslot.setUpdateDate(now);
		timeslot.setInternalRemark(facilityItemStatusDto.getRemark());
		facilityTimeslotDao.saveOrUpdate(timeslot);
		return timeslot;
	}

	private void updateCourse(FacilityMasterChangeDto facilityMasterChangeDto, SimpleDateFormat sf,FacilityMaster tragetFacilityMaster,List<FacilityTimeslot> srcFacilityTimeslots) throws Exception
	{
		Long courseSessionId = 0L;
		List<CourseSessionFacility> srcCourseSessionFacilitys = new ArrayList<CourseSessionFacility>();
		List<Long> oldTimeslotIds = new ArrayList<Long>();
		String reserveType = "";
		if (srcFacilityTimeslots.size() > 0)
		{
			for (FacilityTimeslot srcFacilityTimeslot : srcFacilityTimeslots)
			{
				CourseSessionFacility courseSessionFacility = courseSessionFacilityDao.getCourseSessionFacility(srcFacilityTimeslot.getFacilityTimeslotId());
				if (null != courseSessionFacility)
				{
					if (courseSessionId > 0)
					{
						if (courseSessionId != courseSessionFacility.getCourseSessionId())
							throw new GTACommonException(GTAError.FacilityError.BOOKING_ISNULL);
					}
					courseSessionId = courseSessionFacility.getCourseSessionId();
					srcCourseSessionFacilitys.add(courseSessionFacility);
					oldTimeslotIds.add(srcFacilityTimeslot.getFacilityTimeslotId());
					reserveType = srcFacilityTimeslot.getReserveType();
				}
			}
		}
		if (courseSessionId > 0)
		{
			createCourseSessionFacility(facilityMasterChangeDto,courseSessionId, oldTimeslotIds,tragetFacilityMaster,FacilityStatus.TA.toString().equals(facilityMasterChangeDto.getStatus())?reserveType:Constant.RESERVE_TYPE_MT);
			if (srcCourseSessionFacilitys.size() > 0)
			{
				updateOldCourseSessionFacility(facilityMasterChangeDto, srcCourseSessionFacilitys);
			}
		}
	}

	private void createCourseSessionFacility(FacilityMasterChangeDto facilityMasterChangeDto,Long courseSessionId, List<Long> oldTimeslotIds,FacilityMaster tragetFacilityMaster,String reserveType) throws Exception
	{
		int j = 0;
		Long oldTimeslotId = 0L;
		Timestamp currentTimestamp= new Timestamp(Calendar.getInstance().getTimeInMillis());
		for (int i = facilityMasterChangeDto.getTargetStartTime(); i <= facilityMasterChangeDto.getTargetEndTime(); i++)
		{
			String startdateStr = facilityMasterChangeDto.getTargetBookingDate() + " " + i + ":00:00";
			String enddateStr = facilityMasterChangeDto.getTargetBookingDate() + " " + i + ":59:59";
			FacilityTimeslot timeslot = facilityTimeslotDao.getFacilityTimeslot(tragetFacilityMaster.getFacilityNo(), DateCalcUtil.parseDateTime(startdateStr), DateCalcUtil.parseDateTime(enddateStr));
			if (null == timeslot)
			{
				if (oldTimeslotIds.size() > j)
					oldTimeslotId = oldTimeslotIds.get(j);
				timeslot = createTimeslot(facilityMasterChangeDto.getUpdateBy(), 
						DateCalcUtil.parseDateTime(startdateStr), 
						DateCalcUtil.parseDateTime(enddateStr), 
						tragetFacilityMaster, 
						FacilityStatus.TA.toString().equals(facilityMasterChangeDto.getStatus())?FacilityStatus.TA.toString():FacilityStatus.OP.toString(), 
								oldTimeslotId,
								currentTimestamp,
								reserveType);
				addCourseSessionFacility(facilityMasterChangeDto, courseSessionId, timeslot);

				if (j == 0 && !facilityMasterChangeDto.getStatus().equals(FacilityStatus.TA.toString())) {
					saveFacTimeslotLog(facilityMasterChangeDto, facilityMasterChangeDto.getFacilityType(), timeslot.getFacilityTimeslotId(), oldTimeslotId, reserveType);
				}

				j++;
			}
			else
				throw new GTACommonException(GTAError.FacilityError.FACILITYSTATUS_NOTVACANT);
		}
	}

	private void addCourseSessionFacility(FacilityMasterChangeDto facilityMasterChangeDto, Long courseSessionId, FacilityTimeslot timeslot) throws ParseException
	{
		CourseSessionFacility courseSessionFacility = new CourseSessionFacility();
		courseSessionFacility.setCourseSessionId(courseSessionId);
		courseSessionFacility.setFacilityNo(timeslot.getFacilityMaster().getFacilityNo() + "");
		courseSessionFacility.setFacilityTimeslot(timeslot);
		courseSessionFacility.setCreateBy(facilityMasterChangeDto.getUpdateBy());
		courseSessionFacility.setUpdateBy(facilityMasterChangeDto.getUpdateBy());
		courseSessionFacility.setCreateDate(new Timestamp(new Date().getTime()));
		courseSessionFacility.setUpdateDate(new Date());
		courseSessionFacilityDao.save(courseSessionFacility);
	}

	private void updateOldCourseSessionFacility(FacilityMasterChangeDto changeDto, List<CourseSessionFacility> srcCourseSessionFacilitys) throws ParseException, GTACommonException
	{
		for (CourseSessionFacility oldCourseSession : srcCourseSessionFacilitys)
		{
			FacilityTimeslot timeslotOld = oldCourseSession.getFacilityTimeslot();
			if (timeslotOld != null)
			{
				if(FacilityStatus.TA.name().equals(timeslotOld.getStatus())){
					deleteFacilityTimeslot(timeslotOld);
				}else if (FacilityStatus.OP.name().equals(timeslotOld.getStatus()) || FacilityStatus.RS.name().equals(timeslotOld.getStatus()))
				{
					timeslotOld.setStatus(FacilityStatus.MT.name());
					timeslotOld.setUpdateBy(changeDto.getUpdateBy());
					timeslotOld.setUpdateDate(new Date());
					timeslotOld.setInternalRemark(changeDto.getRemark());
					facilityTimeslotDao.saveOrUpdate(timeslotOld);
					/*oldCourseSession.setFacilityTimeslot(null);
					courseSessionFacilityDao.delete(oldCourseSession);*/
				}
				else
					throw new GTACommonException(GTAError.FacilityError.FACILITYSTATUS_NOTRESERVED);
			}
			else
				throw new GTACommonException(GTAError.FacilityError.STAFFFACILITYSCHEDULE_NOTFOUND);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult getReservationInfoByCustomerId(long customerId)
	{
		List<MemberFacilityTypeBooking> memberFacilityTypeBookingList = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingByCustomerId(customerId);
		List<MemberFacilityTypeBooking> memberFacilityTypeBookingListSelected = new ArrayList<MemberFacilityTypeBooking>();
		List<FacilityItemStatusDto> facilityItemStatusDtoList = new ArrayList<FacilityItemStatusDto>();
		Map<String, Object> responseMap = new HashMap<String, Object>();
		Date now = new Date();
		for (MemberFacilityTypeBooking memberFacilityTypeBooking : memberFacilityTypeBookingList)
		{
			if (memberFacilityTypeBooking.getBeginDatetimeBook().after(now))
			{
				memberFacilityTypeBookingListSelected.add(memberFacilityTypeBooking);
			}
		}
		for (MemberFacilityTypeBooking memberFacilityTypeBooking : memberFacilityTypeBookingListSelected)
		{
			List<MemberReservedFacility> memberReservedFacilityList = memberReservedFacilityDao.getMemberReservedFacilityList(memberFacilityTypeBooking.getResvId());
			List<FacilityTimeslot> facilityTimeslotList = new ArrayList<FacilityTimeslot>();
			for (MemberReservedFacility memberReservedFacility : memberReservedFacilityList)
			{
				facilityTimeslotList.add(facilityTimeslotDao.getFacilityTimeslotById(memberReservedFacility.getFacilityTimeslotId()));
			}
			for (FacilityTimeslot facilityTimeslot : facilityTimeslotList)
			{
				if (facilityTimeslot != null)
				{
					FacilityItemStatusDto facilityItemStatusDto = new FacilityItemStatusDto();
					facilityItemStatusDto.setFacilityTimeslotId(facilityTimeslot.getFacilityTimeslotId());
					facilityItemStatusDto.setFacilityNo((facilityTimeslot.getFacilityMaster().getFacilityNo()));
					facilityItemStatusDto.setFacilityName((facilityTimeslot.getFacilityMaster().getFacilityName()));
					facilityItemStatusDto.setBeginDatetime((facilityTimeslot.getBeginDatetime()));
					facilityItemStatusDto.setEndDatetime((facilityTimeslot.getEndDatetime()));
					facilityItemStatusDto.setBallFeedingStatus((facilityTimeslot.getFacilityTimeslotAddition() != null ? facilityTimeslot.getFacilityTimeslotAddition().getBallFeedingStatus() : "OFF"));
					facilityItemStatusDtoList.add(facilityItemStatusDto);
				}
			}
		}
		if (memberFacilityTypeBookingListSelected.size() > 0)
		{
			CustomerProfile customerProfile = customerProfileDao.getById(new Long(customerId));
			responseMap.put("memberName", getMemberName(customerProfile.getSalutation(), customerProfile.getGivenName(), customerProfile.getSurname()));
			responseMap.put("beginDatetimeBook", memberFacilityTypeBookingListSelected.get(0).getBeginDatetimeBook() != null ? memberFacilityTypeBookingListSelected.get(0).getBeginDatetimeBook() : "");
			responseMap.put("endDatetimeBook", memberFacilityTypeBookingListSelected.get(0).getEndDatetimeBook() != null ? memberFacilityTypeBookingListSelected.get(0).getEndDatetimeBook() : "");
			responseMap.put("resvId", memberFacilityTypeBookingListSelected.get(0).getResvId());
			responseMap.put("timeslots", facilityItemStatusDtoList);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, responseMap);
			return responseResult;
		}
		responseResult.initResult(GTAError.FacilityError.FACILITY_NOTFOUND);
		return responseResult;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityMasterQueryDto> getFacilityMasterList(String facilityType)
	{
		return facilityMasterDao.getFacilityMasterList(facilityType);
	}
	
	//Only for ball feeding machine control
		@Override
		@Transactional(propagation = Propagation.REQUIRED)
		public void setBallFeedingMachineStatus(List<FacilityMasterQueryDto> facilityMasterList, Map<String, String> statusMap) throws Exception {
			
			if (null == facilityMasterList) return;
		
			for (FacilityMasterQueryDto facilityMasterQueryDto: facilityMasterList) {
				if (statusMap.containsKey(facilityMasterQueryDto.getFactorySerialNo())){
					String ballFeedingStatus = statusMap.get(facilityMasterQueryDto.getFactorySerialNo());
					if (null != ballFeedingStatus && ballFeedingStatus.equals("3")) {
						facilityMasterQueryDto.setBallFeedingStatus("OFF");
					}
					else if (null != ballFeedingStatus && ballFeedingStatus.equals("5")) {
						facilityMasterQueryDto.setBallFeedingStatus("ON");
					}
				}
			}
		}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public int countFacilityMasterByTypeAndAttribute(String facilityType, String facilityAttribute,List<Long> facilityNos)
	{
		return facilityMasterDao.getFacilityMasterCount(facilityType, facilityAttribute,null,facilityNos);
	}
	
	//Only for private coaching
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute) throws Exception
	{
		return getFacilityAvailabilityByFacilityNos(facilityType, specifiedDate, facilityAttribute, 1, bayNumberForPrivateCoaching(), false);
	}
	
	private List<Long> bayNumberForPrivateCoaching()
	{
		List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_COACHING);
		return facilityNos;

	}

	//Only for private coaching
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityAvailabilityDto> getFacilityAvailability(String coachId, String facilityType, Date specifiedDate,String facilityAttribute) throws Exception {
		List<FacilityAvailabilityDto> facilityTimeslot = getFacilityAvailabilityByFacilityNos(facilityType, specifiedDate, facilityAttribute, 1, bayNumberForPrivateCoaching(), false);
		
		DateTime dateTime = new DateTime(specifiedDate.getTime());
		Date begin = dateTime.millisOfDay().withMinimumValue().toLocalDateTime().toDate();
		Date end = dateTime.millisOfDay().withMaximumValue().toLocalDateTime().toDate();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(specifiedDate);
		final int weekday = cal.get(Calendar.DAY_OF_WEEK);
		
		final List<StaffTimeslot> staffTimeslot = staffTimeslotDao.loadStaffTimeslot(coachId, begin, end);
		final List<StaffCoachRoaster> coachRoster = staffCoachRoasterDao.loadOffdutyRoster(coachId, begin, end);
		
		CollectionUtil.loop(facilityTimeslot, new NoResultCallBack<FacilityAvailabilityDto>(){
			@Override
			public void execute(FacilityAvailabilityDto t, int index) {
				for(StaffCoachRoaster item : coachRoster){
					if(item.getOnDate() != null){
						if(item.getBeginTime() == t.getHour()){
							t.setAvailable(false);
						}
					}
					if(item.getWeekDay() != null && Integer.parseInt(item.getWeekDay()) == weekday){
						if(item.getBeginTime() == t.getHour()){
							t.setAvailable(false);
						}
					}
				}
				for(StaffTimeslot slot : staffTimeslot){
					String hour = DateFormatUtils.format(slot.getBeginDatetime(), "HH");
					if(Integer.parseInt(hour) == t.getHour()){
						t.setAvailable(false);
					}
				}
			}
		});
		
		return facilityTimeslot;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility,  List<Long> floors) throws Exception
	{
		return this.getFacilityAvailability(facilityType, specifiedDate, facilityAttribute, numberOfFacility, floors, false);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility,  List<Long> floors,boolean checkQuota) throws Exception
	{
		return this.getFacilityAvailabilityCommon(facilityType, specifiedDate, facilityAttribute, numberOfFacility, floors,null,false);
	}
	
	
	public List<FacilityAvailabilityDto> getFacilityAvailabilityCommon(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility,List<Long> floors, List<Long> facilityNos,boolean checkQuota) throws Exception
	{
		List<Date> hours = new ArrayList<Date>();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(specifiedDate);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		for (int i = 7; i <= 22; i++)
		{
			calendar.set(Calendar.HOUR_OF_DAY, i);
			hours.add(calendar.getTime());
		}
		int facilityCount  =0;
		List<FacilityTimeslotQueryDto> queryDtos = new ArrayList<FacilityTimeslotQueryDto>(); 
		facilityCount = facilityMasterDao.getFacilityMasterCount(facilityType, facilityAttribute,floors,facilityNos);
		queryDtos = facilityTimeslotDao.getFacilityTimeslotGroupTimeCountFacilityAttribute(facilityType, hours.toArray(new Date[hours.size()]), facilityAttribute,floors,facilityNos);
		
		List<FacilityAvailabilityDto> availabilityDtoList = new ArrayList<FacilityAvailabilityDto>();
		Long quota = facilityTypeQuotaService.getFacilityQuota(facilityType, calendar.getTime());
		for (int i = 7; i <= 22; i++)
		{
			FacilityAvailabilityDto facilityAvailabilityDto = new FacilityAvailabilityDto();
			facilityAvailabilityDto.setHour(i);
			if (queryDtos.get(i - 7).getFacilityCount().intValue() + numberOfFacility <= facilityCount)
			{
				if (checkQuota && null != quota && Constant.FACILITY_TYPE_GOLF.equals(facilityType))
				{
					calendar.set(Calendar.HOUR_OF_DAY, i);
					boolean quotaFlag = checkMemberBookingQuota(quota,numberOfFacility,facilityType, calendar.getTime());
					if (quotaFlag)
						facilityAvailabilityDto.setAvailable(true);
					else
						facilityAvailabilityDto.setAvailable(false);
				}
				else
				{
					facilityAvailabilityDto.setAvailable(true);
				}
			}
			else
			{
				facilityAvailabilityDto.setAvailable(false);
			}
			availabilityDtoList.add(facilityAvailabilityDto);
		}
		return availabilityDtoList;
	}
	
	private boolean checkMemberBookingQuota(Long totalQuota,int numberOfFacility,String facilityType,Date bookingDateTime) throws Exception, ParseException
	{
		if (Constant.FACILITY_TYPE_GOLF.equals(facilityType))
		{
			int quotaResCount = memberFacilityTypeBookingDao.getMemberFacilityBookedCountForQuota(facilityType, bookingDateTime);
			if ((totalQuota - quotaResCount) < numberOfFacility)
				return false;
		}
		return true;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityAvailabilityDto> getFacilityAvailability(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility) throws Exception
	{
		
		return getFacilityAvailability(facilityType, specifiedDate, facilityAttribute, numberOfFacility, null);
		
	}

	private void validateFacilityCondition(FacilityMasterChangeDto changeDto, SimpleDateFormat sf) throws ParseException, Exception
	{
		List<Long> facilityNos = null;
		if(Constant.FACILITY_TYPE_GOLF.equals(changeDto.getFacilityType())){
			FacilityAdditionAttribute targetZone = facilityAdditionAttributeDao.getFacilityAdditionAttribute(changeDto.getTargetFacilityNo(), Constant.ZONE_ATTRIBUTE);
			facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(targetZone.getId().getAttributeId());
		}
		String attributeType = changeDto.getFacilityType().equalsIgnoreCase(Constant.FACILITY_TYPE_GOLF) ? Constant.GOLFBAYTYPE : Constant.TENNISCRT;
		FacilityAdditionAttribute attribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(changeDto.getTargetFacilityNo(), attributeType);
		if (null != attribute)
		{
			String attributeId = attribute.getId().getAttributeId();
			int facilityCount = facilityMasterDao.getFacilityMasterCount(changeDto.getFacilityType(), attributeId,null,facilityNos);
			Date[] dateTimes = new Date[(changeDto.getTargetEndTime() - changeDto.getTargetStartTime()) + 1];
			int time = 0;
			for (int i = changeDto.getTargetStartTime(); i <= changeDto.getTargetEndTime(); i++)
			{
				String startdateStr = changeDto.getTargetBookingDate() + " " + i + ":00:00";
				dateTimes[time] = sf.parse(startdateStr);
				time++;
			}
			List<FacilityTimeslotQueryDto> queryDtos = facilityTimeslotDao.getFacilityTimeslotGroupTimeCountFacilityAttribute(changeDto.getFacilityType(), dateTimes, attributeId,null,facilityNos);
			if (null != queryDtos && queryDtos.size() > 0)
			{
				for (FacilityTimeslotQueryDto timeslotDto : queryDtos)
				{
					if (timeslotDto.getFacilityCount().intValue() > facilityCount)
					{
						throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT);
					}
				}
			}
		}
	}

	@Override
	@Transactional
	public List<BayTypeDto> getFacilityBayType(String facilityType) {
		try{
			return this.facilityMasterDao.getFacilityBayType(facilityType);
		}catch(Exception e){
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new Object[]{e.getMessage()});
		}
	}
	
	@Override
	@Transactional
	public FacilityMaster getFacilityMasterbyFacilityNo(Integer facilityNo){
		return facilityMasterDao.get(FacilityMaster.class, facilityNo.longValue());
	}

	@Override
	@Transactional
	public List<FacilityMaster> getFacilityByAttribute(String facilityType, String facilityAttribute) {
		return facilityMasterDao.getFacilityByAttribute(facilityType, facilityAttribute);
	}

	@Override
	@Transactional
	public int countFacilityMasterByTypeAndFloors(String facilityType, List<Long> floors)
	{
		return facilityMasterDao.countFacilityMasterByType(facilityType, floors,null);
	}
	
	/*public static List<Long> getAvailableResFacilityFloors(String facilityType)
	{
		if(!StringUtils.isEmpty(facilityType) && Constant.FACILITY_TYPE_GOLF.equals(facilityType)){
			List<Long> floors = new ArrayList<Long>();
			floors.add(0L);
			floors.add(1L);
			return floors;
		}
		return null;
	}*/

	@Override
	@Transactional
	public List<FacilityItemDto> getFacilityStatusByFloorAndTime(String type, String beginDatetime, String endDatetime, Long floor)
	{
		return facilityMasterDao.getFacilityStatusByFloorAndTime(type, beginDatetime, endDatetime, floor);
	}

	@Override
	@Transactional
	public List<FacilityAvailabilityDto> getFacilityAvailabilityByFacilityNos(String facilityType, Date specifiedDate, String facilityAttribute, int numberOfFacility, List<Long> facilityNos, boolean checkQuota) throws Exception
	{
		return this.getFacilityAvailabilityCommon(facilityType, specifiedDate, facilityAttribute, numberOfFacility, null,facilityNos,checkQuota);
	}

	@Override
	@Transactional
	public int countFacilityMasterByTypeAndFacilityNos(String facilityType, List<Long> facilityNos)
	{
		return facilityMasterDao.countFacilityMasterByType(facilityType,null, facilityNos);
	}
}