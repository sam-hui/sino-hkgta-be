package com.sinodynamic.hkgta.service.crm.cardmanage;


import java.util.Date;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.GuestProfileDto;
import com.sinodynamic.hkgta.dto.crm.PaymentOrderDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderPermitCardDto;
import com.sinodynamic.hkgta.entity.rpos.CandidateCustomer;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface DayPassPurchaseService extends IServiceBase {

	boolean checkPurchaseRight(DaypassPurchaseDto dto);

	boolean checkPurchaseDays(DaypassPurchaseDto dto);

	ResponseResult checkAvailability(DaypassPurchaseDto dto);
	
	boolean changeDaypassCardStatus(CustomerOrderPermitCardDto dto);

	ResponseResult saveDayPassPurchase(DaypassPurchaseDto dto,String userId);
	
	DaypassPurchaseDto checkPurchasaeDayPassLimit(DaypassPurchaseDto dto) throws Exception;

	ResponseResult getInitialQueryResult(AdvanceQueryDto queryDto, String sql, ListPage page,String userId);

	String getPassPeriodType(DaypassPurchaseDto dto);

	ResponseResult purchaseDaypass(DaypassPurchaseDto dto,String userId);

	ResponseResult payDaypassPurchase(DaypassPurchaseDto dto,String userId,String purchasedLocationCode );

	boolean updateCustomerId2CustomerOrderPermitCard(PaymentOrderDto dto);

	ResponseResult payment(Long orderNo);

	ResponseResult saveOrUpdateDayPassGuestInfo(GuestProfileDto guestProfileDto) throws Exception;
	
	ResponseResult searchDayPassById(String Id) throws Exception;


	boolean saveOrUpdateCustomerOrderPermitCard(CustomerOrderPermitCard card);
	
	public ResponseResult getMemberDayPass(String sql,String sortBy,String isAscending);
	
	public CustomerOrderPermitCard getDayPassCardByCardNo(String cardNo);

	void returnCard(String cardNo);

	/**   
	* @author: Zero_Wang
	* @since: Sep 9, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	DaypassPurchaseDto scanQRCode(ScanCardMappingDto cardMappingDto, Long orderNo);
	
	public void sendEmail(DaypassPurchaseDto dto,String userId);

	DaypassPurchaseDto getMemberCanBuyDaypassList(Date begin, Date end);

	CandidateCustomerDto getCandidateByQRcode(Long qrcode);

	CandidateCustomerDto updateCandidateCandidateCustomer(CandidateCustomerDto dto);

	
}
