package com.sinodynamic.hkgta.service.fms;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.dao.fms.FacilitySubTypeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTypeDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

/**
 * email service
 * @author Mason_Yang
 *
 */
@Service
public class FacilityEmailServiceImpl extends ServiceBase<FacilityMaster> implements FacilityEmailService {

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;

	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private FacilityAttributeCaptionDao facilityAttributeCaptionDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;
	
	@Autowired
	private FacilitySubTypeDao facilitySubTypeDao;
	
	@Autowired
	private FacilityTypeDao facilityTypeDao;
 
	@Autowired
	private MailThreadService mailThreadService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void sendReceiptConfirmationEmail(long resvId,String userId) {
		logger.info("CustomerEmailServiceImpl.sendReceiptConfirmationEmail invocation start ...");
		
		MemberFacilityTypeBooking facilityTypeBooking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);

		CustomerEmailContent cec = new CustomerEmailContent();
		
		if (facilityTypeBooking != null && facilityTypeBooking.getCustomerOrderHd() != null) {
			Long customerID = facilityTypeBooking.getCustomerOrderHd().getCustomerId();
			
			CustomerProfile cp = customerProfileDao.getById(customerID);
			if (cp == null) {
				throw new GTACommonException(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
			}
			
			MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityTypeBooking.getResvFacilityType())?"golf_confirm_email":"tennis_confirm_email");
			
			if (template == null) {
				logger.error("can't find the refund template");
				return;
			}
			
			String address = cp.getContactEmail();
			cec.setSenderUserId(userId);
			cec.setRecipientCustomerId(customerID.toString());
			cec.setRecipientEmail(address);
			cec.setSubject(template.getMessageSubject());
			cec.setNoticeType(Constant.NOTICE_TYPE_FACILITY);
			long confirmationID = getReservationTransactionNo(facilityTypeBooking);
			if (template.getContentHtml() != null) {

				String userName = cp.getSalutation() + " " + cp.getSurname();
				String fullName = cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname();
				String bookingDate = "";
				try {
					bookingDate = DateCalcUtil.formatDate(facilityTypeBooking.getBeginDatetimeBook());
				} catch (Exception e) {
				}
				String prefix = Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityTypeBooking.getResvFacilityType())?"GBF-"
								:(Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(facilityTypeBooking.getResvFacilityType())?"TCF-":"");
				String transactionId = prefix + resvId + "";
				String startBookingTime = bookingDate + " "
						+ getBookingTimeHour(facilityTypeBooking.getBeginDatetimeBook()) + ":00";
				String endBookingTime = bookingDate + " "
						+ (getBookingTimeHour(facilityTypeBooking.getEndDatetimeBook()) + 1) + ":00";
				String bayTypeName = getFacilityAttributeName(facilityTypeBooking.getResvFacilityType(),facilityTypeBooking.getFacilitySubtypeId(),facilityTypeBooking.getResvId());
				
				FacilityType facilityType = facilityTypeDao.get(FacilityType.class,facilityTypeBooking.getResvFacilityType().toUpperCase());
				String facilityName = (null != facilityType)?facilityType.getDescription():facilityTypeBooking.getResvFacilityType();
				String content = replaceEmailContent(template.getContentHtml(), userName, transactionId, fullName, startBookingTime, endBookingTime, facilityName, bayTypeName);
				cec.setContent(content);
			}
			
			cec.setSendDate(new Date());

			customerEmailContentDao.save(cec);
			
			try {
				String receiptType = "golfbay";
				String fileName = "GolfBayReservationReceipt-";
				if (!Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityTypeBooking.getResvFacilityType())) {
					receiptType = "tenniscourt";
					fileName = "TennisCourtReservationReceipt-";
				}
				byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, String.valueOf(confirmationID), receiptType);
				List<byte[]> attachmentList = Arrays.asList(attachment);
				List<String> mineTypeList = Arrays.asList("application/pdf");
				List<String> fileNameList = Arrays.asList(fileName+String.valueOf(confirmationID)+".pdf");
				CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
				customerEmailAttach.setEmailSendId(cec.getSendId());
				customerEmailAttach.setAttachmentName(fileNameList.get(0));
				customerEmailAttachDao.save(customerEmailAttach);
				
				//send email start
				mailThreadService.sendWithResponse(cec, attachmentList, mineTypeList, fileNameList);
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

		}
				
		logger.info("CustomerEmailServiceImpl.sendReceiptConfirmationEmail invocation end ...");	

	}
	
	public static String replaceEmailContent(String content ,String userName,String transactionId,String fullName,String startBookingTime,String endBookingTime,String facilityType,String bayTypeName){
		content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, userName)
				.replace("{confirmationID}", transactionId)
				.replace("{fullname}", fullName)
				.replace("{startTime}", startBookingTime)
				.replace("{endTime}", endBookingTime)
				.replace("{facilityName}", facilityType)
				.replace("{bayType}", bayTypeName)
				.replace("\\n", "\n");
		return content;
	}
	
	private int getBookingTimeHour(Date bookingTime){
		Calendar cal = Calendar.getInstance();
		cal.setTime(bookingTime);
		return cal.get(Calendar.HOUR_OF_DAY);
	}
	
	public  String getFacilityAttributeName(String facilityType,String facilitySubtype,long resvId) {
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			MemberFacilityBookAdditionAttr attr = memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttrList(resvId).get(0);
			if(null != attr){
				FacilityAttributeCaption caption = facilityAttributeCaptionDao.getFacilityAttributeCaptionByAttributeId(attr.getId().getAttributeId());
				if (null != caption)return caption.getCaption();
			}
		}else{
			FacilitySubType subType = facilitySubTypeDao.get(FacilitySubType.class, facilitySubtype);
			if (null != subType)return subType.getName();
		}
		return "";
	}
	

	private long getReservationTransactionNo(MemberFacilityTypeBooking facilityTypeBooking)
	{
		List<CustomerOrderTrans>  trans = customerOrderTransDao.getPaymentDetailsByOrderNo(facilityTypeBooking.getOrderNo());
		if(null != trans && trans.size() > 0){
			for(CustomerOrderTrans tran : trans){
				if(tran.getStatus().equals(Constant.Status.SUC.name())){
					return tran.getTransactionNo();
				}
			}
		}
		return 0l;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendRefundEmail(long resvId) {

		logger.info("CustomerEmailServiceImpl.sendRefundEmail invocation start ...");
		
		MemberFacilityTypeBooking facilityTypeBooking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);

		boolean mailSuccess = false;

		CustomerEmailContent cec = new CustomerEmailContent();
		
		if (facilityTypeBooking != null && facilityTypeBooking.getCustomerOrderHd() != null) {
			Long customerID = facilityTypeBooking.getCustomerOrderHd().getCustomerId();
			
			CustomerProfile cp = customerProfileDao.getById(customerID);
			if (cp == null) {
				throw new GTACommonException(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
			}
			String address = cp.getContactEmail();

			MessageTemplate template = messageTemplateDao.getTemplateByFunctionId("refund");
			
			if (template == null) {
				logger.error("can't find the refund template");
				return;
			}
			cec.setRecipientCustomerId(customerID.toString());
			cec.setRecipientEmail(address);
			cec.setSubject(template.getMessageSubject());
			cec.setNoticeType(Constant.NOTICE_TYPE_FACILITY);
			
			if (template.getContentHtml() != null) {
				String content = template.getContentHtml().replace(Constant.PLACE_HOLDER_TO_CUSTOMER, getName(cp));
				content = content.replace(Constant.PLACE_HOLDER_FROM_USER, Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER);
				cec.setContent(content);
			}
			
			cec.setSendDate(new Date());

			try {
				//send email start
				mailSuccess = MailSender.sendEmail(cec.getRecipientEmail(), null, null, cec.getSubject(), cec.getContent(), null);
				
				cec.setStatus(EmailStatus.SENT.getName());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			String sendID = (String) customerEmailContentDao.addCustomerEmail(cec);
			cec.setSendId(sendID);
					
		}
		
		if (!mailSuccess) {
			cec.setStatus(EmailStatus.FAIL.getName());
			customerEmailContentService.modifyCustomerEmailContent(cec);

			logger.error("Error in Sending mail!");
			throw new GTACommonException(GTAError.FacilityError.SEND_EMAIL_FAILURE);
		}
		
		logger.info("CustomerEmailServiceImpl.sendRefundEmail invocation end ...");	
	}
	
	private String getName(CustomerProfile cp) {
		
		if (cp == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName()).append(" ").append(cp.getSurname());
		return sb.toString();
	}
}
