package com.sinodynamic.hkgta.service.sys;

import java.util.List;

import com.sinodynamic.hkgta.dto.sys.AssignRoleDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.UserRole;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface UserRoleService extends IServiceBase<UserRole>{

	public List<UserRoleDto> getUserRoleListByUserId(String userId);

	public ResponseResult assignRole(AssignRoleDto dto,String createBy);

	public ResponseResult removeRole(AssignRoleDto dto);

	public ResponseResult getUserPermissions(String userId);

	ResponseResult getUserPermissionsV2(String userId);

	ResponseResult loadUserPermissionsV2(String userId);

}
