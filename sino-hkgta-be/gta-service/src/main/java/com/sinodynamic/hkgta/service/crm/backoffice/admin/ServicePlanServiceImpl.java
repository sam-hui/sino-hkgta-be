
package com.sinodynamic.hkgta.service.crm.backoffice.admin;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.sinodynamic.hkgta.dao.AbstractCommonDataQueryDao;
import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.PermitCardLogDao;
import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.crm.AgeRangeDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDatePosDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanOfferPosDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanPosDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanRatePosDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanRightMasterDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTypeDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dao.rpos.PurchasedDaypassDtoDao;
import com.sinodynamic.hkgta.dto.ListPageDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.PosServiceItemPriceDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanPosDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanRightDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeDto;
import com.sinodynamic.hkgta.dto.membership.FacilityTransactionInfo;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassBackDto;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardLog;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.ServicePlanDatePos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacilityPK;
import com.sinodynamic.hkgta.entity.crm.ServicePlanOfferPos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanPos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRatePos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.CollectionUtil.PairCallBack;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RefundPeriodType;
import com.sinodynamic.hkgta.util.exception.ErrorCodeException;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.pagination.ListPage.OrderType;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Service
public class ServicePlanServiceImpl extends ServiceBase<ServicePlan> implements ServicePlanService {
	@Autowired
	private ServicePlanDao servicePlanDao;
	
	@Autowired
	CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	CustomerOrderPermitCardDao customerOrderPermitCardDao;
	
	@Autowired
	private GlobalParameterDao globalParameterDao;  
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	@Autowired
	private ServicePlanPosDao servicePlanPosDao;
	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;
	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
	
	@Autowired
	private ServicePlanOfferPosDao servicePlanOfferPosDao;
	@Autowired
	private ServicePlanRatePosDao servicePlanRatePosDao;
	@Autowired
	private ServicePlanDatePosDao servicePlanDatePosDao;
	//@Autowired
	//private ServicePlanViewDao servicePlanViewDao;
	@Autowired
	private FacilityTypeDao facilityTypeDao;
	
	@Autowired
	private ServicePlanRightMasterDao servicePlanRightMasterDao;
	
	@Autowired
	private AgeRangeDao ageRangeDao;
	
	@Autowired
	private  PurchasedDaypassDtoDao PurchasedDaypassDtoDao;
	@Autowired
	private PermitCardMasterDao permitCardMasterDao;
	@Autowired
	private PermitCardLogDao permitCardLogDao;
	@Autowired
	private PermitCardMasterService permitCardMasterService;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	@Autowired
	MemberFacilityTypeBookingService memberFacilityTypeBookingService;

	@Autowired
	@Qualifier("commonBySQLDao")
	AbstractCommonDataQueryDao abstractCommonDataQueryDao;
	
	public Logger logger = Logger.getLogger(ServicePlanServiceImpl.class);

	private void setValuesForServicePlan(ServicePlan plan, ServicePlanDto planDto){
		/*
		 * common
		 */
		plan.setPlanName(planDto.getPlanName());
		plan.setEffectiveStartDate(DateConvertUtil.parseString2Date(planDto.getEffectiveStartDate(),"yyyy-MM-dd"));
		plan.setEffectiveEndDate(DateConvertUtil.parseString2Date(planDto.getEffectiveEndDate(),"yyyy-MM-dd"));
		if(CommUtil.notEmpty(planDto.getCreateBy())){
			plan.setCreateBy(planDto.getCreateBy());
		}
		if(CommUtil.notEmpty(planDto.getUpdateBy())){
			plan.setUpdateBy(planDto.getUpdateBy());
		}
		if(null != planDto.getCreateDate()){
			plan.setCreateDate(planDto.getCreateDate());
		}
		if(null != planDto.getUpdateDate()){
			plan.setUpdateDate(planDto.getUpdateDate());
		}
		/**
		 * for serviceplan
		 */
		plan.setContractLengthMonth(planDto.getContractLengthMonth());
		plan.setPassNatureCode(planDto.getPassNatureCode());
		plan.setPlanNo(planDto.getPlanNo());
		plan.setPassPeriodType(planDto.getPassPeriodType());
		if(CommUtil.notEmpty(planDto.getStatus())){
			plan.setStatus(planDto.getStatus());
		}
		/**
		 * for  daypass
		 */
		//refer to Purchased By in the page
		plan.setSubscriberType(planDto.getSubscriberType());
	}
	
	private void addFacilityTypes4ServicePlan(ServicePlan servicePlan,Collection<FacilityTypeDto> facilityTypes){
		if (servicePlan.getServicePlanFacilities() == null){
			servicePlan.setServicePlanFacilities(new ArrayList<ServicePlanFacility>());
		}
		List<ServicePlanFacility> servicePlanFacilities = servicePlan.getServicePlanFacilities();
		
		for (FacilityTypeDto facilityTypeDto:facilityTypes){
			ServicePlanFacility servicePlanFacility = new ServicePlanFacility();
			
			ServicePlanFacilityPK servicePlanFacilityPK = new ServicePlanFacilityPK();
			servicePlanFacilityPK.setFacilityTypeCode(facilityTypeDto.getTypeCode());
			servicePlanFacilityPK.setServicePlanId(servicePlan.getPlanNo());
			
			servicePlanFacility.setId(servicePlanFacilityPK);
			servicePlanFacility.setFacilityTypeCode(facilityTypeDto.getTypeCode());
			servicePlanFacility.setPermission(facilityTypeDto.getPermission());
			servicePlanFacility.setServicePlan(servicePlan);
			servicePlanFacilities.add(servicePlanFacility);
		}
	}
	
	
	private void addRights4ServicePlan(ServicePlan servicePlan,Collection<ServicePlanRightDto> rights){
		if(servicePlan.getServicePlanAdditionRules() == null){
			servicePlan.setServicePlanAdditionRules(new ArrayList<ServicePlanAdditionRule>());
		}
		List<ServicePlanAdditionRule> servicePlanAdditionRules = servicePlan.getServicePlanAdditionRules();
		
		for (ServicePlanRightDto servicePlanRightDto : rights){
			ServicePlanAdditionRule servicePlanAdditionRule = new ServicePlanAdditionRule();
			servicePlanAdditionRule.setRightCode(servicePlanRightDto.getRightCode());
			servicePlanAdditionRule.setInputValue(servicePlanRightDto.getInputValue());
			servicePlanAdditionRule.setServicePlan(servicePlan);
			servicePlanAdditionRules.add(servicePlanAdditionRule);
		}
	}
	

	private void updatePriceItems4ServicePlan(final ServicePlan servicePlan,final PosServiceItemPrice price,final AgeRange ageRange){
		CollectionUtil.loop(servicePlan.getServicePlanPoses(), new NoResultCallBack<ServicePlanPos>(){

			@Override
			public void execute(ServicePlanPos servicePlanPos, int index) {
				servicePlanPos.setAgeRange(ageRange.getAgeRangeCode());

				String hql = "FROM ServicePlanOfferPos s where s.servPosId=?";
				List<Serializable> param = new ArrayList<Serializable>();
				param.add(servicePlanPos.getServPosId());
				List<ServicePlanOfferPos> planOfferPoses = servicePlanOfferPosDao.getByHql(hql, param);
				
				
				//For normal price update
				PosServiceItemPrice normalServiceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, servicePlanPos.getPosItemNo());
				//String normalPriceDescription = servicePlan.getPlanName()+Constant.ENROLL_PRICE_DESC_SUFFIX;//For the normal price , it is ok to do this
				String normalPriceDescription = "Enrollment - "+servicePlan.getPlanName();
				servicePlanPos.setDescription(normalPriceDescription);//For normal price description update
				normalServiceItemPrice.setDescription(normalPriceDescription);//For normal price description update
				normalServiceItemPrice.setItemPrice(price.getItemPrice());//For normal price update
				normalServiceItemPrice.setStatus(Constant.Status.ACT.toString());
				//For special price update
				for (ServicePlanOfferPos specialOffer : planOfferPoses){
					String offerCode = specialOffer.getOfferCode();
					String specialPriceDescription =  "Renewal  - "+servicePlan.getPlanName();
					
//					if(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL.equals(offerCode))
//						specialPriceDescription = servicePlan.getPlanName()+Constant.RENEW_PRICE_DESC_SUFFIX;
//					else
//						specialPriceDescription = servicePlan.getPlanName()+Constant.SPECIAL_PRICE_DESC_SUFFIX;
					specialOffer.setDescription(specialPriceDescription);
					
					PosServiceItemPrice specialServiceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, specialOffer.getPosItemNo());
					specialServiceItemPrice.setDescription(specialPriceDescription);
					specialServiceItemPrice.setItemPrice(price.getItemPrice());
					specialServiceItemPrice.setStatus(Constant.Status.ACT.toString());
				}
				
				
			}
			
		});
	}
	
	
	
	private void addPriceItems4ServicePlan(ServicePlan servicePlan, PosServiceItemPrice price, AgeRange ageRange){
		if(servicePlan.getServicePlanPoses() == null){
			servicePlan.setServicePlanPoses(new ArrayList<ServicePlanPos>());
		}
		
		//For Enrollment Relationship
		ServicePlanPos enrollPos = new ServicePlanPos();
		enrollPos.setAgeRange(ageRange.getAgeRangeCode());
		//enrollPos.setDescription(servicePlan.getPlanName()+Constant.ENROLL_PRICE_DESC_SUFFIX);
		enrollPos.setDescription("Enrollment - "+servicePlan.getPlanName());
		enrollPos.setServicePlan(servicePlan);
		servicePlanPosDao.save(enrollPos);
	
		//For Renew Relationship
		ServicePlanOfferPos offerPos = new ServicePlanOfferPos();
		offerPos.setServPosId(enrollPos.getServPosId());
		offerPos.setOfferCode(Constant.PriceCatagory.RENEW.toString());
		//offerPos.setDescription(servicePlan.getPlanName() + Constant.RENEW_PRICE_DESC_SUFFIX);
		offerPos.setDescription("Renewal - "+servicePlan.getPlanName());
		servicePlanOfferPosDao.save(offerPos);
		
		//For Enrollment Price Item
		PosServiceItemPrice serviceItemEnroll = new PosServiceItemPrice();
		serviceItemEnroll.setItemNo(Constant.ENROLL_PRICE_ITEM_PREFIX + CommUtil.formatPosServiceItemNo(enrollPos.getServPosId()));
		serviceItemEnroll.setItemCatagory(Constant.PriceCatagory.SRV.toString());
		serviceItemEnroll.setDescription(enrollPos.getDescription());
		serviceItemEnroll.setStatus(Constant.Status.ACT.toString());
		serviceItemEnroll.setItemPrice(price.getItemPrice());
		posServiceItemPriceDao.save(serviceItemEnroll);
		enrollPos.setPosItemNo(serviceItemEnroll.getItemNo());
		
		//For Renewal Price Item
		PosServiceItemPrice serviceItemRenew = new PosServiceItemPrice();
		serviceItemRenew.setItemNo(Constant.RENEW_PRICE_ITEM_PREFIX + CommUtil.formatPosServiceItemNo(enrollPos.getServPosId()));
		serviceItemRenew.setItemCatagory(Constant.PriceCatagory.SRV.toString());
		serviceItemRenew.setDescription(offerPos.getDescription());
		serviceItemRenew.setStatus(Constant.Status.ACT.toString());
		serviceItemRenew.setItemPrice(price.getItemPrice());
		posServiceItemPriceDao.save(serviceItemRenew);
		offerPos.setPosItemNo(serviceItemRenew.getItemNo());
		
		
		servicePlan.getServicePlanPoses().add(enrollPos);
	}
	@Transactional
	@Override
	public ResponseMsg saveDayPass(ServicePlanDto dto) {
		ServicePlan dayPass = new ServicePlan();
		
		/* set ServicePlan master table info */
		setValuesForServicePlan(dayPass,dto);
		//set default value for daypass
		dayPass.setPassNatureCode("DP");
		dayPass.setStatus(Constant.Status.ACT.toString());
		//save first
		servicePlanDao.saveServicePlan(dayPass);
		
		//-------------set value for ServicePlanFacility begin ---------
		addFacilityTypes4ServicePlan(dayPass,dto.getFacilityTypes());
		//--------set value for ServicePlanFacility----------END
		//save low service_plan_pos
		addLowRate4Daypass(dayPass, dto.getLowRate(), dto.getAgeRange());
		//save high service_plan_rate_pos
		addHighRate4Daypass(dayPass, dto);
		this.servicePlanDao.save(dayPass);
		this.responseMsg.initResult(GTAError.Success.SUCCESS);
		if(this.checkServicePlanForSaveByName(dayPass.getPlanName(), "DP")){
			responseMsg.initResult(GTAError.DayPassError.DAY_PASS_EXISTED, new Object[]{dayPass.getPlanName(), dayPass.getPlanNo()});
		}
	
		return this.responseMsg;
	}
	private void addHighRate4Daypass(ServicePlan servicePlan,ServicePlanDto dto){
		Long servPosId = servicePlan.getServicePlanPoses().get(0).getServPosId();
		PosServiceItemPrice itemPriceHighRate = new PosServiceItemPrice();
		ServicePlanRatePos ratePos = new ServicePlanRatePos();
		ratePos.setServPosId(servPosId);
		ratePos.setRateType(Constant.RateType.HI.toString());
		//ratePos.setDescription(servicePlan.getPlanName()+" "+dto.getAgeRange().getAgeRangeCode()+ " "+dto.getHighRate()+" "+Constant.RateType.HI.toString());
		ratePos.setDescription("Day Pass - "+servicePlan.getPlanName()+" (High Rate)");
		ratePos.setWeeklyRepeatBit(dto.getWeeklyRepeatBit());
		this.servicePlanRatePosDao.save(ratePos);
		//need to change
		itemPriceHighRate.setItemNo(Constant.SRV_HI_RATE_ITEM_PREFIX_DP+CommUtil.formatPosServiceItemNo(ratePos.getRateId()));
		itemPriceHighRate.setItemCatagory(Constant.PriceCatagory.DP.toString());
		itemPriceHighRate.setDescription(ratePos.getDescription());
		itemPriceHighRate.setStatus(Constant.Status.ACT.toString());
		itemPriceHighRate.setItemPrice(dto.getHighRate());
		//need to add suppliNo
		//itemPriceHighRate.setSupplierNo("zero-test");
		this.posServiceItemPriceDao.save(itemPriceHighRate);
		ratePos.setPosItemNo(itemPriceHighRate.getItemNo());
		
		Set<ServicePlanDatePos> planDatePosSet = new HashSet<ServicePlanDatePos>();
		//save special hight Rate day
		if(dto.getSpecialDate()!=null && dto.getSpecialDate().size()>0){
			List<String> specialDate =  dto.getSpecialDate();
			for(int i=0;i<specialDate.size();i++) {
				if(CommUtil.notEmpty(specialDate.get(i))) {
					ServicePlanDatePos temp = new ServicePlanDatePos();
					temp.setAnnualRepeat(Constant.AnnualRepeat.N.toString());
					temp.setSpecialDate(DateConvertUtil.parseString2Date(specialDate.get(i).replace("/", "-"), "yyyy-MM-dd"));
					temp.setPlanRatePos(ratePos);
					this.servicePlanDatePosDao.save(temp);
					planDatePosSet.add(temp);
				}
			}
		}
		ratePos.setPlanDatePosSet(planDatePosSet);
		this.servicePlanRatePosDao.saveOrUpdate(ratePos);
		
	}
	private void addLowRate4Daypass(ServicePlan servicePlan, BigDecimal lowRate, AgeRange ageRange){
		if(servicePlan.getServicePlanPoses() == null){
			servicePlan.setServicePlanPoses(new ArrayList<ServicePlanPos>());
		}
		PosServiceItemPrice itemPriceLowRate = new PosServiceItemPrice();
		ServicePlanPos servicePlanPosLowRate = new ServicePlanPos();
		servicePlanPosLowRate.setAgeRange(ageRange.getAgeRangeCode());
		
		//servicePlanPosLowRate.setDescription(servicePlan.getPlanName()+" "+ageRange.getAgeRangeCode()+ " "+lowRate); 
		servicePlanPosLowRate.setDescription("Day Pass - "+servicePlan.getPlanName()+" (Low Rate)"); 
		servicePlanPosLowRate.setServicePlan(servicePlan);
		servicePlanPosDao.save(servicePlanPosLowRate);
		//need to change
		itemPriceLowRate.setItemNo(Constant.ENROLL_PRICE_PREFIX_DP+servicePlanPosLowRate.getServPosId());
		itemPriceLowRate.setItemCatagory(Constant.PriceCatagory.DP.toString());
		itemPriceLowRate.setDescription(servicePlanPosLowRate.getDescription());
		itemPriceLowRate.setStatus(Constant.Status.ACT.toString());
		itemPriceLowRate.setItemPrice(lowRate);
		//need to add suppliNo
		//itemPriceLowRate.setSupplierNo("zero-test");
		servicePlanPosLowRate.setPosItemNo(itemPriceLowRate.getItemNo());
		posServiceItemPriceDao.save(itemPriceLowRate);
		
		
		servicePlan.getServicePlanPoses().add(servicePlanPosLowRate);
	}
	@Transactional
	public ResponseMsg saveServicePlan(ServicePlanDto servicePlanJson) {
		try {
			String planName = servicePlanJson.getPlanName();
			ServicePlan plan = new ServicePlan();
			
			/* set ServicePlan master table info */
			setValuesForServicePlan(plan,servicePlanJson);
			servicePlanDao.saveServicePlan(plan);//save first
			Long planNo = plan.getPlanNo();
			
			//-------------set value for ServicePlanFacility begin ---------
			addFacilityTypes4ServicePlan(plan,servicePlanJson.getFacilityTypes());
			//--------set value for ServicePlanFacility----------END
			
			//--------set value for ServicePlanAdditionRule----------BEGIN//
			addRights4ServicePlan(plan,servicePlanJson.getServicePlanRights());
			//--------set value for ServicePlanAdditionRule----------END//
			
			//Set price items
			addPriceItems4ServicePlan(plan, servicePlanJson.getPosServiceItemPrice(), servicePlanJson.getAgeRange());
			
			servicePlanDao.saveServicePlan(plan);
			String  passNatureCode = "LT";
							
			if(checkServicePlanForSaveByName(planName, "LT")){
				responseMsg.initResult(GTAError.DayPassError.SRV_PLAN_EXISTED, new Object[]{plan.getPlanName(),plan.getPlanNo()});
//				responseMsg.setReturnCode("0");
//				responseMsg.setErrorMessageEN(getI18nMessge(GTAError.DayPassError.SRV_PLAN_EXISTED.getCode(), new String[]{plan.getPlanName()}));
			}else{
				this.responseMsg.initResult(GTAError.Success.SUCCESS);
			}
			return this.responseMsg;
		}catch (ErrorCodeException e) {
			throw e;
		}catch (Exception e) {
			logger.error(e);
			throw new ErrorCodeException(GTAError.DayPassError.FAIL_SAVE_SRV_PLAN);
		}
		
	}

	
	@Transactional
	public ResponseMsg updateServicePlan(ServicePlanDto servicePlanJson) {
		try {
			if(isServicePlanUsed(servicePlanJson.getPlanNo())){
				throw new ErrorCodeException(GTAError.DayPassError.SRV_IS_USED);
			}
			ServicePlan servicePlanTemp = servicePlanDao.get(ServicePlan.class, servicePlanJson.getPlanNo());
			String planName = servicePlanTemp.getPlanName();
								
			ServicePlan plan = servicePlanDao.get(ServicePlan.class, servicePlanJson.getPlanNo());
			plan.setStatus(Constant.Status.ACT.toString());
			/* set ServicePlan master table info */
			setValuesForServicePlan(plan,servicePlanJson);
			updatePriceItems4ServicePlan(plan, servicePlanJson.getPosServiceItemPrice(), servicePlanJson.getAgeRange());
			//updateNormalPrice4ServicePlan(plan, servicePlanJson.getPosServiceItemPrice(), servicePlanJson.getAgeRange());
			//updateRenewPrice4ServicePlan(plan);
			
			//--------For Facility Type storage----------BEGIN//
			//delete facility types which are related to serviceplan
			CollectionUtil.retainAll(plan.getServicePlanFacilities(), servicePlanJson.getFacilityTypes(), "facilityTypeCode", "typeCode", new PairCallBack<ServicePlanFacility,FacilityTypeDto>(){
				@Override
				public void execute(ServicePlanFacility t1,FacilityTypeDto t2, int index, boolean isRemoved) {
					if(!isRemoved){
						t1.setPermission(t2.getPermission());
						servicePlanFacilityDao.update(t1);
					}
					if(isRemoved){
						servicePlanFacilityDao.delete(t1);
					}
				}
			});
			CollectionUtil.removeAll(servicePlanJson.getFacilityTypes(), plan.getServicePlanFacilities(), "typeCode", "facilityTypeCode",null);
			addFacilityTypes4ServicePlan(plan,servicePlanJson.getFacilityTypes());
			
			
			//--------For Facility Type storage----------END//
			
			//--------For Rights storage----------BEGIN//
			CollectionUtil.retainAll(plan.getServicePlanAdditionRules(), servicePlanJson.getServicePlanRights(), "rightCode", "rightCode", new PairCallBack<ServicePlanAdditionRule,ServicePlanRightDto>(){

				@Override
				public void execute(ServicePlanAdditionRule t1,ServicePlanRightDto t2, int index, boolean isRemoved) {
					if(!isRemoved){
						t1.setInputValue(t2.getInputValue());
						servicePlanAdditionRuleDao.update(t1);
					}
					if(isRemoved){
						servicePlanAdditionRuleDao.delete(t1);
					}
				}
				
			});
			CollectionUtil.removeAll(servicePlanJson.getServicePlanRights(), plan.getServicePlanAdditionRules(), "rightCode", "rightCode",null);
			addRights4ServicePlan(plan,servicePlanJson.getServicePlanRights());
			//--------For Rights storage----------END//
						
			servicePlanDao.saveOrUpdate(plan);
								
			if(checkServicePlanByName(plan.getPlanName(),"LT",servicePlanTemp.getPlanNo())){
				if(planName.equals(plan.getPlanName())&&servicePlanTemp.getPassNatureCode().equals("LT"))	{
					this.responseMsg.initResult(GTAError.Success.SUCCESS);
				}else{	
					responseMsg.initResult(GTAError.DayPassError.SRV_PLAN_EXISTED,  new Object[]{plan.getPlanName()});
//					responseMsg.setReturnCode("0");
//					responseMsg.setErrorMessageEN(getI18nMessge(GTAError.DayPassError.SRV_PLAN_EXISTED.getCode(), new String[]{plan.getPlanName()}));
				}
			}else{
				this.responseMsg.initResult(GTAError.Success.SUCCESS);
			}

			return this.responseMsg;
		} catch (ErrorCodeException e) {
			throw e;
		} catch (Exception e) {
			logger.error(e);
			throw new ErrorCodeException(GTAError.DayPassError.FAIL_UPD_SRV_PLAN);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hkgta.webservice.crm.ServicePlanService#getServicPlans(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Transactional
	public List<ServicePlanDto> getAllServicPlan() {
		List<ServicePlan> servicePlans = servicePlanDao.getAllServicePlan();
		List<ServicePlanDto> retList = new ArrayList<ServicePlanDto>();
		for(ServicePlan plan :servicePlans ){
			ServicePlanDto planJson = new ServicePlanDto();
			setSimpleFiled4Dto(planJson, plan);
			retList.add(planJson);
		}
		return retList;
	}
	@Transactional
	public ServicePlanDto getServicPlan(ServicePlanDto key) {
		ServicePlan servicePlan = (ServicePlan)servicePlanDao.getUniqueByCols(ServicePlan.class, new String[]{"planName"}, new Serializable[]{key.getPlanName()});
		ServicePlanDto planJson = new ServicePlanDto();
		setSimpleFiled4Dto(planJson, servicePlan);
		return planJson;
	}
	@Transactional
	public ResponseMsg deleteServicPlan(Long planNo) {
		ServicePlan servicePlan = this.servicePlanDao.get(ServicePlan.class, planNo);
		ServicePlanPos planPos = servicePlan.getServicePlanPoses().get(0);
		String hql = "FROM ServicePlanOfferPos s where s.servPosId=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planPos.getServPosId());
		List<ServicePlanOfferPos> planOfferPoses = this.servicePlanOfferPosDao.getByHql(hql, param);
		ServicePlanOfferPos offerPos = planOfferPoses.get(0);
		for(Iterator it=planOfferPoses.iterator(); it.hasNext(); ){
			this.servicePlanOfferPosDao.delete((ServicePlanOfferPos) it.next());
		}
		this.servicePlanDao.delete(servicePlan);
		this.posServiceItemPriceDao.deleteById(PosServiceItemPrice.class, planPos.getPosItemNo());
		this.posServiceItemPriceDao.deleteById(PosServiceItemPrice.class, offerPos.getPosItemNo());
		ResponseMsg responseMsg = new ResponseMsg("0", "success");
		return responseMsg;
	}
	@Transactional
	public ResponseMsg duplicateServicePlan(Long planNo, String userId) {
		ServicePlanDto  dto = this.getServicPlanById(planNo);
		ServicePlan servicePlan = servicePlanDao.getByplanName(CommUtil.getPrefixName(dto.getPlanName()));
		dto.setPlanName(CommUtil.getDuplicateName(servicePlan.getPlanName()));
		dto.setCreateBy(userId);
		dto.setUpdateBy(userId);
		dto.setCreateDate(new Date());
		dto.setUpdateDate(new Date());
		this.saveServicePlan(dto);
		ResponseMsg responseMsg = new ResponseMsg("0", "success");
		return responseMsg;
	}
	@Transactional
	public ResponseMsg duplicateServicePlan2(Long planNo, String userId) {
		try {
			ServicePlan plan = new ServicePlan();
			ServicePlan servicePlan = this.servicePlanDao.get(ServicePlan.class, planNo);
			servicePlan.getServicePlanAdditionRules().size();
			servicePlan.getServicePlanPoses().size();
			servicePlan.getServicePlanFacilities().size();

			plan.setPlanName(servicePlan.getPlanName()+"_copy");
			plan.setPassNatureCode(servicePlan.getPassNatureCode());
			plan.setContractLengthMonth(servicePlan.getContractLengthMonth());
			plan.setPassPeriodType(servicePlan.getPassPeriodType());
			plan.setEffectiveEndDate(servicePlan.getEffectiveEndDate());
			plan.setEffectiveStartDate(servicePlan.getEffectiveStartDate());
			plan.setSubscriberType(servicePlan.getSubscriberType());
			plan.setStatus(servicePlan.getStatus());
			plan.setCreateBy(userId);
			plan.setCreateDate(new Date());
			plan.setUpdateBy(userId);
			plan.setUpdateDate(new Date());
			servicePlanDao.save(plan);
			List<ServicePlanFacility> servicePlanFacilitiesList = new ArrayList<ServicePlanFacility>();
			for (ServicePlanFacility source : servicePlan
					.getServicePlanFacilities()) {
				ServicePlanFacility target = new ServicePlanFacility();
				BeanUtils.copyProperties(source, target, "id", "sysId");
				ServicePlanFacilityPK id = new ServicePlanFacilityPK(
						plan.getPlanNo(), source.getFacilityTypeCode());
				this.servicePlanFacilityDao.getCurrentSession().evict(source);
				target.setId(id);
				target.setServicePlan(plan);
				this.servicePlanFacilityDao.save(target);
				servicePlanFacilitiesList.add(target);
			}
			plan.setServicePlanFacilities(servicePlanFacilitiesList);
			List<ServicePlanAdditionRule> additionRulesList = new ArrayList<ServicePlanAdditionRule>();
			for (ServicePlanAdditionRule source : servicePlan
					.getServicePlanAdditionRules()) {
				ServicePlanAdditionRule target = new ServicePlanAdditionRule();
				BeanUtils.copyProperties(source, target, "ruleId");
				this.servicePlanAdditionRuleDao.getCurrentSession().evict(
						source);
				target.setServicePlan(plan);

				this.servicePlanAdditionRuleDao.save(target);
				additionRulesList.add(target);

			}
			plan.setServicePlanAdditionRules(additionRulesList);
			//save ServicePlanPos and PosServiceItemPrice
			List<ServicePlanPos> servicePlanPosList = new ArrayList<ServicePlanPos>();
			for(ServicePlanPos source : servicePlan.getServicePlanPoses()) {
				PosServiceItemPrice posServiceItemPrice = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, source.getPosItemNo());
				this.posServiceItemPriceDao.evict(posServiceItemPrice);
				posServiceItemPrice.setItemNo("SRV000000"+plan.getPlanNo());
				posServiceItemPrice.setDescription(posServiceItemPrice.getDescription()+"_copy");
				posServiceItemPrice.setItemCatagory(Constant.PriceCatagory.SRV.toString());
				this.posServiceItemPriceDao.save(posServiceItemPrice);
				this.servicePlanPosDao.evict(source);
				source.setPosItemNo(posServiceItemPrice.getItemNo());
				//source.setServPosId(0l);
				this.servicePlanPosDao.save(source);
				servicePlanPosList.add(source);
				source.setServicePlan(plan);
				String hql = "FROM ServicePlanOfferPos s where s.servPosId=?";
				List<Serializable> param = new ArrayList<Serializable>();
				param.add(source.getServPosId());
				List<ServicePlanOfferPos> planRatePos = this.servicePlanOfferPosDao.getByHql(hql, param);
				ServicePlanOfferPos offerPos = new ServicePlanOfferPos();
				BeanUtils.copyProperties(planRatePos.get(0), offerPos, "offerId");
				offerPos.setDescription(offerPos.getDescription()+"_copy");
				PosServiceItemPrice offerPosPrice = new PosServiceItemPrice();
				offerPosPrice.setItemNo("RENEW000000"+plan.getPlanNo());
				offerPosPrice.setDescription(offerPos.getDescription());
				offerPosPrice.setItemCatagory(Constant.PriceCatagory.SRV.toString());
				this.posServiceItemPriceDao.save(offerPosPrice);
				offerPos.setPosItemNo(offerPosPrice.getItemNo());
				offerPos.setServPosId(source.getServPosId());
				this.servicePlanOfferPosDao.save(offerPos);
			}
			plan.setServicePlanPoses(servicePlanPosList);
			servicePlanDao.saveOrUpdate(plan);
			ResponseMsg status = new ResponseMsg("0", "duplicate success");
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Transactional
	public ResponseMsg changeServicePlanStatus(Long planNo,String status,String userId) {
		try {
			ServicePlan servicePlan = servicePlanDao.get(ServicePlan.class, planNo);
			servicePlan.setUpdateBy(userId);
			servicePlan.setUpdateDate(new Date());
			servicePlan.setStatus(status);
			ServicePlanPos planPos = servicePlan.getServicePlanPoses().get(0);//Only one normal price per Service Plan, so this is correct
			String hql = "FROM ServicePlanOfferPos s where s.servPosId=?";
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(planPos.getServPosId());
			List<ServicePlanOfferPos> planOfferPoses = this.servicePlanOfferPosDao.getByHql(hql, param);//Get special prices

			PosServiceItemPrice posServiceItemPriceEnroll = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, planPos.getPosItemNo());//Set normal price status
			posServiceItemPriceEnroll.setStatus(status);
			for(ServicePlanOfferPos offer : planOfferPoses){//Set special price status
				PosServiceItemPrice posServiceItemPrice = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, offer.getPosItemNo());
				posServiceItemPrice.setStatus(status);
			}
			
			ResponseMsg responseMsg = new ResponseMsg("0", "success");
			return responseMsg;
		} catch (Exception e) {
			 e.printStackTrace();
			 throw new RuntimeException(e.getMessage());
		}

	}
	@Transactional
	public ServicePlanDto getServicPlanById(Long planNo) {
		ServicePlanDto planJson = new ServicePlanDto();
		ServicePlan servicePlan = (ServicePlan) servicePlanDao.get(ServicePlan.class, planNo);
		setSimpleFiled4Dto(planJson, servicePlan);
		ServicePlanPos planPos = servicePlan.getServicePlanPoses().get(0);
		setAgeRange4Dto(planJson, planPos.getAgeRange());
		servicePlan.getServicePlanAdditionRules().size();
		setFacility4Dto(planJson, servicePlan);
		setServicePlanRights4Dto(planJson, servicePlan);
		
		List<ServicePlanPosDto> servicePlanPosDtos = new ArrayList<ServicePlanPosDto>();
		ServicePlanPosDto planPosDto = new ServicePlanPosDto();
		planPosDto.setDescription(planPos.getDescription());
		PosServiceItemPrice itemPrice = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, planPos.getPosItemNo());
		PosServiceItemPriceDto itemPriceDto = new PosServiceItemPriceDto();
		itemPriceDto.setItemPrice(itemPrice.getItemPrice());
		planPosDto.setPosServiceItemPriceDto(itemPriceDto);
		servicePlanPosDtos.add(planPosDto);
		planJson.setPosServiceItemPrice(itemPrice);
		planJson.setServicePlanPosDtos(servicePlanPosDtos);
		return planJson;
	}

	private void setServicePlanRights4Dto(ServicePlanDto planJson,
			ServicePlan servicePlan) {
		List<ServicePlanRightDto> servicePlanRights = new ArrayList<ServicePlanRightDto>();
		for(ServicePlanAdditionRule rule : servicePlan.getServicePlanAdditionRules()) {
			ServicePlanRightDto dto = new ServicePlanRightDto();
			ServicePlanRightMaster master = this.servicePlanRightMasterDao.get(ServicePlanRightMaster.class, rule.getRightCode());
			dto.setRightType(master.getRightType());
			dto.setInputValue(rule.getInputValue());
			dto.setRightCode(rule.getRightCode());
			dto.setServicePlanRightMasterDescription(master.getDescription());
			servicePlanRights.add(dto);
		}
		planJson.setServicePlanRights(servicePlanRights);
	}
	@Transactional
	public ServicePlanDto getServicPlanById2(Long planNo) {
		ServicePlanDto planJson = new ServicePlanDto();
		ServicePlan servicePlan = (ServicePlan) servicePlanDao.get(ServicePlan.class, planNo);
		servicePlan.getServicePlanAdditionRules().size();
		ServicePlanPos planPos = servicePlan.getServicePlanPoses().get(0);
		setAgeRange4Dto(planJson, planPos.getAgeRange());
		setFacility4Dto(planJson, servicePlan);
		setServicePlanRights4Dto(planJson, servicePlan);
		List<ServicePlanPosDto> servicePlanPosDtos = new ArrayList<ServicePlanPosDto>();
		for(ServicePlanPos pos : servicePlan.getServicePlanPoses()){
			String itemNo = pos.getPosItemNo();
			ServicePlanPosDto dto = new ServicePlanPosDto();
			dto.setDescription(pos.getDescription());
			dto.setPosItemNo(pos.getPosItemNo());
			//dto.setWeeklyRepeatBit(pos.getWeeklyRepeatBit());
			PosServiceItemPrice price = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, itemNo);
			PosServiceItemPriceDto  priceDto = new PosServiceItemPriceDto();
			priceDto.setItemPrice(price.getItemPrice());
			dto.setPosServiceItemPriceDto(priceDto);
			planJson.setPosServiceItemPrice(price);
			servicePlanPosDtos.add(dto);
		}
		planJson.setServicePlanPosDtos(servicePlanPosDtos);
		planJson.setServicePlanPosDtos(servicePlanPosDtos);
		setSimpleFiled4Dto(planJson, servicePlan);
		return planJson;
	}

	private void setFacility4Dto(ServicePlanDto planJson,
			ServicePlan servicePlan) {
		servicePlan.getServicePlanFacilities().size();
		List<FacilityTypeDto> facilityTypes = new ArrayList<FacilityTypeDto>();
		for(ServicePlanFacility servicePlanFacility : servicePlan.getServicePlanFacilities()) {
			FacilityTypeDto dto = new FacilityTypeDto();
			dto.setPermission(servicePlanFacility.getPermission());
			FacilityType facilityType = this.facilityTypeDao.get(FacilityType.class, servicePlanFacility.getFacilityTypeCode());
			dto.setDescription(facilityType.getDescription());
			dto.setTypeCode(servicePlanFacility.getFacilityTypeCode());
			facilityTypes.add(dto);
		}
		planJson.setFacilityTypes(facilityTypes);
	}

	private void setAgeRange4Dto(ServicePlanDto planJson,
			String ageRageCode) {
		AgeRange ageRange = new AgeRange();
		ageRange.setAgeRangeCode(ageRageCode);
		planJson.setAgeRange(ageRange);
	}
	private void setAgeRangeWithDescription4Dto(ServicePlanDto planJson,
								 String ageRageCode,List<SysCode> sysCodes) {
		AgeRange ageRange = new AgeRange();
		ageRange.setAgeRangeCode(ageRageCode);
		for (SysCode sysCode : sysCodes) {
			if(StringUtils.equals(sysCode.getCodeValue(),ageRageCode)){
				ageRange.setDescription(sysCode.getCodeDisplay());
			}
		}
		planJson.setAgeRange(ageRange);
	}

	private void setSimpleFiled4Dto(ServicePlanDto planJson,
			ServicePlan servicePlan) {
		planJson.setPlanName(servicePlan.getPlanName());
		planJson.setPassPeriodType(servicePlan.getPassPeriodType());
		planJson.setContractLengthMonth(servicePlan.getContractLengthMonth());
		planJson.setSubscriberType(servicePlan.getSubscriberType());
		planJson.setEffectiveEndDate(DateConvertUtil.parseDate2String((servicePlan.getEffectiveEndDate()),"yyyy-MM-dd"));
		planJson.setEffectiveStartDate(DateConvertUtil.parseDate2String((servicePlan.getEffectiveStartDate()),"yyyy-MM-dd"));
		planJson.setPassNatureCode(servicePlan.getPassNatureCode());
		planJson.setPlanNo(servicePlan.getPlanNo());
		planJson.setStatus(servicePlan.getStatus());
		planJson.setUpdateBy(servicePlan.getUpdateBy());
		planJson.setUpdateDate(servicePlan.getUpdateDate());
		planJson.setCreateBy(servicePlan.getCreateBy());
		planJson.setCreateDate(servicePlan.getCreateDate());
		
	}

	@Transactional
	public ResponseResult getPageList(ListPageDto planPageDto) {
		StringBuilder hqlBuilder = new StringBuilder("select new ServicePlan(s.planNo, s.contractLengthMonth,  s.createBy, "+
			 "s.createDate,  s.effectiveEndDate,  s.effectiveStartDate,"+
			" s.passNatureCode,  s.passPeriodType,  s.planName,"+
			" s.status,  s.updateBy,  s.updateDate) FROM ServicePlan s where 1=1 ");
		StringBuilder countHqlBuilder = new StringBuilder("select count(*) FROM ServicePlan s where 1=1 ");
		ListPage<ServicePlan> servicePlans = new ListPage<ServicePlan>();
		DateTime current = new DateTime(System.currentTimeMillis()) ;
		if(planPageDto.getFilterConditions() !=null && planPageDto.getFilterConditions().size()>0){
			List<Object> filterConditions = planPageDto.getFilterConditions();
			for(int i=0;i<filterConditions.size(); i++){
				hqlBuilder.append("   and   s."+ filterConditions.get(i)+"=?");
				countHqlBuilder.append("   and   s."+ filterConditions.get(i)+"=?");
			}
		}
		List<Object> orderConditions = planPageDto.getOrderConditions();
		List<Object> orderConditionsValue =  planPageDto.getOrderConditionsValue();
		List<ListPage<ServicePlan>.OrderBy> orderByList = new ArrayList<ListPage<ServicePlan>.OrderBy>();
		if(planPageDto.getOrderConditions() !=null && planPageDto.getOrderConditions().size()>0){
			for(int i=0; i<orderConditions.size(); i++) {
				if("asc".equals(orderConditionsValue.get(i))) {
					orderByList.add(servicePlans.new OrderBy("s."+orderConditions.get(i), OrderType.ASCENDING));
				} else {
					orderByList.add(servicePlans.new OrderBy("s."+orderConditions.get(i), OrderType.DESCENDING));
				}
			}
		}
		servicePlans.setOrderByList(orderByList);
		servicePlans.setNumber(planPageDto.getCurrentPage());
		servicePlans.setSize(planPageDto.getPageSize());
		
		ResponseResult result = null;
		List<ListPageDto> dtos = new ArrayList<ListPageDto>();
		try {
			this.servicePlanDao.listByHql(servicePlans, countHqlBuilder.toString(), hqlBuilder.toString(), planPageDto.getFilterConditionsValue());
			for(ServicePlan plan : servicePlans.getList()){
				ListPageDto dto = new ListPageDto();
				dto.setContractLengthMonth(plan.getContractLengthMonth());
				dto.setEffectiveEndDate(DateConvertUtil.getYMDDateAndDateDiff(plan.getEffectiveEndDate()));
				dto.setEffectiveStartDate(DateConvertUtil.getYMDDateAndDateDiff(plan.getEffectiveStartDate()));
				dto.setPassPeriodType(plan.getPassPeriodType());
				dto.setPlanName(plan.getPlanName());
				dto.setPlanNo(plan.getPlanNo());
				dto.setStatus(plan.getStatus());
				dtos.add(dto);
			}
			servicePlans.setDtoList(dtos);
			result = new ResponseResult("0", "success", servicePlans);
		} catch(Exception e) {
			e.printStackTrace();
			result = new ResponseResult("-1", "", "fail");
		}
		return result;
	}

	@Override
	@Transactional
	public boolean isServicePlanUsed(Long planNo) {
		try{
			String sql_1 = "select * from customer_service_subscribe where service_plan_no = ?";
			
			String sql_2 = "select * from corporate_service_subscribe where service_plan_no = ?";
			
			String sql_3 = "select * from customer_enrollment where subscribe_plan_no = ?";
			SQLQuery sqlQuery_1 = this.servicePlanDao.getCurrentSession().createSQLQuery(sql_1);
			SQLQuery sqlQuery_2 = this.servicePlanDao.getCurrentSession().createSQLQuery(sql_2);
			SQLQuery sqlQuery_3 = this.servicePlanDao.getCurrentSession().createSQLQuery(sql_3);
			sqlQuery_1.setParameter(0, planNo);
			sqlQuery_2.setParameter(0, planNo);
			sqlQuery_3.setParameter(0, planNo);
			
			List<Object> list_1 = sqlQuery_1.list();
			List<Object> list_2 = sqlQuery_2.list();
			List<Object> list_3 = sqlQuery_3.list();
			
			if (CollectionUtils.isEmpty(list_1) && CollectionUtils.isEmpty(list_2) && CollectionUtils.isEmpty(list_3))
				return false;
			else
				return true;
		}catch(Exception e){
			logger.error(e);
			return true;
		}
	}
	
	@Override
	@Transactional
	public boolean isServicePlanUsed(Long planNo, String paymentStatus) {
		if(planNo==null || !CommUtil.notEmpty(paymentStatus)){
			throw new RuntimeException("serviceplan planNo can not be null");
		}
		String sql = "select spp.plan_no from "+
            " service_plan_pos spp, " +
            " pos_service_item_price psip, " +
            " customer_order_det cod, " +
            " customer_order_trans cot " +
        " where  spp.plan_no =?  and  cot.status = ? and  " +
       " cot.order_no=cod.order_no and cod.item_no=psip.item_no and psip.item_no=spp.pos_item_no ";
		SQLQuery sqlQuery = this.servicePlanDao.getCurrentSession().createSQLQuery(sql);
		sqlQuery.setParameter(0, planNo).setParameter(1, paymentStatus);
		Object obj = sqlQuery.uniqueResult();
			
		String sql2 = "SELECT m.customer_id from service_plan sp,customer_service_subscribe css,customer_service_acc"
				+ " csa, member m where  css.service_plan_no = ? and css.acc_no = csa.acc_no and csa.customer_id "
				+ "= m.customer_id ";
		SQLQuery sqlQuery2 = servicePlanDao.getCurrentSession().createSQLQuery(sql2);
		sqlQuery2.setParameter(0, planNo);
//		Object obj2 = sqlQuery2.uniqueResult();
		List<Object> obj2 =   sqlQuery2.list();
	
		String sql3 = "SELECT ce.enroll_id FROM customer_enrollment ce where ce.subscribe_plan_no = ? ";
		SQLQuery sqlQuery3 = servicePlanDao.getCurrentSession().createSQLQuery(sql3);
		sqlQuery3.setParameter(0, planNo);
//		Object obj3 = sqlQuery3.uniqueResult();
		List<Object> obj3 =   sqlQuery3.list();
				
					
		String sql4 = "select csa.acc_no from corporate_service_acc csa ,corporate_service_subscribe css where css.service_plan_no = ?"
				+ " and css.acc_no = csa.acc_no ";
		SQLQuery sqlQuery4 = servicePlanDao.getCurrentSession().createSQLQuery(sql4);
		sqlQuery4.setParameter(0, planNo);
//		Object obj4 = sqlQuery4.uniqueResult();
		List<Object> obj4 =   sqlQuery4.list();
		
					
		if(obj !=null&&obj2.size()>0&&obj3.size()>0&&obj4.size()>0 ) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public boolean checkServicePlanByName(String planName,String passNatureCode,Long  planNo) {
		List<ServicePlan> servicePlans = servicePlanDao.getByCols(ServicePlan.class, new String[]{"planName"}, new Serializable[]{planName}, null);
		if(servicePlans!=null && servicePlans.size()>1) {
			
			int count = 0;
			for(ServicePlan temp : servicePlans){
			      if((temp.getPassNatureCode().equals(passNatureCode))&&(temp.getPlanNo()!=planNo))
			    	  count++;
			}
			if (count >= 2)
			    return true;
		}
		return false;
	}

	@Override
	@Transactional
	public ResponseResult getDayPassPageList(
			DaypassDto dto) {
		StringBuilder hqlBuilder = new StringBuilder(" FROM ServicePlan s where s.passNatureCode =?  ");
		
		StringBuilder countHqlBuilder = new StringBuilder("select count(*) FROM ServicePlan s where s.passNatureCode = ?");
							
		ListPage<ServicePlan> servicePlans = new ListPage<ServicePlan>();
		if(dto.getOrder() != null) {
			if(dto.getOrder().equals("asc")){
				servicePlans.addAscending(dto.getPropertyName());
			} else {
				servicePlans.addDescending(dto.getPropertyName());
				
			}
		} else {
			servicePlans.addDescending("planNo");		
		}
		List<Serializable> param = new ArrayList<Serializable>();
		param.add("DP");
		if(dto.getDaysLater()>0){
			hqlBuilder.append(" and s.effectiveStartDate between  ? and ?");
        	countHqlBuilder.append(" and s.effectiveStartDate between  ? and ?");
			Date startdate = new Date();
			Date endDate = CommUtil.addDays(startdate, dto.getDaysLater());
			param.add(startdate);
			param.add(endDate);
		}
		
		if(!"All".equals(dto.getStatus()) && !StringUtils.isEmpty(dto.getStatus())){
			hqlBuilder.append(" and s.status = ?");
        	countHqlBuilder.append(" and s.status = ?");
			param.add(dto.getStatus());
		}
		
		if(dto.getMonthLater()>0){
			hqlBuilder.append(" and s.effectiveStartDate between  ? and ?");
        	countHqlBuilder.append(" and s.effectiveStartDate between  ? and ?");
			Date startdate = new Date();
			Date endDate = CommUtil.addMonths(startdate, dto.getMonthLater());
			param.add(startdate);
			param.add(endDate);
		}
        if(dto.getStartDate() != null && dto.getEndDate() != null){
        	hqlBuilder.append(" and s.effectiveStartDate between  ? and ?");
        	countHqlBuilder.append(" and s.effectiveStartDate between  ? and ?");
        	param.add(dto.getStartDate());
        	param.add(dto.getEndDate());
		}
	    servicePlans.setNumber(dto.getCurrentPage());
		servicePlans.setSize(dto.getPageSize());
		List<DaypassDto> dtos = new ArrayList<DaypassDto>();
		ResponseResult result = null;	
		try {
			servicePlanDao.listByHql(servicePlans, countHqlBuilder.toString(), hqlBuilder.toString(), param);
			for(ServicePlan temp : servicePlans.getList()){
				temp.setServicePlanAdditionRules(null);
				temp.setServicePlanFacilities(null);
				temp.setServicePlanPoses(null);
				DaypassDto daypassDto = new DaypassDto();
				daypassDto.setEffectiveEndDate(DateConvertUtil.getYMDDateAndDateDiff(temp.getEffectiveEndDate()));
				daypassDto.setEffectiveStartDate(DateConvertUtil.getYMDDateAndDateDiff(temp.getEffectiveStartDate()));
				daypassDto.setPlanName(temp.getPlanName());
				daypassDto.setPlanNo(temp.getPlanNo());
				daypassDto.setStatus(temp.getStatus());
				dtos.add(daypassDto);
			}
			servicePlans.setDtoList(dtos);
			result = new ResponseResult("0", "success", servicePlans);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}   				
		result.setData(servicePlans );
		result.setErrorMessageEN("");
		result.setReturnCode("0");
		return result;
		
	}

	@Override
	@Transactional
	public ResponseResult changeDayPassStatus(ServicePlanDto dto) {
		ResponseResult result = new ResponseResult();
		Long planNo = dto.getPlanNo();
		ServicePlan daypass = servicePlanDao.getServicePlanById(planNo);
		ServicePlanPos servicePlanPosLowRate = daypass.getServicePlanPoses().get(0);
		PosServiceItemPrice itemPriceLowRate = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, servicePlanPosLowRate.getPosItemNo());
		String hql = "FROM ServicePlanRatePos s where s.servPosId=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(servicePlanPosLowRate.getServPosId());
		List<ServicePlanRatePos> planRatePos = this.servicePlanRatePosDao.getByHql(hql, param);
		ServicePlanRatePos ratePos = planRatePos.get(0);
		PosServiceItemPrice posServiceItemPriceHigh = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, ratePos.getPosItemNo());
		String status = dto.getStatus();
		if(!Constant.Status.ACT.toString().equals(status)  && !Constant.Status.NACT.toString().equals(status)){
			throw new  RuntimeException("status only can be ACT or NACT");
		}
		itemPriceLowRate.setStatus(status);
		posServiceItemPriceHigh.setStatus(status);
		daypass.setUpdateBy(dto.getUpdateBy());
		daypass.setUpdateDate(new Date());
		daypass.setStatus(status);
		servicePlanDao.update(daypass);
		result = new ResponseResult("0", "cheng gong", "success");
		return result;
	}

	@Override
	public ResponseResult setDayPassGlobalQuota(String paramValue) {
		// TODO Auto-generated method stub
		
		ResponseResult result = null;	
		try {
			GlobalParameter globalParameter = new GlobalParameter();
			globalParameter.setParamValue(paramValue);
			globalParameterDao.save(globalParameter);
			return result = new ResponseResult("0", "cheng gong", "success");
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return result = new ResponseResult("-1", "", "fail");
		}	

	}
	@Transactional
	@Override
	public ServicePlanDto getDaypassById(Long planNo) {
		ServicePlan daypass = this.servicePlanDao.get(ServicePlan.class, planNo);
		ServicePlanDto dto = new ServicePlanDto();
		this.setSimpleFiled4Dto(dto, daypass);
		daypass.getServicePlanPoses().size();
		ServicePlanPos planPos = daypass.getServicePlanPoses().get(0);
		String ageRageCode = planPos.getAgeRange();
		setAgeRange4Dto(dto, ageRageCode);
		this.setFacility4Dto(dto, daypass);
		String hql = "FROM ServicePlanRatePos s where s.servPosId=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planPos.getServPosId());
		List<ServicePlanRatePos> planRatePos = this.servicePlanRatePosDao.getByHql(hql, param);
		//need to check data
		ServicePlanRatePos servicePlanRatePos = planRatePos.get(0);
		dto.setWeeklyRepeatBit(servicePlanRatePos.getWeeklyRepeatBit());
		List<String> specialDate = new ArrayList<String>();
		servicePlanRatePos.getPlanDatePosSet().size();
		
		for(Iterator it=servicePlanRatePos.getPlanDatePosSet().iterator(); it.hasNext(); ){
			ServicePlanDatePos datePos = (ServicePlanDatePos) it.next();
			specialDate.add(DateConvertUtil.date2String(datePos.getSpecialDate(), "yyyy/MM/dd"));
		}
		dto.setSpecialDate(specialDate);
		PosServiceItemPrice itemPriceLowRate = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, planPos.getPosItemNo());
		dto.setLowRate(itemPriceLowRate.getItemPrice());
		PosServiceItemPrice itemPriceHighRate =  this.posServiceItemPriceDao.get(PosServiceItemPrice.class, servicePlanRatePos.getPosItemNo());
		dto.setHighRate(itemPriceHighRate.getItemPrice());
		dto.setServicePlanPosDtos(null);
		dto.setServicePlanRights(null);
		return dto;
	}
	@Transactional
	@Override
	public ResponseMsg duplicateDaypass(Long planNo, String userId) {
		ServicePlanDto dto = this.getDaypassById(planNo);
		ServicePlan servicePlan = servicePlanDao.getByplanName(CommUtil.getPrefixName(dto.getPlanName()));
		dto.setPlanName(CommUtil.getDuplicateName(servicePlan.getPlanName()));
		dto.setCreateBy(userId);
		dto.setUpdateBy(userId);
		dto.setCreateDate(new Date());
		dto.setUpdateDate(new Date());
		return this.saveDayPass(dto);
	}
	@Transactional
	@Override
	public ResponseMsg deleteDaypass(Long planNo) {
		ServicePlan daypass = this.servicePlanDao.get(ServicePlan.class, planNo);
		ServicePlanPos servicePlanPosLowRate = daypass.getServicePlanPoses().get(0);
		String hql = "FROM ServicePlanRatePos s where s.servPosId=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(servicePlanPosLowRate.getServPosId());
		List<ServicePlanRatePos> planRatePos = this.servicePlanRatePosDao.getByHql(hql, param);
		ServicePlanRatePos ratePos = planRatePos.get(0);
		for(Iterator it=planRatePos.iterator(); it.hasNext(); ){
			this.servicePlanRatePosDao.delete((ServicePlanRatePos) it.next());
		}
		this.servicePlanDao.delete(daypass);
		this.posServiceItemPriceDao.deleteById(PosServiceItemPrice.class, ratePos.getPosItemNo());
		this.posServiceItemPriceDao.deleteById(PosServiceItemPrice.class, servicePlanPosLowRate.getPosItemNo());
		ResponseMsg responseMsg = new ResponseMsg("0", "success");
		return responseMsg;
	}
	@Transactional
	@Override
	public ResponseMsg updateDaypass(ServicePlanDto dto) {
		ServicePlan daypass = this.servicePlanDao.get(ServicePlan.class, dto.getPlanNo());
		daypass.setPlanName(dto.getPlanName());
		daypass.setEffectiveStartDate(DateConvertUtil.parseString2Date(dto.getEffectiveStartDate(),"yyyy-MM-dd"));
		daypass.setEffectiveEndDate(DateConvertUtil.parseString2Date(dto.getEffectiveEndDate(),"yyyy-MM-dd"));
		daypass.setSubscriberType(dto.getSubscriberType());
		daypass.setUpdateDate(new Date());
		daypass.setUpdateBy(dto.getUpdateBy());
		//update low service_plan_pos
		updateLowRate4Daypass(daypass, dto.getLowRate(), dto.getAgeRange());
		//update high service_plan_rate_pos
		updateHighRate4Daypass(daypass,dto.getHighRate(), dto);
		//update ServicePlanFacility
		updateFacilityTypes4Daypass(daypass, dto.getFacilityTypes());
		this.servicePlanDao.update(daypass);
		
		//this.servicePlanDao.save(dayPass);
//		ResponseMsg status = new ResponseMsg("0", "chenggong");
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	}

	private void updateFacilityTypes4Daypass(ServicePlan daypass, List<FacilityTypeDto> facilityTypes) {
		//--------For Facility Type storage----------BEGIN//
		//delete facility types which are related to serviceplan
		CollectionUtil.retainAll(daypass.getServicePlanFacilities(), facilityTypes, "facilityTypeCode", "typeCode", new PairCallBack<ServicePlanFacility,FacilityTypeDto>(){
			@Override
			public void execute(ServicePlanFacility t1,FacilityTypeDto t2, int index, boolean isRemoved) {
				if(!isRemoved){
					t1.setPermission(t2.getPermission());
					servicePlanFacilityDao.update(t1);
				}
				if(isRemoved){
					servicePlanFacilityDao.delete(t1);
				}
			}
		});
		CollectionUtil.removeAll(facilityTypes, daypass.getServicePlanFacilities(), "typeCode", "facilityTypeCode",null);
		addFacilityTypes4ServicePlan(daypass,facilityTypes);
	}

	private void updateHighRate4Daypass(ServicePlan daypass,BigDecimal highRate, ServicePlanDto dto) {
		ServicePlanPos planPos = daypass.getServicePlanPoses().get(0);
		String hql = "FROM ServicePlanRatePos s where s.servPosId=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planPos.getServPosId());
		List<ServicePlanRatePos> planRatePos = this.servicePlanRatePosDao.getByHql(hql, param);
		ServicePlanRatePos ratePos = planRatePos.get(0);
		PosServiceItemPrice itemPriceHighRate = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, ratePos.getPosItemNo());
		itemPriceHighRate.setItemPrice(highRate);
		//itemPriceHighRate.setDescription(daypass.getPlanName()+" "+dto.getAgeRange().getAgeRangeCode()+ " "+dto.getHighRate()+" "+Constant.RateType.HI.toString());
		itemPriceHighRate.setDescription("Day Pass - "+daypass.getPlanName()+" (High Rate)");
		ratePos.setWeeklyRepeatBit(dto.getWeeklyRepeatBit());
		ratePos.setDescription(itemPriceHighRate.getDescription());
		updateSpecialDate(ratePos,dto.getSpecialDate());
	}

	private void updateSpecialDate(ServicePlanRatePos ratePos,  List<String> specialDate) {
		Set<ServicePlanDatePos> unselectDatePos = new HashSet<ServicePlanDatePos>();
		Set<String> existdates = new HashSet<String>();
		if(specialDate==null || specialDate.size()==0){
			for (ServicePlanDatePos planDatePos : ratePos.getPlanDatePosSet()) {
				this.servicePlanDatePosDao.delete(planDatePos);
			}
			ratePos.getPlanDatePosSet().clear();
		} else {
			if (ratePos.getPlanDatePosSet().size()>0) {
				for (ServicePlanDatePos planDatePos : ratePos.getPlanDatePosSet()) {
					String temp = DateConvertUtil.date2String(planDatePos.getSpecialDate(), "yyyy-MM-dd");
					if (specialDate.contains(temp)) {
						existdates.add(temp);
					} else {
						unselectDatePos.add(planDatePos);
						this.servicePlanDatePosDao.delete(planDatePos);
					}
				}
			}
			ratePos.getPlanDatePosSet().removeAll(unselectDatePos);
			specialDate.removeAll(existdates);
			if (specialDate.size() > 0) {
				for(String d : specialDate) {
					if(CommUtil.notEmpty(d )) {
						ServicePlanDatePos temp = new ServicePlanDatePos();
						temp.setAnnualRepeat(Constant.AnnualRepeat.N.toString());
						temp.setSpecialDate(DateConvertUtil.parseString2Date(d, "yyyy-MM-dd"));
						temp.setPlanRatePos(ratePos);
						this.servicePlanDatePosDao.save(temp);
						ratePos.getPlanDatePosSet().add(temp);
					}
				}
			}
		}
	}

	private void updateLowRate4Daypass(ServicePlan daypass, BigDecimal lowRate,
			AgeRange ageRange) {
		ServicePlanPos servicePlanPosLowRate = daypass.getServicePlanPoses().get(0);
		servicePlanPosLowRate.setAgeRange(ageRange.getAgeRangeCode());
		//servicePlanPosLowRate.setDescription(daypass.getPlanName()+" "+ageRange.getAgeRangeCode()+ " "+lowRate);
		servicePlanPosLowRate.setDescription("Day Pass - "+daypass.getPlanName()+" (Low Rate)");
		PosServiceItemPrice itemPriceLowRate = this.posServiceItemPriceDao.get(PosServiceItemPrice.class, servicePlanPosLowRate.getPosItemNo());
		itemPriceLowRate.setDescription(servicePlanPosLowRate.getDescription());
		itemPriceLowRate.setItemPrice(lowRate);
	}

	@Override
	@Transactional
	public ResponseResult getPurchasedDaypassList( PurchasedDaypassDto purchasedDaypassDto) {
		// TODO Auto-generated method stub
		ListPage<PurchasedDaypassDto> purchasedDaypassDtos = new ListPage<PurchasedDaypassDto>();
		List<Serializable> param = new ArrayList<Serializable>();
		StringBuilder sqlBuilder = new StringBuilder(
				"select t.order_no orderNo,(select count(1) from customer_order_permit_card copc where copc.customer_order_no=t.order_no ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append(" and copc.service_plan_no=").append(purchasedDaypassDto.getPlanNo());
		}
		sqlBuilder.append("  ) orderTotalAmount,  ");
		sqlBuilder
				.append("(case when t.staff_user_id is null then (select   c.contact_email from customer_profile c where c.customer_id=t.customer_id) else null end) contactEmail , ")
				.append(" case when t.staff_user_id is null then ")
				.append(" (select  concat(c.salutation,' ',c.given_name,' ',c.surname)  from customer_profile c where c.customer_id=t.customer_id )  ")
				.append(" else ")
				.append(" (select   concat(c.given_name,' ',c.surname)  from staff_profile c where c.user_id=t.staff_user_id) ")
				.append(" end memberName,   case  when t.staff_user_id is null then 'Y'  else 'N'  end  as memberBuy, t.create_date orderDate ,  ")
				.append(" (select copc.effective_from from customer_order_permit_card copc where ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append("  copc.service_plan_no=").append(purchasedDaypassDto.getPlanNo()).append(" and  ");
		}
		sqlBuilder.append("  copc.customer_order_no=t.order_no    ");
		sqlBuilder
				.append(" limit 1 ) effectiveStartDate, ")
				.append(" (select copc.effective_to from customer_order_permit_card copc where ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append("   copc.service_plan_no=").append(purchasedDaypassDto.getPlanNo()).append(" and  ");
		}
		sqlBuilder.append("  copc.customer_order_no=t.order_no  limit 1) effectiveEndDate, ")
				.append(" t.order_status orderStatus , t.customer_id customerId")
				.append(" from customer_order_hd  t ")
				.append(" where exists(select 1 from customer_order_permit_card tt where tt.customer_order_no=t.order_no ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append(" and   tt.service_plan_no=").append(purchasedDaypassDto.getPlanNo()).append("   ");
		}
		String show = purchasedDaypassDto.getShow();
		if (show == null || "All".equals(show)) {
			sqlBuilder.append(" ) ");
		}else if (show.equals("Today")) {
			sqlBuilder.append("and tt.effective_from = CURRENT_DATE  )");
		}else if (show.equals("Within 90 days")) {
			sqlBuilder.append("and  tt.effective_from <= CURRENT_DATE +90 and tt.effective_from>= CURRENT_DATE  )");
		}else if (show.equals("Within 30 days")) {
			sqlBuilder.append("and  tt.effective_from <= CURRENT_DATE +30  and tt.effective_from>= CURRENT_DATE  )");
		}else if ("effective".equals(show)) {
			sqlBuilder.append("and  tt.effective_to >= CURRENT_DATE   )");
		}else if ("history".equals(show)){
			sqlBuilder.append("and  tt.effective_to < CURRENT_DATE   )");
		}
		if (purchasedDaypassDto.getCustomerId() != null) {
			sqlBuilder.append("  and  t.customer_id = ? ");
			param.add(purchasedDaypassDto.getCustomerId());
		}

		StringBuilder countSqlBuilder = new StringBuilder("select count(*) from( ").append(sqlBuilder).append(" ) m");

		if(purchasedDaypassDto.getPropertyName()==null){ 
			purchasedDaypassDtos.addDescending("orderNo");
		}
		if (purchasedDaypassDto.getOrder() != null) {
			if (purchasedDaypassDto.getOrder().equals("asc")) {
				purchasedDaypassDtos.addAscending(purchasedDaypassDto.getPropertyName());
			} else {
				purchasedDaypassDtos.addDescending(purchasedDaypassDto.getPropertyName());
			}
		}else{
			purchasedDaypassDtos.addDescending("orderNo");
		}
		purchasedDaypassDtos.setNumber(purchasedDaypassDto.getPageNumber());
		purchasedDaypassDtos.setSize(new Long(purchasedDaypassDto.getPageSize()).intValue());
		ResponseResult result = null;
		try {
			PurchasedDaypassDtoDao.listBySqlDto(purchasedDaypassDtos, countSqlBuilder.toString(),
					sqlBuilder.toString(), param, purchasedDaypassDto);
			List<Object> list = purchasedDaypassDtos.getDtoList();

			List<PurchasedDaypassBackDto> dtos = new ArrayList<PurchasedDaypassBackDto>();

			for (Object temp : list) {
				((PurchasedDaypassDto) temp).setPageSize(purchasedDaypassDto.getPageSize());
				((PurchasedDaypassDto) temp).setPageNumber(purchasedDaypassDto.getPageNumber());
				java.math.BigInteger orderNo = ((PurchasedDaypassDto) temp).getOrderNo();
				PurchasedDaypassBackDto purchasedDaypassBackDto = new PurchasedDaypassBackDto();
				purchasedDaypassBackDto.setPageSize(((PurchasedDaypassDto) temp).getPageSize());
				purchasedDaypassBackDto.setPageNumber(((PurchasedDaypassDto) temp).getPageNumber());
				purchasedDaypassBackDto.setContactEmail(((PurchasedDaypassDto) temp).getContactEmail());
				purchasedDaypassBackDto.setOrderNo(((PurchasedDaypassDto) temp).getOrderNo());
				purchasedDaypassBackDto.setOrderTotalAmount(((PurchasedDaypassDto) temp).getOrderTotalAmount());
				purchasedDaypassBackDto.setOrderDate(DateConvertUtil.getYMDDateAndDateDiff(((PurchasedDaypassDto) temp)
						.getOrderDate()));
				purchasedDaypassBackDto.setEffectiveEndDate(DateConvertUtil.getYMDDate(((PurchasedDaypassDto) temp)
						.getEffectiveEndDate()));
				purchasedDaypassBackDto.setEffectiveStartDate(DateConvertUtil.getYMDDate(((PurchasedDaypassDto) temp)
						.getEffectiveStartDate()));
				purchasedDaypassBackDto.setMemberName(((PurchasedDaypassDto) temp).getMemberName());
				purchasedDaypassBackDto.setOrderStatus(((PurchasedDaypassDto) temp).getOrderStatus());
				purchasedDaypassBackDto.setCustomerId(((PurchasedDaypassDto) temp).getCustomerId());
				purchasedDaypassBackDto.setMemberBuy(((PurchasedDaypassDto) temp).getMemberBuy());
				dtos.add(purchasedDaypassBackDto);
			}

			purchasedDaypassDtos.setDtoList(dtos);
			result = new ResponseResult("0", "success", purchasedDaypassDtos);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		result.setErrorMessageEN("");
		result.setReturnCode("0");
		return result;
	}
	@Override
	@Transactional
	public String getPurchasedDaypassListSQL(
			PurchasedDaypassDto purchasedDaypassDto) {
		StringBuilder sqlBuilder = new StringBuilder(
				"select  distinct  * from (select t.order_no orderNo,(select count(1) from customer_order_permit_card copc where copc.customer_order_no=t.order_no ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append(" and copc.service_plan_no=").append(purchasedDaypassDto.getPlanNo());
		}
		sqlBuilder.append("  ) orderTotalAmount,  ");
		sqlBuilder
				.append("(case when t.staff_user_id is null then (select   c.contact_email from customer_profile c where c.customer_id=t.customer_id) else null end) contactEmail , ")
				.append(" case when t.staff_user_id is null then ")
				.append(" (select  concat(c.salutation,' ',c.given_name,' ',c.surname)  from customer_profile c where c.customer_id=t.customer_id )  ")
				.append(" else ")
				.append(" (select   concat(c.given_name,' ',c.surname)  from staff_profile c where c.user_id=t.staff_user_id) ")
				.append(" end memberName,   case  when t.staff_user_id is null then 'Y'  else 'N'  end  as memberBuy, t.create_date orderDate ,  ")
				.append(" (select copc.effective_from from customer_order_permit_card copc where ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append("  copc.service_plan_no=").append(purchasedDaypassDto.getPlanNo()).append(" and  ");
		}
		sqlBuilder.append("  copc.customer_order_no=t.order_no    ");
		sqlBuilder
				.append(" limit 1 ) effectiveStartDate, ")
				.append(" (select copc.effective_to from customer_order_permit_card copc where ");
		if(null!= purchasedDaypassDto.getPlanNo()){
			sqlBuilder.append("   copc.service_plan_no=").append(purchasedDaypassDto.getPlanNo()).append(" and  ");
		}
		sqlBuilder.append("  copc.customer_order_no=t.order_no  limit 1) effectiveEndDate, ")
				.append(" t.order_status orderStatus , t.customer_id customerId")
				.append(" from customer_order_hd  t , customer_order_permit_card card where t.order_no=card.customer_order_no and card.service_plan_no is not NUll ) purchase ");
		return sqlBuilder.toString();
	}
	/**used for pc purchase daypass
	 * list all the daypass which their effectiveStartDate<=current_date<=effectiveEndDate,member can buy all these daypass,
	 */
	@Override
	@Transactional
	public ResponseResult getServiceplanByDate(DaypassPurchaseDto dto) throws ParseException {
		List<ServicePlan > servicePlan =servicePlanDao.getServiceplanByDate(dto);
		List<ServicePlanDto> returnSp = addDescription4MemberCanBuyList(servicePlan); 
		responseResult.initResult(GTAError.Success.SUCCESS, returnSp);
		responseResult.setData(returnSp);
		return responseResult;
	}
	/**used for webprotal purchase daypass
	 * list all the daypass which their effectiveStartDate<=current_date<=effectiveEndDate,member can buy all these daypass,
	 */
	@Override
	@Transactional
	public List<ServicePlanDto> getValidDaypass() {
		List<ServicePlan > servicePlan =servicePlanDao.getValidDaypass();
		List<ServicePlanDto> returnSp = addDescription4MemberCanBuyList(servicePlan); 
		return returnSp;
	}

	private List<ServicePlanDto> addDescription4MemberCanBuyList(
			List<ServicePlan> servicePlan) {
		List<ServicePlanDto>  returnSp=new ArrayList<ServicePlanDto>();
		List<SysCode> ageRange=null;
		try {
			 ageRange = sysCodeDao.selectSysCodeByCategory("ageRange");
		} catch (Exception e) {
			logger.error("getServiceplanByDate get syscode failed!",e);
		}
		for(ServicePlan sp:servicePlan){
			ServicePlanDto planJson = new ServicePlanDto();
			setSimpleFiled4Dto(planJson, sp);
			List<ServicePlanPos> servicePlanPoses = sp.getServicePlanPoses();
			if(servicePlanPoses!=null&&!servicePlanPoses.isEmpty()){
				ServicePlanPos planPos = servicePlanPoses.get(0);
				setAgeRangeWithDescription4Dto(planJson, planPos.getAgeRange(),ageRange);
			}
			returnSp.add(planJson);
		}
		return returnSp;
	}
	@Override
	@Transactional	
	public ResponseResult verifyPaymentItemNo(Long paymentItemNo) {
		Object obj = customerOrderPermitCardDao.get(CustomerOrderPermitCard.class, paymentItemNo);
		if(obj ==null ) {			
			return new ResponseResult("1", "No such payment item # found!");
		}
		Long customerId = ((CustomerOrderPermitCard) obj).getCardholderCustomerId(); 
		if(customerId==null){
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS,customerId);
		}
		return responseResult;		
	}
	
	@Override
	@Transactional
	public ResponseResult cancleUnpayedOrder(Long orderNo) {
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if(Constant.Status.CAN.toString().equals(customerOrderHd.getOrderStatus())){
			throw new GTACommonException(GTAError.DayPassPurchaseError.Already_Canceled);
		}
		if(Constant.Status.CMP.toString().equals(customerOrderHd.getOrderStatus())){
			throw new GTACommonException(GTAError.DayPassPurchaseError.Use_other_Way_Cancel);
		}
		customerOrderHd.setOrderStatus(Constant.Status.CAN.toString());
		customerOrderHdDao.update(customerOrderHd);
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return this.responseResult;
	}
	@Override
	@Transactional	
	public ResponseResult canclePurchaseDaypass(Long orderNo, String userId,String remark,String requesterType,boolean refundFlag) {
		
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if(Constant.Status.CAN.toString().equals(customerOrderHd.getOrderStatus())){
			throw new GTACommonException(GTAError.DayPassPurchaseError.Already_Canceled);
		}
		List<CustomerOrderPermitCard> list = customerOrderHd.getCustomerOrderPermitCards();
		this.permitCardMasterService.disposeCardforDayPassByOrderNo(orderNo+"", userId);
		if(!Constant.Status.CMP.toString().equals(customerOrderHd.getOrderStatus())){
			customerOrderHd.setOrderStatus(Constant.Status.CAN.toString());
			customerOrderHdDao.update(customerOrderHd);
			this.responseResult.initResult(GTAError.Success.SUCCESS,new String[]{"Order doesn't pay, so cancel will not refund"});
			return this.responseResult;
		}
		if(null!=customerOrderHd.getStaffUserId()){
			customerOrderHd.setOrderStatus(Constant.Status.CAN.toString());
			customerOrderHdDao.update(customerOrderHd);
			this.responseResult.initResult(GTAError.Success.SUCCESS,new String[]{"Staff purchased day pass will not refund"});
			return this.responseResult;
		}
		if(list !=null && list.size()>0 ){
			customerOrderHd.setOrderStatus(Constant.Status.CAN.toString());
			customerOrderHdDao.update(customerOrderHd);
//			for(CustomerOrderPermitCard card: list){
//				card.setStatus(status);
//			}
			if("HKGTA".equals(requesterType) || refundFlag){//send CustomerRefundRequest
				boolean flag = false;
				Timestamp begin = new Timestamp(list.get(0).getEffectiveFrom().getTime());
				boolean  refundPeriodFlag = memberFacilityTypeBookingService.checkRefundDate(begin,RefundPeriodType.REFPERIOD_DAYPASS.name(), "daypass");
				if(!refundPeriodFlag && "CUSTOMER".equals(requesterType)){
					flag = memberFacilityTypeBookingService.sendCustomerRefundRequest(customerOrderHd,"SRV",remark,requesterType,userId,Constant.CustomerRefundRequest.NRF.getName());
				}else {
					flag = memberFacilityTypeBookingService.sendCustomerRefundRequest(customerOrderHd,"SRV",remark,requesterType,userId,Constant.CustomerRefundRequest.PND.getName());
				}
				if(flag){
					this.responseResult.initResult(GTAError.Success.SUCCESS);
				}else {
					this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"daypass purchase cancel fail： "+orderNo});
				}
			}else{// do not send CustomerRefundRequest
				this.responseResult.initResult(GTAError.Success.SUCCESS);
			}
		}
		return this.responseResult;				
	}		

	/**   
	* @author: Zero_Wang
	* @since: Sep 15, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	private void disposeCard(List<CustomerOrderPermitCard> cards, String userId) {
		if(null==cards || cards.size()<=0){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderPermitCard  not right"});
		}
		for(CustomerOrderPermitCard c : cards){
			PermitCardMaster master = this.permitCardMasterDao.get(PermitCardMaster.class, c.getLinkedCardNo()+"");
			PermitCardLog cardLog = new PermitCardLog();
			cardLog.setCardNo(new Long(master.getCardNo()));
			cardLog.setCustomerId(master.getMappingCustomerId()+"");
			cardLog.setCardStatusFrom(master.getStatus());
			cardLog.setCardStatusTo(Constant.Status.DPS.toString());
			cardLog.setStatusUpdateDate(new Timestamp(System.currentTimeMillis()));
			cardLog.setUpdateBy(userId);
			this.permitCardLogDao.save(cardLog);
			master.setStatus(Constant.Status.DPS.toString());
			master.setUpdateBy(userId);
			master.setStatusUpdateDate(new Timestamp(System.currentTimeMillis()));
			this.permitCardMasterDao.update(master);
			
		}
	}
	
	@Override
	@Transactional
	public List<ServicePlanDto> getValidServicPlan() {
		List<ServicePlan> servicePlans = servicePlanDao.getValidServicPlan();
		List<ServicePlanDto> retList = new ArrayList<ServicePlanDto>();
		for(ServicePlan plan :servicePlans ){
			ServicePlanDto planJson = new ServicePlanDto();
			setSimpleFiled4Dto(planJson, plan);
			retList.add(planJson);
		}
		return retList;
	}
	
	@Transactional
	public List<ServicePlan> getValidServicPlanForCorporateMember() {
		return servicePlanDao.getValidCorporateServicPlan();
	}
	
	@Override
	@Transactional
	public ServicePlan get(Long  planNo) {
	     ServicePlan servicePlanTemp = servicePlanDao.get(ServicePlan.class,planNo );
		return servicePlanTemp;	     
	}
	
	@Override
	@Transactional
	public void deactivateServicePlans() {
		List<ServicePlan> servicePlans = servicePlanDao.getExpiredServicePlans(Constant.Status.ACT.toString());
		for(ServicePlan plan : servicePlans){
			changeServicePlanStatus(plan.getPlanNo(),Constant.Status.EXP.toString(),"System");
		}
	}

	@Override
	@Transactional
	public boolean checkServicePlanForSaveByName(String planName, String passNatureCode) {
		// TODO Auto-generated method stub
		List<ServicePlan> servicePlans = servicePlanDao.getByCols(ServicePlan.class, new String[]{"planName"}, new Serializable[]{planName}, null);
		if(servicePlans!=null && servicePlans.size()>1) {			
			int count = 0;
			for(ServicePlan temp : servicePlans){
			      if(temp.getPassNatureCode().equals(passNatureCode))
			    	  count++;
			}
			if (count >= 2)
			    return true;
		}
		return false;
	}

	@Override
	@Transactional
	public boolean isDaypassUsed(Long planNo) {
		StringBuilder sql = new StringBuilder(" select count(1) from customer_order_permit_card c where c.service_plan_no=? "); 
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		BigInteger size = (BigInteger) this.servicePlanDao.getUniqueBySQL(sql.toString(), param);
		if(size.intValue()>0){
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public void autoDisposeDaypassCard() {
		List<CustomerOrderPermitCard> cards = this.customerOrderPermitCardDao.getExpireCustomerOrderPermitCard(new Date());
		disposeCard(cards,"system");
	}

	@Override
	@Transactional
	public PurchasedDaypassBackDto addTranscationFlag(
			PurchasedDaypassBackDto purchasedDaypassBackDto) {
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, purchasedDaypassBackDto.getOrderNo().longValue());
		List<CustomerOrderTrans> trans = orderHd.getCustomerOrderTrans();
		if(trans!=null && trans.size()>0){
			for(CustomerOrderTrans c : trans){
				if(Constant.Status.SUC.toString().equalsIgnoreCase(c.getStatus())){
					purchasedDaypassBackDto.setPayed("Y");
					break;
				}
			}
		}
		return purchasedDaypassBackDto;
	}
	@Override
	@Transactional
	public List<FacilityTransactionInfo> getTransactionInfoByOrderNo(
			Long orderNo) {
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		List<CustomerOrderTrans> trans = orderHd.getCustomerOrderTrans();
		List<FacilityTransactionInfo> list = new ArrayList<FacilityTransactionInfo>();
		DateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		if(trans!=null && trans.size()>0){
			for(CustomerOrderTrans c : trans){
				if(Constant.Status.SUC.toString().equalsIgnoreCase(c.getStatus())){
					FacilityTransactionInfo info = new FacilityTransactionInfo();
					info.setExpense(c.getPaidAmount().toString());
					info.setLocation(c.getPaymentLocationCode());
					info.setMedia(c.getPaymentMedia());
					info.setPaymentDate(sdf.format(c.getTransactionTimestamp()));
					info.setPaymentMethod(c.getPaymentMethodCode());
					info.setResvId(orderNo);
					info.setTransactionNo(c.getTransactionNo());
					list.add(info);
				}
			}
		}
		return list;
	}

	@Override
	public String getServicePlans(Map<String, org.hibernate.type.Type> typeMap)
	{
		typeMap.put("planNo", LongType.INSTANCE);
		typeMap.put("contractLengthMonth", LongType.INSTANCE);
		typeMap.put("createBy", StringType.INSTANCE);
		typeMap.put("createDate", DateType.INSTANCE);
		typeMap.put("updateBy", StringType.INSTANCE);
		typeMap.put("updateDate", DateType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("planName", StringType.INSTANCE);
		typeMap.put("passPeriodType", StringType.INSTANCE);
		typeMap.put("subscriberType", StringType.INSTANCE);
		typeMap.put("passNatureCode", StringType.INSTANCE);
		typeMap.put("effectiveEndDate", StringType.INSTANCE);
		typeMap.put("effectiveStartDate", StringType.INSTANCE);
		
		StringBuffer sql = new StringBuffer();
		sql.append(" ")
		.append("SELECT * FROM ( ")
				.append("SELECT plan_no planNo ")
					.append(",contract_length_month contractLengthMonth ")
					.append(",create_by createBy ")
					.append(",create_date createDate ")
					.append(",effective_end_date effectiveEndDate ")
					.append(",effective_start_date effectiveStartDate ")
					.append(",pass_nature_code passNatureCode ")
					.append(",pass_period_type passPeriodType ")
					.append(",subscriber_type  subscriberType ")
					.append(",plan_name planName ")
					.append(",status  ")
					.append(",update_by updateBy ")
					.append(",update_date updateDate ")
				.append("FROM service_plan ")
				.append(") sub ")
				.append("WHERE 1 = 1 and sub.passNatureCode='LT' ");
		return sql.toString();
	}


	@Override
	@Transactional
	public Data getDailyDayPassUsage(String date, String orderColumn,
			String order, int pageSize, int currentPage, AdvanceQueryDto filters) {
		try{
			Data data = new Data();
			String filterSql = null;
			if(filters != null){
				filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
			}
			ListPage listPage = servicePlanDao.getDailyDayPassUsageList(date, orderColumn,  order,  pageSize,  currentPage,
					filterSql);
			
			data.setCurrentPage(currentPage);
			data.setPageSize(pageSize);
			data.setTotalPage(listPage.getAllPage());
			data.setLastPage(currentPage==listPage.getAllPage());
			data.setRecordCount(listPage.getAllSize());
			
			data.setList(listPage.getDtoList());
			
			return data;
		}catch (Exception e){
			logger.error("getDailyDayPassUsage() error", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}
	
	@Override
	@Transactional
	public byte[] getDailyDayPassUsageAttachment(String date, String fileType, String sortBy, String isAscending){
		try{
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String parentjasperPath = reportPath + "DailyDayPassUsage.jasper";
			File reFile = new File(parentjasperPath);
			Map<String, Object> parameters = new HashMap<String, Object>();
			DataSource ds = SessionFactoryUtils.getDataSource(servicePlanDao.getsessionFactory());
			Connection dbconn = DataSourceUtils.getConnection(ds);
			parameters.put("REPORT_CONNECTION", dbconn);
			parameters.put("orderDate", date);
			parameters.put("fileType", fileType);
			
			//Sorting by desired field
			JRDesignSortField sortField = new JRDesignSortField();
			List<JRSortField> sortList = new ArrayList<JRSortField>();
			sortField.setName(sortBy);
			if("true".equalsIgnoreCase(isAscending)){
				sortField.setOrder(SortOrderEnum.ASCENDING);
			}else{
				sortField.setOrder(SortOrderEnum.DESCENDING);
			}
			sortField.setType(SortFieldTypeEnum.FIELD);
			sortList.add(sortField);
			parameters.put(JRParameter.SORT_FIELDS, sortList);
			//Ignore pagination for csv
			if("csv".equals(fileType)){
				parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			}
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

			ByteArrayOutputStream outPut = new ByteArrayOutputStream();
			JRAbstractExporter exporter = null; 
			switch (fileType) {
			case "pdf":
				exporter = new JRPdfExporter();
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
				break;
			case "csv":
				exporter = new JRCsvExporter();
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
				break;
			default:
			}
			
			if (exporter == null) {
				return null;
			} else {
				exporter.exportReport();
				return outPut.toByteArray();
			}
		}catch(Exception e){
			logger.error("getDailyDayPassUsageAttachment() error", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}
}


