package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.AbstractCommonDataQueryDao;
import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.pms.RoomTypeDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberTransactionDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
@Scope("prototype")
public class MemberTransactionServiceImpl extends ServiceBase<MemberCashvalue> implements MemberTransactionService{

	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private CashvalueTopupHistoryDao topupHistoryDao;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;

	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private RoomTypeDao roomTypeDao;
	
	@Autowired
	@Qualifier("commonBySQLDao")
	AbstractCommonDataQueryDao abstractCommonDataQueryDao;

	@Transactional
	public ResponseResult getCurrentBalance(Long customerId){
		Member member = memberDao.getMemberById(customerId);
		if(member==null){
			//return new ResponseResult("1", "Cannot retrieve the member informartion!");
			responseResult.initResult(GTAError.MemberShipError.CANNOT_RETRIEVE_MEMBER);
			return responseResult;
		}
		String memberType = member.getMemberType();
		if(memberType.equals("IDM")||memberType.equals("CDM")){
			customerId = member.getSuperiorMemberId();//superior customer id
		}
		MemberCashvalue memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		
		if(memberCashvalue==null){
			//return new ResponseResult("0", "Successfully Retrieved!", 0);
			responseResult.initResult(GTAError.Success.SUCCESS, "Successfully Retrieved!");
			responseResult.setData(0);
			return responseResult;
		}
        
		//return new ResponseResult("0", "Successfully Retrieved!", memberCashvalue.getAvailableBalance());
		responseResult.initResult(GTAError.Success.SUCCESS, "Successfully Retrieved!");
		responseResult.setData(memberCashvalue.getAvailableBalance());
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getTransactionList(Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending,String clientType, AdvanceQueryDto filters){
			try {
				ListPage<CustomerOrderTrans> page = new ListPage<CustomerOrderTrans>();
				if(CommUtil.notEmpty(pageNumber)){
					page.setNumber(Integer.parseInt(pageNumber));
				}
				if(CommUtil.notEmpty(pageSize)){
					page.setSize(Integer.parseInt(pageSize));
				}
				
				String filterSql = null;
				if(filters != null){
					filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
				}
				ListPage<CustomerOrderTrans> list = customerOrderTransDao.getTransactionList(page,customerID,pageNumber,pageSize,sortBy,isAscending,clientType,filterSql);
				List<Object> memberTransactions = list.getDtoList();
				
				for(Object object:memberTransactions){
					MemberTransactionDto temp = (MemberTransactionDto)object;
					for(Constant.ServiceItem item : Constant.ServiceItem.values()){
						if(item.name().equals(temp.getItemCatagory())){
							temp.setItemCatagory(Constant.ServiceItem.valueOf(temp.getItemCatagory()).toString());
						}
					}
				}
				Data data = new Data();
				data.setList(memberTransactions);
				data.setTotalPage(page.getAllPage());;
				data.setCurrentPage(page.getNumber());
				data.setPageSize(page.getSize());
				data.setRecordCount(page.getAllSize());
				data.setLastPage(page.isLast());
				responseResult.initResult(GTAError.Success.SUCCESS,data);
				return responseResult;
			} catch (NumberFormatException e) {
				e.printStackTrace();
				responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
				return responseResult;
			}
		
	}
	
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> transactionListAdvanceSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "date_format(c.transactionTimestamp,'%Y-%m-%d')", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "c.transactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Payment Method", "c.paymentMethodCode", "java.lang.String", sysCodeDao.selectSysCodeByCategory("PAYMENTMETHOD"), 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Description", "ps.description", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Member", "CONCAT(p.salutation, ' ',p.givenName, ' ',p.surname )", "java.lang.String", "", 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Expenses", "c.paidAmount", "java.math.BigDecimal", "", 6);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5
				,condition6);
	}

	@Override
	@Transactional
	public ResponseResult getTopupHistory(Long customerID, String pageNumber,
			String pageSize, String sortBy, String isAscending, AdvanceQueryDto filters) {
		try {
			//ResponseResult responseResult = new ResponseResult("0","","",new Data());
			responseResult.initResult(GTAError.Success.SUCCESS, new Data());
			ListPage<CustomerOrderTrans> page = new ListPage<CustomerOrderTrans>();
			if(CommUtil.notEmpty(pageNumber)){
				page.setNumber(Integer.parseInt(pageNumber));
			}
			if(CommUtil.notEmpty(pageSize)){
				page.setSize(Integer.parseInt(pageSize));
			}
			
			String filterSql = null;
			if(filters != null){
				filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
			}
			ListPage<CustomerOrderTrans> list = customerOrderTransDao.getTopupHistory(page,customerID,pageNumber,pageSize,sortBy,isAscending,filterSql);
			List<Object> memberTransactions = list.getDtoList();
			
			Data data = new Data();
			data.setList(memberTransactions);
			data.setTotalPage(page.getAllPage());;
			data.setCurrentPage(page.getNumber());
			data.setPageSize(page.getSize());
			data.setRecordCount(page.getAllSize());
			data.setLastPage(page.isLast());
			responseResult.initResult(GTAError.Success.SUCCESS, data);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			//return new ResponseResult("1","Retrieve failed!");
			responseResult.initResult(GTAError.MemberShipError.RETRIEVE_FAILED);
			return responseResult;
		}
	}
	
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> topupHistoryAdvanceSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "date_format(c.transactionTimestamp,'%Y-%b-%d')", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "c.transactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Payment Method", "c.paymentMethodCode", "java.lang.String", sysCodeDao.selectSysCodeByCategory("PAYMENTMETHOD"), 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Member", "CONCAT(p.salutation, ' ',p.givenName, ' ',p.surname )", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Top Up", "c.paidAmount", "java.math.BigDecimal", "", 5);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5);
	}
	
	@Transactional
	public ResponseResult topupByCashCard(TopUpDto topUpDto) {
		Date currentDate = new Date();
		String staffUserId = topUpDto.getStaffUserId();
		Long customerId = topUpDto.getCustomerId();
		String terminalId = null;
		String agentTransactionNo = null;
		BigDecimal topUpAmount = null;
		if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			terminalId = topUpDto.getTerminalId();
			agentTransactionNo = topUpDto.getAgentTransactionNo();
			topUpAmount = topUpDto.getTopUpAmount();
		} else if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			topUpAmount = topUpDto.getTopUpAmount();
		} else {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_PAYMENT_METHOD);
			return responseResult;
		}
		int validate = topUpAmount.compareTo(BigDecimal.ZERO);
		if (validate!=1) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_TOPUP_AMOUNT);
			return responseResult;
		}
		Member member = memberDao.getMemberById(topUpDto.getCustomerId());
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
		}else if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		}
		customerOrderHd.setStaffUserId(staffUserId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(topUpAmount);
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		Long orderNo = (Long) customerOrderHdDao
				.addCustomreOrderHd(customerOrderHd);
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		// CUSTOMER ORDER Detail
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.TOPUP_ITEM_NO);
		if(posServiceItemPrice == null ){
			PosServiceItemPrice psip = new PosServiceItemPrice();
			psip.setStatus(Constant.General_Status_ACT);
			psip.setItemNo(Constant.TOPUP_ITEM_NO);
			psip.setItemCatagory("TOPUP");
			psip.setDescription("Top up for cash value");
			posServiceItemPriceDao.save(psip);
		}
		customerOrderDet.setCustomerOrderHd(temp);
		customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
		customerOrderDet.setItemRemark("");
		customerOrderDet.setOrderQty(1L);
		customerOrderDet.setItemTotalAmout(topUpAmount);
		customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderDet.setCreateBy(staffUserId);
		customerOrderDet.setUpdateDate(currentDate);
		customerOrderDet.setUpdateBy(staffUserId);
		customerOrderDetDao.saveOrderDet(customerOrderDet);

		// CUSTOMER ORDER TRANS
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(topUpAmount);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
		}else if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setStatus(Constant.Status.PND.name());
			customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
			customerOrderTrans.setTerminalId(terminalId);
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
		}
		if (CommUtil.notEmpty(staffUserId)) {
			customerOrderTrans.setPaymentRecvBy(staffUserId);
			customerOrderTrans.setCreateBy(staffUserId);
		}else {
			customerOrderTrans.setCreateBy(member.getUserId());
		}
		customerOrderTrans.setPaymentLocationCode(topUpDto.getTopUpLocation());
		customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			
			Long superiorMemberId = member.getSuperiorMemberId();
			MemberCashvalue memberCashvalue = null;
			if(superiorMemberId!=null){
				//if current member is a dependent member,should update superior member's cash value.
				memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
			}else {
				memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
			}
			if (memberCashvalue == null) {
				memberCashvalue = new MemberCashvalue();
				memberCashvalue.setInitialDate(currentDate);
				memberCashvalue.setInitialValue(topUpAmount);
				memberCashvalue.setExchgFactor(new BigDecimal(1));
				memberCashvalue.setInternalRemark("");
				memberCashvalue.setUpdateDate(currentDate);
				memberCashvalue.setUpdateBy(staffUserId);
				
				if (superiorMemberId != null) {
					//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
					memberCashvalue.setCustomerId(superiorMemberId);
				}else {
					memberCashvalue.setCustomerId(customerId);
				}
				memberCashvalue.setAvailableBalance(topUpAmount);
				memberCashValueDao.saveMemberCashValue(memberCashvalue);
			} else {
				memberCashvalue.setUpdateDate(currentDate);
				memberCashvalue.setUpdateBy(staffUserId);
				BigDecimal updatedAmount = memberCashvalue
						.getAvailableBalance().add(topUpAmount);
				memberCashvalue.setAvailableBalance(updatedAmount);
				memberCashValueDao.updateMemberCashValue(memberCashvalue);
			}
			topUpDto.setCustomerId(memberCashvalue.getCustomerId());//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
			saveTopupHistory(topUpDto);
		}
		topUpDto.setCustomerId(customerId);//receipt email should be sent to current member.
		topUpDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		responseResult.initResult(GTAError.Success.SUCCESS,customerOrderTrans);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult topupByCard(TopUpDto topUpDto) {
		Date currentDate = new Date();
		String staffUserId = topUpDto.getStaffUserId();
		Long customerId = topUpDto.getCustomerId();
		String terminalId = topUpDto.getTerminalId();
		String agentTransactionNo = topUpDto.getAgentTransactionNo();
		BigDecimal topUpAmount = topUpDto.getTopUpAmount();
		int validate = topUpAmount.compareTo(BigDecimal.ZERO);
		if (validate!=1) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_TOPUP_AMOUNT);
			return responseResult;
		}
		Member member = memberDao.getMemberById(topUpDto.getCustomerId());
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		customerOrderHd.setStaffUserId(staffUserId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(topUpAmount);
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		Long orderNo = (Long) customerOrderHdDao
				.addCustomreOrderHd(customerOrderHd);
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		// CUSTOMER ORDER Detail
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		customerOrderDet.setCustomerOrderHd(temp);
		customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
		customerOrderDet.setItemRemark("");
		customerOrderDet.setOrderQty(1L);
		customerOrderDet.setItemTotalAmout(topUpAmount);
		customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderDet.setCreateBy(staffUserId);
		customerOrderDet.setUpdateDate(currentDate);
		customerOrderDet.setUpdateBy(staffUserId);
		customerOrderDetDao.saveOrderDet(customerOrderDet);

		// CUSTOMER ORDER TRANS
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(topUpAmount);
		customerOrderTrans.setStatus(Constant.Status.PND.name());
		customerOrderTrans.setPaymentRecvBy(staffUserId);
		customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
		customerOrderTrans.setTerminalId(terminalId);
		customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
		customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		customerOrderTrans.setCreateBy(staffUserId);
		customerOrderTrans.setCreateDate(currentDate);
		customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		responseResult.initResult(GTAError.Success.SUCCESS);
		responseResult.setData(customerOrderTrans);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult handlePosPaymentResult(PosResponse posResponse) {
		Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
//		BigDecimal topUpAmount = new BigDecimal(Double.parseDouble(posResponse.getAmount().trim()));
		if (null == customerOrderTrans) {
			responseResult.initResult(GTAError.MemberShipError.TRANSACTION_NOT_EXISTS);
			return responseResult;
		}
		BigDecimal topUpAmount = customerOrderTrans.getPaidAmount();
		customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode().trim());
		customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber().trim());
		customerOrderTrans.setInternalRemark(posResponse.getResponseText().trim());
		//customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));
		customerOrderTrans.setStatus(CustomerTransationStatus.SUC.getDesc());
		customerOrderTrans.setUpdateBy("System");
		customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));// Card Type may be Master Visa or Union pay.
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		if (null != customerOrderHd) {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
			BigDecimal totalAmount = BigDecimal.ZERO;
			for (CustomerOrderTrans tempTrans: customerOrderTranses) {
				if (CustomerTransationStatus.SUC.getDesc().equals(tempTrans.getStatus())) {
					totalAmount = totalAmount.add(tempTrans.getPaidAmount());
				}
			}
			if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount)==0) {
				customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
				customerOrderHd.setUpdateDate(new Date());
				customerOrderHdDao.update(customerOrderHd);
			}
		}
		Long customerId = customerOrderHd.getCustomerId();
		Member member = memberDao.getMemberById(customerId);
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		MemberCashvalue memberCashvalue = null;
		Long superiorMemberId = member.getSuperiorMemberId();
		if(superiorMemberId!=null){
			memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
		}else {
			memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		}
		Date currentDate = new Date();
		if (memberCashvalue == null) {
			memberCashvalue = new MemberCashvalue();
			memberCashvalue.setInitialDate(currentDate);
			memberCashvalue.setInitialValue(topUpAmount);
			memberCashvalue.setExchgFactor(new BigDecimal(1));
			memberCashvalue.setInternalRemark("");
			if (superiorMemberId != null) {
				//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
				memberCashvalue.setCustomerId(superiorMemberId);
			}else {
				memberCashvalue.setCustomerId(customerId);
			}
			memberCashvalue.setAvailableBalance(topUpAmount);
			memberCashValueDao.saveMemberCashValue(memberCashvalue);
		} else {
			BigDecimal updatedAmount = memberCashvalue
					.getAvailableBalance().add(topUpAmount);
			memberCashvalue.setAvailableBalance(updatedAmount);
			memberCashValueDao.updateMemberCashValue(memberCashvalue);
		}
		TopUpDto topUpDto = new TopUpDto();
		topUpDto.setCustomerId(memberCashvalue.getCustomerId());//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
		topUpDto.setTransactionNo(transactionNo);
		topUpDto.setPaymentMethod(CommUtil.getCardType(posResponse.getCardType()));
		topUpDto.setTopUpAmount(topUpAmount);
		saveTopupHistory(topUpDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	

	@Override
	@Transactional
	public Data getRecentServedMemberList(ListPage<Member> page, String sortBy, String isAscending, Long[] customerId) {
		ListPage<Member> memberOverviewList = memberDao.getRecentServedMemberList(page, sortBy, isAscending, customerId);

		Data data = new Data();
		data.setList(memberOverviewList.getDtoList());
		data.setLastPage(memberOverviewList.isLast());
		data.setCurrentPage(memberOverviewList.getNumber());
		data.setRecordCount(memberOverviewList.getAllSize());
		data.setPageSize(memberOverviewList.getSize());
		data.setTotalPage(memberOverviewList.getAllPage());
		
		return data;
	}
	
	@Override
	@Transactional
	public ResponseResult getMembersOverviewList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType){
		
		ListPage<Member> memberOverviewList = memberDao.getMemberList(page, sortBy, isAscending, customerId, status, expiry,planName,memberType);
		
		Data data = new Data();
		for(Object object: memberOverviewList.getDtoList()){
			DependentMemberDto dto = (DependentMemberDto) object;
			if(Constant.memberType.IPM.name().equals(dto.getMemberType())||Constant.memberType.CPM.name().equals(dto.getMemberType())){
				boolean dependentCreationRight = customerProfileService.getDependentCreationRight(dto.getEnrollStatus(), dto.getPlanNo(), dto.getCustomerId());
				dto.setDependentCreationRight(dependentCreationRight);
			}else{
				dto.setDependentCreationRight(false);
			}
			
			if(Constant.General_Status_NACT.equals(dto.getStatus())){
				dto.setDependentCreationRight(false);
			}
		}
		data.setList(memberOverviewList.getDtoList());
		data.setLastPage(memberOverviewList.isLast());
		data.setCurrentPage(memberOverviewList.getNumber());
		data.setRecordCount(memberOverviewList.getAllSize());
		data.setPageSize(memberOverviewList.getSize());
		data.setTotalPage(memberOverviewList.getAllPage());
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
		
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(){
		List<SysCode> genderCodes = sysCodeDao.selectSysCodeByCategory("gender");
		List<SysCode> memberStatusCodes = sysCodeDao.selectSysCodeByCategory("memberstatus");
		return memberDao.assembleQueryConditions(genderCodes,memberStatusCodes);
	}

	@Override
	@Transactional
	public void sentTopupEmail(TopUpDto topUpDto) {
		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId("topup");
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, topUpDto.getCustomerId());
		String to = customerProfile.getContactEmail();
		String copyto = null;
		String subject = template.getMessageSubject();
		String username = customerProfile.getSalutation()+" "+customerProfile.getGivenName()+" "+customerProfile.getSurname();
		Member member = memberDao.get(Member.class, topUpDto.getCustomerId());
		MemberCashvalue memberCashvalue = null;
		//if current member is a dependent member,should get superior member's cashvalue balance.
		if (null == member.getSuperiorMemberId()) {
			memberCashvalue = memberCashValueDao.getByCustomerId(topUpDto.getCustomerId());
		}else {
			memberCashvalue = memberCashValueDao.getByCustomerId(member.getSuperiorMemberId());
			CustomerProfile superiorCustomer = customerProfileDao.get(CustomerProfile.class, member.getSuperiorMemberId());
			copyto = superiorCustomer.getContactEmail();
		}
		String content = template.getContentHtml().replaceAll(MessageTemplateParams.UserName.getParam(), username);
		content = content.replaceAll(MessageTemplateParams.TopupMethod.getParam(), topUpDto.getPaymentMethod());
		String currentBalance =  "HK\\$ "+new DecimalFormat("#,###.00").format(memberCashvalue.getAvailableBalance());
		content = content.replaceAll(MessageTemplateParams.CurrentBalance.getParam(),currentBalance);
		byte[] receiptAttach = null;
		try {
			receiptAttach = customerOrderTransDao.getInvoiceReceipt(null, topUpDto.getTransactionNo().toString(),"topup");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<byte[]> attachList = new ArrayList<byte[]>();
		attachList.add(receiptAttach);
		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add("HKGTA_Receipt-"+topUpDto.getTransactionNo().toString()+".pdf");
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		try {
//			MailSender.sendEmailWithBytesMineAttach(to, copyto,null,subject,content, mineTypeList,attachList,fileNameList);
			CustomerEmailContent customerEmailContent = new CustomerEmailContent();
			
			customerEmailContent.setContent(content);
			customerEmailContent.setRecipientCustomerId(member.getCustomerId().toString());
			customerEmailContent.setRecipientEmail(to);
			customerEmailContent.setSendDate(new Date());
			customerEmailContent.setSenderUserId(topUpDto.getStaffUserId());
			customerEmailContent.setSubject(subject);
			customerEmailContent.setCopyto(copyto);
			customerEmailContentDao.save(customerEmailContent);
			
			CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
			customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
			customerEmailAttach.setAttachmentName(fileNameList.get(0));
			customerEmailAttachDao.save(customerEmailAttach);
			mailThreadService.sendWithResponse(customerEmailContent, attachList, mineTypeList, fileNameList);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Sent topup successfully email to user fail!");
		}
	}

	@Override
	@Transactional
	public void saveTopupHistory(TopUpDto topUpDto) {
		MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, topUpDto.getCustomerId());
		CashvalueTopupHistory topupHistory = new CashvalueTopupHistory();
		Date currentDate = new Date();
		topupHistory.setAmount(topUpDto.getTopUpAmount());
		if (null != memberCashvalue && null != memberCashvalue.getExchgFactor()) {
			topupHistory.setCashvalueAmt(topUpDto.getTopUpAmount().multiply(memberCashvalue.getExchgFactor()));
		}else {
			topupHistory.setCashvalueAmt(topUpDto.getTopUpAmount());
		}
		topupHistory.setCustomerId(topUpDto.getCustomerId());
		topupHistory.setSuccessDate(currentDate);
		topupHistory.setTopupDate(currentDate);
		topupHistory.setCreateDate(currentDate);
		topupHistory.setTopupMethod(topUpDto.getPaymentMethod());
		topupHistoryDao.save(topupHistory);
	}

	@Override
	@Transactional
	public ResponseResult getTotalAmount(Long customerId, String transactionType) {
		List<Serializable> param = new ArrayList<Serializable>();
		String sql = "";
		if ("EXP".equalsIgnoreCase(transactionType)) {
			sql = "SELECT\n" +
					"	SUM(c.paid_amount) AS totalAmount\n" +
					"FROM\n" +
					"	customer_order_trans c,\n" +
					"	customer_order_hd h,\n" +
					"	customer_order_det d,\n" +
					"	member m\n" +
					"WHERE\n" +
					"	h.order_no= c.order_no\n" +
					"AND h.order_no = d.order_no\n" +
					"AND d.item_no != 'CVT0000001'\n" +
					"AND d.item_no != 'SRR00000001'\n" +
					"AND d.item_no != 'CVR00000001'\n" +
					"AND (c. STATUS = 'SUC' OR c. STATUS = 'RFU')\n" +
					"AND h.customer_id = m.customer_id\n" +
					"AND (\n" +
					"	m.customer_id = ? \n" +
					"	OR m.superior_member_id = ? \n" +
					")\n" +
					"AND d.order_det_id = (\n" +
					"	SELECT\n" +
					"		min(dd.order_det_id)\n" +
					"	FROM\n" +
					"		customer_order_det dd\n" +
					"	WHERE\n" +
					"		dd.order_no = h.order_no\n" +
					")";
			param.add(customerId);
			param.add(customerId);
		}
		if ("TOP".equalsIgnoreCase(transactionType)) {
			sql = "SELECT\n" +
					"	SUM(c.paid_amount) AS totalAmount\n" +
					"FROM\n" +
					"	customer_order_trans c,\n" +
					"	customer_order_hd h,\n" +
					"	customer_order_det d,\n" +
					"	member m\n" +
					"WHERE\n" +
					"	h.order_no= c.order_no\n" +
					"AND h.order_no = d.order_no\n" +
					"AND d.item_no = 'CVT0000001'\n" +
					"AND c. STATUS = 'SUC' \n" +
					"AND h.customer_id = m.customer_id\n" +
					"AND (\n" +
					"	m.customer_id = ? \n" +
					"	OR m.superior_member_id = ? \n" +
					")\n" +
					"AND d.order_det_id = (\n" +
					"	SELECT\n" +
					"		min(dd.order_det_id)\n" +
					"	FROM\n" +
					"		customer_order_det dd\n" +
					"	WHERE\n" +
					"		dd.order_no = h.order_no\n" +
					")";
			param.add(customerId);
			param.add(customerId);
		}
		if ("SR".equalsIgnoreCase(transactionType)) {
			sql = "SELECT \n" +
					"    SUM(approvedAmt) AS totalAmount\n" +
					"FROM\n" +
					"    (SELECT \n" +
					"            req.refund_id refundId,\n" +
					"            hd.customer_id customerId,\n" +
					"            req.create_date requestDate,\n" +
					"            req.refund_transaction_no refundTransactionNo,\n" +
					"            req.refund_service_type refundFrom,\n" +
					"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
					"            req.status,\n" +
					"            tran.paid_amount paidAmt,\n" +
					"            req.request_amt requestAmt,\n" +
					"            req.approved_amt approvedAmt,\n" +
					"            req.internal_remark remark\n" +
					"    FROM\n" +
					"        customer_refund_request req\n" +
					"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
					"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
					"    LEFT JOIN customer_profile pro ON pro.customer_id = hd.customer_id\n" +
					"    WHERE req.refund_service_type <> 'CVL' AND hd.customer_id = ? \n" +
					") sub";
			param.add(customerId);
		}
		if ("CVR".equalsIgnoreCase(transactionType)) {
			sql = "SELECT \n" +
					"    SUM(approvedAmt) AS totalAmount\n" +
					"FROM\n" +
					"    (SELECT \n" +
					"            req.refund_id refundId,\n" +
					"            hd.customer_id customerId,\n" +
					"            req.create_date requestDate,\n" +
					"            req.refund_transaction_no refundTransactionNo,\n" +
					"            req.refund_money_type refundMethod,\n" +
					"            req.refund_service_type refundFrom,\n" +
					"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
					"            req.status,\n" +
					"            tran.paid_amount paidAmt,\n" +
					"            tran.agent_transaction_no refNo,\n" +
					"            mem.academy_no academyNo,\n" +
					"            req.request_amt requestAmt,\n" +
					"            req.approved_amt approvedAmt,\n" +
					"            (select concat(staff.given_name, ' ', staff.surname) from staff_profile staff where staff.user_id = req.auditor_user_id) approvedBy,\n" +
					"            req.internal_remark remark\n" +
					"    FROM\n" +
					"        customer_refund_request req\n" +
					"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
					"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
					"    LEFT JOIN member mem ON mem.customer_id = hd.customer_id\n" +
					"    JOIN customer_profile pro ON pro.customer_id = mem.customer_id\n" +
					"    WHERE req.refund_service_type = 'CVL' AND hd.customer_id = ? AND req.status = 'APR'\n" +
					") sub";
			param.add(customerId);
		}
		
		
		
		BigDecimal totalAmount = (BigDecimal)memberCashValueDao.getUniqueBySQL(sql, param);
		responseResult.initResult(GTAError.Success.SUCCESS,totalAmount);
		return responseResult;
	}
}
