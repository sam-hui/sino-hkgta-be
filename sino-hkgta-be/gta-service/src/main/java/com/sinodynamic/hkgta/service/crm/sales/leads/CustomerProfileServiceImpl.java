package com.sinodynamic.hkgta.service.crm.sales.leads;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowagie.text.DocumentException;
import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateProfileDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAddressDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerPreEnrollStageDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MemberTypeDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.RemarksDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanRightMasterDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.UserActivateQuestDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerCheckExsitDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEmailDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentSearchDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.EnrollmentListResponseDto;
import com.sinodynamic.hkgta.dto.crm.LeadListResponseDto;
import com.sinodynamic.hkgta.dto.crm.MemberAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.MemberAccountRightsDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.dto.membership.QuestionAnswerOrBirthDateDto;
import com.sinodynamic.hkgta.dto.membership.UserActivateQuestDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CorporateProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.EnrollmentHistoryDto;
import com.sinodynamic.hkgta.entity.crm.FacilityEntitlementDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRuleLog;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuest;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuestPK;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.notification.SMSService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.constant.AddressType;
import com.sinodynamic.hkgta.util.constant.CaptionCategory;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CustomerProfileServiceImpl extends ServiceBase<CustomerProfile> implements CustomerProfileService{
	private Logger logger = Logger.getLogger(CustomerProfileServiceImpl.class);
    
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	@Autowired
	private CustomerAddressDao customerAddressDao;
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerAddressDao CustomerAddressDao;

	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private ServicePlanDao servicePlanDao;

	@Autowired
	private MemberTypeDao memberTypeDao;
	
	@Autowired
	private CustomerPreEnrollStageDao customerPreEnrollStageDao;

	@Autowired
	private CustomerServiceDao customerServiceDao;
	
	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private ServicePlanRightMasterDao servicePlanRightMasterDao;
	
	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
	
	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;
	
	@Autowired
	private RemarksDao remarksDao;
	
	@Autowired
	private CorporateMemberDao corporateMemberDao;
	
	@Autowired
	private CorporateServiceAccDao corporateServiceAccDao;
	
	@Autowired
	private CorporateServiceSubscribeDao corporateServiceSubscribeDao;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private UserActivateQuestDao userActivateQuestDao;
	
	@Autowired
	private UserMasterDao userMasterDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private MemberLimitRuleLogDao memberLimitRuleLogDao;
	
	@Autowired
	private SMSService smsService;
	
	@Autowired
	private CorporateProfileDao corporateProfileDao;
	
	@Transactional
	public ResponseResult getCustomerProfileList(
			ListPage<CustomerProfile> page, CustomerProfileDto dto,String userId,String device) {

		if (CommUtil.notEmpty(dto.getPageNumber())) {
			page.setNumber(Integer.parseInt(dto.getPageNumber()));
		}
		if (CommUtil.notEmpty(dto.getPageSize())) {
			page.setSize(Integer.parseInt(dto.getPageSize()));
		}
		ListPage<CustomerProfile> list = customerProfileDao.getCustomerProfileList(page, dto);
		int count = list.getDtoList().size();
		if (count <= 0) {
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		List<SysCode> sysCode = sysCodeDao.selectSysCodeByCategory("contactClassCode");
		for (Object object : list.getDtoList()) {
			LeadListResponseDto leadDto = (LeadListResponseDto) object;
			BigInteger customerId = leadDto.getCustomerId();
			if(EnrollStatus.TOA.name().equals(leadDto.getStatus())){
				leadDto.setStatus("To Active"+DateConvertUtil.getNoOfDaysInfuture(leadDto.getEffectiveDate()));
			}
			if (customerId != null) {
				int unread = remarksDao.countUnreadRemarkByDevice(customerId.longValue(), userId, device);
				if (unread > 0) {
					leadDto.setUnRead(true);
				}
			}
			for (SysCode objectSys : sysCode) {
				if (objectSys.getCodeValue().equalsIgnoreCase(leadDto.getContactClassCode())) {
					leadDto.setContactClassCode(objectSys.getCodeDisplay());
				}
			}
		}
		Data data = new Data();
		data.setList(list.getDtoList());
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	public Serializable addCustomerProfile(CustomerProfile t) {
		return customerProfileDao.addCustomerProfile(t);
		
	}

	public void deleteCustomerProfile(CustomerProfile t) {
		customerProfileDao.deleteCustomerProfile(t);
		
	}

	public void updateCustomerProfile(CustomerProfile t) {
		customerProfileDao.updateCustomerProfile(t);
		
	}

	public CustomerProfile getCustomerProfile(CustomerProfile t) {
		return customerProfileDao.getCustomerProfile(t);
	}
	
	@Transactional
	public CustomerProfile getById(Long customerId) {
		return customerProfileDao.getById(customerId);
	}

	@Transactional
	public void moveProfileAndSignatureFile(CustomerProfile customerProfile) throws Exception{
		Long customerId = customerProfile.getCustomerId();
		CustomerProfile dBCustomerProfile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
		if(dBCustomerProfile==null) return;
		boolean isPortraitPhotoModified = false;
		boolean isSignatureModified = false;
		String portraitPhoto = customerProfile.getPortraitPhoto();
		String signature = customerProfile.getSignature();
		String currentPortraitPhoto = dBCustomerProfile.getPortraitPhoto();
		String currentSignature = dBCustomerProfile.getSignature();
		if(!StringUtils.isBlank(portraitPhoto)&&!portraitPhoto.equals(currentPortraitPhoto)){
			isPortraitPhotoModified = true;
		}
		
		if(!StringUtils.isBlank(signature)&&!signature.equals(currentSignature)){
			isSignatureModified = true;
		}
		
		String basePath = "";
		try {
			basePath = FileUpload.getBasePath(FileUpload.FileCategory.USER);
		} catch (Exception e) {
			logger.debug("Loading Basepath Failed");
			e.printStackTrace();
		}
		File customerFile = new File(basePath + File.separator + customerId.longValue());
		if(!customerFile.exists()){
			boolean isCreated = customerFile.mkdirs();
			if (!isCreated) {
				logger.debug("Creating Folder Failed");
			}
		}
		boolean isSuccPortrait = false;
		if(!StringUtils.isBlank(portraitPhoto)&&isPortraitPhotoModified){
			isSuccPortrait = FileUtil.moveFile(basePath +  portraitPhoto, basePath + File.separator + customerId.longValue() + portraitPhoto);
			if (isSuccPortrait) {
				customerProfile.setPortraitPhoto("/" + customerId.longValue() +  portraitPhoto);
			}
		}
		boolean isSuccSignature = false;
		if (!StringUtils.isBlank(signature)&&isSignatureModified) {
			isSuccSignature = FileUtil.moveFile(basePath + signature, basePath + File.separator + customerId.longValue() + signature);
			if (isSuccSignature) {
				customerProfile.setSignature("/" + customerId.longValue() + signature);
			}
		}
		
		boolean updatePortrait = isSuccPortrait&&isPortraitPhotoModified;
		boolean updateSignature = isSuccSignature&&isSignatureModified;
		if (updatePortrait || updateSignature) {
			customerProfileDao.updateProfileAndSignatureFile(customerProfile, new Long(dBCustomerProfile.getVersion()));
		}
		
		if(updatePortrait){
			@SuppressWarnings("unused")
			boolean result = new File(basePath+currentPortraitPhoto).delete();
		}
		
		if(updateSignature){
			@SuppressWarnings("unused")
			boolean result = new File(basePath+currentSignature).delete();
		}
		
	}
	
	@Transactional(readOnly=true)
	public CustomerCheckExsitDto checkExistPassportNO(String passportType, String passportNO) {		
		return customerProfileDao.checkAvailableByPassportTypeAndPassportNo(passportType, passportNO);		
	}
	
	@Transactional
	public boolean checkAvailablePassportNo(String passportType, String passportNO,Long customerId){
		return customerProfileDao.checkAvailablePassportNo(customerId, passportType, passportNO);
	}

	@Override
	@Transactional(readOnly=true)
	public CustomerProfile getByCustomerID(Long customerID) throws Exception {
		
		CustomerProfile cp = customerProfileDao.getById(customerID);
		List<CustomerAdditionInfo> cailist = customerAdditionInfoDao.getCustomerAdditionInfoListByCustomerID(cp.getCustomerId());
		List<CustomerAddress> calist = customerAddressDao.getCustomerAddressesByCustomerID(cp.getCustomerId());
		CustomerEnrollment ce = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(cp.getCustomerId());
		Member member = memberDao.getMemberById(cp.getCustomerId());
		List<CustomerEnrollment> celist = new ArrayList<CustomerEnrollment>();
		if(null != ce){
			celist.add(ce);
		}else{
			Long superiorMemberId = member.getSuperiorMemberId();
			CustomerEnrollment superiorEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(superiorMemberId);
			celist.add(superiorEnrollment);
		}
		cp.setCustomerAdditionInfos(cailist);
		cp.setCustomerAddresses(calist);
		cp.setCustomerEnrollments(celist);
		cp.setCheckBillingAddress("false");
		cp.setMemberType(member==null?"":member.getMemberType());
		cp.setMember(member);
		cp.setRelationshipCode(member==null?"":member.getRelationshipCode()); 
//		cp.setRelationshipCode("CHD"); 
		if(calist != null && calist.size() > 0){
			for(CustomerAddress address : calist){
				String billingAddress1 = address.getAddress1();
				String billingAddress2 = address.getAddress2();
				String billingHkDistrict = address.getHkDistrict();
				String address1 = cp.getPostalAddress1();
				String address2 = cp.getPostalAddress2();
				String district = cp.getPostalDistrict();
				if (StringUtils.equals(address1, billingAddress1) && StringUtils.equals(address2, billingAddress2) && StringUtils.equals(district, billingHkDistrict)) 
					cp.setCheckBillingAddress("true");
			}
		}
		
		SysCode natureCode = sysCodeDao.getByCategoryAndCodeValue("businessNature", cp.getBusinessNature());
		SysCode natioanlityCode = sysCodeDao.getByCategoryAndCodeValue("nationality", cp.getNationality());
		
		if(!StringUtils.isEmpty(cp.getBusinessNature())&&natureCode!=null){
			cp.setFullBusinessNature(natureCode.getCodeDisplay());
		}else{
			cp.setFullBusinessNature(cp.getBusinessNature());
		}
		
		if(!StringUtils.isEmpty(cp.getNationality())&&natioanlityCode!=null){
			cp.setFullNationality(natioanlityCode.getCodeDisplay());
		}else{
			cp.setFullNationality(cp.getNationality());
		}
		
		return cp;
	}
	
	
	
	
	@Override
	@Transactional(readOnly=true)
	public CustomerProfile getCustomerInfoByCustomerID(Long customerID) throws Exception {
		
		CustomerProfile cp = customerProfileDao.getById(customerID);
		List<CustomerAdditionInfo> cailist = customerAdditionInfoDao.getCustomerAdditionInfoListByCustomerID(cp.getCustomerId());
		List<CustomerAddress> calist = customerAddressDao.getCustomerAddressesByCustomerID(cp.getCustomerId());
//		Member member = memberDao.getMemberById(cp.getCustomerId());
//		CustomerEnrollment ce = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(cp.getCustomerId());
		
//		List<CustomerEnrollment> celist = new ArrayList<CustomerEnrollment>();
//		if(null != ce){
//			celist.add(ce);
//		}
		
		cp.setCustomerAdditionInfos(cailist);
		cp.setCustomerAddresses(calist);
//		cp.setCustomerEnrollments(celist);
//		cp.setMember(member);
		 
		return cp;
	}
	
	
	@Override
	@Transactional
	public ResponseResult getEnrollments(ListPage<CustomerProfile> page,
			CustomerEnrollmentSearchDto dto,String userId,String device) {

		if (CommUtil.notEmpty(dto.getPageNumber())) {
			page.setNumber(Integer.parseInt(dto.getPageNumber()));
		}
		if (CommUtil.notEmpty(dto.getPageSize())) {
			page.setSize(Integer.parseInt(dto.getPageSize()));
		}
		ListPage<CustomerProfile> list = customerProfileDao.getCustomerEnrollments(page, dto);
		int count = list.getDtoList().size();
		if (count <= 0) {
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		List<Object> enrollmentListResponseDtos = list.getDtoList();
		List<EnrollmentListResponseDto> newEnrollmentListResponseDtos = new ArrayList<EnrollmentListResponseDto>();
		for (Object object : enrollmentListResponseDtos) {
			EnrollmentListResponseDto enrollmentListResponseDto = (EnrollmentListResponseDto) object;
			BigInteger customerId = enrollmentListResponseDto.getCustomerId();
			boolean depCreationRight = getDependentCreationRight(enrollmentListResponseDto.getStatus(),enrollmentListResponseDto.getPlanNo(), customerId.longValue());
			enrollmentListResponseDto.setDependentCreationRight(depCreationRight);
			
			if(EnrollStatus.TOA.name().equals(enrollmentListResponseDto.getStatus())){
				enrollmentListResponseDto.setStatus("To Active"+DateConvertUtil.getNoOfDaysInfuture(enrollmentListResponseDto.getEffectiveDate()));
			}
			if (customerId != null) {
				int unread = remarksDao.countUnreadRemarkByDevice(customerId.longValue(), userId,device);
				if (unread > 0) {
					enrollmentListResponseDto.setUnRead(true);
				}
			}

			newEnrollmentListResponseDtos.add(enrollmentListResponseDto);
		}
		Data data = new Data();
		data.setList(newEnrollmentListResponseDtos);
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	public boolean getDependentCreationRight(String enrollStatus,Long planNo ,Long customerId){
		if(EnrollStatus.NEW.name().equals(enrollStatus)||EnrollStatus.APV.name().equals(enrollStatus)||EnrollStatus.PYA.name().equals(enrollStatus)||EnrollStatus.TOA.name().equals(enrollStatus)){
			ServicePlanAdditionRule servicePlanAdditionRule = servicePlanAdditionRuleDao.getByPlanNoAndRightCode(planNo, "G1");
			if(servicePlanAdditionRule!=null){
				if("true".equalsIgnoreCase(servicePlanAdditionRule.getInputValue())){
					return true;
				}else{
					return false;
				}
			}
		}
		
		if(EnrollStatus.ANC.name().equals(enrollStatus)||EnrollStatus.CMP.name().equals(enrollStatus)){
			MemberLimitRule memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, "G1");
			if(memberLimitRule!=null){
				if("true".equalsIgnoreCase(memberLimitRule.getTextValue())){
					return true;
				}else{
					return false;
				}
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@Override
	@Transactional
	public ResponseResult getAccountInfo(Long customerID) {
		MemberAccountInfoDto memberAccountInfoDto = new MemberAccountInfoDto();
		memberAccountInfoDto.setCustomerId(customerID);
		Member member = memberDao.get(Member.class, customerID);
		
		/*To get the member's basic info*/
		if (null != member) {
			memberAccountInfoDto.setMemberStatus(member.getStatus());
			memberAccountInfoDto.setMemberType(member.getMemberType());
			memberAccountInfoDto.setAcademyNo(member.getAcademyNo());
		}
		
		String memberType = member.getMemberType();
		Long primaryMemberCustomerId = customerID;
		Long limitRuleCustomerId = customerID;
		if (MemberType.IDM.name().equals(memberType) || MemberType.CDM.name().equals(memberType)) {
			primaryMemberCustomerId = member.getSuperiorMemberId();
		}
		
		/*To get the Credit Limit of Primary Member*/
		MemberLimitRule memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(primaryMemberCustomerId, "CR");
		if (null != memberLimitRule) {
			memberAccountInfoDto.setLimitValue(memberLimitRule.getNumValue());
			memberAccountInfoDto.setCreditLimitId(memberLimitRule.getLimitId());
			if (null != memberLimitRule.getVersion()) {
				memberAccountInfoDto.setMemberLimitRuleVersion(memberLimitRule.getVersion());
			}
		}
//		memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerID, "TRN");
//		if (null != memberLimitRule) {
//			memberAccountInfoDto.setLimitValue(memberLimitRule.getNumValue());
//			memberAccountInfoDto.setCreditLimitId(memberLimitRule.getLimitId());
//			if (null != memberLimitRule.getVersion()) {
//				memberAccountInfoDto.setMemberLimitRuleVersion(memberLimitRule.getVersion());
//			}
//		}
		
		/*To get the day pass purchase's daily quota*/
		memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerID, "D1");
		if (null != memberLimitRule) {
			memberAccountInfoDto.setDailyQuota(memberLimitRule.getNumValue());
		}
		
		/*To retrieve the company name for the corporate member*/
		if (MemberType.CDM.name().equals(memberType) || MemberType.CPM.name().equals(memberType)) {
			CorporateMember corporateMember = corporateMemberDao.get(CorporateMember.class, primaryMemberCustomerId);
			CorporateProfile corporateProfile = corporateMember.getCorporateProfile();
			memberAccountInfoDto.setCompanyName(corporateProfile.getCompanyName());
		}

		/*To fetch the enrollment status. Note: may need to re-write*/
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(primaryMemberCustomerId);
		if (customerEnrollment == null)
			return new ResponseResult("1", "Missing record in Customer Enrollment! Please contact backoffice!");
		memberAccountInfoDto.setEnrollStatus(customerEnrollment.getStatus());
		
		/*To get the member's service account info*/
		CustomerAccountInfoDto customerServiceAccountInfo = customerServiceDao.getNearestServiceAccountByCustomerId(primaryMemberCustomerId);
		if (customerServiceAccountInfo != null){
			memberAccountInfoDto.setPlanName(customerServiceAccountInfo.getPlanName());
			memberAccountInfoDto.setEffectiveDate(customerServiceAccountInfo.getEffectiveDate());
			memberAccountInfoDto.setExpiryDate(customerServiceAccountInfo.getExpiryDate());
			memberAccountInfoDto.setServiceAccount(customerServiceAccountInfo.getServiceAccount());
			memberAccountInfoDto.setContractLength(customerServiceAccountInfo.getContractLength());
			memberAccountInfoDto.setOrderTotalAmount(customerServiceAccountInfo.getOrderTotalAmount());
		}

		/*To get the member limit rule and facility entitlement*/
		List<MemberLimitRuleDto> servicePlanRightMasterDtos = memberLimitRuleDao.getEffectiveMemberLimitRuleDtoByCustomerId(limitRuleCustomerId);
		memberAccountInfoDto.setServicePlanRightMasterDtos(servicePlanRightMasterDtos);

		List<FacilityEntitlementDto> facilityEntitlements = memberPlanFacilityRightDao.getEffectiveMemberPlanFacilityRightsByCustomerId(member.getCustomerId());
		memberAccountInfoDto.setFacilityEntitlements(facilityEntitlements);
		
		/*To get the enrollment history*/
		List<EnrollmentHistoryDto> enrollmentHistoryDtos = customerServiceDao.getEnrollmentHistoryList(customerID);
		memberAccountInfoDto.setEnrollmentHistoryDtos(enrollmentHistoryDtos);

		/*To get the login id*/
		if (member != null && !StringUtils.isBlank(member.getUserId())) {
			UserMaster userMaster = userMasterDao.getUserByUserId(member.getUserId());
			memberAccountInfoDto.setLoginId(userMaster != null ? userMaster.getLoginId() : null);
		}

		responseResult.initResult(GTAError.Success.SUCCESS,memberAccountInfoDto);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getCustomerIdByUserId(String userId){
			List<CustomerProfile> customer = customerProfileDao.getCustomerIdByUserId(userId);
			if(customer==null|| customer.size()==0) return new ResponseResult("1","Retrieve customerId failed!");
			responseResult.initResult(GTAError.Success.SUCCESS,customer.get(0));
			return responseResult;
	}
	
	@Transactional
	public ResponseResult getDependentMmeberInforByCustomerId(Long customerId){
		DependentMemberInfoDto memberInfoDto = null;
		try{
			memberInfoDto = customerProfileDao.getDependentMemberInfoByCustomerId(customerId);
			if(memberInfoDto == null) {
				return new ResponseResult("1","Retrieve member info failed!");
			}
			BigDecimal tran = memberLimitRuleDao.getTransactionLimitOfMember(customerId);
			memberInfoDto.setTransactionLimit(tran);
			Boolean dayPass = memberLimitRuleDao.getDayPassPurchaseNoOfMember(customerId);
			memberInfoDto.setDaypassPurchase(dayPass);
			Boolean facility = memberPlanFacilityRightDao.getEffectiveMemberFacilityRight(customerId);
			memberInfoDto.setFacility(facility);
			Boolean training = memberLimitRuleDao.getMemberTrainingRight(customerId);
			memberInfoDto.setTraining(training);
			Boolean event = memberLimitRuleDao.getMemberEventsRight(customerId);
			memberInfoDto.setEvents(event);
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerProfileServiceImpl.getDependentMmeberInforByCustomerId Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS,memberInfoDto);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult updateDependentMembersRight(Long customerId, String facilityRight, String trainingRight, String eventRight, String dayPass, BigDecimal tran) {
		try {
			if (null != tran) {
				int check = tran.compareTo(new BigDecimal(99999999.99).setScale(2, BigDecimal.ROUND_HALF_UP));
				if (!org.springframework.util.StringUtils.isEmpty(tran) && check == 1)
					return new ResponseResult("1", "The credit limit exceeds the maximum!Maximum is 99999999.99");
			}
			if (memberLimitRuleDao.updateMmberTransactionLimit(customerId, tran)) {
				memberLimitRuleDao.updateMemberFacilitiesRight(customerId, facilityRight);
				memberLimitRuleDao.updateMemberTrainingRight(customerId, trainingRight);
				memberLimitRuleDao.updateMemberEventsRight(customerId, eventRight);
				memberLimitRuleDao.updateMemberDayPassPurchasing(customerId, dayPass);
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;

			} else {
				responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_NOT_ALLOWED);
				return responseResult;
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerProfileServiceImpl.updateDependentMembersRight Exception ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	@Transactional
	public ResponseResult getMmeberRightsByCustomerId(Long customerId){
		MemberAccountRightsDto memberAccountRightsDto = new MemberAccountRightsDto();
		try{
			String facilityString = memberPlanFacilityRightDao.getEffectiveMemberFacilityRightsInMyAccount(customerId);
			memberAccountRightsDto.setFacility(facilityString);
			Boolean dayPass = memberLimitRuleDao.getDayPassPurchaseNoOfMember(customerId);
			memberAccountRightsDto.setDaypassPurchase(dayPass);
			Boolean training = memberLimitRuleDao.getMemberTrainingRight(customerId);
			memberAccountRightsDto.setTraining(training);
			Boolean event = memberLimitRuleDao.getMemberEventsRight(customerId);
			memberAccountRightsDto.setEvents(event);
		}catch(Exception e) {
			e.printStackTrace();
			logger.debug("CustomerProfileServiceImpl.getMmeberRightsByCustomerId Exception ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS,memberAccountRightsDto);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult editLimitValue(Long customerID, BigDecimal limitValue,String loginUserId, Integer memberLimitRuleVersion) throws Exception{
		Member member = memberDao.getMemberById(customerID);
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)) {
			responseResult.initResult(GTAError.MemberShipError.INACTIVE_MEMBER);
			return responseResult;
		}
		
		MemberLimitRule memberLimitRule = null;
		if (MemberType.IDM.name().equals(member.getMemberType()) || MemberType.CDM.name().equals(member.getMemberType())) {
			memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerID, "TRN");
		} else {
			memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerID, "CR");
		}
		if (memberLimitRule == null) {
			responseResult.initResult(GTAError.MemberShipError.MISSING_CR_TRN);
			return responseResult;
		}
		
		MemberCashvalue memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerID);
		if (limitValue != null && memberCashvalue != null && memberCashvalue.getAvailableBalance() != null && (MemberType.IPM.name().equals(member.getMemberType()) || MemberType.CPM.name().equals(member.getMemberType()))) {
			BigDecimal balance = memberCashvalue.getAvailableBalance();
			if (balance.add(limitValue).compareTo(BigDecimal.ZERO) < 0) {
				responseResult.initResult(GTAError.MemberShipError.MEMBER_CASH_VALUE_UNPAID);
				return responseResult;
			}

		}
		if (null == limitValue && (MemberType.CPM.name().equals(member.getMemberType()) || MemberType.IPM.name().equals(member.getMemberType()))) {
			limitValue = BigDecimal.ZERO;
		}
		if (MemberType.CPM.name().equals(member.getMemberType())) {
			CorporateMember corporateMember = corporateMemberDao.get(CorporateMember.class, customerID);
			BigDecimal availableCreditLimit = corporateMemberDao.getRemainAvailableCreditLimit(corporateMember.getCorporateProfile().getCorporateId(), customerID);
			if (limitValue.compareTo(availableCreditLimit) > 0) {
				Object[] object = new Object[] { availableCreditLimit.setScale(2, BigDecimal.ROUND_HALF_UP) };
				responseResult.initResult(GTAError.MemberShipError.CPM_OVER_LIMIT, object);
				return responseResult;
			}
		}
		boolean isUpdate = true;
		if (null != memberLimitRule.getNumValue()&&limitValue!=null) {
			if (memberLimitRule.getNumValue().compareTo(limitValue) == 0) {
				isUpdate = false;
			}
		} else {
			if (null == memberLimitRule.getNumValue() && limitValue ==null) {
				isUpdate = false;
			}
		}
		
		if(isUpdate){
			memberLimitRule.setNumValue(limitValue);
			memberLimitRule.setUpdateDate(new Date());
			memberLimitRule.setUpdateBy(loginUserId);
			memberLimitRule.setVersion(new Long(memberLimitRuleVersion));
			memberLimitRuleDao.getCurrentSession().evict(memberLimitRule);
			memberLimitRuleDao.update(memberLimitRule);
			recordMemberLimitRuleLog(memberLimitRule);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
		//Discussed with Nicky and Moby, It does not need send SMS to member when member's Status and Credit Limit be changed.
		/*if (isUpdate && Constant.Status.ACT.name().equalsIgnoreCase(member.getStatus())) {
				try {
						MessageTemplate mt = null;
						if (MemberType.IPM.getType().equalsIgnoreCase(member.getMemberType()) || MemberType.CPM.getType().equalsIgnoreCase(member.getMemberType())) {
								mt = messageTemplateDao.getTemplateByFunctionId("update_credit_limit");
						} else {
								mt = messageTemplateDao.getTemplateByFunctionId("update_spending_limit");
						}

						String content = null;
						if (null == mt) {
								if (MemberType.IPM.getType().equalsIgnoreCase(member.getMemberType()) || MemberType.CPM.getType().equalsIgnoreCase(member.getMemberType())) {
										content = Constant.UPDATE_CUSTOMER_CREDIT_LIMIT_SMS_TEMPLETE;
								} else {
										content = Constant.UPDATE_CUSTOMER_SPENING_LIMIT_SMS_TEMPLETE;
								}
						} else {
								content = mt.getContent();
						}
					smsService.sendSMS(Arrays.asList(customerProfile.getPhoneMobile()),content, new Date());
			} catch (Exception e) {
				logger.debug("Send SMS failed!", e);
			}
		}*/
		
	}
	
	
	public void recordMemberLimitRuleLog(MemberLimitRule memberLimitRule){
		MemberLimitRuleLog memberLimitRuleLog = new MemberLimitRuleLog();
		
		BeanUtils.copyProperties(memberLimitRule, memberLimitRuleLog);
		memberLimitRuleLog.setLogDate(new Date());
		memberLimitRuleLogDao.save(memberLimitRuleLog);
		
	}
	@Override
	@Transactional
	public CustomerProfile getCustomerProfileByPassportTypeAndPassportNo(String passportType, String passportNO) {

		CustomerProfile customerProfile = customerProfileDao.getUniqueCustomerProfileByPassportTypeAndPassportNo(passportType, passportNO);
		if(customerProfile!=null){
			Member member = memberDao.get(Member.class, customerProfile.getCustomerId());
			if(member!=null&&!StringUtils.equals(member.getMemberType(),"MG")
				&&!StringUtils.equals(member.getMemberType(),"HG")){
				throw new RuntimeException("This HKID has been used by a Member!");
			}
		}
		return customerProfile;
	}
	
	@Transactional
	public ResponseResult getAcademyNoReserveStatus(Long customerId){
		MemberDto dto = customerProfileDao.getAcademyNoReservationStatus(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return responseResult;
	}
	
	@SuppressWarnings("deprecation")
	@Transactional
	public ResponseResult sendEnrollmentFormEmail(CustomerEmailDto emailDto,String userName,String userId) {
		Long customerId = emailDto.getCustomerId();
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if (customerEnrollment != null && !EnrollStatus.NEW.name().equals(customerEnrollment.getStatus())) {
			return new ResponseResult("1", "The status is not permited to send the enrollment form!");
		}
		CustomerProfile cp = customerProfileDao.getById(customerId);
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("enrollForm");
		String to = emailDto.getTo();
		String subject = emailDto.getSubject();
		String content = emailDto.getContent();
		if (StringUtils.isEmpty(to)) {
			to = cp.getContactEmail();
		}
		if (StringUtils.isEmpty(subject)) {
			subject = mt.getMessageSubject();
		}
		if (StringUtils.isEmpty(content)) {
			content = mt.getFullContentHtml(cp.getGivenName() + " " + cp.getSurname(),userName);
		}
		try {

			List<byte[]> bytesList = new ArrayList<byte[]>();
			List<String> fileNameList = new ArrayList<String>();
			bytesList.add(getEnrollFormReport(customerId));
			List<String> mineTypeList = new ArrayList<String>();
			mineTypeList.add("application/pdf");
			fileNameList.add("Enrollment Form - " + cp.getGivenName() + " " + cp.getSurname() + ".pdf");
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/TermsConditions.pdf";
			File terms = new File(reportPath);
			FileInputStream inputStream = null;
			if (terms.exists()) {
				inputStream = new FileInputStream(terms);
				bytesList.add(IOUtils.toByteArray(inputStream));
				fileNameList.add("Terms Conditions for HKGTA" + ".pdf");
				mineTypeList.add("application/pdf");
			}

			CustomerEmailContent customerEmailContent = new CustomerEmailContent();

			customerEmailContent.setContent(content);
			customerEmailContent.setRecipientCustomerId(customerId.toString());
			customerEmailContent.setRecipientEmail(to);
			customerEmailContent.setSendDate(new Date());
			customerEmailContent.setSenderUserId(userId);
			customerEmailContent.setSubject(subject);
			String sendId = (String) customerEmailContentDao.save(customerEmailContent);
			customerEmailContent.setSendId(sendId);

			CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
			customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
			customerEmailAttach.setAttachmentName(fileNameList.get(0));
			customerEmailAttachDao.save(customerEmailAttach);

			mailThreadService.sendWithResponse(customerEmailContent, bytesList, mineTypeList, fileNameList);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			logger.debug("CustomerProfileServiceImpl.sendEnrollmentFormEmail Exception ", e);
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@Transactional
	public byte[] getEnrollFormInOnePdf(Long customerId,String userId) throws DocumentException, IOException {
		try {
			return getEnrollFormReport(customerId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	@Transactional
	public List<CustomerProfile> getCustomers() {
		return customerProfileDao.getByHql("from CustomerProfile");
	}
	
	@Override
	@Transactional
	public byte[] getEnrollFormReport(Long customerId) throws Exception {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/"+"jasper/";
		String parentjasperPath = reportPath+"EnrollmentForm.jasper";

		File reFile = new File(parentjasperPath);
		Map<String,Object> parameters = new HashMap<String,Object>();
		DataSource ds=SessionFactoryUtils.getDataSource(customerProfileDao.getsessionFactory());
	    Connection dbconn=DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		
		parameters.put("customerId", customerId);
		parameters.put("tick_image", reportPath+"tick_image.jpg");
		parameters.put("image_path", reportPath+"logo_header.jpg");
		parameters.put("SUBREPORT_DIR", reportPath);
		
		String sql = "SELECT concat(sp.plan_name,' at ','HK$',ps.item_price) AS servicePlan FROM customer_enrollment ce, service_plan_pos pos, pos_service_item_price ps, service_plan sp"
				+ "	WHERE pos.plan_no = sp.plan_no AND pos.pos_item_no = ps.item_no AND ps.item_catagory = 'SRV' AND ps.item_no LIKE 'SRV%' AND ce.subscribe_plan_no = sp.plan_no AND ce.customer_id = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		String servicePlan = (String)customerProfileDao.getUniqueBySQL(sql, param);
		parameters.put("servicePlan", servicePlan);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));

		exporter.exportReport();
		return outPut.toByteArray();
	}

	@Override
	@Transactional
	public ResponseResult verifyMemberInfo(String surname, String givenName, String passportType, String passportNo) {
		CustomerProfile customerProfile = customerProfileDao.getUniqueByCols(CustomerProfile.class, new String[]{"passportType","passportNo"}, new String[]{passportType, passportNo});
		if (null == customerProfile) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_PASSPORTTYPE_OR_PASSPROTNO);
			return responseResult;
		}
		
		/* do not need to verify the surname
		if ((!surname.equals(customerProfile.getSurname())) || (!givenName.equals(customerProfile.getGivenName()))) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_SURNAME_OR_GIVENNAME);
			return responseResult;
		}
		*/
		Member member = memberDao.get(Member.class, customerProfile.getCustomerId());
		if (null == member.getUserId()) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		List<UserActivateQuest> userActivateQuests = userActivateQuestDao.getByCol(UserActivateQuest.class, "id.userId", member.getUserId(), null);
		List<UserActivateQuestDto> userActivateQuestDtos = null;
		if (null != userActivateQuests && userActivateQuests.size()>0) {
			userActivateQuestDtos = new ArrayList<UserActivateQuestDto>();
			
			for (UserActivateQuest quest: userActivateQuests) {
				UserActivateQuestDto userActivateQuestDto = new UserActivateQuestDto();
				userActivateQuestDto.setUserId(quest.getId().getUserId());
				userActivateQuestDto.setQuestionNo(quest.getId().getQuestionNo());
				userActivateQuestDto.setQuestion(quest.getQuestion());
				userActivateQuestDtos.add(userActivateQuestDto);
			}
			Map<String, Object> questionMap = new HashMap<String, Object>();
			questionMap.put("secretType", "true");
			questionMap.put("questionList", userActivateQuestDtos);
			responseResult.initResult(GTAError.Success.SUCCESS, questionMap);
		}else {
			Map<String, Object> questionMap = new HashMap<String, Object>();
			questionMap.put("secretType", "false");
			questionMap.put("userId", member.getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS, questionMap);
		}
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult validateBirthDateOrQuestionAnswer(QuestionAnswerOrBirthDateDto questionAnswerOrBirthDateDto) {
		if ("true".equals(questionAnswerOrBirthDateDto.getSecrectType())) {
			List<UserActivateQuestDto> userActivateQuestDtos = questionAnswerOrBirthDateDto.getUserActivateQuestDtos();
			for (UserActivateQuestDto userActivateQuestDto : userActivateQuestDtos) {
				UserActivateQuestPK id = new UserActivateQuestPK();
				id.setQuestionNo(userActivateQuestDto.getQuestionNo());
				id.setUserId(userActivateQuestDto.getUserId());
				
				UserActivateQuest userActivateQuest = userActivateQuestDao.get(UserActivateQuest.class, id);
				if (!userActivateQuestDto.getAnswer().equalsIgnoreCase(userActivateQuest.getAnswer())) {
					responseResult.initResult(GTAError.MemberShipError.INCORRECT_ANSWER,new Object[]{userActivateQuestDto.getQuestionNo()});
					return responseResult;
				}
			}
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}else {
			String hkid = questionAnswerOrBirthDateDto.getHkid();
			Date birthDate = questionAnswerOrBirthDateDto.getBirthDate();
//			Date birthDate = DateConvertUtil.parseString2Date(birthDateStr, "yyyy-MM-dd");
			CustomerProfile customerProfile = customerProfileDao.getUniqueByCols(CustomerProfile.class, new String[]{"passportNo","dateOfBirth"}, new Serializable[]{hkid,birthDate});
			if (null == customerProfile) {
				responseResult.initResult(GTAError.MemberShipError.INCORRECT_HKID_OR_BIRTHDATE);
				return responseResult;
			}
			Member member = memberDao.get(Member.class, customerProfile.getCustomerId());
			if (null == questionAnswerOrBirthDateDto.getUserId() || !questionAnswerOrBirthDateDto.getUserId().equals(member.getUserId())) {
				responseResult.initResult(GTAError.MemberShipError.INCORRECT_HKID_OR_BIRTHDATE);
				return responseResult;
			}
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult updateMemberPwd(StaffPasswordDto staffPsw) {
		UserMaster user = userMasterDao.getUserByUserId(staffPsw.getUserId());
		
		if (null == user)
		{
			throw new GTACommonException(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
		}
		
		if (!staffPsw.getNewPsw().equals(staffPsw.getRepeatPsw()))
		{
			throw new GTACommonException(GTAError.StaffMgrError.PSW_NOT_SAME);
		}
		
		if (!CommUtil.validatePassword(staffPsw.getNewPsw()))
		{
			throw new GTACommonException(GTAError.StaffMgrError.PSW_NOT_VALID);
		}
		
		
		String psw = CommUtil.getMD5Password(user.getLoginId(), staffPsw.getNewPsw());
		if (psw.equals(user.getPassword()))
		{
			throw new GTACommonException(GTAError.StaffMgrError.NEWPSW_CANNOT_SAMEAS_OLDPSW);
		}
		
		user.setPassword(psw);
		user.setPasswdChangeDate(new Date());
		user.setUpdateBy(staffPsw.getUserId());
		user.setUpdateDate(new Date());
		userMasterDao.update(user);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getUserActiveRequest(String userId) {
		List<UserActivateQuest> userActivateQuests = userActivateQuestDao.getByCol(UserActivateQuest.class, "id.userId", userId, null);
		List<UserActivateQuestDto> userActivateQuestDtos = null;
		if (null != userActivateQuests && userActivateQuests.size()>0) {
			userActivateQuestDtos = new ArrayList<UserActivateQuestDto>();
			
			for (UserActivateQuest quest: userActivateQuests) {
				UserActivateQuestDto userActivateQuestDto = new UserActivateQuestDto();
				userActivateQuestDto.setUserId(quest.getId().getUserId());
				userActivateQuestDto.setQuestionNo(quest.getId().getQuestionNo());
				userActivateQuestDto.setQuestion(quest.getQuestion());
				userActivateQuestDto.setAnswer(quest.getAnswer());
				userActivateQuestDtos.add(userActivateQuestDto);
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS, userActivateQuestDtos);
		return responseResult;
	}

	/**   
	* @author: Zero_Wang
	* @since: Sep 18, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public ResponseResult checkMemberExistByPassportTypeAndPassportNo(
			String passportType, String passportNo) {
		CustomerProfile customerProfile = customerProfileDao.getUniqueCustomerProfileByPassportTypeAndPassportNo(passportType, passportNo);
		if(null==customerProfile){
			this.responseResult.initResult(GTAError.Success.SUCCESS);
		}else {
			Long customerId = customerProfile.getCustomerId();
			Member member = memberDao.get(Member.class, customerId);
			CustomerEnrollment c = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
			if(member!=null){
				//MG - Member Guest;HG - House Gues
				if(Constant.memberType.MG.toString().equalsIgnoreCase(member.getMemberType()) || Constant.memberType.HG.toString().equalsIgnoreCase(member.getMemberType())){
					this.responseResult.initResult(GTAError.Success.SUCCESS,customerProfile);
				//member closed or not
				}else if("CLOSE".equals(member.getStatus())){
					this.responseResult.initResult(GTAError.Success.SUCCESS,customerProfile);
				}else {
					this.responseResult.initResult(GTAError.LeadError.Exist_Member_NoNeed);
				}
			//lead
			}else if(c!=null && c.getStatus().equalsIgnoreCase("OPN")){
				this.responseResult.initResult(GTAError.Success.SUCCESS,customerProfile);
			}
		}
		return this.responseResult;
	}
	
	@Transactional
	public Map<String, Object> getPersonalInfoForTablet(Long customerId) {

		CustomerProfile customerProfile = customerProfileDao.getLimitedCustomerInfoForTablet(customerId);
		List<CustomerAdditionInfoDto> guesHouse = customerAdditionInfoDao.getByCustomerIdAndCategoryForTablet(customerId, CaptionCategory.CNSGH.getDesc());
		List<CustomerAdditionInfoDto> kids = customerAdditionInfoDao.getByCustomerIdAndCategoryForTablet(customerId, CaptionCategory.CNSKID.getDesc());
		List<CustomerAdditionInfoDto> fb = customerAdditionInfoDao.getByCustomerIdAndCategoryForTablet(customerId, CaptionCategory.CNSFB.getDesc());
		List<CustomerAdditionInfoDto> spa = customerAdditionInfoDao.getByCustomerIdAndCategoryForTablet(customerId, CaptionCategory.CNSSPA.getDesc());
		List<CustomerAdditionInfoDto> golf = customerAdditionInfoDao.getByCustomerIdAndCategoryForTablet(customerId, CaptionCategory.CNSGF.getDesc());
		List<CustomerAdditionInfoDto> tennis = customerAdditionInfoDao.getByCustomerIdAndCategoryForTablet(customerId, CaptionCategory.CNSTNS.getDesc());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("customerInfo", customerProfile);
		map.put("guestHouse", guesHouse);
		map.put("kids", kids);
		map.put("fb", fb);
		map.put("spa", spa);
		map.put("golf", golf);
		map.put("tennis", tennis);

		return map;
	}

	@Transactional
	public CustomerProfile importPrimaryDeailForAutofillingDependentForm(Long customerId) {
		CustomerProfile cp = customerProfileDao.getPrimaryDetailForAutoFillingDependent(customerId);
		CustomerAddress billingAddress = customerAddressDao.getByCustomerIDAddressType(customerId,AddressType.BILL.name());
		List<CustomerAddress> calist = Arrays.asList(billingAddress);
		cp.setCustomerAddresses(calist);
		cp.setCheckBillingAddress("false");
		if (billingAddress != null) {
			String billingAddress1 = billingAddress.getAddress1();
			String billingAddress2 = billingAddress.getAddress2();
			String billingHkDistrict = billingAddress.getHkDistrict();
			String address1 = cp.getPostalAddress1();
			String address2 = cp.getPostalAddress2();
			String district = cp.getPostalDistrict();
			if (StringUtils.equals(address1, billingAddress1) && StringUtils.equals(address2, billingAddress2) && StringUtils.equals(district, billingHkDistrict)) {
				cp.setCheckBillingAddress("true");
			}
		}
		return cp;
	}
}

