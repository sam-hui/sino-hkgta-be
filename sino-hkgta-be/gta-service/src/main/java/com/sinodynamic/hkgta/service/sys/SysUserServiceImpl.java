package com.sinodynamic.hkgta.service.sys;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.type.BigIntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.dao.AbstractCommonDataQueryDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.sys.UserMgrAuditDao;
import com.sinodynamic.hkgta.dao.sys.UserRoleDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.sys.RoleMasterDto;
import com.sinodynamic.hkgta.dto.sys.SysUserDto;
import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.crm.UserRole;
import com.sinodynamic.hkgta.entity.crm.UserRolePK;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.AuditUtil;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;

@Repository
public class SysUserServiceImpl extends ServiceBase<UserMaster> implements SysUserService {
	Logger logger = Logger.getLogger(SysUserServiceImpl.class);
	
	private static String SYS_ADMIN = "system.admin";
	
	@Autowired
	private UserMasterDao userMasterDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	private UserRoleDao userRoleDao;
	
	@Autowired
	@Qualifier("commonBySQLDao")
	AbstractCommonDataQueryDao abstractCommonDataQueryDao;
	
	@Autowired
    private SysCodeDao sysCodeDao;
	
	@Autowired
	private MessageTemplateDao templateDao;
	
	@Autowired
	private CustomerEmailContentDao CustomerEmailContentDao;
	
	@Autowired
	private UserMgrAuditDao userMgrAuditDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private MemberDao memberDao;
	
	private class $ParserContext implements ParserContext{

		@Override
		public boolean isTemplate() {
			return true;
		}

		@Override
		public String getExpressionPrefix() {
			return "{";
		}

		@Override
		public String getExpressionSuffix() {
			return "}";
		}
		
	}
	
	private class TemplateObject extends SysUserDto{
		private static final long serialVersionUID = 1L;

		private String changedContent;
		
		private String userName;//the user uses this system
		
		public String getChangedContent() {
			return changedContent;
		}

		public void setChangedContent(String changedContent) {
			this.changedContent = changedContent;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}
	}
	
	private class EmailThread implements Runnable {
		
		
		
		private String email;
		private Object templateObject;
		MessageTemplate template ;
		
		public EmailThread(String moduleId,String email, Object templateObject) {
			this.email = email;
			this.templateObject = templateObject;
			template = templateDao.getTemplateByFunctionId(moduleId);
		}

		@Override
		public void run() {
			try {
				ExpressionParser parser = new SpelExpressionParser();
				Expression exp = parser.parseExpression(template.getContentHtml(), new $ParserContext());
				StandardEvaluationContext context = new StandardEvaluationContext(templateObject);
				MailSender.sendEmail(email, null, null, template.getMessageSubject(), exp.getValue(context, String.class), null);
			} catch (Exception e) {
				logger.error("error sent email", e);
			}
		}
	}
	
	private class ValuePair{
		Object oldValue;
		Object newValue;
		
		ValuePair(){
			
		}
		
		ValuePair(Object oldValue, Object newValue){
			this.oldValue = oldValue;
			this.newValue = newValue;
		}
		public Object getOldValue() {
			return oldValue;
		}
		public void setOldValue(Object oldValue) {
			this.oldValue = oldValue;
		}
		public Object getNewValue() {
			return newValue;
		}
		public void setNewValue(Object newValue) {
			this.newValue = newValue;
		}
		
	}
	
	private class $MethodBeforeAdvice implements MethodBeforeAdvice{
		Map<String, ValuePair> modified;
		
		$MethodBeforeAdvice(Map<String, ValuePair> modified){
			this.modified = modified;
		}
		
		@Override
		public void before(Method method, Object[] args, Object target)
				throws Throwable {
			String methodName = method.getName();
			
			
			if(StringUtils.startsWith(methodName, "set") && args.length == 1){
				String propertyName = StringUtils.uncapitalize(methodName.replaceFirst("set", ""));
				
				Method get = target.getClass().getDeclaredMethod(methodName.replaceFirst("set", "get"), new Class<?>[0]);
				Object originalValue = get.invoke(target, new Object[0]);
				if(!ObjectUtils.equals(originalValue, args[0])){
					modified.put(propertyName, new ValuePair(originalValue, args[0]));
					method.invoke(target, args);
				}
			}
		}
	}
	
	@Override
	@Transactional
	public void addSysUser(SysUserDto user) {
		try{
			Assert.isNull(userMasterDao.getUniqueByCol(UserMaster.class, "loginId", user.getLoginId()), "Login ID has been existed");
			Assert.isNull(userMasterDao.getUniqueByCol(UserMaster.class, "userId", "[" + user.getLoginId() + "]"), "User ID has been existed");
			UserMaster userMaster = new UserMaster();
			
			String password = CommUtil.getMD5Password(user.getLoginId(), user.getPassword());
			
			BeanUtils.copyProperties(user, userMaster);
			userMaster.setPassword(password);
			
			Date now = new Date();
			userMaster.setCreateDate(now);
			userMaster.setUpdateDate(now);
			userMaster.setUserId("[" + user.getLoginId() + "]");
			userMasterDao.addUserMaster(userMaster);
			
			
			if(CollectionUtils.isNotEmpty(user.getRoleIdList())){
				for (Long roleId : user.getRoleIdList()){
					UserRole userRole = new UserRole();
					UserRolePK key = new UserRolePK();
					key.setRoleId(roleId.toString());
					key.setUserId(userMaster.getUserId());
					
					userRole.setId(key);
					userRole.setCreateBy(user.getCreateBy());
					userRole.setCreateDate(new Timestamp(now.getTime()));
					
					userRoleDao.save(userRole);
				}
			}
			
		}catch(Exception e){
			logger.error(e, e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
		
		
		
	}

	@Override
	@Transactional
	public Data loadAllUsers(String propertyName, String order,
			int pageSize, int currentPage, AdvanceQueryDto filters) {
		Data data = new Data();
		String filterSql = null;
		if(filters != null){
			filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
		}
		ListPage<UserMaster> listPage = userMasterDao.getUserMasterList(propertyName,  order,  pageSize,  currentPage,
				filterSql);
		data.setCurrentPage(currentPage);
		data.setPageSize(pageSize);
		data.setTotalPage(listPage.getAllPage());
		data.setLastPage(currentPage==listPage.getAllPage());
		data.setRecordCount(listPage.getAllSize());
		
		Collection<SysUserDto> result = CollectionUtil.map(listPage.getList(), new CallBack<UserMaster, SysUserDto>(){

			@Override
			public SysUserDto execute(UserMaster t, int index) {
				SysUserDto dto = new SysUserDto();
				dto.setCreateBy(t.getCreateBy());
				dto.setDefaultModule(t.getDefaultModule());
				dto.setLoginId(t.getLoginId());
				dto.setNickname(t.getNickname());
				dto.setStatus(t.getStatus());
				dto.setUpdateBy(t.getUpdateBy());
				dto.setUserId(t.getUserId());
				dto.setUserType(t.getUserType());
				dto.setUserType(t.getUserType());
				dto.setSuperAdmin(t.getUserId().equals(appProps.get(SYS_ADMIN).toString()));
				return dto;
			}
			
		});
		
		data.setList(new ArrayList<SysUserDto>(result));
		
		return data;
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> userListAdvanceSearch() {
		
		//List<SysCode> userStatus = sysCodeDao.getSysCodeByCategory("userStatus");
		//List<SysCode> userType = sysCodeDao.getSysCodeByCategory("userType");
		//List<SysCode> defaultModule = sysCodeDao.getSysCodeByCategory("defaultModule");
		
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("User ID", "userId", "java.lang.String", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Login ID", "loginId", "java.lang.String", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Nickname", "nickname", "java.lang.String", "", 3);
		//AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Status", "status", "java.lang.String", userStatus, 4);
		//AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("User Type", "userType", "java.lang.String", userType, 5);
		//AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Default Module", "defaultModule", "java.lang.String", defaultModule, 6);
		
		
		
		return Arrays.asList(condition1
				,condition2
				,condition3
				//,condition4
				//,condition5
				//,condition6
				);
	}

	@Override
	@Transactional
	public void editSysUser(SysUserDto user) {
		try{
			UserMaster userMaster = userMasterDao.getUserByUserId(user.getUserId());
			
			String originalLoginId = userMaster.getLoginId();
			String originJSON = new Gson().toJson(userMaster);
			String primaryKeyValue = AuditUtil.getPrimaryKeyValue(userMaster);
			
			ProxyFactory proxy = new ProxyFactory();
			Map<String, ValuePair> modified = new LinkedHashMap<String, ValuePair>();
			
			proxy.setTarget(userMaster);
			proxy.addAdvice(new $MethodBeforeAdvice(modified));
			
			userMaster = (UserMaster)proxy.getProxy();
			
			userMaster.setNickname(user.getNickname());
			userMaster.setUserType(user.getUserType());
			userMaster.setDefaultModule(user.getDefaultModule());
			
			Date now = new Date();
			
			if(!StringUtils.equals(user.getLoginId(), originalLoginId)){
				Assert.isNull(userMasterDao.getUniqueByCol(UserMaster.class, "loginId", user.getLoginId()), "LoginId has been existed");
				
				if(StringUtils.isNotEmpty(user.getPassword())){
					userMaster.setPassword(CommUtil.getMD5Password(user.getLoginId(), user.getPassword()));
				}else{
					userMaster.setPassword(CommUtil.getMD5Password(user.getLoginId(), "123456"));
				}
				userMaster.setLoginId(user.getLoginId());
				userMaster.setPasswdChangeDate(now);
			}else if(StringUtils.isNotEmpty(user.getPassword())){
				userMaster.setPassword(CommUtil.getMD5Password(user.getLoginId(), user.getPassword()));
				userMaster.setPasswdChangeDate(now);
			}
			
			List<Long> existedRoleIds = getExistedRoleIdList(user.getUserId());
			
			if(isRolesChanged(existedRoleIds, user.getRoleIdList())){
				modified.put("roleIdList", new ValuePair(existedRoleIds, user.getRoleIdList()));
			}
			
			userRoleDao.sqlUpdate("delete from user_role where user_id = ?", Arrays.<Serializable>asList(user.getUserId()));
			
			if(CollectionUtils.isNotEmpty(user.getRoleIdList())){
				for (Long roleId : user.getRoleIdList()){
					UserRole userRole = new UserRole();
					UserRolePK key = new UserRolePK();
					key.setRoleId(roleId.toString());
					key.setUserId(userMaster.getUserId());
					
					userRole.setId(key);
					userRole.setCreateBy(user.getCreateBy());
					userRole.setCreateDate(new Timestamp(now.getTime()));
					
					userRoleDao.save(userRole);
				}
			}
			
			if(!modified.isEmpty()){
				UserRecordActionLog log = new UserRecordActionLog();
				log.setAction("C");
				log.setActionTimestamp(new Date());
				log.setActionUserId(user.getUpdateBy());
				log.setBeforeImage(originJSON);
				log.setPrimaryKeyValue(primaryKeyValue);
				log.setTableName("user_master");
				userMgrAuditDao.save(log);
				
				//////////////////////////
				userMaster.setUpdateBy(user.getUpdateBy());
				userMaster.setUpdateDate(now);
			}
			
			
			if(!modified.isEmpty()){
				TemplateObject template = new TemplateObject();
				BeanUtils.copyProperties(user, template);
				
				setUserName(user, template);
				setChangedContent(user, template, modified);
				
				String email = getContactEmail(user);
				
				if(StringUtils.isNotEmpty(email)){
					Thread thread = new Thread(new EmailThread("temp_profile_edit", email, template));
					thread.start();
				}
				
			}
		}catch(Exception e){
			logger.error("editSysUser()", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}
	
	boolean isRolesChanged(List<Long> before, List<Long> after){
		return !before.equals(after);
	}
	
	List<Long> getExistedRoleIdList(String userId){
		SQLQuery sql = userRoleDao.getCurrentSession().createSQLQuery("select role_id from user_role where user_id = ?");
		sql.setString(0, userId);
		List<Long> list = sql.addScalar("role_id", LongType.INSTANCE).list();
		
		return list;
	}
	
	//can return null
	CustomerProfile getCustomerProfile(SysUserDto user){
		Member customer = memberDao.getMemberByUserId(user.getUserId());
		if(customer == null)
		{
			return null;
		}
		CustomerProfile customerProfile = customerProfileDao.getCustomerProfileByCustomerId(customer.getCustomerId());
		
		return customerProfile;
	}
	
	//can return null
	StaffProfile getStaffProfile(SysUserDto user){
		StaffProfile staffProfile = staffProfileDao.getByUserId(user.getUserId());
		
		return staffProfile;
	}
	
	String getContactEmail(SysUserDto user){
		CustomerProfile customerProfile = getCustomerProfile(user);
		StaffProfile staffProfile = getStaffProfile(user);
		
		if(customerProfile != null){
			return ObjectUtils.toString(customerProfile.getContactEmail());
		}else if(staffProfile != null){
			return ObjectUtils.toString(staffProfile.getContactEmail());
		}else{
			return "";
		}
	}
	
	void setUserName(SysUserDto user, TemplateObject template){
		CustomerProfile customerProfile = getCustomerProfile(user);
		StaffProfile staffProfile = getStaffProfile(user);
		
		if(customerProfile != null){
			template.setUserName(customerProfile.getSalutation()+ " " +customerProfile.getGivenName() + " " + customerProfile.getSurname());
		}else if(staffProfile != null){
			template.setUserName(staffProfile.getGivenName() + " " + staffProfile.getSurname());
		}else{
			template.setUserName("");
		}
	}
	
	void setChangedContent(SysUserDto user, TemplateObject template, Map<String, ValuePair> modified){
		Map<String,String> propertyToDescReset2 = new LinkedHashMap<String,String>();
		
		propertyToDescReset2.put("loginId", "Log-in ID");
		propertyToDescReset2.put("nickname", "Nick name");
		propertyToDescReset2.put("userType", "User type");
		
		Map<String,String> propertyToDescReset = new LinkedHashMap<String,String>();
		propertyToDescReset.put("password", "Password");
		propertyToDescReset.put("roleIdList", "User role");
		
		String reset2Template = "{['fieldName']} is reset to {['newValue']}</br>";
		String resetTemplate =  "{['fieldName']} is reset</br>";
		
		ExpressionParser parser = new SpelExpressionParser();
		Expression reset2Exp = parser.parseExpression(reset2Template, new $ParserContext());
		Expression resetExp = parser.parseExpression(resetTemplate, new $ParserContext());
		
		Map<Map<String,String>, Expression> data = new HashMap<Map<String,String>, Expression>();
		data.put(propertyToDescReset2, reset2Exp);
		data.put(propertyToDescReset, resetExp);
		
		StringBuilder changedContent = new StringBuilder();
		
		for(Map<String,String> property2Desc : data.keySet()){
			for(String property : property2Desc.keySet()){
				if(modified.get(property) != null) {
					Map<String, Object> valuePair = new HashMap<String, Object>();
					
					valuePair.put("fieldName", property2Desc.get(property));
					valuePair.put("oldValue", modified.get(property).getOldValue());
					valuePair.put("newValue", modified.get(property).getNewValue());
					
					changedContent.append(data.get(property2Desc).getValue(valuePair, String.class)).append("\n");
				}
			}
		}
		template.setChangedContent(changedContent.toString());
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void refreshUserRole(String userId)
	{
		UserRoleMap.getInstance().setUserRole(userId, userRoleDao.getUserRoleListByUserId(userId));
	}
	

	@Override
	@Transactional
	public SysUserDto viewSysUser(String userId) {
		try{
			UserMaster userMaster = userMasterDao.getUserByUserId(userId);
			SysUserDto dto = new SysUserDto();
			dto.setUserId(userMaster.getUserId());
			dto.setLoginId(userMaster.getLoginId());
			dto.setNickname(userMaster.getNickname());
			//dto.setPassword(userMaster.getPassword());
			dto.setStatus(userMaster.getStatus());
			dto.setUserType(userMaster.getUserType());
			dto.setDefaultModule(userMaster.getDefaultModule());
			
			
			String sql = "SELECT\n" + 
				    "ur.role_id roleId, m.role_name roleName\n" + 
				    "FROM\n" + 
				    "    user_role ur JOIN role_master m ON m.role_id = ur.role_id\n" + 
				    "WHERE\n" + 
				    "    ur.user_id = ?\n";
			
			Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
			typeMap.put("roleId", BigIntegerType.INSTANCE);
			typeMap.put("roleName", StringType.INSTANCE);
		
			
			List<RoleMasterDto> roleDtoList = userRoleDao.getDtoBySql(sql, Arrays.<Serializable>asList(userId), RoleMasterDto.class, typeMap);
			
			Collection<Map<String, Object>> result = CollectionUtil.map(roleDtoList, new CallBack<RoleMasterDto, Map<String, Object>>(){

				@Override
				public Map<String, Object> execute(RoleMasterDto t, int index) {
					Map<String, Object> role = new HashMap<String, Object>();
					role.put("roleId", t.getRoleId());
					role.put("roleName", t.getRoleName());
					return role;
				}
				
			});
			
			dto.setRoleList(new ArrayList(result));
			StaffProfile profile = staffProfileDao.getByUserId(userId);
			dto.setHasProfile(null!=profile);
			dto.setSuperAdmin(userId.equals(appProps.get(SYS_ADMIN).toString()));
			
			return dto;
		}catch(Exception e){
			logger.error(e, e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

}
