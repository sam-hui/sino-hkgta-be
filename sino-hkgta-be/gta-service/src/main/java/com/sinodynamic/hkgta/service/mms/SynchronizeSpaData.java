package com.sinodynamic.hkgta.service.mms;

public interface SynchronizeSpaData {
    
    public void synchronizePayments();
    
    public void synchronizeAppointments();
}
