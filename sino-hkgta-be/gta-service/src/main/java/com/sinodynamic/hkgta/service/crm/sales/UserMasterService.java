package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface UserMasterService extends IServiceBase<UserMaster> {
	
	public void addUserMaster(UserMaster m) throws Exception;
	
	public void getUserMasterList(ListPage<UserMaster> page,UserMaster m) throws Exception;

	public void updateUserMaster(UserMaster m) throws Exception;

	public void deleteUserMaster(UserMaster m) throws Exception;	
	
	public UserMaster getUserByLoginId(String id) throws Exception;
	
	public UserMaster getUserByUserId(String userId) throws Exception;
	
	public void SaveSessionToken(String userId, String authToken, String device) throws Exception;
	
	public GTAError updateSessionToken(String authToken);
	
	public GTAError updateSessionToken(String authToken, boolean shouldUpdateToken);
	
	public boolean deleteSessionToken(String userId, String device);
	
	public boolean requireValidateKaptchaCode(UserMaster userMaster);
	
	public boolean requireValidateKaptchaCode(String device);
	
	public boolean requireValidateLocation(String device);
	
	public boolean hasBeenLocked(UserMaster usermaster);
	
	public void updateLoginFailTime(UserMaster usermaster) throws Exception;
	
	public UserMaster getUserMasterByCustomerId(Long customerId);
	
	public void setAllLoginIdAndPassword(List<Member>customerIdList, String defaultPassword) throws Exception;
	
	
}
