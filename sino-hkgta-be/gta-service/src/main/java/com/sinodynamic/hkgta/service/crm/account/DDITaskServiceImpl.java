package com.sinodynamic.hkgta.service.crm.account;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.DDITransactionLogDao;
import com.sinodynamic.hkgta.dao.crm.DDInterfaceFileLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dto.account.DDIDto;
import com.sinodynamic.hkgta.dto.account.DDIReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDIReportDto;
import com.sinodynamic.hkgta.entity.crm.DdInterfaceFileLog;
import com.sinodynamic.hkgta.entity.crm.DdiTransactionLog;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.SftpByJsch;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class DDITaskServiceImpl extends ServiceBase implements DDITaskService {
	

	private Logger ddxLogger = LoggerFactory.getLogger("ddx");
	
	@Resource(name = "ddxProperties")
	protected Properties ddxProps;
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	@Autowired
	private DDInterfaceFileLogDao ddInterfaceFileLogDao;
	@Autowired
	private DDITransactionLogDao ddiTransactionLogDao;
	@Autowired
	private GtaPublicHolidayDao gtaPublicHolidayDao;
	
	private String ftpHost;
	private String port;
	private String ftpUserName;
	private String ftpPassword;
	private String timeout;

	private String uploadPath;
	private String receivePath;
	private String receiveBackPath;
	private String downLoadPath;//store all the remote files in local 
	private String downLoadBackPath;//store the success processed files in local
	
	private String originatorAccountNumber = "44708063295";
	
	@PostConstruct
	public void init() {
		ftpHost = ddxProps.getProperty(Constant.SFTP_HOST, null);
		port = ddxProps.getProperty(Constant.SFTP_PORT, null);
		ftpUserName = ddxProps.getProperty(Constant.SFTP_USERNAME, null);
		ftpPassword = ddxProps.getProperty(Constant.SFTP_PASSWORD, null);
		timeout = ddxProps.getProperty(Constant.SFTP_TIMEOUT, null);

		uploadPath = ddxProps.getProperty(Constant.SFTP_SNED_PATH, null);//remote upload dir
		receivePath = ddxProps.getProperty(Constant.SFTP_RECEIVE_PATH, null);//remote downloand dir
		receiveBackPath = ddxProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, null);//remote back dir, after processed move to this dir
		downLoadPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_PATH, null);//store all the remote files in local 
		downLoadBackPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_BACK_PATH, null);//store the success processed files in local

	}
	
	
	private String getNextWorkingDayFromPropFile(Date date){
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		HashSet holidaySet = new HashSet();
		String holidayListStr = ddxProps.getProperty("hk.holiday", null);
		StringTokenizer st = new StringTokenizer(holidayListStr, ",");
		while(st.hasMoreTokens()){
			holidaySet.add(st.nextToken());
		}
			
		Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		c.add(Calendar.DATE, 1);
		while( c.get(Calendar.DAY_OF_WEEK)==1 || c.get(Calendar.DAY_OF_WEEK)==7 || holidaySet.contains(sdf_yyyymmdd.format(c.getTime())) ){
			c.add(Calendar.DATE, 1);
		}
		date = c.getTime();
		String valueDate = sdf_yyyymmdd.format(date);
		
		return valueDate;
		
	}
	
	private String getNextWorkingDayFromDB(Date date) {
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		List<Date> holidays = gtaPublicHolidayDao.getNearestHolidays(date);
		List<String> holidayStrs= new ArrayList<String>();
		for(Date d:holidays) {
			holidayStrs.add(sdf_yyyymmdd.format(d));
		}
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		c.add(Calendar.DATE, 1);
		while( c.get(Calendar.DAY_OF_WEEK)==1 || c.get(Calendar.DAY_OF_WEEK)==7 || holidayStrs.contains(sdf_yyyymmdd.format(c.getTime())) ){
			c.add(Calendar.DATE, 1);
		}
		date = c.getTime();
		String valueDate = sdf_yyyymmdd.format(date);
		
		return valueDate;
	}
	
	
	
	
	
	private  void readDDIDetail(String[] csvItems, DDIReportDto ddiReport) {
		DDIReportDetailDto detail = new DDIReportDetailDto();
		detail.setEbswCorpId(csvItems[1]);
		detail.setBatchNumber(csvItems[2]);
		detail.setTransactionSeq(csvItems[3]);
		detail.setDebtorBankCode(csvItems[4]);
		detail.setDebtorBankBranchCode(csvItems[5]);
		detail.setDebtorAccountNumber(csvItems[6]);
		detail.setDebtorName(csvItems[7]);
		detail.setAmount(csvItems[8]);
		detail.setDebtorReference(csvItems[9]);
		detail.setValueDate(csvItems[10]);
		detail.setCrBankCode(csvItems[11]);
		detail.setCrAccountCode(csvItems[12]);
		detail.setParticular(csvItems[13]);
		detail.setReturnDate(csvItems[14]);
		detail.setReturnReason(csvItems[15]);
		detail.setEbswBranchCode(csvItems[16]);
		ddiReport.getDetails().add(detail);
	}
	
	public void readDDIHeader(String[] csvItems, DDIReportDto ddiReport) {
		ddiReport.setStartDate(csvItems[1]);
		ddiReport.setEndDate(csvItems[2]);
	}

	
	public void readDDITail(String[] csvItems, DDIReportDto ddiReport) {
		ddiReport.setNoOfRecord(Integer.parseInt(csvItems[1]));
	}
	
	private  DDIReportDto parseRejectReport(File file) throws Exception {
		DDIReportDto ddiReport = new DDIReportDto();
		ddiReport.setDetails(new ArrayList<DDIReportDetailDto>());
        BufferedReader br = null;
        try {
        	br = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            
            while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				String[] items = sCurrentLine.split(",");				
				if(items.length > 0 && 
						"H".equals(items[0]))
					readDDIHeader(items, ddiReport);
				
				else if(items.length >0 && 
						"D".equals(items[0]))
					readDDIDetail(items, ddiReport);
				else if(items.length >0 && 
						"T".equals(items[0]))
					readDDITail(items, ddiReport);
			}
            
            if(ddiReport.getDetails().size() != ddiReport.getNoOfRecord())
            	System.out.println("ERROR ! no. of record not equals");
            System.out.println(ddiReport);
            // TODO: save and check ddiReport to database
        } finally {
            br.close();
        }
        return ddiReport;
	}
	
	

	private File uploadDDIReport(List<DDIDto>  ddiList,String fileName){
		File ddiFile = null;
		//Start
		System.out.println("upload start");
		//prepare
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		StringBuilder fileContent = new StringBuilder();
		String resultRemark = "";
		String valueDate = StringUtils.isBlank(getNextWorkingDayFromDB(new Date()))?getNextWorkingDayFromPropFile(new Date()):getNextWorkingDayFromDB(new Date());	
		//String valueDate = "2015-12-23";
		BigDecimal bigDecimal = BigDecimal.ZERO;
		
		try{
			if(ddiList == null){
				throw new RuntimeException("ddiList is null");
			}else if (ddiList.size() == 0){
				throw new RuntimeException("ddiList len is 0");
			}
			
			//header
			fileContent.append(
					String.format("%s,%s,%s\n", 
							"H",
							"003",
							originatorAccountNumber
							)
					);
			
			
			int detailCount = 0;
			//detail
			for(DDIDto ddi : ddiList){
				
				if(ddi==null){
					System.out.println("dda is null");
					continue;
				}
				System.out.println("dda : " + ddi.toString());
				/*if(dda.getDrawerBankCode()==null || dda.getDrawerBankCode().trim().length() != 3 ){
					System.out.println("getDrawerBankCode error : " + dda.getDrawerBankCode());
					continue;
				}*/
				
				detailCount++;
				fileContent.append(
						String.format("%s,%s,%s,%s,%s,%s,%s,%s\n", 
								"D",//1.Identifier
								ddi.getBankCode()==null?"":ddi.getBankCode(),//2.Debtor Bank/ACH Code
								"",//3.Debtor Branch Code
								ddi.getAccountNumber()==null?"":ddi.getAccountNumber(),//4.Account Number
								ddi.getAccountName()==null?"":ddi.getAccountName(),//5.Account Name
								ddi.getCustomerId()==null?"":ddi.getCustomerId(),//6.Reference :: customerId
								ddi.getAmount()==null?"":ddi.getAmount(),//7.Amount : N(16,2)
								valueDate==null?"":valueDate//8.valueDate
								//""//9.Particulars
								)
						);
				
					
				bigDecimal = bigDecimal.add(ddi.getAmount());
			}
			
			//Trailer
			fileContent.append(
					String.format("%s,%s,%s,%s,%s\n", 
							"T",
							detailCount,
							0,
							bigDecimal.setScale(2).toPlainString(),
							0
							)
					);
			
			if(detailCount != ddiList.size())
				resultRemark += " Some item(s) are skiped! ";
			
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		//Creating the directory to store file
        //String rootPath = System.getProperty("catalina.home");
        File dir = new File("hkgta");
        if (!dir.exists())
            dir.mkdirs();

        // Create the file on server
        try{
        	ddiFile = new File(dir.getAbsolutePath()+ File.separator + fileName);
	        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(ddiFile));
	        stream.write(fileContent.toString().getBytes());
	        stream.close();
	        System.out.println("Server File Location=" + ddiFile.getAbsolutePath());
	        
	       
			try {
				SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
				sftp.upload(uploadPath, ddiFile.getName(), ddiFile);
			} catch (Exception e) {
				System.out.println("SftpException : " + e.getMessage());
			}
        }catch(IOException e) {
        	e.printStackTrace();
        }

        return ddiFile;
	}
	
	private void generateDDIReporter() {
		Date date =DateUtils.addMonths(new Date(),-1 )	;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		
		List<DDIDto> ddiList = ddiTransactionLogDao.queryDDITransactionList(year,month);
		if(ddiList != null && !ddiList.isEmpty()) {
			SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
			String filename = String.format("hkgta.%s.H2H-DDI-ICOL", sdf_yyyymmdd.format(new Date()));
			uploadDDIReport(ddiList,filename);
			
			DdInterfaceFileLog ddInterfaceFileLog = new DdInterfaceFileLog();
			ddInterfaceFileLog.setFilename(filename);
			ddInterfaceFileLog.setTransactionType("DDI");
			ddInterfaceFileLog.setStatus("PENDING");
			ddInterfaceFileLog.setProcessTimestamp(new Date());
			//ddInterfaceFileLog.setAckTimestamp(ackTimestamp); ???????
			ddInterfaceFileLog.setStatusUpdateBy("System Job");
			ddInterfaceFileLog.setStatusUpdateDate(new Date());
			ddInterfaceFileLogDao.save(ddInterfaceFileLog);
			
			for(DDIDto ddi: ddiList) {
				DdiTransactionLog ddiTransactionLog = new DdiTransactionLog();
				ddiTransactionLog.setTransactionNo(ddi.getTransactionNo());
				ddiTransactionLog.setDdInterfaceFileLog(ddInterfaceFileLog);
				ddiTransactionLogDao.save(ddiTransactionLog);
			}
		}
		
	}


	private void parseDDIRejectReporter(File file) {
		try {
			DDIReportDto rejectDDIReport = parseRejectReport(file);
			DdInterfaceFileLog ddInterfaceFileLog = new DdInterfaceFileLog();
			ddInterfaceFileLog.setFilename(file.getName());
			ddInterfaceFileLog.setTransactionType("DDI");
			ddInterfaceFileLog.setStatus("FAIL");
			ddInterfaceFileLog.setProcessTimestamp(new Date());
			ddInterfaceFileLog.setStatusUpdateBy("System Job");
			ddInterfaceFileLog.setStatusUpdateDate(new Date());
			//ddInterfaceFileLog.setAckTimestamp(ackTimestamp); ???????
			ddInterfaceFileLogDao.save(ddInterfaceFileLog);
			
			/*for(DDIReportDetailDto ddi: rejectDDIReport.getDetails()) {
				DdiTransactionLog ddiTransactionLog = new DdiTransactionLog();
				//ddiTransactionLog.setTransactionNo(ddi.getTransactionNo());
				ddiTransactionLog.setDdInterfaceFileLog(ddInterfaceFileLog);
				ddiTransactionLogDao.save(ddiTransactionLog);
			}*/
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	private void processDDIRejectReporter() {
		//file name regular 
		//String remoteFileNameReg = ".*RCMS_E_01\\..*";
		String ddiRejectRepoterFileReg = ddxProps.getProperty("ddiRejectRepoterFileReg");
		try{
			SftpByJsch sftp = new SftpByJsch(ftpHost,ftpUserName,ftpPassword,Integer.valueOf(port).intValue(),null,null,Integer.valueOf(timeout).intValue());
			List<File> files = sftp.dowloadByFileNameRegex(receivePath, ddiRejectRepoterFileReg, downLoadPath);
			for(File file:files) {
				try{
					parseDDIRejectReporter(file);
				} catch(RuntimeException e) {
					e.printStackTrace();
					ddxLogger.error("Happened Exception when parse file:" + file.getName());
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			ddxLogger.error("Happened Exception when process DDI reject reporter");
		}
	}
	
	@Transactional
	public void generateDDIReport() {
		generateDDIReporter();
	}
	
	@Transactional
	public void parseDDIRejectReporter(){
		System.out.println("debug dex log1");
		ddxLogger.error("debug dex log");
		processDDIRejectReporter();
	}

}
