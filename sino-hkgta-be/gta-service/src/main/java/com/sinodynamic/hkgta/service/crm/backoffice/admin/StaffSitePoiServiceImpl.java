package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.StaffSitePoiDao;
import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.dao.ibeacon.SitePoiDao;

@Service
public class StaffSitePoiServiceImpl extends ServiceBase<StaffSitePoiMonitor> implements StaffSitePoiService {

	@Autowired
	StaffSitePoiDao staffSitePoiDao;
	
	@Autowired
	SitePoiDao SitePoiDao;
	
	@Transactional
	public void saveStaffSitePoiMonitor(StaffSitePoiMonitor staffSitePoiMonitor)
			throws GTACommonException {
		StaffSitePoiMonitor rs = staffSitePoiDao.findByStaff(staffSitePoiMonitor.getStaffUserId());
		if (rs == null){
		    staffSitePoiDao.save(staffSitePoiMonitor);
		}
		else {
			rs.setSitePoiId(staffSitePoiMonitor.getSitePoiId());
			staffSitePoiDao.update(rs);
		}
	}
	
	@Transactional
	public List<StaffSitePoiMonitor> listByUserId(String userId)
			throws GTACommonException {
		StaffSitePoiMonitor sm = staffSitePoiDao.findByStaff(userId);
		List<StaffSitePoiMonitor> rs = new LinkedList<StaffSitePoiMonitor>();
		rs.add(sm);
		return rs;
	}


	@Transactional
	public List<StaffSitePoiMonitor> listBySiteId(Long sitePoiId)
			throws GTACommonException {
		return staffSitePoiDao.listBySiteId(sitePoiId);
	}

	@Transactional
	public void deleteById(Long sysId)
			throws GTACommonException {
		staffSitePoiDao.deleteById(StaffSitePoiMonitor.class, sysId);
	}

	@Transactional
	public List<SitePoi> listLocations() throws GTACommonException {
		return SitePoiDao.listSitePois();
	}
}
