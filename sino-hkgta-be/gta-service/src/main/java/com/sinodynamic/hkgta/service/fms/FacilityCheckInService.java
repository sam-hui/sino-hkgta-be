package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.dto.fms.FacilityCheckInDto;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import java.util.List;

public interface FacilityCheckInService {

	public FacilityCheckInDto<?> getCheckAvailability(long resvId, boolean isMember);

	public void checkIn(FacilityCheckInRequestDto requestDto, String userId, boolean isMember);

	public ResponseResult returnCheckedInFacilitiesByResvId(long resvId);

	public List<FacilityMasterDto> getFacilityAllotment(long resvId);
}
