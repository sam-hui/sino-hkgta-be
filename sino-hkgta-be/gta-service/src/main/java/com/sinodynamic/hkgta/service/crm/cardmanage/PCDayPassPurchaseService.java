package com.sinodynamic.hkgta.service.crm.cardmanage;


import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerOrderDetDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.GuestProfileDto;
import com.sinodynamic.hkgta.dto.crm.PaymentOrderDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderPermitCardDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface PCDayPassPurchaseService extends IServiceBase {

	boolean checkPurchaseRight(DaypassPurchaseDto dto);

	boolean checkPurchaseDays(DaypassPurchaseDto dto);

	ResponseResult checkAvailability(DaypassPurchaseDto dto);
	
	boolean changeDaypassCardStatus(CustomerOrderPermitCardDto dto);

	ResponseResult saveDayPassPurchase(DaypassPurchaseDto dto,String userId);
	
	DaypassPurchaseDto checkPurchasaeDayPassLimit(Member member);

	ResponseResult getInitialQueryResult(AdvanceQueryDto queryDto, String sql, ListPage page,String userId);


	ResponseResult purchaseDaypass(DaypassPurchaseDto dto,String userId);

	ResponseResult payDaypassPurchase(DaypassPurchaseDto dto,String userId,String purchasedLocationCode);

	boolean updateCustomerId2CustomerOrderPermitCard(PaymentOrderDto dto);

	ResponseResult payment(Long orderNo,String acdemyID,String id,String memberName,String cardNo) throws Exception;

	ResponseResult saveOrUpdateDayPassGuestInfo(GuestProfileDto guestProfileDto) throws Exception;
	
	ResponseResult searchDayPassById(String Id) throws Exception;


	boolean saveOrUpdateCustomerOrderPermitCard(CustomerOrderPermitCard card);
	
	public ResponseResult getMemberDayPass(String sql,String sortBy,String isAscending);
	
	public CustomerOrderPermitCard getDayPassCardByCardNo(String cardNo);

	/**   
	* @author: Zero_Wang
	* @since: Sep 9, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	DaypassPurchaseDto scanQRCode(ScanCardMappingDto cardMappingDto, Long orderNo);

	/**   
	* @author: Zero_Wang
	* @since: Sep 17, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	Map<String, CustomerOrderDetDto> getPurchaseDaypassList(Long orderNo);
	
	public void sendEmail(DaypassPurchaseDto dto,String userId);

	void sendCancelSMS(Long orderNo) throws Exception ;
	
	CustomerOrderHd getDaypassOrder(Long orderNo);
	
}
