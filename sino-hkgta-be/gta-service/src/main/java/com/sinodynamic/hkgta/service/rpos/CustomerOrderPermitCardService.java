package com.sinodynamic.hkgta.service.rpos;

import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CustomerOrderPermitCardService extends IServiceBase<CustomerOrderPermitCard> {

	/**   
	* @author: Zero_Wang
	* @since: Sep 7, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	void updateCustomerOrderPermitCardStatus();

}
