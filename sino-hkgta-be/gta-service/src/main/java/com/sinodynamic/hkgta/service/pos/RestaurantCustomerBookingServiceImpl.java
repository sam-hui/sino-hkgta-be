package com.sinodynamic.hkgta.service.pos;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantBookingQuotaDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantCustomerBookingDao;
import com.sinodynamic.hkgta.dto.pos.RestaurantCustomerBookingDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RestaurantCustomerBookingStatus;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RestaurantCustomerBookingServiceImpl extends ServiceBase<RestaurantCustomerBooking> implements
		RestaurantCustomerBookingService {
	@Autowired
	private RestaurantCustomerBookingDao restaurantCustomerBookingDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private PMSApiService pMSService;
	
	@Autowired
	private RestaurantMasterService restaurantMasterService;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private ShortMessageService  smsService;
	
	@Override
	@Transactional
	public ResponseResult saveRestaurantCustomerBooking(String createBy,RestaurantCustomerBookingDto bookingDto) throws Exception
	{
		Member member = memberDao.getMemberByCustomerId( bookingDto.getCustomerId());
		if(member != null && member.getStatus().equals(Constant.General_Status_NACT)){
			responseResult.initResult(GTAError.FacilityError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, bookingDto.getCustomerId());
		if(null != customerProfile){
			RestaurantCustomerBooking booking = new RestaurantCustomerBooking();
			RestaurantMaster restaurantMaster  = restaurantMasterService.getRestaurantMaster(bookingDto.getRestaurantId());
			if(null == restaurantMaster)responseResult.initResult(GTAError.RestaurantError.RESTAURANT_ID_INVALID);
			booking.setRestaurantMaster(restaurantMaster);;
			booking.setCustomerProfile(customerProfile);
			booking.setBookTime(DateCalcUtil.parseDateTime(bookingDto.getBookTime()));
			booking.setPartySize(bookingDto.getPartySize());
			booking.setStatus(RestaurantCustomerBookingStatus.CFM.name());
			booking.setBookVia(bookingDto.getBookVia());
			booking.setRemark(bookingDto.getRemark());
			booking.setCreateBy(createBy);
			booking.setCreateDate(new Timestamp(new Date().getTime()));
			restaurantCustomerBookingDao.save(booking);
			RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
			responseResult.initResult(GTAError.Success.SUCCESS,result);
		}else{
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
		}
		
		return responseResult;
	}

	private RestaurantCustomerBookingDto createRestaurantCustomerBookingDto(RestaurantCustomerBooking booking)
	{
		CustomerProfile customerProfile = booking.getCustomerProfile();
		Member member = memberDao.get(Member.class, customerProfile.getCustomerId());
		RestaurantCustomerBookingDto queryDto = new RestaurantCustomerBookingDto(); 
		queryDto.setResvId(booking.getResvId());
		queryDto.setRestaurantId(booking.getRestaurantMaster().getRestaurantId());
		queryDto.setBookTime(DateCalcUtil.formatDatetime(booking.getBookTime()));
		queryDto.setCustomerId(booking.getCustomerProfile().getCustomerId());
		queryDto.setBookVia(booking.getBookVia());
		queryDto.setPartySize(booking.getPartySize());
		queryDto.setRemark(booking.getRemark());
		queryDto.setStatus(booking.getStatus());
		queryDto.setAcademyNo(member.getAcademyNo());
		queryDto.setMemberName(customerProfile.getSalutation()+" "+ customerProfile.getGivenName() + " " +customerProfile.getSurname());
		return queryDto;
	}
	
	@Override
	@Transactional
	public ResponseResult updateRestaurantCustomerBooking(String updateBy,RestaurantCustomerBookingDto bookingDto) throws Exception
	{
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, bookingDto.getResvId().longValue());
		if(null != booking){
			booking.setBookTime(DateCalcUtil.parseDateTime(bookingDto.getBookTime()));
			booking.setPartySize(bookingDto.getPartySize());
			booking.setRemark(bookingDto.getRemark());
			booking.setUpdateBy(updateBy);
			booking.setUpdateDate(new Timestamp(new Date().getTime()));
			restaurantCustomerBookingDao.update(booking);
			RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
			responseResult.initResult(GTAError.Success.SUCCESS,result);
		}
		return responseResult;
	}
	@Override
	@Transactional
	public RestaurantCustomerBookingDto getRestaurantCustomerBookingByResvId(Long resvId)
	{
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
		return result;
	}
	
	@Override
	@Transactional
	public RestaurantCustomerBooking getRestaurantCustomerBooking(Long resvId)
	{
		return restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
	}
	
	@Override
	@Transactional
	public ResponseResult cancelRestaurantCustomerBooking(String updateBy, Long resvId) throws Exception
	{
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if(null != booking){
			booking.setStatus(RestaurantCustomerBookingStatus.DEL.name());
			booking.setUpdateBy(updateBy);
			booking.setUpdateDate(new Timestamp(new Date().getTime()));
			restaurantCustomerBookingDao.update(booking);
			RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
			responseResult.initResult(GTAError.Success.SUCCESS,result);
		}
		return responseResult;
	}
	@Override
	@Transactional
	public int updateRestaurantCustomerBookingExpired() throws Exception
	{
		return restaurantCustomerBookingDao.updateRestaurantCustomerBookingExpired();
	}
	
	@Override
	@Transactional
	public List<RestaurantCustomerBooking> getRestaurantCustomerBookingConfirmedList()
	{
		return restaurantCustomerBookingDao.getRestaurantCustomerBookingConfirmedList();
	}
	
	@Override
	@Transactional
	public void sendRestaurantBookingSMS(Long resvId,String functionId) throws Exception
	{
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if(null != booking){
			CustomerProfile customerProfile = booking.getCustomerProfile();
			if (customerProfile != null) {
				String mobilePhone = customerProfile.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<String>();
				phonenumbers.add(mobilePhone);
				MessageTemplate messageTemplate = messageTemplateDao.getTemplateByFunctionId(functionId);
				if(null != messageTemplate){
					String content = messageTemplate.getContent();
					String message = content.replace("{timeLeft}", 120+"")
							.replace("{bookingTime}", DateCalcUtil.formatDatetime(booking.getBookTime()))
							.replace("{restaurantName}", booking.getRestaurantMaster().getRestaurantName())
							.replace("{partySize}",booking.getPartySize().toString());
					smsService.sendSMS(phonenumbers, message, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
				}
			}
		}
	}
}
