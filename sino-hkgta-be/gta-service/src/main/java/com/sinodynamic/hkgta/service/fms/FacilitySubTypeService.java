package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilitySubtypeDto;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilitySubTypeService extends IServiceBase<FacilitySubType> {

	List<FacilitySubtypeDto> getFacilitySubType(String facilityType,String subType);
	
	List<FacilitySubType> getFacilitySubTypeByFacilityType(String facilityType);
	
	FacilitySubType getFacilitySubTypeBySubTypeId(String subTypeId);
}
