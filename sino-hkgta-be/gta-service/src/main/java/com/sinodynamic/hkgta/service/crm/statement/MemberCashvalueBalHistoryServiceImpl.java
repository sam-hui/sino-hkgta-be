package com.sinodynamic.hkgta.service.crm.statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.DeliveryRecordDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashvalueBalHistoryDao;
import com.sinodynamic.hkgta.dto.statement.SearchStatementsDto;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class MemberCashvalueBalHistoryServiceImpl extends ServiceBase<MemberCashvalueBalHistory> implements
		MemberCashvalueBalHistoryService {
	
	@Autowired
	private MemberCashvalueBalHistoryDao memberCashvalueBalHistoryDao;
	
	@Autowired
	private DeliveryRecordDao deliveryRecordDao;
	
	@Override
	@Transactional
	public ResponseResult getStatements(ListPage page, SearchStatementsDto dto) {
		ListPage<MemberCashvalueBalHistory> listPage = memberCashvalueBalHistoryDao.getStatements(page,dto);
		Data data = new Data();
		data.setList(listPage.getDtoList());
		data.setTotalPage(page.getAllPage());;
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

}
