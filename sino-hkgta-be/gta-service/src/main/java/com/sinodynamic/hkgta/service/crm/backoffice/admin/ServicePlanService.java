package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.ListPageDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.dto.membership.FacilityTransactionInfo;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassBackDto;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface ServicePlanService extends IServiceBase<ServicePlan>{

	public abstract ResponseMsg saveServicePlan(ServicePlanDto servicePlanJson);

	public abstract List<ServicePlanDto> getAllServicPlan();

	public abstract ResponseMsg updateServicePlan(ServicePlanDto servicePlanJson);

	public abstract ServicePlanDto getServicPlan(ServicePlanDto key);
	
	public abstract ResponseMsg deleteServicPlan(Long planNo);
	
	public ResponseMsg duplicateServicePlan(Long planNo,String userId);

	public abstract ResponseMsg changeServicePlanStatus(Long planNo,String status,String userId);

	public abstract ServicePlanDto getServicPlanById(Long planNo);

	public abstract ResponseResult getPageList(ListPageDto planPageDto);
	
	public boolean isServicePlanUsed(Long planNo, String paymentStatus);
	public boolean isServicePlanUsed(Long planNo);
	public boolean isDaypassUsed(Long planNo);
	
	public ResponseResult getDayPassPageList(DaypassDto dto);

	public abstract ResponseMsg saveDayPass(ServicePlanDto servicePlanJson);

	public ResponseResult changeDayPassStatus(ServicePlanDto dto);

	public  ResponseResult setDayPassGlobalQuota(String paramValue);

	public abstract ServicePlanDto getDaypassById(Long planNo);

	public abstract ResponseMsg duplicateDaypass(Long planNo,String userId);

	public abstract ResponseMsg deleteDaypass(Long planNo);

	public abstract ResponseMsg updateDaypass(ServicePlanDto servicePlanJson);

	public ResponseResult getPurchasedDaypassList(PurchasedDaypassDto purchasedDaypassDto);

	public abstract ResponseResult getServiceplanByDate(DaypassPurchaseDto dto) throws ParseException;

	public abstract ResponseResult verifyPaymentItemNo(Long orderDetId);

	public abstract List<ServicePlanDto> getValidServicPlan();
	
	public ResponseResult canclePurchaseDaypass(Long orderNo, String userId,String remark,String requesterType,boolean refundFlag);
	
	public abstract void deactivateServicePlans();

	public ServicePlan get(Long  planNo);

	boolean checkServicePlanByName(String planName, String passNatureCode,Long  planNo);
	
	public boolean checkServicePlanForSaveByName(String planName,String passNatureCode);

	public abstract List<ServicePlanDto> getValidDaypass();

	public abstract void autoDisposeDaypassCard();

	public abstract List<FacilityTransactionInfo> getTransactionInfoByOrderNo(
			Long orderNo);

	public abstract String getServicePlans(Map<String,org.hibernate.type.Type> typeMap);

	public abstract String getPurchasedDaypassListSQL(
			PurchasedDaypassDto purchasedDaypassDto);

	public abstract ResponseResult cancleUnpayedOrder(Long orderNo);

	public abstract PurchasedDaypassBackDto addTranscationFlag(
			PurchasedDaypassBackDto purchasedDaypassBackDto);
	
	Data getDailyDayPassUsage(String date, String orderColumn,
			String order, int pageSize, int currentPage, AdvanceQueryDto filters);
	
	byte[] getDailyDayPassUsageAttachment(String date, String fileType, String sortBy, String isAscending);
	
	public List<ServicePlan> getValidServicPlanForCorporateMember();
}