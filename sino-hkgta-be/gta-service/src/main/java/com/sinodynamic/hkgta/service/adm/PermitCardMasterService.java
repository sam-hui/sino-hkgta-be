package com.sinodynamic.hkgta.service.adm;

/*
 * @Author Becky
 * @Date 4-29
*/

import com.sinodynamic.hkgta.dto.crm.PermitCardMasterDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface PermitCardMasterService extends IServiceBase<PermitCardMaster> {
	
	public String checkPermitCardStatus(Long enrollId);
	
	public Boolean changePermitCardMasterStatus(String cardNo, PermitCardMasterDto param) throws Exception;
	
	public String replaceOldPermitCardMaster(String customerId, PermitCardMasterDto param) throws Exception;
	
	public PermitCardMaster getPermitCardMaster(String cardNo) throws Exception;
	
	public ResponseResult linkCard(String cardNo, PermitCardMasterDto dto, String createBy) throws Exception;

	void disposeCard(String cardNo, String updateBy) throws Exception;

	void returnCard(String cardNo, String updateBy) throws Exception;

	void disposeCardforDayPassByOrderNo(String orderNo, String updateBy);
	
	public ResponseResult getPreviewCardInfo(Long customerId);

}
