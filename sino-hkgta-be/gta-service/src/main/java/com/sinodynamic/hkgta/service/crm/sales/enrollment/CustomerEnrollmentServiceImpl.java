package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.jsoup.helper.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAddressDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollPoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentLogDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.HomePageSummaryDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MemberTypeDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.RemarksDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanPosDao;
import com.sinodynamic.hkgta.dao.crm.VirtualAccPoolDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.MessageTemplateDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;
import com.sinodynamic.hkgta.entity.crm.CustomerAddressPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPo;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollmentLog;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribePK;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.crm.VirtualAccPool;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.AddressType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.constant.ServicePlanRightTypeStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.pagination.ListPage.OrderType;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CustomerEnrollmentServiceImpl extends ServiceBase<CustomerEnrollment> implements CustomerEnrollmentService {
	
	public Logger logger = Logger.getLogger(CustomerEnrollmentServiceImpl.class);
	
	private static final Serializable[] pengdingMemberList = { "New", "Active(No Card)", "Payment Fail" };
	private static final Serializable[] activatedMemberList = { "Complete" };
	private static final Serializable[] deactivatedMemberList = { "Reject", "Cancel" };
	private static final String PENDING = "p";
	private static final String ACTIVATED = "a";
	private static final String DEACTIVATED = "d";
	private static final String STAFF = "s";
	private static final String CUSTOMER = "c";
	private static final String AcademnyID = "[0-9]{7}";
	private static final String defUserType = "CUSTOMER";
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;

	@Autowired
	private UserMasterDao userMasterDao;

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private MemberTypeDao memberTypeDao;

	@Autowired
	private CustomerServiceDao customerServiceDao;

	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private CustomerEnrollPoDao customerEnrollPoDao;

	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;

	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;

	@Autowired
	private HomePageSummaryDao homePageSummaryDao;

	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;

	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;

	@Autowired
	private ServicePlanPosDao servicePlanPosDao;

	@Autowired
	private PosServiceItemPriceDao pServiceItemPriceDao;

	@Autowired
	private CustomerOrderDetDao cOrderDetDao;

	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;

	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;

	@Autowired
	private ServicePlanDao servicePlanDao;

	@Autowired
	private RemarksDao remarksDao;

	@Autowired
	private VirtualAccPoolDao virtualAccPoolDao;

	@Autowired
	private MemberCashValueDao memberCashValueDao;

	@Autowired
	private GlobalParameterDao globalParameterDao;

	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;

	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;

	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;

	@Autowired
	private CustomerAddressDao CustomerAddressDao;

	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;
	
	@Autowired
	private CustomerEnrollmentLogDao customerEnrollmentLogDao;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	
	@Autowired
	private CorporateMemberDao corporateMemberDao;

	@Autowired
	private SpaMemberSyncDao spaMemberSyncDao;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;

	public void addCustomerEnrollment(CustomerEnrollment ce) {
		customerEnrollmentDao.addCustomerEnrollment(ce);
	}

	public void deleteCustomerEnrollment(CustomerEnrollment ce) {

	}

	public void updateCustomerEnrollment(CustomerEnrollment cp) {

	}
	@Transactional
	public ListPage<CustomerEnrollment> searchAllCustomerEnrollments(ListPage<CustomerEnrollment> page) {

		return customerEnrollmentDao.selectAllCustomerEnrollments(page);
	}

	public ListPage<CustomerEnrollment> searchCustomerEnrollmentInfoByName(ListPage<CustomerEnrollment> page,
			String operType, String name) {

		ListPage<CustomerEnrollment> customerEnrollments = null;

		if (STAFF.equals(operType)) {
			customerEnrollments = customerEnrollmentDao.selectCustomerEnrollmentInfoByStaffName(page, name);
		}
		if (CUSTOMER.equals(operType)) {
			customerEnrollments = customerEnrollmentDao.selectCustomerEnrollmentInfoByCustomerName(page, name);
		}

		return customerEnrollments;

	}

	@Transactional
	public boolean checkAvailableAcademyID(String academyId) {
		if (!academyId.matches(AcademnyID)) {

			return false;
		}

		return memberDao.validateAcademyID(academyId);

	}

	@Transactional
	public ResponseResult reserveAcademId(MemberDto dto, String userId) {
		Long customerId = dto.getCustomerId();
		String academyNo = dto.getAcademyNo();
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if (customerEnrollment != null && !EnrollStatus.NEW.name().equals(customerEnrollment.getStatus())){
			responseResult.initResult(GTAError.EnrollError.ONLY_NEW_RESERVE);
			return responseResult;
		}
		if (!StringUtils.isEmpty(academyNo)&& !academyNo.matches("[0-9]{7}")){
			responseResult.initResult(GTAError.EnrollError.ACADEMY_NOT_INVALID,new String[]{academyNo});
			return responseResult;
		}
		boolean check = memberDao.validateAcademyID(academyNo);
		if (!check){
			responseResult.initResult(GTAError.EnrollError.ACADEMY_USED, new String[]{academyNo});
			return responseResult;
		}
		Member checkMember = memberDao.getMemberById(customerId);
		if (checkMember != null && checkMember.getAcademyNo() != null){
			responseResult.initResult(GTAError.EnrollError.ACADEMY_ALLOCATED);
			return responseResult;
		}
		if (checkMember == null) {
			Member member = new Member();
			member.setAcademyNo(academyNo);
			member.setCustomerId(customerId);
			member.setMemberType(MemberType.IPM.name());
			member.setInternalRemark("Reserved Academy No for Individual Primary Member.");
			member.setCreateDate(new Date());
			member.setCreateBy(userId);
			memberDao.save(member);
		} else {
			checkMember.setAcademyNo(academyNo);
			checkMember.setInternalRemark("Reserved Academy No for Individual Primary Member.");
			memberDao.updateMemberer(checkMember);
		}
		Member returnMember = memberDao.getMemberById(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS);
		responseResult.setData(returnMember.getAcademyNo());
		return responseResult;
	}

	@Transactional
	public ResponseResult removeReservedAcademyId(Long customerId) {
		Member member = memberDao.getMemberByCustomerId(customerId);
		member.setAcademyNo(null);
		memberDao.updateMemberer(member);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Transactional
	public ResponseResult getDependentBySuperiorId(Long superiorCustomerId) {
		List<DependentMemberInfoDto> list = customerProfileDao
				.getDependentMemberInfoListBySuperiorId(superiorCustomerId);
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}

	@Transactional
	public boolean setAcademyID(String academyId, Long customerId) {

		return customerEnrollmentDao.setAcademyID(academyId, customerId);

	}

	@Transactional
	public ListPage<CustomerEnrollment> viewEnrollmentActivationHistory(ListPage<CustomerEnrollment> page, String type) {

		ListPage<CustomerEnrollment> memberEnrollInfos = null;

		if (PENDING.equals(type)) {
			memberEnrollInfos = customerEnrollmentDao.viewEnrollmentActivationOfMember(page, pengdingMemberList);
		}
		if (ACTIVATED.equals(type)) {
			memberEnrollInfos = customerEnrollmentDao.viewEnrollmentActivationOfMember(page, activatedMemberList);
		}
		if (DEACTIVATED.equals(type)) {
			memberEnrollInfos = customerEnrollmentDao.viewEnrollmentActivationOfMember(page, deactivatedMemberList);
		}

		return memberEnrollInfos;

	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseResult regEnrollment(CustomerProfile dto, String userId) throws Exception {
		logger.debug("regEnrollment method :Passport No = " + dto.getPassportNo());

		Long superiorMemberId = dto.getSuperiorMemberId();
		CustomerEnrollment enrollSuperior = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(superiorMemberId);
		if (superiorMemberId != null && enrollSuperior != null
				&& !EnrollStatus.NEW.name().equals(enrollSuperior.getStatus())
				&& !EnrollStatus.APV.name().equals(enrollSuperior.getStatus())
				&& !EnrollStatus.PYA.name().equals(enrollSuperior.getStatus())
				&& !EnrollStatus.TOA.name().equals(enrollSuperior.getStatus())){
			responseResult.initResult(GTAError.EnrollError.ONLY_NEW_APV_PYA_TOA_CREATE_DEP_INFO);
			return responseResult;
		}
		if (dto == null || dto.getCustomerEnrollments() == null || dto.getCustomerEnrollments().size() == 0) {
			responseResult.initResult(GTAError.EnrollError.INCORRECT_JSON_INPUT);
			return responseResult;
		}
		ResponseResult msg = checkInputData(dto,null,superiorMemberId);
		if (msg.getReturnCode() != "0") {
			return msg;
		}
		
		boolean checkForID = customerProfileDao.checkAvailablePassportNo(null, dto.getPassportType(),
				dto.getPassportNo());
		if (!StringUtils.isEmpty(dto.getPassportType()) && !StringUtils.isEmpty(dto.getPassportNo()) && !checkForID) {
			throw new GTACommonException(GTAError.EnrollError.ID_EXIST_IN_REG,new Object[]{dto.getPassportType(),dto.getPassportNo()});
		}
		
		dto.setCreateDate(new Date());
		dto.setCreateBy(userId);
		dto.setIsDeleted(Constant.DELETE_NO);
		//Images will be saved by using individual method
		String portraitPhoto = dto.getPortraitPhoto();
		String signature = dto.getSignature();
		dto.setPortraitPhoto(null);
		dto.setSignature(null);
		//add company name
		if (dto.getCustomerAdditionInfos() !=null && dto.getCustomerAdditionInfos().size() > 0) {
			for (CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()) {
				if (cust.getCaptionId() != null && Integer.parseInt(cust.getCaptionId()) == 14) {
					dto.setCompanyName(cust.getCustomerInput());
					break;
				}
			}
		}
		Long customerId = (Long) customerProfileDao.save(dto);
		dto.setCustomerId(customerId);
		if (dto.getCustomerAdditionInfos() != null && dto.getCustomerAdditionInfos().size() > 0) {
			for (CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()) {
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.parseLong(cust.getCaptionId()));
				cpk.setCustomerId(customerId);
				cust.setId(cpk);
				cust.setCreateDate(new Date());
				cust.setCreateBy(userId);
				customerAdditionInfoDao.saveCustomerAdditionInfo(cust);
			}
		}

		if (superiorMemberId == null && dto.getCustomerEnrollments() != null
				&& dto.getCustomerEnrollments().get(0) != null) {
			CustomerEnrollment enrollment = dto.getCustomerEnrollments().get(0);
			enrollment.setCustomerId(customerId);
			enrollment.setCreateDate(new Date());
			enrollment.setCreateBy(userId);
			enrollment.setEnrollDate(new Date());
			enrollment.setEnrollType(com.sinodynamic.hkgta.util.constant.MemberType.IPM.getType());
			enrollment.setStatus(EnrollStatus.NEW.name());
			ServicePlan plan = servicePlanDao.get(ServicePlan.class, enrollment.getSubscribePlanNo());
			if (plan != null && plan.getContractLengthMonth() != null) {
				enrollment.setSubscribeContractLength(Long.parseLong(plan.getContractLengthMonth() + ""));
			}
			customerEnrollmentDao.addCustomerEnrollment(enrollment);
		}
		if (dto.getCustomerAddresses() != null && dto.getCustomerAddresses().get(0) != null) {
			CustomerAddress address = dto.getCustomerAddresses().get(0);
			CustomerAddressPK apk = new CustomerAddressPK();
			apk.setAddressType(AddressType.BILL.name());
			apk.setCustomerId(customerId);
			address.setId(apk);
			if ("true".equalsIgnoreCase(dto.getCheckBillingAddress())) {
				address.setAddress1(dto.getPostalAddress1());
				address.setHkDistrict(dto.getPostalDistrict());
			}
			CustomerAddressDao.save(address);
		}

		if (enrollSuperior != null) {
			if (EnrollStatus.APV.name().equals(enrollSuperior.getStatus())||EnrollStatus.PYA.name().equals(enrollSuperior.getStatus())
					||EnrollStatus.TOA.name().equals(enrollSuperior.getStatus())) {
				setMember(customerId, getNewAcademyNo(), superiorMemberId, userId,dto.getRelationshipCode());
			} else if (EnrollStatus.NEW.name().equals(enrollSuperior.getStatus())) {
				setMember(customerId, null, superiorMemberId, userId,dto.getRelationshipCode());
			}
		} else {
			setMember(customerId, null, superiorMemberId, userId,dto.getRelationshipCode());
		}
		
		//Record the customer enrollment log if the customer is a IPM
		if(superiorMemberId == null){
			recordCustomerEnrollmentUpdate(null,EnrollStatus.NEW.name(),customerId);
		}
		CustomerProfile returnCustomerProfile = new CustomerProfile();
		returnCustomerProfile.setCustomerId(customerId);
		returnCustomerProfile.setPortraitPhoto(portraitPhoto);
		returnCustomerProfile.setSignature(signature);
		responseResult.initResult(GTAError.Success.SUCCESS,returnCustomerProfile);
		return responseResult;

	}

	@SuppressWarnings("unused")
	@Transactional(rollbackFor = Exception.class)
	public ResponseMsg editEnrollment(CustomerProfile dto, String updateBy,String fromName) throws Exception {
		logger.debug("editEnrollment method :Customer Id = " + dto.getCustomerId());
		Date currentDate = new Date();
		Long customerId = dto.getCustomerId();
		if (dto.getCustomerId() == null){
			responseMsg.initResult(GTAError.EnrollError.CUSTOMER_ID_REQ);
			return responseMsg;
		}
		if (dto == null || dto.getCustomerId() == null) {
			responseMsg.initResult(GTAError.EnrollError.INCORRECT_JSON_INPUT);
			return responseMsg;
		}
		Member member = memberDao.get(Member.class, dto.getCustomerId());
		String memberType =null;
		if(member!=null){
			memberType = member.getMemberType();
		}
		ResponseMsg msg = checkInputData(dto,memberType,null);
		if (msg.getReturnCode() != "0") {
			return msg;
		}
		
		CustomerProfile profEntity = customerProfileDao.get(CustomerProfile.class, dto.getCustomerId());
		
		if (member != null && null != member.getAcademyNo()) {
			spaMemberSyncDao.addSpaMemberSyncWhenUpdate(profEntity, dto);
		}
		
		CustomerEnrollment enroll = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(dto.getCustomerId());
		// ONLY STATUS = OPN, NEW, APV, PYA, TOA CAN UPDATE THE CONTACT EMAIL BECAUSE THE LOGINID IS NOT GENERATED AT THIS MOMENT
		if (enroll != null && !EnrollStatus.OPN.name().equals(enroll.getStatus())
				&& !EnrollStatus.NEW.name().equals(enroll.getStatus())
				&& !EnrollStatus.APV.name().equals(enroll.getStatus())
				&& !EnrollStatus.PYA.name().equals(enroll.getStatus())
				&& !EnrollStatus.TOA.name().equals(enroll.getStatus())
				&&!profEntity.getContactEmail().equalsIgnoreCase(dto.getContactEmail())) {
			responseMsg.initResult(GTAError.EnrollError.UPDATE_NOTALLOW_FOR_ACTIVATED);
			return responseMsg;
		}
		
		String[] ignoreFiledsForCustomerProfile = new String[]{"portraitPhoto","signature","createBy","isDeleted","version","createDate"};

		dto.setUpdateDate(new Date());
		dto.setUpdateBy(updateBy);
		if (StringUtils.isEmpty(dto.getContactClassCode())) {
			dto.setContactClassCode(profEntity.getContactClassCode());
		}
		if (StringUtils.isEmpty(dto.getInternalRemark())) {
			dto.setInternalRemark(profEntity.getInternalRemark());
		}

		

		boolean checkForID = customerProfileDao.checkAvailablePassportNo(dto.getCustomerId(), dto.getPassportType(),
				dto.getPassportNo());
		if (!StringUtils.isEmpty(dto.getPassportType()) && !StringUtils.isEmpty(dto.getPassportNo()) && !checkForID) {
			throw new GTACommonException(GTAError.EnrollError.ID_EXIST_IN_EDIT,new Object[]{PassportType.VISA.name().equals(dto.getPassportType())?"Passport":dto.getPassportType(),dto.getPassportNo()});
		}
		
		BeanUtils.copyProperties(dto, profEntity,ignoreFiledsForCustomerProfile);
		customerProfileDao.getCurrentSession().evict(profEntity);
		profEntity.setDateOfBirth(DateConvertUtil.parseString2Date(dto.getDateOfBirth(),"yyyy-MM-dd"));
		//add company name for customer profile
		if (dto.getCustomerAdditionInfos() != null && dto.getCustomerAdditionInfos().size() > 0) {
			for (CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()) {
				if (cust.getCaptionId() != null && Integer.parseInt(cust.getCaptionId()) == 14) {
					profEntity.setCompanyName(cust.getCustomerInput());
					break;
				}
			}
		}
		
		customerProfileDao.update(profEntity);
		if (dto.getCustomerAdditionInfos() != null && dto.getCustomerAdditionInfos().size() > 0) {
			for (CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()) {
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.parseLong(cust.getCaptionId()));
				cpk.setCustomerId(dto.getCustomerId());
				cust.setId(cpk);
				CustomerAdditionInfo targetEntity = customerAdditionInfoDao.get(CustomerAdditionInfo.class, cpk);
				if (targetEntity != null) {
					cust.setUpdateBy(updateBy);
					cust.setUpdateDate(new Date());
					String[] ignoreFileds = new String[] { "id", "createBy", "createDate","version"};
					BeanUtils.copyProperties(cust, targetEntity, ignoreFileds);
					customerAdditionInfoDao.getCurrentSession().evict(targetEntity);
					customerAdditionInfoDao.update(targetEntity);
				} else {
					cust.setCreateBy(updateBy);
					cust.setCreateDate(new Date());
					customerAdditionInfoDao.save(cust);
				}
			}
		}

		
		// used to enroll a Lead (Open)
		if (member == null && enroll != null && EnrollStatus.OPN.name().equals(enroll.getStatus())) {
			Member m = new Member();
			m.setCustomerId(dto.getCustomerId());
			m.setAcademyNo(null);
			m.setMemberType(Constant.memberType.IPM.name());
			m.setStatus(Constant.Status.NACT.name());
			m.setFirstJoinDate(currentDate);
			m.setEffectiveDate(null);
			m.setCreateDate(currentDate);
			m.setCreateBy(updateBy);
			m.setUpdateDate(currentDate);
			m.setUpdateBy(updateBy);
			m.setSuperiorMemberId(null);
			m.setRelationshipCode(dto.getRelationshipCode());
			memberDao.addMember(m);
		}
		
		//used to insert/update the relationshop for IDM
		if(member!=null&&MemberType.IDM.name().equals(member.getMemberType())){
			boolean updateRelation = memberDao.updateRelationship(dto.getRelationshipCode(), dto.getCustomerId(), updateBy);
		}

		if (enroll != null && EnrollStatus.OPN.name().equals(enroll.getStatus())
				|| dto.getCustomerEnrollments() != null && dto.getCustomerEnrollments().get(0) != null
				&& member != null && !Constant.memberType.IDM.name().equals(member.getMemberType())
				&& !Constant.memberType.CDM.name().equals(member.getMemberType())) {

			String status = enroll.getStatus();
			CustomerEnrollment enrollment = dto.getCustomerEnrollments().get(0);
			// ONLY STATUS = NEW OR OPN CAN UPDATE OR CHOOSE SERVICE PLAN
			if (EnrollStatus.NEW.name().equals(status) || EnrollStatus.OPN.name().equals(status)) {
				enrollment.setCustomerId(dto.getCustomerId());
				enrollment.setInternalRemark(dto.getInternalRemark());
				enrollment.setUpdateDate(new Date());
				enrollment.setUpdateBy(updateBy);
				ServicePlan plan = servicePlanDao.get(ServicePlan.class, enrollment.getSubscribePlanNo());
				if (plan != null && plan.getContractLengthMonth() != null) {
					enrollment.setSubscribeContractLength(Long.parseLong(plan.getContractLengthMonth() + ""));
				}
				CustomerEnrollment targetEntity = customerEnrollmentDao.get(CustomerEnrollment.class,
						enrollment.getEnrollId());
				String[] ignoreFileds = new String[] { "createBy", "createDate", "enrollId", "customerId",
						"enrollDate", "enrollType", "status", "salesFollowBy" };
				BeanUtils.copyProperties(enrollment, targetEntity, ignoreFileds);
				// first enroll from lead to enrollment
				if (targetEntity.getEnrollDate() == null) {
					targetEntity.setEnrollDate(new Date());
				}
				if (StringUtils.isEmpty(targetEntity.getEnrollType())) {
					targetEntity.setEnrollType(com.sinodynamic.hkgta.util.constant.MemberType.IPM.getType());
				}
				//used to enroll a Lead (Open)
				if (EnrollStatus.OPN.name().equals(targetEntity.getStatus())) {
					recordCustomerEnrollmentUpdate(targetEntity.getStatus(),EnrollStatus.NEW.name(),targetEntity.getCustomerId());
					targetEntity.setStatus(EnrollStatus.NEW.name());
				}
				customerEnrollmentDao.saveOrUpdate(targetEntity);
			}
		}

		//used to update the addresses
		if (dto.getCustomerAddresses() != null && dto.getCustomerAddresses().get(0) != null) {
			for (CustomerAddress address : dto.getCustomerAddresses()) {
				CustomerAddressPK apk = new CustomerAddressPK();
				apk.setAddressType(address.getAddressType());
				apk.setCustomerId(dto.getCustomerId());
				address.setId(apk);
				if ("true".equalsIgnoreCase(dto.getCheckBillingAddress())) {
					address.setAddress1(dto.getPostalAddress1());
					address.setAddress2(dto.getPostalAddress2());
					address.setHkDistrict(dto.getPostalDistrict());
				}
				CustomerAddress targetEntity = CustomerAddressDao.get(CustomerAddress.class, apk);
				if (targetEntity != null) {
					String[] ignoreFileds = new String[] { "id" };
					BeanUtils.copyProperties(address, targetEntity, ignoreFileds);
					CustomerAddressDao.saveOrUpdate(targetEntity);
				} else {
					CustomerAddressDao.save(address);
				}
			}
		}

		
		//Re-enroll the canceled or rejected IPM to IDM
		if(dto.getRelationshipCode()!=null&&member!=null&&"true".equalsIgnoreCase(dto.getImportStatus()) && MemberType.IPM.name().equals(member.getMemberType())){
			Long superiorMemberId = dto.getSuperiorMemberId();
			CustomerEnrollment superiorEnroll = null;
			if (!StringUtils.isEmpty(superiorMemberId))
				superiorEnroll = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(superiorMemberId);
			if (superiorEnroll == null) throw new GTACommonException(GTAError.EnrollError.SUPERIOR_MEMBER_NOT_FOUND);
				
			String superiorEnrollStatus = superiorEnroll.getStatus();
			
			Member primaryMember = memberDao.getMemberById(superiorMemberId);
			String randomPassword = CommUtil.generateRandomPassword();
			if(EnrollStatus.ANC.name().equals(superiorEnrollStatus) ||EnrollStatus.CMP.name().equals(superiorEnrollStatus)){
				String academyNo = member.getAcademyNo();
				if (academyNo == null) {
					academyNo = getTheNextAvailableAcademyNo();
				}
				String userId = getTheUserId(academyNo, dto.getCustomerId());
				boolean checkLoginId = userMasterDao.checkAvailableLoginId(academyNo, userId);
				if (!checkLoginId) throw new GTACommonException(GTAError.EnrollError.LOGIN_ID_IS_NOT_AV_IN_EDIT);
				UserMaster user = createUserMaster(academyNo, customerId, updateBy, dto.getSurname(), dto.getGivenName(),randomPassword);
				userMasterDao.save(user);
				member.setUserId(userId);
				member.setAcademyNo(academyNo);
				member.setEffectiveDate(primaryMember.getEffectiveDate());
			}else{
				member.setAcademyNo(null);
			}
			
			member.setMemberType("IDM");
			member.setStatus(Constant.General_Status_ACT);
			member.setFirstJoinDate(currentDate);
			member.setUpdateDate(currentDate);
			member.setUpdateBy(updateBy);
			member.setRelationshipCode(dto.getRelationshipCode());
			member.setSuperiorMemberId(superiorMemberId);
			memberDao.update(member);
		
			if (EnrollStatus.NEW.name().equals(superiorEnrollStatus) || EnrollStatus.APV.name().equals(superiorEnrollStatus)
					||EnrollStatus.REJ.name().equals(superiorEnrollStatus)||EnrollStatus.CAN.name().equals(superiorEnrollStatus)
					||EnrollStatus.TOA.name().equals(superiorEnrollStatus)) {
				
			}else if(EnrollStatus.ANC.name().equals(superiorEnrollStatus) ||EnrollStatus.CMP.name().equals(superiorEnrollStatus)){
				//Copy Member Limit Rule From Primary Member
				copyMemberLimitRuleFromPrimayMember(superiorMemberId,dto.getCustomerId(),updateBy);
				
				//Copy Member Facility Right From Primary Member
				copyMemberFacilityRight(superiorMemberId,dto.getCustomerId());
				
				MemberLimitRule limitRule = setMemberLimitRuleTRN(BigDecimal.ZERO);
				
				MemberLimitRule primayMemberCR = memberLimitRuleDao.getEffectiveMemberLimitRule(superiorMemberId, "CR");
				limitRule = setMemberLimitRule(limitRule, customerId, updateBy,new Date(),primayMemberCR!=null?primayMemberCR.getExpiryDate():null);
				memberLimitRuleDao.save(limitRule);
				
				CustomerEmailContent contentPrimary = customerEmailContentService.setEmailContentForMemberActivationEmail(customerId, EmailType.ACTIVATION.getFunctionId(), updateBy, fromName,true,member.getAcademyNo(),randomPassword);
				
				if(contentPrimary!=null) mailThreadService.sendWithResponse(contentPrimary, null, null, null);
			}
			
			//remove the customer enroll po to avoid the duplicate of enrollment
			CustomerEnrollment pasEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
			int enrollPoDelete = customerEnrollPoDao.deleteCustomerEnrollPoByEnrollId(pasEnrollment.getEnrollId());
			
		}
		
		
		
		// Re-enroll the IPM which is canceled or rejected before (IPM->IPM)
		if (dto.getRelationshipCode()==null&&member!=null&&"true".equalsIgnoreCase(dto.getImportStatus()) && MemberType.IPM.name().equals(member.getMemberType())) {
			member.setAcademyNo(null);
			member.setUpdateBy(updateBy);
			member.setUpdateDate(currentDate);
			member.setEffectiveDate(null);
			member.setStatus(Constant.General_Status_NACT);
			member.setFirstJoinDate(currentDate);
			memberDao.update(member);

			recordCustomerEnrollmentUpdate(enroll.getStatus(), EnrollStatus.NEW.name(), enroll.getCustomerId());
			enroll.setStatus(EnrollStatus.NEW.name());
			enroll.setInternalRemark(dto.getInternalRemark());
			enroll.setUpdateBy(updateBy);
			enroll.setUpdateDate(currentDate);
			CustomerEnrollment enrollUserInput = dto.getCustomerEnrollments().get(0);
			ServicePlan plan = servicePlanDao.get(ServicePlan.class, enrollUserInput.getSubscribePlanNo());
			if (plan != null && plan.getContractLengthMonth() != null) {
				enroll.setSubscribeContractLength(plan.getContractLengthMonth());
			}
			enroll.setSalesFollowBy(enrollUserInput.getSalesFollowBy());
			enroll.setSubscribePlanNo(enrollUserInput.getSubscribePlanNo());
			enroll.setEnrollDate(currentDate);

			customerEnrollmentDao.update(enroll);

			int enrollPoDelete = customerEnrollPoDao.deleteCustomerEnrollPoByEnrollId(enroll.getEnrollId());

		}
				
		//Upgrade the IDM (IDM->IPM)
		if (dto.getRelationshipCode()==null&&member!=null&&"true".equalsIgnoreCase(dto.getImportStatus())&&(MemberType.IDM.name().equals(member.getMemberType())||MemberType.CDM.name().equals(member.getMemberType()))) {
			Long superiorId = member.getSuperiorMemberId();
			CustomerEnrollment superiorEnroll = null;
			if (!StringUtils.isEmpty(superiorId))
				superiorEnroll = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(superiorId);
			if (superiorEnroll == null) throw new GTACommonException(GTAError.EnrollError.SUPERIOR_MEMBER_NOT_FOUND);
				
			String superiorEnrollStatus = superiorEnroll.getStatus();
			if (EnrollStatus.NEW.name().equals(superiorEnrollStatus) || EnrollStatus.APV.name().equals(superiorEnrollStatus)
					||EnrollStatus.REJ.name().equals(superiorEnrollStatus)||EnrollStatus.CAN.name().equals(superiorEnrollStatus)
					||EnrollStatus.TOA.name().equals(superiorEnrollStatus)) {
				setCustomerEnrollment(dto.getCustomerId(), updateBy, dto.getCustomerEnrollments().get(0).getSubscribePlanNo());
				memberDao.updateMemberer(updateMember(member, updateBy, superiorEnrollStatus));
			}
			if(EnrollStatus.ANC.name().equals(superiorEnrollStatus) ||EnrollStatus.CMP.name().equals(superiorEnrollStatus)){
				UserMaster dependentUserMaster = userMasterDao.getUserByUserId(member.getUserId());
				dependentUserMaster.setStatus(Constant.General_Status_NACT);
				userMasterDao.updateUserMaster(dependentUserMaster);
				memberDao.updateMemberer(updateMember(member, updateBy, superiorEnrollStatus));
				int rule = memberLimitRuleDao.deleteMemberLimitRule(dto.getCustomerId(), null);
				int right = memberPlanFacilityRightDao.deletePlanFacilityRight(dto.getCustomerId());
				setCustomerEnrollment(dto.getCustomerId(), updateBy, dto.getCustomerEnrollments().get(0).getSubscribePlanNo());
			}
			recordCustomerEnrollmentUpdate(null, EnrollStatus.NEW.name(), dto.getCustomerId());
		}
		
		//Enroll the Member Guest or House Guest(upgrade) (MG,HG->IPM)
		if(dto.getRelationshipCode()==null&&member!=null&&"true".equalsIgnoreCase(dto.getImportStatus())&&(MemberType.MG.name().equals(member.getMemberType())||MemberType.HG.name().equals(member.getMemberType()))){
			member.setUpdateBy(updateBy);
			member.setUpdateDate(currentDate);
			member.setStatus(Constant.General_Status_NACT);
			member.setFirstJoinDate(currentDate);
			member.setMemberType(MemberType.IPM.name());
			memberDao.update(member);
			
			recordCustomerEnrollmentUpdate(enroll.getStatus(), EnrollStatus.NEW.name(), enroll.getCustomerId());
			
			enroll.setEnrollType(MemberType.IPM.name());
			enroll.setStatus(EnrollStatus.NEW.name());
			enroll.setInternalRemark(dto.getInternalRemark());
			enroll.setUpdateBy(updateBy);
			enroll.setUpdateDate(currentDate);
			CustomerEnrollment enrollUserInput = dto.getCustomerEnrollments().get(0);
			ServicePlan plan = servicePlanDao.get(ServicePlan.class, enrollUserInput.getSubscribePlanNo());
			if (plan != null && plan.getContractLengthMonth() != null) {
				enroll.setSubscribeContractLength(plan.getContractLengthMonth());
			}
//			enroll.setSalesFollowBy(enrollUserInput.getSalesFollowBy());//the origin sales person who is responsible for the MG or HG
			enroll.setSalesFollowBy(updateBy);//update the sales person by using the current sales who logged in
			enroll.setSubscribePlanNo(enrollUserInput.getSubscribePlanNo());
			enroll.setEnrollDate(currentDate);

			customerEnrollmentDao.update(enroll);
		}
		
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;

	}
	
	public MemberLimitRule setMemberLimitRuleTRN(BigDecimal spendingLimit){
		MemberLimitRule memberLimitRule = new MemberLimitRule();
		memberLimitRule.setLimitType("TRN");
		memberLimitRule.setLimitUnit("EACH");
		memberLimitRule.setNumValue(spendingLimit);
		return memberLimitRule;
	}
	
	private MemberLimitRule setMemberLimitRule(MemberLimitRule limitRule, Long customerId, String createBy,Date effectiveDate, Date expiryDate){
		if("TRN".equals(limitRule.getLimitType())){
			limitRule.setLimitUnit("EACH");
		}
		limitRule.setCustomerId(customerId);
		limitRule.setCreateDate(new Timestamp(System.currentTimeMillis()));
		limitRule.setUpdateDate(new Date());
		limitRule.setCreateBy(createBy);
		limitRule.setUpdateBy(createBy);
		limitRule.setEffectiveDate(effectiveDate);
		limitRule.setExpiryDate(expiryDate);
		return limitRule;
	}
	
	private void copyMemberLimitRuleFromPrimayMember(Long superiodMemberId,Long customerId,String updateBy){
		List<MemberLimitRule> primaryLimitRule = memberLimitRuleDao.getEffectiveListByCustomerId(superiodMemberId);
		if(null != primaryLimitRule && primaryLimitRule.size() > 0){
			for(MemberLimitRule rule : primaryLimitRule){
				if(null != primaryLimitRule && !"CR".equals(rule.getLimitType())&&!"G1".equals(rule.getLimitType())){
					MemberLimitRule r = new MemberLimitRule();
					BeanUtils.copyProperties(rule, r, new String[]{"effectiveDate","limitId","customerId","updateBy","updateDate"});
					r.setEffectiveDate(new Date());
					r.setCustomerId(customerId);
					r.setUpdateBy(updateBy);
					r.setUpdateDate(new Date());
					memberLimitRuleDao.save(r);
				}
			}
		}
	}
	
	private void copyMemberFacilityRight(Long superiodMemberId,Long customerId){
		List<MemberPlanFacilityRight> mfr = memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(superiodMemberId);
		if(null != mfr && mfr.size() > 0){
			for(MemberPlanFacilityRight right : mfr){
				if(null != right){
					MemberPlanFacilityRight r = new MemberPlanFacilityRight();
					BeanUtils.copyProperties(right, r, new String[]{"effectiveDate","sysId","customerId"});
					r.setCustomerId(customerId);
					r.setEffectiveDate(new Date());
					memberPlanFacilityRightDao.save(r);
				}
			}
		}
	}
	
	private UserMaster createUserMaster(String academyNo,Long customerId,String createBy,String surName,String givenName,String password){
		
		String userId = getTheUserId(academyNo,customerId);
		UserMaster userMaster = new UserMaster();
		userMaster.setUserId(userId);
		userMaster.setLoginId(academyNo);
		userMaster.setNickname(givenName+"_"+surName);
		userMaster.setPassword(CommUtil.getMD5Password(academyNo, password));
		userMaster.setUserType("CUSTOMER");
		userMaster.setStatus("ACT");
		userMaster.setCreateDate(new Date());
		userMaster.setCreateBy(createBy);
		userMaster.setUpdateDate(new Date());
		userMaster.setUpdateBy(createBy);
		return userMaster;
	}
	
	/**
	 * Used to get the next available academy No.
	 * @author Liky_Pan
	 * @return
	 */
	private String getTheNextAvailableAcademyNo() {
		String stringAcademyNo = memberDao.findLargestAcademyNo();
		if (stringAcademyNo == null)
			stringAcademyNo = "0";
		Integer tempAcademyNo = Integer.parseInt(stringAcademyNo) + 1;
		return CommUtil.formatAcademyNo(tempAcademyNo);
	}
	
	/**
	 * Used to get the used by using the academy no and customer Id
	 * @author Liky_Pan
	 * @param academyNo
	 * @param customerId
	 * @return
	 */
	private String getTheUserId(String academyNo, Long customerId){
		return academyNo+"-"+Long.toHexString(customerId);
	}
	
	public Member updateMember(Member member,String userId,String superiorEnrollStatus){
		if(EnrollStatus.NEW.name().equals(superiorEnrollStatus) || EnrollStatus.APV.name().equals(superiorEnrollStatus)){
			member.setUserId(null);
			member.setAcademyNo(null);
		}
		member.setRelationshipCode(null);
		member.setStatus(Constant.Member_Status_NACT);
		member.setMemberType(MemberType.IPM.name());
		member.setSuperiorMemberId(null);
		member.setUpdateBy(userId);
		member.setUpdateDate(new Date());
		return member;
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseResult approveMember(Long customerId, String userId) {
		Member member = memberDao.getMemberByCustomerId(customerId);
		if (member != null && MemberType.IDM.name().equals(member.getMemberType())){
			responseResult.initResult(GTAError.EnrollError.APV_PRIMARY_FIRST);
			return responseResult;
		}

		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if (customerEnrollment == null){
			responseResult.initResult(GTAError.EnrollError.ENROLL_NOT_FOUND);
			return responseResult;
		}
		if (customerEnrollment != null && !EnrollStatus.NEW.name().equals(customerEnrollment.getStatus())) {
			responseResult.initResult(GTAError.EnrollError.ONLY_NEW_APV);
			return responseResult;
		}

		setMember(customerId, getNewAcademyNo(), null, userId,null);
		List<Member> listDependents = memberDao.getListMemberBySuperiorId(customerId);
		for (Member tempDependent : listDependents) {
			setMember(tempDependent.getCustomerId(), getNewAcademyNo(), customerId, userId,tempDependent.getRelationshipCode());
		}

		customerEnrollmentDao.updateStatus(EnrollStatus.APV.name(), customerId, userId);

		Long servicePlanNo = customerEnrollment.getSubscribePlanNo();

		PosServiceItemPrice pServiceItemPrice = getPosServiceItemPrice(servicePlanNo);
		Long orderNo = setCustomerOrderHd(customerId, pServiceItemPrice, "OPN", null, userId);
		Long orderDetId = setCustomerOderDet(orderNo, pServiceItemPrice, null, 1L, userId);
		setCustomerEnrollPo(customerEnrollment, orderDetId, orderNo);
		recordCustomerEnrollmentUpdate(EnrollStatus.NEW.name(),EnrollStatus.APV.name(),customerId);
		//add mobile notification
		CustomerProfile temProfile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
		devicePushService.pushMessage(new String[]{customerEnrollment.getSalesFollowBy()},"enrollment_approved_notification",new String[]{temProfile.getGivenName()+ " " +temProfile.getSurname()},Constant.SALESKIT_PUSH_APPLICATION);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Transactional
	public ResponseResult upgradeIDM(Long customerId, String userId, Long planNo) {
		Member member = memberDao.getMemberById(customerId);
		Long superiorId = member.getSuperiorMemberId();
		CustomerEnrollment superiorEnroll = null;
		if (!StringUtils.isEmpty(superiorId)) {
			superiorEnroll = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(superiorId);
		}
		if (superiorEnroll == null){
			responseResult.initResult(GTAError.EnrollError.SUPERIOR_MEMBER_NOT_FOUND);
			return responseResult;
		}
		String superiorEnrollStatus = superiorEnroll.getStatus();
		setCustomerEnrollment(customerId, userId, planNo);
		if (EnrollStatus.NEW.name().equals(superiorEnrollStatus)
				|| EnrollStatus.APV.name().equals(superiorEnrollStatus)) {
			member.setUserId(null);
			member.setAcademyNo(null);
			member.setMemberType(Constant.Member_Status_NACT);
			member.setSuperiorMemberId(null);
			member.setUpdateBy(userId);
			member.setUpdateDate(new Date());
			memberDao.updateMemberer(member);
		}

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	public void setCustomerEnrollment(Long customerId, String userId, Long planNo) {
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if(customerEnrollment==null){
			customerEnrollment = new CustomerEnrollment();
			customerEnrollment.setCustomerId(customerId);
			customerEnrollment.setCreateDate(new Date());
			customerEnrollment.setCreateBy(userId);
		}
		customerEnrollment.setEnrollDate(new Date());
		customerEnrollment.setEnrollType(MemberType.IPM.name());
		customerEnrollment.setStatus(EnrollStatus.NEW.name());
		customerEnrollment.setSubscribePlanNo(planNo);
		customerEnrollment.setSalesFollowBy(userId);
		ServicePlan plan = servicePlanDao.getServicePlanById(planNo);
		if (plan != null && plan.getContractLengthMonth() != null) {
			customerEnrollment.setSubscribeContractLength(plan.getContractLengthMonth());
		}
		customerEnrollmentDao.saveOrUpdate(customerEnrollment);
	}

	public String getNewAcademyNo() {
		String stringAcademyNo = memberDao.findLargestAcademyNo();
		if (stringAcademyNo == null)
			stringAcademyNo = "0";
		Integer tempAcademyNo = Integer.parseInt(stringAcademyNo) + 1;
		String academyNo = CommUtil.formatAcademyNo(tempAcademyNo);
		return academyNo;
	}

	public PosServiceItemPrice getPosServiceItemPrice(Long servicePlanNo) {
		PosServiceItemPrice pServiceItemPrice = null;
		List<PosServiceItemPrice> itemNos = servicePlanDao.getDaypassPriceItemNo(servicePlanNo,
				Constant.ServiceplanType.SERVICEPLAN.name());
		for (PosServiceItemPrice psip : itemNos) {
			if (psip.getItemNo().startsWith("SRV")) {
				pServiceItemPrice = psip;
			}
		}
		return pServiceItemPrice;
	}

	public void setMember(Long customerId, String academyNo, Long superiorMemberId, String userId,String relationshipCode) {
		Date currentDate = new Date();
		Member existingMember = memberDao.getMemberById(customerId);
		// will create the login user id once activated
		if (existingMember != null) {
			if (existingMember.getAcademyNo() == null)
				existingMember.setAcademyNo(academyNo);// for non-reserved
														// academyNo
			existingMember.setUpdateDate(currentDate);
			existingMember.setUpdateBy(userId);
			existingMember.setStatus(Constant.Status.NACT.name());
			existingMember.setFirstJoinDate(currentDate);
			existingMember.setEffectiveDate(null);
			existingMember.setInternalRemark(null);
			existingMember.setSuperiorMemberId(superiorMemberId);
			existingMember.setRelationshipCode(relationshipCode);
			memberDao.update(existingMember);
			if (null != academyNo) {
				spaMemberSyncDao.addSpaMemberSyncWhenInsert(existingMember);
			}
		} else {// for new member
			Member member = new Member();
			member.setCustomerId(customerId);
			member.setAcademyNo(academyNo);// when create dependent member,
											// academyNo = null is allocated
			if (superiorMemberId == null) {
				member.setMemberType(Constant.memberType.IPM.name());
			} else {
				member.setMemberType(Constant.memberType.IDM.name());
			}
			member.setStatus(Constant.Status.NACT.name());
			member.setFirstJoinDate(currentDate);
			member.setEffectiveDate(null);
			member.setCreateDate(currentDate);
			member.setCreateBy(userId);
			member.setUpdateDate(currentDate);
			member.setUpdateBy(userId);
			member.setRelationshipCode(relationshipCode);
			member.setSuperiorMemberId(superiorMemberId);
			memberDao.addMember(member);
			if (null != academyNo) {
				spaMemberSyncDao.addSpaMemberSyncWhenInsert(member);
			}
		}
	}

	public Long setCustomerOrderHd(Long customerId, PosServiceItemPrice pServiceItemPrice, String status,
			String orderRemark, String userId) {
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		Date currentDate = new Date();
		customerOrderHd.setOrderDate(currentDate);
		customerOrderHd.setOrderStatus(status);
		customerOrderHd.setStaffUserId(userId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(pServiceItemPrice.getItemPrice());
		customerOrderHd.setOrderRemark(orderRemark);
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(userId);
		customerOrderHd.setUpdateDate(currentDate);
		customerOrderHd.setUpdateBy(userId);
		return (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
	}

	public Long setCustomerOderDet(Long orderNo, PosServiceItemPrice pServiceItemPrice, String orderRemark,
			Long orderQty, String userId) {
		CustomerOrderDet cOrderDet = new CustomerOrderDet();
		Date currentDate = new Date();
		CustomerOrderHd cOrderHd = customerOrderHdDao.getOrderById(orderNo);
		cOrderDet.setCustomerOrderHd(cOrderHd);
		cOrderDet.setItemNo(pServiceItemPrice.getItemNo());
		cOrderDet.setItemRemark(orderRemark);
		cOrderDet.setOrderQty(orderQty);
		cOrderDet.setItemTotalAmout(pServiceItemPrice.getItemPrice());
		cOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		cOrderDet.setCreateBy(userId);
		cOrderDet.setUpdateDate(currentDate);
		cOrderDet.setUpdateBy(userId);
		return (Long) cOrderDetDao.saveOrderDet(cOrderDet);
	}

	public Serializable setCustomerEnrollPo(CustomerEnrollment customerEnrollment, Long orderDetId, Long orderNo) {
		CustomerEnrollPo customerEnrollPo = new CustomerEnrollPo();
		CustomerEnrollPoPK cEnrollPoPK = new CustomerEnrollPoPK();
		cEnrollPoPK.setEnrollId(customerEnrollment.getEnrollId());
		cEnrollPoPK.setOrderDetId(orderDetId);
		customerEnrollPo.setId(cEnrollPoPK);
		customerEnrollPo.setOrderNo(orderNo);
		return customerEnrollPoDao.saveCustomerEnrollPo(customerEnrollPo);
	}
	
	private VirtualAccPool allocateVirtualAccount() {
		VirtualAccPool virtualAccount = null;
		virtualAccount = virtualAccPoolDao.queryOneAvailableVirtualAccount();
		if (virtualAccount == null) {
			throw new GTACommonException(GTAError.EnrollError.NO_VIRTUAL_ACCOUNT);
		}
		virtualAccount.setStatus(Constant.VIR_ACC_STATUS_US);
		virtualAccPoolDao.update(virtualAccount);

		return virtualAccount;
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseResult setActivationDateForMember(ActivateMemberDto activeMemberDto,String loginUserId,String userName) throws Exception{
		Long customerId = activeMemberDto.getCustomerId();
		Date activationDate = activeMemberDto.getActivationDate();
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
				
		if(EnrollStatus.PYA.name().equals(customerEnrollment.getStatus())||EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())){
			
			Member member =  memberDao.get(Member.class, customerId);
			member.setEffectiveDate(activationDate);
			
			recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(), EnrollStatus.TOA.name(), customerEnrollment.getCustomerId());
			
			customerEnrollment.setStatus(EnrollStatus.TOA.name());
			
			memberDao.update(member);
			customerEnrollmentDao.update(customerEnrollment);
			
			
			Long timeDiff = CommUtil.CompareDates(new Date(),activeMemberDto.getActivationDate() );
			if(timeDiff.longValue()>=0){
				activateMember(activeMemberDto,loginUserId,userName);
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			responseResult.initResult(GTAError.EnrollError.ONLY_APV_TOA_CAN_BE_ACTIVATED);
			
		}
		return responseResult;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult encapsulateActivationMember(ActivateMemberDto activeMemberDto, String loginUserId,String loginUserName) throws Exception{
		Member member = memberDao.getMemberById(activeMemberDto.getCustomerId());
		if (member == null){
			responseResult.initResult(GTAError.EnrollError.MEMBER_NOT_FOUND);
			return responseResult;
		}
		member.setEffectiveDate(new Date());
		memberDao.update(member);
		return activateMember(activeMemberDto, loginUserId, loginUserName);
	}
	
	@SuppressWarnings("rawtypes")
	public ResponseResult activateMember(ActivateMemberDto activeMemberDto, String loginUserId,String loginUserName) throws Exception {
		Date currentDate = new Date();
		Long customerId = activeMemberDto.getCustomerId();

		CustomerProfile temProfile = customerProfileDao.getById(customerId);
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);

		if (temProfile == null || customerEnrollment == null){
			responseResult.initResult(GTAError.EnrollError.CUSTOMER_PROFILE_NOT_FOUND);
			return responseResult;
		}

		if (!customerEnrollment.getStatus().equals(EnrollStatus.PYA.name())&&!customerEnrollment.getStatus().equals(EnrollStatus.TOA.name())){
			responseResult.initResult(GTAError.EnrollError.ONLY_PYA_TOA_ACTIVATED);
			return responseResult;
		}

		Member member = memberDao.getMemberById(customerId);
		
		if (member == null){
			responseResult.initResult(GTAError.EnrollError.MEMBER_NOT_FOUND);
			return responseResult;
		}
		if (StringUtils.isEmpty(member.getAcademyNo())){
			responseResult.initResult(GTAError.EnrollError.ACADEMY_IS_EMPRY);
			return responseResult;
		}
		
		Date effectiveDate = null;
		if(member.getEffectiveDate()==null){
			effectiveDate = new Date();
		}else{
			effectiveDate = member.getEffectiveDate();
		}
		String checkUserId =member.getUserId();
		String memberUserId = setUserIdInMember(member);

		//Record the customer enrollment status changing log
		recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),EnrollStatus.ANC.name(),customerId);
		
		customerEnrollment.setStatus(EnrollStatus.ANC.name());
		customerEnrollment.setUpdateBy(loginUserId);
		customerEnrollment.setUpdateDate(currentDate);
		customerEnrollmentDao.update(customerEnrollment);
		Long servicePlanNo = customerEnrollment.getSubscribePlanNo();

		// UseMaster
		boolean checkLoginId = userMasterDao.checkAvailableLoginId(member.getAcademyNo(), memberUserId);
		if (!checkLoginId) throw new GTACommonException(GTAError.EnrollError.LOGIN_ID_IS_NOT_AV);
				
		String randomPrimaryPassword = CommUtil.generateRandomPassword();
		setUserMaster(checkUserId,member,memberUserId, temProfile, loginUserId,randomPrimaryPassword);

		// member limit rule
		ServicePlan servicePlan = servicePlanDao.getServicePlanById(servicePlanNo);
		if (servicePlan == null)
			throw new GTACommonException(GTAError.EnrollError.SERVICE_PLAN_NOT_FOUND);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(effectiveDate);
		if (servicePlan.getContractLengthMonth() != null) {
			calendar.add(Calendar.MONTH, servicePlan.getContractLengthMonth().intValue());
		}
		List servicePlanAdditionRuleList = servicePlanAdditionRuleDao.getServicePlanAdditionRuleByPlanNo(servicePlanNo);
		if (null != servicePlanAdditionRuleList && servicePlanAdditionRuleList.size() > 0) {
			for (int i = 0; i < servicePlanAdditionRuleList.size(); i++) {
				Object[] objectArry = (Object[]) servicePlanAdditionRuleList.get(i);
				MemberLimitRule memberLimitRule = new MemberLimitRule();
				memberLimitRule.setCustomerId(customerId);
				memberLimitRule.setLimitType((String) objectArry[0]);
				String inputValueType = (String) objectArry[1];
				if (!StringUtils.isEmpty(inputValueType))
					inputValueType = inputValueType.trim();
				if (ServicePlanRightTypeStatus.TXT.getDesc().equals(inputValueType))
					memberLimitRule.setTextValue((String) objectArry[2]);
				else if (ServicePlanRightTypeStatus.INT.getDesc().equals(inputValueType)) {
					memberLimitRule.setLimitUnit("MONTH");
					memberLimitRule.setNumValue(BigDecimal.valueOf(Long.valueOf((String) objectArry[2])));
				}
				memberLimitRule.setDescription((String) objectArry[3]);
				memberLimitRule.setCreateDate(new Timestamp(currentDate.getTime()));
				memberLimitRule.setCreateBy(loginUserId);
				memberLimitRule.setUpdateDate(currentDate);
				memberLimitRule.setUpdateBy(loginUserId);
				memberLimitRule.setEffectiveDate(effectiveDate);
				memberLimitRule.setExpiryDate(calendar.getTime());
				memberLimitRuleDao.save(memberLimitRule);
			}
		}

		// member limit rule
		setMemberLimitRule(customerId, "CR", BigDecimal.ZERO, "Credit Limit for Primary Member", loginUserId,calendar.getTime(),effectiveDate);

		// member_plan_facility_right
		List<MemberPlanFacilityRight> mFacilityRight = new ArrayList<MemberPlanFacilityRight>();
		List<ServicePlanFacility> sPlanFacility = servicePlanFacilityDao.getListByServicePlanNo(servicePlanNo);
		MemberPlanFacilityRight temp;
		Member tempMember = memberDao.getMemberById(customerId);
		for (ServicePlanFacility spf : sPlanFacility) {
			temp = new MemberPlanFacilityRight();
			temp.setCreateBy(loginUserId);
			temp.setCreateDate(currentDate);
			temp.setCustomerId(customerId);
			temp.setServicePlan(servicePlanNo);
			temp.setFacilityTypeCode(spf.getFacilityTypeCode());

			temp.setMember(tempMember);
			temp.setPermission(spf.getPermission());
			temp.setServicePlan(servicePlanNo);
			temp.setEffectiveDate(effectiveDate);
			temp.setExpiryDate(calendar.getTime());
			mFacilityRight.add(temp);
		}
		memberPlanFacilityRightDao.savePlanFacilityRight(mFacilityRight);

		// Customer_Service_ACC
		List<CustomerEnrollPo> customerEnrollPoList = customerEnrollPoDao.getListByEnrollId(customerEnrollment.getEnrollId());
		CustomerEnrollPo customerEnrollPo = null;
		if(customerEnrollPoList!=null&&customerEnrollPoList.size()>0){
			customerEnrollPo = customerEnrollPoList.get(0);
		}
		if (customerEnrollPo == null) throw new GTACommonException(GTAError.EnrollError.CUSTOMER_ORDER_NOT_FOUND);
		
		CustomerServiceAcc customerServiceAcc = new CustomerServiceAcc();
		customerServiceAcc.setCustomerId(customerId);
		if(MemberType.CPM.name().equals(member.getMemberType())){
			customerServiceAcc.setAccCat("COP");
		}else{
			customerServiceAcc.setAccCat("IDV");
		}
		customerServiceAcc.setEffectiveDate(effectiveDate);
		customerServiceAcc.setExpiryDate(calendar.getTime());
		customerServiceAcc.setStatus(Constant.General_Status_ACT);
		customerServiceAcc.setPeriodCode(servicePlan.getPassPeriodType());
		customerServiceAcc.setRemark("NEW JOIN");
		customerServiceAcc.setStatementDeliveryBy("EMAIL");
		customerServiceAcc.setSettlementMethodCode(null);
		customerServiceAcc.setOrderNo(customerEnrollPo.getOrderNo());
		Serializable serial = customerServiceDao.saveCustomerServiceAcc(customerServiceAcc);

		// Customer_Service_Subscribe
		boolean paidAmount = verifyPaymentAmount(customerEnrollment);
		if (!paidAmount) {
			throw new GTACommonException(GTAError.EnrollError.CLEAR_PAYMENT_FIRST);
		}
		CustomerServiceSubscribe customerServiceSubscribe = new CustomerServiceSubscribe();
		CustomerServiceAcc csAcc = customerServiceDao.getAccountNoByCustomerId(customerId);
		if (csAcc == null) throw new GTACommonException(GTAError.EnrollError.SERVICE_ACCOUNT_NOT_FOUND);
		customerServiceSubscribe.setCustomerServiceAcc(csAcc);
		CustomerServiceSubscribePK cspk = new CustomerServiceSubscribePK();
		cspk.setServicePlanNo(customerEnrollment.getSubscribePlanNo());
		cspk.setAccNo((Long) serial);
		customerServiceSubscribe.setId(cspk);
		customerServiceSubscribe.setOrderDetId(customerEnrollPo.getId().getOrderDetId());
		customerServiceSubscribe.setSubscribeDate(currentDate);
		customerServiceSubscribe.setCreateBy(loginUserId);
		customerServiceSubscribe.setCreateDate(currentDate);
		customerServiceSubscribe.setUpdateBy(loginUserId);
		customerServiceSubscribe.setUpdateDate(currentDate);
		customerServiceSubscribeDao.save(customerServiceSubscribe);

		// create member_cashvalue
		MemberCashvalue mcv = new MemberCashvalue();
		Date date = new Date();
		mcv.setInitialDate(date);
		mcv.setInitialValue(BigDecimal.ZERO);
		mcv.setAvailableBalance(BigDecimal.ZERO);
		mcv.setExchgFactor(new BigDecimal(1));
		mcv.setMember(member);
		mcv.setCustomerId(customerId);
		memberCashValueDao.saveMemberCashValue(mcv);

		VirtualAccPool virtualAccount = allocateVirtualAccount();
		
		// create member_payment_acc, and link a virtual account
		MemberPaymentAcc mpa = new MemberPaymentAcc();
		mpa.setCustomerId(customerId);
		mpa.setAccType(Constant.VIRTUAL_ACCOUNT);
		mpa.setAccountNo(String.valueOf(virtualAccount.getVaAccNo()));
		mpa.setBankId(virtualAccount.getBankId());
		mpa.setStatus(Constant.PAYMENT_ACC_STATUS_ACT);
		mpa.setCustomerId(customerId);
		mpa.setMember(member);
		memberPaymentAccDao.addMemberPaymentAcc(mpa);
		
		
		if (MemberType.CPM.name().equals(member.getMemberType())) {
			CorporateMember corporateMember = corporateMemberDao.getCorporateMemberById(customerId);
			if (corporateMember != null) {
				corporateMember.setStatus(Constant.General_Status_ACT);
				corporateMember.setUpdateBy(loginUserId);
				corporateMember.setUpdateDate(currentDate);
				corporateMemberDao.update(corporateMember);
			}
		}
		
		CustomerEmailContent contentPrimary = customerEmailContentService.setEmailContentForMemberActivationEmail(customerId, EmailType.ACTIVATION.getFunctionId(), loginUserId, loginUserName,true,member.getAcademyNo(),randomPrimaryPassword);
		
		if(contentPrimary!=null) mailThreadService.sendWithResponse(contentPrimary, null, null, null);
		
		if (MemberType.IPM.name().equals(member.getMemberType())) {
			List<Member> listDependent = memberDao.getListMemberBySuperiorId(customerId);
			for (Member tempDependent : listDependent) {
				String userId = setUserIdInMember(tempDependent);
				Long dependentCustomerId = tempDependent.getCustomerId();
				CustomerProfile tempDependentProfile = customerProfileDao.getById(dependentCustomerId);
				boolean dependentLoginAvailablity = userMasterDao.checkAvailableLoginId(tempDependent.getAcademyNo(), null);
				if (!dependentLoginAvailablity)
					throw new GTACommonException(GTAError.EnrollError.LOGIN_ID_IS_NOT_AV);
				String randomDependentPassword = CommUtil.generateRandomPassword();
				setUserMaster(null, tempDependent, userId, tempDependentProfile, loginUserId, randomDependentPassword);
				logger.debug("-------------Dependent Customer Id:" + dependentCustomerId + "------------------");
				setMemberLimitRule(dependentCustomerId, "TRN", BigDecimal.ZERO, null, userId, calendar.getTime(), effectiveDate);
				activateDependentMemer(customerId, dependentCustomerId, userId);
				CustomerEmailContent contentDependent = customerEmailContentService.setEmailContentForMemberActivationEmail(dependentCustomerId, EmailType.ACTIVATION.getFunctionId(), loginUserId, loginUserName, true, tempDependent.getAcademyNo(),
						randomDependentPassword);
				if (contentDependent != null)
					mailThreadService.sendWithResponse(contentDependent, null, null, null);
			}
		}
		
		//Add message notification for IPM
		if(MemberType.IPM.name().equals(member.getMemberType())){
			devicePushService.pushMessage(new String[]{customerEnrollment.getSalesFollowBy()},"member_activiated_notification",new String[]{temProfile.getGivenName()+ " " +temProfile.getSurname()},Constant.SALESKIT_PUSH_APPLICATION);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	public boolean verifyPaymentAmount(CustomerEnrollment customerEnrollment) {
		Long enrollId = customerEnrollment.getEnrollId();
		List<CustomerEnrollPo> customerEnrollPo = customerEnrollPoDao.getListByEnrollId(enrollId);
		BigDecimal paidAmount = null;
		if (customerEnrollPo != null && customerEnrollPo.size() > 0) {
			Long orderNo = customerEnrollPo.get(0).getOrderNo();
			Long orderDetNo = customerEnrollPo.get(0).getId().getOrderDetId();
			paidAmount = customerOrderTransDao.getApprovedPaymentAmount(orderNo);
			if (paidAmount == null) {
				paidAmount = BigDecimal.ZERO;
			}
			CustomerOrderDet customerOrderDet = cOrderDetDao.get(CustomerOrderDet.class, orderDetNo);
			int compareAmount = paidAmount.compareTo(customerOrderDet.getItemTotalAmout());
			if (compareAmount == -1) {
				return false;
			}
		}
		return true;
	}

	public void setUserMaster(String checkUserId,Member member,String memberUserId, CustomerProfile temProfile, String userId,String randomPassword) {
		Date currentDate = new Date();
		if (StringUtils.isEmpty(checkUserId)){
			UserMaster userMaster = new UserMaster();
			userMaster.setCreateDate(currentDate);
			userMaster.setUserId(memberUserId);
			userMaster.setLoginId(member.getAcademyNo());
			userMaster.setPassword(CommUtil.getMD5Password(member.getAcademyNo(), randomPassword));
			userMaster.setNickname(temProfile.getGivenName() + "_" + temProfile.getSurname());
			userMaster.setCreateDate(currentDate);
			userMaster.setCreateBy(userId);
			userMaster.setUpdateBy(userId);
			userMaster.setUpdateDate(currentDate);
			userMaster.setUserType(defUserType);
			userMaster.setStatus(Constant.General_Status_ACT);
			userMasterDao.save(userMaster);
		}else{
			UserMaster existUser = userMasterDao.getUserByUserId(member.getUserId());
			existUser.setLoginId(member.getAcademyNo());
			existUser.setPassword(CommUtil.getMD5Password(member.getAcademyNo(), randomPassword));
			existUser.setStatus(Constant.General_Status_ACT);
			existUser.setUpdateBy(userId);
			existUser.setUpdateDate(currentDate);
			userMasterDao.update(existUser);
		}
		
	}

	private void setMemberLimitRule(Long dependentCustomerId, String limitType, BigDecimal limitValue, String description,
			String userId,Date expiryDate,Date effectiveDate) {
		Date currentDate = new Date();
		MemberLimitRule memberLimitRuleCR = new MemberLimitRule();
		memberLimitRuleCR.setCustomerId(dependentCustomerId);
		if("TRN".equals(limitType)){
			memberLimitRuleCR.setLimitUnit("EACH");
		}
		memberLimitRuleCR.setLimitType(limitType);
		memberLimitRuleCR.setNumValue(limitValue);
		memberLimitRuleCR.setDescription(description);
		memberLimitRuleCR.setEffectiveDate(effectiveDate);
		memberLimitRuleCR.setExpiryDate(expiryDate);
		memberLimitRuleCR.setCreateDate(new Timestamp(currentDate.getTime()));
		memberLimitRuleCR.setCreateBy(userId);
		memberLimitRuleCR.setUpdateBy(userId);
		memberLimitRuleCR.setUpdateDate(currentDate);
		Long returnedDependentId = (Long)memberLimitRuleDao.saveMemberLimitRuleImpl(memberLimitRuleCR);
		logger.debug("-------------Returned Limit Id:"+returnedDependentId+"--------------For customerId:"+dependentCustomerId);
	}

	public String setUserIdInMember(Member member) {
		String memberUserId = null;
		if(StringUtils.isEmpty(member.getUserId())){
			memberUserId = member.getAcademyNo() + "-" + Long.toHexString(member.getCustomerId());
			member.setUserId(memberUserId);
		}else{
			memberUserId = member.getUserId();
		}
		member.setStatus(Constant.Member_Status_ACT);
		memberDao.updateMemberer(member);
		return memberUserId;
	}

	public void activateDependentMemer(Long superiorMemberId, Long dependentCustomerId, String userId) {
		// member limit rule
		List<MemberLimitRule> primaryLimitRule = memberLimitRuleDao.getEffectiveListByCustomerId(superiorMemberId);
		if (null != primaryLimitRule && primaryLimitRule.size() > 0) {
			for (MemberLimitRule rule : primaryLimitRule) {
				if (null != primaryLimitRule && !"CR".equals(rule.getLimitType()) && !"G1".equals(rule.getLimitType())) {
					MemberLimitRule r = new MemberLimitRule();
					BeanUtils.copyProperties(rule, r,
							new String[] { "limitId", "customerId", "updateBy", "updateDate" });
					r.setCustomerId(dependentCustomerId);
					r.setUpdateBy(userId);
					r.setUpdateDate(new Date());
					memberLimitRuleDao.save(r);
				}
			}
		}
		// save MemberPlanFacilityRight
		List<MemberPlanFacilityRight> mfr = memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(superiorMemberId);
		if (null != mfr && mfr.size() > 0) {
			for (MemberPlanFacilityRight right : mfr) {
				if (null != right) {
					MemberPlanFacilityRight r = new MemberPlanFacilityRight();
					BeanUtils.copyProperties(right, r, new String[] { "sysId", "customerId" });
					r.setCustomerId(dependentCustomerId);
					memberPlanFacilityRightDao.save(r);
				}
			}
		}
	}

	@Transactional
	public ResponseMsg removeActivedMemberInPendingList(Long customerId) {
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if (customerEnrollment == null){
			responseMsg.initResult(GTAError.EnrollError.CUSTOMER_ENROLL_NOT_FOUND_IN_REMOVE_ACT_MEMBER);
			return responseMsg;
		}
		try {
			customerEnrollment.setStatus(EnrollStatus.CMP.name());
			customerEnrollmentDao.updateCustomerEnrollment(customerEnrollment);
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		} catch (Exception e) {
			responseMsg.initResult(GTAError.EnrollError.UPDATE_FAIL);
			return responseMsg;
		}

	}

	@Transactional
	public String checkEnrollmentStatus(Long enrollId) {
		return customerEnrollmentDao.checkEnrollmentStatus(enrollId);
	}

	@Transactional
	public boolean deleteEnrollmentsWithoutPayment(Long enrollId) {
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByEnrollId(enrollId);
		recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(), EnrollStatus.OPN.name(), customerEnrollment.getCustomerId());
		
		boolean status = customerEnrollmentDao.deleteEnrollmentsWithoutPayment(enrollId);
		return status;
	}

	@Transactional
	public boolean checkTransactionsStatusAsSuccessful(Long enrollId) {

		try {
			Long customerEnrollPoOrderNo = null;

			CustomerOrderHd customerOrderHd = customerOrderHdDao.getActiveOrderByEnrollId(enrollId);
			customerEnrollPoOrderNo = customerOrderHd.getOrderNo();
			if (customerEnrollPoOrderNo == null) {
				return false;
			}

			List<CustomerOrderTrans> customerOrderTransList = customerOrderTransDao
					.getPaymentDetailsByOrderNo(customerEnrollPoOrderNo);

			for (CustomerOrderTrans customerOrderTrans : customerOrderTransList) {
				if (!customerOrderTrans.getStatus().equals("SUC")) {
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional
	public boolean checkBalanceDueAsZero(Long enrollId) {

		try {
			if (customerOrderHdDao.getActiveOrderByEnrollId(enrollId) != null) {
				Long orderNo = customerOrderHdDao.getActiveOrderByEnrollId(enrollId).getOrderNo();
				BigDecimal balanceDue = homePageSummaryDao.countBalanceDue(orderNo);
				int r = balanceDue.compareTo(BigDecimal.ZERO);
				if (r == 0 || r == -1) {
					return true;
				}
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Transactional
	public int updateEnrollmentStatusToComplete(Long enrollId, String status, String userId) {
		return customerEnrollmentDao.updateEnrollmentStatusToComplete(enrollId, status, userId);
	}

	@Transactional
	@Override
	public ResponseResult viewEnrollmentRecords(CustomerEnrollmentDto dto, String userId,String device) {

		/*
		 * StringBuilder sql = new
		 * StringBuilder("select c.enroll_date as enrollDateDB, ")
		 * .append(" c.enroll_id as enrollId2 , ") .append(
		 * " concat( cp.salutation , ' ', cp.given_name, ' ', cp.surname) as memberName, "
		 * ) .append(" c.status, s.pass_period_type as passPeriodType,  ")
		 * .append
		 * (" concat(sp.given_name , ' ',  sp.surname)  as salesFollowBy ,")
		 * .append(" cp.customer_id as customerId2 , ")
		 * .append(" cp.contact_email as contactEmail ") //
		 * .append("po.order_no as orderNo2") .append(
		 * " from customer_enrollment c, customer_profile cp ,service_plan s , staff_profile sp "
		 * ) .append(
		 * " where c.customer_id = cp.customer_id and c.subscribe_plan_no = s.plan_no  "
		 * ) .append(" and c.sales_follow_by = sp.user_id  "); //
		 * .append("  and po.enroll_id = c.enroll_id "); StringBuilder countSql
		 * = new StringBuilder(
		 * " select count(*) from customer_enrollment c, customer_profile cp ,service_plan s "
		 * +
		 * " where c.customer_id = cp.customer_id and c.subscribe_plan_no = s.plan_no"
		 * );
		 */
//		String newfields = ",m.academy_no academyNo,IFNULL(n.fail_trans_no,0) failTransNo,ifnull(hd.order_total_amount - p.paid,hd.order_total_amount) balanceDue, CASE c.`status` WHEN 'TOA' THEN DATEDIFF(m.effective_date, NOW()) ELSE NULL END AS noOfDays,hd.order_status orderStatus,spop.offer_code offerCode ";
//		String failTransNoSql = " LEFT JOIN (SELECT order_no,count(*) fail_trans_no FROM customer_order_trans trans WHERE trans.`status` = 'FAIL' OR trans.`status` = 'VOID' GROUP BY order_no) n ON n.order_no = po.order_no ";
//		String balanceDueSql = " LEFT JOIN (SELECT order_item.order_no,sum(paid_amount) paid FROM (SELECT d.order_no,t.paid_amount,t.`status` FROM customer_order_det d,customer_order_trans t WHERE d.order_no = t.order_no) order_item WHERE order_item. STATUS = 'SUC' GROUP BY order_item.order_no) p ON po.order_no = p.order_no LEFT JOIN customer_order_hd hd ON po.order_no = hd.order_no ";
//		String offerCodeSql = " LEFT JOIN customer_order_det cod ON hd.order_no = cod.order_no LEFT JOIN service_plan_offer_pos spop ON cod.item_no = spop.pos_item_no ";
//		StringBuilder sql = new StringBuilder("Select * from ( ")
//				.append("select c.enroll_date as enrollDateDB, ")
//				.append(" c.enroll_id as enrollId2 , ")
//				.append(" concat( cp.salutation , ' ', cp.given_name, ' ', cp.surname) as memberName, ")
//				.append(" c.status, s.pass_period_type as passPeriodType,  ")
//				.append(" concat(sp.given_name , ' ',  sp.surname)  as salesFollowBy ,")
//				.append(" cp.customer_id as customerId2 , ")
//				.append(" cp.contact_email as contactEmail , ")
//				.append("po.order_no as orderNo2")
//				.append(newfields)
//				.append(" from   customer_enrollment c  INNER JOIN member m on c.customer_id = m.customer_id and m.member_type!= ? inner join customer_profile cp  on  c.customer_id = cp.customer_id and c.status!= ? ")
//				.append(" inner join service_plan s on c.subscribe_plan_no = s.plan_no  ")
//				.append(" inner join staff_profile sp on c.sales_follow_by = sp.user_id ")
//				.append(" left join customer_enroll_po po on po.enroll_id = c.enroll_id  ")
//				.append(failTransNoSql)
//				.append(balanceDueSql)
//				.append(offerCodeSql)
//				.append(") t where 1=1 ");
//		StringBuilder countSql = new StringBuilder(" select count(1) ")
//				.append(" from   customer_enrollment c  INNER JOIN member m on c.customer_id = m.customer_id and m.member_type!= ? inner join customer_profile cp  on  c.customer_id = cp.customer_id and c.status!= ? ")
//				.append(" inner join service_plan s on c.subscribe_plan_no = s.plan_no  ")
//				.append(" inner join staff_profile sp on c.sales_follow_by = sp.user_id ")
//				.append(" left join customer_enroll_po po on po.enroll_id = c.enroll_id  ")
//				.append(failTransNoSql)
//				.append(balanceDueSql)
//				.append(offerCodeSql)
//				.append(" where 1=1 ");
		
		StringBuilder sql = new StringBuilder("SELECT * FROM ( ")
			.append(" SELECT trans.enrollDateDB, ")
			.append(" trans.enrollId2, ")
			.append(" trans.memberName, ")
			.append(" trans.`status`, ")
			.append(" sp.pass_period_type AS passPeriodType, ")
			.append(" concat(spr.given_name, ' ', spr.surname ) AS salesFollowBy, ")
			.append(" trans.customerId2, ")
			.append(" trans.contactEmail, ")
			.append(" trans.order_no orderNo2, ")
			.append(" trans.academyNo academyNo, ")
			.append(" trans.failTransNo, ")
			.append(" trans.balance_due balanceDue, ")
			.append(" CASE trans.`status` WHEN 'TOA' THEN DATE_FORMAT(trans.effective_date,'%Y-%m-%d') ELSE NULL END AS effectiveDate, ")
			.append(" CASE trans.`status` WHEN 'TOA' THEN DATEDIFF(trans.effective_date, NOW()) ELSE NULL END AS noOfDays, ")
			.append(" trans.order_status orderStatus, ")
			.append(" spop.offer_code offerCode ")
			.append(" FROM (SELECT trans.*, enroll.sales_follow_by, enroll.enroll_date AS enrollDateDB, enroll.enroll_id AS enrollId2, concat(cp.salutation,' ',cp.given_name,' ',cp.surname) AS memberName,enroll.`status`,cp.customer_id AS customerId2,cp.contact_email AS contactEmail,	mem. STATUS memberStatus, mem.academy_no academyNo, mem.effective_date, IFNULL(trans.fail_trans_no, 0) failTransNo ")
			.append(" FROM (SELECT h.order_no,h.order_date, h.customer_id, h.order_total_amount, ifnull(h.order_total_amount - p.paid,h.order_total_amount ) balance_due,h.order_status,n.fail_trans_no,h.update_date ")
			.append(" FROM (SELECT hd.*, plan.plan_no, plan.pass_nature_code ")
			.append(" FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN pos_service_item_price price ON price.item_no = pos.pos_item_no JOIN customer_order_det det ON det.item_no = price.item_no JOIN customer_order_hd hd ON hd.order_no = det.order_no WHERE plan.pass_nature_code = 'LT' ")
			.append(" UNION SELECT hd.*, plan.plan_no, plan.pass_nature_code ")
			.append(" FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN service_plan_offer_pos spop ON spop.serv_pos_id = pos.serv_pos_id JOIN pos_service_item_price price ON price.item_no = spop.pos_item_no JOIN customer_order_det det ON det.item_no = price.item_no JOIN customer_order_hd hd ON hd.order_no = det.order_no WHERE plan.pass_nature_code = 'LT' ")
			.append(" ) h LEFT JOIN ( SELECT order_item.order_no, sum(paid_amount) paid FROM (SELECT d.order_no, t.paid_amount, t.`status` FROM customer_order_det d, customer_order_trans t WHERE d.order_no = t.order_no ) order_item WHERE order_item. STATUS = 'SUC' GROUP BY order_item.order_no ")
			.append(" ) p ON h.order_no = p.order_no LEFT JOIN ( SELECT order_no, count(*) fail_trans_no FROM customer_order_trans WHERE customer_order_trans.`status` = 'FAIL' OR customer_order_trans.`status` = 'VOID' GROUP BY order_no ) n ON h.order_no = n.order_no ")
			.append(" ) trans, customer_profile cp, customer_enrollment enroll,customer_enroll_po po, member mem WHERE /*trans.customer_id = cp.customer_id*/ trans.order_no = po.order_no AND po.enroll_id = enroll.enroll_id AND cp.customer_id = enroll.customer_id AND enroll.customer_id = mem.customer_id AND mem.member_type = ? AND enroll.`status` != ? ) trans")
			.append(" JOIN customer_order_det cod ON trans.order_no = cod.order_no ")
			.append(" JOIN pos_service_item_price psip ON cod.item_no = psip.item_no ")
			.append(" JOIN staff_profile spr ON trans.sales_follow_by = spr.user_id ")
			.append(" LEFT OUTER JOIN service_plan_offer_pos spop ON cod.item_no = spop.pos_item_no ")
			.append(" LEFT OUTER JOIN service_plan_pos spp ON cod.item_no = spp.pos_item_no ")
			.append(" LEFT OUTER JOIN service_plan sp ON spp.plan_no = sp.plan_no ")
			.append(" WHERE psip.item_catagory = 'SRV' ")
			.append(" UNION SELECT custe.enroll_date enrollDateDB, custe.enroll_id enrollId2, concat( custp.salutation, ' ', custp.given_name, ' ', custp.surname ) AS memberName, custe.`status`, svp.pass_period_type AS passPeriodType, concat( stapr.given_name, ' ', stapr.surname ) AS salesFollowBy, custe.customer_id customerId2, custp.contact_email contactEmail, NULL AS orderNo2, NULL AS acdemyNo, 0 AS failTransNo, 0 AS balanceDue, null AS effectiveDate, NULL AS noOfDays, NULL AS orderStatus, NULL AS offerCode ")
			.append(" FROM customer_enrollment custe ")
			.append(" JOIN staff_profile stapr ON custe.sales_follow_by = stapr.user_id ")
			.append(" LEFT JOIN customer_profile custp ON custe.customer_id = custp.customer_id ")
			.append(" LEFT JOIN member mb ON custp.customer_id = mb.customer_id ")
			.append(" LEFT JOIN customer_service_acc custacc ON mb.customer_id = custacc.customer_id ")
			.append(" LEFT JOIN customer_service_subscribe custsub ON custacc.acc_no = custsub.acc_no ")
			.append(" LEFT JOIN service_plan svp ON custsub.service_plan_no = svp.plan_no ")
			.append(" WHERE mb.member_type = 'IPM' AND custe.`status` = 'NEW'")
			.append(" ) t WHERE 1 = 1 " );
		StringBuilder countSql = new StringBuilder(" select count(1) from ( ").append(sql.toString()).append(" ) c WHERE 1=1");
		

		ListPage<CustomerEnrollment> listPage = new ListPage<CustomerEnrollment>();
		List<ListPage<CustomerEnrollment>.OrderBy> orderByList = new ArrayList<ListPage<CustomerEnrollment>.OrderBy>();
		if (dto.getOrderBy() != null && dto.getOrderBy().length() > 0){
			if (dto.getOrderBy().equals("enrollDate")){
				dto.setOrderBy("enrollDateDB");
			}
			orderByList.add(listPage.new OrderBy("t." + dto.getOrderBy(), dto.getIsAscending().equals("true") ? OrderType.ASCENDING : OrderType.DESCENDING));
		}
//		if (dto.getEnrollDateOrder() != null && dto.getEnrollDateOrder().length() > 0) {
//			if ("asc".equals(dto.getEnrollDateOrder())) {
//				orderByList.add(listPage.new OrderBy("c.enroll_date", OrderType.ASCENDING));
//			} else {
//				orderByList.add(listPage.new OrderBy("c.enroll_date", OrderType.DESCENDING));
//			}
//		}
//		if (CommUtil.notEmpty(dto.getMemberNameOrder())) {
//			if ("asc".equals(dto.getMemberNameOrder())) {
//				orderByList.add(listPage.new OrderBy("memberName", OrderType.ASCENDING));
//			} else {
//				orderByList.add(listPage.new OrderBy("memberName", OrderType.DESCENDING));
//			}
//		}
//		if (CommUtil.notEmpty(dto.getSalesFollowBy())) {
//			if ("asc".equals(dto.getSalesFollowBy())) {
//				orderByList.add(listPage.new OrderBy("salesFollowBy", OrderType.ASCENDING));
//			} else {
//				orderByList.add(listPage.new OrderBy("salesFollowBy", OrderType.DESCENDING));
//			}
//		}
//		if (CommUtil.notEmpty(dto.getStatusOrder())) {
//			if ("asc".equals(dto.getStatusOrder())) {
//				orderByList.add(listPage.new OrderBy("c.status", OrderType.ASCENDING));
//			} else {
//				orderByList.add(listPage.new OrderBy("c.status", OrderType.DESCENDING));
//			}
//		}
//		if (CommUtil.notEmpty(dto.getPassPeriodType())) {
//			if ("asc".equals(dto.getPassPeriodType())) {
//				orderByList.add(listPage.new OrderBy("s.pass_period_type", OrderType.ASCENDING));
//			} else {
//				orderByList.add(listPage.new OrderBy("s.pass_period_type", OrderType.DESCENDING));
//			}
//		}
		if (0 == orderByList.size()) {
			orderByList.add(listPage.new OrderBy("t.enrollDateDB", OrderType.DESCENDING));
		}
		orderByList.add(listPage.new OrderBy("t.academyNo", OrderType.ASCENDING));
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(MemberType.IPM.name());
		param.add(EnrollStatus.OPN.name());
		if (CommUtil.notEmpty(dto.getStatus())) {
			sql.append(" and  t.status =?   ");
			countSql.append(" and  c.status =?   ");
			param.add(dto.getStatus());
		}
		if (!StringUtils.isEmpty(dto.getOfferCode()) && dto.getOfferCode().equalsIgnoreCase(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)){
			sql.append(" and t.offerCode =?  ");
			countSql.append(" and c.offerCode =?  ");
			param.add(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL);
		}
		else if (!StringUtils.isEmpty(dto.getOfferCode()) && dto.getOfferCode().equalsIgnoreCase(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)){
			sql.append(" and t.offerCode is null  ");
			countSql.append(" and c.offerCode is null  ");
		}
		listPage.setOrderByList(orderByList);
		listPage.setNumber(dto.getCurrentPage());
		listPage.setSize(dto.getPageSize());
		List<CustomerEnrollmentDto> returnDto = new ArrayList<CustomerEnrollmentDto>();
		try {
			this.customerEnrollmentDao.listBySqlDto(listPage, countSql.toString(), sql.toString(), param, dto);
			List<Object> list = listPage.getDtoList();
			for (Object temp : list) {
				CustomerEnrollmentDto customerEnrollmentDto = (CustomerEnrollmentDto) temp;
				long customerId = customerEnrollmentDto.getCustomerId();
				customerEnrollmentDto.setEnrollDate(DateConvertUtil.getYMDDateAndDateDiff(customerEnrollmentDto
						.getEnrollDateDB()));
				long remarkNo = (long) this.remarksDao.countUnreadRemarkByDevice(customerId, userId,device);
				customerEnrollmentDto.setRemarkNo(remarkNo + "");
				returnDto.add(customerEnrollmentDto);
			}
			responseResult.initResult(GTAError.Success.SUCCESS,listPage);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	@Transactional
	public void assignSaleperson(Long enrollmentId, String salepersonId, String userId) {
		RuntimeException no_user = new RuntimeException(this.getI18nMessge("not_exsit.saleperson_id",
				new String[] { salepersonId }, Locale.ROOT));
		RuntimeException not_found = new RuntimeException(this.getI18nMessge("not_exsit.enrollment_id",
				new Long[] { enrollmentId }, Locale.ROOT));
		RuntimeException fail_assign = new RuntimeException(this.getI18nMessge("fail.assign_enrollment_to.saleperson",
				new String[] { salepersonId }, Locale.ROOT));
		CustomerEnrollment enrollment;

		try {
			UserMaster user = userMasterDao.get(UserMaster.class, salepersonId);
			if (user == null) {
				throw no_user;
			}
		} catch (Exception e) {
			throw no_user;
		}

		try {
			enrollment = customerEnrollmentDao.getCustomerEnrollmentByEnrollId(enrollmentId);
			if (enrollment == null) {
				throw not_found;
			}
		} catch (Exception e) {
			throw not_found;
		}

		try {
			enrollment.setSalesFollowBy(salepersonId);
			enrollment.setUpdateBy(userId);
			enrollment.setUpdateDate(new Date());
			customerEnrollmentDao.updateCustomerEnrollment(enrollment);
		} catch (Exception e) {
			throw fail_assign;
		}
	}

	@Override
	@Transactional
	public void updateEnrollmentStatus(Long enrollmentId, String status,String userId, String userName ) {
		RuntimeException fail_update = new RuntimeException(this.getI18nMessge("fail.update.enrollment_status",
				new String[] { status }, Locale.ROOT));
		RuntimeException not_found = new RuntimeException(this.getI18nMessge("not_exsit.enrollment_id",
				new Long[] { enrollmentId }, Locale.ROOT));
		CustomerEnrollment enrollment;

		try {
			enrollment = customerEnrollmentDao.getCustomerEnrollmentByEnrollId(enrollmentId);
			if (enrollment == null) {
				throw not_found;
			}
		} catch (Exception e) {
			throw not_found;
		}

		try {
			recordCustomerEnrollmentUpdate(enrollment.getStatus(), status, enrollment.getCustomerId());
			
			enrollment.setStatus(status);
			customerEnrollmentDao.updateCustomerEnrollment(enrollment);
			if (EnrollStatus.CAN.getName().equals(status)  || EnrollStatus.REJ.getName().equals(status)) {
				Member member = memberDao.get(Member.class, enrollment.getCustomerId());
				member.setStatus(Constant.Member_Status_NACT);
				memberDao.update(member);
				List<CustomerEnrollPo> customerEnrollPos = customerEnrollPoDao.getListByEnrollId(enrollmentId);
				if (null != customerEnrollPos && customerEnrollPos.size() > 0) {
					CustomerEnrollPo customerEnrollPo = customerEnrollPos.get(0);
					CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, customerEnrollPo.getOrderNo());
					if (null != customerOrderHd) {
						customerOrderHd.setOrderStatus(status);
						customerOrderHd.setUpdateDate(new Date());
					    customerOrderHd.setUpdateBy(userId);
						customerOrderHdDao.update(customerOrderHd);
					}
				}
				List<Member> tempMembers = memberDao.getListMemberBySuperiorId(enrollmentId);
				for(Member tempmember: tempMembers){
					tempmember.setStatus(Constant.Member_Status_NACT);
					memberDao.update(tempmember);
				}
				
//				String hql = "Update Member m set m.status = ? where m.superiorMemberId = ? ";
//				List<Serializable> param = new ArrayList<Serializable>();
//				param.add(Constant.Member_Status_NACT);
//				param.add(enrollmentId);
//				memberDao.hqlUpdate(hql, param);
			}
			
			if(EnrollStatus.REJ.name().equals(enrollment.getStatus())){
				confirmSendEmailAfterRejectTheEnrollment(enrollment.getCustomerId(),userId,userName);
				//add mobile notification
				CustomerProfile temProfile = customerProfileDao.getCustomerProfileByCustomerId(enrollment.getCustomerId());
				devicePushService.pushMessage(new String[]{enrollment.getSalesFollowBy()},"enrollment_rejected_notification",new String[]{temProfile.getGivenName()+ " " +temProfile.getSurname()},Constant.SALESKIT_PUSH_APPLICATION);
			}
		} catch (Exception e) {
			throw fail_update;
		}
	}

	@Override
	@Transactional
	public ResponseMsg updateRenewalStatus(Long orderNo, String status, String userId, String userName) {
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if (Constant.Status.CMP.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())) {
			responseMsg.initResult(GTAError.EnrollmentAdvanceError.COMPLETE_ORDER_CAN_NOT_BE_UPDATED);
			return responseMsg;
		}
		customerOrderHd.setOrderStatus(status);
		customerOrderHd.setUpdateBy(userName);
		customerOrderHd.setUpdateDate(new Date());
		boolean isSuccess = customerOrderHdDao.update(customerOrderHd);
		if (isSuccess) {
			responseMsg.initResult(GTAError.Success.SUCCESS);
		}else {
			responseMsg.initResult(GTAError.EnrollmentAdvanceError.UPDATE_ORDER_STATUS_FAIL);
		}
		return responseMsg;
	}
	
	/**
	 * Method used to send the email after clicking the reject button
	 * @param customerId
	 * @param userId
	 * @param userName
	 */
	private void confirmSendEmailAfterRejectTheEnrollment(Long customerId,String userId, String userName){
		
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(EmailType.REJECT.getFunctionId());
		CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, customerId);
		String contentMT= mt.getContentHtml();
		contentMT = mt.getFullContentHtml(cp.getSalutation()+" "+cp.getGivenName() + " " + cp.getSurname(),userName);
		
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		customerEmailContent.setRecipientCustomerId(customerId.toString());
		customerEmailContent.setSendDate(new Date());
		customerEmailContent.setSenderUserId(userId);
		customerEmailContent.setCopyto(null);
		customerEmailContent.setSubject(mt.getMessageSubject());
		customerEmailContent.setContent(contentMT);
		customerEmailContent.setRecipientEmail(cp.getContactEmail());
		customerEmailContent.setStatus(EmailStatus.PND.name());
		String sendId = (String) customerEmailContentDao.save(customerEmailContent);
		customerEmailContent.setSendId(sendId);
		mailThreadService.sendWithResponse(customerEmailContent, null, null, null);
	}

	@Override
	@Transactional
	public CustomerEnrollment getEnrollmentById(Long enrollId) {
		return customerEnrollmentDao.get(CustomerEnrollment.class, enrollId);
	}

	@Transactional
	public CustomerEnrollment getEnrollmentByCustomerId(Long customerId) {
		return customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
	}

	@Transactional
	public ResponseResult getMemberTypeByAcademyNo(String academyNo) {
		String memberType = memberDao.getMemberTypeByAcademyNo(academyNo);
		if (memberType == null){
			responseResult.initResult(GTAError.EnrollError.CAN_NOT_RETRIEVE_MEMBER_DETAIL);
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS);
			responseResult.setData(memberType);
		}
		return responseResult;
	}

	@SuppressWarnings("unused")
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult deleteDependentMemebr(Long customerId, String userId) {
		Member member = memberDao.getMemberByCustomerId(customerId);
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(member
				.getSuperiorMemberId());
		if (customerEnrollment != null && !userId.equals(customerEnrollment.getSalesFollowBy())) {
			responseResult.initResult(GTAError.EnrollError.DELETE_AUTH);
			return responseResult;
		}
		if(customerEnrollment!=null&&!EnrollStatus.NEW.name().equals(customerEnrollment.getStatus())&&!EnrollStatus.APV.name().equals(customerEnrollment.getStatus())
				&&!EnrollStatus.PYA.name().equals(customerEnrollment.getStatus())&&!EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())){
			responseResult.initResult(GTAError.EnrollError.DELETE_ONLY_FOR_INACTIVATE_PM);
			return responseResult;
		}
		CustomerProfile customerProfile = customerProfileDao.getById(customerId);
		customerProfile.setIsDeleted("Y");
		boolean updateStatus = customerProfileDao.saveOrUpdate(customerProfile);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@SuppressWarnings("unused")
	private String[] getTestAccs() {

		String config = appProps.getProperty("virtual.account.test", null);
		if (config == null)
			return null;

		String[] accs = config.split("\\|");
		return accs;
	}

	private ResponseResult checkInputData(CustomerProfile dto,String memberType,Long superiorMemberId) {
		if (dto.getGender() == null || "".equals(dto.getGender())){
			responseResult.initResult(GTAError.EnrollError.GENDER_REQ);
			return responseResult;
		}
		if (!"M".equals(dto.getGender()) && !"F".equals(dto.getGender())) {
			responseResult.initResult(GTAError.EnrollError.GENDER_SUGG);
			return responseResult;
		}
		List<CustomerAdditionInfo> ciList = dto.getCustomerAdditionInfos();
		if (ciList != null && ciList.size() > 0) {
			for (CustomerAdditionInfo info : ciList) {
				if (info.getCaptionId() == null) {
					responseResult.initResult(GTAError.EnrollError.CAPTION_REQ);
					return responseResult;
				}
				/*
				 * if (dto.getCustomerId() != null) { if (info.getSysId()==null)
				 * { return new ResponseMsg("1", "Sys Id is required!"); } }
				 */
				/*
				 * if(!CommUtil.notEmpty(info.getCustomerInput()+"")){ return
				 * new ResponseMsg("1","","Customer Input is required!"); }
				 */
			}
		}
		List<CustomerEnrollment> ceList = dto.getCustomerEnrollments();
		if (ceList != null && ceList.size() > 0) {
			for (CustomerEnrollment ce : ceList) {
				// if(ce.getSubscribePlanNo()==null){
				// return new ResponseMsg("1","Service Plan No is required!");
				// }
				if (dto.getCustomerId() != null) {
					if (ce.getEnrollId() == null) {
						responseResult.initResult(GTAError.EnrollError.ENROLL_ID_REQ);
						return responseResult;
					}
				}
				if (!CommUtil.notEmpty(ce.getSalesFollowBy())) {
					responseResult.initResult(GTAError.EnrollError.SALES_FOLLOW_REQ);
					return responseResult;
				}
			}
		}
		List<CustomerAddress> caList = dto.getCustomerAddresses();
		if ("false".equalsIgnoreCase(dto.getCheckBillingAddress())) {
			if (caList != null && caList.size() > 0) {
				for (CustomerAddress ca : caList) {
					if (!CommUtil.notEmpty(ca.getAddress1())) {
						responseResult.initResult(GTAError.EnrollError.ADDRESS_REQ);
						return responseResult;
					}
					if (!CommUtil.notEmpty(ca.getHkDistrict())) {
						responseResult.initResult(GTAError.EnrollError.DISTRICT_REQ);
						return responseResult;
					}
					if (!CommUtil.notEmpty(ca.getAddressType())) {
						responseResult.initResult(GTAError.EnrollError.ADDRESS_TYPE_REQ);
						return responseResult;
					}
				}
			}
		}

		StringBuilder ms = new StringBuilder();
		if (!CommUtil.notEmpty(dto.getPassportType())) {
			ms.append("Passport Type, ");
		}
		if (!CommUtil.notEmpty(dto.getPassportNo())) {
			ms.append("Passport No, ");
		}
		if (!CommUtil.notEmpty(dto.getSalutation())) {
			ms.append("Salutation, ");
		}
		if (!CommUtil.notEmpty(dto.getSurname())) {
			ms.append("Surname, ");
		}
		if (!CommUtil.notEmpty(dto.getGivenName())) {
			ms.append("Given Name, ");
		}
		if (!CommUtil.notEmpty(dto.getGender())) {
			ms.append("Gender, ");
		}
		if (!CommUtil.notEmpty(dto.getPhoneMobile())) {
			ms.append("Phone Mobile, ");
		}
		if (!CommUtil.notEmpty(dto.getContactEmail())) {
			ms.append("Contact Email, ");
		}
		if (!CommUtil.notEmpty(dto.getPostalAddress1())) {
			ms.append("Postal Address1, ");
		}
		if (!CommUtil.notEmpty(dto.getPostalDistrict())) {
			ms.append("Postal District, ");
		}
		if (!CommUtil.notEmpty(dto.getCheckBillingAddress())) {
			ms.append("CheckBillingAddress, ");
		}
		if (!CommUtil.notEmpty(dto.getDateOfBirth())) {
			ms.append("DateOfBirth, ");
		}
		if (!CommUtil.notEmpty(dto.getNationality())) {
			ms.append("Nationality, ");
		}
//		if (!CommUtil.notEmpty(dto.getSignature())) {
//			//allow no signature for individual member
//			if(!MemberType.IDM.name().equals(memberType)&&superiorMemberId==null){
//				ms.append("Signature, ");
//			}
//		}
		if (ms.length() > 0) {
			responseResult.initResult(GTAError.EnrollError.GENERAL_REQ,new String[]{ms.toString().substring(0, ms.length() - 2)});
			return responseResult;
		}

		if (!PassportType.HKID.name().equals(dto.getPassportType())
				&& !PassportType.VISA.name().equals(dto.getPassportType())) {
			responseResult.initResult(GTAError.EnrollError.PASS_TYPE_INCORR);
			return responseResult;
		}

		if (PassportType.HKID.name().equals(dto.getPassportType()) && !CommUtil.validateHKID(dto.getPassportNo())) {
			responseResult.initResult(GTAError.EnrollError.HKID_INVAILD);
			return responseResult;
		}
		if (PassportType.VISA.name().equals(dto.getPassportType()) && !CommUtil.validateVISA(dto.getPassportNo())) {
			responseResult.initResult(GTAError.EnrollError.PASSPORT_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
			responseResult.initResult(GTAError.EnrollError.MOBILE_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
			responseResult.initResult(GTAError.EnrollError.BUSINESS_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
			responseResult.initResult(GTAError.EnrollError.HOME_PHONE_INVALID);
			return responseResult;
		}
		if (!CommUtil.validateEmail(dto.getContactEmail())) {
			responseResult.initResult(GTAError.EnrollError.EMAIL_INVALID);
			return responseResult;
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public void updateEnrollmentStatusJob(Long enrollId, String status) {
		customerEnrollmentDao.updateStatus(status, enrollId);
	}
	
	@Override
	@Transactional
	public ResponseResult getAllSalesmen() {
		String sql = "select DISTINCT sp.user_id as userId, CONCAT(sp.given_name,' ',sp.surname) as staffName from  staff_profile sp, customer_enrollment ce where ce.sales_follow_by = sp.user_id";
		List<StaffDto> sales = customerEnrollmentDao.getDtoBySql(sql, null, StaffDto.class);
		responseResult.initResult(GTAError.Success.SUCCESS, sales);
		return responseResult;
	}
	
	@Override
	@Transactional
	public List<CustomerEnrollment> getEnrollmentMembersByStatus(String status){
		return customerEnrollmentDao.getEnrollmentMembersByStatus(status);
	}
	
	public Long recordCustomerEnrollmentUpdate(String statusFrom,String statusTo,Long customerId){
		CustomerEnrollmentLog enrollmentLog = new CustomerEnrollmentLog();
		CustomerEnrollment enrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if(enrollment!=null){
			enrollmentLog.setEnrollId(enrollment.getEnrollId());
			enrollmentLog.setStatusFrom(statusFrom);
			enrollmentLog.setStatusTo(statusTo);
			enrollmentLog.setStatusUpdateDate(new Date());
			enrollmentLog.setSubscribePlanNo(enrollment.getSubscribePlanNo());
			enrollmentLog.setSubscribeContractLength(enrollment.getSubscribeContractLength());
			enrollmentLog.setSalesFollowBy(enrollment.getSalesFollowBy());
			return (Long) customerEnrollmentLogDao.save(enrollmentLog);
		}else {
			return null;
		}
		
	}

	@Override
	@Transactional
	public String getEnrollmentRecordsSQL(String offerCode) {
		StringBuilder sql = new StringBuilder("SELECT * FROM ( ")
		.append(" SELECT trans.enrollDateDB, ")
		.append(" trans.enrollCreateDate, ")
		.append(" trans.enrollId2,trans.enrollId2 as enrollId, ")
		.append(" trans.memberName, ")
		.append(" trans.`status`, ")
//		.append(" CASE spop.offer_code WHEN 'RENEW' THEN(SELECT sp.pass_period_type FROM customer_order_det cod LEFT JOIN service_plan_offer_pos spop ON cod.item_no = spop.pos_item_no LEFT JOIN service_plan_pos spp ON spop.serv_pos_id = spp.serv_pos_id LEFT JOIN service_plan sp ON spp.plan_no = sp.plan_no WHERE cod.order_no = trans.order_no) ")
//		.append(" ELSE (SELECT sp.pass_period_type FROM customer_order_det cod LEFT JOIN service_plan_offer_pos spop ON cod.item_no = spop.pos_item_no LEFT JOIN service_plan_pos spp ON cod.item_no = spp.pos_item_no LEFT JOIN service_plan sp ON spp.plan_no = sp.plan_no WHERE cod.order_no = trans.order_no )END AS passPeriodType, ")
		.append(" sp.pass_period_type AS passPeriodType, ")
		.append(" concat(spr.given_name, ' ', spr.surname ) AS salesFollowBy, ")
		.append(" trans.customerId2, ")
		.append(" trans.customerId, ")
		.append(" trans.contactEmail, ")
		.append(" trans.order_no orderNo2, ")
		.append(" trans.academyNo academyNo, ")
		.append(" trans.failTransNo, ")
		.append(" trans.balance_due balanceDue, ")
		.append(" CASE trans.`status` WHEN 'TOA' THEN DATE_FORMAT(trans.effective_date,'%Y-%m-%d') ELSE NULL END AS effectiveDate, ")
		.append(" CASE trans.`status` WHEN 'TOA' THEN DATEDIFF(trans.effective_date, NOW()) ELSE NULL END AS noOfDays, ")
		.append(" trans.order_status orderStatus, ")
		.append(" spop.offer_code offerCode ")
		.append(" FROM (SELECT trans.*, enroll.sales_follow_by, enroll.enroll_date AS enrollDateDB, enroll.enroll_id AS enrollId2, enroll.create_date AS enrollCreateDate, concat(cp.salutation,' ',cp.given_name,' ',cp.surname) AS memberName,enroll.`status`,cp.customer_id AS customerId,cp.customer_id AS customerId2,cp.contact_email AS contactEmail,	mem. STATUS memberStatus, mem.academy_no academyNo, mem.effective_date, IFNULL(trans.fail_trans_no, 0) failTransNo ")
		.append(" FROM (SELECT h.order_no,h.order_date, h.customer_id, h.order_total_amount, ifnull(h.order_total_amount - p.paid,h.order_total_amount ) balance_due,h.order_status,n.fail_trans_no,h.update_date ")
		.append(" FROM (SELECT hd.*, plan.plan_no, plan.pass_nature_code ")
		.append(" FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN pos_service_item_price price ON price.item_no = pos.pos_item_no JOIN customer_order_det det ON det.item_no = price.item_no JOIN customer_order_hd hd ON hd.order_no = det.order_no WHERE plan.pass_nature_code = 'LT' ")
		.append(" UNION SELECT hd.*, plan.plan_no, plan.pass_nature_code ")
		.append(" FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN service_plan_offer_pos spop ON spop.serv_pos_id = pos.serv_pos_id JOIN pos_service_item_price price ON price.item_no = spop.pos_item_no JOIN customer_order_det det ON det.item_no = price.item_no JOIN customer_order_hd hd ON hd.order_no = det.order_no WHERE plan.pass_nature_code = 'LT' ")
		.append(" ) h LEFT JOIN ( SELECT order_item.order_no, sum(paid_amount) paid FROM (SELECT d.order_no, t.paid_amount, t.`status` FROM customer_order_det d, customer_order_trans t WHERE d.order_no = t.order_no ) order_item WHERE order_item. STATUS = 'SUC' GROUP BY order_item.order_no ")
		.append(" ) p ON h.order_no = p.order_no LEFT JOIN ( SELECT order_no, count(*) fail_trans_no FROM customer_order_trans WHERE customer_order_trans.`status` = 'FAIL' OR customer_order_trans.`status` = 'VOID' GROUP BY order_no ) n ON h.order_no = n.order_no ")
		.append(" ) trans LEFT JOIN customer_enrollment enroll ON trans.customer_id = enroll.customer_id and enroll.enroll_type = 'IPM' LEFT JOIN customer_profile cp ON trans.customer_id = cp.customer_id LEFT JOIN member mem ON trans.customer_id = mem.customer_id and mem.member_type = 'IPM') trans")
		.append(" JOIN customer_order_det cod ON trans.order_no = cod.order_no ")
		.append(" JOIN pos_service_item_price psip ON cod.item_no = psip.item_no ")
		.append(" JOIN staff_profile spr ON trans.sales_follow_by = spr.user_id ")
		.append(" LEFT OUTER JOIN service_plan_offer_pos spop ON cod.item_no = spop.pos_item_no ")
		//add replacement
		.append(" <passPeriodTypeForEnrollmentOrRenew> ")
		.append(" LEFT OUTER JOIN service_plan sp ON spp.plan_no = sp.plan_no ")
		.append(" WHERE psip.item_catagory = 'SRV' ")
		.append(" UNION SELECT custe.enroll_date enrollDateDB, custe.create_date enrollCreateDate, custe.enroll_id enrollId2,custe.enroll_id as enrollId, concat( custp.salutation, ' ', custp.given_name, ' ', custp.surname ) AS memberName, custe.`status`, svp.pass_period_type AS passPeriodType, concat( stapr.given_name, ' ', stapr.surname ) AS salesFollowBy, custe.customer_id customerId,custe.customer_id customerId2, custp.contact_email contactEmail, NULL AS orderNo2, NULL AS acdemyNo, 0 AS failTransNo, 0 AS balanceDue, null AS effectiveDate, NULL AS noOfDays, NULL AS orderStatus, NULL AS offerCode ")
		.append(" FROM customer_enrollment custe ")
		.append(" JOIN staff_profile stapr ON custe.sales_follow_by = stapr.user_id ")
		.append(" LEFT JOIN customer_profile custp ON custe.customer_id = custp.customer_id ")
		.append(" LEFT JOIN member mb ON custp.customer_id = mb.customer_id ")
		.append(" LEFT JOIN customer_service_acc custacc ON mb.customer_id = custacc.customer_id ")
		.append(" LEFT JOIN customer_service_subscribe custsub ON custacc.acc_no = custsub.acc_no ")
		.append(" LEFT JOIN service_plan svp ON custsub.service_plan_no = svp.plan_no ")
		.append(" LEFT JOIN customer_enrollment_log enrollmentLog ON enrollmentLog.enroll_id = custe.enroll_id ")
		.append(" WHERE enrollmentLog.status_update_date =  (  SELECT max(log.status_update_date) FROM customer_enrollment_log log  WHERE log.enroll_id = custe.enroll_id ) and ")
		.append(" mb.member_type = 'IPM' and custe.enroll_type = 'IPM' AND (custe.`status` = 'NEW' or (custe.`status` = 'REJ' and enrollmentLog.status_from = 'NEW' ) or (custe.`status` = 'CAN' and enrollmentLog.status_from = 'NEW' )) ")
		.append(" ) t WHERE 1 = 1 " );
		
		String sqlStatement = sql.toString();
		String passPeriodTypeForEnrollment = "LEFT OUTER JOIN service_plan_pos spp ON cod.item_no = spp.pos_item_no";
		String passPeriodTypeForRenew = "LEFT OUTER JOIN service_plan_pos spp ON spop.serv_pos_id = spp.serv_pos_id";
		
		if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)) {
			sqlStatement = sqlStatement.replace("<passPeriodTypeForEnrollmentOrRenew>", passPeriodTypeForRenew);
		}else if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)){
			sqlStatement = sqlStatement.replace("<passPeriodTypeForEnrollmentOrRenew>", passPeriodTypeForEnrollment);
		}
		
		return sqlStatement;
	}

	@Override
	@Transactional
	public ResponseResult getDailyEnrollment(ListPage page, String enrollDate) {
		ListPage<CustomerEnrollment> listPage = customerEnrollmentDao.getDailyEnrollment(page, enrollDate);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	@Transactional
	public byte[] getDailyEnrollmentAttach(String enrollDate,String fileType,String sortBy,String isAscending) throws JRException {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/"+"jasper/";
		String parentjasperPath = reportPath+"DailyEnrollment.jasper";

		File reFile = new File(parentjasperPath);
		Map<String,Object> parameters = new HashMap<String,Object>();
		DataSource ds=SessionFactoryUtils.getDataSource(customerProfileDao.getsessionFactory());
	    Connection dbconn=DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("enrollDate", enrollDate);
		parameters.put("fileType", fileType);
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if ("true".equalsIgnoreCase(isAscending)) {
			sortField.setOrder(SortOrderEnum.ASCENDING);
		} else {
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		
		//Ignore pagination for csv
		if ("csv".equals(fileType)) {
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
				
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType,jasperPrint,outPut);
		exporter.exportReport();
		return outPut.toByteArray();
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JRAbstractExporter exportedByFileType(String fileType, JasperPrint jasperPrint, ByteArrayOutputStream outPut) {
		JRAbstractExporter exporter = null;
		switch (fileType) {
		case "pdf":
			exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
			break;
		case "csv":
			exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
			break;
		default:
		}

		return exporter;
	}

	@Override
	@Transactional
	public ResponseResult getDailyLeads(ListPage page, String selectedDate) {
		ListPage<CustomerEnrollment> listPage = customerEnrollmentDao.getDailyLeads(page, selectedDate);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Override
	@Transactional
	public byte[] getDailyLeadsAttach(String selectedDate, String fileType, String sortBy, String isAscending) throws JRException{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/"+"jasper/";
		String parentjasperPath = reportPath+"DailyGeneratedLeads.jasper";

		File reFile = new File(parentjasperPath);
		Map<String,Object> parameters = new HashMap<String,Object>();
		DataSource ds=SessionFactoryUtils.getDataSource(customerProfileDao.getsessionFactory());
	    Connection dbconn=DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("selectedDate", selectedDate);
		
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if ("true".equalsIgnoreCase(isAscending)) {
			sortField.setOrder(SortOrderEnum.ASCENDING);
		} else {
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		
		//Ignore pagination for csv
		if ("csv".equalsIgnoreCase(fileType)) {
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
				
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType,jasperPrint,outPut);
		exporter.exportReport();
		return outPut.toByteArray();
	}
}