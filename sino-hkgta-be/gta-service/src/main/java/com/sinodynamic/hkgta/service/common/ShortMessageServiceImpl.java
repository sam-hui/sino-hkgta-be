package com.sinodynamic.hkgta.service.common;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinodynamic.hkgta.notification.SMSService;

@Service("SMSServiceLayer")
public class ShortMessageServiceImpl implements ShortMessageService {
    
    	private Logger logger = Logger.getLogger(ShortMessageServiceImpl.class);
    	
	@Autowired
	SMSService smsService;
	
	private static ExecutorService pool = Executors.newCachedThreadPool();

	@Override
	public void sendSMS(final List<String> phonenumbers, final String content, final Date sendDateTime)
	{
	    
	    Runnable smsTask = new Runnable() {
		
	        public void run() {
	            
	            try {
	        	smsService.sendSMS(phonenumbers, content, sendDateTime);
		    } catch (Exception e) {
			logger.error(e.getMessage(), e);
		    }
	        }
	    };
	    
	    pool.execute(smsTask);
	}

}
