package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.fms.CourseEnrollmentDao;
import com.sinodynamic.hkgta.dao.fms.CourseMasterDao;
import com.sinodynamic.hkgta.dao.fms.CourseSessionDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.dao.fms.FacilityMasterDao;
import com.sinodynamic.hkgta.dao.fms.FacilitySubTypeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationPosDao;
import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationRateDateDao;
import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationRateTimeDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityAttendanceDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedFacilityDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedStaffDao;
import com.sinodynamic.hkgta.dao.fms.PrivateCoachBookListDtoDao;
import com.sinodynamic.hkgta.dao.fms.StaffFacilityScheduleDao;
import com.sinodynamic.hkgta.dao.fms.StaffTimeslotDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.fms.CancelDto;
import com.sinodynamic.hkgta.dto.fms.CustomerFacilityBookingDetail;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotQueryDto;
import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.dto.membership.FacilityTransactionInfo;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilitySubTypePos;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationPos;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateDate;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttrPK;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;
import com.sinodynamic.hkgta.entity.fms.MemberReservedStaff;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.crm.backoffice.coachmanage.CoachManagementService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CourseType;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.FacilityName;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.FacilitySubType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberFacilityTypeBookingStatus;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.RateType;
import com.sinodynamic.hkgta.util.constant.RefundPeriodType;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.constant.ServicePlanRight;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class MemberFacilityTypeBookingServiceImpl extends ServiceBase<MemberFacilityTypeBooking> implements MemberFacilityTypeBookingService
{
	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;

	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;

	@Autowired
	private FacilityUtilizationRateDateDao facilityUtilizationRateDateDao;

	@Autowired
	private FacilityUtilizationPosDao facilityUtilizationPosDao;

	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;

	@Autowired
	private FacilityUtilizationRateTimeDao facilityUtilizationRateTimeDao;

	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;

	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;

	@Autowired
	private FacilityTimeslotDao facilityTimeslotDao;

	@Autowired
	private MemberReservedFacilityDao memberReservedFacilityDao;

	@Autowired
	private FacilityMasterDao facilityMasterDao;
	
	@Autowired
	private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private MemberFacilityAttendanceDao memberFacilityAttendanceDao;
	
	@Autowired
	private StaffFacilityScheduleDao staffFacilityScheduleDao;
	
  	@Autowired
	private GlobalParameterDao globalParameterDao;
  	
  	@Autowired
	private MemberCashvalueService memberCashValueService;

  	@Autowired
  	private PrivateCoachBookListDtoDao privateCoachBookListDtoDao ;
  	
  	@Autowired
	private CustomerEmailContentService customerEmailContentService;

	@Autowired
	private MessageTemplateService templateService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	@Autowired
	private FacilityAttributeCaptionDao facilityAttributeCaptionDao;
	@Autowired
	private StaffTimeslotDao staffTimeslotDao;
	@Autowired
	private UserMasterDao userMasterDao;
	@Autowired
	private CoachManagementService  coachManagementService;
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	@Autowired
	private MemberReservedStaffDao  memberReservedStaffDao;
	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;
	@Autowired
	private MessageTemplateDao templateDao;
	
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private CourseMasterDao courseMasterDao;
	
	@Autowired
	private CourseEnrollmentDao courseEnrollmentDao;
	
	@Autowired
	private FacilitySubTypePosService facilitySubTypePosService;
	
	@Autowired
	private FacilityEmailService facilityEmailService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	@Autowired
	private FacilitySubTypeDao facilitySubTypeDao;
	@Autowired
	private MailThreadService mailThreadService;

	@Autowired
	private CourseSessionDao courseSessionDao;
	
	@Autowired
	private FacilityMasterService facilityMasterService;
	
	@Autowired
	private FacilityCheckInService facilityCheckInService;
	
	@Autowired
	private FacilityTypeQuotaService facilityTypeQuotaService;
	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;
	@Autowired
	private RoomReservationRecDao roomReservationRecDao;
	
	@Autowired
	private UsageRightCheckService usageRightCheckService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public MemberFacilityTypeBooking getMemberFacilityTypeBooking(long resvId)
	{
		
		return memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Member checkMemberStatus(Long customerId) {
		Member member = memberDao.getMemberByCustomerId(customerId);
		if(member != null && member.getStatus().equals(Constant.General_Status_NACT))
			throw new GTACommonException(GTAError.FacilityError.MEMBER_NOT_ACTIVE );
		return member;
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GTACommonException.class)
	public MemberFacilityBookingDto createMemberFacilityTypeBooking(MemberFacilityBookingDto bookingDto,boolean isMemberApp,List<Long> facilityNos) throws Exception
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try
		{
			Member member = checkMemberStatus(bookingDto.getCustomerId());
			bookingDto.setAcademyNo(member.getAcademyNo());
			
			//checking corporate permission
			checkFacilityRight(bookingDto);
			
			Date bookingDate = format.parse(bookingDto.getBookingDate());
			if (isMemberApp)
			{
				checkMemberBookingCondition(bookingDto, bookingDate);
				
				checkMemberBookingQuota(bookingDto);
				
				if (StringUtils.isEmpty(bookingDto.getReserveVia()))
					bookingDto.setReserveVia("APS");
			}
			else if (StringUtils.isEmpty(bookingDto.getReserveVia()))
				bookingDto.setReserveVia("FD");
			
			checkFacilityBookingDate(bookingDto);
			List<FacilityMaster> facilityMasters = checkFacilityCount(bookingDto, sf, false,facilityNos);

			Map<String, Object> map = calculateFacilityBookingPrice(bookingDto, bookingDate, bookingDto.getCount());
			BigDecimal totalPrice = ((BigDecimal) map.get("itemTotalPrice")).multiply(new BigDecimal(bookingDto.getCount()));
			MemberCashValuePaymentDto paymentDto = buildMemberCashValuePaymentDto(bookingDto, isMemberApp, (Map<String, Integer>) map.get("itemNosMap"), totalPrice);
			CustomerOrderTransDto orderTransDto = null;
			if (bookingDto.getPaymentMethod().equals(Constant.CASH_Value))
				orderTransDto = memberCashValueService.paymentByMemberCashvalue(paymentDto);
			else
				orderTransDto = customerOrderTransService.payment(paymentDto);

			if (null != orderTransDto)
			{
				MemberFacilityTypeBooking booking = saveFacilityBookingData(bookingDto, sf, totalPrice, facilityMasters, orderTransDto.getOrderNo(),Constant.RESERVE_TYPE_MR);
				if (null != booking)
				{
					if (Constant.FACILITY_TYPE_GOLF.equals(bookingDto.getFacilityType().toUpperCase()))
						saveMemberFacilityBookAdditionAttr(bookingDto, booking.getResvId());

					bookingDto.setAvailableBalance(orderTransDto.getAvailableBalance());
					bookingDto.setResvId(booking.getResvId());
					bookingDto.setTransactionId(orderTransDto.getTransactionNo());
					bookingDto.setOrderNo(orderTransDto.getOrderNo());
					bookingDto.setTotalPrice(totalPrice);
					if (bookingDto.getPaymentMethod().equals(Constant.CREDIT_CARD))
					{
						CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, bookingDto.getTransactionId());
						String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
						bookingDto.setRedirectUrl(redirectUrl);
					}
				}
				checkFacilityCount(bookingDto, sf, true,facilityNos);
			}
		}
		catch (GTACommonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new GTACommonException(e);
		}
		return bookingDto;
	}
	private void checkMemberBookingQuota(MemberFacilityBookingDto bookingDto) throws Exception, ParseException
	{
		if(Constant.FACILITY_TYPE_GOLF.equals(bookingDto.getFacilityType())){
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Long quota = facilityTypeQuotaService.getFacilityQuota(bookingDto.getFacilityType(),DateCalcUtil.parseDate(bookingDto.getBookingDate()));
			for (int i = bookingDto.getBeginTime(); i <= bookingDto.getEndTime(); i++)
			{
				if(null != quota){
					Date bookingDateTime = sf.parse((bookingDto.getBookingDate() + " " + i + ":00:00"));
					int quotaResCount = memberFacilityTypeBookingDao.getMemberFacilityBookedCountForQuota(bookingDto.getFacilityType(),bookingDateTime);
					if((quota - quotaResCount) < bookingDto.getCount())
						throw new GTACommonException(GTAError.FacilityError.RESVCOUNT_MORETHAN_QUOTA);
				}
			}
		}
	}
	private void checkMemberBookingCondition(MemberFacilityBookingDto bookingDto, Date bookingDate) throws Exception
	{
		if ((bookingDto.getEndTime() - bookingDto.getBeginTime()) > 1)
			throw new GTACommonException(GTAError.FacilityError.RESERVATIONTIME_MORETHANTWOHOURS);
		else if (bookingDto.getCount() > 2)
			throw new GTACommonException(GTAError.FacilityError.BAYCOUNT_MORETHAN_TWO);
		if (null != bookingDto.getCustomerId() && bookingDto.getCustomerId() > 0)
		{
			int count = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingBayCount(bookingDto.getCustomerId(), bookingDate, bookingDto.getFacilityType());
			if(count >=2){
				throw new GTACommonException(GTAError.CoachMgrError.Over_Times);
			}
			
			/*List<MemberFacilityTypeBooking> memberFacilityTypeBookingList = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(bookingDto.getCustomerId(), bookingDate, bookingDto.getFacilityType());
			if (null != memberFacilityTypeBookingList && memberFacilityTypeBookingList.size() >= 2){
				throw new GTACommonException(GTAError.CoachMgrError.Over_Times);
			}*/
		}
	}
	private MemberCashValuePaymentDto buildMemberCashValuePaymentDto(MemberFacilityBookingDto bookingDto, boolean isMemberApp, Map<String, Integer> itemNosMap, BigDecimal totalPrice)
	{
		MemberCashValuePaymentDto paymentDto = new MemberCashValuePaymentDto();
		paymentDto.setCustomerId(bookingDto.getCustomerId());
		paymentDto.setItemNoMap(itemNosMap);
		paymentDto.setPaymentMethod(bookingDto.getPaymentMethod());
		paymentDto.setTotalAmount(totalPrice);
		paymentDto.setUserId(bookingDto.getUpdateBy());
		paymentDto.setIsOnlinePayment(isMemberApp);
		paymentDto.setLocation(bookingDto.getLocation());
		return paymentDto;
	}

	

	private void saveMemberFacilityBookAdditionAttr(MemberFacilityBookingDto bookingDto, long resvId)
	{
		MemberFacilityBookAdditionAttrPK pk = new MemberFacilityBookAdditionAttrPK();
		pk.setResvId(resvId);
		pk.setAttributeId(bookingDto.getAttributeType());
		MemberFacilityBookAdditionAttr memberFacilityBookAdditionAttr = memberFacilityBookAdditionAttrDao.get(MemberFacilityBookAdditionAttr.class, pk);
		if (null == memberFacilityBookAdditionAttr)
		{
			memberFacilityBookAdditionAttr = new MemberFacilityBookAdditionAttr();
			memberFacilityBookAdditionAttr.setId(pk);
		}
		memberFacilityBookAdditionAttr.setAttrValue(bookingDto.getAttributeType());
		memberFacilityBookAdditionAttr.setFacilityType(bookingDto.getFacilityType());
		memberFacilityBookAdditionAttrDao.saveOrUpdate(memberFacilityBookAdditionAttr);
	}

	private Map<String, Object> calculateFacilityBookingPrice(MemberFacilityBookingDto bookingDto, Date bookingDate,Integer orderQty) throws GTACommonException
	{
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> serviceItemNos = new ArrayList<String>();
		List<BigDecimal> serviceItemCountList = new ArrayList<BigDecimal>();
		List<String> serviceItemDescriptionList = new ArrayList<String>();
		List<BigDecimal> serviceItemPriceList = new ArrayList<BigDecimal>();
		Map<String, Integer> itemNosMap = new HashMap<String, Integer>();
		BigDecimal itemTotalPrice = BigDecimal.ZERO;
		FacilityUtilizationRateDate rateDate = facilityUtilizationRateDateDao.getSpecialRateDate(bookingDto.getFacilityType(), bookingDate, Constant.FACILITY_TYPE_GOLF.equals(bookingDto.getFacilityType().toUpperCase()) ? null : bookingDto.getAttributeType());
		if (null != rateDate)
		{
			List<FacilityUtilizationRateTime> facilityUtilizationRateTimes = rateDate.getFacilityUtilizationRateTimes();
			for (int time = bookingDto.getBeginTime(); time <= bookingDto.getEndTime(); time++)
			{
				String rateType = getFacilityRateType(facilityUtilizationRateTimes, time, bookingDto);
				itemTotalPrice = buildFacilityItemPrice(bookingDto, serviceItemNos, serviceItemCountList, serviceItemDescriptionList, serviceItemPriceList, itemNosMap, itemTotalPrice, rateType);
			}
		}
		else
		{
			String weekDay = getWeekOfDate(bookingDate) + "";
			for (int time = bookingDto.getBeginTime(); time <= bookingDto.getEndTime(); time++)
			{
				FacilityUtilizationRateTime rateTime = facilityUtilizationRateTimeDao.getFacilityUtilizationRateTime(bookingDto.getFacilityType(), weekDay, time, Constant.FACILITY_TYPE_GOLF.equals(bookingDto.getFacilityType().toUpperCase()) ? null : bookingDto.getAttributeType());
				String rateType = "";
				if (null != rateTime)
					rateType = rateTime.getRateType();
				itemTotalPrice = buildFacilityItemPrice(bookingDto, serviceItemNos, serviceItemCountList, serviceItemDescriptionList, serviceItemPriceList, itemNosMap, itemTotalPrice, rateType);
			}
		}
		map.put("serviceItemDescription", serviceItemDescriptionList);
		map.put("serviceItemCount", serviceItemCountList);
		map.put("serviceItemPrices", serviceItemNos);
		map.put("serviceItemPriceList", serviceItemPriceList);
		map.put("itemTotalPrice", itemTotalPrice);
		for (Map.Entry<String, Integer> entry : itemNosMap.entrySet())
		{
			itemNosMap.put(entry.getKey(), entry.getValue() * orderQty);
		}
		map.put("itemNosMap", itemNosMap);
		return map;
	}
	private int getWeekOfDate(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}
	private String getFacilityRateType(List<FacilityUtilizationRateTime> facilityUtilizationRateTimes, int beginTime,MemberFacilityBookingDto bookingDto)
	{
		String rateType = "";
		for (FacilityUtilizationRateTime rateTime : facilityUtilizationRateTimes)
		{
			if (rateTime.getBeginTime() == beginTime)
			{
				if (Constant.FACILITY_TYPE_GOLF.equals(bookingDto.getFacilityType().toUpperCase()) || (Constant.FACILITY_TYPE_TENNIS.equals(bookingDto.getFacilityType().toUpperCase()) && bookingDto.getAttributeType().equals(rateTime.getFacilitySubtypeId())))
				{
					rateType = rateTime.getRateType();
					break;
				}
			}
		}
		return rateType;
	}
	private BigDecimal buildFacilityItemPrice(MemberFacilityBookingDto bookingDto, List<String> serviceItemNos, List<BigDecimal> serviceItemCountList, List<String> serviceItemDescriptionList, List<BigDecimal> serviceItemPriceList, Map<String, Integer> itemNosMap, BigDecimal itemTotalPrice, String rateType)
	{
		PosServiceItemPrice serviceItemPrice = null;
		if (Constant.FACILITY_TYPE_GOLF.equals(bookingDto.getFacilityType().toUpperCase()))
		{
			FacilityUtilizationPos utilizationPos = facilityUtilizationPosDao.getFacilityUtilizationPos(bookingDto.getFacilityType(), bookingDto.getAgeRangeCode(), StringUtils.isEmpty(rateType) ? RateType.LOW.getDesc() : rateType);
			if (null != utilizationPos)
			{
				serviceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, utilizationPos.getPosItemNo());
				if (null == serviceItemPrice)
					throw new GTACommonException(GTAError.FacilityError.FACILITY_PRICE_NOT_FOUND);
			}
			else
				throw new GTACommonException(GTAError.FacilityError.FACILITY_PRICE_NOT_FOUND);
		}
		else
		{
			FacilitySubTypePos subTypePos = facilitySubTypePosService.getFacilitySubTypePos(bookingDto.getAttributeType(), bookingDto.getAgeRangeCode(), StringUtils.isEmpty(rateType) ? RateType.LOW.getDesc() : rateType);
			if (null != subTypePos)
			{
				serviceItemPrice = subTypePos.getPosItemPrice();
				if (null == serviceItemPrice)
					throw new GTACommonException(GTAError.FacilityError.FACILITY_PRICE_NOT_FOUND);
			}
			else
				throw new GTACommonException(GTAError.FacilityError.FACILITY_PRICE_NOT_FOUND);
		}
		itemTotalPrice = itemTotalPrice.add(serviceItemPrice.getItemPrice());
		if (!serviceItemNos.contains(serviceItemPrice.getItemNo()))
		{
			serviceItemNos.add(serviceItemPrice.getItemNo());
			serviceItemCountList.add(new BigDecimal(1));
			serviceItemDescriptionList.add(serviceItemPrice.getDescription());
			serviceItemPriceList.add(serviceItemPrice.getItemPrice());
			itemNosMap.put(serviceItemPrice.getItemNo(), 1);
		}
		else
		{
			int index = serviceItemNos.indexOf(serviceItemPrice.getItemNo());
			serviceItemCountList.set(index, serviceItemCountList.get(index).add(new BigDecimal(1)));
			Integer itemQty = itemNosMap.get(serviceItemPrice.getItemNo());
			itemNosMap.put(serviceItemPrice.getItemNo(), itemQty + 1);
		}
		return itemTotalPrice;
	}

	private MemberFacilityTypeBooking saveFacilityBookingData(MemberFacilityBookingDto bookingDto,SimpleDateFormat sf, BigDecimal totalPrice,List<FacilityMaster> facilityMasters,Long orderNo,String reserveType) throws ParseException
	{
		List<Long> timeslotIds = new ArrayList<Long>();
		Timestamp currentTimestamp= new Timestamp(Calendar.getInstance().getTimeInMillis());
		for (int c = 0; c < bookingDto.getCount(); c++)
		{
			FacilityMaster facilityMaster =facilityMasters.get(c);
			for (int i = bookingDto.getBeginTime(); i <= bookingDto.getEndTime(); i++)
			{
				String startdateStr = bookingDto.getBookingDate() + " " + i + ":00:00";
				String enddateStr = bookingDto.getBookingDate() + " " + i + ":59:59";
				FacilityTimeslot timesLot = createTimeslot(bookingDto.getUpdateBy(), sf.parse(startdateStr), sf.parse(enddateStr),facilityMaster,currentTimestamp,reserveType);
				timeslotIds.add(timesLot.getFacilityTimeslotId());
			}
		}
		MemberFacilityTypeBooking booking = createMemberFacilityBooking(bookingDto, sf, orderNo);
		if (null != timeslotIds && timeslotIds.size() > 0)
		{
			for (Long timeslotId : timeslotIds)
			{
				MemberReservedFacility memberReservedFacility = new MemberReservedFacility();
				memberReservedFacility.setFacilityTimeslotId(timeslotId);
				memberReservedFacility.setResvId(booking.getResvId());
				memberReservedFacilityDao.save(memberReservedFacility);
			}
		}
		return booking;
	}

	private List<FacilityMaster> checkFacilityCount(MemberFacilityBookingDto bookingDto, SimpleDateFormat sf,boolean isEnd,List<Long> facilityNos) throws ParseException, Exception
	{
		int facilityCount = facilityMasterDao.getFacilityMasterCount(bookingDto.getFacilityType(),bookingDto.getAttributeType(),null,facilityNos);
		
		Date[] dateTimes = new Date[(bookingDto.getEndTime() - bookingDto.getBeginTime()) + 1];
		int time = 0;
		for (int i = bookingDto.getBeginTime(); i <= bookingDto.getEndTime(); i++)
		{
			String startdateStr = bookingDto.getBookingDate() + " " + i + ":00:00";
			dateTimes[time] = sf.parse(startdateStr);
			time++;
		}
		List<FacilityTimeslotQueryDto> queryDtos = facilityTimeslotDao.getFacilityTimeslotGroupTimeCountFacilityAttribute(bookingDto.getFacilityType(), dateTimes,bookingDto.getAttributeType(),null,facilityNos);
		if (null != queryDtos && queryDtos.size() > 0)
		{
			for (FacilityTimeslotQueryDto timeslotDto : queryDtos)
			{
				if(isEnd){
					if (timeslotDto.getFacilityCount().intValue() > facilityCount)
					{
							throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT );
					}
				}else if (bookingDto.getCount() > (facilityCount-timeslotDto.getFacilityCount().intValue()))
				{
						throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT );
				}
			}
		}
		List<FacilityMaster> facilityMasters = null;
		if(!isEnd){
			facilityMasters = facilityMasterDao.getFacilityMasterAvailableList(bookingDto.getFacilityType(), bookingDto.getAttributeType(), dateTimes,null,facilityNos);
			if(null == facilityMasters || facilityMasters.size() < bookingDto.getCount()){
				throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT );
			}
		}
		return facilityMasters;
	}
	
	private List<FacilityMaster> checkFacilityCount(MemberFacilityBookingDto bookingDto, SimpleDateFormat sf,boolean isEndCheck) throws ParseException, Exception
	{
		return this.checkFacilityCount(bookingDto, sf, isEndCheck, null);
	}

	public MemberFacilityTypeBooking createMemberFacilityBooking(MemberFacilityBookingDto bookingDto, SimpleDateFormat sf, Long orderNo) throws ParseException
	{
		String startdateStr = bookingDto.getBookingDate() + " " + bookingDto.getBeginTime() + ":00:00";
		String enddateStr = bookingDto.getBookingDate() + " " + bookingDto.getEndTime() + ":59:59";
		MemberFacilityTypeBooking newBooking = new MemberFacilityTypeBooking();
		newBooking.setOrderNo(orderNo);
		newBooking.setCustomerId(bookingDto.getCustomerId());
		newBooking.setResvFacilityType(bookingDto.getFacilityType());
		newBooking.setFacilityTypeQty(bookingDto.getCount().longValue());
		if (bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CREDIT_CARD) || bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.PaymentMethodCode.VISA.toString()) || bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.PaymentMethodCode.MASTER.toString()))
		{
			newBooking.setStatus(MemberFacilityTypeBookingStatus.PND.toString());
		}
		else
			newBooking.setStatus(MemberFacilityTypeBookingStatus.RSV.toString());

		if (Constant.FACILITY_TYPE_TENNIS.equals(bookingDto.getFacilityType().toUpperCase()))
			newBooking.setFacilitySubtypeId(bookingDto.getAttributeType());
		newBooking.setReserveVia(bookingDto.getReserveVia());
		newBooking.setCreateDate(new Timestamp(new Date().getTime()));
		newBooking.setCreateBy(bookingDto.getUpdateBy());
		newBooking.setBeginDatetimeBook(new Timestamp(sf.parse(startdateStr).getTime()));
		newBooking.setEndDatetimeBook(new Timestamp(sf.parse(enddateStr).getTime()));
		memberFacilityTypeBookingDao.save(newBooking);
		return newBooking;
	}

	private FacilityTimeslot createTimeslot(String updateBy,Date startdate, Date enddate,FacilityMaster facilityMaster,Timestamp currentTimestamp,String reserveType) throws ParseException
	{
		FacilityTimeslot timeslot = new FacilityTimeslot();
		timeslot.setFacilityMaster(facilityMaster);
		timeslot.setBeginDatetime(startdate);
		timeslot.setEndDatetime(enddate);
		timeslot.setStatus(FacilityStatus.TA.toString());
		timeslot.setCreateBy(updateBy);
		timeslot.setCreateDate(currentTimestamp);
		timeslot.setReserveType(reserveType);
		facilityTimeslotDao.save(timeslot);
		return timeslot;
	}
	
	private FacilityTimeslot createTimeslot(String updateBy,Date startdate, Date enddate,FacilityMaster facilityMaster,Timestamp currentTimestamp,String reserveType, Long transferTimeslotId) throws ParseException
	{
		FacilityTimeslot timeslot = new FacilityTimeslot();
		timeslot.setFacilityMaster(facilityMaster);
		timeslot.setBeginDatetime(startdate);
		timeslot.setEndDatetime(enddate);
		timeslot.setStatus(FacilityStatus.TA.toString());
		timeslot.setCreateBy(updateBy);
		timeslot.setCreateDate(currentTimestamp);
		timeslot.setReserveType(reserveType);
		timeslot.setTransferFromTimeslotId(transferTimeslotId);
		facilityTimeslotDao.save(timeslot);
		return timeslot;
	}
	
	private StaffTimeslot createStaffTimeslot(MemberFacilityBookingDto bookingDto,
			Date beginDatetime, Date endDatetime, Timestamp currentTimestamp) {
		StaffTimeslot staffTimeslot = new StaffTimeslot();
		staffTimeslot.setStaffUserId(bookingDto.getCoachId());
		staffTimeslot.setStatus(Constant.Status.OP.toString());
		staffTimeslot.setBeginDatetime(beginDatetime);
		staffTimeslot.setEndDatetime(endDatetime);
		//staffTimeslot.setRevId(bookingDto.getResvId());
		if("GOLF".equals(bookingDto.getFacilityType())){
			staffTimeslot.setDutyCategory(Constant.duty_category.PVGCOACH.toString());
			staffTimeslot.setDutyDescription("staff is occupied for golf");
		}else if("TENNIS".equals(bookingDto.getFacilityType())){
			staffTimeslot.setDutyCategory(Constant.duty_category.PVTCOACH.toString());
			staffTimeslot.setDutyDescription("staff is occupied for tennis");
		}
		staffTimeslot.setCreateDate(currentTimestamp);
		staffTimeslot.setCreateBy(bookingDto.getUpdateBy());
		this.staffTimeslotDao.save(staffTimeslot);
		return staffTimeslot;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Long finalizeMemberFacilityTypeBooking(Long transactionNo, String updateBy, String responseCode, String traceNumber, String remark, String cardType)
 {
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		if (null != customerOrderTrans && !Constant.Status.CMP.toString().equals(customerOrderTrans.getStatus())) {
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
			customerOrderHd.setUpdateBy(updateBy);
			customerOrderHd.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderHdDao.update(customerOrderHd);
			customerOrderTrans.setOpgResponseCode(responseCode);
			customerOrderTrans.setAgentTransactionNo(traceNumber);
			customerOrderTrans.setInternalRemark(remark);
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
			customerOrderTrans.setAuditBy(updateBy);
			customerOrderTrans.setAuditDate(new Timestamp(new Date().getTime()));
			if (cardType != null && cardType.length() > 0) {
				customerOrderTrans.setPaymentMethodCode(cardType);
			}
			customerOrderTransDao.update(customerOrderTrans);
			
			if (customerOrderTrans.getPaymentMethodCode().equalsIgnoreCase(Constant.PaymentMethodCode.VISA.toString())
					|| customerOrderTrans.getPaymentMethodCode()
							.equalsIgnoreCase(Constant.PaymentMethodCode.MASTER.toString())) {
				MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
						.getMemberFacilityTypeBookingByOrderNo(customerOrderHd.getOrderNo());

				if (null != memberFacilityTypeBooking) {
					if (MemberFacilityTypeBookingStatus.CAN.name().equals(memberFacilityTypeBooking.getStatus())) {
						String reason = "refund after cancel";
						sendRefundRequestFacility(memberFacilityTypeBooking, Constant.BizParties.HKGTA.toString(), reason, updateBy);
					} else {
						memberFacilityTypeBooking.setStatus(MemberFacilityTypeBookingStatus.RSV.toString());
						memberFacilityTypeBookingDao.update(memberFacilityTypeBooking);
					}
				}
				try {
					facilityEmailService.sendReceiptConfirmationEmail(memberFacilityTypeBooking.getResvId(),updateBy);
				} catch (Exception e) {
					logger.debug("Occurrd exception when send email for facility reservation!", e);
				}

			}
			return customerOrderTrans.getTransactionNo();
		}
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Long voidMemberFacilityTypeBooking(Long orderNo, String updateBy)
	{
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if(null != customerOrderHd){
			MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingByOrderNo(customerOrderHd.getOrderNo());
			if (null != memberFacilityTypeBooking)
			{
				List<MemberFacilityBookAdditionAttr> memberFacilityBookAdditionAttrs =  memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttrList(memberFacilityTypeBooking.getResvId());
				if(null != memberFacilityBookAdditionAttrs && memberFacilityBookAdditionAttrs.size() >0){
					for(MemberFacilityBookAdditionAttr attr : memberFacilityBookAdditionAttrs){
						memberFacilityBookAdditionAttrDao.delete(attr);
					}
				}
				List<MemberReservedFacility> memberReservedFacilitys = memberReservedFacilityDao.getMemberReservedFacilityList(memberFacilityTypeBooking.getResvId());
				List<MemberReservedStaff> memberReservedStaffs = memberReservedStaffDao.getByHql("from MemberReservedStaff where resvId = ?", Arrays.<Serializable>asList(memberFacilityTypeBooking.getResvId()));
				
				
				
				if( null != memberReservedFacilitys && memberReservedFacilitys.size() >0) {
					
					for (MemberReservedFacility memberReservedFacility : memberReservedFacilitys)
					{
						FacilityTimeslot facilityTimeslot = facilityTimeslotDao.get(FacilityTimeslot.class, memberReservedFacility.getFacilityTimeslotId());
						if (null != facilityTimeslot)
						{
							List<MemberFacilityAttendance> memberFacilityAttendances = memberFacilityAttendanceDao.getMemberFacilityAttendanceList(facilityTimeslot.getFacilityTimeslotId(),0);
							if(null != memberFacilityAttendances && memberFacilityAttendances.size() >0){
								for(MemberFacilityAttendance attendance : memberFacilityAttendances ){
									memberFacilityAttendanceDao.delete(attendance);
								}
							}
							//memberReservedFacilityDao.delete(memberReservedFacility);
							facilityTimeslot.getFacilityMaster().removeFacilityTimeslot(facilityTimeslot);
							facilityTimeslot.setFacilityTimeslotAddition(null);
							facilityTimeslot.setStaffFacilitySchedule(null);
							facilityTimeslot.setMemberFacilityTypeBooking(null);
							facilityTimeslotDao.delete(facilityTimeslot);
						}
					}
				}
				
				if( null != memberReservedStaffs && memberReservedStaffs.size() >0) {
					for (MemberReservedStaff memberReservedStaff : memberReservedStaffs){
						StaffTimeslot staffTimeslot = staffTimeslotDao.get(StaffTimeslot.class, memberReservedStaff.getStaffTimeslotId());
						if (null != staffTimeslot){
							staffTimeslot.setMemberFacilityTypeBooking(null);
							staffTimeslotDao.delete(staffTimeslot);
						}
					}
				}

				memberFacilityTypeBooking.setStatus(MemberFacilityTypeBookingStatus.CAN.toString());
				memberFacilityTypeBookingDao.update(memberFacilityTypeBooking);
				List<CustomerOrderTrans> customerOrderTrans = customerOrderHd.getCustomerOrderTrans();
				if (null != customerOrderTrans && customerOrderTrans.size() > 0){
					CustomerOrderTrans customerOrderTran = customerOrderTrans.get(customerOrderTrans.size()-1);
					customerOrderTran.setStatus(Constant.Status.FAIL.toString());
					customerOrderTran.setAuditBy(updateBy);
					customerOrderTran.setAuditDate(new Timestamp(new Date().getTime()));
					customerOrderTransDao.update(customerOrderTran);
				}
				customerOrderHd.setOrderStatus(Constant.Status.REJ.toString());
				customerOrderHd.setUpdateBy(updateBy);
				customerOrderHd.setUpdateDate(new Timestamp(new Date().getTime()));
				customerOrderHdDao.update(customerOrderHd);
				
				if (null != customerOrderTrans && customerOrderTrans.size() > 0) return customerOrderTrans.get(0).getTransactionNo();
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public MemberFacilityBookingDto getMemberFacilityTypeBookingPrice(MemberFacilityBookingDto bookingDto,boolean isMemberApp) throws Exception
	{
		checkMemberStatus(bookingDto.getCustomerId());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date bookingDate = format.parse(bookingDto.getBookingDate());
		if(isMemberApp){
			checkMemberBookingCondition(bookingDto, bookingDate);
		}
		
		//checking corporate permission
		checkFacilityRight(bookingDto);
		
		/*
		//check whether member's service plan has this facility
		boolean permitted = false;
		List<MemberPlanFacilityRight> memberPlanFacilityRightList = memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(bookingDto.getCustomerId());
		for(MemberPlanFacilityRight memberPlanFacilityRight : memberPlanFacilityRightList) {
			String facilityTypeCode = memberPlanFacilityRight.getFacilityTypeCode();
			String permission = memberPlanFacilityRight.getPermission();
			if (facilityTypeCode != null && permission != null && bookingDto.getFacilityType().equalsIgnoreCase(facilityTypeCode) && permission.contains("B")) {
				permitted = true;
				break;
			}
		}
		if (permitted == false) {
			throw new GTACommonException(GTAError.FacilityError.SERVICE_PLAN_NOT_INCLUDE_THIS_FACILITY);
		}*/
		//check booking date
		checkFacilityBookingDate(bookingDto);
		
		Map<String, Object> map = calculateFacilityBookingPrice(bookingDto, bookingDate,bookingDto.getCount());
		if (map != null && map.size() > 0)
		{
			BigDecimal itemTotalPrice = (BigDecimal) map.get("itemTotalPrice");
			bookingDto.setTotalPrice(itemTotalPrice.multiply(new BigDecimal(bookingDto.getCount())));
			List<CustomerFacilityBookingDetail> customerFacilityBookingDetailList = new ArrayList<CustomerFacilityBookingDetail>();
			List<BigDecimal> serviceItemCountList = (List<BigDecimal>)map.get("serviceItemCount");
			List<String> serviceItemDescriptionList = (List<String>)map.get("serviceItemDescription");
			List<BigDecimal> serviceItemPriceList = (List<BigDecimal>)map.get("serviceItemPriceList");
			CustomerFacilityBookingDetail customerFacilityBookingDetail = null;
			for(int i=0;i<serviceItemDescriptionList.size();i++) {
				customerFacilityBookingDetail = new CustomerFacilityBookingDetail();
				customerFacilityBookingDetail.setName(serviceItemDescriptionList.get(i));
				customerFacilityBookingDetail.setNumber(serviceItemCountList.get(i).multiply(new BigDecimal(bookingDto.getCount())));
				customerFacilityBookingDetail.setSubTotalPrice(serviceItemPriceList.get(i).multiply(customerFacilityBookingDetail.getNumber()));
				customerFacilityBookingDetail.setUnitPrice(serviceItemPriceList.get(i));
				customerFacilityBookingDetailList.add(customerFacilityBookingDetail);
			}
			bookingDto.setCustomerFacilityBookingDetailList(customerFacilityBookingDetailList);
		}
		if (null != bookingDto.getCustomerId() && bookingDto.getCustomerId() > 0)
		{
			Member member = memberDao.getMemberByCustomerId(bookingDto.getCustomerId());
			MemberCashvalue memberCashvalue = null;
			if(null != member){
				bookingDto.setAcademyNo(member.getAcademyNo());
				String memberType = member.getMemberType();
				if(!StringUtils.isEmpty(memberType)){
					if((Constant.memberType.IDM.toString().equalsIgnoreCase(memberType) || Constant.memberType.CDM.toString().equalsIgnoreCase(memberType)) && 
							(null != member.getSuperiorMemberId() && member.getSuperiorMemberId() >0) ){
						memberCashvalue = memberCashValueDao.getMemberCashvalueById(member.getSuperiorMemberId());
					}else{
						memberCashvalue = memberCashValueDao.getMemberCashvalueById(member.getCustomerId());
					}
				}
			}
			if(null != memberCashvalue)bookingDto.setAvailableBalance(memberCashvalue.getAvailableBalance());
			else bookingDto.setAvailableBalance(BigDecimal.ZERO);
			if (member.getMemberType().equalsIgnoreCase(Constant.memberType.IPM.toString())
					|| member.getMemberType().equalsIgnoreCase(Constant.memberType.CPM.toString()))
			{
				bookingDto.setSpendingLimit(null);
				bookingDto.setSpendingLimitUnit(null);
				MemberLimitRule memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(member.getCustomerId(), "CR");
				BigDecimal personalCreditLimit = BigDecimal.ZERO;
				if (memberLimitRule != null){
					personalCreditLimit = memberLimitRule.getNumValue();
				}
				bookingDto.setPersonalCreditLimit(personalCreditLimit);
				if (bookingDto.getPersonalCreditLimit().add(bookingDto.getAvailableBalance()).compareTo(bookingDto.getTotalPrice()) > 0) {
					bookingDto.setIsThisTransactionValid(true);
				}else{
					bookingDto.setIsThisTransactionValid(false);
					bookingDto.setDescription("The member's balance and credit limit is not enough to pay.");
				}
			}
			else if (member.getMemberType().equalsIgnoreCase(Constant.memberType.IDM.toString())
					|| member.getMemberType().equalsIgnoreCase(Constant.memberType.CDM.toString())) 
			{
				Member primaryMember = memberDao.getMemberByCustomerId(member.getSuperiorMemberId());
				bookingDto.setSpendingLimit(BigDecimal.ZERO);
				bookingDto.setSpendingLimitUnit("");
				bookingDto.setPersonalCreditLimit(BigDecimal.ZERO);
				if (primaryMember != null) {
					MemberLimitRule memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(primaryMember.getCustomerId(), "CR");
					BigDecimal personalCreditLimit = BigDecimal.ZERO;
					if (memberLimitRule != null){
						personalCreditLimit = memberLimitRule.getNumValue();
					}
					bookingDto.setPersonalCreditLimit(personalCreditLimit);
				}
				MemberLimitRule dependentMemberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(member.getCustomerId(), "TRN");
				BigDecimal spendingLimit = BigDecimal.ZERO;
				BigDecimal monthAmount = BigDecimal.ZERO;
				BigDecimal dayAmount = BigDecimal.ZERO;
				Boolean isSpendingLimitAllowed = true;
				if (dependentMemberLimitRule != null) {
					spendingLimit = dependentMemberLimitRule.getNumValue();
					bookingDto.setSpendingLimit(spendingLimit);
					bookingDto.setSpendingLimitUnit(dependentMemberLimitRule.getLimitUnit());
					Date today = new Date();
					Date oneMonthAgo = DateCalcUtil.getNearDay(today, -30);
					if (dependentMemberLimitRule.getLimitUnit() != null) {
						if (dependentMemberLimitRule.getLimitUnit().equalsIgnoreCase("MONTH")) {
							monthAmount = customerOrderHdDao.getSpendingTotalDuringRange(member.getCustomerId(), oneMonthAgo, today);
							if (monthAmount == null) {
								monthAmount = BigDecimal.ZERO;
							}
							if (bookingDto.getTotalPrice().add(monthAmount).compareTo(spendingLimit) > 0) {
								isSpendingLimitAllowed = false;
								bookingDto.setDescription("Month spending limit reached. No more transaction can be done.");
							}
						}
						else if (dependentMemberLimitRule.getLimitUnit().equalsIgnoreCase("DAY")) {
							Date startOfDay = DateCalcUtil.parseDate(DateCalcUtil.formatDate(today) + "00:00:00");
							Date endOfDay = DateCalcUtil.parseDate(DateCalcUtil.formatDate(today) + "23:59:59");
							dayAmount = customerOrderHdDao.getSpendingTotalDuringRange(member.getCustomerId(), startOfDay, endOfDay);
							if (dayAmount == null) {
								dayAmount = BigDecimal.ZERO;
							}
							if (bookingDto.getTotalPrice().add(dayAmount).compareTo(spendingLimit) > 0) {
								isSpendingLimitAllowed = false;
								bookingDto.setDescription("Day spending limit reached. No more transaction can be done.");
							}
						}
						else if (dependentMemberLimitRule.getLimitUnit().equalsIgnoreCase("EACH")) {
							if (null != spendingLimit && bookingDto.getTotalPrice().compareTo(spendingLimit) > 0) {
								isSpendingLimitAllowed = false;
								bookingDto.setDescription("Spending limit in each transaction reached. No more transaction can be done.");
							}
						}
					}
					else
					{
						if (bookingDto.getSpendingLimit() != null && bookingDto.getSpendingLimit().compareTo(bookingDto.getTotalPrice()) < 0) {
							isSpendingLimitAllowed = false;
							bookingDto.setDescription("Spending limit is not enough to pay.");
						}
					}
				}
				if (isSpendingLimitAllowed == true) {
					if (bookingDto.getPersonalCreditLimit().add(bookingDto.getAvailableBalance()).compareTo(bookingDto.getTotalPrice()) > 0) {
						bookingDto.setIsThisTransactionValid(true);
					}else{
						bookingDto.setIsThisTransactionValid(false);
						bookingDto.setDescription("Primary member's balance and credit limit is not enough to pay.");
					}
				}
				else
				{
					bookingDto.setIsThisTransactionValid(false);
					if (bookingDto.getDescription() == null) {
						bookingDto.setDescription("Spending limit is not enough to pay.");
					}
				}
			}
			
			CustomerProfile customerProfile = customerProfileDao.getCustomerProfileByCustomerId(bookingDto.getCustomerId().toString());
			if(null !=customerProfile)bookingDto.setContactEmail(customerProfile.getContactEmail());
		}
		return bookingDto;
	}
	
	private void checkFacilityRight(MemberFacilityBookingDto bookingDto)
	{
		UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(bookingDto.getCustomerId(), UsageRightCheckService.FacilityCode.GOLF.name().equals(bookingDto.getFacilityType())?UsageRightCheckService.FacilityCode.GOLF : UsageRightCheckService.FacilityCode.TENNIS);
		if(!facilityRight.isCanBook()){
			throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
		}
	}
	
	private void checkCancelCondition(Long resvId) {
		MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);
		Timestamp endDatetimeBook = memberFacilityTypeBooking.getEndDatetimeBook();
		Date now = new Date();
		
		// end date must after current time
		if (endDatetimeBook.before(now)) {
			throw new GTACommonException(GTAError.FacilityError.BEGIN_DATE_AFTER_NOW);
		}
		// validate status
		if (MemberFacilityTypeBookingStatus.CAN.name().equals(memberFacilityTypeBooking.getStatus())) {
			throw new GTACommonException(GTAError.FacilityError.ALREADY_CANCEL);
		}
	}
	
	private void deleteData4MemberFacilityTypeBooking(long resvId, String userId, String remark) {
		MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);
		if (memberFacilityTypeBooking != null) {
			List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
			if (facilityTimeslots != null) {
				
				for (FacilityTimeslot facilityTimeslot: facilityTimeslots) {
					long facilityTimeslotId = facilityTimeslot.getFacilityTimeslotId();
					
					// delete the attendance record
					List<MemberFacilityAttendance>  memberFacilityAttendances = memberFacilityAttendanceDao.getMemberFacilityAttendanceList(facilityTimeslotId, 0);
					if (memberFacilityAttendances != null) {
						for (MemberFacilityAttendance memberFacilityAttendance: memberFacilityAttendances) {
							memberFacilityAttendanceDao.delete(memberFacilityAttendance);
						}
					}
									
				}
				for (FacilityTimeslot facilityTimeslot: facilityTimeslots) {					
					facilityTimeslot.setFacilityTimeslotAddition(null);
				
					//delete the coach time_slot if there is private coach reserved
					if (facilityTimeslot.getStaffFacilitySchedule() != null) {
						facilityTimeslot.getStaffFacilitySchedule().setFacilityTimeslot(null);
						facilityTimeslot.getStaffFacilitySchedule().setStaffTimeslot(null);
						staffFacilityScheduleDao.delete(facilityTimeslot.getStaffFacilitySchedule());
					}
					facilityTimeslot.setStaffFacilitySchedule(null);
					facilityTimeslot.setFacilityMaster(null);
					facilityTimeslot.setStatus("CAN");
					
					facilityTimeslotDao.saveOrUpdate(facilityTimeslot);
				}
				facilityTimeslots.clear();
			}
			List<StaffTimeslot> staffTimeslots = memberFacilityTypeBooking.getStaffTimeslots();
			if(null!=staffTimeslots && staffTimeslots.size()>0){
				for(StaffTimeslot lot : staffTimeslots){
					lot.setStatus("CAN");
					this.staffTimeslotDao.saveOrUpdate(lot);
				}
				staffTimeslots.clear();
			}
			
			if (null != remark) {
				memberFacilityTypeBooking.setCustomerRemark(remark);
			}
			memberFacilityTypeBooking.setUpdateBy(userId);
			memberFacilityTypeBooking.setUpdateDate(new Date());
			memberFacilityTypeBooking.setStatus(MemberFacilityTypeBookingStatus.CAN.name());
			memberFacilityTypeBookingDao.update(memberFacilityTypeBooking);
		}
	}
		
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean cancelMemberFacilityTypeBooking(long resvId, String userId, String remark, String requesterType,
			String createRefundRequest) {

		checkCancelCondition(resvId);
		this.deleteData4MemberFacilityTypeBooking(resvId, userId, remark);
		MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);

		if (createRefundRequest != null && checkRefundTransaction(booking)
				&& createRefundRequest.equalsIgnoreCase("TRUE")) {
				sendRefundRequestFacility(booking, requesterType, booking.getCustomerRemark(), userId);
		}
		return true;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public boolean cancelMemberFacilityTypeBookingCheck(long resvId) {
		MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);
		return canRefund(memberFacilityTypeBooking);
	}

	private boolean canRefund(MemberFacilityTypeBooking memberFacilityTypeBooking) {
		return checkRefundDate(memberFacilityTypeBooking.getBeginDatetimeBook(),memberFacilityTypeBooking.getResvFacilityType(),"facility") && checkRefundTransaction(memberFacilityTypeBooking);
	}

	public boolean checkRefundDate(Timestamp beginDatetimeBook,String facilityType,String type) {
		int refundPeriod = 0;
		refundPeriod = getRefundPeriod(facilityType, type);
		
		Date now = new Date();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.add(Calendar.HOUR, refundPeriod);

		Date refundPeriodDate = calendar.getTime();
		
		if (beginDatetimeBook.after(refundPeriodDate)) {
			return true;
		}
		
		return false;
	}
	
	private boolean checkRefundTransaction(MemberFacilityTypeBooking memberFacilityTypeBooking) {
		
		CustomerOrderHd customerOrderHd = memberFacilityTypeBooking.getCustomerOrderHd();

		if (customerOrderHd != null && Constant.Status.CMP.toString().equals(customerOrderHd.getOrderStatus())) {
			return true;
		}
		return false;
		
	}
	
	@Override
	public String getAllMemberFacilityTypeBooking(String facilityType, String dateRange, String customerId,boolean isPushMessage) throws Exception
	{
		return memberFacilityTypeBookingDao.getAllMemberFacilityTypeBooking(facilityType, dateRange, customerId,isPushMessage);
	}
	
	/**
	 *  refund rule of a service
	 *  
	 * @param memberFacilityTypeBooking
	 * @param loginUser
	 */
	private void refund(MemberFacilityTypeBooking memberFacilityTypeBooking, String userId) {

		if (canRefund(memberFacilityTypeBooking)) {

			// 1.1 do refund to cash value
			long customerId = memberFacilityTypeBooking.getCustomerId();
			MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
			CustomerOrderHd customerOrderHd = memberFacilityTypeBooking.getCustomerOrderHd();

			if (customerOrderHd != null) {
				BigDecimal itemTotalAmout = customerOrderHd.getOrderTotalAmount();
				BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(itemTotalAmout);
				memberCashvalue.setAvailableBalance(updatedAmount);
				memberCashValueDao.updateMemberCashValue(memberCashvalue);

				saveRefundTrans(userId, customerOrderHd);

			}
		}
	}

	

	/**
	 * Since dependent member does not have his own cash value account, the refund from dependent member would still debit into primary member account
	 * 
	 * @param customerId
	 * @return
	 */
	private MemberCashvalue getMemberCashvalue(long customerId) {
		Member member = memberDao.get(Member.class, customerId);
		
		if (member == null) {
			throw new GTACommonException(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
		}
		
		MemberCashvalue memberCashvalue = null;
		String type = member.getMemberType();

		if (Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)) {
			Long superiorMemberId = member.getSuperiorMemberId();

			if (superiorMemberId != null) {
				memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
			}
		} else {
			memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
		}

		if (memberCashvalue == null) {
			logger.error("member cash value not found for customer" + customerId);
			throw new GTACommonException(GTAError.FacilityError.REFUND_NOT_AVAILABLE);
		}
		return memberCashvalue;

	}
	
	private Long getFromTransactionNo(List<CustomerOrderTrans> customerOrderTranses) {
		Long fromTransactionNo = null;
		if (customerOrderTranses != null && !customerOrderTranses.isEmpty()) {

			for (CustomerOrderTrans customerOrderTran : customerOrderTranses) {
				
				if ("SUC".equals(customerOrderTran.getStatus())) {
					fromTransactionNo = customerOrderTran.getTransactionNo();
				}
			}
		}
		return fromTransactionNo;
	}


	private int getRefundPeriod(String facilityType,String type) {
		String keyName = "REFPERIOD_GOLFINGBAY";
		if("facility".equals(type)){
			if (facilityType.equals("GOLF")) {
				keyName = "REFPERIOD_GOLFINGBAY";
				keyName = RefundPeriodType.REFPERIOD_GOLFINGBAY.name();
			}
			else if (facilityType.equals("TENNIS")) {
				keyName = "REFPERIOD_TENNISCOURT";
				keyName = RefundPeriodType.REFPERIOD_TENNISCOURT.name();
			}
		} else if("coach".equals(type)){
			if (facilityType.equals("GOLF")) {
				keyName = "REFPERIOD_GOLFCOACHING";
				keyName = RefundPeriodType.REFPERIOD_GOLFCOACHING.name();
			}
			else if (facilityType.equals("TENNIS")) {
				keyName = "REFPERIOD_TENNISCOACHING";
				keyName = RefundPeriodType.REFPERIOD_TENNISCOACHING.name();
			}
		}else if("course".equals(type)){

			if (CourseType.GOLFCOURSE.toString().equals(facilityType)) {
				keyName = RefundPeriodType.REFPERIOD_GOLFCOURSE.name();
			} else if (CourseType.TENNISCOURSE.toString().equals(facilityType)) {
				keyName = RefundPeriodType.REFPERIOD_TENNISCOURSE.name();
			}
		}else if("daypass".equals(type)){
			keyName = RefundPeriodType.REFPERIOD_DAYPASS.name();
		}
		
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class,keyName);

		// Default Refund Period
		int refundPeriod = Constant.REFUND_PERIOD_DEFAULT_HOURS;
		String paramValue = null;
		if (globalParameter != null && Constant.General_Status_ACT.equals(globalParameter.getStatus())) {
			paramValue = globalParameter.getParamValue();
		}

		if (paramValue != null) {
			refundPeriod = (int)(Float.parseFloat(paramValue) * 24);
		}
		return refundPeriod;
	}

	private void checkFacilityBookingDate(MemberFacilityBookingDto bookingDto) throws Exception
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int todayHour = calendar.get(Calendar.HOUR_OF_DAY);
		if (format.parse(bookingDto.getBookingDate()).before(format.parse(format.format(new Date()))))
			throw new GTACommonException(GTAError.FacilityError.DATE_LESSTHAN_TODAY );
		if (format.parse(bookingDto.getBookingDate()).compareTo(format.parse(format.format(new Date()))) == 0 && bookingDto.getBeginTime() < todayHour)
			throw new GTACommonException(GTAError.FacilityError.DATETIME_LESSTHAN_TODAY);
		int parameterValue = 0;
		String facTypeConst = Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(bookingDto.getFacilityType()) ? Constant.ADVANCEDRESPERIOD_GOLF : Constant.ADVANCEDRESPERIOD_TENNIS;
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, facTypeConst);
		if (null != globalParameter)
		{
			if (!StringUtils.isEmpty(globalParameter.getParamValue()))
				parameterValue = Integer.parseInt(globalParameter.getParamValue());
		}
		if (parameterValue > 0)
		{
			Date futureDate = getFutureDate(new Date(), parameterValue);
			if (format.parse(bookingDto.getBookingDate()).after(futureDate))
				throw new GTACommonException(GTAError.FacilityError.DATE_OVER_ADVANCEDPERIOD,new Object[]{parameterValue});
		}
	}
	
	public static Date getFutureDate(Date date, int day) throws ParseException
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return format.parse(format.format(calendar.getTime()));
	}

	@Transactional
	@Override
	public String  getPrivateCoachBookList(String status, Integer show,String facilityType, String customerId) {
		String joinHQL = this.privateCoachBookListDtoDao.getPrivateCoachBookList(status, show,facilityType, customerId);
		return joinHQL;
	}

	private void addWhereColumn(StringBuilder sql, StringBuilder countHql,
			PrivateCoachBookListDto dto,List<Serializable> param ) {
		if(null!=dto.getShow()){
			if(0==dto.getShow()){
				sql.append(" and t2.showDate2 = CURRENT_DATE   ");
				countHql.append(" and t2.showDate2 = CURRENT_DATE   ");
			}
			if(30==dto.getShow()){
				sql.append(" and t2.showDate2  >= CURRENT_DATE  and  t2.showDate2  <= CURRENT_DATE +30  ");
				countHql.append(" and t2.showDate2  >= CURRENT_DATE  and  t2.showDate2 <= CURRENT_DATE +30  ");
			}
			if(90==dto.getShow()){
				sql.append(" and t2.showDate2  >= CURRENT_DATE  and t2.showDate2  <= CURRENT_DATE +90  ");
				countHql.append(" and t2.showDate2  >= CURRENT_DATE  and  t2.showDate2  <= CURRENT_DATE +90  ");
			}
		}
		if(!StringUtils.isEmpty(dto.getResvId())){
			sql.append(" and t2.resvId=? ");
			countHql.append(" and t2.resvId=? ");
			param.add(dto.getResvId());
		}
		if(!StringUtils.isEmpty(dto.getMemberID())){
			sql.append(" and t2.memberID=? ");
			countHql.append(" and t2.memberID=? ");
			param.add(dto.getMemberID());
		}
		if(!StringUtils.isEmpty(dto.getMemberName())){
			sql.append(" and t2.memberName like \"%").append(dto.getMemberName()).append("%\"  ");
			countHql.append(" and t2.memberName like \"%").append(dto.getMemberName()).append("%\"  ");
			param.add(dto.getMemberName());
		}
		if(!StringUtils.isEmpty(dto.getBeginDate())){
			sql.append(" and t2.showDate2  >= '").append(dto.getBeginDate()).append("'   ");
			countHql.append(" and t2.showDate2  >= '").append(dto.getBeginDate()).append("'  ");
		}
		if(!StringUtils.isEmpty(dto.getEndDate())){
			sql.append(" and t2.showDate2  <= '").append(dto.getEndDate()).append("'  ");
			countHql.append(" and t2.showDate2  <= '").append(dto.getEndDate()).append("'  ");
		}
		if(!StringUtils.isEmpty(dto.getBayType())){
			sql.append(" and t2.BayType=?  ");
			countHql.append(" and t2.BayType=?  ");
			param.add(dto.getBayType());
		}
		if(!StringUtils.isEmpty(dto.getCoach())){
			sql.append(" and t2.coach like \"%").append(dto.getCoach()).append("%\" ");
			countHql.append(" and t2.coach like \"%").append(dto.getCoach()).append("%\" ");
			param.add(dto.getCoach());
		}
	}
	private String getName(CustomerProfile cp) {
		if (cp == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName()).append(" ").append(cp.getSurname());
		return sb.toString();
	}
	@Transactional
	@Override
	public boolean getRightByCustomerIdAndFacilityType(Long customerId, String facilityType){
		boolean flag = false;
//		MemberPlanFacilityRight facilityRight = this.memberPlanFacilityRightDao.getRightByCustomerIdAndFacilityType(customerId,facilityType);
//		if(null!=facilityRight && "BA".equals(facilityRight.getPermission())){
//			flag = true;
//		}
		/*
		 * TRAIN2--Golf Private Coaching
		 * TRAIN4--Tennis Private Coaching
		 */
		String limitType = "";
		if("TENNIS".equals(facilityType) ){
			limitType = ServicePlanRight.TENNIS_COACHING.getValue();
		}else if("GOLF".equals(facilityType) ){
			limitType = ServicePlanRight.GOLF_COACHING.getValue();
		}
		MemberLimitRule rule = this.memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, limitType);
		if(null!=rule && "true".equals(rule.getTextValue())){
			flag = true;
		}
		return flag;
	}
	
	Long[] getTransferTimeslotId(MemberFacilityBookingDto bookingDto, int size){
		if(bookingDto.getOriginResvId() == null){
			return new Long[0];
		}
		
		MemberFacilityTypeBooking booking = getMemberFacilityTypeBooking(bookingDto.getOriginResvId());
		List<FacilityTimeslot> facilityTimeslots = booking.getFacilityTimeslots();
		
		Long[] result = new Long[size];
		if(size <= facilityTimeslots.size()){
			for(int i = 0 ; i < size ; i++){
				result[i] = facilityTimeslots.get(i).getFacilityTimeslotId();
			}
		}else{
			for(int i = 0 ; i < size ; i++){
				result[i] = facilityTimeslots.get(facilityTimeslots.size() > i ? i : facilityTimeslots.size()-1).getFacilityTimeslotId();
			}
		}
		return result;
	}
	
	@Transactional
	@Override
	public Map<String,Object> saveReservationOrder(MemberFacilityBookingDto bookingDto) throws Exception{
//		boolean facilityRight = this.getRightByCustomerIdAndFacilityType(bookingDto.getCustomerId(),bookingDto.getFacilityType());
//		if(false==facilityRight){
//			throw new GTACommonException(GTAError.FacilityError.No_FacilityRight);
//		}
		// calculate totalPrice
		ResponseResult result = this.coachManagementService.calcBookingPrice(bookingDto);
		MemberFacilityBookingDto dto = (MemberFacilityBookingDto) result.getData();
		// save FacilityTimeslot and StaffTimeslot
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date[] dateTimes = new Date[(bookingDto.getEndTime() - bookingDto.getBeginTime()) + 1];
		int time = 0;
		for (int i = bookingDto.getBeginTime(); i <= bookingDto.getEndTime(); i++)
		{
			String startdateStr = bookingDto.getBookingDate() + " " + i + ":00:00";
			dateTimes[time] = sf.parse(startdateStr);
			time++;
		}
		/**
		 * get available FacilityMaster rule is:first  2 floor then 0 floor, last 1 floor, the facilityNo should be from small to big
		 
		//first check 2 floor 
		int venueFloor = 2;
		List<FacilityMaster> facilityMasters = facilityMasterDao.getFacilityMasterAvailableList(bookingDto.getFacilityType(), bookingDto.getAttributeType(), dateTimes,venueFloor);
		if (null == facilityMasters || facilityMasters.size() < 1){
		//then check 0 floor 	
			venueFloor = 0;
			facilityMasters = facilityMasterDao.getFacilityMasterAvailableList(bookingDto.getFacilityType(), bookingDto.getAttributeType(), dateTimes,venueFloor);
		}
		if (null == facilityMasters || facilityMasters.size() < 1){
		//last check 1 floor 	
			venueFloor = 1;
			facilityMasters = facilityMasterDao.getFacilityMasterAvailableList(bookingDto.getFacilityType(), bookingDto.getAttributeType(), dateTimes,venueFloor);
		}
		//if 2,0,1 floor ,all  can not find a available facility,throw  exception
		if (null == facilityMasters || facilityMasters.size() < 1){
			throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT);
		}
		*/
		List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_COACHING);
		List<FacilityMaster> facilityMasters = facilityMasterDao.getFacilityMasterAvailableList(bookingDto.getFacilityType(), bookingDto.getAttributeType(), dateTimes, null, facilityNos);
		if (null == facilityMasters || facilityMasters.size() < 1){
			throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT);
		}
		FacilityMaster facilityMaster = facilityMasters.get(0);
		List<Long> timeslotIds = new ArrayList<Long>();
		List<String> staffTimeslotIds = new ArrayList<String>();
		Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
		MemberFacilityTypeBooking booking = saveMemberFacilityBooking(bookingDto, sf, null);
		bookingDto.setResvId(booking.getResvId());
		Long[] transfers = getTransferTimeslotId(bookingDto, bookingDto.getEndTime() - bookingDto.getBeginTime() + 1);
		for (int i = bookingDto.getBeginTime(); i <= bookingDto.getEndTime(); i++)
		{
			String startdateStr = bookingDto.getBookingDate() + " " + i + ":00:00";
			String enddateStr = bookingDto.getBookingDate() + " " + i + ":59:59";
			StaffTimeslot staffTimeslot = createStaffTimeslot(bookingDto, sf.parse(startdateStr), sf.parse(enddateStr), currentTimestamp);
			staffTimeslotIds.add(staffTimeslot.getStaffTimeslotId());
			FacilityTimeslot timesLot = null;
			if(bookingDto.isCreateOffer()){
				timesLot = createTimeslot(bookingDto.getUpdateBy(), sf.parse(startdateStr), sf.parse(enddateStr), facilityMaster, currentTimestamp,Constant.RESERVE_TYPE_PC, transfers[i - bookingDto.getBeginTime()]);
			}else{
				timesLot = createTimeslot(bookingDto.getUpdateBy(), sf.parse(startdateStr), sf.parse(enddateStr), facilityMaster, currentTimestamp,Constant.RESERVE_TYPE_PC);
			}
			timeslotIds.add(timesLot.getFacilityTimeslotId());
		}
	
		if (null != timeslotIds && timeslotIds.size() > 0 && null!=staffTimeslotIds && staffTimeslotIds.size()>0 )
		{
			long resvId = booking.getResvId();
			for(int i=0;i<staffTimeslotIds.size();i++){
				long timeslotId = timeslotIds.get(i);
				String staffTimeslotId = staffTimeslotIds.get(i);
				MemberReservedFacility memberReservedFacility = new MemberReservedFacility();
				memberReservedFacility.setFacilityTimeslotId(timeslotId);
				memberReservedFacility.setResvId(resvId);
				memberReservedFacilityDao.save(memberReservedFacility);
				
				MemberReservedStaff memberReservedStaff = new MemberReservedStaff();
				memberReservedStaff.setResvId(resvId);
				memberReservedStaff.setStaffTimeslotId(staffTimeslotId);
				this.memberReservedStaffDao.save(memberReservedStaff);
			}
		}
		// save MemberFacilityBookAdditionAttr
		if (null != booking && Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(bookingDto.getFacilityType())){
			saveMemberFacilityBookAdditionAttr(bookingDto, booking.getResvId());
		}else if(null != booking && Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(bookingDto.getFacilityType())){
			booking.setFacilitySubtypeId(bookingDto.getAttributeType());
			
		}
		checkFacilityCount(bookingDto, sf, true);
		dto.setCustomerId(bookingDto.getCustomerId());
		dto.setPaymentMethod(bookingDto.getPaymentMethod());
		dto.setLocation(bookingDto.getLocation());
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("bookingDto", dto);
		map.put("booking", booking);
		map.put("userId", dto.getUpdateBy());
		return map;
	}
	@Transactional
	@Override
	public MemberFacilityBookingDto doPay(Map<String,Object> map) throws Exception
	{	
		//first call saveReservationOrder() than pay for this order
		MemberFacilityBookingDto bookingDto = (MemberFacilityBookingDto) map.get("bookingDto");
		MemberFacilityTypeBooking booking = (MemberFacilityTypeBooking) map.get("booking");
		
		if(bookingDto.isCreateOffer()){
			booking.setStatus(Constant.Status.RSV.toString());
			return bookingDto;
		}
		
		if(null!=bookingDto && null!=booking){
			// pay the order begin
			MemberCashValuePaymentDto paymentDto = new MemberCashValuePaymentDto();
			paymentDto.setCustomerId(bookingDto.getCustomerId());
			paymentDto.setPaymentMethod(bookingDto.getPaymentMethod());
			paymentDto.setTotalAmount(bookingDto.getTotalPrice());
			paymentDto.setReserveVia(bookingDto.getReserveVia());
			paymentDto.setLocation(bookingDto.getLocation());
			// set price itemNo
			List<String> itemNos = new ArrayList<String>();
			if (bookingDto.getHighPriceItemNo() != null)
			{
				itemNos.add(bookingDto.getHighPriceItemNo());
			}
			if (bookingDto.getLowPriceItemNo() != null)
			{
				itemNos.add(bookingDto.getLowPriceItemNo());
			}
			String[] priceItemNos = new String[itemNos.size()];
			paymentDto.setItemNoMap(bookingDto.getItemNoMap());
			paymentDto.setUserId(bookingDto.getUpdateBy());
			
			if((Constant.reserve_via.APS.toString().equals(bookingDto.getReserveVia()) || Constant.reserve_via.WP.toString().equals(bookingDto.getReserveVia()) ) && Constant.CREDIT_CARD.equals(bookingDto.getPaymentMethod())){
				paymentDto.setIsOnlinePayment(true);
			}else{
				paymentDto.setIsOnlinePayment(false);
			}
			
			CustomerOrderTransDto orderTransDto = null;
			if(bookingDto.getPaymentMethod().equals(Constant.CASH_Value))
				orderTransDto = memberCashValueService.paymentByMemberCashvalue(paymentDto);
			else 
				orderTransDto = customerOrderTransService.payment(paymentDto);
			// pay the order end
			if (null != orderTransDto){
				booking.setOrderNo(orderTransDto.getOrderNo());
				CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, orderTransDto.getOrderNo());
				booking.setCustomerOrderHd(customerOrderHd);
				//first save memberFacilityTypeBooking order then pay,after user CASH or CASH_Value method successfully then set memberFacilityTypeBooking as RSV
				if(!bookingDto.getPaymentMethod().equals(Constant.CREDIT_CARD)){
					booking.setStatus(Constant.Status.RSV.toString());
				}
				this.memberFacilityTypeBookingDao.update(booking);
				setValue4bookingDto(bookingDto, orderTransDto, booking);
			}
			if((Constant.reserve_via.APS.toString().equals(bookingDto.getReserveVia()) || Constant.reserve_via.WP.toString().equals(bookingDto.getReserveVia()) ) && Constant.CREDIT_CARD.equals(bookingDto.getPaymentMethod())){
				CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, bookingDto.getTransactionId());
				String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
				bookingDto.setRedirectUrl(redirectUrl);
			} 
		}
		return bookingDto;
	}

	private MemberFacilityTypeBooking saveMemberFacilityBooking(
			MemberFacilityBookingDto bookingDto, SimpleDateFormat sf,
			Long orderNo) throws ParseException {

		String startdateStr = bookingDto.getBookingDate() + " " + bookingDto.getBeginTime() + ":00:00";
		String enddateStr = bookingDto.getBookingDate() + " " + bookingDto.getEndTime() + ":59:59";
		MemberFacilityTypeBooking booking = new MemberFacilityTypeBooking();
		booking.setCustomerId(bookingDto.getCustomerId());
		booking.setResvFacilityType(bookingDto.getFacilityType());
		booking.setFacilityTypeQty(1L);
		if(bookingDto.getEndTime()-bookingDto.getBeginTime()>=1){
			booking.setSameFacility("Y");
		}else {
			booking.setSameFacility("N");
		}
		booking.setBeginDatetimeBook(new Timestamp(sf.parse(startdateStr).getTime()));
		booking.setEndDatetimeBook(new Timestamp(sf.parse(enddateStr).getTime()));

		if (Constant.FACILITY_TYPE_TENNIS.equals(bookingDto.getFacilityType().toUpperCase()))
		{
			booking.setFacilitySubtypeId(bookingDto.getAttributeType());
		}
		
		booking.setExpCoachUserId(bookingDto.getCoachId());
		
		booking.setReserveVia(bookingDto.getReserveVia());
		booking.setStatus(Constant.Status.PND.toString());
		//booking.setOrderNo(orderNo);
		booking.setCreateDate(new Timestamp(new Date().getTime()));
		booking.setCreateBy(bookingDto.getUpdateBy());
		memberFacilityTypeBookingDao.save(booking);
		return booking;
	}

	private void setValue4bookingDto(MemberFacilityBookingDto bookingDto,
			CustomerOrderTransDto orderTransDto,
			MemberFacilityTypeBooking booking) {
		Long resvId = booking.getResvId();
		bookingDto.setResvId(resvId);
		bookingDto.setTransactionId(orderTransDto.getTransactionNo());
		bookingDto.setOrderNo(orderTransDto.getOrderNo());
		StaffDto staffDto = this.userMasterDao.getStaffDto(bookingDto.getCoachId());
		String staffName = "";
		if(!StringUtils.isEmpty(staffDto.getStaffName())){
			staffName = staffDto.getStaffName().trim();
		} else if(!StringUtils.isEmpty(staffDto.getNickname())){
			staffName = staffDto.getNickname().trim();
		}else {
			staffName = staffDto.getUserId();
		}
		bookingDto.setCoachName(staffName);
	}
	
	/**   
	* @author: Zero_Wang
	 * @throws Exception 
	* @since: Aug 4, 2015
	* 
	* @description
	* check whether the cancel beyond the refund period or not
	*/  
	@Transactional
	@Override
	public ResponseResult preCancel(Long resvId,String type) throws Exception {
		boolean  flag = true;
		CancelDto cancelDto = new CancelDto();
		cancelDto.setResvId(resvId);
		if("course".equals(type)){
			CourseMaster course = courseMasterDao.get(CourseMaster.class,resvId);
			Date courseStartDate = courseMasterDao.getCourseSessionStartDate(course.getCourseId());
			if(courseStartDate!=null){
				Timestamp startTime = new Timestamp(courseStartDate.getTime());
				flag = this.checkRefundDate(startTime,course.getCourseType(), type);
			}
		}else if ("courseEnroll".equals(type)) {
			CourseEnrollment ce = courseEnrollmentDao.get(CourseEnrollment.class, resvId.toString());
			CourseMaster course = courseMasterDao.get(CourseMaster.class,ce.getCourseId());
			Date courseStartDate = courseMasterDao.getCourseSessionStartDate(course.getCourseId());
			if(courseStartDate!=null){
				Timestamp startTime = new Timestamp(courseStartDate.getTime());
				flag = this.checkRefundDate(startTime,course.getCourseType(), type);
			}
		}else if("daypass".equals(type)){
			CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, resvId);
			if(!StringUtils.isEmpty(customerOrderHd.getStaffUserId())){
				cancelDto.setPurchaser("HKGTA");
			}
			List<CustomerOrderPermitCard> list = customerOrderHd.getCustomerOrderPermitCards();
			if(list !=null && list.size()>0 ){
				Timestamp begin = new Timestamp(list.get(0).getEffectiveFrom().getTime());
				flag = this.checkRefundDate(begin,RefundPeriodType.REFPERIOD_DAYPASS.name(), type);
			}
		}else if("guestroom".equals(type)){
			RoomReservationRec rec = roomReservationRecDao.get(RoomReservationRec.class, resvId);
			Timestamp startTime = new Timestamp(rec.getArrivalDate().getTime());
			flag = this.checkRefundDate(startTime,RefundPeriodType.REFPERIOD_GUESTROOM.name(), type);
		}
		else{
			MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);
			flag = this.checkRefundDate(memberFacilityTypeBooking.getBeginDatetimeBook(),memberFacilityTypeBooking.getResvFacilityType(), type);
		}
		cancelDto.setFlag(flag);
		this.responseResult.initResult(GTAError.Success.SUCCESS, cancelDto);
		return this.responseResult;
	}
	
	@Transactional
	@Override
	public ResponseResult doCancel(Long resvId, String userId,String remark,String requesterType,boolean refundFlag) {
		MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);
		//1、based on defined company rule 检测是否可以执行Cancel操作
		String status = booking.getStatus();
		if (MemberFacilityTypeBookingStatus.CAN.name().equals(status)) {
			throw new GTACommonException(GTAError.FacilityError.ALREADY_CANCEL);
		}
		if(booking.getEndDatetimeBook().before(new Date())){
			throw new GTACommonException(GTAError.FacilityError.Endtime_Overdue);
		}
		  //2、delete member_reserved_facility  	and facility_timeslot where resv_id=''
		  //3、delete member_facility_book_addition_attr where resv_id=''
		  //4、delete staff_timeslot where staff_timeslot_id=''
		 // 5、update member_facility_type_booking set status='CAN'  ,staff_timeslot_id=null,
		
			this.deleteData4MemberFacilityTypeBooking(resvId, userId, remark);
			CustomerOrderHd customerOrderHd = booking.getCustomerOrderHd();
			if(MemberFacilityTypeBookingStatus.RSV.name().equals(status) && customerOrderHd!=null){
				if(Constant.Status.CMP.toString().equals(customerOrderHd.getOrderStatus())){
					String refundServiceType = "";
					if(FacilityName.GOLF.name().equals(booking.getResvFacilityType())){
						refundServiceType = RefundServiceType.GCH.getName();
					}else if(FacilityName.TENNIS.name().equals(booking.getResvFacilityType())){
						refundServiceType = RefundServiceType.TCH.getName();
					}
					if("HKGTA".equals(requesterType) || refundFlag ){//send CustomerRefundRequest
						boolean flag = false;
						boolean  refundPeriodFlag = this.checkRefundDate(booking.getBeginDatetimeBook(),booking.getResvFacilityType(), "coach");
						if(!refundPeriodFlag && "CUSTOMER".equals(requesterType)){
							flag = sendCustomerRefundRequest(customerOrderHd,refundServiceType,remark,requesterType,userId,Constant.CustomerRefundRequest.NRF.getName());
						}else {
							flag = sendCustomerRefundRequest(customerOrderHd,refundServiceType,remark,requesterType,userId,Constant.CustomerRefundRequest.PND.getName());
						}
						if(flag){
							this.responseResult.initResult(GTAError.Success.SUCCESS);
						}else {
							this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"private coach reservations cancel fail： "+resvId});
						}
					}
				}
			}else {
				this.responseResult.initResult(GTAError.Success.SUCCESS);
			}
		
		return this.responseResult;
	}

	/**
	 * 
	* @author: Zero_Wang
	* @since: Jul 31, 2015
	* 
	* @description
	* first save refund transaction then send CustomerRefundRequest
	 */
	private CustomerOrderTrans saveRefundTrans(String userId, CustomerOrderHd customerOrderHd) {
		// get and store the paid transaction no
		List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
		Long fromTransactionNo = getFromTransactionNo(customerOrderTranses);
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setPaymentMethodCode(Constant.CASH_Value);
		customerOrderTrans.setTransactionTimestamp(new Timestamp(System.currentTimeMillis()));
		customerOrderTrans.setPaidAmount(customerOrderHd.getOrderTotalAmount());
		customerOrderTrans.setStatus(Constant.Status.PR.toString());
		customerOrderTrans.setFromTransactionNo(fromTransactionNo);
		customerOrderTrans.setPaymentRecvBy(userId);
		customerOrderTrans.setInternalRemark(Constant.INTERNAL_REMARK_REFUND);
		customerOrderTransDao.save(customerOrderTrans);
		return customerOrderTrans;
	}
	@Transactional
	@Override
	public boolean sendCustomerRefundRequest(final CustomerOrderHd customerOrderHd, final String refundServiceType,String customerReason,
			String requesterType, String userId,String status) {
//		DistributedLock lock = null;
//		try{
//			lock = new DistributedLock(zookeeperConnectionString, "MemberFacilityTypeBookingServiceImpl");
//			lock.lock();
Long begin = System.currentTimeMillis();
			if (customerOrderHd != null) {
				List<CustomerOrderTrans>  trans = customerOrderHd.getCustomerOrderTrans();
				if(null==trans || trans.size()<=0){
					throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderTrans  not right.oderNo:"+customerOrderHd.getOrderNo()});
				}
				final Date d = new Date();
				for(CustomerOrderTrans c : trans){
					if(Constant.Status.SUC.toString().equalsIgnoreCase(c.getStatus())){
						CustomerRefundRequest refundRequest = new CustomerRefundRequest();
						refundRequest.setRefundTransactionNo(c.getTransactionNo());
						refundRequest.setRequesterType(requesterType);
						refundRequest.setRefundServiceType(refundServiceType);
						refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
						refundRequest.setCustomerReason(customerReason);
						refundRequest.setStatus(status);
						refundRequest.setRequestAmount(customerOrderHd.getOrderTotalAmount());
						refundRequest.setCreateBy(userId);
						refundRequest.setCreateDate(d);
						this.customerRefundRequestDao.save(refundRequest);
					}
				}
Long end = System.currentTimeMillis();
System.out.println("sendCustomerRefundRequest insert request intoDB finish.spend:"+(end - begin));
this.logger.error("sendCustomerRefundRequest insert request intoDB finish.spend:"+(end - begin));
				sendRefundRequestEmail(customerOrderHd,refundServiceType,d);
				return true;
			}
//		}finally {
//			lock.unlock();
//		}	
		return false;
	}
	@Transactional
	@Override
	public void sendRefundRequestEmail(CustomerOrderHd customerOrderHd,
			String refundServiceType,Date refundDate) {
Long begin = System.currentTimeMillis();
		MessageTemplate template = null;
		String templateContent = "";
		String content = "";
		if("CVL".equals(refundServiceType)){
			template = templateDao.getTemplateByFunctionId("cashValue_refund_email");
			if (template == null) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Refund Confirmation e-mail: cvcl"});
			}
			templateContent = template.getContentHtml();
		}else {
			template = templateDao.getTemplateByFunctionId("service_refund_email");
			if (template == null) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Refund Confirmation e-mail: srcl"});
			}
			templateContent = template.getContentHtml();
		}
Long end = System.currentTimeMillis();
System.out.println("sendRefundRequestEmail get template finish.spend:"+(end - begin));
this.logger.error("sendRefundRequestEmail get template finish.spend:"+(end - begin));
begin = System.currentTimeMillis();
		CustomerProfile cp = customerProfileDao.getById(customerOrderHd.getCustomerId());
		if (cp == null) {
			throw new GTACommonException(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_CUSTOMER_PROFILE);
		}
end = System.currentTimeMillis();
System.out.println("sendRefundRequestEmail get CustomerProfile finish.spend:"+(end - begin));
this.logger.error("sendRefundRequestEmail get CustomerProfile finish.spend:"+(end - begin));
		String salutation = cp.getSalutation();
		String lastName = cp.getSurname();
		String firstName = cp.getGivenName();
		String fullName = getName(cp);
		String refundStatus = "";
		String resultMessage = "";
		String serviceType = RefundServiceType.valueOf(refundServiceType).getDesc();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fefundedDate = sdf.format(refundDate);
		String refundAmount = customerOrderHd.getOrderTotalAmount().toString();
		/**
		 Dear {salutation} {lastName},</p>
		This is to inform you that your refund request has been sent, please kindly find the refund details information below. 
		
		
		Private Member Name: {fullName}
		Service Type: {refundServiceType}
		Refunded Date: {updateDate}
		Refund Amount: {approvedAmt}
		
		Best Regards, HKGTA 

		 */
		String memberEmail = cp.getContactEmail();
		content = templateContent
				.replaceAll(MessageTemplateParams.MemberEmail.getParam(), memberEmail)
				.replaceAll(MessageTemplateParams.Salutation.getParam(), salutation+" ")
				.replaceAll(MessageTemplateParams.FirstName.getParam(), firstName)
				.replaceAll(MessageTemplateParams.LastName.getParam(), lastName)
				.replaceAll(MessageTemplateParams.FullName.getParam(), fullName)
				.replaceAll(MessageTemplateParams.RefundStatus.getParam(), refundStatus)
				.replaceAll(MessageTemplateParams.ServiceType.getParam(), serviceType)
				.replaceAll(MessageTemplateParams.RefundedDate.getParam(), fefundedDate)
				.replaceAll(MessageTemplateParams.RefundAmount.getParam(), refundAmount)
				.replaceAll(MessageTemplateParams.ResultMessage.getParam(), resultMessage);
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setRecipientEmail(memberEmail);
		cec.setSubject(template.getMessageSubject());
		cec.setNoticeType("R");
		cec.setSendDate(new Date());
		try {
begin = System.currentTimeMillis();
			logger.debug("MemberFacilityTypeBookingServiceImpl  sendRefundRequestEmail run start ...serviceType:"+serviceType+"--fefundedDate:"+fefundedDate);
			MailSender.sendEmail(cec.getRecipientEmail(), null, null, cec.getSubject(), cec.getContent(), null);
end = System.currentTimeMillis();
System.out.println("sendRefundRequestEmail call MailSender.sendEmail finish.spend:"+(end - begin));
this.logger.error("sendRefundRequestEmail call MailSender.sendEmail finish.spend:"+(end - begin));
begin = System.currentTimeMillis();
			customerEmailContentDao.save(cec);
			logger.debug("MemberFacilityTypeBookingServiceImpl  sendRefundRequestEmail run end ...serviceType:"+serviceType+"--fefundedDate:"+fefundedDate);
end = System.currentTimeMillis();
System.out.println("sendRefundRequestEmail insert customerEmailContent into DB finish.spend:"+(end - begin));
this.logger.error("sendRefundRequestEmail insert customerEmailContent into DB finish.spend:"+(end - begin));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("send refund request e-mail error", e);
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Refund Confirmation e-mail error"});
		}
	}
	private void sendRefundRequestFacility(MemberFacilityTypeBooking booking, String requesterType, String customerReason, String userId) {

		CustomerOrderHd customerOrderHd = booking.getCustomerOrderHd();
		
		String refundServiceType = null;
		
		if (FacilityName.GOLF.name().equals(booking.getResvFacilityType())) {
			refundServiceType = RefundServiceType.GMS.getName();
		} else if (FacilityName.TENNIS.name().equals(booking.getResvFacilityType())) {
			refundServiceType = RefundServiceType.TMS.getName();
		}
		
		String status = null;
		if ("CUSTOMER".equals(requesterType) && !canRefund(booking)) {
			status = Constant.CustomerRefundRequest.NRF.getName();
		} else {
			status = Constant.CustomerRefundRequest.PND.getName();
		}
		
		sendCustomerRefundRequest(customerOrderHd, refundServiceType, customerReason, requesterType, userId, status);
	}

	@Override
	@Transactional
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingByOrderNo(Long orderNo)
	{
		return memberFacilityTypeBookingDao.getMemberFacilityTypeBookingByOrderNo(orderNo);
	}

	@Override
	@Transactional
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingIncludeAllStatus(long resvId)
	{
		return memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(resvId);
	}
	@Transactional
	@Override
	public void sendPrivateCoachingReservationEmail(long resvId,String userId) {
		if (StringUtils.isEmpty(resvId)) {
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"Private Coaching Reservation: "+resvId});
		}

		MessageTemplate template = null;
		MemberFacilityTypeBooking booking = this.getMemberFacilityTypeBooking(resvId);
		String caption ="";
		if (null != booking && Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(booking.getResvFacilityType())){
			MemberFacilityBookAdditionAttr attr = memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttrList(resvId).get(0);
			FacilityAttributeCaption facilityAttributeCaption = this.facilityAttributeCaptionDao.get(FacilityAttributeCaption.class, attr.getId().getAttributeId());
			caption = facilityAttributeCaption.getCaption();
		}else if(null != booking && Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(booking.getResvFacilityType())){
			com.sinodynamic.hkgta.entity.fms.FacilitySubType facilitySubType = facilitySubTypeDao.getById(booking.getFacilitySubtypeId());
			caption = facilitySubType.getName();
		}
		String templateContent = "";
		String content = "";
		String facilityName	= "";
		String confirmationID = "";
		if(FacilityName.GOLF.name().equals(booking.getResvFacilityType())){
			template = templateDao.getTemplateByFunctionId("pgcrc");
			if (template == null) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching Reservation e-mail: gCoach"});
			}
			templateContent = template.getContentHtml();
			facilityName = "golf";
			confirmationID = "GPC-" + resvId;
		}else if(FacilityName.TENNIS.name().equals(booking.getResvFacilityType())){
			template = templateDao.getTemplateByFunctionId("ptcrc");
			if (template == null) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching Reservation e-mail: tCoach"});
			}
			templateContent = template.getContentHtml();
			facilityName = "tennis";
			confirmationID = "TPC-" + resvId;
		}
		CustomerProfile cp = customerProfileDao.getById(booking.getCustomerId());
		if (cp == null) {
			throw new GTACommonException(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_CUSTOMER_PROFILE);
		}
		String salutation = cp.getSalutation();
		String lastName = cp.getSurname();
		
		String fullName = getName(cp);
		//Timestamp's toString() String object in yyyy-mm-dd hh:mm:ss.fffffffff format
		String temp = booking.getBeginDatetimeBook().toString();
		String startTime = temp.substring(0, temp.indexOf("."));
		temp = booking.getEndDatetimeBook().toString();
		String endTime = temp.substring(0, temp.indexOf("."));
		StaffDto staffDto = this.userMasterDao.getStaffDto(booking.getExpCoachUserId());
		String coachName = "";
		if(!StringUtils.isEmpty(staffDto.getStaffName())){
			coachName = staffDto.getStaffName().trim();
		} else if(StringUtils.isEmpty(staffDto.getNickname())){
			coachName = staffDto.getNickname().trim();
		}else {
			coachName = staffDto.getUserId();
		}
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, booking.getOrderNo());
		CustomerOrderTrans customerOrderTrans = customerOrderHd.getCustomerOrderTrans().get(0);
		String memberEmail = cp.getContactEmail();
		content = templateContent
				.replaceAll(MessageTemplateParams.Salutation.getParam(), salutation)
				.replaceAll(MessageTemplateParams.LastName.getParam(), lastName)
				.replaceAll(MessageTemplateParams.Confirmation.getParam(), confirmationID)
				.replaceAll(MessageTemplateParams.FullName.getParam(), fullName)
				.replaceAll(MessageTemplateParams.StartTime.getParam(), startTime)
				.replaceAll(MessageTemplateParams.EndTime.getParam(), endTime)
				.replaceAll(MessageTemplateParams.CoachName.getParam(), coachName)
				.replaceAll(MessageTemplateParams.FacilityName.getParam(), facilityName);
		if(FacilityName.GOLF.name().equals(booking.getResvFacilityType())){
			content = content.replaceAll(MessageTemplateParams.BayType.getParam(), caption);
		}else if(FacilityName.TENNIS.name().equals(booking.getResvFacilityType())){
			content = content.replaceAll(MessageTemplateParams.TennisType.getParam(), caption);
		}
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setSenderUserId(userId);
		cec.setContent(content);
		cec.setRecipientEmail(memberEmail);
		cec.setSubject(template.getMessageSubject());
		cec.setNoticeType("R");
		cec.setSendDate(new Date());
		customerEmailContentDao.save(cec);
		
		try {
			String receiptType = "golfcoach";
			String fileName = "GolfPrivateCoachingReservationReceipt-";
			if (FacilityName.TENNIS.name().equals(booking.getResvFacilityType())) {
				receiptType = "tenniscoach";
				fileName = "TennisPrivateCoachingReservationReceipt-";
			}
			byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, String.valueOf(customerOrderTrans.getTransactionNo()), receiptType);
			List<byte[]> attachmentList = Arrays.asList(attachment);
			List<String> mineTypeList = Arrays.asList("application/pdf");
			List<String> fileNameList = Arrays.asList(fileName+String.valueOf(customerOrderTrans.getTransactionNo())+".pdf");
			CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
			customerEmailAttach.setEmailSendId(cec.getSendId());
			customerEmailAttach.setAttachmentName(fileNameList.get(0));
			customerEmailAttachDao.save(customerEmailAttach);
			logger.debug("MemberFacilityTypeBookingServiceImpl  sendPrivateCoachingReservationEmail run start ...resvId:"+resvId);
			//send email start
			mailThreadService.sendWithResponse(cec, attachmentList, mineTypeList, fileNameList);
			logger.debug("MemberFacilityTypeBookingServiceImpl  sendPrivateCoachingReservationEmail run end ...resvId:"+resvId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
//		String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
//		cec.setSendId(sendId);
//		try {
//			MailSender.sendEmail(cec.getRecipientEmail(), null, null, cec.getSubject(), cec.getContent(), null);
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("send Private Coaching Reservation e-mail error", e);
//			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching Reservation e-mail error"});
//		}
	}

	
	
	
	@Transactional
	public ResponseResult getBookingRecordsByCustomerId(ListPage<MemberFacilityTypeBooking> page, Long customerId, String appType, String year, String month, String day, String selectedStartDate, String selectedEndDate) throws Exception {
		ListPage<MemberFacilityTypeBooking> listPage = memberFacilityTypeBookingDao.getMemberBookingRecordsByCustomerId(page, new MemberFacilityTypeBookingDto(), customerId, appType, year, month, day, selectedStartDate, selectedEndDate);
		List<Object> listBooking = listPage.getDtoList();

		for (Object item : listBooking) {
			MemberFacilityTypeBookingDto tempDto = (MemberFacilityTypeBookingDto) item;
			String facilityType = tempDto.getFacilityType();
			String status = tempDto.getStatus();
			String enrollStatus = tempDto.getEnrollStatus();
			Date beginDate = tempDto.getBeginDate();
			Date endDate = tempDto.getEndDate();

			/* Wellness*/
			if ("WELLNESS".equals(facilityType)) {
				setWellness(status, tempDto);
			}

			/* Guest Room*/
			if ("GUESTROOM".equals(facilityType)) {
				setGuestRoom(status, tempDto);
			}

			/* Restaurant*/
			if ("RESTAURANT".equals(facilityType)) {
				setRestaurant(status, tempDto);
			}

			/* Tennis/Golf Course*/
			if ("TSS".equals(facilityType) || "GSS".equals(facilityType)) {
				setCourse(enrollStatus, status, tempDto);
			}

			/* Golf/Tennis Court/Bay; Private Coaching*/
			if ("GOLF".equals(facilityType) || "TENNIS".equals(facilityType)) {
				setFacilities(beginDate, endDate, status, tempDto, appType);
			}

		}

		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	private void setWellness(String status, MemberFacilityTypeBookingDto tempDto) {
		Date endDate = tempDto.getEndDate();
		switch (status) {
		case "RSV":
			Date currentDate = new Date();
			int dateEnd = 0;
			if (null != endDate) {
				dateEnd = currentDate.compareTo(endDate);
			}
			if (dateEnd <= 0) {
				tempDto.setStatus("Reserved");
			} else {
				tempDto.setStatus("Overdue");
			}
			break;
		case "ATN":
			tempDto.setStatus("Checked-in");
			break;
		case "NAT":
			tempDto.setStatus("Overdue");
			break;
		case "CAN":
			tempDto.setStatus("Cancelled");
			break;
		case "CLD":
			tempDto.setStatus("Completed");
			break;
		}
	}

	private void setGuestRoom(String status, MemberFacilityTypeBookingDto tempDto) {
		
		if (tempDto.getCheckinTimestamp() == null) {
			switch (status) {
			case "SUC":
				tempDto.setStatus("Booked");
				break;
			case "EXP":
				tempDto.setStatus("Expired");
				break;
			}
		} else if (tempDto.getCheckinTimestamp() != null && tempDto.getCheckoutTimestamp() == null) {
			tempDto.setStatus("Checked-in");
		} else if (tempDto.getCheckinTimestamp() != null && tempDto.getCheckoutTimestamp() != null) {
			tempDto.setStatus("Checked-out");
		}
		
		tempDto.setBeginDateStr(DateConvertUtil.getYMDDate(tempDto.getBeginDate()));
		tempDto.setEndDateStr(DateConvertUtil.getYMDDate(tempDto.getEndDate()));

		List<MemberFacilityTypeBookingDto> roomFacilities = memberFacilityTypeBookingDao.getRoomReservationFacilities(tempDto.getResvId());
		tempDto.setRoomResFacilitys(roomFacilities);
	}

	private void setRestaurant(String status, MemberFacilityTypeBookingDto tempDto) {
		switch (status) {
		case "CFM":
			tempDto.setStatus("Confirmed");
			break;
		case "EXP":
			tempDto.setStatus("Expired");
			break;
		case "ATN":
			tempDto.setStatus("Attended");
			break;
		}
	}

	private void setCourse(String enrollStatus, String status, MemberFacilityTypeBookingDto tempDto) {
		String facilityType = tempDto.getFacilityType();
		switch (enrollStatus) {
		case "ACT":
			tempDto.setEnrollStatus("Waiting for payment");
			break;
		case "REG":
			tempDto.setEnrollStatus("Registered");
			break;
		}

		if (StringUtils.isEmpty(status)) {
			status = "Other";
		}
		switch (status) {
		case "ABS":
			tempDto.setStatus("Absent");
			break;
		case "ATD":
			tempDto.setStatus("Attended");
			break;
		default:
			tempDto.setStatus("Not Attended");
			break;
		}

		List<MemberFacilityTypeBookingDto> listSessions = courseSessionDao.getActiveSessionByCourseId(tempDto.getCourseId());
		int indexSession = 1;
		for (MemberFacilityTypeBookingDto listSession : listSessions) {
			if (listSession.getSessionId().equals(tempDto.getSessionId())) {
				tempDto.setSessionNo(indexSession);
				break;
			}
			indexSession++;
		}
		tempDto.setTotalSessionNo((long) listSessions.size());

		if (CourseType.GOLFCOURSE.getDesc().equals(facilityType)) {
			tempDto.setFacilityName("Golf Course");
			if (tempDto.getSessionId() != null) {
				List<FacilityMasterDto> sessionFacilities = courseSessionDao.getCourseFacilitiesByCourseSessionId(tempDto.getSessionId().intValue());
				tempDto.setVenue("Bay # " + assemableCourseFacilities(sessionFacilities));
			}
		} else if (CourseType.TENNISCOURSE.getDesc().equals(facilityType)) {
			tempDto.setFacilityName("Tennis Course");
			if (Constant.Status.ACT.name().equals(status)) {
				tempDto.setStatus("Waiting for payment");
			}
			if (tempDto.getSessionId() != null) {
				List<FacilityMasterDto> sessionFacilities = courseSessionDao.getCourseFacilitiesByCourseSessionId(tempDto.getSessionId().intValue());
				tempDto.setVenue("Court # " + assemableCourseFacilities(sessionFacilities));
			}
		}

	}

	private void setFacilities(Date beginDate, Date endDate, String status, MemberFacilityTypeBookingDto tempDto, String appType) {
		String facilityType = tempDto.getFacilityType();
		Date currentDate = new Date();
		Calendar cal = Calendar.getInstance();
		int dateBegin = 0;
		int dateEnd = 0;
		if (beginDate != null) {

			cal.setTime(beginDate);
			cal.add(Calendar.MINUTE, -30);
			dateBegin = currentDate.compareTo(cal.getTime());
			if (null != endDate)
				dateEnd = currentDate.compareTo(endDate);
		}

		if (Constant.Status.ATN.name().equals(status)) {
			tempDto.setStatus("Checked-in");
		} else {
			if ("PC".equals(appType)) {
				if (dateEnd > 0) {
					tempDto.setStatus("Overdue");
				} else {
					tempDto.setStatus("Ready to Check-in");
				}
			} else {
				if (dateBegin >= 0 && dateEnd <= 0) {
					tempDto.setStatus("Ready to Check-in");
				} else if (dateBegin < 0) {
					tempDto.setStatus("Check-in Pending");
				} else if (dateEnd > 0) {
					tempDto.setStatus("Overdue");
				}
			}

		}

		if (FacilityName.GOLF.name().equalsIgnoreCase(facilityType)) {
			if (StringUtils.isEmpty(tempDto.getCoachName())) {
				tempDto.setFacilityName("Golfing Bay");
			} else {
				tempDto.setFacilityName("Golf Private Coaching");
			}

			String facilities = assemableBayCourtCoachingFacilities(tempDto.getResvId());
			if (StringUtils.isEmpty(facilities)) {
				facilities = "N/A";
			} else {
				facilities = "Bay # " + facilities;
			}
			tempDto.setVenue(facilities);
		} else if (FacilityName.TENNIS.name().equalsIgnoreCase(facilityType)) {
			if (StringUtils.isEmpty(tempDto.getCoachName())) {
				if (FacilitySubType.TENNISCRTPAD.getDesc().equals(tempDto.getVenueCode()) || FacilitySubType.TENNISCRTMINI.getDesc().equals(tempDto.getVenueCode()) || FacilitySubType.TENNISCRTITFID.getDesc().equals(tempDto.getVenueCode())) {
					tempDto.setFloorNo(Long.valueOf(2));
				} else if (FacilitySubType.TENNISCRTHALF.getDesc().equals(tempDto.getVenueCode()) || FacilitySubType.TENNISCRTITFOD.getDesc().equals(tempDto.getVenueCode()) || FacilitySubType.TENNISCRTSTD.getDesc().equals(tempDto.getVenueCode())) {
					tempDto.setFloorNo(Long.valueOf(3));
				}
				tempDto.setFacilityName("Tennis Court");
			} else {
				tempDto.setFacilityName("Tennis Private Coaching");
			}

			String facilities = assemableBayCourtCoachingFacilities(tempDto.getResvId());
			if (StringUtils.isEmpty(facilities)) {
				facilities = "N/A";
			} else {
				facilities = "Court # " + facilities;
			}
			tempDto.setVenue(facilities);
		}

		if (!StringUtils.isEmpty(tempDto.getCoachName())) {
			StaffProfile staffProfile = staffProfileDao.getStaffProfileByUserId(tempDto.getCoachName());
			if (staffProfile != null) {
				tempDto.setCoachName(staffProfile.getGivenName() + " " + staffProfile.getSurname());
			}
		}
	}
	
	private String assemableCourseFacilities(List<FacilityMasterDto> courseSessionFacilities){
		StringBuilder facilities = new StringBuilder("");
		for(FacilityMasterDto courseSessionFacility: courseSessionFacilities){
			facilities.append(courseSessionFacility.getFacilityName()+", ");
		}
		if(facilities.length()>=2){
			return facilities.substring(0, facilities.length() - 2);
		}else{
			return facilities.toString();
		}
	}
	
	@SuppressWarnings("unchecked")
	private String assemableBayCourtCoachingFacilities(Long resvId){
		ResponseResult responseFacilities = facilityCheckInService.returnCheckedInFacilitiesByResvId(resvId);
		FacilityCheckInDto<Object> facilityCheckInDto =   (FacilityCheckInDto<Object>) responseFacilities.getDto();
		List<FacilityMasterDto> checkedInFacilities = facilityCheckInDto.getCheckedInFacilities();
		StringBuilder facilities = new StringBuilder("");
		for(FacilityMasterDto bayCourtCoachFacility: checkedInFacilities){
			facilities.append(bayCourtCoachFacility.getFacilityName()+", ");
		}
		if(facilities.length()>=2){
			return facilities.substring(0, facilities.length() - 2);
		}else{
			return facilities.toString();
		}
	}
	@Transactional
	public List<FacilityReservationDto> getAllTodayMemberFacilityTypeBooking(boolean isPushMessage)
	{
		try
		{
			String sql = memberFacilityTypeBookingDao.getAllMemberFacilityTypeBooking("ALL", "TODAY", null,isPushMessage);
			List<FacilityReservationDto> facilityReservationDtoList = memberFacilityTypeBookingDao.getDtoBySql(sql, Arrays.asList(new Object[] {new Date(), new Date(), new Date(), new Date()}), FacilityReservationDto.class);
			return facilityReservationDtoList;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	@Transactional
	public FacilityTransactionInfo getFacilityTransactionInfo(long resvId) {

		MemberFacilityTypeBooking memberFacilityTypeBooking = getMemberFacilityTypeBooking(resvId);
		FacilityTransactionInfo dto = new FacilityTransactionInfo();

		CustomerOrderHd customerOrderHd = memberFacilityTypeBooking.getCustomerOrderHd();
		dto.setResvId(memberFacilityTypeBooking.getResvId());

		if (customerOrderHd == null) {
			throw new GTACommonException(GTAError.FacilityError.TRANSACTION_RECORD_NOT_FOUND);
		}

		List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
		if (customerOrderTranses != null && !customerOrderTranses.isEmpty()) {

			for (CustomerOrderTrans customerOrderTran : customerOrderTranses) {

				if (CustomerTransationStatus.SUC.getDesc().equals(customerOrderTran.getStatus())) {
					dto.setTransactionNo(customerOrderTran.getTransactionNo());
					dto.setPaymentMethod(customerOrderTran.getPaymentMethodCode());
					dto.setMedia(customerOrderTran.getPaymentMedia());
					String expense = customerOrderTran.getPaidAmount().setScale(2).toPlainString();
					dto.setExpense(expense);
					DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
					dto.setPaymentDate(df.format(customerOrderTran.getTransactionTimestamp()));
					dto.setLocation(customerOrderTran.getPaymentLocationCode());
				}
			}
		}

		return dto;
	}
	/**   
	* @author: Zero_Wang
	* @since: Sep 8, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public MemberFacilityTypeBooking getCancelBooking(Long resvId) {
		return this.memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, resvId);
	}
	@Override
	@Transactional
	public int getbookingTimes(MemberFacilityBookingDto booking) {
		try {
			String hql = "FROM  MemberFacilityTypeBooking m where customerId=? and beginDatetimeBook>? and endDatetimeBook<? and resvFacilityType=? and expCoachUserId is not null";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date d = sdf.parse(booking.getBookingDate());
			Timestamp begin = new Timestamp(d.getTime());
			Calendar now= Calendar.getInstance();
			now.setTime(begin);
			now.add(Calendar.DAY_OF_MONTH, 1);
			Timestamp end =  new Timestamp(now.getTimeInMillis());
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(booking.getCustomerId());
			param.add(begin);
			param.add(end);
			param.add(booking.getFacilityType());
			int times = 0;
			List<MemberFacilityTypeBooking> list = memberFacilityTypeBookingDao.getByHql(hql, param);
			if(list!=null ){
				if(list.size()>0){
					for(MemberFacilityTypeBooking m :list){
						if(Constant.reserve_via.APS.toString().equals(m.getReserveVia()) || Constant.reserve_via.WP.toString().equals(m.getReserveVia())){
							times = times+1;
						}
					}
					return times;
				}
				return list.size();
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	@Override
	@Transactional
	public MemberFacilityBookingDto saveAndPayBooking(
			MemberFacilityBookingDto dto) throws Exception {
		Map<String,Object>  map = this.saveReservationOrder(dto);
		MemberFacilityBookingDto returnDto = this.doPay(map);
		return returnDto;
	}
	@Override
	@Transactional
	public void update(MemberFacilityTypeBooking booking) {
		this.memberFacilityTypeBookingDao.update(booking);
		
	}
	@Override
	public int getMemberFacilityBookedCountForQuota(String facilityType,Date bookingDateTime)throws Exception
	{
		
		return memberFacilityTypeBookingDao.getMemberFacilityBookedCountForQuota(facilityType,bookingDateTime);
	}
	
	@Transactional
	public ResponseResult getDailyMonthlyFacilityUsage(String timePeriodType,ListPage page, String selectedDate, String facilityType){
		ListPage<MemberFacilityTypeBooking> listPage = memberFacilityTypeBookingDao.getDailyMonthlyFacilityUsage(timePeriodType,page, selectedDate, facilityType);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	@Transactional
	public byte[] getDailyFacilityUsageAttach(String timePeriodType,String selectedDate, String fileType, String sortBy, String isAscending, String facilityType){
		try {
			return memberFacilityTypeBookingDao.getDailyMonthlyFacilityUsageAttachment(timePeriodType, selectedDate, fileType, sortBy, isAscending, facilityType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@Override
	@Transactional
	public Long getresvIdByFacilityTimeslotId(long facilityTimeslotId,long customerId) {
		String sql = "select mf.resv_id resid from member_facility_type_booking mf inner join member_reserved_facility m on mf.resv_id = m.resv_id where m.facility_timeslot_id=? and mf.customer_id=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(facilityTimeslotId);
		param.add(customerId);
		BigInteger temp = (BigInteger) this.memberFacilityTypeBookingDao.getUniqueBySQL(sql, param);
		Long resvId = temp.longValue(); 
		return resvId;
	}
	@Override
	@Transactional
	public ResponseResult getDailyMonthlyPrivateCoaching(String timePeriodType, ListPage page, String selectedDate, String facilityType)
	{
		ListPage<MemberFacilityTypeBooking> listPage = memberFacilityTypeBookingDao.getDailyMonthlyPrivateCoaching(timePeriodType,page, selectedDate, facilityType);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	@Override
	@Transactional
	public byte[] getDailyMonthlyPrivateCoachingAttach(String timePeriodType, String selectedDate, String fileType, String sortBy, String isAscending, String facilityType)
	{
		try {
			return memberFacilityTypeBookingDao.getDailyMonthlyPrivateCoachingAttachment(timePeriodType, selectedDate, fileType, sortBy, isAscending, facilityType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}