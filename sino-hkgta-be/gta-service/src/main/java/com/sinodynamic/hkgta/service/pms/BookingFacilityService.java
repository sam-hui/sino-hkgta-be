package com.sinodynamic.hkgta.service.pms;

import java.util.Date;

import com.sinodynamic.hkgta.entity.fms.TLock;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface BookingFacilityService extends IServiceBase{
	public Long bookTimeslot(Long customerId,Date startTime,Date endTime,String facilityType,String attributeId,String status);
	
	public Long bookFacility(Long customerId,Date startTime,Date endTime,String facilityType,String attributeId,String status);
	
	public Long bookFacility(String paymentMethod, Long customerId,Date startTime,Date endTime,String facilityType,String attributeId,String status);
	
	public void deleteReservationById(Long resvId);
	
	public void saveOrUpdateLock(TLock lock);
	
	public void testNestTransaction(TLock lock);
}
