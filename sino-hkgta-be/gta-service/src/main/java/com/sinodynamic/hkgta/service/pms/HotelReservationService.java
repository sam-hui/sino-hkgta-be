package com.sinodynamic.hkgta.service.pms;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.crm.GuessRoomReservationDetailDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;

public interface HotelReservationService extends IServiceBase {
	CustomerOrderTrans hotelReservationOnlinePayment(HotelReservationPaymentDto paymentDto);
	CustomerOrderTrans hotelReservationCashvaluePayment(HotelReservationPaymentDto paymentDto);
	void bookHotel(List<RoomReservationDto> reservations);
	void updateRoomReservationStatus(String confirmId, RoomReservationStatus status);
	void updateRoomReservationStatusInSameTxn(String confirmId, RoomReservationStatus status);
	void requestCancelPayment(String confirmId, HotelReservationCancelDto cancelDto);
	List<RoomReservationRec> getAllFailedReservations();
	List<RoomReservationRec> getAllFailedReservations(Integer timeoutMin);
	List<Long> getBundledFacilityBookings(String confirmId);
	void cancelBundledFacilityBookings(String confirmId, HotelReservationCancelDto cancelDto);
	GuessRoomReservationDetailDto getHotelReservationDetail(String confirmId);
	void updateGuessroomAndFacilityAssociation(String loginUserId, Map<String, List<Long>> association, Map<String, Object> params);
	Long getRefundIdIfAvailable(String confirmId);
	Map<String, Object> exposeTxnInfo(String confirmId);
}
