package com.sinodynamic.hkgta.service.crm.membercash;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface VirtualAccountTaskService extends IServiceBase{
	//Added by Nick 2015,08/24 start
	public void topupCashValueByVirtualAccInBatch(List<CashvalueTopupHistory> list);
	//Added by Nick 2015,08/24 end
		
	public void sendTopUpEmail(CashvalueTopupHistory topUp, MemberCashvalue memberCashvalue);
		
	public void processPndChTransactions(List<CashvalueTopupHistory> list);
		
	public void processRejChTransactions(List<CashvalueTopupHistory> list);
		
	public void downloadVirtualAccountTransactionExtract();
}
