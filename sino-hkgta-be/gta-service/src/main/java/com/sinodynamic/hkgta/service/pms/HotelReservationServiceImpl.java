package com.sinodynamic.hkgta.service.pms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.pms.RoomFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dto.crm.GuessRoomReservationDetailDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.HotelReservationItemInfoDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.HotelReservationPaymentInfoDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.pms.RoomFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
public class HotelReservationServiceImpl extends ServiceBase implements
		HotelReservationService {
	static Logger log = Logger.getLogger(HotelReservationServiceImpl.class);
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private RoomReservationRecDao roomReservationRecDao;
	
	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;
	
	@Autowired
	private RoomFacilityTypeBookingDao roomFacilityTypeBookingDao;
	
	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	CustomerOrderTrans nonsplitPayment(HotelReservationPaymentDto paymentDto){
		Date currentDate = paymentDto.getCurrentDate();
		String staffUserId = paymentDto.getUserId();
		Long customerId = paymentDto.getCustomerId();
		Long orderNo = null;
		
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		customerOrderHd.setStaffUserId(staffUserId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo()));
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value) ||
			StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.PRE_AUTH) ||
			StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH)){
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
		}else{
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		}
		orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
		
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
			BigDecimal itemAmount = itemInfo.getItemAmount();
			
			
			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(temp);
			customerOrderDet.setItemNo(itemInfo.getItemNo());
			customerOrderDet.setItemRemark("");
			customerOrderDet.setOrderQty(itemInfo.getNights());
			customerOrderDet.setItemTotalAmout(itemAmount.multiply(new BigDecimal(itemInfo.getNights())));
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setCreateBy(staffUserId);
			customerOrderDet.setUpdateDate(currentDate);
			customerOrderDet.setUpdateBy(staffUserId);
			customerOrderDet.setExtRefNo(itemInfo.getReservationId());
			customerOrderDetDao.saveOrderDet(customerOrderDet);

			RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", itemInfo.getReservationId());
			book.setStatus(RoomReservationStatus.PAY.name());
			book.setOrderNo(orderNo);
		}	
			
		Assert.isTrue(paymentDto.getHotelReservationPaymentsInfo().size() != 0, "No transaction is available");
		Assert.isTrue(paymentDto.getHotelReservationPaymentsInfo().size() == 1, "Multiple transactions are not available");
		HotelReservationPaymentInfoDto paymentInfo = paymentDto.getHotelReservationPaymentsInfo().get(0);
		
		BigDecimal paidAmount = paymentInfo.getPaidAmount()==null ? 
				getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo()):paymentInfo.getPaidAmount();
		//UPDATE CASHVALUE
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value)){
			MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
			BigDecimal deductedAmount = memberCashvalue.getAvailableBalance().subtract(paidAmount);
			
			if (memberCashvalue.getAvailableBalance().compareTo(paidAmount) < 0)
			{
				MemberLimitRule memberLimitRuleCR = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, LimitType.CR.name());
				MemberLimitRule memberLimitRuleTRN = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, LimitType.TRN.name());
				
				MemberLimitRule memberLimitRule = memberLimitRuleCR!=null ? memberLimitRuleCR : memberLimitRuleTRN;
				
				if(null != memberLimitRule){
					BigDecimal numValue = memberLimitRule.getNumValue();
					if(((memberCashvalue.getAvailableBalance().subtract(paidAmount)).negate()).compareTo(numValue) > 0 ){
						throw new GTACommonException(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_CREDITLIMIT);
					}
				}else{
					throw new GTACommonException(GTAError.MemberError.MEMBER_INSUFFICIENT_BALANCE);
				}
			}
			
			//Assert.isTrue(deductedAmount.compareTo(BigDecimal.ZERO)>=0, "The order amount can't be greater than Cash Value Balance");
			memberCashvalue.setAvailableBalance(deductedAmount);
			memberCashValueDao.updateMemberCashValue(memberCashvalue);
		}
		
		// CUSTOMER ORDER TRANS
		String agentTransactionNo = paymentInfo.getAgentTransactionNo();
		String terminalId = paymentInfo.getTerminalId();
		
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(paidAmount);
		customerOrderTrans.setPaymentRecvBy(staffUserId);
		customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
		customerOrderTrans.setTerminalId(terminalId);
		
		
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value)){
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CV.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH)){
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASH.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.PRE_AUTH)){
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.PREAUTH.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CREDIT_CARD)){
            customerOrderTrans.setStatus(Constant.Status.PND.name());
            customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
            customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		}
		//Only CASH , CASHVALUE, CREDITCARD need to be create txn record
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value) ||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CREDIT_CARD) ||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH)){
			
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		}
		
		
		return customerOrderTrans;
	}
	
	@Override
	@Transactional
	public CustomerOrderTrans hotelReservationOnlinePayment(final HotelReservationPaymentDto paymentDto) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (CustomerOrderTrans)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				
				
				return nonsplitPayment(paymentDto);
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
	}

	public MemberCashvalue getMemberCashvalue(Long customerId) {
		Member member = memberDao.get(Member.class, customerId);
		
		if (member == null) {
			throw new GTACommonException(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
		}
		
		MemberCashvalue memberCashvalue = null;
		String type = member.getMemberType();

		if (Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)) {
			Long superiorMemberId = member.getSuperiorMemberId();

			if (superiorMemberId != null) {
				memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
			}
		} else {
			memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
		}

		if (memberCashvalue == null) {
			logger.error("member cash value not found for customer" + customerId);
			throw new GTACommonException(GTAError.FacilityError.REFUND_NOT_AVAILABLE);
		}
		return memberCashvalue;

	}
	
	BigDecimal getTotalOrderAmount(List<HotelReservationItemInfoDto> items){
		BigDecimal totalAmount = BigDecimal.ZERO;
		for(HotelReservationItemInfoDto item : items){
			BigDecimal nights = new BigDecimal(item.getNights());
			
			totalAmount = totalAmount.add(item.getItemAmount().multiply(nights));
		}
		return totalAmount;
	}
	
	@Override
	@Transactional
	public CustomerOrderTrans hotelReservationCashvaluePayment(final HotelReservationPaymentDto paymentDto) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (CustomerOrderTrans)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				
				
				return nonsplitPayment(paymentDto);
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
	}

	@Override
	@Transactional
	public void bookHotel(final List<RoomReservationDto> reservations) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				for(RoomReservationDto reservation: reservations){
					RoomReservationRec resvRec = new RoomReservationRec();
					resvRec.setConfirmId(reservation.getReservationId());
					resvRec.setArrivalDate(DateUtils.parseDate(reservation.getStartDate(), new String[]{"yyyy-MM-dd"}));
					resvRec.setDepartureDate(DateUtils.parseDate(reservation.getEndDate(), new String[]{"yyyy-MM-dd"}));
					resvRec.setNight(reservation.getNoOfStayingNight());
					resvRec.setRoomTypeCode(reservation.getRoomTypeCode());
					resvRec.setCreateBy(reservation.getCreateBy());
					resvRec.setCreateDate(reservation.getCreateDate());
					resvRec.setUpdateBy(reservation.getUpdateBy());
					resvRec.setUpdateDate(reservation.getUpdateDate());
					resvRec.setCustomerId(reservation.getCustomerId());
					resvRec.setRequestDate(reservation.getRequestDate());
					resvRec.setStatus(Constant.RoomReservationStatus.RSV.name());
					roomReservationRecDao.save(resvRec);
				}
				return null;
			}
		});
		
	}
	
	void _updateRoomReservationStatus(final String confirmId, final RoomReservationStatus status){
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
				
				book.setStatus(status.name());
				roomReservationRecDao.save(book);
				
				return null;
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
	}
	
	/**   
	* @author: Ray_Liang
	* @since: Nov 2, 2015
	* 
	* @description
	* Please call session.refresh() out of this method if you want to update the state of the entity 
	*/  
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void updateRoomReservationStatus(final String confirmId, final RoomReservationStatus status){
		_updateRoomReservationStatus(confirmId, status);
	}

	@Override
	@Transactional
	public List<RoomReservationRec> getAllFailedReservations() {
		try{
			return roomReservationRecDao.getAllFailedReservations(20);
		}catch(Exception e){
			log.error("getAllFailedReservations fail", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

	
	
	@Override
	@Transactional
	public List<RoomReservationRec> getAllFailedReservations(Integer timeoutMin) {
		try{
			return roomReservationRecDao.getAllFailedReservations(timeoutMin);
		}catch(Exception e){
			log.error("getAllFailedReservations(timeout) fail", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}

	@Override
	@Transactional
	public void updateGuessroomAndFacilityAssociation(String loginUserId, Map<String, List<Long>> association, Map<String, Object> params) {
		try{
			Date now = new Date();
			for(String confirmId : association.keySet()){
				for(Long facilityBookingId : association.get(confirmId)){
					RoomFacilityTypeBooking relation = new RoomFacilityTypeBooking();
					relation.setCreateBy(loginUserId);
					relation.setCreateDate(now);
					relation.setIsBundle("Y");
					relation.setFacilityTypeBookingId(facilityBookingId);
					
					RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
					
					relation.setRoomBookingId(new Long(book.getResvId()));
					
					roomFacilityTypeBookingDao.save(relation);
				}
			}
		}catch(Exception e){
			log.error("updateGuessroomAndFacilityAssociation fail", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}

	@Override
	@Transactional
	public void updateRoomReservationStatusInSameTxn(String confirmId, RoomReservationStatus status) {
		_updateRoomReservationStatus(confirmId, status);
	}

	CustomerOrderDet getOriginDetail(RoomReservationRec resv){
		Assert.notNull(resv.getOrderNo(), "No order info for this reservation");
		
		List<CustomerOrderDet> details = customerOrderHdDao.getOrderById(resv.getOrderNo()).getCustomerOrderDets();
		CustomerOrderDet result = null;
		for(CustomerOrderDet detail : details){
			if(ObjectUtils.equals(detail.getExtRefNo(), resv.getConfirmId()) &&
					ObjectUtils.equals(detail.getCustomerOrderHd().getOrderNo(), resv.getOrderNo())	){
				result = detail;
				break;
			}
		}
		
		Assert.notNull(result, "No order item info for this reservation");
		return result;
	}
	
	BigDecimal getOriginItemAmount(RoomReservationRec resv){
		CustomerOrderDet det = getOriginDetail(resv);
		return det.getItemTotalAmout();
	}
	
	Long getOriginTxn(RoomReservationRec resv){
		Long txnNo = getOriginTxnInfo(resv).getTransactionNo();
		return txnNo;
	}
	
	CustomerOrderTrans getOriginTxnInfo(RoomReservationRec resv){
		Assert.notNull(resv.getOrderNo(), "No order info for this reservation");
		
		List<CustomerOrderTrans> txns = customerOrderHdDao.getOrderById(resv.getOrderNo()).getCustomerOrderTrans();
		Assert.notEmpty(txns, "No transaction info for this reservation");
		
		return txns.get(0);
	}
	
	/**   
	* transactionItemDetail  -->  CustomerOrderDet
	* transactionDetail  -->  CustomerOrderTrans 
	* @since: Jan 11, 2016
	* 
	*/  
	@Override
	@Transactional
	public Map<String, Object> exposeTxnInfo(String confirmId) {
		Map<String, Object> txnInfo = new HashMap<String, Object>();
		RoomReservationRec book = roomReservationRecDao.getReservationByConfirmId(confirmId);
		
		txnInfo.put("transactionItemDetail", getOriginDetail(book));
		txnInfo.put("transactionDetail", getOriginTxnInfo(book));
		
		return txnInfo;
	}

	Long getOriginDetailItem(RoomReservationRec resv){
		CustomerOrderDet det = getOriginDetail(resv);
		return det.getOrderDetId();
	}
	
	@Override
	@Transactional
	public void requestCancelPayment(String confirmId, HotelReservationCancelDto cancelDto) {
		try{
			RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId.toString());
			
			CustomerRefundRequest refundRequest = new CustomerRefundRequest();
			refundRequest.setRefundTransactionNo(getOriginTxn(book));
			refundRequest.setRequesterType(cancelDto.getRequesterType());
			refundRequest.setRefundServiceType(RefundServiceType.ROOM.name());
			refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
			refundRequest.setCustomerReason(cancelDto.getCancelReason());
			refundRequest.setStatus(Status.PND.name());
			refundRequest.setRequestAmount(getOriginItemAmount(book));
			refundRequest.setCreateBy(cancelDto.getUserId());
			refundRequest.setCreateDate(cancelDto.getCurrentDate());
			refundRequest.setOrderDetailId(getOriginDetailItem(book));
			
			customerRefundRequestDao.save(refundRequest);
		}catch(Exception e){
			log.error("requestCancelPayment fail", e);
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Long> getBundledFacilityBookings(final String confirmId) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (List<Long>)executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				RoomReservationRec book = roomReservationRecDao.getReservationByConfirmId(confirmId);
				List<RoomFacilityTypeBooking> bundles = roomFacilityTypeBookingDao.getByHql("from RoomFacilityTypeBooking where roomBookingId = ?", Arrays.<Serializable>asList(book.getResvId()) );
				
				List<Long> facilityResvIdList = new ArrayList<Long>();
				
				for(RoomFacilityTypeBooking bundle : bundles){
					MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(bundle.getFacilityTypeBookingId());//get facility booking
					
					facilityResvIdList.add(booking.getResvId());
				}
				
				return facilityResvIdList;
			}
		});
	}

	@Override
	@Transactional
	public GuessRoomReservationDetailDto getHotelReservationDetail(final String confirmId) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (GuessRoomReservationDetailDto)executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				RoomReservationRec book = roomReservationRecDao.getReservationByConfirmId(confirmId);
				GuessRoomReservationDetailDto guessRoomDetail = new GuessRoomReservationDetailDto();
				guessRoomDetail.setArrivalDate(book.getArrivalDate());
				guessRoomDetail.setDepartureDate(book.getDepartureDate());
				guessRoomDetail.setItemAmount(getOriginItemAmount(book));
				guessRoomDetail.setNights(book.getNight());
				guessRoomDetail.setRoomType(book.getRoomTypeCode());
				guessRoomDetail.setStatus(book.getStatus());
				
				List<Date[]> golfingBayPeriod = new ArrayList<Date[]>();
				List<Long> resvIds = getBundledFacilityBookings(confirmId);
				for(Long id : resvIds){
					MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(id);
					
					List<FacilityTimeslot> timeslots = booking.getFacilityTimeslots();
					
					if(CollectionUtils.isNotEmpty(timeslots)){
						FacilityTimeslot slot = timeslots.get(0);//This facility booking only has one timeslot
						Date[] begin_end = new Date[2];
						begin_end[0] = slot.getBeginDatetime();
						begin_end[1] = slot.getEndDatetime();
						golfingBayPeriod.add(begin_end);
					}
				}
				
				guessRoomDetail.setGolfingBayPeriod(golfingBayPeriod);
				return guessRoomDetail;
			}
		});
	}

	@Override
	@Transactional
	public void cancelBundledFacilityBookings(final String confirmId, final HotelReservationCancelDto cancelDto) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				for(Long facilityResvId : getBundledFacilityBookings(confirmId)){
					memberFacilityTypeBookingService.cancelMemberFacilityTypeBooking(facilityResvId, cancelDto.getUserId(), cancelDto.getCancelReason(), cancelDto.getRequesterType(), "false");
				}
				return null;
			}
		});
	}

	@Override
	@Transactional
	public Long getRefundIdIfAvailable(final String confirmId) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (Long)executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				RoomReservationRec roomReservationRec = roomReservationRecDao.getReservationByConfirmId(confirmId);
				Long detailItemId = getOriginDetailItem(roomReservationRec);
				CustomerRefundRequest refundRequest = customerRefundRequestDao.getUniqueByCol(CustomerRefundRequest.class, "orderDetailId", detailItemId);
				if(refundRequest == null){
					return null;
				}else{
					return refundRequest.getRefundId();
				}
			}
		});
		
	}
}
