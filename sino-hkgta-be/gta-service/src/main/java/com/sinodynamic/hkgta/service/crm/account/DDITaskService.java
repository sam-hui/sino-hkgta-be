package com.sinodynamic.hkgta.service.crm.account;

import com.sinodynamic.hkgta.service.IServiceBase;

public interface DDITaskService extends IServiceBase{

	/**
	 * upload DDI transaction report
	 *//*
	public void generateDDIReporter();
	
	//TODO --pending
	public void parseDDIAckReporter(File file);
	
	*//**
	 * download DDI reject reporter
	 * @param file
	 *//*
	public void parseDDIRejectReporter(File file);
	
	public void test();*/
	
	public void generateDDIReport();
	
	public void parseDDIRejectReporter();
	
}
