package com.sinodynamic.hkgta.service.crm.sales.template;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dto.crm.MessageTemplateDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class MessageTemplateServiceImpl extends ServiceBase<MessageTemplate>
		implements MessageTemplateService {

	@Autowired
	private MessageTemplateDao templateDao;
	
	@Autowired 
	private CustomerProfileDao customerProfileDao;

	@Override
	@Transactional
	public MessageTemplate getTemplateByFuncId(String colValue) {

		return templateDao.getTemplateByFunctionId(colValue);
	}

	@Override
	@Transactional
	public boolean saveMessageTemplate(MessageTemplateDto mtd,
			Long templateId) throws Exception {

		MessageTemplate mt = null;
		Date date = new Date();
		String templateType = mtd.getFunctionId();
		String templateName = mtd.getTemplateName();
		String content = mtd.getContent();
		String contentHtml = mtd.getContentHtml();
		String messageSubject = mtd.getMessageSubject();
		String stuffId = mtd.getStaffId();
		if (templateId == null) {

			mt = new MessageTemplate();
			mt.setFunctionId(templateType);
			mt.setTemplateName(templateName);
			mt.setContent(content);
			mt.setContentHtml(contentHtml);
			mt.setMessageSubject(messageSubject);
			mt.setCreateBy(stuffId);
			mt.setCreateDate(new Timestamp(date.getTime()));
			mt.setUpdateBy(stuffId);
			mt.setUpdateDate(date);
		} else {

			mt = templateDao.getMessageTemplateById(templateId);
			if (mt == null)
				return false;

			mt.setFunctionId(templateType);
			mt.setTemplateName(templateName);
			mt.setContent(content);
			mt.setContentHtml(contentHtml);
			mt.setMessageSubject(messageSubject);
			mt.setTemplateId(templateId);
			mt.setUpdateBy(stuffId);
			mt.setUpdateDate(date);
		}

		return templateDao.saveMessageTemplate(mt);
	}

	@Override
	@Transactional
	public List<MessageTemplate> getAllMessageTemplate() throws Exception {
		return templateDao.selectAllMessageTemplate();
	}

	@Override
	@Transactional
	public boolean deleteMessageTemplate(Long templateId) throws Exception {
		return templateDao.deleteMessageTemplateById(templateId);
	}
	
	/**
	 * This is a public method for get email template
	 * @param templateType
	 * @param customerId
	 * @param params The parameters need to replace.
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getEmailTemplate(String templateType,Long customerId,Map<String, String> params) {
		MessageTemplate template = null;
		CustomerProfile cp = null;
		
		try {
			
			template = templateDao.getTemplateByFunctionId(templateType);
			cp = customerProfileDao.getById(customerId);
			if (template == null) {
				logger.error("templateType is invalid!");
				responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_TYPE_ERROR);
				return responseResult;
			}
			if (cp == null) {
				logger.error("customerId is invalid!");
				responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_CUSTOMERID_ERROR);
				return responseResult;
			}
			
			String content = template.getContentHtml();
			content.replace("\\n", "\n");
			for (String key : params.keySet()) {
				content.replace(key, params.get(key));
			}
			MessageTemplateDto dto = new MessageTemplateDto();
			dto.setFunctionId(template.getFunctionId());
			dto.setTemplateName(template.getTemplateName());
			dto.setMessageSubject(template.getMessageSubject());
			dto.setContent(content);
			dto.setContactEmail(cp.getContactEmail());
			dto.setCustomerId(customerId);
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_OTHER_ERROR);
		}
				
		return responseResult;
	}

	
	@Override
	@Transactional
	public ResponseResult getTemplateTypeList() {
		
		List<String> result = templateDao.selectTemplateTypeList();
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getTemplatesByType(String templateType) {
		
	    if (StringUtils.isEmpty(templateType)) return null;
	    List<MessageTemplate> result = templateDao.getMessageTemplatesByType(templateType);
	    responseResult.initResult(GTAError.Success.SUCCESS, result);
	    return responseResult;
	}
	
}
