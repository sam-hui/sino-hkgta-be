package com.sinodynamic.hkgta.service.crm.ibeacon;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.dto.crm.IBeaconNearbyPersonDto;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.ibeacon.Ibeacon;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface IbeaconService extends IServiceBase<IbeaconNearbyPerson> {

	public ResponseResult addVistorFromApp(IBeaconNearbyPersonDto dto) throws Exception;

	public ResponseResult getRecogBeaconList(ListPage<IbeaconNearbyPerson> page, String byMajor, String status,String targetTypeCode);

	public ResponseResult changeIbeaconStatus(BeaconManageDto dto, String currentUserId);

	public ResponseResult saveOrEditIbeaconInfo(BeaconManageDto dto, String currentUserId) throws Exception;

	public ResponseResult getByLocationType(String status);

	public ResponseResult deleteIbeacon(Long ibeaconIdDB,String loginUserId);

	public ResponseResult getBeaconMember(Long[] poiIdArray, String venueCode, Integer beaconMemberCount);

	public ResponseResult getBeaconByVenueCode(String venueCode);

	public ResponseResult checkAvailability(Long major, Long minor, Long ibeaconIdDB);

	public ResponseResult updateMajor(Long major,String targetType);

	public List<AdvanceQueryConditionDto> assembleQueryConditions(String module);

	public ResponseResult getRoomBeaconsList();

	public ResponseResult getTheNextAvailableMinorId(Long major);
	
	public ResponseResult getMemberByBeaconIdForTablet(ListPage<Ibeacon> page,String resvSortBy,Integer resvPageNumber,Integer resvPageSize, String resvIsAscending,String venueCode) throws Exception;
	
}
