package com.sinodynamic.hkgta.service.pms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.pms.RoomFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.pms.CancelReservationDto;
import com.sinodynamic.hkgta.dto.pms.HotelPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto.HotelReservationCancelItemDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationChangeDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.HotelReservationItemInfoDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.PaymentResult;
import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.pms.RoomFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberTransactionService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.rpos.RefundService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.MessageResult;

@Service
public class HotelReservationServiceFacade{
	static Logger logger = Logger.getLogger(HotelReservationServiceFacade.class);
	
	@Resource(name="appProperties") 
	protected Properties appProps;
	
	@Autowired
	private HotelReservationService hotelReservationService;
	
	@Autowired
	private PMSApiService pmsApiService;
	
	@Autowired
	private RoomTypeService roomTypeService;

	@Autowired
	private MemberTransactionService memberCashValueService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private PMSRequestProcessorService pmsRequestProcessorService;
	
	@Autowired
	private RoomReservationRecDao roomReservationRecDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private RoomFacilityTypeBookingDao roomFacilityTypeBookingDao;
	
	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private RefundService refundService;
	
	@Autowired
	private GlobalParameterService globalParameterService;

	@Transactional
	public String getGuestroomAdvanceSearch(Map<String, org.hibernate.type.Type> typeMap) {
		
		
		String sql = roomReservationRecDao.getGuestroomAdvanceSearch(typeMap);
		
		return sql;
	}
	
	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;
	
	private String generateMsg(Map<Object, ?> msgs){
		StringBuilder string = new StringBuilder();
		for(Object msgKey : msgs.keySet()){
			string.append(msgKey + " : " + msgs.get(msgKey) + ";\n");
		}
		return string.toString();
	}
	
	void checkPaid(RoomReservationRec book) throws Exception{
		Long orderNo = book.getOrderNo();
		if(orderNo != null){//check this order status
			CustomerOrderHd hd = customerOrderHdDao.getOrderById(orderNo);
			CustomerOrderTrans txn = hd.getCustomerOrderTrans().get(0);//This order only has one txn
			if(StringUtils.equalsIgnoreCase(txn.getStatus(), CustomerTransationStatus.SUC.name())){//if this order has been paid, throw exception
				throw new Exception("Order has been paid");
			}
		}
	}
	
	CallBackExecutor oasisPayment(final HotelReservationPaymentDto paymentDto){
		final CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceFacade.class);
		try{
			executor.execute(new AbstractCallBack(){

				@Override
				public Object doTry() throws Exception {
					Map<Object, RoomReservationRec> cache = new HashMap<Object, RoomReservationRec>();
					Map<Object, Map<String, String>> errorResult = new HashMap<Object, Map<String, String>>();
					Map<Object, Map<String, String>> successResult = new HashMap<Object, Map<String, String>>();
					
					executor.getContextValues().put("cache", cache);
					executor.getContextValues().put("errorResult", errorResult);
					executor.getContextValues().put("successResult", successResult);
					
					Date now = new Date();
					
					for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
						RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", itemInfo.getReservationId());
						
						checkPaid(book);
						
						if(now.getTime() - book.getRequestDate().getTime() > 15*60*1000 ){
							throw new Exception("Order has expired");//If not pay within 15minutes , can't pay any more
						}
						
						cache.put(book.getConfirmId(), book);
						
						if(book.getStatus() .equals(RoomReservationStatus.RSV.name()) ){//only the reserved record needed to call oasis api
							HotelPaymentDto oasisPayment = new HotelPaymentDto();
							CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(paymentDto.getCustomerId());
							if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(),Constant.CASH_Value)){
								oasisPayment.setResID(book.getConfirmId());
								oasisPayment.setAmount(itemInfo.getItemAmount().multiply(new BigDecimal(itemInfo.getNights())));
								//oasisPayment.setAmountAfterTax(itemInfo.getAmountAfterTax());
								oasisPayment.setCertificateNumber("|"+paymentDto.getTerminal()+"|"+book.getConfirmId()+"|");//APP/WEB
								oasisPayment.setMemberNumber(ObjectUtils.toString(profile.getAcademyNo()));
								oasisPayment.setPaymentType("CashValue");
							}
							if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(),Constant.CREDIT_CARD) ||
									StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(),Constant.PRE_AUTH) ||
									StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(),Constant.CASH)){
								
								oasisPayment.setResID(book.getConfirmId());
								oasisPayment.setAmount(itemInfo.getItemAmount().multiply(new BigDecimal(itemInfo.getNights())));
								oasisPayment.setCardHolderName(profile.getAcademyNo());//membershipNo
								oasisPayment.setMaskedCardNumber("|"+paymentDto.getTerminal()+"|"+book.getConfirmId()+"|");//APP / WEB
								oasisPayment.setPaymentType("Online");
								oasisPayment.setCardCode("VI");
							}
							
							
							logger.info("begin to call pmsApiService.payment() for " + book.getConfirmId());
							MessageResult msgResult = pmsApiService.payment(oasisPayment);
							logger.info("end pmsApiService.payment() for " + book.getConfirmId());
							
							if(!msgResult.isSuccess()){
								//Oasis fail
								errorResult.put(book.getConfirmId(), msgResult.getProperties());
							}else{
								successResult.put(book.getConfirmId(), msgResult.getProperties());
							}
						}
					}
					return null;
				}
				
				@Override
				protected GTACommonException newTryException() {
					return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
				}
				
			});
		}catch(Exception e){
			executor.getContextValues().put("exception", e);
			//swallow the exception
		}
		return executor;
	}
	
	CallBackExecutor oasisPaymentCancel(final HotelReservationCancelDto cancelDto){
		final CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceFacade.class);
		try{
			executor.execute(new AbstractCallBack(){

				@Override
				public Object doTry() throws Exception {
					Map<Object, RoomReservationRec> cache = new HashMap<Object, RoomReservationRec>();
					Map<Object, Map<String, String>> errorResult = new HashMap<Object, Map<String, String>>();
					Map<Object, Map<String, String>> successResult = new HashMap<Object, Map<String, String>>();
					
					executor.getContextValues().put("cache", cache);
					executor.getContextValues().put("errorResult", errorResult);
					executor.getContextValues().put("successResult", successResult);
					
					for(HotelReservationCancelItemDto itemInfo : cancelDto.getHotelReservationCancelItems()){
						RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", itemInfo.getReservationId());

						cache.put(book.getConfirmId(), book);
						
						if(true){//don't check status now
							CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(book.getCustomerId());
							CancelReservationDto cancel = new CancelReservationDto();
							cancel.setReservationTimeSpanEnd(DateFormatUtils.format(book.getArrivalDate(), "yyyy-MM-dd"));
							cancel.setReservationTimeSpanEnd(DateFormatUtils.format(book.getDepartureDate(), "yyyy-MM-dd"));
							cancel.setResId((String)book.getConfirmId());
							cancel.setMembershipID(profile.getAcademyNo());
							
							logger.info("begin to call pmsApiService.cancelReservation() for " + book.getConfirmId());
							MessageResult msgResult = pmsApiService.cancelReservation(cancel);
							logger.info("end pmsApiService.cancelReservation() for " + book.getConfirmId());
							
							if(!msgResult.isSuccess()){
								//Oasis fail
								errorResult.put(book.getConfirmId(), msgResult.getProperties());
							}else{
								
								successResult.put(book.getConfirmId(), msgResult.getProperties());
							}
						}
					}
					return null;
				}
				
				@Override
				protected GTACommonException newTryException() {
					return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
				}
				
			});
		}catch(Exception e){
			executor.getContextValues().put("exception", e);
			//swallow the exception
		}
		return executor;
	}
	
	
	void handleSuccessfulOasisPayment(final Map<Object, Map<String, String>> successfulOasisPayment){
		Thread t = new Thread(){
			@Override
			public void run() {
				for(Object confirmId : successfulOasisPayment.keySet()){
					hotelReservationService.updateRoomReservationStatus(confirmId.toString(), RoomReservationStatus.PAY);
				}
			}
		};
		t.start();
	}
	
	@Transactional
	public CustomerOrderTrans hotelReservationOnlinePayment(final HotelReservationPaymentDto paymentDto) {
		CallBackExecutor executor = oasisPayment(paymentDto);
		
		final Map<Object, RoomReservationRec> cache = (Map<Object, RoomReservationRec>)executor.getContextValues().get("cache");
		final Map<Object, Map<String, String>> errorResult = (Map<Object, Map<String, String>>)executor.getContextValues().get("errorResult");
		final Map<Object, Map<String, String>> successResult = (Map<Object, Map<String, String>>)executor.getContextValues().get("successResult");
		final Object exceptionObject = executor.getContextValues().get("exception");
		
		return (CustomerOrderTrans)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				if(exceptionObject != null){
					throw (Exception)exceptionObject;
				}
				
				if(!errorResult.isEmpty()){//Oasis fail, cancel the successful booking
					String error = generateMsg(errorResult);
					
					logger.error(error);

					throw new Exception("Oasis system raises errors, fail to book room(s)");
				}else{//Oasis success , call bank API
					for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
						roomTypeService.saveRoomTypeItemPrice(itemInfo.getItemNo().replaceFirst(Constant.PMS_ROOMTYPE_ITEM_PREFIX, ""));
					}
					
					CustomerOrderTrans txn = hotelReservationService.hotelReservationOnlinePayment(paymentDto);
					return txn;
				}
			}
			
			@Override
			public void doCatch() throws Exception {
				handleSuccessfulOasisPayment(successResult);
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
			
		});
		
		
	}
	
	@Transactional
	public PaymentResult hotelReservationLocalPayment(final HotelReservationPaymentDto paymentDto) {
		CallBackExecutor executor = oasisPayment(paymentDto);
		
		final Map<Object, RoomReservationRec> cache = (Map<Object, RoomReservationRec>)executor.getContextValues().get("cache");
		final Map<Object, Map<String, String>> errorResult = (Map<Object, Map<String, String>>)executor.getContextValues().get("errorResult");
		final Map<Object, Map<String, String>> successResult = (Map<Object, Map<String, String>>)executor.getContextValues().get("successResult");
		final Object exceptionObject = executor.getContextValues().get("exception");
		
		return (PaymentResult)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				if(exceptionObject != null){
					throw (Exception)exceptionObject;
				}
				
				if(!errorResult.isEmpty()){
					String error = generateMsg(errorResult);
					
					logger.error(error);
					
					throw new Exception("Oasis system raises errors, fail to book room(s)");//All things will be rolled back
				}else{
					//Oasis success
					for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
						roomTypeService.saveRoomTypeItemPrice(itemInfo.getItemNo().replaceFirst(Constant.PMS_ROOMTYPE_ITEM_PREFIX, ""));
					}
					
					CustomerOrderTrans customerOrderTrans = hotelReservationService.hotelReservationCashvaluePayment(paymentDto);
					
					PaymentResult paymentResult = new PaymentResult();
					if(!StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(),Constant.PRE_AUTH)){
						paymentResult.setTransactionNo(ObjectUtils.toString(customerOrderTrans.getTransactionNo()));
						paymentResult.setOrderNo(customerOrderTrans.getCustomerOrderHd().getOrderNo());
					}
					
					paymentResult.setLeftAmount((BigDecimal)memberCashValueService.getCurrentBalance(paymentDto.getCustomerId()).getDto());
					
					
					List<RoomReservationRec> bookings = roomReservationRecDao.getByHql("from RoomReservationRec where orderNo = ?", Arrays.<Serializable>asList(customerOrderTrans.getCustomerOrderHd().getOrderNo()));
					
					if(bookings != null && bookings.size() > 0){
						List<RoomReservationInfoDto> bundles = new ArrayList<RoomReservationInfoDto>();
						
						for (RoomReservationRec book : bookings){
							book.setStatus(RoomReservationStatus.SUC.name());//1. Update booking to SUC
							
							
							RoomReservationInfoDto bundle = new RoomReservationInfoDto();
							bundle.setReservationId(book.getConfirmId());
							bundle.setArriveDate(book.getArrivalDate());
							bundle.setDepartDate(book.getDepartureDate());
							
							bundles.add(bundle);
						}
						
						//2. Golfing bay bundle
						Map<String, List<Long>> association = pmsRequestProcessorService.giftBundleForReservationsOneNightOneBooking(paymentDto.getCustomerId(), bundles);
						//3. Update room and facility booking association
						hotelReservationService.updateGuessroomAndFacilityAssociation(paymentDto.getUserId(), association, new HashMap<String, Object>());
					}
					
					return paymentResult;
				}
			}
			
			
			@Override
			public void doCatch() throws Exception {
				handleSuccessfulOasisPayment(successResult);
			}


			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
			
		});
		
		
	}
	
	private int daySpan(Date start, Date end){
		DateTime s = new DateTime(start).millisOfDay().withMaximumValue();
		
		DateTime e = new DateTime(end).millisOfDay().withMaximumValue();
		int counter = 0;
		while(true){
			if(s.compareTo(e) < 0){
				counter++;
				s = s.plusDays(1);
			}else{
				break;
			}
		}
		return counter;
	}
	
	/**
	 * RoomResDto is for oasis
	 * RoomReservationDto is for PMS system
	 * */
	@Transactional
	public Object bookHotel(final List<RoomResDto> reservations, final String loginUserId){
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		Object ret = executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				Map<Object, Map<String, String>> bookingFailures = new HashMap<Object, Map<String, String>>();
				final Map<Object, Integer> counter = new HashMap<Object, Integer>();
				
				List<RoomReservationDto> successfulReservations = new ArrayList<RoomReservationDto>();
				
				Map<Long, Object> cache = new HashMap<Long, Object>();
				for(RoomResDto reservation : reservations){
					for(int i = 0 ; i < reservation.getRoomBookingCount() ; i++){
						if(counter.get(reservation.getRoomTypeCode()) == null){
							counter.put(reservation.getRoomTypeCode(), 0);
						}
						
						Long customerId = reservation.getCustomerId();
						
						if(cache.get(customerId) == null){
							cache.put(customerId, customerProfileService.getById(customerId));
						}
						
						CustomerProfile profile = (CustomerProfile)cache.get(customerId);
						reservation.setAddress1(profile.getPostalAddress1());
						reservation.setAddress2(profile.getPostalAddress2());
						reservation.setEmail(profile.getContactEmail());
						reservation.setGender(profile.getGender());
						reservation.setGivenName(profile.getGivenName());
						reservation.setSurname(profile.getSurname());
						reservation.setAddress3("");
						reservation.setAddress4("");
						reservation.setNamePrefix(profile.getSalutation());
						reservation.setPhoneNumber(profile.getPhoneMobile());
						MessageResult bookingMsgResult = pmsApiService.hotelReservation(reservation);//oasis hotel reservation
						
						if(bookingMsgResult.isSuccess()){//oasis success
							Map<String, String> bookingInfo = bookingMsgResult.getProperties();
							RoomReservationDto pmsReservation = new RoomReservationDto();//to be saved into db and return to page
							Date now = new Date();
							
							pmsReservation.setReservationId(bookingInfo.get("ResID_Value"));
							pmsReservation.setAmountAfterTax(new BigDecimal(bookingInfo.get("AmountAfterTax")));
							pmsReservation.setRoomTypeCode(reservation.getRoomTypeCode());
							pmsReservation.setStartDate(reservation.getStartDate());
							pmsReservation.setEndDate(reservation.getEndDate());
							pmsReservation.setNoOfStayingNight(new Long(daySpan(DateUtils.parseDate(reservation.getStartDate(), new String[]{"yyyy-MM-dd"}), DateUtils.parseDate(reservation.getEndDate(), new String[]{"yyyy-MM-dd"}))));
							pmsReservation.setCreateBy(loginUserId);
							pmsReservation.setCreateDate(now);
							pmsReservation.setUpdateBy(loginUserId);
							pmsReservation.setUpdateDate(now);
							pmsReservation.setCustomerId(customerId);
							pmsReservation.setRequestDate(now);
							
							
							successfulReservations.add(pmsReservation);
						}else{//oasis fail
							bookingFailures.put(reservation, bookingMsgResult.getProperties());
						}
					}
					
				}
				
				if(bookingFailures.size() > 0){//oasis fail, rollback booking
					Map<Object, Map<String, String>> ignoreFailures = new HashMap<Object, Map<String, String>>();
					
					for(RoomReservationDto resv : successfulReservations){
						MessageResult ignoreMsgResult = pmsApiService.ignoreReservation(resv.getReservationId());
						if(!ignoreMsgResult.isSuccess()){
							ignoreFailures.put(resv.getReservationId(), ignoreMsgResult.getProperties());
						}else{
							Integer rooms = counter.get(resv.getRoomTypeCode());
							counter.put(resv.getRoomTypeCode(), rooms + 1);
						}
					}
					
					for(RoomResDto dto: reservations){
						Long roomBookingCount = dto.getRoomBookingCount();
						Integer availableBookingCount = counter.get(dto.getRoomTypeCode());
						if(roomBookingCount.equals(availableBookingCount)){
							counter.remove(dto.getRoomTypeCode());
						}
					}
					
					if(!ignoreFailures.isEmpty()){
						logger.error("can not ignore successful reservation :" + generateMsg(ignoreFailures));
					}
					
					logger.error("can not book :" + generateMsg(bookingFailures));
					
					final StringBuilder builder = new StringBuilder();
					final String template = "{roomTypeCode} is allow to select max {number} room(s)";
					
					final int size = counter.keySet().size();
					
					CollectionUtil.loop(counter.keySet(), new NoResultCallBack<Object>(){
						@Override
						public void execute(Object code, int index) {
							builder.append(template.replace("{roomTypeCode}", code.toString()).replace("{number}", counter.get(code).toString()) + ((size-1)==index?"":", "));
						}
					});
					
					throw new GTACommonException(GTAError.GuestRoomError.ROOM_NOT_ENOUGH, new Object[]{builder.toString(), counter});
				}
				
				hotelReservationService.bookHotel(successfulReservations);
				return successfulReservations;
			}
			
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
		
		
		return (List<RoomReservationDto>)ret;
	}
	
	
	private class $ParserContext implements ParserContext{

		@Override
		public boolean isTemplate() {
			return true;
		}

		@Override
		public String getExpressionPrefix() {
			return "{";
		}

		@Override
		public String getExpressionSuffix() {
			return "}";
		}
		
	}
	
	private class TemplateObject implements Serializable{
		private static final long serialVersionUID = 1L;

		private String confirmationId;
		private String memberName;
		private String reservationStart;
		private String reservationEnd;
		private String roomTypeName;
		private int nights;
		private String bundledPeriod;
		public String getConfirmationId() {
			return confirmationId;
		}
		public void setConfirmationId(String confirmationId) {
			this.confirmationId = confirmationId;
		}
		public String getMemberName() {
			return memberName;
		}
		public void setMemberName(String memberName) {
			this.memberName = memberName;
		}
		public String getReservationStart() {
			return reservationStart;
		}
		public void setReservationStart(String reservationStart) {
			this.reservationStart = reservationStart;
		}
		public String getReservationEnd() {
			return reservationEnd;
		}
		public void setReservationEnd(String reservationEnd) {
			this.reservationEnd = reservationEnd;
		}
		public String getRoomTypeName() {
			return roomTypeName;
		}
		public void setRoomTypeName(String roomTypeName) {
			this.roomTypeName = roomTypeName;
		}
		public int getNights() {
			return nights;
		}
		public void setNights(int nights) {
			this.nights = nights;
		}
		public String getBundledPeriod() {
			return bundledPeriod;
		}
		public void setBundledPeriod(String bundledPeriod) {
			this.bundledPeriod = bundledPeriod;
		}
		
	}
	
	String getBundledFacilityPeriod(RoomReservationRec reservation){
		//get all bundles
		List<RoomFacilityTypeBooking> bundles = roomFacilityTypeBookingDao.getByHql("from RoomFacilityTypeBooking where roomBookingId = ?", Arrays.<Serializable>asList(new Long(reservation.getResvId())) );
		
		List<String> dateStrings = new ArrayList<String>();
		
		for(RoomFacilityTypeBooking bundle : bundles){
			MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(bundle.getFacilityTypeBookingId());//get facility booking
			List<FacilityTimeslot> timeslots = booking.getFacilityTimeslots();
			
			if(CollectionUtils.isNotEmpty(timeslots)){
				FacilityTimeslot slot = timeslots.get(0);//This facility booking only has one timeslot
				dateStrings.add(DateFormatUtils.format(slot.getBeginDatetime(), "yyyy-MM-dd HH:mm") + " - " + DateFormatUtils.format(slot.getEndDatetime(), "HH:mm"));
			}
		}
		
		return StringUtils.join(dateStrings, ", ");
		
	}
	
	@Transactional
	public void sendGuestroomReservationEmail(final Long transactionNo, final Map<String, Object> params) {
		
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				String functionId = "guest_room_book_confirm";
				CustomerOrderTrans txn = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
				CustomerOrderHd order = txn.getCustomerOrderHd();
				
				CustomerProfile customer = customerProfileService.getById(order.getCustomerId());
				
				MessageTemplate template = messageTemplateService.getTemplateByFuncId(functionId);

				TransactionEmailDto dto = new TransactionEmailDto();
				dto.setSendTo(customer.getContactEmail());
				dto.setEmailType(EmailType.GUESTROOMBOOKRECEIPT.name());
				dto.setTransactionNO(transactionNo);
				
				TemplateObject templateObject = new TemplateObject();
				String expressionTemplate = new StringBuilder()
						.append("<b>Confirmation ID:&nbsp;</b>{confirmationId}<br/>\n")
						.append("<b>Reserved Person:&nbsp;</b>{memberName}<br/>\n")
						.append("<b>Reservation Period:&nbsp;</b>{reservationStart} to {reservationEnd}<br/>\n")
						.append("<b>Guest Room Type:&nbsp;</b>{roomTypeName}<br/>\n")
						.append("<b>No. of Staying Night:&nbsp;</b>{nights} Night(s)<br/>\n")
						.append("<b>Reservation Time of Bundled Bay:&nbsp;</b>{bundledPeriod}<br/>\n").toString();
				
				
				ExpressionParser parser = new SpelExpressionParser();
				Expression exp = parser.parseExpression(expressionTemplate, new $ParserContext());
				//StandardEvaluationContext context = new StandardEvaluationContext(templateObject);
				
				List<RoomReservationRec> books = roomReservationRecDao.getByHql("from RoomReservationRec where orderNo = ?", Arrays.<Serializable>asList(order.getOrderNo()) );
				
				StringBuilder reservationItems = new StringBuilder();
				for (RoomReservationRec book : books){
					templateObject.setConfirmationId("GRM-"+book.getConfirmId());
					templateObject.setMemberName(customer.getSalutation()+" "+customer.getGivenName()+" "+customer.getSurname());
					templateObject.setNights(book.getNight()==null ? 0:book.getNight().intValue());
					templateObject.setReservationEnd(DateFormatUtils.format(book.getDepartureDate(), "yyyy-MM-dd"));
					templateObject.setReservationStart(DateFormatUtils.format(book.getArrivalDate(), "yyyy-MM-dd"));
					templateObject.setRoomTypeName(book.getRoomTypeCode());
					templateObject.setBundledPeriod(getBundledFacilityPeriod(book));
					
					reservationItems.append(exp.getValue(templateObject, String.class))
					.append("\n")
					.append("<br/>");
				}
				
				String content = template.getContentHtml().replace("{salutation}", customer.getSalutation())
						.replace("{lastname}", customer.getGivenName())
						.replace("{reservationItems}", reservationItems.toString());
				
				
				dto.setEmailContent(content);
				dto.setSubject(template.getMessageSubject());

				LoginUserDto userDto = new LoginUserDto();
				userDto.setUserId(params.get("userId").toString());
				userDto.setUserName(params.get("userName").toString());
				
				customerOrderTransService.sentTransactionEmail(dto,userDto);
				logger.debug("Complete sending guest room reservation email to " + customer.getContactEmail());
				//if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(responseResult.getReturnCode())) {
					//return true;
				//}else {
					//return false;
				//}
				return null;
			}
		});
	}
	
	@Transactional
	public void precancelReservation(final HotelReservationPaymentDto paymentDto){
		boolean hasError = false;
		for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
			try{
				MessageResult msgRes = pmsApiService.ignoreReservation(itemInfo.getReservationId());
				logger.info("pmsApiService.ignoreReservation() : " + itemInfo.getReservationId());
				
				if(msgRes.isSuccess()){
					hotelReservationService.updateRoomReservationStatus(itemInfo.getReservationId(), RoomReservationStatus.CAN);
					logger.info("hotelReservationService.updateRoomReservationStatus() : " + itemInfo.getReservationId());
				}else{
					hasError = true;//don't distinct internal or oasis error
				}
				
			}catch(Exception e){
				hasError = true;//don't distinct internal or oasis error
				logger.error("cancelReservation("+itemInfo.getReservationId()+") error", e);
			}
		}
		
		if(hasError){
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{"System raises errors, some of reservations can't be cancelled"});
		}
	}

	
	void handleSuccessfulOasisPaymentCancel(final Map<Object, Map<String, String>> successfulOasisPayment, final HotelReservationCancelDto cancelDto){
		for(Object confirmId : successfulOasisPayment.keySet()){
			hotelReservationService.updateRoomReservationStatus(confirmId.toString(), RoomReservationStatus.RFU);
			if(cancelDto.isCreateRefundRequest()){
				hotelReservationService.requestCancelPayment(confirmId.toString(), cancelDto);
			}
			hotelReservationService.cancelBundledFacilityBookings(confirmId.toString(), cancelDto);
		}
	}
	
	@Transactional
	public void cancelReservation(final HotelReservationCancelDto cancelDto){
		
		GlobalParameter globalParam = globalParameterService.getGlobalParameter("REFPERIOD_GUESTROOM");
		int refundPeriod = globalParam.getParamValue() != null ? Integer.parseInt(globalParam.getParamValue()) : 0;
		DateTime now = new DateTime(cancelDto.getCurrentDate()).millisOfDay().withMinimumValue();
		for(HotelReservationCancelItemDto cancelItem : cancelDto.getHotelReservationCancelItems()){
			RoomReservationRec cancelReservationRec = roomReservationRecDao.getReservationByConfirmId(cancelItem.getReservationId());
			DateTime arrivalDate = new DateTime(cancelReservationRec.getArrivalDate()).millisOfDay().withMinimumValue();
			
			if((arrivalDate.getMillis() - 60*60*1000*24*refundPeriod) < now.getMillis()){
				throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{"Refund is not allowed due to refund period limitation"});
			}
		}
		
		CallBackExecutor executor = oasisPaymentCancel(cancelDto);
		
		final Map<Object, RoomReservationRec> cache = (Map<Object, RoomReservationRec>)executor.getContextValues().get("cache");
		final Map<Object, Map<String, String>> errorResult = (Map<Object, Map<String, String>>)executor.getContextValues().get("errorResult");
		final Map<Object, Map<String, String>> successResult = (Map<Object, Map<String, String>>)executor.getContextValues().get("successResult");
		final Object exceptionObject = executor.getContextValues().get("exception");
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				if(exceptionObject != null){
					throw (Exception)exceptionObject;
				}
				
				if(!errorResult.isEmpty()){
					String error = generateMsg(errorResult);
					
					logger.error(error);
					
					throw new Exception("Oasis system raises errors, fail to cancel reservation(s)");//All things will be rolled back
				}else{
					handleSuccessfulOasisPaymentCancel(successResult, cancelDto);
				}
				
				return null;
			}
			@Override
			public void doCatch() throws Exception {
				Thread t = new Thread(){
					@Override
					public void run() {
						handleSuccessfulOasisPaymentCancel(successResult, cancelDto);
					}
				};
				t.start();
				
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
			
		});
	}

	void checkRequired(HotelReservationChangeDto changeDto){
		String errorMsg = "Only allow changing one guessroom booking record per time";
		List<HotelReservationCancelItemDto> cancelItems = changeDto.getReservationCancel().getHotelReservationCancelItems();
		Assert.isTrue(cancelItems.size()==1, errorMsg);
		
		HotelReservationCancelItemDto cancelItem = cancelItems.get(0);
		RoomReservationRec cancelReservationRec = roomReservationRecDao.getReservationByConfirmId(cancelItem.getReservationId());
		
		if(!cancelReservationRec.getCustomerId().equals(changeDto.getReservation().getCustomerId())){
			//Must specify the same customer for creating a maintenance offer
			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{"Must specify the same customer for changing guess room reservation"});
		}
	}
	
	@Transactional
	public void changeReservation(final HotelReservationChangeDto changeDto){
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceFacade.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				checkRequired(changeDto);
				//1. Book Hotel
				List<RoomReservationDto> successfulReservations = (List<RoomReservationDto>)bookHotel(Arrays.asList(changeDto.getReservation()), changeDto.getUserId());
				
				//2. Init pre-required object
				RoomReservationDto changedBooking = successfulReservations.get(0);
				HotelReservationCancelItemDto originBooking = changeDto.getReservationCancel().getHotelReservationCancelItems().get(0);
				RoomReservationRec changedReservationRec = roomReservationRecDao.getReservationByConfirmId(changedBooking.getReservationId());
				RoomReservationRec originReservationRec = roomReservationRecDao.getReservationByConfirmId(originBooking.getReservationId());
				
				Map<String, Object> txnInfo = hotelReservationService.exposeTxnInfo(originReservationRec.getConfirmId());
				
				CustomerOrderDet transactionItemDetail = (CustomerOrderDet) txnInfo.get("transactionItemDetail");
				CustomerOrderTrans transactionDetail = (CustomerOrderTrans) txnInfo.get("transactionDetail");
				
				Assert.notNull(transactionItemDetail);
				Assert.notNull(transactionDetail);
				
				//3. Link the txn record to new record
				//Move the orderNo to new reservation
				changedReservationRec.setOrderNo(originReservationRec.getOrderNo());
				changedReservationRec.setStatus(RoomReservationStatus.SUC.name());
				
				originReservationRec.setOrderNo(null);
				originReservationRec.setStatus(RoomReservationStatus.CAN.name());
				
				//Link to new confirmId
				transactionItemDetail.setExtRefNo(changedReservationRec.getConfirmId());
				
				//Update the item info
				roomTypeService.saveRoomTypeItemPrice(changedBooking.getRoomTypeCode());
				transactionItemDetail.setItemNo(Constant.PMS_ROOMTYPE_ITEM_PREFIX + changedBooking.getRoomTypeCode());
				transactionItemDetail.setOrderQty(changedBooking.getNoOfStayingNight());
				transactionItemDetail.setItemTotalAmout(changedBooking.getAmountAfterTax().multiply(new BigDecimal(changedBooking.getNoOfStayingNight())));
				
				//4. Cancel bundled golfing bay gift and bind the new one 
				hotelReservationService.cancelBundledFacilityBookings(originReservationRec.getConfirmId(), changeDto.getReservationCancel());
				List<RoomReservationInfoDto> bundles = new ArrayList<RoomReservationInfoDto>();
				
				RoomReservationInfoDto bundle = new RoomReservationInfoDto();
				bundle.setReservationId(changedReservationRec.getConfirmId());
				bundle.setArriveDate(changedReservationRec.getArrivalDate());
				bundle.setDepartDate(changedReservationRec.getDepartureDate());
				bundles.add(bundle);
				
				//Golfing bay bundle
				Map<String, List<Long>> association = pmsRequestProcessorService.giftBundleForReservationsOneNightOneBooking(changeDto.getReservation().getCustomerId(), bundles);
				//Update room and facility booking association
				hotelReservationService.updateGuessroomAndFacilityAssociation(changeDto.getUserId(), association, new HashMap<String, Object>());
				
				
				//5. Oasis payment
				HotelReservationPaymentDto paymentDto = new HotelReservationPaymentDto();
				paymentDto.setUserId(changeDto.getUserId());
				paymentDto.setCurrentDate(changeDto.getCurrentDate());
				paymentDto.setCustomerId(changeDto.getReservation().getCustomerId());
				paymentDto.setPaymentMethod(transactionDetail.getPaymentMethodCode());
				paymentDto.setHotelReservationItemsInfo(new ArrayList<HotelReservationItemInfoDto>());
				
				HotelReservationItemInfoDto reservationItem = new HotelReservationItemInfoDto();
				reservationItem.setAmountAfterTax(changedBooking.getAmountAfterTax());
				reservationItem.setItemAmount(reservationItem.getAmountAfterTax());
				reservationItem.setItemNo(Constant.PMS_ROOMTYPE_ITEM_PREFIX + changedBooking.getRoomTypeCode());
				reservationItem.setNights(changedBooking.getNoOfStayingNight());
				reservationItem.setReservationId(changedBooking.getReservationId());
				paymentDto.getHotelReservationItemsInfo().add(reservationItem);
				
				oasisPayment(paymentDto);
				
				//6. Cancel the previous booking
				oasisPaymentCancel(changeDto.getReservationCancel());
				return null;
			}
		});
	}
	
	@Transactional
	public void processCallbackAfterPayment(CustomerOrderTrans txn, Map<String, Object> params) {
		Assert.notNull(params.get("userId"), "missing param: userId");
		Assert.notNull(params.get("userName"), "missing param: userName");
		Assert.notNull(params.get("paymentMethod"), "missing param: paymentMethod");
		CustomerOrderHd customerOrderHd = txn.getCustomerOrderHd();
		
		//Callback from bank, bank successfully pay, do three things
		List<RoomReservationRec> bookings = roomReservationRecDao.getByHql("from RoomReservationRec where orderNo = ?", Arrays.<Serializable>asList(customerOrderHd.getOrderNo()) );
		
		//1. Update booking status to SUC
		for (RoomReservationRec booking : bookings){
			booking.setStatus(RoomReservationStatus.SUC.name());
		}
		
		//2. Golfing bay bundle
		List<RoomReservationInfoDto> bundles = new ArrayList<RoomReservationInfoDto>();
		
		for (RoomReservationRec book : bookings){
			RoomReservationInfoDto bundle = new RoomReservationInfoDto();
			bundle.setReservationId(book.getConfirmId());
			bundle.setArriveDate(book.getArrivalDate());
			bundle.setDepartDate(book.getDepartureDate());
			
			bundles.add(bundle);
		}
		Map<String, List<Long>> association = pmsRequestProcessorService.giftBundleForReservationsOneNightOneBooking(params.get("paymentMethod").toString(), customerOrderHd.getCustomerId(), bundles);
		
		//3. Update room and facility booking association
		hotelReservationService.updateGuessroomAndFacilityAssociation(params.get("userId").toString(), association, new HashMap<String, Object>());
		//4. Send Email
		sendGuestroomReservationEmail(txn.getTransactionNo(), params);
	}
	
	@Transactional
	public void processCallbackAfterCreditCardLocalPayment(PosResponse posResponse, Map<String, Object> params) {
		logger.info("Got callback !!");
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug(gson.toJson(posResponse));
		if (posResponse.getReferenceNo() != null) {
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans != null) {
				if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
					
					//1. Set payment method
					String paymentMethod = PaymentMethod.VISA.name();
					if ("MC".equalsIgnoreCase(posResponse.getCardType())) {
						paymentMethod = PaymentMethod.MASTER.name();
					}else if ("VC".equalsIgnoreCase(posResponse.getCardType())) {
						paymentMethod = PaymentMethod.VISA.name();
					}
					
					
					//2. Update transaction info
					customerOrderTrans.setPaymentMethodCode(paymentMethod);
					customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
					customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
					customerOrderTrans.setUpdateBy("System");
					customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
					customerOrderTrans.setStatus(CustomerTransationStatus.SUC.name());
					customerOrderTransDao.saveOrUpdate(customerOrderTrans);
					
					CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
					
					//3. Update order info
					List<CustomerOrderTrans> transactions = customerOrderHd.getCustomerOrderTrans();
					BigDecimal totalAmount = BigDecimal.ZERO;
					for (CustomerOrderTrans txn: transactions) {
						if (CustomerTransationStatus.SUC.getDesc().equals(txn.getStatus())) {
							totalAmount = totalAmount.add(txn.getPaidAmount());
						}
					}
					if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount)==0) {
						customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
						customerOrderHd.setUpdateDate(new Date());
						customerOrderHdDao.update(customerOrderHd);
					}
					
					params.put("paymentMethod", paymentMethod);
					processCallbackAfterPayment(customerOrderTrans, params);
				}
				else
				{
					if (customerOrderTrans.getStatus().equalsIgnoreCase("PND"))
					{
						customerOrderTrans.setStatus("FAIL");
						customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
						customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
						customerOrderTrans.setInternalRemark(posResponse.getResponseText());
						customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
					}
				}
			}
		}
		
	}
}
