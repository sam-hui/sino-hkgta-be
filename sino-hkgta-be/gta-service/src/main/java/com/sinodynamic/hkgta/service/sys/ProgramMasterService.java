package com.sinodynamic.hkgta.service.sys;

import java.math.BigInteger;
import java.util.List;

import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface ProgramMasterService extends IServiceBase<ProgramMaster> {

	public List<ProgramMaster> getAvailablePrograms(BigInteger roleId);

}
