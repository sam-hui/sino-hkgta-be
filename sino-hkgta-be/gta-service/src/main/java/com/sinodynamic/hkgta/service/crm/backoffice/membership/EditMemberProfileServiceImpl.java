package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAddressDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;
import com.sinodynamic.hkgta.entity.crm.CustomerAddressPK;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.notification.SMSService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileServiceImpl;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
@SuppressWarnings("deprecation")
public class EditMemberProfileServiceImpl extends ServiceBase<CustomerProfile> implements EditMemberProfileService{
	
	private Logger logger = Logger.getLogger(CustomerProfileServiceImpl.class);
    
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	@Autowired
	private CustomerAddressDao customerAddressDao;
	
	@Autowired 
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private SMSService smsService;
	
	@Autowired 
	private SpaMemberSyncDao spaMemberSyncDao;

	
	private ResponseMsg checkInputData(CustomerProfile dto) {
		List<CustomerAdditionInfo> ciList = dto.getCustomerAdditionInfos();
		if (ciList != null && ciList.size() > 0) {
			for (CustomerAdditionInfo info : ciList) {
				if (!CommUtil.notEmpty(info.getCaptionId() + "")) {
					//return new ResponseMsg("1", "Caption Id is required!");
					responseMsg.initResult(GTAError.EnrollError.CAPTION_ID_EMPTY);
					return responseMsg;
				}
				if (!CommUtil.notEmpty(info.getSysId() + "")) {
					//return new ResponseMsg("1", "Sys Id is required!");
					responseMsg.initResult(GTAError.EnrollError.SYS_ID_EMPTY);
					return responseMsg;
				}
			}
		}

		List<CustomerAddress> caList = dto.getCustomerAddresses();
		if ("false".equalsIgnoreCase(dto.getCheckBillingAddress())) {
			if (caList != null && caList.size() > 0) {
				for (CustomerAddress ca : caList) {
					if (!CommUtil.notEmpty(ca.getAddress1())) {
						//return new ResponseMsg("1", "Bill Address is required!");
						responseMsg.initResult(GTAError.EnrollError.BILL_ADDRESS_EMPTY);
						return responseMsg;
					}
					if (!CommUtil.notEmpty(ca.getHkDistrict())) {
						//return new ResponseMsg("1", "Bill District is required!");
						responseMsg.initResult(GTAError.EnrollError.BILL_DISTRICT_EMPTY);
						return responseMsg;
					}
					if (!CommUtil.notEmpty(ca.getAddressType())) {
						//return new ResponseMsg("1", "Bill Address Type is required!");
						responseMsg.initResult(GTAError.EnrollError.BILL_ADDRESS_TYPE_EMPTY);
						return responseMsg;
					}
				}
			}

		}
		
		StringBuilder ms = new StringBuilder();
		if (!CommUtil.notEmpty(dto.getPhoneMobile())) {
			ms.append("Phone Mobile, ");
		}
		if (!CommUtil.notEmpty(dto.getContactEmail())) {
			ms.append("Contact Email, ");
		}
		if (!CommUtil.notEmpty(dto.getPostalDistrict())) {
			ms.append("Postal District, ");
		}
		if (!CommUtil.notEmpty(dto.getPassportType())) {
			ms.append("PassportType, ");
		}
		if (!CommUtil.notEmpty(dto.getPassportNo())) {
			ms.append("PassportNo, ");
		}

		if (ms.length() > 0) {
			responseMsg.initResult(GTAError.EnrollError.GENERAL_REQ,new String[]{ms.toString().substring(0, ms.length() - 2)});
			return responseMsg;
		}

		if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
			responseMsg.initResult(GTAError.EnrollError.MOBILE_PHONE_INVALID);
			return responseMsg;
		}
		if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
			responseMsg.initResult(GTAError.EnrollError.BUSINESS_PHONE_INVALID);
			return responseMsg;
		}
		if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
			responseMsg.initResult(GTAError.EnrollError.HOME_PHONE_INVALID);
			return responseMsg;
		}
		
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	}
	
	@Transactional
	public ResponseMsg editMemberProfile(CustomerProfile dto,String userId){
		Long customerId = dto.getCustomerId();
		if(customerId==null){
			//return new ResponseMsg("1","Customer Id is required for edit customer profile!");
			responseMsg.initResult(GTAError.EnrollError.CUSTOMER_ID_EMPTY);
			return responseMsg;
		}
		
		ResponseMsg res = checkInputData(dto);
		if(res.getReturnCode() != "0") return res;
		CustomerProfile targetProfile = customerProfileDao.get(CustomerProfile.class, customerId);
		boolean checkForID = customerProfileDao.checkAvailablePassportNo(customerId, dto.getPassportType(),dto.getPassportNo());
		if (!checkForID) {
			throw new GTACommonException(GTAError.EnrollError.ID_EXIST_IN_REG,new Object[]{dto.getPassportType(),dto.getPassportNo()});
		}
		
		dto.setUpdateDate(new Date());
		dto.setUpdateBy(userId);
			
		Member member = memberDao.get(Member.class, customerId);
		if (null != member.getAcademyNo()) {
			spaMemberSyncDao.addSpaMemberSyncWhenUpdate(targetProfile, dto);
		}
		List<CustomerAddress> customerAddresses = customerAddressDao.getByCol(CustomerAddress.class, "id.customerId", customerId, null);
		//Persistent instance to detached instance for updating 
		customerProfileDao.getCurrentSession().evict(targetProfile);
		targetProfile.setCustomerAddresses(customerAddresses);
		boolean isUpdate = true;
		if (dto.equals(targetProfile)) {
			isUpdate = false;
		}
		String[] ignoreFileds = new String[]{"portraitPhoto","signature","id","contactClassCode","isDeleted","companyName","createBy","createDate"};
		BeanUtils.copyProperties(dto, targetProfile,ignoreFileds);
		//Add version control
		targetProfile.setVersion(dto.getVersion());
		targetProfile.setDateOfBirth(dto.getOriDateOfBirth());
		customerProfileDao.update(targetProfile);
		if(dto.getCustomerAdditionInfos()!=null && dto.getCustomerAdditionInfos().size()>0){
			for(CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()){
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.valueOf(cust.getCaptionId()));
				cpk.setCustomerId(customerId);
				cust.setId(cpk);
				cust.setCreateBy(userId);
				cust.setUpdateDate(new Date());
				CustomerAdditionInfo targetEntity = customerAdditionInfoDao.get(CustomerAdditionInfo.class, cpk);
				
				if(targetEntity != null){
					cust.setUpdateBy(userId);
					cust.setUpdateDate(new Date());
					//Persistent instance to detached instance for updating 
					customerAdditionInfoDao.getCurrentSession().evict(targetEntity);;
					String[] ignoreFileds1 = new String[]{"id","createBy","createDate"};
					BeanUtils.copyProperties(cust, targetEntity,ignoreFileds1);
					//Add version control
					targetEntity.setVersion(cust.getVersion());
					customerAdditionInfoDao.update(targetEntity);
				}else{
					customerAdditionInfoDao.save(cust);
				}
			}
		}
		
		if(dto.getCustomerAddresses()!=null && dto.getCustomerAddresses().size() >0){
			for(CustomerAddress address: dto.getCustomerAddresses()){
				CustomerAddressPK apk = new CustomerAddressPK();
				apk.setAddressType(address.getAddressType());
				apk.setCustomerId(customerId);
				address.setId(apk);
				if("true".equalsIgnoreCase(dto.getCheckBillingAddress())){
					address.setAddress1(dto.getPostalAddress1());
					address.setAddress2(dto.getPostalAddress2());
					address.setHkDistrict(dto.getPostalDistrict());
				}
				CustomerAddress targetEntity = customerAddressDao.get(CustomerAddress.class, apk);
				if(targetEntity != null){
					String[] ignoreFileds2 = new String[]{"id"};
					BeanUtils.copyProperties(address,targetEntity,ignoreFileds2);
					customerAddressDao.update(targetEntity);
				}else{
					customerAddressDao.save(address);
				}
			}
		}
		
		member.setRelationshipCode(dto.getRelationshipCode());
		memberDao.update(member);
		if (isUpdate && Constant.Status.ACT.name().equalsIgnoreCase(member.getStatus())) {
			try {
				MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("update_profile");
				String content = mt.getContent();
				if (null == content) {
					content = Constant.UPDATE_CUSTOMER_PROFILE_SMS_TEMPLETE;
				}
				smsService.sendSMS(Arrays.asList(dto.getPhoneMobile()), content , new Date());
			} catch (Exception e) {
				logger.debug("Send SMS failed!", e);
			}
		}
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	} 


	

	}

