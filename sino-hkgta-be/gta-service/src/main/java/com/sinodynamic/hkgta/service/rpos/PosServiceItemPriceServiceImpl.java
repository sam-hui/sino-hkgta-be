package com.sinodynamic.hkgta.service.rpos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class PosServiceItemPriceServiceImpl extends
		ServiceBase<PosServiceItemPrice> implements PosServiceItemPriceService {

	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public PosServiceItemPrice getByItemNo(String itemNo)
	{
		return posServiceItemPriceDao.getByItemNo(itemNo);
	}

}
