package com.sinodynamic.hkgta.service.fms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityTypeQuotaDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTypeQuota;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityTypeQuotaService extends IServiceBase<FacilityTypeQuota> {

	public List<FacilityTypeQuotaDto> getFacilityTypeQuotaList(String facilityTypeCode, String sortBy, String isAscending);

	public FacilityTypeQuotaDto saveFacilityTypeQuota(FacilityTypeQuotaDto quotaDto, String userId);

	public FacilityTypeQuotaDto saveFacilityTypeDefaultQuota(FacilityTypeQuotaDto quotaDto);

	public FacilityTypeQuota getFacilityTypeQuota(String facilityType, Date specificDate, String subtypeId);

	public FacilityTypeQuotaDto getFacilityTypeQuotaById(long sysId);

	public FacilityTypeQuotaDto updateFacilityTypeQuota(FacilityTypeQuotaDto quotaDto, String userId);

	public void deleteFacilityTypeQuota(long sysId, String userId);

	public Long getFacilityQuota(String facilityType, Date specificDate);

}
