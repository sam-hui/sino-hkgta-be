package com.sinodynamic.hkgta.service.crm.sales;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerPreEnrollStageDao;
import com.sinodynamic.hkgta.dao.crm.HomePageSummaryDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerTransactionDao;
import com.sinodynamic.hkgta.dto.crm.CustomerActionContentDto;
import com.sinodynamic.hkgta.dto.crm.HomePageSummaryDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.EnrollmentStatus;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Service
public class HomePageSummaryServiceImpl extends ServiceBase<String> implements
		HomePageSummaryService {

	public Logger logger = Logger.getLogger(ServiceBase.class);

	@Autowired
	private HomePageSummaryDao homePageSummaryDao;

	
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerPreEnrollStageDao  customerPreEnrollStageDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerTransactionDao customerTransactionDao;

	@Transactional
	public HomePageSummaryDto countNumber(String userId) throws Exception {

		Integer customerCount = homePageSummaryDao.countCustomerProfile();

		Integer enrollmentCount = homePageSummaryDao
				.countEnrollmentsWithActiveMembers(userId);

		Integer presentationCount = homePageSummaryDao.countPresentation();

		Integer renewalCount = homePageSummaryDao.countRenewal();

		Integer pendingPaymentCount = customerTransactionDao.getCustomerTransactionList(new ListPage<CustomerOrderHd>(), Constant.TRANSACTION_BALANCEDUE_PENDING, "", "effectiveDate", false, null, userId, null,null,"IPM").getAllSize();

		Boolean enrollRemark = homePageSummaryDao.countEnrollmentRemark(userId);
		
		HomePageSummaryDto dto = new HomePageSummaryDto();
		dto.setProspectsNumber(customerCount);
		dto.setActiveMemberNumber(enrollmentCount);
		dto.setPresentationNumber(presentationCount);
		dto.setPaymentPendingNumber(pendingPaymentCount);
		dto.setWillExpireNumber(renewalCount);
		
		dto.setIsShowRemarkIconEnrollment(enrollRemark);
		return dto;
	}

	/**
	 * @author Li_Chen 
	 * Date:2015-5-4
	 * Description:  
	 *  1. get the default 20 latest activaties for customer_enrollment,include four status ('NEW','ANC','CMP','OPN'), order by update_date desc
	 *  2. get the default 20 latest activaties for customer_pre_enroll_stage order by update_date desc
	 *  3. get the default 20 latest activaties for customer_order_trans,include two status ('SUC','NSC') , order by update_date desc
	 *  4. put the all activaties into arrayList
	 *  5. sort the all activatiesList
	 *  6. get 20 latest activaties from the all activatiesList
	 * Revision history: 
	 * 2015-5-4  Li_Chen  The initial implementation 
	 */
	@Override
	@Transactional
	public List<CustomerActionContentDto> getLatestActionContent(int totalSize)
			throws Exception {
		
		//return the param
		List<CustomerActionContentDto> actionContentDtoList = new ArrayList<CustomerActionContentDto>();
		//get default 20 latest customerEnrollmentActivaties 
		List enrollmentList = customerEnrollmentDao.getLatestEnrollmentActivaties(totalSize);
		//get default 20 latest customerCommentsActivaties
		List commentsList = customerPreEnrollStageDao.getLatestCommentsActivities(totalSize);
		//get default 20 latest customerCommentsActivaties
		List transationList = customerOrderTransDao.getLatestTransationActivaties(totalSize);
		
		if (null != enrollmentList && enrollmentList.size()>0) {
			for (int i=0;i<enrollmentList.size();i++){
				Object[] objArray = (Object[])enrollmentList.get(i);
				if ( null == (Date)objArray[0]) 
					continue;
				CustomerActionContentDto actionDto = new CustomerActionContentDto();
				actionDto.setDateTime((Date)objArray[0]);
				actionDto.setDatetimeStringEN(DateConvertUtil.getObliqueYMDDateAndDateDiff((Date)objArray[0]));
				actionDto.setNewsContentEN(getEnrollmentTemplate((String)objArray[1],(String)objArray[2]));
				actionContentDtoList.add(actionDto);
			}
		}
		
		if (null != commentsList && commentsList.size()>0) {
			for (int i=0;i<commentsList.size();i++){ 
				Object[] objArray = (Object[])commentsList.get(i);
				if ( null == (Date)objArray[0]) 
					continue;
				CustomerActionContentDto actionDto = new CustomerActionContentDto();
				actionDto.setDateTime((Date)objArray[0]);
				actionDto.setDatetimeStringEN(DateConvertUtil.getObliqueYMDDateAndDateDiff((Date)objArray[0]));
				actionDto.setNewsContentEN(getCommentsTemplate((String)objArray[1],(String)objArray[2]));
				actionContentDtoList.add(actionDto);
			}
		}
		if (null != transationList && transationList.size()>0) {
			for (int i=0;i<transationList.size();i++){ 
				Object[] objArray = (Object[])transationList.get(i);
				if ( null == (Date)objArray[0]) 
					continue;
				CustomerActionContentDto actionDto = new CustomerActionContentDto();
				actionDto.setDateTime((Date)objArray[0]);
				actionDto.setDatetimeStringEN(DateConvertUtil.getObliqueYMDDateAndDateDiff((Date)objArray[0]));
				actionDto.setNewsContentEN(getTransationTemplate((String)objArray[1],(String)objArray[2]));
				actionContentDtoList.add(actionDto);
			}
		}
		//sort the all arrayList
		Collections.sort(actionContentDtoList, new Comparator<CustomerActionContentDto>() {
		    public int compare(CustomerActionContentDto o1, CustomerActionContentDto o2) {
		        return o2.getDateTime().compareTo(o1.getDateTime());
		    }
		});
		
		return totalSize>actionContentDtoList.size()?actionContentDtoList:actionContentDtoList.subList(0, totalSize);
	}
	/**
	 * @author Li_Chen
	 * Date :2015-5-8
	 * description:
	 * get the template by the customer enrollment's status
	 * 
	 * @param statue enrollment status
	 * @param userName customer enrollment name
	 * @return home page's template
	 */
	private String getEnrollmentTemplate (String statue,String customerName){
		String template = "";
		if (EnrollmentStatus.NEW.getDesc().equals(statue)) 
			template = "New Enrollment record for "+customerName+" is created";
		else if (EnrollmentStatus.APV.getDesc().equals(statue))
			template = "The membership of "+customerName+" is approved";
		else if (EnrollmentStatus.ANC.getDesc().equals(statue))
			template = "The membership of "+customerName+" is activated";
		else if (EnrollmentStatus.CMP.getDesc().equals(statue)) 
			template = "The membership card for "+customerName+" is issued";
		else if (EnrollmentStatus.OPN.getDesc().equals(statue)) 
			template = "New Lead record for "+customerName+" is created";
		else if (EnrollStatus.PYA.name().equals(statue)) 
			template = "The payment of "+customerName+" has been approved";
		else if (EnrollStatus.TOA.name().equals(statue)) 
			template = customerName+" is waiting for activation";
		return template;
	}
	
	/**
	 * @author Li_Chen
	 * Date :2015-5-8
	 * description:
	 * get the comments's template
	 * @param salemenNickName salesmenName
	 * @param customerName customerName
	 * @return comments's template
	 */
	private String getCommentsTemplate (String salemenNickName,String customerName) {
		String template = "";
		template = salemenNickName+" added remark for " + customerName;
		return template;
	}
	
	/**
	 * @author Li_Chen
	 * Date :2015-5-8
	 * description:
	 * get the transation's template
	 * @param customerName customerName
	 * @return
	 */
	private String getTransationTemplate (String transationStatu,String customerName) {
		String template = "";
		if (CustomerTransationStatus.SUC.getDesc().equals(transationStatu)) 
			template = "The payment from "+customerName+" is success";
		else if (CustomerTransationStatus.NSC.getDesc().equals(transationStatu)) 
			template = "The payment from "+customerName+" is fail";
		return template;
	}
	
	public static void main(String[] args) {
		System.out.println("fdsfs".equals(null));
	}
	
	

}
