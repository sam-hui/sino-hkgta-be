package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotLogDao;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotLogDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class FacilityTimeslotLogServiceImpl extends ServiceBase<FacilityTimeslotLog> implements
		FacilityTimeslotLogService {

	@Autowired
	private FacilityTimeslotLogDao dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityTimeslotLogDto> getLogsByFacilityNo(String facilityType, Long facilityNo) {
		List<FacilityTimeslotLog> entityList = dao.getByFacilityNo(facilityType, facilityNo);
		return FacilityTimeslotLogDto.parseEntities(entityList);
	}

}
