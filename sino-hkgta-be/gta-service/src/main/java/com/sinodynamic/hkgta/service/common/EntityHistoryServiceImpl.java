package com.sinodynamic.hkgta.service.common;


import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.EntityHistoryDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dto.EntityHistoryDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
@Transactional
public class EntityHistoryServiceImpl implements EntityHistoryService {

    @Autowired
    private SysCodeDao sysCodeDao;

    @Autowired
    private EntityHistoryDao entityHistoryDao;

    @Autowired
    private UserMasterDao userMasterDao;

    private static Logger logger = Logger.getLogger(EntityHistoryServiceImpl.class);

    private Properties properties;

    @PostConstruct
    public void init() {
        try {
            Resource resource = new ClassPathResource("/placeholder/route_entity_map.properties");
            properties = PropertiesLoaderUtils.loadProperties(resource);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    @Override
    public EntityHistoryDto getEntityHistory(String route, String[] names, String[] values) {

        if (names == null || values == null || names.length != values.length) {
            throw new GTACommonException(GTAError.CommonError.PARAMETER_VALUE_INVALID);
        }

        if (properties == null) {
            throw new GTACommonException(GTAError.CommonError.Error);
        }

        String entity = properties.getProperty(route);

        if (entity == null) {
            throw new GTACommonException(GTAError.CommonError.PARAMETER_VALUE_INVALID);
        }
        
        EntityHistoryDto result = entityHistoryDao.getEntityHistory(entity, names, values);

        if (result != null) {
            UserMaster creatorMaster = userMasterDao.getUserByUserId(result.getCreateBy());
            if (creatorMaster != null) {
                result.setCreateBy(creatorMaster.getNickname());
            }

            UserMaster updateMaster = userMasterDao.getUserByUserId(result.getUpdateBy());

            if (updateMaster != null) {
                result.setUpdateBy(updateMaster.getNickname());
            }
        }


        return result;
    }
}
