package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.crm.EnrollmentReportDto;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.Data;

public interface EnrollmentReportService extends IServiceBase<EnrollmentReportDto> {
	Data generateEnrollmentReport(String propertyName, String order,int pageSize, int currentPage, Date date);
	BigDecimal getMonthTotalAmount(Date date);
}
