package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface ServicePlanRightMasterService extends IServiceBase<ServicePlanRightMaster>{
	public List<ServicePlanRightMaster> getAllServicePlanRightMaster();
}
