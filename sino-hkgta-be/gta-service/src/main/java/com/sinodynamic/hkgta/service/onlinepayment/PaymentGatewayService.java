package com.sinodynamic.hkgta.service.onlinepayment;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface PaymentGatewayService extends IServiceBase<PaymentGateway> {

	public String payment(CustomerOrderTrans customerOrderTrans);
	
	public String payment(CustomerOrderTrans customerOrderTrans, Map<String, Object> urlParams);

	public String processResult(HttpServletRequest request,String userId);

	public CustomerOrderTrans queryResult(CustomerOrderTrans customerOrderTrans);

}
