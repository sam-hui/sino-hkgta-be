package com.sinodynamic.hkgta.service.common;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.statement.DeliveryRecordService;
import com.sinodynamic.hkgta.util.EmailResponse;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.EmailStatus;

/**
 * This Component is used to send email asynchronously
 * @author Mianping_Wu
 */
@Component
public class MailThreadServiceImpl implements MailThreadService {
	
	
    private Logger logger = Logger.getLogger(MailThreadServiceImpl.class);
    private static ExecutorService pool = Executors.newCachedThreadPool();
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
    @Autowired
    private DeliveryRecordService deliveryRecordService;
    
    public void send(final CustomerEmailContent cec, final File[] attachments) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					boolean success = MailSender.sendEmail(
							cec.getRecipientEmail(), null, null,
							cec.getSubject(), cec.getContent(), attachments);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}

					customerEmailContentService.modifyCustomerEmailContent(cec);

				} catch (Exception e) {

					e.printStackTrace();
					logger.error(e.getMessage());
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
		});
		pool.execute(thread);
    }
    
    public void send(final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					boolean success = MailSender.sendEmailWithBytesMineAttach(
							cec.getRecipientEmail(), null, null,
							cec.getSubject(), cec.getContent(),mineTypeList, bytesList,
							fileNameList);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}

					customerEmailContentService.modifyCustomerEmailContent(cec);

				} catch (Exception e) {

					e.printStackTrace();
					logger.error(e.getMessage());
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
    
    public void sendWithResponse(final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					 EmailResponse response = MailSender.sendEmailWithBytesMineAttachWithResponse(
							cec.getRecipientEmail(), cec.getCopyto(), cec.getBlindCopyTo(),
							cec.getSubject(), cec.getContent(),mineTypeList, bytesList,
							fileNameList);
					if (response!=null&&response.isReportSuccess()) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					
					if(response!=null){
						cec.setServerReturnCode(response.getLastReturnCode()!=null?String.valueOf(response.getLastReturnCode()):null);
						cec.setServerReturnMsg(response.getLastServerResponse());
						cec.setServerRespTimestamp(response.getServerRespTimestamp());
					}
					
					customerEmailContentService.modifyCustomerEmailContent(cec);

				} catch (Exception e) {

					e.printStackTrace();
					logger.error(e.getMessage());
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
    
public void sendWithResponse(final Long batchId,final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					 EmailResponse response = MailSender.sendEmailWithBytesMineAttachWithResponse(
							cec.getRecipientEmail(), null, null,
							cec.getSubject(), cec.getContent(),mineTypeList, bytesList,
							fileNameList);
					if (response!=null&&response.isReportSuccess()) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
						deliveryRecordService.updateErrorCountInBatchSendStatementHd(batchId);
					}
					
					if(response!=null){
						cec.setServerReturnCode(response.getLastReturnCode()!=null?String.valueOf(response.getLastReturnCode()):null);
						cec.setServerReturnMsg(response.getLastServerResponse());
						cec.setServerRespTimestamp(response.getServerRespTimestamp());
					}
					
					customerEmailContentService.modifyCustomerEmailContent(cec);
					

				} catch (Exception e) {

					e.printStackTrace();
					logger.error(e.getMessage());
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
    
    public void send(final CustomerEmailContent cec) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					boolean success = MailSender.sendTextEmail(cec.getRecipientEmail(), null, null, cec.getSubject(),cec.getContent());
					if (success) {
						cec.setStatus(EmailStatus.SENT.getDesc());
					} else {
						cec.setStatus(EmailStatus.FAIL.getDesc());
					}

					customerEmailContentService.modifyCustomerEmailContent(cec);

				} catch (Exception e) {

					e.printStackTrace();
					logger.error(e.getMessage());
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
}
