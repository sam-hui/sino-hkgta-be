package com.sinodynamic.hkgta.service.sys;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.sys.SysUserDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.Data;

public interface SysUserService extends IServiceBase<UserMaster> {
	void addSysUser(SysUserDto user);
	void editSysUser(SysUserDto user);
	SysUserDto viewSysUser(String userId);
	Data loadAllUsers(String propertyName, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters);
	
	List<AdvanceQueryConditionDto> userListAdvanceSearch();
	void refreshUserRole(String userId);
}
