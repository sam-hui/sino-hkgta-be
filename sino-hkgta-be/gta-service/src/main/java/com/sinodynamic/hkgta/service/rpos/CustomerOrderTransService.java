package com.sinodynamic.hkgta.service.rpos;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.statement.StatementParamsDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CustomerOrderTransService extends IServiceBase<CustomerOrderTrans> {
	public ListPage<CustomerOrderHd> getCustomerTransactionList(ListPage<CustomerOrderHd> page,String balanceDue,String serviceType,String orderBy,boolean isAscending,String searchText,String filterBySalesMan,String mode,String userId, String enrollStatusFilter,String memberType,String device);

    public ResponseResult savecustomerOrderTransService(CustomerOrderTransDto customerOrderTransDto,Map<String, Long> transactionNoMap) throws Exception;

	public ResponseResult getPaymentDetailsByOrderNo(Long orderNo);
	public ResponseResult getPaymentDetailsAccountantByOrderNo(Long orderNo);
	public void moveTranscriptFile(Long transactionNo,Long customerId);
	
	public CustomerOrderTrans getCustomerOrderTrans(Long transactionNo);
	public ResponseResult paymentByCreditCard(CustomerOrderTransDto customerOrderTransDto);
	public Long getOrderTimeoutMin();
	public List<CustomerOrderTrans> getTimeoutPayments();
	public void updateCustomerOrderTrans(CustomerOrderTrans customerOrderTrans);
	public CustomerOrderHd getCustomerOrderHdByOrderNo(Long orderNo);
	public void addNewTransactionRecord(CustomerOrderTrans customerOrderTrans);
	public List<CustomerOrderTrans> getCustomerOrderTransListByOrderNo(Long orderNo);
	
	public CustomerOrderTransDto payment(MemberCashValuePaymentDto memberCashValuePaymentDto);
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String serviceType);
	public ResponseResult getAllCreatedStaff();
	public ResponseResult getAllAuditedStaff();
	public ResponseResult getSattlementReport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto);
	public ResponseResult getSattlementReportExport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto,HttpServletResponse response)throws Exception;

	public ResponseResult readTrans(CustomerOrderTransDto customerOrderTransDto);
	
	public ResponseResult handleCreditCardByPos(PosResponse posResponse);
	
	/**
	 * Used to change enrollment status from Approved to Payment Approved or Payment Approved to Approved
	 * @param transactionNo
	 * @author Liky_Pan
	 */
	public void updateEnrollmentStatusOncePayementChange(Long transactionNo);
	public void generatePatronAccountStatement()throws Exception;
	public void manualGenerateStatement(String year,String month)throws Exception;
	
	public void modifyCustomerOrderHd(CustomerOrderHd order);
	public ResponseResult getStatementPDF(StatementParamsDto statementParamsDto) throws Exception;
	public byte[] getInvoiceReceipt(Long orderNo,String transactionNo,String receiptType);
	public ResponseResult sentTransactionEmail(TransactionEmailDto dto, LoginUserDto userDto);
	
	public TransactionEmailDto getEmailDeailReady(TransactionEmailDto transactionEmailDto,String userName);
	public ResponseResult handlePosPayDaypass(PosResponse posResponse);
	public ResponseResult handlePosPayPrivateCoach(PosResponse posResponse);
	public ResponseResult getMonthlyEnrollment(ListPage<CustomerOrderTrans> page, String year, String month);
	public byte[] getMonthlyEnrollmentAttachment(String year, String month,String fileType,String sortBy,String isAscending);

	public ResponseResult getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage<?> page,
			String selectedDate, String facilityType);
	public byte[] getDailyMonthlyPrivateCoachingRevenueAttach(String timePeriodType, String selectedDate,
			String fileType, String sortBy, String isAscending, String facilityType);
	
	public byte[] getEnrollmentRenewal(String fileType, String sortBy, String isAscending) throws Exception;

}
