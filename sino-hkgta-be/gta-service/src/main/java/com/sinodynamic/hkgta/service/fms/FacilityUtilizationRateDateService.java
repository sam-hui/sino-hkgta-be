package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateDateDto;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateDate;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityUtilizationRateDateService extends IServiceBase<FacilityUtilizationRateDate> {

	List<FacilityUtilizationRateDateDto> getSpecialRateDateList(String facilityType,int year, String sortBy, String ascending, String subType) throws Exception;

	FacilityUtilizationRateDateDto getSpecialRateDateListByDate(String facilityType, String specialDate, String subType) throws Exception;
	
	void saveSpecialRateDate(String facilityType, FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) throws Exception;
	
	void updateSpecialRateDate(String facilityType, FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) throws Exception;
	
	void deleteSpecialRateDate(long dateId) throws Exception;
	
	Boolean checkSpecialDateExistance(String specialDate, String facilityType, String subType) throws Exception;
	
	Boolean checkSpecialDateExistanceById(long dateId) throws Exception;
}
