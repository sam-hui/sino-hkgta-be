package com.sinodynamic.hkgta.service.carpark;

import java.util.List;

import com.sinodynamic.hkgta.dto.carpark.CarParkMemberCheckDto;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CarParkMemberCheckService extends IServiceBase{

	public List<CarParkMemberCheckDto> getCarParkMemberCheckList();
	public List<CarParkMemberCheckDto> getCarParkStaffCheckList();
}
