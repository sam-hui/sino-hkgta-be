package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityTypeService extends IServiceBase<FacilityType>{
	public List<FacilityType> getAllFacilityType();
	
	public FacilityType getFacilityType(String typeCode);
}
