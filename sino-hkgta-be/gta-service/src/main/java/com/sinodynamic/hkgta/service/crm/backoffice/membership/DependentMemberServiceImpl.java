package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAddressDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollPoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.dto.crm.AdditionalInfoDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;
import com.sinodynamic.hkgta.entity.crm.CustomerAddressPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.AddressType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.memberType;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Service
@SuppressWarnings("deprecation")
public class DependentMemberServiceImpl extends ServiceBase<CustomerProfile> implements DependentMemberService{
	
	private Logger logger = Logger.getLogger(DependentMemberServiceImpl.class);
	
	@Autowired
	private UserMasterDao userMasterDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	@Autowired
	private CustomerAddressDao customerAddressDao;

	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEnrollPoDao customerEnrollPoDao;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private SpaMemberSyncDao spaMemberSyncDao;
 
	@Transactional(rollbackFor=Exception.class)
	public ResponseResult saveDependentMemberInfo(CustomerProfile cp, String currentUserId,String fromName)
			throws Exception {
		logger.debug("creating DepartmentMember now!");
		
		if(null == cp || null == cp.getMember()){
			responseResult.initResult(GTAError.DependentMemberError.INCORRECT_JSON_FORMAT);
			return responseResult;
		}
		
		MemberLimitRule limitRule = cp.getMemberLimitRule();
		if(limitRule==null){
			limitRule = setMemberLimitRuleTRN(BigDecimal.ZERO);
		}
		
		ResponseResult profileMsg = checkInputProfileData(cp);
		
		if(!"0".equals(profileMsg.getReturnCode()))
			return profileMsg;
		
		if(cp.getPassportNo()!=null&&cp.getPassportType()!=null&&!customerProfileDao.checkAvailablePassportNo(null,cp.getPassportType(), cp.getPassportNo())){
			responseResult.initResult(GTAError.DependentMemberError.ID_BEEN_USED);
			return responseResult;
		}
		
		ResponseResult limitRuleMsg = checkLimitRuleInputData(limitRule);
		if(!"0".equals(limitRuleMsg.getReturnCode()))
			return limitRuleMsg;

		ResponseResult memberMsg = checkMemberInputData(cp.getMember());
		if(!"0".equals(memberMsg.getReturnCode()))
			return memberMsg;
		
		cp.setCreateBy(currentUserId);
		cp.setUpdateBy(currentUserId);
		cp.setCreateDate(new Date());
		cp.setUpdateDate(new Date());
		cp.setIsDeleted("N");
		
		//----------------Images will be saved by using individual method(in Controller)--------------
		String portraitPhoto = cp.getPortraitPhoto();
		String signature = cp.getSignature();
		cp.setPortraitPhoto(null);
		cp.setSignature(null);
		//-------------------------------------------------------------------------------
		
		Long customerId = (Long)customerProfileDao.addCustomerProfile(cp);
		
		String academyNo = getTheNextAvailableAcademyNo();
		
		//save userMaster
		boolean checkLoginId = userMasterDao.checkAvailableLoginId(academyNo, null);
		if(!checkLoginId) throw new GTACommonException(GTAError.DependentMemberError.LOGIN_ID_BEEN_USED);
		String randomPassword =  CommUtil.generateRandomPassword();
		UserMaster user = createUserMaster(academyNo, customerId, currentUserId, cp.getSurname(), cp.getGivenName(), randomPassword);
		String userId = (String)userMasterDao.save(user);
		
		Member member = cp.getMember();
		//seach primary
		Member pm = memberDao.getMemberById(member.getSuperiorMemberId());
		
		if(null == pm){
			responseResult.initResult(GTAError.DependentMemberError.PRIMARY_MEMBER_NULL);
			return responseResult;
		}
		
		if(!memberType.IPM.name().equals(pm.getMemberType()) && !memberType.CPM.name().equals(pm.getMemberType())){
			responseResult.initResult(GTAError.DependentMemberError.PRIMARY_MEMBER_TYPE_ERROR);
			return responseResult;
		}
		
		member = setMemberInfo(member, pm, customerId, userId, academyNo, currentUserId,cp.getRelationshipCode());
		memberDao.addMember(member);
		spaMemberSyncDao.addSpaMemberSyncWhenInsert(member);
		
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(new Date());
//		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(member.getSuperiorMemberId());
//		if ("IPM".equals(pm.getMemberType())&&customerEnrollment!=null&&customerEnrollment.getSubscribeContractLength() != null) {
//			calendar.add(Calendar.MONTH, customerEnrollment.getSubscribeContractLength().intValue());
//		}
//		
//		if("CPM".equals(pm.getMemberType())){
//			calendar.add(Calendar.MONTH, 50);//for corporate dependent usage: 50 Year
//		}
		
		if(null != limitRule){
			//Save the member limit rule for transaction usage
			MemberLimitRule primayMemberCR = memberLimitRuleDao.getEffectiveMemberLimitRule(member.getSuperiorMemberId(), "CR");
			limitRule = setMemberLimitRule(limitRule, customerId, currentUserId,new Date(),primayMemberCR!=null?primayMemberCR.getExpiryDate():null);
			memberLimitRuleDao.save(limitRule);
		}
		
		//Copy Member Limit Rule From Primary Member
		copyMemberLimitRuleFromPrimayMember(member.getSuperiorMemberId(),customerId,currentUserId);
		
		//Copy Member Facility Right From Primary Member
		copyMemberFacilityRight(member.getSuperiorMemberId(),customerId);
		
		
		//Update or Save Optional Or favor
		saveOrUpdateOptionalOrFavor(cp,customerId,user.getUserId());
		
		//Update or save billing address
		saveOrUpdateBillingAddress(cp);
		
		CustomerEmailContent contentDependent = customerEmailContentService.setEmailContentForMemberActivationEmail(customerId, EmailType.ACTIVATION.getFunctionId(), currentUserId, fromName,true,academyNo,randomPassword);
		if(contentDependent!=null) mailThreadService.sendWithResponse(contentDependent, null, null, null);
		
		//For Moving Portrait Photo in Controller
		CustomerProfile returnCustomerProfile = new CustomerProfile();
		returnCustomerProfile.setCustomerId(customerId);
		returnCustomerProfile.setPortraitPhoto(portraitPhoto);
		returnCustomerProfile.setSignature(signature);
		
		responseResult.initResult(GTAError.Success.SUCCESS,returnCustomerProfile);
		return responseResult;
	}
	
	private void copyMemberLimitRuleFromPrimayMember(Long superiodMemberId,Long customerId,String updateBy){
		List<MemberLimitRule> primaryLimitRule = memberLimitRuleDao.getEffectiveListByCustomerId(superiodMemberId);
		if(null != primaryLimitRule && primaryLimitRule.size() > 0){
			for(MemberLimitRule rule : primaryLimitRule){
				if(null != primaryLimitRule && !"CR".equals(rule.getLimitType())&&!"G1".equals(rule.getLimitType())){
					MemberLimitRule r = new MemberLimitRule();
					BeanUtils.copyProperties(rule, r, new String[]{"effectiveDate","limitId","customerId","updateBy","updateDate"});
					r.setEffectiveDate(new Date());
					r.setCustomerId(customerId);
					r.setUpdateBy(updateBy);
					r.setUpdateDate(new Date());
					memberLimitRuleDao.save(r);
				}
			}
		}
	}
	
	private void copyMemberFacilityRight(Long superiodMemberId,Long customerId){
		List<MemberPlanFacilityRight> mfr = memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(superiodMemberId);
		if(null != mfr && mfr.size() > 0){
			for(MemberPlanFacilityRight right : mfr){
				if(null != right){
					MemberPlanFacilityRight r = new MemberPlanFacilityRight();
					BeanUtils.copyProperties(right, r, new String[]{"effectiveDate","sysId","customerId"});
					r.setCustomerId(customerId);
					r.setEffectiveDate(new Date());
					memberPlanFacilityRightDao.save(r);
				}
			}
		}
	}
	
	private CustomerEmailContent setCustomerEmailContent(MessageTemplate template, CustomerProfile profile){
		if(null != template){
			CustomerEmailContent email = new CustomerEmailContent();
			email.setRecipientCustomerId(profile.getCustomerId().toString());
			email.setRecipientEmail(profile.getContactEmail());
			email.setSubject(template.getMessageSubject());
			email.setNoticeType(Constant.NOTICE_TYPE_STAFF);
			email.setContent(template.getContentHtml().replaceAll("<Member's First Name>", profile.getSurname()));
			email.setSendDate(new Date());
			return email;
		}
		return null;
	}
	
	public MemberLimitRule setMemberLimitRuleTRN(BigDecimal spendingLimit){
		MemberLimitRule memberLimitRule = new MemberLimitRule();
		memberLimitRule.setLimitType("TRN");
		memberLimitRule.setLimitUnit("EACH");
		memberLimitRule.setNumValue(spendingLimit);
		return memberLimitRule;
	}
	
	private MemberLimitRule setMemberLimitRule(MemberLimitRule limitRule, Long customerId, String createBy,Date effectiveDate, Date expiryDate){
		if("TRN".equals(limitRule.getLimitType())){
			limitRule.setLimitUnit("EACH");
		}
		limitRule.setCustomerId(customerId);
		limitRule.setCreateDate(new Timestamp(System.currentTimeMillis()));
		limitRule.setUpdateDate(new Date());
		limitRule.setCreateBy(createBy);
		limitRule.setUpdateBy(createBy);
		limitRule.setEffectiveDate(effectiveDate);
		limitRule.setExpiryDate(expiryDate);
		return limitRule;
	}
	
	
	private Member setMemberInfo(Member m ,Member pm,  Long customerId, String userId, String academyNo, String createBy, String relationshipCode){
		m.setCustomerId(customerId);
		m.setUserId(userId);
		m.setAcademyNo(academyNo);
		
		if("IPM".equals(pm.getMemberType())){
			m.setMemberType("IDM");
		}else if("CPM".equals(pm.getMemberType())){
			m.setMemberType("CDM");
		}
		m.setStatus("ACT");
		m.setRelationshipCode(relationshipCode);
		m.setFirstJoinDate(new Date());
		m.setEffectiveDate(pm.getEffectiveDate());
		m.setCreateDate(new Date());
		m.setCreateBy(createBy);
		m.setUpdateDate(new Date());
		m.setUpdateBy(createBy);
		return m;
	}
	
	
	private UserMaster createUserMaster(String academyNo, Long customerId, String createBy, String surName, String givenName, String password) {
	
		String userId = getTheUserId(academyNo,customerId);
		UserMaster userMaster = new UserMaster();
		userMaster.setUserId(userId);
		userMaster.setLoginId(academyNo);
		userMaster.setNickname(givenName+"_"+surName);
		userMaster.setPassword(CommUtil.getMD5Password(academyNo, password));
		userMaster.setUserType("CUSTOMER");
		userMaster.setStatus("ACT");
		userMaster.setCreateDate(new Date());
		userMaster.setCreateBy(createBy);
		userMaster.setUpdateDate(new Date());
		userMaster.setUpdateBy(createBy);
		return userMaster;
	}
	
	/**
	 * Used to get the next available academy No.
	 * @author Liky_Pan
	 * @return
	 */
	private String getTheNextAvailableAcademyNo() {
		String stringAcademyNo = memberDao.findLargestAcademyNo();
		if (stringAcademyNo == null)
			stringAcademyNo = "0";
		Integer tempAcademyNo = Integer.parseInt(stringAcademyNo) + 1;
		return CommUtil.formatAcademyNo(tempAcademyNo);
	}
	
	/**
	 * Used to get the used by using the academy no and customer Id
	 * @author Liky_Pan
	 * @param academyNo
	 * @param customerId
	 * @return
	 */
	private String getTheUserId(String academyNo, Long customerId){
		return academyNo+"-"+Long.toHexString(customerId);
	}
	
	/**
	 * validate data for add and edit enrollment
	 * @param dto
	 * @return
	 */
	
	
	private ResponseResult checkMemberInputData(Member member){
		
		if(null == member ||null == member.getSuperiorMemberId()){
			//return new ResponseResult("1", " member's SuperiorMemberId is required!");
			responseResult.initResult(GTAError.DependentMemberError.SUPERIOR_MEMBER_ID_NULL);
			return responseResult;
		}
		
		if(member.getSuperiorMemberId().toString().length() > 20){
			//return new ResponseResult("2", "member's SuperiorMemberId is too long!");
			responseResult.initResult(GTAError.DependentMemberError.SUPERIOR_MEMBER_ID_TOO_LONG);
			return responseResult; 
		}
		
		//return new ResponseResult("0", "");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult; 
	}
	
	
	private ResponseResult checkLimitRuleInputData(MemberLimitRule memberlimitrule){
		if(null == memberlimitrule || StringUtils.isEmpty(memberlimitrule.getLimitType())){
			//return new ResponseResult("1", " memberlimitrule's LimitType is required!");
			responseResult.initResult(GTAError.DependentMemberError.LIMITTYPE_NULL);
			return responseResult;
		}
		
		if(!memberlimitrule.getLimitType().toUpperCase().equals("TRN")){
			//return new ResponseResult("2", "LimitType should be 'TRN'");
			responseResult.initResult(GTAError.DependentMemberError.LIMITTYPE_INVALID);
			return responseResult;
		}
//		if(memberlimitrule.getNumValue().toString().length() > 10){
//			return new ResponseMsg("2", "memberlimitrule's NumValue is too long!");
//		}
		//return new ResponseResult("0", "");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult; 
	}
	
	
	private ResponseResult checkInputProfileData(CustomerProfile cp){
		
//		if(!"M".equals(cp.getGender()) && !"F".equals(cp.getGender())){
//			return new ResponseMsg("1","Gender is 'M' or 'F'!");
//		}
		List<CustomerAdditionInfo> ciList = cp.getCustomerAdditionInfos();
		if(ciList != null && ciList.size()>0){
			for(CustomerAdditionInfo info : ciList){
				if(StringUtils.isEmpty(info.getCaptionId())){
					//return new ResponseResult("1","Caption Id is required!");	
					responseResult.initResult(GTAError.DependentMemberError.CAPTION_ID_NULL);
					return responseResult;
				}
//				if(null == info.getSysId()){
//					return new ResponseMsg("1","Sys Id is required!");
//				}
				/*if(!CommUtil.notEmpty(info.getCustomerInput()+"")){
					return new ResponseMsg("1","","Customer Input is required!");
				}*/
			}
		}

		List<CustomerAddress> caList = cp.getCustomerAddresses();
		if("false".equalsIgnoreCase(cp.getCheckBillingAddress())){
			if(caList != null && caList.size()>0){
				for(CustomerAddress ca : caList){
					if(StringUtils.isEmpty(ca.getAddress1())){
						//return new ResponseResult("1","Bill Address is required!");
						responseResult.initResult(GTAError.DependentMemberError.BILL_ADDRESS_NULL);
						return responseResult;
					}
					if(StringUtils.isEmpty(ca.getHkDistrict())){
						//return new ResponseResult("1","Bill District is required!");
						responseResult.initResult(GTAError.DependentMemberError.BILL_DISSTRICT_NULL);
						return responseResult;
					}
					if(StringUtils.isEmpty(ca.getAddressType())){
						//return new ResponseResult("1","Bill Address Type is required!");
						responseResult.initResult(GTAError.DependentMemberError.BILL_ADDRESS_TYPE_NULL);
						return responseResult;
					}
				}
			}
		}
		
		StringBuilder ms = new StringBuilder();
		if(StringUtils.isEmpty(cp.getPassportType())){
			ms.append("Passport Type, ");
		}
		if(StringUtils.isEmpty(cp.getPassportNo())){
			ms.append("Passport No, ");
		}
		if(StringUtils.isEmpty(cp.getSalutation())){
			ms.append("Salutation, ");
		}
		if(StringUtils.isEmpty(cp.getSurname())){
			ms.append("Surname, ");
		}
		if(StringUtils.isEmpty(cp.getGivenName())){
			ms.append("Given Name, ");
		}
		if(StringUtils.isEmpty(cp.getContactEmail())){
			ms.append("Contact Email, ");
		}
		if(ms.length()>0){
			//return new ResponseResult("1",ms.toString()+" are required parameters!");
			responseResult.initResult(GTAError.DependentMemberError.PARAMETER_NULL, new Object[]{ms.toString()});
			return responseResult;
		}
			
		if(!PassportType.HKID.name().equals(cp.getPassportType()) && !PassportType.VISA.name().equals(cp.getPassportType())){
			//return new ResponseResult("1", "Passport Type incorrect, please input HKID or VISA! ");
			responseResult.initResult(GTAError.DependentMemberError.PASSPORT_TYPE_INCORRECT);
			return responseResult;
		}

		if(PassportType.HKID.name().equals(cp.getPassportType())&& !CommUtil.validateHKID(cp.getPassportNo())){
			//return new ResponseResult("1", "Passport Number: Invalid!,please input a new Passport Number!)");
			responseResult.initResult(GTAError.DependentMemberError.PASSPORT_NUMBER_INVALID);
			return responseResult;
		}
		if(PassportType.VISA.name().equals(cp.getPassportType())&& !CommUtil.validateVISA(cp.getPassportNo())){
			//return new ResponseResult("1", "VISA Number: Invalid!,Please input a new VISA Number!");
			responseResult.initResult(GTAError.DependentMemberError.VISA_NUMBER_INVALID);
			return responseResult;
		}
		
		if (!StringUtils.isEmpty(cp.getPhoneMobile()) && !CommUtil.validatePhoneNo(cp.getPhoneMobile())) {
			responseResult.initResult(GTAError.LeadError.MOBILE_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(cp.getPhoneBusiness()) && !CommUtil.validatePhoneNo(cp.getPhoneBusiness())) {
			responseResult.initResult(GTAError.LeadError.BUSINESS_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(cp.getPhoneHome()) && !CommUtil.validatePhoneNo(cp.getPhoneHome())) {
			responseResult.initResult(GTAError.LeadError.HOME_PHONE_INVALID);
			return responseResult;
		}
		
		if(StringUtils.isNotEmpty(cp.getContactEmail()) && !CommUtil.validateEmail(cp.getContactEmail())){
			//return new ResponseResult("1", "Email Address  : Invalid!,please input a new Email address!");
			responseResult.initResult(GTAError.DependentMemberError.EMAIL_ADDRESS_INVALID);
			return responseResult;
		}

		//return new ResponseResult("0", "");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult; 
	}

	@Override
	@Transactional
	public ResponseMsg updateProfilePhoto(CustomerProfile cp) throws Exception {
		// TODO Auto-generated method stub
		if(null == cp || null ==  cp.getCustomerId() || StringUtils.isEmpty(cp.getPortraitPhoto())){
			//return new ResponseMsg("1", "profile customId and PortraitPhoto are required");
			responseResult.initResult(GTAError.DependentMemberError.PROFILE_AND_PORTRAITPHOTO_NULL);
			return responseResult;
		}
		CustomerProfile cs = customerProfileDao.getById(cp.getCustomerId());
		cs.setPortraitPhoto(cp.getPortraitPhoto());
		cs.setUpdateBy(cp.getUpdateBy());
		cs.setUpdateDate(new Date());
		
		boolean result = customerProfileDao.update(cs);
		
		if(result){
			//return new ResponseMsg("0", "update profilephoto success!");
			responseResult.initResult(GTAError.Success.SUCCESS, "update profilephoto success!");
			return responseResult; 
		}
		else{
			//return new ResponseMsg("1", "update profilephoto failed!");
			responseResult.initResult(GTAError.DependentMemberError.FAILED_TO_UPDATE_PROFILE_PHOTO);
			return responseResult;
		}
	}

	@Transactional
	public ResponseResult checkDependentMember(String academyNo){
		Member member = memberDao.getMemberByAcademyNo(academyNo);
		if(member==null){
			//return new ResponseResult("1", "Can not find the member's info");
			responseResult.initResult(GTAError.DependentMemberError.CANNOT_FIND_MEMBER_INFO);
			return responseResult;
		}
		String status = member.getStatus();
		if(Constant.Member_Status_NACT.equals(status)){
			//return new ResponseResult("1", "Please re-activate your member before processing");
			responseResult.initResult(GTAError.DependentMemberError.RETRIEVE_BEFORE_PROCESSING);
			return responseResult;
		}
		String memberType = member.getMemberType();
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(member.getCustomerId());
		if(MemberType.IPM.name().equals(memberType)){
			if(customerEnrollment!=null&&EnrollStatus.ANC.name().equals(customerEnrollment.getStatus())||EnrollStatus.CMP.name().equals(customerEnrollment.getStatus())){
				CustomerProfile customerProfile = customerProfileDao.getById(member.getCustomerId());
				MemberDto memberDto = new MemberDto();
				memberDto.setPrimaryMemberFullName(customerProfile.getSalutation()+" "+customerProfile.getGivenName()+" "+ customerProfile.getSurname());
				memberDto.setDependentOrPrimary(false);
				memberDto.setSuperiorMemberId(member.getCustomerId());
				responseResult.initResult(GTAError.Success.SUCCESS,memberDto);
				return responseResult;
			}else{
				//return new ResponseResult("1", "Please finish the enrollment registration and payment");
				responseResult.initResult(GTAError.DependentMemberError.ENROLLMENT_AND_PAYMENT_IS_NOT_COMPLETED);
				return responseResult;
			}
		}else if(MemberType.IDM.name().equals(memberType)||MemberType.CDM.name().equals(memberType)){
			MemberDto memberDto = new MemberDto();
			memberDto.setDependentOrPrimary(true);
			responseResult.initResult(GTAError.Success.SUCCESS,memberDto);
			return responseResult;
		}else if(MemberType.CPM.name().equals(memberType)){
			CustomerProfile customerProfile = customerProfileDao.getById(member.getCustomerId());
			MemberDto memberDto = new MemberDto();
			memberDto.setPrimaryMemberFullName(customerProfile.getSalutation()+" "+customerProfile.getGivenName()+" "+ customerProfile.getSurname());
			memberDto.setDependentOrPrimary(false);
			memberDto.setSuperiorMemberId(member.getCustomerId());
			responseResult.initResult(GTAError.Success.SUCCESS,memberDto);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult getDependentMemberByAcademyNo(String academyNo)
			throws Exception {
		
		if(StringUtils.isEmpty(academyNo)){
			//return new ResponseResult("1", "AcademyNo is nul!");
			responseResult.initResult(GTAError.DependentMemberError.ACADEMY_NO_NULL);
			return responseResult;
		}
		Member member = memberDao.getMemberByAcademyNo(academyNo);
		
		if(null == member){
			//return new ResponseResult("1", "Can't find the member!");
			responseResult.initResult(GTAError.DependentMemberError.CANNOT_FIND_MEMBER);
			return responseResult;
		}
		
		if(!"IDM".equals(member.getMemberType()) && !"CDM".equals(member.getMemberType())){
			//return new ResponseResult("1", "The member is not a dependent member!");
			responseResult.initResult(GTAError.DependentMemberError.NOT_A_DEPENDENT_MEMBER);
			return responseResult;
		}
		
		//search profile
		CustomerProfile customer = customerProfileDao.getById(member.getCustomerId());
		customer.setRelationshipCode(member.getRelationshipCode());
		
		if(null == member.getSuperiorMemberId()){
			//return new ResponseResult("1", "data error,departmentmember has no primary member!");
			responseResult.initResult(GTAError.DependentMemberError.NO_PRIMARY_MEMBER);
			return responseResult;
		}
		
		CustomerProfile parentCustomerProfile = customerProfileDao.getById(member.getSuperiorMemberId());
		MemberDto memberDto = new MemberDto();
		memberDto.setCustomerId(member.getSuperiorMemberId());
		memberDto.setCustomerName(parentCustomerProfile.getSalutation()+" "+parentCustomerProfile.getGivenName()+" "+ parentCustomerProfile.getSurname());
		memberDto.setDependentOrPrimary(true);
		
		MemberLimitRule limitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(member.getCustomerId(), "TRN");
		
		List<AdditionalInfoDto> customerOptAdditionInfo = customerAdditionInfoDao.getByCustomerIdAndCategory(member.getCustomerId(),"OPT");
		List<AdditionalInfoDto> customerFvrAdditionInfo = customerAdditionInfoDao.getByCustomerIdAndCategory(member.getCustomerId(),"FVR");
		
		List<CustomerAddress> customerAddresses = customerAddressDao.getCustomerAddressesByCustomerID(member.getCustomerId());
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("primaryMember", memberDto);
		resultMap.put("limitRule", limitRule);
		resultMap.put("customerProfile", customer);
		resultMap.put("customerOptAdditionInfo", customerOptAdditionInfo);
		resultMap.put("customerFvrAdditionInfo", customerFvrAdditionInfo);
		resultMap.put("customerAddresses", customerAddresses);
		responseResult.initResult(GTAError.Success.SUCCESS, resultMap);
		return responseResult;
	}


	@SuppressWarnings("unused")
	@Override
	@Transactional(rollbackFor= Exception.class)
	public ResponseMsg updateDependentMemberInfo(CustomerProfile customerProfile,
			String updateBy,String fromName) throws Exception {
	logger.debug("update DepartmentMember now!");
		Date currentDate = new Date();
		if(null == customerProfile){
			//return new ResponseMsg("1","Please input correct parameters json format!");
			responseResult.initResult(GTAError.DependentMemberError.PARAMETER_FORMAT_INVALID);
			return responseResult;
		}
		
		ResponseMsg msg = checkInputProfileData(customerProfile);
		if(!"0".equals(msg.getReturnCode()))
			return msg;
		
		String passportNo = customerProfile.getPassportNo();
		String passportType = customerProfile.getPassportType();
		String contactEmail = customerProfile.getContactEmail();
		Long customerId = customerProfile.getCustomerId();
		if(passportNo!=null&&passportType!=null&&!customerProfileDao.checkAvailablePassportNo(customerProfile.getCustomerId(),passportType, passportNo)){
			responseMsg.initResult(GTAError.LeadError.PASSPORT_IS_EXIST);
			return responseMsg;
		}
		
		CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(customerProfile.getCustomerId().toString());
		
		if(null == profile){
			//return new ResponseMsg("1","dependentmember isn't exist!");
			responseResult.initResult(GTAError.DependentMemberError.DEPENDENT_MEMBER_NOT_EXISTS);
			return responseResult;
		}
		
		customerProfileDao.getCurrentSession().evict(profile);
		
		customerProfile.setUpdateBy(updateBy);
		customerProfile.setUpdateDate(new Date());
		customerProfile.setIsDeleted("N");
		customerProfile.setVersion(profile.getVersion());
		boolean updateMemberStatus = memberDao.updateRelationship(customerProfile.getRelationshipCode(), customerProfile.getCustomerId(),updateBy);
		
		
		Member member = memberDao.getMemberByCustomerId(customerProfile.getCustomerId());
		
		//update customerProfile
		customerProfileDao.update(customerProfile);
		
		//Update or Save Optional Or favor
		saveOrUpdateOptionalOrFavor(customerProfile,customerId,updateBy);
		
		//Update or save billing address
		saveOrUpdateBillingAddress(customerProfile);
		
		MemberLimitRule limitRule = customerProfile.getMemberLimitRule();
		/* If import status = true, 
		 * the member is used to upgrade the Rejected/Canceled Primary Member to Dependent Member 
		 * else Just for normal edit.
		 */
		if ("true".equalsIgnoreCase(customerProfile.getImportStatus())) {
			Long superiorMemberId = customerProfile.getSuperiorMemberId();
			Member primaryMember = memberDao.getMemberById(superiorMemberId);
			
			CustomerEnrollment superiorEnroll = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(superiorMemberId);
			String superiorEnrollStatus = superiorEnroll.getStatus();
			
			CustomerEnrollment currentEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
			
			//Update Member, from  Rejected or Canceled IPM to IDM or CDM
			if((EnrollStatus.REJ.name().equals(superiorEnrollStatus)||EnrollStatus.CAN.name().equals(superiorEnrollStatus))&&MemberType.IPM.name().equals(member.getMemberType())){
				String academyNo = member.getAcademyNo();
				if (academyNo == null) {
					academyNo = getTheNextAvailableAcademyNo();
				}
				String userId = getTheUserId(academyNo, customerId);
				boolean checkLoginId = userMasterDao.checkAvailableLoginId(academyNo, userId);
				if (!checkLoginId)
					throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
				String randomPassword = CommUtil.generateRandomPassword();
				UserMaster user = createUserMaster(academyNo, customerId, updateBy, customerProfile.getSurname(), customerProfile.getGivenName(),randomPassword);
				boolean validateUserMaster = userMasterDao.saveOrUpdate(user);
				
				member.setUserId(userId);
				member.setAcademyNo(academyNo);
				if (memberType.IPM.name().equals(primaryMember.getMemberType())) {
					member.setMemberType("IDM");
				} else if (memberType.CPM.name().equals(primaryMember.getMemberType())) {
					member.setMemberType("CDM");
				}
				member.setStatus(Constant.General_Status_ACT);
				member.setFirstJoinDate(currentDate);
				member.setEffectiveDate(primaryMember.getEffectiveDate());
				member.setUpdateDate(currentDate);
				member.setUpdateBy(updateBy);
				member.setSuperiorMemberId(superiorMemberId);
				memberDao.update(member);
				
				//Copy Member Limit Rule From Primary Member
				copyMemberLimitRuleFromPrimayMember(superiorMemberId,customerProfile.getCustomerId(),updateBy);
				
				//Copy Member Facility Right From Primary Member
				copyMemberFacilityRight(superiorMemberId,customerProfile.getCustomerId());
				
				if (limitRule == null) {
					limitRule = setMemberLimitRuleTRN(BigDecimal.ZERO);
				}
				if (null != limitRule) {
					MemberLimitRule primayMemberCR = memberLimitRuleDao.getEffectiveMemberLimitRule(superiorMemberId, "CR");
					limitRule = setMemberLimitRule(limitRule, customerProfile.getCustomerId(), updateBy, currentDate, primayMemberCR!=null?primayMemberCR.getExpiryDate():null);
					memberLimitRuleDao.save(limitRule);
				}
				
				//remove the customer enroll po to avoid the duplicate of enrollment
				int enrollPoDelete = customerEnrollPoDao.deleteCustomerEnrollPoByEnrollId(currentEnrollment.getEnrollId());
				
				CustomerEmailContent contentDependent = customerEmailContentService.setEmailContentForMemberActivationEmail(customerId, EmailType.ACTIVATION.getFunctionId(), updateBy, fromName,true,academyNo,randomPassword);
				if(contentDependent!=null) mailThreadService.sendWithResponse(contentDependent, null, null, null);
			}
			
			//Import Lead. Upgrade Lead to IDM or CDM
			if(member==null&&EnrollStatus.OPN.name().equals(currentEnrollment.getStatus())){
				
				String nextAcademyNo = getTheNextAvailableAcademyNo();
				String userId = getTheUserId(nextAcademyNo, customerId);
				boolean checkLoginId = userMasterDao.checkAvailableLoginId(customerProfile.getContactEmail(), userId);
				if (!checkLoginId)
					throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
				String randomPassword = CommUtil.generateRandomPassword();
				UserMaster user = createUserMaster(nextAcademyNo, customerId, updateBy, customerProfile.getSurname(), customerProfile.getGivenName(), randomPassword);
				boolean validateUserMaster = userMasterDao.saveOrUpdate(user);
				
				Member m = new Member();
				m.setCustomerId(customerId);
				m.setAcademyNo(nextAcademyNo);
				if (memberType.IPM.name().equals(primaryMember.getMemberType())) {
					m.setMemberType("IDM");
				} else if (memberType.CPM.name().equals(primaryMember.getMemberType())) {
					m.setMemberType("CDM");
				}
				m.setUserId(userId);
				m.setStatus(Constant.General_Status_ACT);
				m.setFirstJoinDate(currentDate);
				m.setEffectiveDate(primaryMember.getEffectiveDate());
				m.setCreateDate(currentDate);
				m.setCreateBy(updateBy);
				m.setSuperiorMemberId(superiorMemberId);
				m.setRelationshipCode(customerProfile.getRelationshipCode());
				memberDao.addMember(m);
				spaMemberSyncDao.addSpaMemberSyncWhenInsert(m);
				//Copy Member Limit Rule From Primary Member
				copyMemberLimitRuleFromPrimayMember(superiorMemberId,customerId,updateBy);
				
				//Copy Member Facility Right From Primary Member
				copyMemberFacilityRight(superiorMemberId,customerId);
				
				if (limitRule == null) {
					limitRule = setMemberLimitRuleTRN(BigDecimal.ZERO);
				}
				if (null != limitRule) {
					MemberLimitRule primayMemberCR = memberLimitRuleDao.getEffectiveMemberLimitRule(superiorMemberId, "CR");
					limitRule = setMemberLimitRule(limitRule, customerId, updateBy, currentDate, primayMemberCR!=null?primayMemberCR.getExpiryDate():null);
					memberLimitRuleDao.save(limitRule);
				}
				CustomerEmailContent contentDependent = customerEmailContentService.setEmailContentForMemberActivationEmail(customerId, EmailType.ACTIVATION.getFunctionId(), updateBy, fromName,true,nextAcademyNo,randomPassword);
				if(contentDependent!=null) mailThreadService.sendWithResponse(contentDependent, null, null, null);
			}
			
			
			//Import MG/HG. Upgrade Member Guest /House Guest to IDM or CDM
			if(member!=null&&(MemberType.MG.name().equals(member.getMemberType())||MemberType.HG.name().equals(member.getMemberType()))){
				
				String nextAcademyNo = getTheNextAvailableAcademyNo();
				String userId = getTheUserId(nextAcademyNo, customerId);
				boolean checkLoginId = userMasterDao.checkAvailableLoginId(customerProfile.getContactEmail(), userId);
				if (!checkLoginId)
					throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
				String randomPassword = CommUtil.generateRandomPassword();
				UserMaster user = createUserMaster(nextAcademyNo, customerId, updateBy, customerProfile.getSurname(), customerProfile.getGivenName(), randomPassword);
				boolean validateUserMaster = userMasterDao.saveOrUpdate(user);
				
				member.setUserId(userId);
				member.setAcademyNo(nextAcademyNo);
				if (memberType.IPM.name().equals(primaryMember.getMemberType())) {
					member.setMemberType("IDM");
				} else if (memberType.CPM.name().equals(primaryMember.getMemberType())) {
					member.setMemberType("CDM");
				}
				member.setStatus(Constant.General_Status_ACT);
				member.setFirstJoinDate(currentDate);
				member.setEffectiveDate(primaryMember.getEffectiveDate());
				member.setUpdateDate(currentDate);
				member.setUpdateBy(updateBy);
				member.setSuperiorMemberId(superiorMemberId);
				memberDao.update(member);
				
				//Copy Member Limit Rule From Primary Member
				copyMemberLimitRuleFromPrimayMember(superiorMemberId,customerId,updateBy);
				
				//Copy Member Facility Right From Primary Member
				copyMemberFacilityRight(superiorMemberId,customerId);
				
				if (limitRule == null) {
					limitRule = setMemberLimitRuleTRN(BigDecimal.ZERO);
				}
				if (null != limitRule) {
					MemberLimitRule primayMemberCR = memberLimitRuleDao.getEffectiveMemberLimitRule(superiorMemberId, "CR");
					limitRule = setMemberLimitRule(limitRule, customerId, updateBy, currentDate, primayMemberCR!=null?primayMemberCR.getExpiryDate():null);
					memberLimitRuleDao.save(limitRule);
				}
				
				CustomerEmailContent contentDependent = customerEmailContentService.setEmailContentForMemberActivationEmail(customerId, EmailType.ACTIVATION.getFunctionId(), updateBy, fromName,true,nextAcademyNo,randomPassword);
				if(contentDependent!=null) mailThreadService.sendWithResponse(contentDependent, null, null, null);
				
			}
			
			
			
		}else{
			if(null != limitRule){
				MemberLimitRule temp = memberLimitRuleDao.getEffectiveMemberLimitRule(customerProfile.getCustomerId(), limitRule.getLimitType());
				temp.setNumValue(limitRule.getNumValue());
				memberLimitRuleDao.update(temp);
			}
		}
		
		//move photo
		if(!StringUtils.isEmpty(customerProfile.getPortraitPhoto()) && !customerProfile.getPortraitPhoto().equals(profile.getPortraitPhoto())){
			String path = moveProfilePhoto(member.getUserId(), customerProfile.getPortraitPhoto());
			if(!StringUtils.isEmpty(path)){
				customerProfile.setPortraitPhoto(path);
			}
		}
		//move signature
		if(!StringUtils.isEmpty(customerProfile.getSignature()) && !customerProfile.getSignature().equals(profile.getSignature())){
			String path = moveProfilePhoto(member.getUserId(), customerProfile.getSignature());
			if(!StringUtils.isEmpty(path)){
				customerProfile.setSignature(path);
			}
		}
		
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	}
	
	private void saveOrUpdateOptionalOrFavor(CustomerProfile customerProfile,Long customerId,String createBy){
		if(null != customerProfile.getCustomerAdditionInfos() && customerProfile.getCustomerAdditionInfos().size()>0){
			customerAdditionInfoDao.deleteByCustomerId(customerProfile.getCustomerId()); 
			for(CustomerAdditionInfo cai : customerProfile.getCustomerAdditionInfos()){
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.parseLong(cai.getCaptionId()));
				cpk.setCustomerId(customerId);
				cai.setId(cpk);	
				cai.setCreateDate(new Date());
				cai.setCreateBy(createBy);
				if(StringUtils.isEmpty(cai.getCustomerInput())){
					continue;
				}
				cai.setUpdateBy(createBy);
				cai.setUpdateDate(new Date());
				customerAdditionInfoDao.saveOrUpdate(cai);
			}
		}
	}
	
	private void saveOrUpdateBillingAddress(CustomerProfile customerProfile){
		if(null != customerProfile.getCustomerAddresses() && customerProfile.getCustomerAddresses().size() > 0){
			CustomerAddress address = customerProfile.getCustomerAddresses().get(0);
			CustomerAddressPK apk = new CustomerAddressPK();
			apk.setAddressType(AddressType.BILL.name());
			apk.setCustomerId(customerProfile.getCustomerId());
			address.setId(apk);
			if("true".equalsIgnoreCase(customerProfile.getCheckBillingAddress())){
				if(StringUtils.isNotEmpty(customerProfile.getPostalAddress1())){
					address.setAddress1(customerProfile.getPostalAddress1());
				}
				if(StringUtils.isNotEmpty(customerProfile.getPostalAddress2())){
					address.setAddress2(customerProfile.getPostalAddress2());
				}
				address.setHkDistrict(customerProfile.getPostalDistrict());
			}
			
			if(StringUtils.isNotEmpty(address.getAddress1())||StringUtils.isNotEmpty(address.getAddress2())||StringUtils.isNotEmpty(address.getHkDistrict())){
				customerAddressDao.saveOrUpdate(address);
			}
		}
	}
	
	private String moveProfilePhoto(String userId, String filename){
		String basePath = "";
		try {
			basePath = FileUpload.getBasePath(FileUpload.FileCategory.USER);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File photoFile = new File(basePath + File.separator + userId);
		if(!photoFile.exists()){
			boolean isCreated = photoFile.mkdirs();
			if (!isCreated) {
				logger.error("Can't create folder!");
			}
		}
		boolean isSuccess = FileUtil.moveFile(basePath + filename, basePath + File.separator + userId + filename);
		if(isSuccess){
			return "/"+userId + filename;
		}else{
			return "";
		}
	}
	
	
	@Override
	@Transactional
	public ResponseResult getDepartMemberByCustomerId(String customerId)
			throws Exception {
		// TODO Auto-generated method stub
		
		if(StringUtils.isEmpty(customerId)){
			responseResult.initResult(GTAError.MemberShipError.SEARCH_DEPENDENTMEMBER_PARAMNULL);
			return responseResult;
		}
		CustomerProfile cp = customerProfileDao.getCustomerProfileByCustomerId(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS,cp);
		return responseResult;
	}
}
