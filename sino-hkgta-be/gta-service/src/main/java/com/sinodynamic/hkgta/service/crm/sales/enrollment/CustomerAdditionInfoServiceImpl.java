package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.memberapp.MemberAppCustomerAdditionInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CustomerAdditionInfoServiceImpl extends ServiceBase<CustomerAdditionInfo> implements
		CustomerAdditionInfoService {
    
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfo;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		return customerAdditionInfo.getCustomerAdditionInfo(t);
	}

	public void saveCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		customerAdditionInfo.saveCustomerAdditionInfo(t);

	}

	public void updateCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		customerAdditionInfo.updateCustomerAdditionInfo(t);

	}

	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		customerAdditionInfo.deleteCustomerAdditionInfo(t);

	}

	@Override
	@Transactional
	public ResponseResult editCustomerAdditionInfo(
			MemberAppCustomerAdditionInfoDto dto, String userId) {
		if(dto.getCustomerAdditionInfoDtos()!=null && dto.getCustomerAdditionInfoDtos().size() >0){
			for(CustomerAdditionInfoDto cust : dto.getCustomerAdditionInfoDtos()){
				CustomerAdditionInfo customerAdditionInfo = new CustomerAdditionInfo();
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.parseLong(cust.getCaptionId()));
				cpk.setCustomerId(dto.getCustomerId());
				customerAdditionInfo.setCustomerInput(cust.getCustomerInput());
				customerAdditionInfo.setId(cpk);
				customerAdditionInfo.setUpdateBy(userId);
				customerAdditionInfo.setUpdateDate(new Date());
				CustomerAdditionInfo targetEntity = customerAdditionInfoDao.get(CustomerAdditionInfo.class, cpk);
				if(targetEntity != null){
					String[] ignoreFileds = new String[]{"id","createBy","createDate","sysId"};
					BeanUtils.copyProperties(customerAdditionInfo, targetEntity,ignoreFileds);
					customerAdditionInfoDao.saveOrUpdate(targetEntity);
				}else{
					customerAdditionInfoDao.save(customerAdditionInfo);
				}
			}
		}
		return new ResponseResult("0","Update success!");
	}

}
