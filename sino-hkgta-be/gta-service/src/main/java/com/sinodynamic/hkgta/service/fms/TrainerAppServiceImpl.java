package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.fms.CourseSessionDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.dao.fms.FacilitySubTypeDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityAttendanceDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.TrainerAppDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.crm.TrainingRecordDto;
import com.sinodynamic.hkgta.dto.staff.CoachMonthyAchivementDto;
import com.sinodynamic.hkgta.dto.staff.CoachReservationDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityName;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class TrainerAppServiceImpl extends ServiceBase implements
		TrainerAppService {
	
	@Autowired
	TrainerAppDao trainerAppDao;
	
	@Autowired
	MemberFacilityAttendanceDao memberFacilityAttendanceDao;
	@Autowired
	CourseSessionDao courseSessionDao;
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	@Autowired
	MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	@Autowired
	private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;
	@Autowired
	private FacilityAttributeCaptionDao facilityAttributeCaptionDao;
	
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private MessageTemplateDao templateDao;
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
    private ShortMessageService  smsService;
	
	@Autowired
    private AdvanceQueryService  advanceQueryService;
	@Autowired
	private FacilitySubTypeDao facilitySubTypeDao;
	@Override
	@Transactional
	public Data getReservationList(final String coachId,
			final String orderColumn, final String order, final int pageSize, final int currentPage,
			final AdvanceQueryDto filters) {
		
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		Data result = (Data)exe.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				ListPage<CoachReservationDto> listPage = trainerAppDao.reservationList(coachId, orderColumn, order, pageSize, currentPage, null);
				Data data = new Data();
				data.setCurrentPage(currentPage);
				data.setPageSize(pageSize);
				data.setTotalPage(listPage.getAllPage());
				data.setLastPage(currentPage==listPage.getAllPage());
				data.setRecordCount(listPage.getAllSize());
				
				data.setList(listPage.getDtoList());
				return data;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{getException().toString()});
			}
			
			
		});
		
		return result;
	}
	
	@Override
	@Transactional
	public Data getReservationListByDate(final String coachId,
			final String orderColumn, final String order, final int pageSize, final int currentPage,
			final String resvDate) {
		
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		Data result = (Data)exe.execute(new AbstractCallBack(){

			@SuppressWarnings("unchecked")
			@Override
			public Object doTry() throws Exception {
				String filterSql = ""; 
				if(resvDate.length()==7)
				{
					filterSql =  "WHERE DATE_FORMAT(staffBegin, '%Y-%m') = '" + resvDate +"'";
				}
				else
				{
					filterSql = "WHERE DATE_FORMAT(staffBegin, '%Y-%m-%d') = '" + resvDate +"'";
				}
				
				ListPage<CoachReservationDto> listPage = trainerAppDao.reservationList(coachId, orderColumn, order, pageSize, currentPage, filterSql);
				
				Data data = new Data();
				data.setCurrentPage(currentPage);
				data.setPageSize(pageSize);
				data.setTotalPage(listPage.getAllPage());
				data.setLastPage(currentPage==listPage.getAllPage());
				data.setRecordCount(listPage.getAllSize());
				List<CoachReservationDto> list = new ArrayList<CoachReservationDto>();
				List<Object> tmp = listPage.getDtoList();
				for(Object obj : tmp)
				{
					list.add((CoachReservationDto) obj);
				}
				Collections.sort(list);
				data.setList(list);
//				data.setList(listPage.getDtoList());
				
				return data;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{getException().toString()});
			}
			
			
		});
		
		return result;
	}


	/**   
	* @author: Zero_Wang
	* @since: Sep 7, 2015
	* 
	* @description
	* this method will do follow two things
	* private coach Auto send MSM remind member InAdvance1Hour
	* private coach Auto push msg remind coach InAdvance1Hour
	*/  
	@Override
	@Transactional
	public void autoRemindInAdvance1Hour() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		Timestamp begin = new Timestamp(calendar.getTimeInMillis());
		String hour = this.appProps.getProperty("advance.remind.coach");
		calendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
		Timestamp end = new Timestamp(calendar.getTimeInMillis());
		String hql = "FROM  MemberFacilityTypeBooking m where  beginDatetimeBook between ? and ?  and status<> 'CAN' ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(begin);
		param.add(end);
		List<MemberFacilityTypeBooking> list =this.memberFacilityTypeBookingDao.getByHql(hql, param);
		for(MemberFacilityTypeBooking booking : list){
			
			sendRemindSMS2Member(booking);
			
			pushRemindMsg2Coach(booking);
		}
	}
	
	@Override
	@Transactional
	public void pushRemindMsg2Coach(MemberFacilityTypeBooking booking) {
		CustomerProfile cp = customerProfileDao.getById(booking.getCustomerId());
		String username = cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname();
		MessageTemplate messageTemplate = templateDao.getTemplateByFunctionId("app_coach_notice");
		String facilityName	= "";
		if("GOLF".equals(booking.getResvFacilityType())){
			facilityName = "golf";
		}else if("TENNIS".equals(booking.getResvFacilityType())){
			facilityName = "tennis";
		}
		String content = messageTemplate.getContent();
		content = content
				.replaceAll(MessageTemplateParams.UserName.getParam(), username);
		messageTemplate.setContent(content);
		content = assemblyContent(booking, messageTemplate, facilityName);
		logger.error("TrainerAppServiceImpl  pushRemindMsg2Coach run start ...resvId:"+booking.getResvId());
		devicePushService.pushMessage(new String[]{booking.getExpCoachUserId()},content, "trainer");
		logger.error("TrainerAppServiceImpl  pushRemindMsg2Coach run end ...resvId:"+booking.getResvId());
		
	}
	@Override
	@Transactional
	public void sendRemindSMS2Member(MemberFacilityTypeBooking booking) {
		List<String> phonenumbers = new ArrayList<String>();
		CustomerProfile customerProfile = customerProfileDao.getById(booking.getCustomerId());
		phonenumbers.add(customerProfile.getPhoneMobile());
		MessageTemplate messageTemplate = null;
		String facilityName	= "";
		if("GOLF".equals(booking.getResvFacilityType())){
			facilityName = "golf";
			messageTemplate = templateDao.getTemplateByFunctionId("coach_g");
		}else if("TENNIS".equals(booking.getResvFacilityType())){
			facilityName = "tennis";
			messageTemplate = templateDao.getTemplateByFunctionId("coach_t");
		}
		String content = assemblyContent(booking, messageTemplate, facilityName);
		try {
			logger.error("TrainerAppServiceImpl  sendRemindSMS2Member run start ...resvId:"+booking.getResvId());
			smsService.sendSMS(phonenumbers, content, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
			logger.error("TrainerAppServiceImpl  sendRemindSMS2Member run end ...resvId:"+booking.getResvId());
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	private String assemblyContent(MemberFacilityTypeBooking booking,
			MessageTemplate messageTemplate, String facilityName) {
		String temp = booking.getBeginDatetimeBook().toString();
		String startTime = temp.substring(0, temp.indexOf("."));
		String bookingDate = startTime.substring(0,10);
		startTime = startTime.substring(11,16);
		temp = booking.getEndDatetimeBook().toString();
		String endTime = temp.substring(0, temp.indexOf("."));
		endTime = endTime.substring(11,16);
		String templateContent = messageTemplate.getContent();
		Long timeLeft = (booking.getBeginDatetimeBook().getTime()-System.currentTimeMillis())/60/1000;
		
		/*
		Your  Private Coaching booking will be in {timeLeft} minutes. {bookingDate} {startTime} {facilityType} {facilityTypeQty} HKGTA
		 */
		String content = templateContent
				.replaceAll(MessageTemplateParams.BookingDate.getParam(), bookingDate)
				.replaceAll(MessageTemplateParams.StartTime.getParam(), startTime)
				.replaceAll(MessageTemplateParams.EndTime.getParam(), endTime)
				.replaceAll(MessageTemplateParams.TimeLeft.getParam(), String.valueOf(timeLeft))
				.replaceAll(MessageTemplateParams.FacilityType.getParam(), facilityName)
				.replaceAll(MessageTemplateParams.FacilityTypeQty.getParam(), Long.toString(booking.getFacilityTypeQty()));
		content = content.replace("{", "");
		content = content.replace("}", "");
		return content;
	}

	/**   
	* @author: Zero_Wang
	* @since: Aug 27, 2015
	* 
	* @description
	*	Auto push message to coach in adavance 2 days
	*/  
	@Override
	@Transactional
	public void autoPushMsgInAdvance2Day() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String day = this.appProps.getProperty("advance.pushmessage.coach");
		calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
		Timestamp end = new Timestamp(calendar.getTimeInMillis());
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Timestamp begin = new Timestamp(calendar.getTimeInMillis());
		String hql = "FROM  MemberFacilityTypeBooking m where  beginDatetimeBook between ? and ? and endDatetimeBook between ? and ? and status='RSV'";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(begin);
		param.add(end);
		param.add(begin);
		param.add(end);
		List<MemberFacilityTypeBooking> list =this.memberFacilityTypeBookingDao.getByHql(hql, param);
		for(MemberFacilityTypeBooking booking : list){
			MessageTemplate template = null;
			String templateContent = "";
			String content = "";
			String facilityName	= "";
			String caption ="";
			if (null != booking && Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(booking.getResvFacilityType())){
				MemberFacilityBookAdditionAttr attr = memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttrList(booking.getResvId()).get(0);
				FacilityAttributeCaption facilityAttributeCaption = this.facilityAttributeCaptionDao.get(FacilityAttributeCaption.class, attr.getId().getAttributeId());
				caption = facilityAttributeCaption.getCaption();
			}else if(null != booking && Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(booking.getResvFacilityType())){
				com.sinodynamic.hkgta.entity.fms.FacilitySubType facilitySubType = facilitySubTypeDao.getById(booking.getFacilitySubtypeId());
				caption = facilitySubType.getName();
			}
			if("GOLF".equals(booking.getResvFacilityType())){
				template = templateDao.getTemplateByFunctionId("golf_coach_auto");
				if (template == null) {
					throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching auto remind: golf_coach_auto"});
				}
				templateContent = template.getContent();
				facilityName = "golf";
			}else if("TENNIS".equals(booking.getResvFacilityType())){
				template = templateDao.getTemplateByFunctionId("tennis_coach_auto");
				if (template == null) {
					throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching auto remind: tennis_coach_auto"});
				}
				templateContent = template.getContent();
				facilityName = "tennis";
			}
			CustomerProfile cp = customerProfileDao.getById(booking.getCustomerId());
			if (cp == null) {
				throw new GTACommonException(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_CUSTOMER_PROFILE);
			}
			String salutation = cp.getSalutation();
			String lastName = cp.getSurname();
			Long confirmation = booking.getOrderNo();
			String fullName = getName(cp);
			//Timestamp's toString() String object in yyyy-mm-dd hh:mm:ss.fffffffff format
			String temp = booking.getBeginDatetimeBook().toString();
			String startTime = temp.substring(0, temp.indexOf("."));
			String bookingDate = startTime.substring(0,10);
			startTime = startTime.substring(11,16);
			temp = booking.getEndDatetimeBook().toString();
			String endTime = temp.substring(0, temp.indexOf("."));
			endTime = endTime.substring(11,16);
		
			/**
			Tennis
			Dear {username} your private tennis coach booking information is: {bookingDate} {startTime}-{endTime} facility type: {tennisCourtType} HKGTA
			Golf
			Dear {username} your private golf coach booking information is: {bookingDate} {startTime}-{endTime} facility type: {golfBayType} HKGTA
			
			 * 
			 */
			content = templateContent
					.replaceAll(MessageTemplateParams.BookingDate.getParam(), bookingDate)
					.replaceAll(MessageTemplateParams.UserName.getParam(), fullName)
					.replaceAll(MessageTemplateParams.StartTime.getParam(), startTime)
					.replaceAll(MessageTemplateParams.EndTime.getParam(), endTime);
			if(FacilityName.GOLF.name().equals(booking.getResvFacilityType())){
				content = content.replaceAll(MessageTemplateParams.BayType.getParam(), caption);
			}else if(FacilityName.TENNIS.name().equals(booking.getResvFacilityType())){
				content = content.replaceAll(MessageTemplateParams.TennisType.getParam(), caption);
			}
			content = content.replace("{", "");
			content = content.replace("}", "");
			logger.debug("TrainerAppServiceImpl  autoPushMsgInAdvance2Day run start ...resvId:"+booking.getResvId());
			devicePushService.pushMessage(new String[]{booking.getCustomerId()+""}, template.getMessageSubject(), content, "trainer");
			logger.debug("TrainerAppServiceImpl  autoPushMsgInAdvance2Day run end ...resvId:"+booking.getResvId());
			
		}
	}	
	private String getName(CustomerProfile cp) {
		if (cp == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName()).append(" ").append(cp.getSurname());
		return sb.toString();
	}
	private int hours(Object value){
		return value == null ? 0 : Integer.parseInt(value.toString());
	}
	
	@Override
	@Transactional
	public CoachMonthyAchivementDto calculateCoachAchievement(
			final Map<String, Object> params) {
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		CoachMonthyAchivementDto result = (CoachMonthyAchivementDto)exe.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				CoachMonthyAchivementDto achievement = new CoachMonthyAchivementDto();
				
				Object coaching = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.HOUR_OF_CLASSES, params);
				Object course = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.HOUR_OF_COURSES, params);
				Object students = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.TOTAL_OF_STUDENTS, params);
				Object newStudents = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.TOTAL_OF_NEW_STUDENTS, params);
				achievement.setSumClassesHours(new Long(hours(coaching)));
				achievement.setSumCourseHours(new Long(hours(course)));
				achievement.setSumNewStudent(new Long(hours(newStudents)));
				achievement.setSumStudent(new Long(hours(students)));
				
				return achievement;
			}
			
		});
		return result;
	}


	@Override
	@Transactional
	public ResponseResult rollCallforCoaching(Long resvId, String qrCode) throws Exception
	{
		MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneByResvId(resvId);
		if (null == attendance)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.CHECKING_RECORD_CANNOT_FIND, new Object[] { resvId });
		}
		
		Date NOW = new Date();
		MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);
		if(NOW.before(booking.getBeginDatetimeBook()))
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.ROLLCALL_LATER);
		}
		
		ScanCardMappingDto memberInfo = new ScanCardMappingDto();
		if (qrCode.length()==7)
		{
			Member member = memberDao.getMemberByAcademyNo(qrCode);
			if (null == member || !member.getCustomerId().equals(attendance.getId().getCustomerId()))
			{
				throw new GTACommonException(GTAError.PrivateCoachingError.ROLLCALL_FOR_WRONG_MEMBER);
			}
			CustomerProfile p = customerProfileDao.getCustomerProfileByCustomerId(member.getCustomerId());
			memberInfo.setAcademyNo(member.getAcademyNo());
			memberInfo.setPortraitPhoto(p.getPortraitPhoto());
			memberInfo.setGivenName(p.getGivenName());
			memberInfo.setSurname(p.getSurname());
			memberInfo.setSalutation(p.getSalutation());
			memberInfo.setCustomerId(member.getCustomerId());
		}
		else
		{
			ResponseResult tmpResult = advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(qrCode)));
			if (null == tmpResult.getDto() || !((ScanCardMappingDto) tmpResult.getDto()).getCustomerId().equals(attendance.getId().getCustomerId()))
			{
				throw new GTACommonException(GTAError.PrivateCoachingError.ROLLCALL_FOR_WRONG_MEMBER);
			}
			memberInfo = (ScanCardMappingDto)tmpResult.getDto();
		}
		
		
		if (attendance.getAttendTime() == null)
		{
			attendance.setAttendTime(new Timestamp(new Date().getTime()));
			attendance.setMachineId("ROLLCALL");
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, memberInfo);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult hasRollCall(Long resvId) throws Exception
	{
		MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneByResvId(resvId);
		
		if (null == attendance)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.CHECKING_RECORD_CANNOT_FIND, new Object[] { resvId.toString() });
		}
		
		Map<String,String> result = new HashMap<String,String>();
		result.put("hasRollCall", attendance.getAttendTime() != null ? "true" : "");
		
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}



	/**   
	* @author: Zero_Wang
	* @since: Aug 31, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public List<CustomerProfileDto> getPrivateCoachTrainedMemberList(
			String staffNo, String isAscending) {
		
		return this.trainerAppDao.getPrivateCoachTrainedMemberList(staffNo, isAscending);
	}
	@Override
	@Transactional
	public ListPage<TrainingRecordDto> getTrainingRecord(ListPage<TrainingRecordDto> pListPage, Long customerId, String coachId, String sortBy, String isAscending, String filterBy) throws Exception {
		
		StringBuffer hql = new StringBuffer();
		StringBuffer conditionHql = new StringBuffer();
		

		hql.append("SELECT atte.facility_timeslot_id as facilityTimeslotId, ")
					.append("atte.customer_id as customerId, ")
			        .append("atte.attend_time as attendTime, ")
			        .append("DATE_FORMAT(booking.begin_datetime_book,'%Y/%m/%d %H:%i') AS beginBookTime, ")
			        .append("DATE_FORMAT(DATE_ADD(booking.end_datetime_book, INTERVAL 1 SECOND), '%Y/%m/%d %H:%i')AS endBookTime, ")
			        .append("concat( sp.given_name,' ', sp.surname) coachName, ")
			        .append("atte.trainer_comment AS comment, ")
			        .append(" booking.resv_facility_type AS facilityType , ")
			        .append("atte.score AS score ");
		conditionHql.append("FROM member_facility_attendance atte  ")
			.append("LEFT JOIN member_reserved_facility res on res.facility_timeslot_id = atte.facility_timeslot_id ")
			.append("LEFT JOIN member_facility_type_booking booking on res.resv_id = booking.resv_id ")
			.append("LEFT JOIN staff_profile sp on sp.user_id = booking.exp_coach_user_id ")
			.append("WHERE atte.attend_time is not null ");
		
		List<Serializable> param = new ArrayList<Serializable>();
		if (null != customerId)
		{
			conditionHql.append(" AND atte.customer_id = ? ");
			param.add(customerId);
		}
		
		if (!StringUtils.isEmpty(coachId))
		{
			conditionHql.append(" AND booking.exp_coach_user_id = ? ");
			param.add(coachId);
		}
		if (!StringUtils.isEmpty(filterBy)){
			
			conditionHql.append(" AND booking.resv_facility_type = ? ");
			param.add(filterBy);
		}
		
		String queryHql ="Select * from (" + hql.append(conditionHql.toString()).toString() + ") t where 1=1 ";
		
		if (!StringUtils.isEmpty(sortBy)) {
			if(isAscending.equals("false")){
				pListPage.addDescending("t." + sortBy);
			}
			else {
				pListPage.addAscending("t." + sortBy);
			}
		}
		
		//pListPage.addDescending("atte.attend_time");
		StringBuffer countHql = new StringBuffer("SELECT count(1) from (");
		countHql.append(queryHql.toString()).append(") c");
		
		return  memberFacilityAttendanceDao.getTrainingRecordList(pListPage, param, countHql.toString(), queryHql);

	}


	@Override
	@Transactional
	public ResponseResult getTrainingComments(Long timeslotId, Long customerId) throws Exception
	{
		//MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneById(timeslotId, customerId);
		List<TrainingRecordDto> trainingRecordDtos = memberFacilityAttendanceDao.getTrainingCommentDetail(timeslotId, customerId);
		TrainingRecordDto trainingRecordDto = null;
		if (null != trainingRecordDtos && trainingRecordDtos.size() > 0){
			trainingRecordDto = trainingRecordDtos.get(0);
		}
		
		if (null == trainingRecordDto)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.NO_TRAINING_RECORD);
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, trainingRecordDto);
		return responseResult;
	}


	@Override
	@Transactional
	public ResponseResult updateTrainingComments(Long timeslotId, Long customerId, Double score, String comments, String assignment) throws Exception
	{
		MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneById(timeslotId, customerId);
		
		if (null == attendance)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.NO_TRAINING_RECORD);
		}
		
		attendance.setScore(score);
		attendance.setAssignment(assignment);
		attendance.setTrainerComment(comments);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
		
	}


	/**   
	* @author: Zero_Wang
	* @since: Sep 1, 2015
	* 
	* @description
	* write the description here
	*/  
	@Transactional
	@Override
	public List<CustomerProfileDto> getPrivateCoachTrainedMemberListByName(
			String staffNo, String name, String isAsending) {
		
		return this.trainerAppDao.getPrivateCoachTrainedMemberListByName(staffNo, name, isAsending);
	}

	



}
