package com.sinodynamic.hkgta.service.pms;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.RoomTypeDao;
import com.sinodynamic.hkgta.dao.pms.RoomTypePicDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.pms.RoomPicPathDto;
import com.sinodynamic.hkgta.dto.pms.RoomRateDto;
import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.pms.RoomPicPath;
import com.sinodynamic.hkgta.entity.pms.RoomType;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.AppType;
import com.sinodynamic.hkgta.util.constant.Constant.Terminal;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RoomTypeServiceImpl extends ServiceBase<RoomType> implements RoomTypeService {
	
	private final static String THUMBNAIL_SUFFIX = "_small";
	
	@Autowired
	private RoomTypeDao roomTypeDao;
	
	@Autowired
	private RoomTypePicDao roomTypePicDao;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;

	@Override
	@Transactional
	public List<RoomTypeDto> getRoomListByTypes(String roomType)
	{
		List<RoomTypeDto> roomList = null;
		roomList = roomTypeDao.getRoomListByTypes(roomType);
		setRoomPics(roomList);
		for(RoomTypeDto room : roomList)
		{
			if (room.getStatus().equalsIgnoreCase("DEL")) 
			{
				room.setRoomPics(null);
			}
		}
		
		return roomList;
	}
	
	@Override
	@Transactional
	public void setRoomPics(List<RoomTypeDto> roomList)
	{
		for(RoomTypeDto room : roomList)
		{
			List<RoomPicPathDto> roomPics = roomTypeDao.getRoomPics(room.getRoomTypeCode());
			room.setRoomPics(roomPics);
		}
	}

	@Override
	public void setRoomPrice(List<RoomTypeDto> roomList, Map<String, Double> roomPrice)
	{
		for(String roomType : roomPrice.keySet())
		{
			RoomTypeDto tmpRoom = new RoomTypeDto();
			tmpRoom.setRoomTypeCode(roomType);
			if (!roomList.contains(tmpRoom)) continue;
			
//			tmp = rooms.get(rooms.indexOf(tmp));
			
			roomList.get(roomList.indexOf(tmpRoom)).setRoomPrice(roomPrice.get(roomType));
			RoomTypeDto source = roomList.get(roomList.indexOf(tmpRoom));
			
			System.out.println(source.getRoomTypeCode() + ":" + source.getRoomPrice());
			
		}
		
	}

	@Override
	@Transactional
	public ResponseResult createRoomType(RoomTypeDto room, String createBy) throws Exception
	{
		RoomType tmpRoom = roomTypeDao.getRoomTypeByCode(room.getRoomTypeCode());
		
		if (tmpRoom!=null && "ACT".equals(tmpRoom.getStatus()))
		{
			throw new GTACommonException(GTAError.CommonError.Error, "The RoomType has already exist!");
		}
		
//		if (tmpRoom!=null && "DEL".equals(tmpRoom.getStatus()))
//		{
//			updateRoomType(room, createBy);
//		}
		
		RoomType newRoom = new RoomType();
		newRoom.setTypeName(room.getRoomTypeName());
		newRoom.setNumOfGuest(room.getNumOfGuest());
		newRoom.setAmenity(room.getAmenity());
		newRoom.setDescription(room.getRoomTypeDescription());
		newRoom.setTypeCode(room.getRoomTypeCode().toUpperCase());
		newRoom.setCreateBy(createBy);
		newRoom.setCreateDate(new Date());
		newRoom.setStatus("ACT");
		roomTypeDao.save(newRoom);
		saveRoomTypeItemPrice(room.getRoomTypeCode());
		roomTypePicDao.saveRoomPic(room.getRoomPics());
		moveRoomPics(room.getRoomPics());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public void saveRoomTypeItemPrice(String roomTypeCode)
	{
		PosServiceItemPrice item = posServiceItemPriceDao.get(PosServiceItemPrice.class, Constant.PMS_ROOMTYPE_ITEM_PREFIX + roomTypeCode);
		if (null == item)
		{
			PosServiceItemPrice roomTypeItemPrice = new PosServiceItemPrice();
			roomTypeItemPrice.setItemNo(Constant.PMS_ROOMTYPE_ITEM_PREFIX + roomTypeCode);
			roomTypeItemPrice.setItemCatagory(Constant.PriceCatagory.PMS_ROOM.toString());
			roomTypeItemPrice.setDescription("Guest Room - " + roomTypeCode);
			roomTypeItemPrice.setStatus(Constant.Status.ACT.toString());
			roomTypeItemPrice.setItemPrice(BigDecimal.ZERO);
			posServiceItemPriceDao.save(roomTypeItemPrice);
		}
	}

	private void moveRoomPics(List<RoomPicPathDto> roomPics) throws Exception
	{
		if (null == roomPics) return;
		String basePath = "";
		
		basePath = FileUpload.getBasePath(FileUpload.FileCategory.GUESTROOM);
		
		File photoFile = new File(basePath + File.separator + roomPics.get(0).getRoomTypeCode());
		if(!photoFile.exists()){
			boolean isCreated = photoFile.mkdirs();
			if (!isCreated) {
				logger.error("Can't create folder!");
				return;
			}
		}
		
		for(RoomPicPathDto pic : roomPics)
		{
			File file = new File(basePath + File.separator + pic.getServerFilename());
			if (!file.exists()) continue;
			FileUtil.moveFile(basePath + File.separator + pic.getServerFilename(), basePath + File.separator + pic.getRoomTypeCode() + File.separator + pic.getServerFilename());
			
			String[] tmp = pic.getServerFilename().split("\\.");
			String thumbnail = tmp[0] + THUMBNAIL_SUFFIX + "." + tmp[1];
			FileUtil.moveFile(basePath + File.separator + thumbnail, basePath + File.separator + pic.getRoomTypeCode() + File.separator + thumbnail);
		}
		
	}

	@Override
	@Transactional
	public ResponseResult updateRoomType(RoomTypeDto room, String updateBy) throws Exception
	{
		RoomType tmpRoom = roomTypeDao.getRoomTypeByCode(room.getRoomTypeCode());
		
		if (tmpRoom==null)
		{
			throw new GTACommonException(GTAError.CommonError.Error, "Room Type not exist!");
		}
		
		tmpRoom.setTypeName(room.getRoomTypeName());
		tmpRoom.setNumOfGuest(room.getNumOfGuest());
		tmpRoom.setAmenity(room.getAmenity());
		tmpRoom.setDescription(room.getRoomTypeDescription());
		tmpRoom.setUpdateBy(updateBy);
		tmpRoom.setUpdateDate(new Date());
		tmpRoom.setStatus("ACT");
		roomTypeDao.saveOrUpdate(tmpRoom);
		roomTypePicDao.updateRoomPic(room.getRoomPics());
		pricessRoomPics(room.getRoomPics());
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	private void pricessRoomPics(List<RoomPicPathDto> originPics)
	{
		String root = appProps.getProperty(FileUpload.FileCategory.GUESTROOM.getConfigName());
		for(RoomPicPathDto pic : originPics)
		{
			if ("DEL".equalsIgnoreCase(pic.getAction()))
			{
				File file = new File(root + File.separator + pic.getRoomTypeCode() + File.separator + pic.getServerFilename());
				if (file.exists())
				{
					file.delete();
				}
				
				String[] tmp = pic.getServerFilename().split("\\.");
				String thumbnail = tmp[0] + THUMBNAIL_SUFFIX + "." + tmp[1];
				File thumbnailpic = new File(root + File.separator + pic.getRoomTypeCode() + File.separator + thumbnail);
				if (thumbnailpic.exists())
				{
					thumbnailpic.delete();
				}
				continue;
			}
			
			if ("NEW".equalsIgnoreCase(pic.getAction()))
			{
				FileUtil.moveFile(root + File.separator + pic.getServerFilename(), root + File.separator + pic.getRoomTypeCode() + File.separator + pic.getServerFilename());
				String[] tmp = pic.getServerFilename().split("\\.");
				String thumbnail = tmp[0] + THUMBNAIL_SUFFIX + "." + tmp[1];
				FileUtil.moveFile(root + File.separator + pic.getServerFilename(), root + File.separator + pic.getRoomTypeCode() + File.separator + thumbnail);
			}
		}
	}

	@Override
	@Transactional
	public RoomPicPath getRoomPicByPicId(Long picId) throws Exception
	{
		return roomTypePicDao.get(RoomPicPath.class, picId);
	}

	@Override
	@Transactional
	public ResponseResult getRoomTypeByCode(String roomTypeCode) throws Exception
	{
		List<RoomTypeDto> list = roomTypeDao.getRoomListByTypes(roomTypeCode);
		
		if (null == list || list.size() == 0)
		{
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_NOT_EXIST);
		}
		
		RoomTypeDto room = list.get(0);
		List<RoomPicPathDto> roomPics = roomTypeDao.getRoomPics(room.getRoomTypeCode());
		room.setRoomPics(roomPics);
		
		responseResult.initResult(GTAError.Success.SUCCESS, room);
		return responseResult;

	}
	
	@Override
	@Transactional
	public ResponseResult retrieveGuestRoom(String roomTypeCode) throws Exception
	{
		RoomType tmpRoom = roomTypeDao.getRoomTypeByCode(roomTypeCode);
		
		if (null == tmpRoom)
		{
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_NOT_EXIST);
		}
		
		if ("ACT".equalsIgnoreCase(tmpRoom.getStatus()))
		{
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_ALREADY_ACTIVE);
		}
		
		if ("DEL".equalsIgnoreCase(tmpRoom.getStatus()))
		{
			tmpRoom.setStatus("ACT");
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}
	
	@Override
	public String getRoomList() {
		return roomTypeDao.getRoomTypeList();
		
	}

	@Override
	@Transactional
	public List<RoomTypeDto> getRoomListByTypes(RoomStayDto roomType)
	{
		return getRoomListByTypes(roomType, Terminal.WEB, AppType.MEMBER);
	}

	RoomTypeDto getRoomTypeFromList(List<RoomTypeDto> oasisRoomTypeList, String roomTypeCode){
		RoomTypeDto tmpRoom = new RoomTypeDto(roomTypeCode);
		return oasisRoomTypeList.get(oasisRoomTypeList.indexOf(tmpRoom));
	}
	
	List<RoomTypeDto> _getRoomListByTypes4Memeber(RoomStayDto roomType){
		List<RoomTypeDto> roomTypes = roomType.getRoomTypes();
		List<RoomRateDto> roomRates = roomType.getRoomRates();
		StringBuffer roomTypeCodes = new StringBuffer();

		Map<String, Double> roomPrices = new HashMap<String, Double>();
		for (RoomRateDto roomRate : roomRates)
		{
			if (!"ONLRACK".equalsIgnoreCase(roomRate.getRatePlanCode())) continue;
			roomPrices.put(roomRate.getRoomTypeCode(), Double.parseDouble(roomRate.getAmountAfterTax()));
			roomTypeCodes.append(",").append(roomRate.getRoomTypeCode());
		}
		
		List<RoomTypeDto> tmpRooms = new ArrayList<RoomTypeDto>();
		if (StringUtils.isEmpty(roomTypeCodes.toString())) return null;
		for(String roomCode : roomTypeCodes.toString().substring(1).split(","))
		{
			RoomTypeDto tmpRoom = new RoomTypeDto(roomCode);
			tmpRooms.add(roomTypes.get(roomTypes.indexOf(tmpRoom)));
		}
		setRoomPrice(tmpRooms, roomPrices);
		
		List<RoomTypeDto> rooms = getRoomListByTypes(roomTypeCodes.toString().substring(1));
		setRoomPrice(rooms, roomPrices);
		
		for(RoomTypeDto tmp : rooms)
		{
			tmpRooms.remove(tmp);
			tmpRooms.add(tmp);
		}
		
		return tmpRooms;
	}
	
	List<RoomTypeDto> _getRoomListByTypes4Staff(RoomStayDto roomType){
		List<RoomTypeDto> oasisRoomTypes = roomType.getRoomTypes();
		List<RoomRateDto> oasisRoomRates = roomType.getRoomRates();
		
		List<String> oasisRoomTypeCodes = new ArrayList<String>();
		Map<RoomRateDto, RoomTypeDto> oasisRoomRateRoomTypeMap = new HashMap<RoomRateDto, RoomTypeDto>();
		
		
		for (RoomRateDto roomRate : oasisRoomRates)
		{
			oasisRoomTypeCodes.add(roomRate.getRoomTypeCode());
			oasisRoomRateRoomTypeMap.put(roomRate, getRoomTypeFromList(oasisRoomTypes, roomRate.getRoomTypeCode()));
		}
		
		Map<RoomTypeDto, List<RoomRateDto>> roomTypeRoomRatesMap = new HashMap<RoomTypeDto, List<RoomRateDto>>();
		
		if (oasisRoomRateRoomTypeMap.isEmpty()) return null;
		for(RoomRateDto roomRate : oasisRoomRateRoomTypeMap.keySet())
		{
			
			RoomTypeDto roomTypeDto = oasisRoomRateRoomTypeMap.get(roomRate);
			
			List<RoomRateDto> originRoomRateList = roomTypeRoomRatesMap.get(roomTypeDto);
			if(originRoomRateList != null){
				if(!originRoomRateList.contains(roomRate)){
					originRoomRateList.add(roomRate);
				}
			}else{
				List<RoomRateDto> roomRates = new ArrayList<RoomRateDto>();
				roomRates.add(roomRate);
				roomTypeRoomRatesMap.put(roomTypeDto, roomRates);
			}
		}
		
		for(RoomTypeDto roomTypeDto : roomTypeRoomRatesMap.keySet()){
			roomTypeDto.setRoomRates(roomTypeRoomRatesMap.get(roomTypeDto));
		}
		List<RoomTypeDto> result = new ArrayList<RoomTypeDto>(roomTypeRoomRatesMap.keySet());
		
		List<RoomTypeDto> dataBaseRoomTypes = getRoomListByTypes(StringUtils.join(oasisRoomTypeCodes, ","));
		
		for(RoomTypeDto dataBaseRoomType : dataBaseRoomTypes)
		{
			
			RoomTypeDto originOasisRoomType = getRoomTypeFromList(result, dataBaseRoomType.getRoomTypeCode());
			dataBaseRoomType.setRoomRates(originOasisRoomType.getRoomRates());
			result.remove(dataBaseRoomType);
			result.add(dataBaseRoomType);
		}
		
		return result;
	}
	
	@Override
	@Transactional
	public List<RoomTypeDto> getRoomListByTypes(RoomStayDto roomType, Terminal terminal, AppType appType){
		if(AppType.MEMBER.equals(appType)){
			return _getRoomListByTypes4Memeber(roomType);
		}else{
			return _getRoomListByTypes4Staff(roomType);
		}
	}
	
	@Override
	@Transactional
	public void deleteRoomType(String roomTypeCode, String updateBy)
	{
		RoomType tmpRoom = roomTypeDao.getRoomTypeByCode(roomTypeCode);
		
		if (tmpRoom==null)
		{
			throw new GTACommonException(GTAError.CommonError.Error, "Room Type not exist!");
		}
		
		tmpRoom.setUpdateBy(updateBy);
		tmpRoom.setUpdateDate(new Date());
		tmpRoom.setStatus("DEL");
	}

	
}
