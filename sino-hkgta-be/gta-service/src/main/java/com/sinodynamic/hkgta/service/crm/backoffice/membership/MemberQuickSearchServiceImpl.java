package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Service
public class MemberQuickSearchServiceImpl extends ServiceBase<CustomerProfile> implements MemberQuickSearchService{
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private MemberDao memberDao;
	
	
	@Transactional
	public ResponseResult quickSeachMember(String number) {
		List<CustomerProfile> customer =customerProfileDao.quickSearchCustomerIdByAcademyIdOrPassportNo(number);
		if (customer == null || customer.size() == 0)
		{
			responseResult.initResult(GTAError.CommomError.GET_DATA_FAIL, "Member not found");
			return responseResult;
		}
		
		Map<String, String> resultMap = new HashMap<String, String>();
		
		CustomerProfile cp = customer.get(0);
		resultMap.put("customerId", cp.getCustomerId().toString());
		resultMap.put("salutation", cp.getSalutation());
		resultMap.put("givenName", cp.getGivenName());
		resultMap.put("surname", cp.getSurname());
		resultMap.put("memberType", cp.getMemberType());
		resultMap.put("memberName", cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname());
		resultMap.put("academyNo", cp.getAcademyNo());
		//member
		Member m = memberDao.getMemberByCustomerId(cp.getCustomerId());
		resultMap.put("status", m.getStatus());
		
		responseResult.initResult(GTAError.CommomError.GET_MEMBER_BYID_SUCC, resultMap);
		return responseResult;
	}

}
