package com.sinodynamic.hkgta.service.fms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.dto.fms.CourseEnrollmentDto;
import com.sinodynamic.hkgta.dto.fms.CourseInfoDto;
import com.sinodynamic.hkgta.dto.fms.CourseListDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemDto;
import com.sinodynamic.hkgta.dto.fms.MemberAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.fms.MemberInfoDto;
import com.sinodynamic.hkgta.dto.fms.NewCourseDto;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CourseService extends IServiceBase<CourseMaster> {
	public ResponseResult createCourseSession(String coachNo,
			String[] facilityNo, CourseSessionDto sessionDto, String userName)
			throws Exception;

	public ResponseResult getCourseList(ListPage<CourseMaster> page,
			CourseListDto dto);

	public List<StaffProfile> getAvailableCoachList(String type, List<String[]> periodList);
	
	public ResponseResult getAvailableCoachList(String type,
			String beginDatetime, String endDatetime) throws Exception;

	public ResponseResult createCourse(NewCourseDto dto);

	public ResponseResult getAvailableFacilities(String type,
			String beginDatetime, String endDatetime);
	
	public Map<String, List<FacilityItemDto>> getAvailableFacilities(String type, List<String[]> periodList);

	public ResponseResult getCourseInfo(String courseId, String device);

	public ResponseResult changeCourseSessionStatus(String courseId,
			String status,  String userId) throws Exception;

	public ResponseResult modifyCourseSession(String coachNo,
			String[] facilityNo, CourseSessionDto sessionDto, String username)
			throws Exception;

	public ResponseResult getMemberInfo(ListPage<CourseEnrollment> page,
			String courseId, String attendance, String status,String sessionId);

    public ResponseResult changeCourseStatus(CourseInfoDto dto, String userId) throws Exception;
    
    public ResponseResult getCourseSessionInfo(Long sysId, String type) throws Exception;

	public ResponseResult updateCourse(NewCourseDto dto);

	public ResponseResult uploadFeatureImage(MultipartFile file);
	
	public ResponseResult getCourseAndMemberInfo(String customerId, String courseId);
	
	public ResponseResult getCourseAndMemberInfo(String customerId, String courseId, Long refundId);

	/**
	 * @discription This method is used to change course status to close when register due date come
	 * @author Mianping_Wu
	 * @date 7/14/2015
	 */
	public void closeCourseOnDueDate() throws Exception;

	/**
	 * 
	* @param
	* @return ResponseResult
	* @author Vineela_Jyothi
	* Date: Jul 15, 2015
	 */
	public ResponseResult changeMemberAttendanceStatus(String sessionId, String enrollId, String attendId, String status, String userId);
	
	/**
	 * 1.member enroll a course<br>
	 * 2.call payment flow
	 * @param courseEnrollmentDto
	 * @param createBy
	 * @return
	 * @throws Exception
	 * @author Miranda_Zhang
	 * @date Aug 7, 2015
	 */
	public ResponseResult enrollCourseFinal(CourseEnrollmentDto courseEnrollmentDto,String createBy) throws Exception;

	/**
	* @param
	* @return String
	* @author Vineela_Jyothi
	* Date: Jul 22, 2015
	*/
	public String getCommonQueryByCourseType(String courseType,
			String dateRange, String expired, String customerId);
	
	public String getCommonQueryByCourseTypeAndroid(String courseType,
		String dateRange, String expired, String customerId);
	
	/**
	* @param
	* @return String
	* @author Vineela_Jyothi
	* Date: Aug 3, 2015
	*/
	public String getCommonQueryForMembersList(String courseId,
			String attendance, String status);

	/**
	* @param
	* @return void
	* @author Vineela_Jyothi
	* Date: Aug 4, 2015
	*/
	public List<MemberAttendanceDto> getMembersAttendance(String courseId, List<MemberInfoDto> members,String sessionId);

	/**
	* @param
	* @return ResponseResult
	* @author Vineela_Jyothi
	* Date: Aug 4, 2015
	 * @param status 
	 * @param attendance 
	 * @param page 
	*/
	public ResponseResult prepareMemberListResponse(String courseId,
			ResponseResult responseResult, String attendance, String status, ListPage<CourseMaster> page,String sessionId);
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 19, 2015
	 * @Param @param dto
	 * @Param @param userId
	 * @Param @param ce
	 * @Param @param cm
	 * @Param @throws Exception
	 * @return void
	 */
	public void processRefundRequest(CourseInfoDto dto, String userId, CourseEnrollment ce, CourseMaster cm) throws Exception;
	
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 25, 2015
	 * @Param @param courseEnrollmentDto
	 * @Param @param createBy
	 * @Param @return
	 * @Param @throws Exception
	 * @return ResponseResult
	 */
	public ResponseResult repaymentCourse(CourseEnrollmentDto courseEnrollmentDto, String createBy) throws Exception;
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 27, 2015
	 * @Param @param courseEnrollmentDto
	 * @Param @return
	 * @return boolean
	 */
	public boolean isRepayCourse(CourseEnrollmentDto courseEnrollmentDto);
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Sep 23, 2015
	 * @Param @param sceneType
	 * @Param @param course
	 * @Param @return
	 * @return Map<String,Object>
	 */
	public Map<String, Object> notifyCourseMembers(SceneType sceneType, CourseMaster course);
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Sep 23, 2015
	 * @Param @param sceneType
	 * @Param @param ce
	 * @Param @param course
	 * @Param @return
	 * @return Map<String,Object>
	 */
	public Map<String, Object> notifyEnrollMember(SceneType sceneType, CourseEnrollment ce, CourseMaster course);
	
	/**
	 * send course reminder message to member
	 * @return
	 * @author Miranda_Zhang
	 * @date Sep 30, 2015
	 */
	public List<Map<String, Object>> sendReminderMsg(Date begin, Date end) ;

	public MemberCashvalueDto getMemberInfo(String customerId);

	public ResponseResult getMonthlyCourseCompletionReport(ListPage<CourseMaster> page, String closedDate, String courseType);

	byte[] getMonthlyCourseCompletionReportAttach(String closedDate, String fileType, String courseType, String sortBy, String isAscending);

	ResponseResult getMonthlyCourseRevenueReport(ListPage page, String transactionDate, String facilityType);

	byte[] getMonthlyCourseRevenueReportAttach(String transactionDate, String fileType, String facilityType, String sortBy, String isAscending);
}

