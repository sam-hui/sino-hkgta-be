package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerTransactionDao;
import com.sinodynamic.hkgta.dto.rpos.UpdateServiceAccountStatusDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRightPK;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.constant.ServicePlanRightTypeStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Service
public class CustomerServiceAccServiceImpl extends ServiceBase<CustomerServiceAcc> implements CustomerServiceAccService {

	@Autowired
	private CustomerServiceDao customerServiceDao;
	
	@Autowired
	CustomerTransactionDao customerTransactionDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private PermitCardMasterDao permitCardMasterDao;
	
	@Autowired
	private CustomerServiceAccDao  customerServiceAccDao;
	
	@Autowired
	private UserMasterDao userMasterDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;

	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;
	
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;

	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
	
	@Autowired
	private ServicePlanDao servicePlanDao;

	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Override
	@Transactional
	public ResponseResult updateCustomerServiceAccStatus(Long orderNo) {
		ResponseResult result = new ResponseResult();
		UpdateServiceAccountStatusDto updateServiceAccountStatusDto = new UpdateServiceAccountStatusDto();
		BigInteger customerId = customerTransactionDao.getCustomerID(orderNo);
		System.out.println(customerId);
		BigDecimal b = customerTransactionDao.countBalanceDueStatus(orderNo);	
		        if (b.compareTo(BigDecimal.ZERO) <= 0) {
		        	 System.out.println(customerId);
		        	 int accNo = customerServiceDao.updateCustomerServiceAccStatus(customerId);
		        	 System.out.println(accNo);
		        	 updateServiceAccountStatusDto.setAccNo(accNo);
		        	 updateServiceAccountStatusDto.setStatus("ACT");
		        	 result.setData(updateServiceAccountStatusDto);	
		     		 result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
	     		     result.setErrorMessageEN("");
		        	 return  result;
		        }		        
		        int accNo = customerServiceDao.getAccNo(customerId);
		       updateServiceAccountStatusDto.setAccNo(accNo);
		       updateServiceAccountStatusDto.setStatus("NACT");
    	       result.setData(updateServiceAccountStatusDto);	
 		       result.setReturnCode("0" );
		       result.setErrorMessageEN("");
		       return result;

	}

	@Override
	@Transactional
	public List<CustomerServiceAcc> getExpiringCustomers() throws Exception {
		
		String expiringPeriod = appProps.getProperty("member.expire.period", "30");
		return customerServiceDao.selectExpiringMember(Integer.parseInt(expiringPeriod));
	}

	@Override
	@Transactional
	public void handleExpiredCustomerServiceAcc() throws Exception{
		List<CustomerServiceAcc> customerServiceAccs = customerServiceDao.getExpiredCustomerServiceAccs();
		for (CustomerServiceAcc customerServiceAcc: customerServiceAccs) {
			Member member = memberDao.get(Member.class, customerServiceAcc.getCustomerId());
			List<Member> dependentMemebers = memberDao.getByCol(Member.class, "superiorMemberId", member.getCustomerId(), null);
			for (Member dependentMember:dependentMemebers) {
				dependentMember.setStatus(Constant.Member_Status_NACT);
				memberDao.update(dependentMember);
				changePermitCard2Expired(dependentMember.getCustomerId());
				UserMaster denUserMaster = userMasterDao.get(UserMaster.class, dependentMember.getUserId());
				denUserMaster.setStatus(Constant.Status.NACT.name());
				denUserMaster.setUpdateBy("System");
				denUserMaster.setUpdateDate(new Date());
				userMasterDao.update(denUserMaster);
				
			}
			member.setStatus(Constant.Member_Status_NACT);
			memberDao.update(member);
			UserMaster userMaster = userMasterDao.get(UserMaster.class, member.getUserId());
			userMaster.setStatus(Constant.Status.NACT.name());
			userMaster.setUpdateBy("System");
			userMaster.setUpdateDate(new Date());
			userMasterDao.update(userMaster);
			changePermitCard2Expired(member.getCustomerId());
			customerServiceAcc.setStatus(Constant.Status.EXP.name());
			customerServiceDao.update(customerServiceAcc);
		}
	}
	
	
	private void changePermitCard2Expired(Long customerId) {
	    
	    String hql = "from PermitCardMaster where status = 'ISS' and mappingCustomerId = " + customerId;
	    List<PermitCardMaster> cards = permitCardMasterDao.getByHql(hql);
	    if (cards == null || cards.size() == 0) return;
	    
	    for (PermitCardMaster card : cards) {
		
		card.setStatus("EXP");
		card.setUpdateBy("ADMIN");
		card.setStatusUpdateDate(new Date());
		permitCardMasterDao.updatePermitCardMaster(card);
	    }
	}

	/**   
	* @author: Zero_Wang
	* @since: Sep 9, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public CustomerServiceAcc getAactiveByCustomerId(Long customerId) {
		CustomerServiceAcc acc = this.customerServiceAccDao.getAactiveByCustomerId(customerId);
		if(null==acc){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerServiceAcc customerId not right:"+customerId});
		}
		return acc;
		
	}

	@Override
	@Transactional
	public boolean isCustomerServiceAccValid(Long customerId, ContractHelper obj) {
	    
	    if (customerId == null || obj == null) return false;
	    
	    Member member = memberDao.getMemberByCustomerId(customerId);
	    if (member == null) return false;
	    
	    Long cID = null;
	    String memberType = member.getMemberType();
	    if (MemberType.IDM.name().equals(memberType) || MemberType.IPM.name().equals(memberType)) {
		
		if (MemberType.IDM.name().equals(memberType)) {
		    cID = member.getSuperiorMemberId();
		} else {
		    cID = customerId;
		}
		
	    } else if (MemberType.CDM.name().equals(memberType) || MemberType.CPM.name().equals(memberType)) {
		
		if (MemberType.CDM.name().equals(memberType)) {
		    cID = member.getSuperiorMemberId();
		} else {
		    cID = customerId;
		}
	    }
	    
	    String sql = "select distinct 1 "
			+ "from customer_service_acc "
			+ "where status = 'ACT' "
			+ "and effective_date <= ?"
			+ " and date_add(expiry_date, interval 1 day) >= ?"
			+ " and customer_id = ?";
	    
	    List<Serializable> param = new ArrayList<Serializable>();
	    param.add(obj.getPeriodFrom());
	    param.add(obj.getPeriodTo());
	    param.add(cID);
	    
	    boolean result = customerServiceAccDao.isCustomerServiceValid(sql, param);
	    return result;
	}

	@Override
	@Transactional
	public void startRenewServiceAccount() throws Exception{
		List<CustomerServiceAcc> customerServiceAccs = customerServiceDao.getEffctiveRenewCustomerServiceAccs();
		for (CustomerServiceAcc customerServiceAcc: customerServiceAccs) {
			try {
				startCurrentServiceAccount(customerServiceAcc);
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug("Set facility right or limit rule failed!", e);
			}
		}
	
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void startCurrentServiceAccount(CustomerServiceAcc customerServiceAcc) throws Exception {
		if (null == customerServiceAcc.getOrderNo() || "".equals(customerServiceAcc.getOrderNo())) {//Handle the dirty data before.
			return;
		}
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, customerServiceAcc.getOrderNo());
		if (null == customerOrderHd) {//Handle the dirty data before.
			return;
		}
		CustomerServiceSubscribe customerServiceSubscribe = customerServiceSubscribeDao.getUniqueByCol(CustomerServiceSubscribe.class, "id.accNo", customerServiceAcc.getAccNo());
		ServicePlan renewalServicePlan = servicePlanDao.get(ServicePlan.class, customerServiceSubscribe.getId().getServicePlanNo());
		if (null != customerOrderHd && Constant.Status.CMP.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())) {
			customerServiceAcc.setStatus(Constant.Status.ACT.name());
			customerServiceDao.update(customerServiceAcc);
			Member member = memberDao.get(Member.class, customerServiceAcc.getCustomerId());
			member.setStatus(Constant.Status.ACT.name());
			memberDao.update(member);
			UserMaster userMaster = userMasterDao.get(UserMaster.class, member.getUserId());
			userMaster.setStatus(Constant.Status.ACT.name());
			userMaster.setUpdateBy("System");
			userMaster.setUpdateDate(new Date());
			userMasterDao.update(userMaster);
			//Don't need change dependent member status
//			List<Member> dependentMemebers = memberDao.getByCol(Member.class, "superiorMemberId", member.getCustomerId(), null);
//			for (Member dependentMember:dependentMemebers) {
//				dependentMember.setStatus(Constant.Member_Status_NACT);
//				memberDao.update(dependentMember);
//				changePermitCard2Expired(dependentMember.getCustomerId());
//				UserMaster denUserMaster = userMasterDao.get(UserMaster.class, dependentMember.getUserId());
//				denUserMaster.setStatus(Constant.Status.NACT.name());
//				denUserMaster.setUpdateBy("System");
//				denUserMaster.setUpdateDate(new Date());
//				userMasterDao.update(denUserMaster);
//				
//			}
			
			// save member_plan_facility_right 
			starUpCustomerPlanFacilityRight(customerOrderHd.getCustomerId(),renewalServicePlan.getPlanNo(),customerServiceAcc,"System");
			
			//save member_limit_rule
			startUpMemberLimitRule(customerOrderHd.getCustomerId(),renewalServicePlan.getPlanNo(),customerServiceAcc,"System");
			
			//save member_plan_facility_right and member_limit_rule for dependent members.
			startUpLimitRuleAndFacilityRightForDependentMember(customerOrderHd.getCustomerId(),customerServiceAcc.getEffectiveDate(),customerServiceAcc.getExpiryDate());
		}
	}

	private void startUpLimitRuleAndFacilityRightForDependentMember(Long customerId, Date effectiveDate, Date expiryDate) {

		List<Member> listDependent = memberDao.getListMemberBySuperiorId(customerId);
		
		for (Member tempDependent : listDependent) {
			Long dependentCustomerId = tempDependent.getCustomerId();
			setMemberLimitRule(dependentCustomerId, "TRN", BigDecimal.ZERO, null, "System",expiryDate, effectiveDate);
			
			// member limit rule
			List<MemberLimitRule> primaryLimitRule = memberLimitRuleDao.getEffectiveListByCustomerId(customerId);
			if (null != primaryLimitRule && primaryLimitRule.size() > 0) {
				for (MemberLimitRule rule : primaryLimitRule) {
					if (null != primaryLimitRule && !"CR".equals(rule.getLimitType()) && !"G1".equals(rule.getLimitType())) {
						MemberLimitRule r = new MemberLimitRule();
						BeanUtils.copyProperties(rule, r,
								new String[] { "limitId", "customerId", "updateBy", "updateDate" });
						r.setCustomerId(dependentCustomerId);
						r.setUpdateBy("System");
						r.setUpdateDate(new Date());
						memberLimitRuleDao.save(r);
					}
				}
			}
			// save MemberPlanFacilityRight
			List<MemberPlanFacilityRight> mfr = memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(customerId);
			if (null != mfr && mfr.size() > 0) {
				for (MemberPlanFacilityRight right : mfr) {
					if (null != right) {
						MemberPlanFacilityRight r = new MemberPlanFacilityRight();
						BeanUtils.copyProperties(right, r, new String[] { "sysId", "customerId" });
						r.setCustomerId(dependentCustomerId);
						memberPlanFacilityRightDao.save(r);
					}
				}
			}
			
		}
	}

	/**
	 * save the customer's CustomerPlanFacilityRight as follows:
	 * 
	 * 1. get the right data from  service_plan_facility by servicePlanNo
	 * 2. save the search data from service_plan_facility into member_plan_facility_right
	 * 
	 * @param servicePlanNo planNo
	 * @throws Exception
	 */
	private void starUpCustomerPlanFacilityRight(Long customerId,Long servicePlanNo,CustomerServiceAcc customerServiceAcc, String createBy) throws Exception{
		/*
		 * these variable is not sure,then change later
		 *
		 */
//		String createBy = "Li_Chen"; //get this by session user
		
		List<ServicePlanFacility> planFacilityList =  servicePlanFacilityDao.getByCol(ServicePlanFacility.class, "servicePlan.planNo", servicePlanNo, null);
		if (null != planFacilityList && planFacilityList.size()>0) {
			for (ServicePlanFacility facility : planFacilityList) {
				MemberPlanFacilityRight memberPlanFacilityRight = new MemberPlanFacilityRight();
				MemberPlanFacilityRightPK pk = new MemberPlanFacilityRightPK();
				pk.setCustomerId(customerId);
				pk.setServicePlan(servicePlanNo);
				pk.setFacilityTypeCode(facility.getFacilityTypeCode());
				memberPlanFacilityRight.setEffectiveDate(customerServiceAcc.getEffectiveDate());
				memberPlanFacilityRight.setExpiryDate(customerServiceAcc.getExpiryDate());
//				memberPlanFacilityRight.setId(pk);
				memberPlanFacilityRight.setCustomerId(customerId);
				memberPlanFacilityRight.setServicePlan(servicePlanNo);
				memberPlanFacilityRight.setFacilityTypeCode(facility.getFacilityTypeCode());
				
				memberPlanFacilityRight.setPermission(facility.getPermission());
				memberPlanFacilityRight.setCreateBy(createBy);
				memberPlanFacilityRight.setCreateDate(new Timestamp(new Date().getTime()));
				memberPlanFacilityRightDao.save(memberPlanFacilityRight);
			}
		}
	}
	
	/**
	 * 
	 * @param servicePlanNo
	 */
	private void startUpMemberLimitRule(Long customerId,Long servicePlanNo,CustomerServiceAcc customerServiceAcc,String createBy) throws Exception{
		List servicePlanAdditionRuleList = servicePlanAdditionRuleDao.getServicePlanAdditionRuleByPlanNo(servicePlanNo);
		if ( null != servicePlanAdditionRuleList && servicePlanAdditionRuleList.size()>0) {
			for (int i=0;i<servicePlanAdditionRuleList.size();i++) {
				Object[] objectArry = (Object[])servicePlanAdditionRuleList.get(i);
				MemberLimitRule memberLimitRule = new MemberLimitRule();
				memberLimitRule.setCustomerId(customerId);
				memberLimitRule.setLimitType( (String)objectArry[0]);
				String inputValueType = (String)objectArry[1];
				if (!StringUtils.isEmpty(inputValueType))
					inputValueType = inputValueType.trim();
				if (ServicePlanRightTypeStatus.TXT.getDesc().equals(inputValueType)) 
					memberLimitRule.setTextValue((String)objectArry[2]);
				else if (ServicePlanRightTypeStatus.INT.getDesc().equals(inputValueType)) {
					memberLimitRule.setLimitUnit("MONTH");
					memberLimitRule.setNumValue(new BigDecimal((String)objectArry[2]));
				}	
				memberLimitRule.setDescription((String)objectArry[3]);
				memberLimitRule.setEffectiveDate(customerServiceAcc.getEffectiveDate());
				memberLimitRule.setExpiryDate(customerServiceAcc.getExpiryDate());
				memberLimitRule.setCreateDate(new Timestamp(new Date().getTime()));
				memberLimitRule.setCreateBy(createBy);
				memberLimitRule.setUpdateDate(new Date());
				memberLimitRule.setUpdateBy(createBy);
				memberLimitRuleDao.save(memberLimitRule);
			}
		}
		// member limit rule
		setMemberLimitRule(customerId, "CR", BigDecimal.ZERO, "Credit Limit for Individual Primary Member", createBy,customerServiceAcc.getExpiryDate(),customerServiceAcc.getEffectiveDate());
	}
	private void setMemberLimitRule(Long customerId, String limitType, BigDecimal limitValue, String description,
			String userId,Date expiryDate,Date effectiveDate) {
		Date currentDate = new Date();
		MemberLimitRule memberLimitRuleCR = new MemberLimitRule();
		memberLimitRuleCR.setCustomerId(customerId);
		if("TRN".equals(limitType)){
			memberLimitRuleCR.setLimitUnit("EACH");
		}
		memberLimitRuleCR.setLimitType(limitType);
		memberLimitRuleCR.setNumValue(limitValue);
		memberLimitRuleCR.setDescription(description);
		memberLimitRuleCR.setEffectiveDate(effectiveDate);
		memberLimitRuleCR.setExpiryDate(expiryDate);
		memberLimitRuleCR.setCreateDate(new Timestamp(currentDate.getTime()));
		memberLimitRuleCR.setCreateBy(userId);
		memberLimitRuleCR.setUpdateBy(userId);
		memberLimitRuleCR.setUpdateDate(currentDate);
		memberLimitRuleDao.saveMemberLimitRuleImpl(memberLimitRuleCR);
	}
	
}
