package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface DependentMemberService extends IServiceBase<CustomerProfile>{
	
	public ResponseResult saveDependentMemberInfo(CustomerProfile w, String currentUserId, String fromName) throws Exception;
	
	public ResponseMsg updateDependentMemberInfo(CustomerProfile w, String createBy,String fromName) throws Exception;
	
	public ResponseMsg updateProfilePhoto(CustomerProfile w) throws Exception;
	
	
	public ResponseResult getDependentMemberByAcademyNo(String academyNo) throws Exception;

	public ResponseResult getDepartMemberByCustomerId(String CustomerId) throws Exception;
	
	public ResponseResult checkDependentMember(String academyNo);
}
