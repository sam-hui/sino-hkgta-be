package com.sinodynamic.hkgta.service.crm.sales;

import java.math.BigDecimal;
import java.util.List;

import com.sinodynamic.hkgta.dto.staff.SecurityQuestionDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberService extends IServiceBase<Member>{
	
	public void addMember(Member m) throws Exception;
	
	public void getMemberList(ListPage<Member> page, Member m) throws Exception;

	public void updateMember(Member m) throws Exception;

	public void deleteMember(Member m) throws Exception;
	
	public Member getMemberById(Long id) throws Exception;

	public ResponseResult getDependentMembers(Long customerID,ListPage page);
	
	public ResponseResult checkDMCreditLimit(Long customerId);

	public ResponseResult checkTransactionLimit(Long customerId, BigDecimal spending);
	
	public ResponseResult setSecurityQuestion(SecurityQuestionDto questions) throws Exception;
	
	public List<Member> getAllMembers() throws Exception;

	public String getMemberTypeByCustomerId(Long customerID);
	
	public Member getMemberByAcademyNo(String academyNo);
}
