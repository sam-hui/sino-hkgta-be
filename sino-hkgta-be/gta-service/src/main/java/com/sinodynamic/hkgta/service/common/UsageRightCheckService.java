package com.sinodynamic.hkgta.service.common;

import com.sinodynamic.hkgta.service.IServiceBase;

/**   
 * Only for Service Plan
* @author: Ray_Liang
* @since: Nov 25, 2015
*/

    
public interface UsageRightCheckService extends IServiceBase {
	enum FacilityCode{GOLF,GUEST,GYM,SWIM,TENNIS,WELLNESS};
	enum TrainingCode{
		
		TRAIN1("Golf Courses"),
		TRAIN2("Golf Private Coaching"),
		TRAIN3("Tennis Courses"),
		TRAIN4("Tennis Private Coaching");
		
		private String description;	
		TrainingCode(String description){
			this.description = description;
		}
		public String getDescription(){
			return this.description;
		}
	};
	enum OtherRightCode{
		D1("Day Pass Purchasing"),EVENT1("Events"),G1("Dependent Creation");
		
		private String description;	
		OtherRightCode(String description){
			this.description = description;
		}
		public String getDescription(){
			return this.description;
		}
	};
	
	class Right{
		private boolean canBook = false;
		
		public boolean isCanBook() {
			return canBook;
		}
		public void setCanBook(boolean canBook) {
			this.canBook = canBook;
		}
	}
	
	class FacilityRight extends Right{
		private boolean canAccess = false;
		
		public boolean isCanAccess() {
			return canAccess;
		}
		public void setCanAccess(boolean canAccess) {
			this.canAccess = canAccess;
		}
	}
	
	class TrainingRight extends Right{
	}
	
	class OtherRight extends Right{
		private Object additionalAttribute = null;

		public Object getAdditionalAttribute() {
			return additionalAttribute;
		}

		public void setAdditionalAttribute(Object additionalAttribute) {
			this.additionalAttribute = additionalAttribute;
		}
	}
	
	FacilityRight checkFacilityRight(Long customerId, FacilityCode facilityCode);
	TrainingRight checkTrainingRight(Long customerId, TrainingCode trainingCode);
	OtherRight checkOtherRight(Long customerId, OtherRightCode otherRightCode);
}
