package com.sinodynamic.hkgta.service.crm.pub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.pub.NoticeDao;
import com.sinodynamic.hkgta.dto.crm.pub.NoticeDto;
import com.sinodynamic.hkgta.dto.crm.pub.NoticeFileDto;
import com.sinodynamic.hkgta.entity.crm.Notice;
import com.sinodynamic.hkgta.entity.crm.NoticeFile;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.NoticeFileType;
import com.sinodynamic.hkgta.util.constant.NoticeStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;

/**
 * @author Mason_Yang
 *
 */
@Service
@Transactional
public class NoticeServiceImpl extends ServiceBase<Notice> implements NoticeService {

	@Autowired
	private NoticeDao noticeDao;
	
	@Autowired
	private SysCodeDao syscodeDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Override
	public long save(NoticeDto noticeDto, String createBy) {
		if (StringUtils.isEmpty(noticeDto.getSubject())) {
			throw new GTACommonException(GTAError.PublicationError.SUBJECT_IS_EMPTY);
		} else if (StringUtils.isEmpty(noticeDto.getContent())) {
			throw new GTACommonException(GTAError.PublicationError.CONTENT_IS_EMPTY);
		}
		
		String noticeType = noticeDto.getNoticeType();
		
		if (noticeType == null || syscodeDao.getByCategoryAndCodeValue("noticeType", noticeType) == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_TYPE);
		}
		
		Notice notice = new Notice();
		notice.setSubject(noticeDto.getSubject());
		notice.setContent(noticeDto.getContent());
		notice.setNoticeType(noticeType);
		notice.setNoticeBegin(noticeDto.getNoticeBegin());
		
		setStatus(noticeDto.getStatus(), notice);
		
		notice.setCreateDate(new Date());
		notice.setCreateBy(createBy);
		NoticeFileDto noticeFileDto = noticeDto.getNoticeFile();
		if (noticeFileDto != null) {
			NoticeFile noticeFile = new NoticeFile();
			noticeFile.setCreateBy(createBy);
			noticeFile.setCreateDate(new Date());
			noticeFile.setNotice(notice);
			noticeFile.setFileName(noticeFileDto.getFileName());
			noticeFile.setFileType(NoticeFileType.IMG.toString());
			notice.addNoticeFiles(noticeFile);
		}
		return (long) noticeDao.save(notice);
	}

	private void setStatus(String status, Notice notice) {
		if (NoticeStatus.ACT.toString().equals(status)) {
			notice.setStatus(NoticeStatus.ACT.toString());
		} else if (NoticeStatus.NACT.toString().equals(status)) {
			notice.setStatus(NoticeStatus.NACT.toString());
		} else {
			throw new GTACommonException(GTAError.PublicationError.STATUS_ERROR);
		}
	}

	@Override
	public NoticeDto getNotice(long noticeId) {
		
		Notice notice = noticeDao.get(Notice.class, noticeId);
		
		if(notice == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_NOT_FOUND);
		}
		return getNoticeDto(notice);
	}

	private NoticeDto getNoticeDto(Notice notice) {
		NoticeDto noticeDto = new NoticeDto();
		noticeDto.setNoticeId(notice.getNoticeId());
		noticeDto.setSubject(notice.getSubject());
		String noticeType = notice.getNoticeType();
		noticeDto.setNoticeType(noticeType);
		noticeDto.setRecipientType(notice.getRecipientType());
		noticeDto.setContent(notice.getContent());
		noticeDto.setNoticeBegin(notice.getNoticeBegin());
		noticeDto.setStatus(notice.getStatus());
		noticeDto.setCreateBy(notice.getCreateBy());
		
		StaffProfile staffProfile = staffProfileDao.getByUserId(notice.getCreateBy());
		
		if (staffProfile != null) {
			noticeDto.setCreator(staffProfile.getGivenName() + " " + staffProfile.getSurname());
		}
		noticeDto.setCreateDate(notice.getCreateDate());
		//categoryName
		if (noticeType != null) {
			SysCode sysCode = syscodeDao.getByCategoryAndCodeValue("noticeType", noticeType);
			
			if (sysCode != null) {
				noticeDto.setCategoryName(sysCode.getCodeDisplay());
			}
		}
		
		List<NoticeFile> noticeFiles = notice.getNoticeFiles();
		if (noticeFiles != null && !noticeFiles.isEmpty()) {
			for (NoticeFile noticeFile : noticeFiles) {
				NoticeFileDto noticeFileDto = new NoticeFileDto();
				noticeFileDto.setFileId(noticeFile.getFileId());
				noticeFileDto.setFileName(noticeFile.getFileName());
				noticeDto.setNoticeFile(noticeFileDto);
			}

		}
		return noticeDto;
	}

	@Override
	public void update(NoticeDto noticeDto, String createBy) {
		if (StringUtils.isEmpty(noticeDto.getSubject())) {
			throw new GTACommonException(GTAError.PublicationError.SUBJECT_IS_EMPTY);
		} else if (StringUtils.isEmpty(noticeDto.getContent())) {
			throw new GTACommonException(GTAError.PublicationError.CONTENT_IS_EMPTY);
		}
		
		String noticeType = noticeDto.getNoticeType();
		
		if (noticeType == null || syscodeDao.getByCategoryAndCodeValue("noticeType", noticeType) == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_TYPE);
		}
		
		Notice notice = noticeDao.get(Notice.class, noticeDto.getNoticeId());
		
		if(notice == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_NOT_FOUND);
		}
		
		notice.setSubject(noticeDto.getSubject());
		notice.setContent(noticeDto.getContent());
		notice.setNoticeType(noticeType);
		notice.setNoticeBegin(noticeDto.getNoticeBegin());
		setStatus(noticeDto.getStatus(), notice);
		notice.setUpdateDate(new Date());
		notice.setUpdateBy(createBy);

		notice.getNoticeFiles().clear();
		NoticeFileDto noticeFileDto = noticeDto.getNoticeFile();

		if (noticeFileDto != null) {
			NoticeFile noticeFile = new NoticeFile();
			noticeFile.setCreateBy(createBy);
			noticeFile.setCreateDate(new Date());
			noticeFile.setNotice(notice);
			noticeFile.setFileName(noticeFileDto.getFileName());
			noticeFile.setFileType(NoticeFileType.IMG.toString());

			notice.addNoticeFiles(noticeFile);
		}
		noticeDao.update(notice);
	}

	@Override
	public void updateStatus(Long noticeId, String status, String userId) {

		Notice notice = noticeDao.get(Notice.class, noticeId);
		
		if(notice == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_NOT_FOUND);
		}
		
		setStatus(status, notice);
		notice.setUpdateDate(new Date());
		notice.setUpdateBy(userId);
		noticeDao.update(notice);
	}

	@Override
	public Data getNoticeList(Integer pageNumber, Integer pageSize) {

		ListPage<Notice> listPage = noticeDao.queryNoticeList(null, pageNumber, pageSize);
		List<Notice> notices = listPage.getList();
		List<NoticeDto> noticeDtos = new ArrayList<>();
		if (notices != null && !notices.isEmpty()) {
			for (Notice notice: notices) {
				NoticeDto noticeDto  = getNoticeDto(notice);
				noticeDtos.add(noticeDto);
			}
		}

		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(noticeDtos);
		
		return data;
	}

}
