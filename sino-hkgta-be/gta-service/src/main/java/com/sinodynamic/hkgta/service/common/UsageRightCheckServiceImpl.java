package com.sinodynamic.hkgta.service.common;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.MemberType;

@Service
public class UsageRightCheckServiceImpl extends ServiceBase implements UsageRightCheckService {
	enum Permission{BA,AO,A};
	
	private Logger logger = Logger.getLogger(UsageRightCheckServiceImpl.class);
	@Autowired
	CustomerServiceDao customerServiceDao;
	
	@Autowired
	CustomerServiceSubscribeDao customerServiceSubscribeDao;
	
	@Autowired
	ServicePlanDao servicePlanDao;
	
	@Autowired
	MemberDao memberDao;
	
	@Autowired
	CorporateMemberDao corporateMemberDao;
	
	@Autowired
	CorporateServiceAccDao corporateServiceAccDao;
	
	@Autowired
	CorporateServiceSubscribeDao corporateServiceSubscribeDao;
	
	@Autowired
	ServicePlanFacilityDao servicePlanFacilityDao;
	
	@Autowired
	MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	ServicePlan getServicePlan(Long customerId){
		try{
			Member member = memberDao.get(Member.class, customerId);
			String memberType = member.getMemberType();
			
			if(MemberType.CPM.name().equals(memberType) || MemberType.CDM.name().equals(memberType)){
				if(MemberType.CDM.name().equals(memberType)){
					customerId = member.getSuperiorMemberId();
				}
				CorporateMember corporateMember = corporateMemberDao.getCorporateMemberById(customerId);
				Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateMember.getCorporateProfile().getCorporateId());
				CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
				CorporateServiceSubscribe corporateServiceSubscribe = corporateServiceSubscribeDao.getByAccountNo(corporateServiceAcc.getAccNo());
				ServicePlan servicePlan = servicePlanDao.get(ServicePlan.class, corporateServiceSubscribe.getId().getServicePlanNo());
				
				return servicePlan;
			}else{
				if(MemberType.IDM.name().equals(memberType)){
					customerId = member.getSuperiorMemberId();
				}
				
				CustomerServiceAcc customerServiceAcc = customerServiceDao.getCurrentActiveAcc(customerId);
				List<CustomerServiceSubscribe> customerServiceSubscribes = customerServiceSubscribeDao.getCustomerServiceSubscribeByAccNo(customerServiceAcc.getAccNo());
				ServicePlan servicePlan = servicePlanDao.get(ServicePlan.class, customerServiceSubscribes.get(0).getId().getServicePlanNo());
				return servicePlan;
			}
		}catch(Exception e){
			logger.error("getServicePlan() error", e);
			return null;
		}
	}
	
	@Override
	@Transactional
	public FacilityRight checkFacilityRight(Long customerId, FacilityCode facilityCode) {
		FacilityRight result = new FacilityRight();
		try{
			MemberPlanFacilityRight right = memberPlanFacilityRightDao.getEffectiveRightByCustomerIdAndFacilityType(customerId, facilityCode.name());
			
			if (null == right)
			{
				result.setCanAccess(false);
				result.setCanBook(false);
				return result;
			}
			
			Permission permission = Permission.valueOf(right.getPermission());
			if(Permission.BA == permission){
				result.setCanAccess(true);
				result.setCanBook(true);
				return result;
			}
			if(Permission.A == permission || Permission.AO == permission){
				result.setCanAccess(true);
				return result;
			}
			return result;
		}catch(Exception e){
			logger.error("checkFacilityRight() error", e);
			return result;
		}
	}

	@Override
	@Transactional
	public TrainingRight checkTrainingRight(Long customerId, TrainingCode trainingCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public OtherRight checkOtherRight(Long customerId, OtherRightCode trainingCode) {
		// TODO Auto-generated method stub
		return null;
	}

}
