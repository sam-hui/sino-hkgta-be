package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface GlobalParameterService extends IServiceBase<GlobalParameter>{

	public  ResponseResult saveGlobalParameter();
	
	public  ResponseResult getGlobalParameterById(String paramId);
	
	public ResponseResult setGlobalQuota(String paramValue, String userId);
	
	public  GlobalParameter getGlobalParameter(String paramId);
	
	public List<GlobalParameter> getGlobalParameterListByParamCat(String paramCat);
	
	public void saveOrUpdateGlobalParameters(List<GlobalParameter> globalParameters);

	public ListPage<GlobalParameter> getGlobalParameterList(String paramCat, String sortBy, Boolean isAscending, ListPage<GlobalParameter> page);
	
	public void updateGlobalParameter(GlobalParameter globalParameter);
}
