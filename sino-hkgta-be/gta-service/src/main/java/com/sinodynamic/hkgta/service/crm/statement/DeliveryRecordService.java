package com.sinodynamic.hkgta.service.crm.statement;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
public interface DeliveryRecordService extends IServiceBase {

	public ResponseResult getDeliveryRecord(ListPage page) ;

	public ResponseResult getDeliveryRecordDetail(ListPage page,Long jobId, String deliveryStatus);

	public List<AdvanceQueryConditionDto> assembleQueryConditions(String module);
	
	public boolean updateErrorCountInBatchSendStatementHd(Long batchId);
}
