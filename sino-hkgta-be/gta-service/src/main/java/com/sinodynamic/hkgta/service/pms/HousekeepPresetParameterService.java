package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.HousekeepPresetParameterDto;
import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface HousekeepPresetParameterService extends IServiceBase<HousekeepPresetParameter> {

	public List<HousekeepPresetParameter> getHousekeepPresetParameterList();
	
	public List<HousekeepPresetParameter> getHousekeepPresetParameterCatList();
	
	public ListPage<HousekeepPresetParameter> getHousekeepPresetParameterList(HousekeepPresetParameterDto queryParam,String sortBy, boolean isDesc, ListPage<HousekeepPresetParameter> pListPage) throws Exception;
	
	public HousekeepPresetParameter saveHousekeepPresetParameter(HousekeepPresetParameterDto housekeepPresetParameterDto );
	
	public HousekeepPresetParameter editHousekeepPresetParameter(HousekeepPresetParameterDto housekeepPresetParameterDto );
	
	public HousekeepPresetParameter getHousekeepPresetParameter(Long paramId);

	public void deleteHousekeepPresetParameter(Long paramId, String userId);
}