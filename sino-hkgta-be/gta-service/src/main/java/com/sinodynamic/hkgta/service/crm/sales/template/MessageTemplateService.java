package com.sinodynamic.hkgta.service.crm.sales.template;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.crm.MessageTemplateDto;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MessageTemplateService extends IServiceBase<MessageTemplate> {
	
	MessageTemplate getTemplateByFuncId(String colValue);
	
	public boolean saveMessageTemplate(MessageTemplateDto mtd, Long templateId) throws Exception;
	
	public List<MessageTemplate> getAllMessageTemplate() throws Exception;
	
	public boolean deleteMessageTemplate(Long templateId) throws Exception;
	
	public ResponseResult getEmailTemplate(String templateType,Long customerId,Map<String, String> params);
	
	public ResponseResult getTemplateTypeList();
	
	public ResponseResult getTemplatesByType(String templateType);

}
