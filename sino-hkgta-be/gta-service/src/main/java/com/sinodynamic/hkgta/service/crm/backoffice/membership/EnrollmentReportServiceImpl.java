package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.crm.EnrollmentReportDao;
import com.sinodynamic.hkgta.dto.crm.EnrollmentReportDto;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
@Service
public class EnrollmentReportServiceImpl extends ServiceBase<EnrollmentReportDto> implements EnrollmentReportService{
	
	final String sql = "(select distinct a.acc_no as accNo,"
			+ "c.customer_id as customerId,"
			+ "o.order_date as enrollDate,"
			+ "p.plan_no as planNo,"
			+ "p.plan_name as planName,"
			+ "ifnull(items.offer_code,'ENROLLMENT') as offerCode,"
			+ "p.pass_period_type as passPeriodType,"
			+ "p.contract_length_month as contractLengthMonth,"
			+ "m.academy_no as academyNo,"
			+ "c.salutation,"
			+ "c.given_name as givenName,"
			+ "c.surname,"
			+ "concat (u.given_name,' ',u.surname) as salesFollowBy,"
			+ "o.order_total_amount as orderTotalAmount from customer_profile c "+
			"join member m on c.customer_id=m.customer_id "+
			"join customer_enrollment e on m.customer_id=e.customer_id "+ 
			"left join staff_profile u on e.sales_follow_by=u.user_id "+
			"join customer_order_hd o on e.customer_id=o.customer_id "+
			"join customer_order_det det on det.order_no=o.order_no "+ 
			"join customer_service_subscribe s on s.order_det_id=det.order_det_id "+
			"join customer_service_acc a on a.acc_no=s.acc_no "+
			"join service_plan p on s.service_plan_no=p.plan_no "+
			"join (select null as offer_code,pos.pos_item_no from service_plan_pos pos union "+
			"select offer.offer_code,offer.pos_item_no from service_plan_offer_pos offer) items on items.pos_item_no=det.item_no "+
			"where e.enroll_date between DATE_FORMAT(? ,'%Y-%m-01') and last_day(?)) report";
	
	@Autowired
	EnrollmentReportDao enrollmentReportDao;
	@Override
	@Transactional
	public Data generateEnrollmentReport(String propertyName, String order,int pageSize, int currentPage, Date date) {
		try{
			ListPage<EnrollmentReportDto> pageParam = new ListPage<EnrollmentReportDto>(pageSize, currentPage);
			
			if(order == null) {
				order = "asc";
			}
			if(order.equals("asc")){
				pageParam.addAscending(propertyName);
			}
			if(order.equals("desc")){
				pageParam.addDescending(propertyName);
			}
			
			//


			
			String sqlSelect = "select * from " + sql;
			String sqlCount = "select count(*) from " + sql;
			
			Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
			typeMap.put("accNo", LongType.INSTANCE);
			typeMap.put("customerId", LongType.INSTANCE);
			typeMap.put("planNo", LongType.INSTANCE);
			typeMap.put("enrollDate", DateType.INSTANCE);
			typeMap.put("planName", StringType.INSTANCE);
			typeMap.put("passPeriodType", StringType.INSTANCE);
			typeMap.put("contractLengthMonth", LongType.INSTANCE);
			typeMap.put("academyNo", StringType.INSTANCE);
			typeMap.put("salutation", StringType.INSTANCE);
			
			typeMap.put("givenName", StringType.INSTANCE);
			typeMap.put("surname", StringType.INSTANCE);
			typeMap.put("salesFollowBy", StringType.INSTANCE);
			typeMap.put("orderTotalAmount", BigDecimalType.INSTANCE);
			typeMap.put("offerCode", StringType.INSTANCE);
			
			List param = new ArrayList();
			param.add(date);
			param.add(date);
			
			ListPage<EnrollmentReportDto> pageList = ((GenericDao)enrollmentReportDao).listBySqlDto(pageParam, sqlCount, sqlSelect, param, new EnrollmentReportDto() ,typeMap);
			Data result = new Data();
			result.setCurrentPage(currentPage);
			result.setPageSize(pageSize);
			result.setTotalPage(pageList.getAllPage());
			result.setLastPage(currentPage==pageList.getAllPage());
			result.setRecordCount(pageList.getAllSize());
			
			
			
			result.setList(pageList.getDtoList());
			return result;
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("db.fail.load.folders", Locale.ROOT));
		}
	}
	@Override
	@Transactional
	public BigDecimal getMonthTotalAmount(Date date) {
		String sqlTotalAmount = "select sum(orderTotalAmount) from " + sql;
		Query q = enrollmentReportDao.getCurrentSession().createSQLQuery(sqlTotalAmount).setDate(0, date).setDate(1, date);
		return (BigDecimal)q.uniqueResult();
	}
}
