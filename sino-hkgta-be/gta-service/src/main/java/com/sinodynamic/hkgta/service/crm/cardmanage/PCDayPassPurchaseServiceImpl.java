package com.sinodynamic.hkgta.service.crm.cardmanage;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.management.RuntimeErrorException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.CommonDataQueryByHQLDao;
import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.LeadCustomerDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.memberapp.CandidateCustomerDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CorporateServiceAccDto;
import com.sinodynamic.hkgta.dto.crm.CustomerOrderDetDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.GuestProfileDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.OneDayDaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.PaymentOrderDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderPermitCardDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.rpos.CandidateCustomer;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerOrderPermitCardEnumStatus;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PlanRight;
import com.sinodynamic.hkgta.util.exception.CommonException;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service("pcdayPassPurchaseService")
public class PCDayPassPurchaseServiceImpl extends ServiceBase implements PCDayPassPurchaseService{
	
	@Autowired
	private CommonDataQueryByHQLDao commonByHQLDao;
	@Autowired
	private ServicePlanDao servicePlanDao;
	@Autowired
	private ServicePlanService servicePlanService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;

	@Autowired
	private GlobalParameterDao globalParameterDao;
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	@Autowired
	private CustomerOrderPermitCardDao  customerOrderPermitCardDao;
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
	private UserMasterDao userMasterDao;

	@Autowired
	private LeadCustomerDao leadCustomerDao;

	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;

	@Autowired
	private CorporateServiceAccDao corporateServiceAccDao;
	
	@Autowired
	private CandidateCustomerDao candidateCustomerDao;
	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	@Autowired
    private ShortMessageService  smsService;
	@Autowired
	private CustomerServiceAccDao customerServiceAccDao;
	@Autowired
	private CorporateMemberDao corporateMemberDao;
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	String format = "yyyy-MM-dd";

	@Override
	@Transactional
	public boolean checkPurchaseRight(DaypassPurchaseDto dto) {
		Long customerId = dto.getCustomerId();
		if(customerId==null ){
				return false;
		}
		String hqlstr = " from MemberLimitRule m where m.limitType='D1' and  m.customerId="+customerId+" order by m.expiryDate desc";
		List<MemberLimitRule> rules =  commonByHQLDao.getByHql(hqlstr);
		if(null==rules || rules.size()<=0){
			return false;
		}
		if(0==rules.get(0).getNumValue().intValue()){
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	public boolean checkPurchaseDays(DaypassPurchaseDto dto) {
		Long customerId = dto.getCustomerId();
		Date activationDate = DateConvertUtil.parseString2Date(dto.getActivationDate(), format);
		Date deactivationDate = DateConvertUtil.parseString2Date(dto.getDeactivationDate(), format);
		if(activationDate==null||deactivationDate==null||activationDate.getTime()>deactivationDate.getTime()){
			return false;
		}
		String hqlstr=	" select  sp.passPeriodType  "
							+ " from CustomerServiceSubscribe  css , CustomerServiceAcc csa,ServicePlan sp ,Member s   "
							+ "  where css.id.accNo=csa.accNo  and css.id.servicePlanNo=sp.planNo "
							+ "  and s.customerId=csa.customerId and  csa.status='ACT' and s.customerId="+customerId;
		List weektypeList = (List) commonByHQLDao.excuteByHql(null ,hqlstr);
		if(weektypeList!=null &&!weektypeList.isEmpty()){
			String type=(String) weektypeList.get(0);
			if(StringUtils.equals("FULL",type))	{
				return true;
			}
			Calendar start = Calendar.getInstance();  
			start.setTime(activationDate);
			Calendar end = Calendar.getInstance();  
			end.setTime(deactivationDate);
			while(start.compareTo(end) <= 0) {
	            int w = start.get(Calendar.DAY_OF_WEEK);
	            if(w == Calendar.SUNDAY||w==Calendar.SATURDAY){
	                 return false;
	            }
	            start.set(Calendar.DATE, start.get(Calendar.DATE) + 1); //add one day 
	        }
		}
		return true;
	}

	@Override
	@Transactional
	public ResponseResult checkAvailability(DaypassPurchaseDto dto) {
		Long[] buysNos = dto.getBuysNos();
		Long[] daypassNos = dto.getDaypassNos();
		Long sum = (long) 0;
		for(Long temp : buysNos){
			sum = sum + temp;
		}
		boolean buyFlag = true;
		/*
		 * first get HKGTA golbal Quota then check enough or not
		 */
		Long golbalHkgtaQuota = getGolbalHkgtaQuota(sum);
		int total =  this.daysBetween(DateConvertUtil.parseString2Date(dto.getActivationDate(), format), DateConvertUtil.parseString2Date(dto.getDeactivationDate(), format)) ;
		Map<String, OneDayDaypassPurchaseDto> map  = obtainHkgtaQuota(dto, total);
		buyFlag  = checkGolbalHkgtaQuota(map, sum, golbalHkgtaQuota);
		/*
		 * first get Member related serviceplan Quota then check enough or not
		 */
		Long memberQuota = (long) 0;
		if(Constant.Purchase_Daypass_Type.Member.toString().equals(this.buyerType(dto.getId()))){
			dto.setIsMember(1);
		//if(1 == dto.getIsMember()){
			memberQuota = getServiceplanQuota(Long.parseLong(dto.getId()));
			obtainMemberQuota(dto, map,total);
			buyFlag = checkMemberQuota(map, sum, memberQuota);
		}else {
			dto.setIsMember(0);
		}
		if(buyFlag){
			dto.setBuyFlag(Constant.Purchase_Flag_Type.OK.toString());
		}else {
			dto.setBuyFlag(Constant.Purchase_Flag_Type.FAIL.toString());
			//add error msg into DaypassPurchaseDto errorMsg
			List<String> errorMsg = new ArrayList<String>();
			for(OneDayDaypassPurchaseDto o : map.values()){
				if(!StringUtils.isEmpty(o.getErrorMsg())){
					errorMsg.add(o.getErrorMsg());
				}
			}
			dto.setErrorMsg(errorMsg);
		}
		/*
		 * set HKGTAQuota memberQuota, calculateOnedayPrice and set total price for aypassPurchaseDto
		 */
		ResponseResult result = setValue4DaypassPurchaseDto(dto,golbalHkgtaQuota,memberQuota ,daypassNos, buysNos, map , total);
		if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
			return result;
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
	}
	

	private Long getGolbalHkgtaQuota(Long sum) {
		String paramId ="DPDAYQUOTA";
		GlobalParameter globalParameter = this.globalParameterDao.get(GlobalParameter.class, paramId);
		if(null == globalParameter ){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{paramId});
		}
		Long golbalHkgtaQuota = Long.parseLong(globalParameter.getParamValue());
		if(sum>golbalHkgtaQuota){
			throw new GTACommonException(GTAError.DayPassPurchaseError.HKGTA_QUOTA_NOT_ENOUGH,"Not enough quota for your selected date. Please select another date.");
		}
		return golbalHkgtaQuota;
	}

	private ResponseResult setValue4DaypassPurchaseDto(DaypassPurchaseDto dto,
			Long golbalHkgtaQuota, Long memberQuota, Long[] daypassNos,
			Long[] buysNos, Map<String, OneDayDaypassPurchaseDto> map, int total) {
		List<OneDayDaypassPurchaseDto> days = new ArrayList<OneDayDaypassPurchaseDto>();
		dto.setTotalHkgtaQuota(golbalHkgtaQuota);
		dto.setTotalMemberQuota(memberQuota);
		ResponseResult result = calculateOnedayPrice(dto, daypassNos, map,total, days,buysNos);
		if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
			return result;
		}
		Map<String, CustomerOrderDetDto> itemMap = (Map<String, CustomerOrderDetDto>) result.getData();
		BigDecimal totalPrice = new BigDecimal(0);
		for(OneDayDaypassPurchaseDto purchaseDto : days){
			totalPrice = totalPrice.add(purchaseDto.getOneDayPrice());
		}
		dto.setTotalPrice(totalPrice);
		dto.setItemMap(itemMap);
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
		
	}

	private ResponseResult calculateOnedayPrice(DaypassPurchaseDto dto, Long[] daypassNos, Map<String, OneDayDaypassPurchaseDto> map ,int total, List<OneDayDaypassPurchaseDto> days,Long[] buysNos) {
		Map<String, CustomerOrderDetDto> itemMap = new HashMap<String, CustomerOrderDetDto>();
		Calendar cal = Calendar.getInstance();  
		String format = "yyyy-MM-dd";
		for (int j = 0; j <= total; j++) {
			cal.setTime(DateConvertUtil.parseString2Date(dto.getActivationDate(), format));
			cal.add(Calendar.DATE, j);
			Date current = cal.getTime();
			String dateStr = CommUtil.toChar(current, format);
			OneDayDaypassPurchaseDto temp = map.get(dateStr);
			if(null==temp){
				temp = new OneDayDaypassPurchaseDto();
				//when this day there is no member buy daypass and hkgta buy daypass record,temp will be null
				//fix  SINOGTA-6570 Quota value issue
				Long sum = 0L;
				for(int i=0;i<buysNos.length;i++){
					sum = sum + buysNos[i];
				}
				Long golbalHkgtaQuota = dto.getTotalHkgtaQuota();
				Long memberQuota = dto.getTotalMemberQuota();
				if(sum<=golbalHkgtaQuota){
					temp.setHkgtaFlag(Constant.Purchase_Flag_Type.OK.toString());
				}else {
					temp.setHkgtaFlag(Constant.Purchase_Flag_Type.FAIL.toString());
				}
				if(sum<=memberQuota){
					temp.setMemberFlag(Constant.Purchase_Flag_Type.OK.toString());
				}else {
					temp.setMemberFlag(Constant.Purchase_Flag_Type.FAIL.toString());
				}
				temp.setOneday(dateStr);
				map.put(dateStr, temp);
			}
			days.add(temp);
		}
		dto.setDays(days);
		for(int i=0;i<daypassNos.length;i++){
			Long planNo = daypassNos[i];
			ServicePlanDto daypass = null;
			String[] itemNos = null;
			try{
				daypass = this.servicePlanService.getDaypassById(planNo);
				itemNos = getPriceItemNos(planNo);
			} catch(Exception e){
				logger.error(e);
				e.printStackTrace();
				this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"daypass planNo: "+planNo});
				return this.responseResult;
			}
//			finally {
//				if(GTAError.CommomError.DATA_ISSUE.getCode().equals(this.responseResult.getReturnCode())){
//					logger.error("daypass planNo:"+planNo+" data is not integrity");
//					throw new RuntimeErrorException(null, "daypass planNo:"+planNo+" data is not integrity");
//				}
//			}
			Long buyNo = dto.getBuysNos()[i];
			List<Integer> highpriceDay = null;
			highpriceDay = getHighpriceDay(daypass);
			for(String d : map.keySet()){
				OneDayDaypassPurchaseDto purchaseDto = map.get(d);
				try {
					cal.setTime(CommUtil.toDate(d, "yyyy-MM-dd"));
				} catch (Exception e) {
					logger.error(e);
					e.printStackTrace();
					this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"date formate error "+d});
					return this.responseResult;
				}
				BigDecimal quantity = new BigDecimal(dto.getBuysNos()[i]);
				BigDecimal oneDayPrice = BigDecimal.ZERO;
				String itemNo = null;
				// high day like this 2015/03/28
				List<String> highDays = daypass.getSpecialDate();
				if(highpriceDay.contains(cal.get(Calendar.DAY_OF_WEEK)) || highDays.contains(d.replace("-", "/"))){
					itemNo = itemNos[0];
					oneDayPrice = daypass.getHighRate().multiply(quantity);
				} else {
					itemNo = itemNos[1];
					oneDayPrice = daypass.getLowRate().multiply(quantity);
				}
				if(null == purchaseDto.getOneDayPrice()){
					purchaseDto.setOneDayPrice(oneDayPrice);
				} else {
					purchaseDto.setOneDayPrice(purchaseDto.getOneDayPrice().add(oneDayPrice));
				}
				purchaseDto.setItemNo(itemNo);
				if(itemMap.get(itemNo) !=null){
//					CustomerOrderDet det = itemMap.get(itemNo);
//					det.setOrderQty(buyNo.intValue()+det.getOrderQty());
//					det.setItemTotalAmout(oneDayPrice.add(det.getItemTotalAmout()));
//					itemMap.put(itemNo, det);
					CustomerOrderDetDto detDto = itemMap.get(itemNo);
					detDto.setOrderQty(buyNo.intValue()+detDto.getOrderQty());
					detDto.setItemTotalAmout(oneDayPrice.add(detDto.getItemTotalAmout()));
					detDto.setPlanNo(planNo);
					itemMap.put(itemNo, detDto);
				}else {
//					CustomerOrderDetD det  = new CustomerOrderDet();
//					det.setOrderQty(buyNo.intValue());
//					det.setItemTotalAmout(oneDayPrice);
					CustomerOrderDetDto detDto  = new CustomerOrderDetDto();
					detDto.setOrderQty(buyNo);
					detDto.setItemTotalAmout(oneDayPrice);
					detDto.setPlanNo(planNo);
					itemMap.put(itemNo, detDto);
				}
			}
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS, itemMap);
		return this.responseResult;
	}

	private String[] getPriceItemNos(Long planNo) {
		String[] highLowItemNo = null;
		try {
			List<PosServiceItemPrice> itemNos = this.servicePlanDao.getDaypassPriceItemNo(planNo, Constant.ServiceplanType.DAYPASS.toString());
			highLowItemNo = new String[2];
			for(PosServiceItemPrice item : itemNos){
				if(item.getItemNo().startsWith(Constant.SRV_HI_RATE_ITEM_PREFIX_DP.toString())){
					highLowItemNo[0] = item.getItemNo();
				} else {
					highLowItemNo[1] = item.getItemNo();
				}
			}
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
			throw new RuntimeErrorException(null, "daypass planNo:"+planNo+" data is not integrity");
		}
		
		return highLowItemNo;
	}

	private List<Integer> getHighpriceDay(ServicePlanDto daypass) {
		String weeklyRepeatBit = daypass.getWeeklyRepeatBit();
		List<String> specialDate = daypass.getSpecialDate();
		List<Integer> highpriceDay = new ArrayList<Integer>();
		char[] week = weeklyRepeatBit.toCharArray();
		if('1' == week[Calendar.SUNDAY-1]){
			highpriceDay.add(Calendar.SUNDAY);
		}
		if('1' == week[Calendar.MONDAY-1]){
			highpriceDay.add(Calendar.MONDAY);
		}
		if('1' == week[Calendar.TUESDAY-1]){
			highpriceDay.add(Calendar.TUESDAY);
		}
		if('1' == week[Calendar.WEDNESDAY-1]){
			highpriceDay.add(Calendar.WEDNESDAY);
		}
		if('1' == week[Calendar.THURSDAY-1]){
			highpriceDay.add(Calendar.THURSDAY);
		}
		if('1' == week[Calendar.FRIDAY-1]){
			highpriceDay.add(Calendar.FRIDAY);
		}
		if('1' == week[Calendar.SATURDAY-1]){
			highpriceDay.add(Calendar.SATURDAY);
		}
		return highpriceDay;
	}

	private Long getServiceplanQuota(Long customerId) {
		String quotaStr = servicePlanDao.getServiceplanQuotaById(customerId);
		if(null == quotaStr){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"member_limit_rule table Day Pass Purchasing record is null. Customer_id: "+ customerId});
		}
		Long memberQuota = (long) 0;
		try {
			memberQuota = Long.parseLong(quotaStr);
		}catch(Exception e){
			logger.error(e);
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"Day Pass Purchasing format is not right "+ quotaStr});
		}
		return memberQuota;
	}

	private boolean checkMemberQuota(Map<String, OneDayDaypassPurchaseDto> map,
			Long sum, Long memberQuota) {
		boolean buyFlag = true;
		List<String> errorMsg = new ArrayList<String>();
		if(memberQuota<sum){
			buyFlag = false;
		}
		for(String d : map.keySet()){
			Long total = sum;
			OneDayDaypassPurchaseDto dto = map.get(d);
			Long hkgta = dto.getMemberQuota();
			if(hkgta!=null){
				total = hkgta+sum;
			}
			if(total >memberQuota){
				StringBuilder msg = new StringBuilder();
				msg.append(d).append(" you have exceeded your day pass quotas.").append("(").append(memberQuota).append(" passes per day)");
				errorMsg.add(msg.toString());
				dto.setErrorMsg(msg.toString());
				buyFlag = false;
				dto.setMemberFlag(Constant.Purchase_Flag_Type.FAIL.toString());
			}else{
				dto.setMemberFlag(Constant.Purchase_Flag_Type.OK.toString());
			}
		}
		
		return buyFlag;
	}
	private  int daysBetween(Date smdate,Date bdate) {  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(smdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);      
       return Integer.parseInt(String.valueOf(between_days));     
	}  
	private Map<String, OneDayDaypassPurchaseDto> obtainMemberQuota(DaypassPurchaseDto dto, Map<String, OneDayDaypassPurchaseDto> map,int total  ) {
		String format = "yyyy-MM-dd";
//			StringBuilder sql = new StringBuilder("select count(*) as memberQuota , det.create_date as oneday from ")
//			.append(" (select c.order_no as order_no  from customer_order_hd c  where 1=1  and c.customer_id=? ) orderNos, ")
//			.append(" customer_order_det det ")
//			.append(" where 	orderNos.order_no=det.order_no  ")
//			.append(" and  exists ( select p.item_no from service_plan s, service_plan_pos sp, service_plan_rate_pos pos, pos_service_item_price p ")
//			.append(" where s.plan_no = sp.plan_no and sp.serv_pos_id=pos.serv_pos_id ")
//			.append(" and (p.item_no=sp.pos_item_no or p.item_no=pos.pos_item_no) and s.plan_no=?  ) ")
//			.append("and det.create_date between STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s') and STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s') ")
//			.append(" group by det.create_date  ");
//			StringBuilder sql = new StringBuilder(" select copc.effective_from as activationDate , copc.effective_to as deactivationDate , count(1) as totalMemberQuota ")
//			.append(" from service_plan sp inner join service_plan_pos spp on sp.plan_no = spp.plan_no and  sp.plan_no=?")
//			.append(" inner join service_plan_rate_pos sprp on spp.serv_pos_id = sprp.serv_pos_id ")
//			.append(" inner join pos_service_item_price psip on (psip.item_no = spp.pos_item_no or psip.item_no = sprp.pos_item_no) ")
//			.append(" inner join customer_order_det cod on cod.item_no = psip.item_no ")
//			.append(" inner join customer_order_hd coh on cod.order_no = coh.order_no and coh.customer_id = ? ")
//			.append(" inner join customer_order_permit_card copc on cod.order_no = copc.customer_order_no and copc.order_det_id = cod.order_det_id ")
//			.append(" where copc.effective_from between STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s') and STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s') ")
//			.append(" and copc.effective_to between STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s') and STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s')  ")
//			.append(" group by copc.effective_from, copc.effective_to ");
		Calendar cal = Calendar.getInstance();  
		for (int j = 0; j <= total; j++) {
			cal.setTime(DateConvertUtil.parseString2Date(dto.getActivationDate(), format));
			cal.add(Calendar.DATE, j);
			Date current = cal.getTime();
			StringBuilder sql = new StringBuilder("select count(*) as memberQuota2 ,  '").append(CommUtil.toChar(current, format)).append("' as oneday from ")
				.append(" customer_order_hd  coh inner join customer_order_permit_card copc on coh.order_no=copc.customer_order_no  ")
				.append("  where coh.order_status=? and coh.customer_id=? ")
				.append(" and '").append(CommUtil.toChar(current, format)).append("' >= copc.effective_from   ")
				.append(" and '").append(CommUtil.toChar(current, format)).append("' <=copc.effective_to  ")
			    .append(" group by  oneday ");
				System.out.println("obtainMemberQuota: \n"+ sql.toString());
				List<Serializable> param = new ArrayList<Serializable>();
				param.add(Constant.Status.CMP.toString());
				//param.add(dto.getCustomerId());
				param.add(Long.parseLong(dto.getId()));
				@SuppressWarnings("unchecked")
				List<OneDayDaypassPurchaseDto> temp = this.commonByHQLDao.getDtoBySql(sql.toString(), param, OneDayDaypassPurchaseDto.class);
				System.out.println(temp);
				if(temp !=null && temp.size()>0){
					for(OneDayDaypassPurchaseDto tempDto : temp){
						String d = tempDto.getOneday();
						if(map.get(d)==null){
							map.put(d, tempDto);
						} else {
							OneDayDaypassPurchaseDto exist = map.get(d);
							if(exist.getMemberQuota() !=null){
								exist.setMemberQuota(exist.getMemberQuota()+tempDto.getMemberQuota());
							} else {
								exist.setMemberQuota(tempDto.getMemberQuota());
							}
						}
					}
				}
		}
		return map;
	}


	private boolean checkGolbalHkgtaQuota(Map<String, OneDayDaypassPurchaseDto> map, Long sum, Long golbalHkgtaQuota) {
		Long total = sum;
		boolean buyFlag = true;
		List<String> errorMsg = new ArrayList<String>();
		if(golbalHkgtaQuota<total){
			buyFlag = false;
		}
		for(String d : map.keySet()){
			OneDayDaypassPurchaseDto dto = map.get(d);
			Long hkgta = dto.getHkgtaQuota();
			if(hkgta!=null){
				total = hkgta+sum;
			}
			if(total >golbalHkgtaQuota){
				StringBuilder msg = new StringBuilder();
				//msg.append(d).append(" not enough quota for your selected date. Please select another date.").append("(HKGTA ").append(golbalHkgtaQuota).append(" passes per day");
				msg.append(d).append(" not enough quota for your selected date. Please select another date.");
				errorMsg.add(msg.toString());
				dto.setErrorMsg(msg.toString());
				dto.setHkgtaFlag(Constant.Purchase_Flag_Type.FAIL.toString());
				buyFlag = false;
			}else{
				dto.setHkgtaFlag(Constant.Purchase_Flag_Type.OK.toString());
			}
			
		}
		if(!buyFlag){
			throw new GTACommonException(GTAError.DayPassPurchaseError.HKGTA_QUOTA_NOT_ENOUGH,errorMsg.toString());
		}
		return buyFlag;
		
	}
	private void memberQuota(){
		StringBuilder sql = new StringBuilder("set @i=:-1; ")
			.append(" select cetDays.d as oneday , ")
			.append(" if (cetDays.d >= cetTotal.effective_from and cetDays.d <= cetTotal.effective_to, cetTotal.c, 0) as memberQuota ")
			.append(" from ( select copc.effective_from, copc.effective_to, count(1) as c ")
			.append(" from service_plan sp inner join service_plan_pos spp on sp.plan_no = spp.plan_no ")
			.append(" inner join service_plan_rate_pos sprp on spp.serv_pos_id = sprp.serv_pos_id ")
			.append(" inner join pos_service_item_price psip on (psip.item_no = spp.pos_item_no or psip.item_no = sprp.pos_item_no) ")
			.append(" inner join customer_order_det cod on cod.item_no = psip.item_no ")
			.append(" inner join customer_order_hd coh on cod.order_no = coh.order_no and coh.customer_id = 1 ")
			.append(" inner join customer_order_permit_card copc on cod.order_no = copc.customer_order_no and copc.order_det_id = cod.order_det_id ")
			.append(" group by copc.effective_from, copc.effective_to ")
			.append(" ) cetTotal, ")
			.append(" ( select date(date_add(str_to_date('2015-07-18', '%Y-%m-%d'), interval @i:=@i + 1 day)) as d ")
			.append(" from ( select 1 from service_plan s limit 1, 4 ) t ")
			.append(" ) cetDays; ")
			.append(" set @i=:-1; ");
		List<OneDayDaypassPurchaseDto> temp = this.commonByHQLDao.getDtoBySql(sql.toString(), null, OneDayDaypassPurchaseDto.class);
	}
	//obtain everyday hkgta have already sell daypass quota
	private Map<String, OneDayDaypassPurchaseDto> obtainHkgtaQuota(DaypassPurchaseDto dto,int total) {
		Map<String, OneDayDaypassPurchaseDto> map = new HashMap<String, OneDayDaypassPurchaseDto>();
		String format = "yyyy-MM-dd";
		Calendar cal = Calendar.getInstance();  
		for (int j = 0; j <= total; j++) {
			cal.setTime(DateConvertUtil.parseString2Date(dto.getActivationDate(), format));
			cal.add(Calendar.DATE, j);
			Date current = cal.getTime();
			StringBuilder sql = new StringBuilder("select count(*) as hkgtaQuota2 ,  '").append(CommUtil.toChar(current, format)).append("' as oneday from ")
				.append(" customer_order_hd  coh inner join customer_order_permit_card copc on coh.order_no=copc.customer_order_no  ")
				.append("  where coh.order_status=?  ")
				.append(" and '").append(CommUtil.toChar(current, format)).append("' >= copc.effective_from   ")
				.append(" and '").append(CommUtil.toChar(current, format)).append("' <=copc.effective_to  ")
			    .append(" group by  oneday");
			System.out.println("obtainMemberQuota: \n"+ sql.toString());
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(Constant.Status.CMP.toString());
			@SuppressWarnings("unchecked")
			List<OneDayDaypassPurchaseDto> temp = this.commonByHQLDao.getDtoBySql(sql.toString(), param, OneDayDaypassPurchaseDto.class);
			System.out.println(temp);
			if(temp !=null && temp.size()>0){
				for(OneDayDaypassPurchaseDto tempDto : temp){
					String d = tempDto.getOneday();
					if(map.get(d)==null){
						map.put(d, tempDto);
					} else {
						OneDayDaypassPurchaseDto exist = map.get(d);
						if(exist.getHkgtaQuota() !=null){
							exist.setHkgtaQuota(exist.getHkgtaQuota()+tempDto.getHkgtaQuota());
						}else {
							exist.setHkgtaQuota(tempDto.getHkgtaQuota());
						}
					}
				}
			}
		}
		return map;
	}

	@Override
	@Transactional
	public boolean changeDaypassCardStatus(CustomerOrderPermitCardDto dto) {
		String status = dto.getStatus();
		if (status != null && status.trim().length() != 0 && CustomerOrderPermitCardEnumStatus.isValid(status)) {
			Long orderDetId = dto.getOrderDetId();
			CustomerOrderPermitCard daypassCard = (CustomerOrderPermitCard) commonByHQLDao.get(CustomerOrderPermitCard.class, orderDetId);
			if(daypassCard!=null){
				 daypassCard.setStatus(status);
			/*	 if need update operator and date 
				 CustomerOrderDet customerOrderDet = daypassCard.getCustomerOrderDet();
				 customerOrderDet.setUpdateBy("HAHA");
				 customerOrderDet.setUpdateDate(new Date());
			     commonByHQLDao.save(daypassCard);*/
				return true;
			}
		}
		return false;	}

	@Override
	@Transactional
	public ResponseResult saveDayPassPurchase(DaypassPurchaseDto daypassPurchaseDto,String userId) {
		ResponseResult result = this.checkAvailability(daypassPurchaseDto);
		if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
			return result;
		}
		DaypassPurchaseDto dto = (DaypassPurchaseDto) result.getData();
		CustomerOrderHd orderHd = new CustomerOrderHd();
		setSimpleVaule4orderHd(dto, orderHd,userId);
		//this.customerOrderHdDao.save(orderHd);
		//establish relations between CustomerOrderHd  CustomerOrderPermitCard and CustomerOrderDet
		setDet4orderHd(dto, orderHd,userId);
		this.customerOrderHdDao.save(orderHd);
		dto.setOrderNo(orderHd.getOrderNo());
		List<CustomerOrderDet> orderDets = orderHd.getCustomerOrderDets();
//		Map<String, CustomerOrderDetDto> itemMap = new HashMap<String, CustomerOrderDetDto>();
//		for(CustomerOrderDet det : orderDets){
//			CustomerOrderDetDto detDto = new CustomerOrderDetDto();
//			detDto.setItemTotalAmout(det.getItemTotalAmout());
//			detDto.setOrderQty(det.getOrderQty());
//			itemMap.put(det.getItemNo(), detDto);
//		}
		Map<String, CustomerOrderDetDto> itemMap = getPurchaseDaypassList(orderHd.getOrderNo());
		dto.setItemMap(itemMap);
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
	}

	private void setDet4orderHd(DaypassPurchaseDto dto, CustomerOrderHd orderHd,String userId) {
		List<CustomerOrderDet> customerOrderDets = new ArrayList<CustomerOrderDet>();
		List<CustomerOrderPermitCard> cards = new ArrayList<CustomerOrderPermitCard>();
		Map<String, CustomerOrderDetDto> map = dto.getItemMap();
		for(String itemNo : map.keySet()){
			CustomerOrderDetDto temp = map.get(itemNo);
			CustomerOrderDet det = new CustomerOrderDet();
			det.setCustomerOrderHd(orderHd);
			det.setItemNo(itemNo);
			det.setItemTotalAmout(temp.getItemTotalAmout());
			//maybe need to change 
			det.setCreateBy(userId);
			//maybe need to change 
			det.setUpdateBy(userId);
			det.setCreateDate(new Timestamp(System.currentTimeMillis()));
			det.setUpdateDate(new Date());
			det.setOrderQty(temp.getOrderQty());
			this.customerOrderDetDao.save(det);
			customerOrderDets.add(det);
		}
		Long[] buysNos = dto.getBuysNos();
		Long[] daypassNos = dto.getDaypassNos();
		List<CandidateCustomerDto> candidateCustomerDtos = dto.getCandidateCustomerDtos();
//////////delete
//		Long sum = (long) 0;
//		for(Long temp : buysNos){
//			sum = sum + temp;
//		}
//		for(long i=0;i<sum;i++){
//			CustomerOrderPermitCard card = new CustomerOrderPermitCard();
//			card.setCustomerOrderHd(orderHd);
//			card.setEffectiveFrom(DateConvertUtil.parseString2Date(dto.getActivationDate(), format));
//			card.setEffectiveTo(DateConvertUtil.parseString2Date(dto.getDeactivationDate(), format));
//			card.setStatus(Constant.Status.PND.toString());
//			//the guest customer_id   customer_profile.customer_id
//			//card.setCardholderCustomerId(cardholderCustomerId);
//			this.customerOrderPermitCardDao.save(card);
//			cards.add(card);
//		}
//////////delete
		int k =0;
		for(int i=0;i<buysNos.length;i++){
			Long planNo = daypassNos[i];
			for(int j=0;j<buysNos[i];j++){
				CustomerOrderPermitCard card = new CustomerOrderPermitCard();
				card.setCustomerOrderHd(orderHd);
				card.setEffectiveFrom(DateConvertUtil.parseString2Date(dto.getActivationDate(), format));
				card.setEffectiveTo(DateConvertUtil.parseString2Date(dto.getDeactivationDate(), format));
				card.setStatus(Constant.Status.PND.toString());
				card.setServicePlanNo(planNo);
				if (null != candidateCustomerDtos && candidateCustomerDtos.size() >0) {
					CandidateCustomerDto candidateCustomerDto = candidateCustomerDtos.get(k);
					CandidateCustomer candidateCustomer = new CandidateCustomer();
					candidateCustomer.setCreateBy(userId);
					candidateCustomer.setCreateDate(new Timestamp(System.currentTimeMillis()));
					candidateCustomer.setGivenName(candidateCustomerDto.getGivenName());
					candidateCustomer.setGivenNameNls(candidateCustomerDto.getGivenNameNls());
					candidateCustomer.setSurname(candidateCustomerDto.getSurname());
					candidateCustomer.setSurnameNls(candidateCustomerDto.getSurnameNls());
					candidateCustomer.setPhoneMobile(candidateCustomerDto.getPhoneMobile());
					candidateCustomer.setCustomerType("DP");
					candidateCustomerDao.save(candidateCustomer);
					card.setCandidateCustomer(candidateCustomer);
				}
				this.customerOrderPermitCardDao.save(card);
				cards.add(card);
				k++;
			}
		}
		orderHd.setCustomerOrderDets(customerOrderDets);
		orderHd.setCustomerOrderPermitCards(cards);
		this.customerOrderHdDao.saveOrUpdate(orderHd);
	}

	private void setSimpleVaule4orderHd(DaypassPurchaseDto dto,
			CustomerOrderHd orderHd,String userId) {
		orderHd.setOrderDate(new Date());
		orderHd.setOrderStatus(Constant.Status.OPN.toString());
		if(Constant.Purchase_Daypass_Type.Member.toString().equals(this.buyerType(dto.getId()))){
			dto.setIsMember(1);
		//if(1 == dto.getIsMember()){
			//orderHd.setCustomerId(dto.getCustomerId());
			orderHd.setCustomerId(Long.parseLong(dto.getId()));
		} else {
			dto.setIsMember(0);
			orderHd.setStaffUserId(userId);
		}
		orderHd.setOrderTotalAmount(dto.getTotalPrice());
		orderHd.setCreateBy(userId);
		orderHd.setUpdateBy(userId);
		orderHd.setCreateDate(new Timestamp(System.currentTimeMillis()));
		orderHd.setUpdateDate(new Date());
		this.customerOrderHdDao.saveOrUpdate(orderHd);
	}
	@Override
	@Transactional
	public ResponseResult purchaseDaypass(DaypassPurchaseDto daypassPurchaseDto,String userId) {
		Long ofPass = (long) 0;
		
		if(null == daypassPurchaseDto.getOrderNo()){
			//first save then pay 
			ResponseResult result = this.saveDayPassPurchase(daypassPurchaseDto,userId);
			if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
				return result;
			}
			DaypassPurchaseDto dto = (DaypassPurchaseDto) result.getData();
			Long[] buysNos = dto.getBuysNos();
			for(Long temp : buysNos){
				ofPass = ofPass + temp;
			}
			if(Constant.Purchase_Daypass_Type.Member.toString().equals(this.buyerType(dto.getId()))){
				Long customerId = Long.parseLong(dto.getId());
				dto.setMemberName(this.getMemberName(customerId));
				dto.setIsMember(1);
				Member member = this.memberDao.get(Member.class, Long.parseLong(dto.getId()));
				dto.setAcdemyID(member.getAcademyNo());
				String type = member.getMemberType();
				MemberDto memberDto = null;
				MemberDto pmemberDto = null;
				BigDecimal cashvalue = BigDecimal.ZERO;
				if(Constant.memberType.CPM.toString().equals(type) || Constant.memberType.IPM.toString().equals(type)){
					memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.CR.name());
					cashvalue = memberDto.getAvailableBalance();
					dto.setCreditLimit(memberDto.getCreditLimit());
				}else if(Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)){
					memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.TRN.name());
					pmemberDto = this.memberDao.getMemberCashvalue(member.getSuperiorMemberId(),LimitType.CR.name());
					cashvalue = pmemberDto.getAvailableBalance();
					dto.setSpendingLimit(memberDto.getCreditLimit());
					dto.setCreditLimit(pmemberDto.getCreditLimit());
				}
				dto.setCurrentCashvalue(cashvalue);
			}else{
				StaffDto staffDto = this.userMasterDao.getStaffDto(dto.getId());
				
				String memberNameString ="";
				if(StringUtils.isNotBlank(staffDto.getStaffName())){
					memberNameString = staffDto.getStaffName().trim();
				} else if(StringUtils.isNotBlank(staffDto.getNickname())){
					memberNameString = staffDto.getNickname().trim();
				}else {
					memberNameString = staffDto.getUserId();
				}
				
				dto.setMemberName(memberNameString);
				dto.setIsMember(0); 
			}
			daypassPurchaseDto = dto;
		} else {
			CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, daypassPurchaseDto.getOrderNo());
			List<CustomerOrderPermitCard> cards = orderHd.getCustomerOrderPermitCards();
			if(null == cards || cards.size()<=0){
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderPermitCard"});
			}
			ofPass = (long) cards.size();
			CustomerOrderPermitCard card  = cards.get(0);
			daypassPurchaseDto.setActivationDate(DateConvertUtil.date2String(card.getEffectiveFrom(), format) );
			daypassPurchaseDto.setDeactivationDate(DateConvertUtil.date2String(card.getEffectiveTo(), format));
			daypassPurchaseDto.setTotalPrice(orderHd.getOrderTotalAmount());
	//////////Refactor
			Long golbalHkgtaQuota = getGolbalHkgtaQuota(ofPass);
			int total =  this.daysBetween(DateConvertUtil.parseString2Date(daypassPurchaseDto.getActivationDate(), format), DateConvertUtil.parseString2Date(daypassPurchaseDto.getDeactivationDate(), format)) ;
			Map<String, OneDayDaypassPurchaseDto> map  = obtainHkgtaQuota(daypassPurchaseDto, total);
			checkGolbalHkgtaQuota(map, ofPass, golbalHkgtaQuota);
			if(orderHd.getStaffUserId()!=null){
				//TODO 
				//check
				daypassPurchaseDto.setId(orderHd.getStaffUserId());
				StaffDto staffDto = this.userMasterDao.getStaffDto(orderHd.getStaffUserId());
				
				String staffName = "";
				
				if(StringUtils.isNotBlank(staffDto.getStaffName())){
					staffName = staffDto.getStaffName().trim();
				} else if(StringUtils.isNotBlank(staffDto.getNickname())){
					staffName = staffDto.getNickname().trim();
				}else {
					staffName = staffDto.getUserId();
				}
				daypassPurchaseDto.setMemberName(staffName);
				daypassPurchaseDto.setIsMember(0);
			}else if(orderHd.getCustomerId()!=null){
				Long customerId = orderHd.getCustomerId();
				daypassPurchaseDto.setId(String.valueOf(customerId));
				daypassPurchaseDto.setMemberName(this.getMemberName(customerId));
				daypassPurchaseDto.setIsMember(1);
				Member member = this.memberDao.get(Member.class, customerId);
				daypassPurchaseDto.setAcdemyID(member.getAcademyNo());
				Long memberQuota = getServiceplanQuota(orderHd.getCustomerId());
				obtainMemberQuota(daypassPurchaseDto, map,total);
				checkMemberQuota(map, ofPass, memberQuota);
				String type = member.getMemberType();
				MemberDto memberDto = null;
				MemberDto pmemberDto = null;
				BigDecimal cashvalue = BigDecimal.ZERO;
				if(Constant.memberType.CPM.toString().equals(type) || Constant.memberType.IPM.toString().equals(type)){
					memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.CR.name());
					cashvalue = memberDto.getRemainSum();
					daypassPurchaseDto.setCreditLimit(memberDto.getCreditLimit());
				}else if(Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)){
					memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.TRN.name());
					pmemberDto = this.memberDao.getMemberCashvalue(member.getSuperiorMemberId(),LimitType.CR.name());
					cashvalue = pmemberDto.getRemainSum();
					daypassPurchaseDto.setSpendingLimit(memberDto.getCreditLimit());
					daypassPurchaseDto.setCreditLimit(pmemberDto.getCreditLimit());
				}
				daypassPurchaseDto.setCurrentCashvalue(cashvalue);
			}
		}
		daypassPurchaseDto.setOfPass(ofPass);
//		ResponseResult result = checkCashvalue(daypassPurchaseDto);
//		if(GTAError.DayPassPurchaseError.NONEEDPAY.getCode().equals(result.getReturnCode())){
//			Map<String, CustomerOrderDetDto> itemMap = getPurchaseDaypassList(daypassPurchaseDto.getOrderNo());
//			daypassPurchaseDto.setItemMap(itemMap);
//			this.responseResult.initResult(GTAError.Success.SUCCESS, daypassPurchaseDto);
//			return this.responseResult;
//		}else {
//			
//		}
////		else if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
////			return result;
////		}
//		if(!GTAError.CommomError.DATA_ISSUE.getCode().equals(result.getReturnCode())){
//			if(Constant.Purchase_Daypass_Type.Member.toString().equals(this.buyerType(daypassPurchaseDto.getId()))){
//				//if(1==dto.getIsMember()){
//				MemberDto memberDto = (MemberDto) result.getData();
//				daypassPurchaseDto.setCurrentCashvalue(memberDto.getAvailableBalance());
//				daypassPurchaseDto.setCreditLimit(memberDto.getCreditLimit());
//			}
//		}
		
		Map<String, CustomerOrderDetDto> itemMap = getPurchaseDaypassList(daypassPurchaseDto.getOrderNo());
		daypassPurchaseDto.setItemMap(itemMap);
		
		
		this.responseResult.initResult(GTAError.Success.SUCCESS, daypassPurchaseDto);
		return this.responseResult;
	}
	
	/**   
	* @author: Zero_Wang
	* @since: Sep 17, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	public Map<String, CustomerOrderDetDto> getPurchaseDaypassList(Long orderNo) {
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		List<CustomerOrderPermitCard> cards = orderHd.getCustomerOrderPermitCards();
		if(null == cards || cards.size()<=0){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderPermitCard"});
		}
		Map<Long,Long> map = new HashMap<Long,Long>();
		for(CustomerOrderPermitCard c : cards){
			Long daypassNo = c.getServicePlanNo();
			if(null==map.get(daypassNo)){
				map.put(daypassNo, 1L);
			}else {
				map.put(daypassNo, map.get(daypassNo)+1);
			}
		}
		CustomerOrderPermitCard card = cards.get(0);
		Map<String, CustomerOrderDetDto> itemMap = new HashMap<String, CustomerOrderDetDto>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		Calendar cal = Calendar.getInstance();  
		for(Long daypassNo : map.keySet()){
			ServicePlanDto planDto = this.servicePlanService.getDaypassById(daypassNo);
			List<Integer> highpriceDay = getHighpriceDay(planDto);
			List<String> highDays = planDto.getSpecialDate();
			int total =  this.daysBetween(card.getEffectiveFrom(), card.getEffectiveTo()) ;
			BigDecimal totalAmout = BigDecimal.ZERO;
			for(int i=0;i<=total;i++){
				cal.setTime(card.getEffectiveFrom());
				cal.add(Calendar.DATE, i);
				String d = sdf.format(cal.getTime());
				if(highpriceDay.contains(cal.get(Calendar.DAY_OF_WEEK)) || highDays.contains(d.replace("-", "/"))){
					totalAmout = planDto.getHighRate().add(totalAmout);
				} else {
					totalAmout = planDto.getLowRate().add(totalAmout);
				}
			}
			if(null==itemMap.get(daypassNo+"")){
				CustomerOrderDetDto detDto = new CustomerOrderDetDto();
				detDto.setItemTotalAmout(totalAmout);
				detDto.setOrderQty(map.get(daypassNo));
				detDto.setPlanNo(daypassNo);
				List<SysCode> ageRange=null;
				try {
					 ageRange = sysCodeDao.selectSysCodeByCategory("ageRange");
				} catch (Exception e) {
					logger.error("getServiceplanByDate get syscode failed!",e);
				}
				for (SysCode sysCode : ageRange) {
					if(StringUtils.equals(sysCode.getCodeValue(),planDto.getAgeRange().getAgeRangeCode())){
						detDto.setShowName(planDto.getPlanName()+"-"+sysCode.getCodeDisplay());
					}
				}
				itemMap.put(daypassNo+"", detDto);
			}else{
				CustomerOrderDetDto detDto = itemMap.get(daypassNo+"");
				detDto.setItemTotalAmout(detDto.getItemTotalAmout().add(totalAmout));
			}
		}
		return itemMap;
	}

	private ResponseResult checkCashvalue(DaypassPurchaseDto dto) {
		//member purchase daypass
		if(Constant.Purchase_Daypass_Type.Member.toString().equals(this.buyerType(dto.getId()))){
		//if(1== dto.getIsMember()){
			//Long customerId = dto.getCustomerId();
			dto.setIsMember(1);
			Long customerId = Long.parseLong(dto.getId());
			BigDecimal totalPrice = dto.getTotalPrice();
			Member  member = this.memberDao.get(Member.class, customerId);
			if(null == member){
				this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"Member:" + customerId});
				return this.responseResult;
			}
			BigDecimal cashvalue = BigDecimal.ZERO;
			String type = member.getMemberType();
			MemberDto memberDto = null;
			MemberDto pmemberDto = null;
			if(Constant.memberType.CPM.toString().equals(type) || Constant.memberType.IPM.toString().equals(type)){
				memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.CR.name());
				if(null == memberDto){
					this.responseResult.initResult(GTAError.DayPassPurchaseError.CREDITLIMIT_NOT_ENOUGH);
					return this.responseResult;
				}
				
				this.responseResult.initResult(GTAError.Success.SUCCESS, memberDto);
				cashvalue = memberDto.getRemainSum();
			}else if(Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)){
				memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.TRN.name());
				if(memberDto.getCreditLimit()!=null){
					if(dto.getTotalPrice().compareTo(memberDto.getCreditLimit()) > 0){
						this.responseResult.initResult(GTAError.DayPassPurchaseError.SpendLimit_NOT_ENOUGH);
						return this.responseResult;
					}
				}
				pmemberDto = this.memberDao.getMemberCashvalue(member.getSuperiorMemberId(),LimitType.CR.name());
				if(null == pmemberDto){
					this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"Member's customerId: "+customerId+" superior member not exist. Member" });
					return this.responseResult;
				}
				cashvalue = pmemberDto.getRemainSum();
				this.responseResult.initResult(GTAError.Success.SUCCESS, pmemberDto);
			}
			return this.responseResult;
		}else {
			//staff purchase daypass no need to pay
			this.responseResult.initResult(GTAError.DayPassPurchaseError.NONEEDPAY);
			return this.responseResult;
		}
		
	}
	@Override
	@Transactional
	public ResponseResult payment(Long orderNo,String acdemyID,String id,String memberName,String cardNo) throws Exception {
		DaypassPurchaseDto dto = new DaypassPurchaseDto();
		dto.setOrderNo(orderNo);
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if(Constant.Status.CMP.toString().equals(orderHd.getOrderStatus())){
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Already_Paid);
			return this.responseResult;
		}
		if(Constant.Status.CAN.toString().equals(orderHd.getOrderStatus())){
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Already_Canceled);
			return this.responseResult;
		}
		List<CustomerOrderPermitCard> cards = orderHd.getCustomerOrderPermitCards();
		if(null == cards || cards.size()<=0){
			//TODO
			this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE,"CustomerOrderPermitCard");
			return this.responseResult;
		}
		Long sum = (long) cards.size();
		dto.setOfPass(sum);
		CustomerOrderPermitCard card  = cards.get(0);
		if(card.getEffectiveTo().before(new Date(System.currentTimeMillis()-1000*60*60*24))){
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Already_Expired);
			return this.responseResult;
		}
		dto.setActivationDate(DateConvertUtil.date2String(card.getEffectiveFrom(), format) );
		dto.setDeactivationDate(DateConvertUtil.date2String(card.getEffectiveTo(), format) );
		dto.setTotalPrice(orderHd.getOrderTotalAmount());
		
		Long golbalHkgtaQuota = getGolbalHkgtaQuota(sum);
		int total =  this.daysBetween(DateConvertUtil.parseString2Date(dto.getActivationDate(), format), DateConvertUtil.parseString2Date(dto.getDeactivationDate(), format)) ;
		Map<String, OneDayDaypassPurchaseDto> map  = obtainHkgtaQuota(dto, total);
		checkGolbalHkgtaQuota(map, sum, golbalHkgtaQuota);
		if(orderHd.getStaffUserId()!=null){
			dto.setId(orderHd.getStaffUserId());
			StaffDto staffDto = this.userMasterDao.getStaffDto(orderHd.getStaffUserId());
			
			String staffName = "";
			
			if(StringUtils.isNotBlank(staffDto.getStaffName())){
				staffName = staffDto.getStaffName().trim();
			} else if(StringUtils.isNotBlank(staffDto.getNickname())){
				staffName = staffDto.getNickname().trim();
			}else {
				staffName = staffDto.getUserId();
			}
			dto.setMemberName(staffName);
		}else if(orderHd.getCustomerId()!=null){
			Long customerId = Long.parseLong(id);
			if(!customerId.equals(orderHd.getCustomerId())){
				this.responseResult.initResult(GTAError.DayPassPurchaseError.ORDER_BELONGS_TO_OTHER, new String[] { memberName, acdemyID });
				return this.responseResult;
			}
			dto.setAcdemyID(acdemyID);
			dto.setId(String.valueOf(orderHd.getCustomerId()));
			dto.setMemberName(memberName);
			Long memberQuota = getServiceplanQuota(orderHd.getCustomerId());
			obtainMemberQuota(dto, map,total);
			checkMemberQuota(map, sum, memberQuota);
		}
		
		ResponseResult result = checkCashvalue(dto);
		if(GTAError.DayPassPurchaseError.NONEEDPAY.getCode().equals(result.getReturnCode())){
			//TODO
			this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return this.responseResult;
		}
		//if(GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
		if(!GTAError.CommomError.DATA_ISSUE.getCode().equals(result.getReturnCode())){
			MemberDto memberDto = (MemberDto) result.getData();
			BigDecimal cashvalue = memberDto.getRemainSum();
			BigDecimal totalPrice = orderHd.getOrderTotalAmount();
			//compare BigDecimal use compareTo(),when left>right return 1 lef=right return 0 , left<right return -1
//			if(cashvalue.compareTo(totalPrice)<0){
//				//TODO
//				this.responseResult.initResult(GTAError.DayPassPurchaseError.CASHVALUE_NOT_ENOUGH);
//				return this.responseResult;
//			}
			dto.setCurrentCashvalue(memberDto.getAvailableBalance());
			dto.setCreditLimit(memberDto.getCreditLimit());
		}
//		if(GTAError.CommomError.DATA_ISSUE.getCode().equals(result.getReturnCode())){
//			return this.responseResult;
//		}
//		if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
//			return this.responseResult;
//		}
		Map<String, CustomerOrderDetDto> itemMap = getPurchaseDaypassList(orderNo);
		dto.setItemMap(itemMap);
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
	}
	@Override
	@Transactional
	/*
	 * (non-Javadoc)
	 * @see com.sinodynamic.hkgta.service.crm.cardmanage.DayPassPurchaseService#payDaypassPurchase(com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto)
	 * this method used for pay purchase daypass submit button really pay money
	 */
	public ResponseResult payDaypassPurchase(DaypassPurchaseDto dto,String userId,String purchasedLocationCode) {
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, dto.getOrderNo());
		orderHd.setPurchasedLocationCode(purchasedLocationCode);
		if(Constant.Status.CMP.toString().equals(orderHd.getOrderStatus())){
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Already_Paid);
			return this.responseResult;
		}
		if(Constant.Status.CAN.toString().equals(orderHd.getOrderStatus())){
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Already_Canceled);
			return this.responseResult;
		}
		List<CustomerOrderPermitCard> cards = orderHd.getCustomerOrderPermitCards();
		if(null == cards || cards.size()<=0){
			//TODO
			this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE,"CustomerOrderPermitCard");
			return this.responseResult;
		}
		if(orderHd.getStaffUserId()!=null){
			dto.setId(orderHd.getStaffUserId());
			//dto.setMemberName(getStaffName(orderHd.getStaffUserId()));
		}else if(orderHd.getCustomerId()!=null){
			dto.setId(String.valueOf(orderHd.getCustomerId()));
			//dto.setMemberName(this.getMemberName(orderHd.getCustomerId()));
		}
		CustomerOrderPermitCard card  = cards.get(0);
		dto.setDeactivationDate(DateConvertUtil.date2String(card.getEffectiveTo(), format) );
		dto.setTotalPrice(orderHd.getOrderTotalAmount());
		if(Constant.CASH_Value.equals(dto.getPaymentMethodCode())){
			ResponseResult result = checkCashvalue(dto);
			//staff purchase daypass no need to pay
			if(GTAError.DayPassPurchaseError.NONEEDPAY.getCode().equals(result.getReturnCode())){
				pay4Order(dto, orderHd,userId,purchasedLocationCode);
				this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
				return this.responseResult;
				//member purchase daypass
			}else if(GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				MemberDto memberDto = (MemberDto) result.getData();
				BigDecimal cashvalue = memberDto.getRemainSum();
				BigDecimal totalPrice = orderHd.getOrderTotalAmount();
				//compare BigDecimal use compareTo(),when left>right return 1 lef=right return 0 , left<right return -1
				if(cashvalue.compareTo(totalPrice)<0){
					//TODO
					this.responseResult.initResult(GTAError.DayPassPurchaseError.CASHVALUE_NOT_ENOUGH);
					return this.responseResult;
				}
				cashvalue = cashvalue.subtract(totalPrice);
				//MemberCashvalue mc = this.memberCashValueDao.get(MemberCashvalue.class, dto.getCustomerId());
				long customerId = Long.parseLong(dto.getId());
				Member  member = this.memberDao.get(Member.class, customerId);
				MemberCashvalue mc = null;
				MemberDto  DMdto= null;
				if (MemberType.CDM.getType().equals(member.getMemberType()) || MemberType.IDM.getType().equals(member.getMemberType())) {
					DMdto = this.memberDao.getMemberCashvalue(customerId,LimitType.TRN.name());
					if(DMdto.getCreditLimit()!=null){
						if(dto.getTotalPrice().compareTo(DMdto.getCreditLimit()) > 0){
							this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE, new String[]{"Total price can not beyond spending limit" });
							return this.responseResult;
						}
					}
					mc =  this.memberCashValueDao.get(MemberCashvalue.class, member.getSuperiorMemberId());
				}else {
					mc =  this.memberCashValueDao.get(MemberCashvalue.class, customerId);
				}
				mc.setAvailableBalance(mc.getAvailableBalance().subtract(totalPrice));
				this.memberCashValueDao.saveOrUpdate(mc);
				pay4Order(dto, orderHd,userId,purchasedLocationCode);
				this.responseResult.initResult(GTAError.Success.SUCCESS,dto);
				//some issue happen do not need to inner data into DB
			}else if(!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())){
				return this.responseResult;
			}
		}else if (Constant.CREDIT_CARD.equals(dto.getPaymentMethodCode())) {
			CustomerOrderTrans customerOrderTrans = pay4OrderByCreditCard(dto, orderHd);
			dto.setTransactionNo(customerOrderTrans.getTransactionNo());
			String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
			Map<String, String> redirectUrlMap = new HashMap<String, String>();
			redirectUrlMap.put("redirectUrl", redirectUrl);//payment by credit cart need redirect to bank, the redirectUrl is used to redirect to bank.
			redirectUrlMap.put("transactionNo", customerOrderTrans.getTransactionNo().toString());
			this.responseResult.initResult(GTAError.Success.SUCCESS,dto);
			responseResult.setData(redirectUrlMap);
		}else if(Constant.CASH.equals(dto.getPaymentMethodCode())){
			pay4Order(dto, orderHd,userId,purchasedLocationCode);
			this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		}
		
		return this.responseResult;
	}
	@Transactional
	@Override
	public void sendCancelSMS(Long orderNo) throws Exception {
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId("daypass_cancel_sms");
	
		/*
	Dear {username},your day pass is cancled sucessfully.Order ID:{Confirmation #}.Reserved Date:{Start Time} to {End Time}.No of Day Pass: {no of day pass}.Day Pass Type: {type} HKGTA
		 */
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if(null!=orderHd.getCustomerId()){
			CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, orderHd.getCustomerId());
			List<CustomerOrderPermitCard> list = orderHd.getCustomerOrderPermitCards();
			CustomerOrderPermitCard card = list.get(0);
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			StringBuilder sb = new StringBuilder();
			HashSet<String> name = new HashSet<String>();
			for(CustomerOrderPermitCard c : list){
				ServicePlan d = servicePlanDao.get(ServicePlan.class, c.getServicePlanNo());
				name.add(d.getPlanName());
			}
			for(String str : name){
				sb.append(str).append("; ");
			}
			String type = "";
			if(sb.length()>0){
				type = sb.subSequence(0, sb.length()-2).toString();
			}
			if (cp != null) {
				String mobilePhone = cp.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<String>();
				phonenumbers.add(mobilePhone);
				
				ServicePlan daypass = servicePlanDao.get(ServicePlan.class, card.getServicePlanNo());
				String message = mt.getContent().replaceAll(Constant.MessageTemplate.UserName.getName(), cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname())
						.replaceAll(Constant.MessageTemplate.Confirmation.getName(), orderNo+"")
						.replaceAll(Constant.MessageTemplate.StartTime.getName(), sdf.format(card.getEffectiveFrom()))
						.replaceAll(Constant.MessageTemplate.EndTime.getName(), sdf.format(card.getEffectiveTo()))
						.replaceAll(Constant.MessageTemplate.OffPass.getName(),list.size()+"")
						.replaceAll(Constant.MessageTemplate.Type.getName(),type);
				logger.debug("PCDayPassPurchaseServiceImpl  sendCancelSMS run start ...orderNo:"+orderNo);
				smsService.sendSMS(phonenumbers, message, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
				logger.debug("PCDayPassPurchaseServiceImpl  sendCancelSMS run end ...orderNo:"+orderNo);
				
			}
		}
	}

	@Override
	@Transactional
	public void sendEmail(DaypassPurchaseDto dto,String userId){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(EmailType.Purchase_Daypass.getFunctionId());
			CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, dto.getOrderNo());
			if(null!=orderHd.getCustomerId()){
				CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, orderHd.getCustomerId());
				List<CustomerOrderPermitCard> list = orderHd.getCustomerOrderPermitCards();
				CustomerOrderPermitCard card = list.get(0);
				ServicePlan daypass = servicePlanDao.get(ServicePlan.class, card.getServicePlanNo());
				StringBuilder sb = new StringBuilder();
				HashSet<String> name = new HashSet<String>();
				for(CustomerOrderPermitCard c : list){
					ServicePlan d = servicePlanDao.get(ServicePlan.class, c.getServicePlanNo());
					name.add(d.getPlanName());
				}
				for(String str : name){
					sb.append(str).append("; ");
				}
				String type = "";
				if(sb.length()>0){
					type = sb.subSequence(0, sb.length()-2).toString();
				}
				TransactionEmailDto emailDto = new TransactionEmailDto();
				String content = mt.getContentHtml().replaceAll(Constant.MessageTemplate.UserName.getName(), cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname())
						.replaceAll(Constant.MessageTemplate.Confirmation.getName(), dto.getOrderNo()+"")
						.replaceAll(Constant.MessageTemplate.StartTime.getName(), sdf.format(card.getEffectiveFrom()))
						.replaceAll(Constant.MessageTemplate.EndTime.getName(), sdf.format(card.getEffectiveTo()))
						.replaceAll(Constant.MessageTemplate.OffPass.getName(),list.size()+"")
						.replaceAll(Constant.MessageTemplate.Type.getName(),type);
				emailDto.setEmailContent(content);
				emailDto.setOrderNO(dto.getOrderNo());
				emailDto.setEmailType(EmailType.DAYPASSRECEIPT.name());
				emailDto.setSendTo(cp.getContactEmail());
				emailDto.setSubject(mt.getMessageSubject());
				emailDto.setTransactionNO(dto.getTransactionNo());
				LoginUserDto userDto = new LoginUserDto();
				userDto.setUserId(userId);
				userDto.setUserName("HKGTA");
				logger.debug("PCDayPassPurchaseServiceImpl  sendEmail run start ...orderNo:"+dto.getOrderNo());
				customerOrderTransService.sentTransactionEmail(emailDto, userDto);
				logger.debug("PCDayPassPurchaseServiceImpl  sendEmail run end ...orderNo:"+dto.getOrderNo());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error sent email."+ e.getMessage());
		}
	}
	
	private DaypassPurchaseDto pay4Order(DaypassPurchaseDto dto, CustomerOrderHd orderHd,String userId,String purchasedLocationCode) {
		orderHd.setOrderStatus(Constant.Status.CMP.toString());
		orderHd.setUpdateBy(userId);
		orderHd.setUpdateDate(new Date());
		orderHd.setPurchasedLocationCode(purchasedLocationCode);
		//maybe need to change
		//orderHd.setOrderTotalAmount(BigDecimal.ZERO);
		//maybe need to change
		//updateDet(orderHd);
		updateCustomerOrderPermitCard(orderHd);
		return saveCustomerOrderTrans(orderHd, dto);
	}
	private CustomerOrderTrans pay4OrderByCreditCard(DaypassPurchaseDto dto, CustomerOrderHd orderHd) {
		CustomerOrderTrans trans = new CustomerOrderTrans();
		trans.setCustomerOrderHd(orderHd);
		trans.setPaymentMethodCode(dto.getPaymentMethodCode());
		trans.setPaymentMedia(PaymentMediaType.ECR.name());
		trans.setPaymentLocationCode(orderHd.getPurchasedLocationCode());
		trans.setTransactionTimestamp(new Timestamp(System.currentTimeMillis()));
		//maybe need to change
		trans.setStatus(CustomerTransationStatus.PND.name());
		trans.setPaidAmount(orderHd.getOrderTotalAmount());
		
		this.customerOrderTransDao.save(trans);
		return trans;
	}

	

	private DaypassPurchaseDto saveCustomerOrderTrans(CustomerOrderHd orderHd, DaypassPurchaseDto dto) {
		CustomerOrderTrans trans = new CustomerOrderTrans();
		trans.setCustomerOrderHd(orderHd);
		trans.setPaymentMethodCode(dto.getPaymentMethodCode());
		if(Constant.CASH_Value.equals(dto.getPaymentMethodCode())){
			trans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(Constant.CASH.equals(dto.getPaymentMethodCode())){
			trans.setPaymentMedia(PaymentMediaType.FTF.name());
		}
		trans.setTransactionTimestamp(new Timestamp(System.currentTimeMillis()));
		trans.setPaymentLocationCode(orderHd.getPurchasedLocationCode());
		//maybe need to change
		trans.setStatus(CustomerTransationStatus.SUC.name());
		trans.setPaidAmount(orderHd.getOrderTotalAmount());
		this.customerOrderTransDao.save(trans);
		dto.setTransactionNo(trans.getTransactionNo());
		return dto;
	}

	private void updateCustomerOrderPermitCard(CustomerOrderHd orderHd) {
		List<CustomerOrderPermitCard> cards = orderHd.getCustomerOrderPermitCards();
		for(CustomerOrderPermitCard card : cards) {
			card.setStatus(Constant.Status.RA.toString());
		}
		
	}

	private void updateDet(CustomerOrderHd orderHd,String userId) {
		List<CustomerOrderDet> customerOrderDets = orderHd.getCustomerOrderDets();
		for(CustomerOrderDet det : customerOrderDets){
			det.setUpdateBy(userId);
			det.setUpdateDate(new Date());
		}
	}

	@Override
	@Transactional
	public ResponseResult getInitialQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page,String userId) {
		commonByHQLDao.getInitialQueryResultByHQL(queryDto, joinSQL, page);
		List<PaymentOrderDto> list = page.getList();
		List<PaymentOrderDto> newresult = new ArrayList<>();
		for(PaymentOrderDto p :list){
			if(p.getCustomerId()!=null	){
				//1.get customerName
				  String memberName=   (String) commonByHQLDao.getUniqueByHql("select  CONCAT(c.salutation,' ',c.givenName,' ',c.surname)   from CustomerProfile c where c.customerId="+p.getCustomerId());
				  p.setMemberName(memberName);
				//2.get cardNo and card status. Q1:when a customer get more than 1 card ?
				List<PermitCardMaster> byHql = commonByHQLDao.getByHql("from PermitCardMaster p where p.mappingCustomerId="+p.getCustomerId());
				if(byHql!=null&&!byHql.isEmpty()){
					  PermitCardMaster permitCardMaster = byHql.get(0);
					  p.setCardNo(Integer.valueOf(permitCardMaster.getCardNo()));
					  p.setCardStatus( permitCardMaster.getStatus());
				}
				newresult.add(p);
			}
		}
		if(queryDto.getCodeAscending()!=null){
			sortByColumn(queryDto, newresult);		
		}
		page.setList(newresult);
		return	 assembleResponseResult(page);
	}

	private void sortByColumn(final AdvanceQueryDto queryDto, List<PaymentOrderDto> newresult) {
			Collections.sort(newresult, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					  if(o1==null&&o2==null){
						   return 0;
					  }else if(o1==null&&o2!=null ){
						  return -1;
					  }else if(o1!=null &&o2==null){
						   return 1;
					  }else{
						  try{
							  Field o1Field = ((PaymentOrderDto)o1).getClass().getDeclaredField(queryDto.getCodeSortBy());  
							  Field o2Filed = ((PaymentOrderDto)o2).getClass().getDeclaredField(queryDto.getCodeSortBy());  
							  Object obj1 = (String) o1Field.get(o1);
							  Object  obj2 = (String) o2Filed.get(o2);
							   if(o1Field.getType().getName().equals(java.lang.Long.class.getName())){
								  return (int) ((Long)obj1-(Long)obj2);
							  }else{
								  return ((String)obj1).compareTo((String)obj2);
							  }
						  }catch(Exception e){
							  return 1 ;
						  }
					  }
				}
			});
			if(queryDto.getCodeAscending()!=null&&queryDto.getCodeAscending().indexOf(",")==-1&&!Boolean.valueOf(queryDto.getCodeAscending())){
				Collections.reverse(newresult);
			}
	}
	public ResponseResult assembleResponseResult(ListPage page) {
		ResponseResult responseResult = new ResponseResult("0", "", "", new Data());
		responseResult.getListData().setList(page.getList());
		responseResult.getListData().setTotalPage(page.getAllPage());
		responseResult.getListData().setCurrentPage(page.getNumber());
		responseResult.getListData().setPageSize(page.getSize());
		responseResult.getListData().setRecordCount(page.getAllSize());
		responseResult.getListData().setLastPage(page.isLast());
		return responseResult;
	}
	@Override
	@Transactional
	/**
	 * check member can buy daypass  or not.
	 * If member can buy daypass  then return the he can buy WD or FULL and the day  period he can buy
	 */
	public DaypassPurchaseDto checkPurchasaeDayPassLimit(Member member) {
		//1.check could buy day pass
//		Long customerId = dto.getCustomerId();
//		Member m = (Member) commonByHQLDao.get(Member.class, customerId);
//		//corporate member only  need to check member_limit_rule table
//		if(StringUtils.equals("CPM", m.getMemberType()) || StringUtils.equals("CDM", m.getMemberType())) {
//			Long quota  = this.getServiceplanQuota(customerId);
//			if(quota>0){
//				return true;
//			}
//		//IPM  IDM need to check both customer_service_acc and member_limit_rule table
//		}else {
//			String hql = "select distinct sp.planNo from  CustomerServiceAcc csa ,CustomerServiceSubscribe css,Member m ," +
//					" ServicePlan sp  where  csa.accNo=css.customerServiceAcc.accNo and css.id.servicePlanNo=sp.planNo " +
//					" and csa.status='ACT'  and (csa.customerId=m.customerId or csa.customerId=m.superiorMemberId ) and  m.customerId=" + customerId ;
//			List<Long> planNos = commonByHQLDao.getByHql(hql);
//			if(planNos!=null && planNos.size()>0){
//				Long quota  = this.getServiceplanQuota(customerId);
//				if(quota>0){
//					return true;
//				}
//			}
//			
//		}
		Long quota = 0L;
		Long customerId = member.getCustomerId();
		String type = member.getMemberType();
		CustomerServiceAcc customerServiceAcc = null;
		CorporateServiceAccDto corporateServiceAccDto = null;
		DaypassPurchaseDto dto = new DaypassPurchaseDto();
		CorporateMember cm = null;
		String passPeriodType = "";
		MemberLimitRule rule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId,PlanRight.D1.name());
		if(null!=rule && null!=rule.getNumValue() && rule.getNumValue().longValue()>0){
			if(Constant.memberType.IPM.toString().equals(type) || Constant.memberType.CPM.toString().equals(type)){
				customerServiceAcc = this.customerServiceAccDao.getAactiveByCustomerId(customerId);
				if (customerServiceAcc==null) throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, "There is something wrong with this member, please check this member's information");
				passPeriodType = customerServiceAcc.getPeriodCode();
				
			}else if(Constant.memberType.IDM.toString().equals(type) || Constant.memberType.CDM.toString().equals(type)){
				customerServiceAcc = this.customerServiceAccDao.getAactiveByCustomerId(member.getSuperiorMemberId());
				if (customerServiceAcc==null) throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, "There is something wrong with this member, please check this member's information");
				passPeriodType = customerServiceAcc.getPeriodCode();
			}
//			else if(Constant.memberType.CPM.toString().equals(type)){
//				cm = this.corporateMemberDao.getCorporateMemberById(member.getCustomerId());
//				corporateServiceAccDto = corporateServiceAccDao.getAactiveByCustomerId(cm.getCorporateProfile().getCorporateId());
//				passPeriodType = corporateServiceAccDto.getPeriodCode();
//			}else if(Constant.memberType.CDM.toString().equals(type)){
//				cm = this.corporateMemberDao.getCorporateMemberById(member.getSuperiorMemberId());
//				corporateServiceAccDto = corporateServiceAccDao.getAactiveByCustomerId(cm.getCorporateProfile().getCorporateId());
//				passPeriodType = corporateServiceAccDto.getPeriodCode();
//			}
			dto.setPassPeriodType(passPeriodType);
			dto.setActivationDate(DateConvertUtil.date2String(rule.getEffectiveDate(), "yyyy-MM-dd"));
			dto.setDeactivationDate(DateConvertUtil.date2String(rule.getExpiryDate(), "yyyy-MM-dd"));
			quota = rule.getNumValue().longValue();
		}
		dto.setTotalMemberQuota(quota);
		return dto;
	}
	private List<Long> getCustumerPlanNo(Long customerId) {
		Member m = (Member) commonByHQLDao.get(Member.class, customerId);
		if(StringUtils.equals("CPM", m.getMemberType()) || StringUtils.equals("CDM", m.getMemberType())) {
			Integer corporateId = (Integer) commonByHQLDao.getUniqueBySQL(" select cm.corporate_id corporateId " +
					" from  corporate_member cm ,member  m  " +
					" where  cm.customer_id= COALESCE(m.superior_member_id,m.customer_id) and m.customer_id= " + customerId + " limit 1", null);
			if(corporateId!=null) {
				Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(Long.valueOf(corporateId));
				String hql = "select  distinct css.planNo " +
						"  from CorporateServiceSubscribe css where css.status='ACT' and css.id.accNo= "+accNo ;
				List<Long> planNos = commonByHQLDao.getByHql(hql);
				return planNos;
			}
			return null;
		}else{
			String hql = "select distinct sp.planNo from  CustomerServiceAcc csa ,CustomerServiceSubscribe css,Member m ," +
					" ServicePlan sp  where  csa.accNo=css.customerServiceAcc.accNo and css.id.servicePlanNo=sp.planNo " +
					" and csa.status='ACT'  and (csa.customerId=m.customerId or csa.customerId=m.superiorMemberId ) and  m.customerId=" + customerId ;
			List<Long> planNos = commonByHQLDao.getByHql(hql);
			return planNos;
		}
	}


	@Override
	@Transactional
	public boolean updateCustomerId2CustomerOrderPermitCard(PaymentOrderDto dto) {
		CustomerOrderPermitCard customerCardRecord =(CustomerOrderPermitCard)commonByHQLDao.getUniqueByHql("from  CustomerOrderPermitCard t where t.qrCode= " + dto.getPaymentNo());
		if(customerCardRecord!=null&&dto.getCustomerId()!=null){
			customerCardRecord.setCardholderCustomerId(dto.getCustomerId().longValue());
			return commonByHQLDao.update(customerCardRecord);
		}
		return false;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public ResponseResult saveOrUpdateDayPassGuestInfo( GuestProfileDto dto) throws Exception{
		String salutation = dto.getSalutation();
		String surname = dto.getSurname();
		String givenName = dto.getGivenName();
		String phoneMobile = dto.getPhoneMobile();
		if (StringUtils.isEmpty(salutation))
			return new ResponseResult("1", "", "Please choose a salutation!");
		if (StringUtils.isEmpty(surname)) 
			return new ResponseResult("1", "", "Please input your surname!");
		if (StringUtils.isEmpty(givenName))
			return new ResponseResult("1", "", "Please input your givenname!");
		if (StringUtils.isEmpty(phoneMobile))
			return new ResponseResult("1", "", "Please input your mobile phone number!");

		String passportType = dto.getPassportType();
		if (StringUtils.isEmpty(passportType)) {
			dto.setPassportType(null);
		}
		String passportNo = dto.getPassportNo();
		if (StringUtils.isEmpty(passportNo)) {
			dto.setPassportNo(null);
		}
		String contactEmail = dto.getContactEmail();
		if (StringUtils.isEmpty(contactEmail)) {
			dto.setContactEmail(null);
		}
		Long customerId = dto.getCustomerId();
		String loginUserId = dto.getLoginUserId();
		Date currentDate = new Date();
		
		
		if (passportType != null && !"HKID".equals(passportType)
				&& !"VISA".equals(passportType)) {
			responseResult.initResult(GTAError.LeadError.PASSPORT_TYPE_INVALID);
			return responseResult;
		}
		
		if ("HKID".equals(passportType)&&passportNo!=null&&!CommUtil.validateHKID(passportNo)){
				responseResult.initResult(GTAError.LeadError.HKID_NO_INVALID);
				return responseResult;
			}
		else if ("VISA".equals(passportType)&&passportNo!=null&&!CommUtil.validateVISA(passportNo)){
				responseResult.initResult(GTAError.LeadError.VISA_NO_INVALID);
				return responseResult;
		}
		
		if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
			responseResult.initResult(GTAError.LeadError.MOBILE_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
			responseResult.initResult(GTAError.LeadError.BUSINESS_PHONE_INVALID);
			return responseResult;
		}
		if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
			responseResult.initResult(GTAError.LeadError.HOME_PHONE_INVALID);
			return responseResult;
		}
		
		if (contactEmail!=null&&!CommUtil.validateEmail(contactEmail)) {
			responseResult.initResult(GTAError.LeadError.EMAIL_ADDRESS_INVALID);
			return responseResult;
		}
		
		if (dto.getCustomerId() != null) {
			CustomerProfile cp = leadCustomerDao.getByCustomerId(dto.getCustomerId());
			CustomerEnrollment ce = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
			if (cp == null || ce == null) {
				responseResult.initResult(GTAError.LeadError.UPDATED_FAILED);
				return responseResult;
			} else {
				if(passportNo!=null&&passportType!=null&&!customerProfileDao.checkAvailablePassportNo(customerId,passportType, passportNo)){
					responseResult.initResult(GTAError.LeadError.PASSPORT_IS_EXIST);
					return responseResult;
				}
				
				dto.setCreateDate(cp.getCreateDate());
				dto.setCreateBy(cp.getCreateBy());
				if(StringUtils.isEmpty(dto.getPortraitPhoto())){
					BeanUtils.copyProperties(dto, cp,new String[]{"portraitPhoto"});
				}else{
					BeanUtils.copyProperties(dto, cp);
				}
				cp.setUpdateDate(currentDate);
				cp.setUpdateBy(loginUserId);
				leadCustomerDao.updatePotentialCustomerProfile(cp);
			}

		} else {
			if(passportNo!=null&&passportType!=null&&!customerProfileDao.checkAvailablePassportNo(null,passportType, passportNo)){
				responseResult.initResult(GTAError.LeadError.PASSPORT_IS_EXIST);
				return responseResult;
			}
			
			CustomerProfile newCustomerProfile = new CustomerProfile();
			BeanUtils.copyProperties(dto, newCustomerProfile);
			newCustomerProfile.setIsDeleted("N");
			newCustomerProfile.setCreateDate(currentDate);
			newCustomerProfile.setCreateBy(loginUserId);
			newCustomerProfile.setUpdateDate(currentDate);
			newCustomerProfile.setUpdateBy(loginUserId);
			Long idSerial = (Long) leadCustomerDao.savePotentialCustomerProfile(newCustomerProfile);

			CustomerEnrollment customerEnrollment = new CustomerEnrollment();
			customerEnrollment.setStatus("OPN");
			customerEnrollment.setInternalRemark(dto.getInternalRemark());
			customerEnrollment.setCustomerId(idSerial);
			customerEnrollment.setSalesFollowBy(dto.getSalesFollowBy());
			customerEnrollment.setCreateDate(currentDate);
			customerEnrollment.setCreateBy(loginUserId);
			customerEnrollment.setUpdateDate(currentDate);
			customerEnrollment.setUpdateBy(loginUserId);
			customerEnrollmentDao.save(customerEnrollment);
			customerId = idSerial;

			//save member
			Member member=new Member();
			member.setCustomerId(customerId);
			member.setCreateBy(loginUserId);
			member.setCreateDate(currentDate);
			member.setUpdateBy(loginUserId);
			member.setUpdateDate(currentDate);
			member.setStatus("ACT");
			BigInteger qrCode = dto.getQrCode();
			List param=new ArrayList();
			param.add(qrCode);
			List ohqList = commonByHQLDao.getDtoBySql("select t.staff_User_Id staffUserId from customer_order_hd t where exists(select 1 from customer_order_permit_card c where c.customer_order_no=t.order_no and c.qr_code=?) limit 1", param, CustomerOrderHd.class);
			if(ohqList!=null && ohqList.size()>0){
				CustomerOrderHd ohq = (CustomerOrderHd) ohqList.get(0);
				if(StringUtils.equals(loginUserId, ohq.getStaffUserId())){
					member.setMemberType("HG");
				}else{
					member.setMemberType("MG");
				}
			}else {
				member.setMemberType("MG");
			}
			commonByHQLDao.save(member);
			
			CustomerOrderPermitCard customerCardRecord =(CustomerOrderPermitCard)commonByHQLDao.getUniqueByHql("from  CustomerOrderPermitCard t where t.qrCode= " + dto.getQrCode());
			if(customerCardRecord!=null&&idSerial!=null){
				customerCardRecord.setCardholderCustomerId(idSerial);
				boolean status = commonByHQLDao.update(customerCardRecord);
			}
			
		}
		CustomerProfile cpRetrieve = customerProfileDao.getById(customerId);
		return new ResponseResult("0", "Success", cpRetrieve);
	}

	@Override
	@Transactional
	public boolean saveOrUpdateCustomerOrderPermitCard(CustomerOrderPermitCard card) {
		CustomerOrderPermitCard permitCard = (CustomerOrderPermitCard) commonByHQLDao.get(CustomerOrderPermitCard.class, card.getQrCode());
		if(permitCard==null){
			return false;
		}
		permitCard.setLinkedCardNo(card.getLinkedCardNo());
		permitCard.setLinkedCardDate(card.getLinkedCardDate());
		commonByHQLDao.saveOrUpdate(permitCard);
		return true;
	}




	/*public ResponseResult validateInput(StringBuilder ms, GuestPro dto){

		if(StringUtils.isEmpty(dto.getPassportType())){
			ms.append(" ID Type,");
		}
		if(StringUtils.isEmpty(dto.getPassportNo())){
			ms.append(" ID No,");
		}
		if(StringUtils.isEmpty(dto.getSalutation())){
			ms.append(" Salutation,");
		}
		if(StringUtils.isEmpty(dto.getSurname())){
			ms.append(" Surname,");
		}
		if(StringUtils.isEmpty(dto.getGivenName())){
			ms.append(" Given Name,");
		}
		if(StringUtils.isEmpty(dto.getContactEmail())){
			ms.append(" Contact Email,");
		}
		if(StringUtils.isEmpty(dto.getPostalAddress1())){
			ms.append(" Check Postal address,");
		}
		if(ms.length()>0){
			return new ResponseResult("1",ms.substring(0,ms.length()-1).toString()+" are required parameters!");
		}

		if(!PassportType.HKID.name().equals(dto.getPassportType()) && !PassportType.VISA.name().equals(dto.getPassportType())){
			return new ResponseResult("1", "ID Type: invalid! Please input HKID or VISA!");
		}

		if(PassportType.HKID.name().equals(dto.getPassportType())&& !CommUtil.validateHKID(dto.getPassportNo())){
			return new ResponseResult("1", "HKID Number: Invalid! Please input a new HKID Number!");
		}
		if(PassportType.VISA.name().equals(dto.getPassportType())&& !CommUtil.validateVISA(dto.getPassportNo())){
			return new ResponseResult("1", "Passport Number: Invalid! Please input a new Passport Number!");
		}
		if(!CommUtil.validateEmail(dto.getContactEmail())){
			return new ResponseResult("1", "Email Address  : Invalid! Please input a new Email address!");
		}
		return new ResponseResult("0","Required fileds are all settled and checked!");

	}*/











	private String buyerType(String id){
		boolean flag = false;
		try {
			Long customerId = Long.parseLong(id);
			CustomerProfile cp = this.customerProfileDao.get(CustomerProfile.class, customerId);
			if(cp!=null){
				flag = true;
				return Constant.Purchase_Daypass_Type.Member.toString();
			}
		}catch(Exception e){
			UserMaster um = this.userMasterDao.get(UserMaster.class, id);
			if(um!=null){
				flag = true;
				return Constant.Purchase_Daypass_Type.Staff.toString();
			}
		}finally{
			if(false == flag){
				throw new RuntimeException("Id "+id+" can not find corresponding staff or member in system!");
			}
		}
		return null;
	}

	
	private String getMemberName(Long customerId){ 
		CustomerProfile cp = this.customerProfileDao.get(CustomerProfile.class, customerId);
		if(cp!=null){
			//maybe need to change
			return cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname();
		}
		return null;
	}

	@Override
	@Transactional
	public ResponseResult searchDayPassById(String Id) throws Exception
	{
		String sql=    " SELECT t.*, CONCAT(c.salutation,' ',c.given_Name,' ',c.surname) memberName, concat(t.planName,'(',t.ageRange,')') daypassType  "
				+"	FROM (   "
				+"		SELECT copc.qr_code paymentNo, copc.effective_from activationDate,copc.linked_card_no cardNo,copc.cardholder_customer_id customerId,sp.plan_name planName,spp.age_range_code ageRange,copc.customer_order_no orderNo "
				+"		FROM customer_order_permit_card copc, service_plan sp, service_plan_pos spp "
				+" 		WHERE copc.service_plan_no=sp.plan_no AND copc.service_plan_no=spp.plan_no "
				+"   ) t "
			/*	+" LEFT JOIN permit_card_master pcm ON pcm.card_no=t.cardNo "*/
				+" LEFT JOIN customer_profile c ON c.customer_id =t.customerId "
				+" LEFT JOIN sys_code s on s.code_value=t.ageRange  and s.category='ageRange' ";

		if (Id != null)
		{
			sql += " where t.paymentNo='"+Id.substring(2)+"'";
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			
			ResponseResult tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, sql, new ListPage<PaymentOrderDto>(), PaymentOrderDto.class);
			if (tmpResult.getListData() == null)
			{
				responseResult.initResult(GTAError.CommomError.GET_DATA_FAIL, "No Day pass found");
			}
			else
			{
				responseResult.initResult(GTAError.CommomError.GET_DAYPASS_SUCC, tmpResult.getListData());
			}
			return responseResult;
		}
		
		return null;
	}
	
	@Transactional
	public ResponseResult getMemberDayPass(String sql,String sortBy,String isAscending){
		sql = sql + " order by "+sortBy;
		if("true".equals(isAscending)){
			sql = sql + " asc ";
		}else if("false".equals(isAscending)){
			sql = sql + " desc ";
		}
		List<PaymentOrderDto> list = customerOrderPermitCardDao.getDtoBySql(sql, null, PaymentOrderDto.class);
		Data data = new Data();
		data.setList(list);
		data.setRecordCount(list!=null?list.size():0);
		data.setTotalPage(list!=null&&list.size()>0?1:0);
		data.setCurrentPage(list!=null&&list.size()>0?1:0);
		data.setPageSize(list!=null?list.size():0);
		data.setLastPage(true);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}

	@Override
	@Transactional
	public CustomerOrderPermitCard getDayPassCardByCardNo(String cardNo)
	{
		List<CustomerOrderPermitCard> cardList = customerOrderPermitCardDao.getByCol(CustomerOrderPermitCard.class, "linkedCardNo", Long.parseLong(cardNo), null);
		
		if (cardList != null && cardList.size() > 0)
		{
			return cardList.get(0);
		}
		return null;
	}

	/**   
	* @author: Zero_Wang
	* @since: Sep 9, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public DaypassPurchaseDto scanQRCode(ScanCardMappingDto cardMappingDto, Long orderNo) {
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		if(null == orderHd){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderHd orderNo not right:"+orderNo});
		}
		Long customerId = cardMappingDto.getCustomerId();
		orderHd.setCustomerId(customerId);
		List<CustomerOrderDet> orderDets = orderHd.getCustomerOrderDets();
		if(null==orderDets || orderDets.size()<=0){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderHd CustomerOrderDet orderNo not right:"+orderNo});
		}
		Map<String, CustomerOrderDetDto> itemMap = new HashMap<String, CustomerOrderDetDto>();
		for(CustomerOrderDet det : orderDets){
			CustomerOrderDetDto dto = new CustomerOrderDetDto();
			dto.setItemTotalAmout(det.getItemTotalAmout());
			dto.setOrderQty(dto.getOrderQty());
			itemMap.put(det.getItemNo(), dto);
		}
		List<CustomerOrderPermitCard> cards = orderHd.getCustomerOrderPermitCards();
		if(null==cards || cards.size()<=0){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"CustomerOrderHd CustomerOrderPermitCard orderNo not right:"+orderNo});
		}
		Member  member = this.memberDao.get(Member.class, customerId);
		if(null == member){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"Member:" + customerId});
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		DaypassPurchaseDto purchaseDto = new DaypassPurchaseDto();
		String type = member.getMemberType();
		if(Constant.memberType.CPM.toString().equals(type) || Constant.memberType.IPM.toString().equals(type)){
			MemberDto memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.CR.name());
			if(null == memberDto){
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"MemberCashvalue or member_limit_rule :" + customerId});
			}
			purchaseDto.setCreditLimit(memberDto.getCreditLimit());
			purchaseDto.setCurrentCashvalue(memberDto.getAvailableBalance());
		}else if(Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)){
			MemberDto memberDto = this.memberDao.getMemberCashvalue(customerId,LimitType.TRN.name());
			purchaseDto.setSpendingLimit(memberDto.getCreditLimit());
			purchaseDto.setCurrentCashvalue(memberDto.getAvailableBalance());
		}
		CustomerOrderPermitCard card = cards.get(0);
		
		purchaseDto.setMemberName(cardMappingDto.getMemberName());
		purchaseDto.setAcdemyID(cardMappingDto.getAcademyNo());
		purchaseDto.setActivationDate(sdf.format(card.getEffectiveFrom()));
		purchaseDto.setDeactivationDate(sdf.format(card.getEffectiveTo()));
		purchaseDto.setOfPass((long) cards.size());
		purchaseDto.setItemMap(itemMap);
		purchaseDto.setTotalPrice(orderHd.getOrderTotalAmount());
		return purchaseDto;
	}


	@Override
	@Transactional
	public CustomerOrderHd getDaypassOrder(Long orderNo) {
		CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
		return orderHd;
	}
	

	
}
