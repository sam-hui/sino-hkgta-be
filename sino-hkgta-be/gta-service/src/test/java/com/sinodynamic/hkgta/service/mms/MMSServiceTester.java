package com.sinodynamic.hkgta.service.mms;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDetailDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDto;
import com.sinodynamic.hkgta.dto.mms.SpaCenterInfoDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatDto;
import com.sinodynamic.hkgta.entity.mms.SpaRetreat;
import com.sinodynamic.hkgta.integration.spa.response.QueryCategoryResponse;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/config/applicationContext-service-test.xml" }) 
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class MMSServiceTester{

	@Autowired
	private MMSService MMSService;
	
//	@Test
//	public void testQueryCategories() throws Exception {
//		QueryCategoryResponse response = MMSService.queryTopCategories();
//		System.out.print(response);
//	}
	
//	@Test
	public void testBookAppointment() throws Exception{
		SpaAppointmentDto dto = new SpaAppointmentDto();
		dto.setCustomerId(new Long(4));
		dto.setPaymentMethod("CV");
//		dto.setInvoiceNo("280");
		List <SpaAppointmentDetailDto> serviceList = new ArrayList<SpaAppointmentDetailDto>();
		SpaAppointmentDetailDto param = new SpaAppointmentDetailDto();
		param.setCategoryCode("BLC001");
//		param.setCustomerId(new Long(10));
		param.setSubCategoryCode("BLSC012");		
		param.setStartDatetime("2015-12-08 09:00:00");
		param.setEndDatetime("2015-12-08 11:00:00");
		param.setServiceCode("Service1");
//		param.setServiceCost(new BigDecimal(1000));
		param.setTherapistCode("BLEMP001");
		param.setTherapistType("-1");		
		serviceList.add(param);
		
		SpaAppointmentDetailDto param1 = new SpaAppointmentDetailDto();
		param1.setCategoryCode("BLC002");
//		param1.setCustomerId(new Long(10));
		param1.setSubCategoryCode("BLSC012");		
		param1.setStartDatetime("2015-11-19 13:00:00");
		param1.setEndDatetime("2015-11-19 15:00:00");
		param1.setServiceCode("Service1");
		param1.setServiceCost(new BigDecimal(1000));
		param1.setTherapistCode("BLEMP001");
		param1.setTherapistType("0");
//		serviceList.add(param1);		
		dto.setServiceDetails(serviceList);
		ResponseResult responseResult = MMSService.bookAppointment(dto);
	}
	
	
//	@Test
	public void testPayment()throws Exception{
		
		MMSPaymentDto paymentDto = new MMSPaymentDto();
		paymentDto.setInvoiceNo("281");		
		paymentDto.setCustomerId(new Long(4));
		paymentDto.setPaymentMethod(Constant.CASH_Value);
		ResponseResult result = MMSService.payment(paymentDto);
		System.out.print(result);
		
	}
	
//	@Test
	public void testCancelAppointment() throws Exception{
		
		String invoiceNo = "281";
		String userId = "system";
		ResponseResult result = MMSService.canCelAppointment(invoiceNo, userId);
		System.out.print(result);;
	}
	
	@Test
	public void testGetLatestService() throws Exception{
		
		String invoiceNo = "334";
		String userId = "system";
		ResponseResult result = MMSService.getLatestAppointmentDetail(invoiceNo);
		System.out.print(result);;
	}
	
	@Test
	public void testAesEncode()
	{
		String TOKEN_KEY="20160119i37iq3nlu3a4b6v9yrjxrjeit3qhptg534v8u54hj40joo0hoo6lzb60s7otiamm32j"
				+ "k60y5hg89jr0u3pwu816868bi23x5r6nmtjcybepum39l45tu3axy5dwz9rb8dol2lewvxgwl6s91h50334zjj3du77xnr7yzkl98fb5feaj3055bu1cyf6ixsdnplw5br4ri";
		
		 String TOKEY_KEY_UUID = "ceeb83109a59489abff4e3b98fab59ec";

		 System.out.println(MMSService.aesEncode(TOKEN_KEY, TOKEY_KEY_UUID));	
		
	}
	
	@Test
	public void testAesDecode()
	{
		String TOKEN_KEY="73B7B7A6A8BC665B14A5D89C24E86BC2BD98731C93B9DCC6DF3407A279D89C57295631"
				+ "C59F949412270DB363034A7C4A1F31C84449B0FC992E0D6FF268660D7753ECEFD221BFE2C449C"
				+ "606E615CF39C0C476C2916EC4D7DCBD082786E94966BB751607141AA50E82D07DFB744ACEFEA5"
				+ "F010319CB30CECEB155A7FE531F25AA336FEA23C71B77DC879D4288B8D31DA6EA3C20D2BDDB03A"
				+ "BE7D6DDFD945763068B63BB50A8A6A5D60936A6A07EEDE07918FDE6FA35115F4A6E1FDD695F714"
				+ "FF24A52DCD8CBE762D27B3F09183D389B942";
				
		
		 String TOKEY_KEY_UUID = "ceeb83109a59489abff4e3b98fab59ec";

		 System.out.println(MMSService.aesDecode(TOKEN_KEY, TOKEY_KEY_UUID));	
		
	}
	
	@Test
	public void testGetSpaCenterInfo()
	{
		System.out.print(MMSService.getSpaCenterInfo().getCenterName());
	}
	
//	@Test
	public void testUpdateSpaCenterInfo()
	{
		SpaCenterInfoDto dto = new SpaCenterInfoDto();
		dto.setAdvanceBookPeriod("14");
		dto.setCenterName("Bliss");
		
		MMSService.updateSpaCenterInfo(dto, "test");
	}
	
	@Test
	public void testGetCategoryList()
	{
		System.out.print(MMSService.getSpaCategoryList().size());
		
	}
	@Test
	public void testSwitchStatus()
	{
		Long sourceRetId = new Long(1);
		Long tartgetRetId = new Long(2);
//		MMSService.switchRetreatOrder(sourceRetId, tartgetRetId, "System");
	}
	
	@Test
	public void testUpdateSpaRetreat()
	{
		SpaRetreatDto retreat = new SpaRetreatDto();
		retreat.setDescription("tttt");
		retreat.setPicPath("ddddff.jpg");
		retreat.setRetName("rddfff");
		MMSService.updateSpaRetreat(retreat, "System");
	}
}
