package com.sinodynamic.hkgta.service.pms;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.pms.RoomPicPathDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class GuestRoomServiceTest extends BaseServiceTester
{
	@Autowired
	private RoomTypeService roomTypeService;

	private static String TESTROOM = "TESTROOM99";

	@Test
	@Transactional
	public void createRoomType()
	{
		try
		{
			createRoom();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public void updateRoomType()
	{
		try
		{
			createRoom();
			String updateName = update();

			ResponseResult result2 = roomTypeService.getRoomTypeByCode(TESTROOM);
			RoomTypeDto newRoom = (RoomTypeDto) result2.getDto();

			Assert.assertEquals("0", result2.getReturnCode());
			Assert.assertEquals(updateName, newRoom.getRoomTypeName());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Transactional
	private void createRoom() throws Exception
	{
		ResponseResult result;
		result = roomTypeService.createRoomType(generateRoomType(), "[TESTUSER]");
		Assert.assertEquals("0", result.getReturnCode());

		result = roomTypeService.getRoomTypeByCode(TESTROOM);
		Assert.assertEquals("0", result.getReturnCode());
		RoomTypeDto room = (RoomTypeDto) result.getDto();

		System.out.println(room.getRoomTypeName());
	}

	@Transactional
	private String update() throws Exception
	{
		ResponseResult result;
		result = roomTypeService.getRoomTypeByCode(TESTROOM);
		RoomTypeDto oldRoom = (RoomTypeDto) result.getDto();

		String updateName = oldRoom.getRoomTypeName() + "_Update";
		String updateAmenity = oldRoom.getAmenity() + "_Update";
		oldRoom.setRoomTypeName(updateName);
		oldRoom.setAmenity(updateAmenity);

		roomTypeService.updateRoomType(oldRoom, "[TESTUSER]");
		return updateName;
	}

	private RoomTypeDto generateRoomType()
	{
		RoomTypeDto room = new RoomTypeDto();
		room.setNumOfGuest("2-4");
		room.setRoomTypeCode(TESTROOM);
		room.setAmenity("Have WIFI");
		room.setRoomTypeDescription("DESC:" + TESTROOM);
		room.setRoomTypeName(TESTROOM);

		List<RoomPicPathDto> pics = new ArrayList<RoomPicPathDto>();
		for (int i = 1; i <= 5; i++)
		{
			RoomPicPathDto pic = new RoomPicPathDto();
			pic.setServerFilename("TestPic" + i + ".jpg");
			pic.setRoomTypeCode(TESTROOM);
			pic.setDisplayOrder((long) i);
			pics.add(pic);
		}
		room.setRoomPics(pics);
		return room;
	}

}
