package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.dto.pms.RoomTaskDto;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class RoomServiceTest extends BaseServiceTester
{

	@Autowired
	private RoomService roomService;
	
	@Test
	public void getRoomTaskList(){
		try
		{
			ResponseResult resultMyRoom = roomService.getRoomTaskList("[staff001]", true, null);
			List<RoomMemberDto> roomMemberDtosMyRoom = (List<RoomMemberDto>) resultMyRoom.getData();
			System.out.println("-------------------------------room size = "+roomMemberDtosMyRoom.size());
			for(RoomMemberDto room: roomMemberDtosMyRoom){
				System.out.println("-------------------------------roomno = "+room.getRoomNo());
				System.out.println("-------------------------------tasklist size = "+room.getTasks().size());
				for(RoomTaskDto task : room.getTasks()){
					System.out.print("----"+task.getTaskId());
				}
			}
			System.out.println();
			ResponseResult result = roomService.getRoomTaskList("[staff001]", false,  null);
			List<RoomMemberDto> roomMemberDtos = (List<RoomMemberDto>) result.getData();
			System.out.println("-------------------------------room size = "+roomMemberDtos.size());
			for(RoomMemberDto room: roomMemberDtos){
				System.out.println("-------------------------------roomno = "+room.getRoomNo());
				System.out.println("-------------------------------tasklist size= "+room.getTasks().size());
				for(RoomTaskDto task : room.getTasks()){
					System.out.print("----"+task.getTaskId());
				}
			}
			System.out.println();
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void getRoomListWithTaskCounts(){
		try
		{
			roomService.getRoomListWithTaskCounts("");
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void getRoomCrewList(){
		try
		{
			roomService.getRoomCrewList("ISP");
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

}
