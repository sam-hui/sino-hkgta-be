package com.sinodynamic.hkgta.service.fms;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.constant.AgeRangeCode;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

public class MemberFacilityTypeBookingServiceTest extends BaseServiceTester
{
	private Logger logger = Logger.getLogger(FacilityUtilizationRateDateServiceTest.class);
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;

	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;
	
	@Test
	public void getMemberFacilityTypeBookingPrice()
	{
		try
		{
			MemberFacilityBookingDto memberFacilityBookingDto = buildMemberFacilityBookingDto();
			memberFacilityTypeBookingService.getMemberFacilityTypeBookingPrice(memberFacilityBookingDto, false);
		}
		catch (GTACommonException e)
		{
			logger.error(e.getError());
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void createMemberFacilityTypeBooking()
	{
		try
		{
			MemberFacilityBookingDto memberFacilityBookingDto = buildMemberFacilityBookingDto();
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			memberFacilityTypeBookingService.createMemberFacilityTypeBooking(memberFacilityBookingDto, false,facilityNos);
		}
		catch (GTACommonException e)
		{
			logger.error(e.getError());
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	public static List<Long> getAvailableResFacilityFloors()
	{
		List<Long> floors = new ArrayList<Long>();
		floors.add(0L);
		floors.add(1L);
		return floors;
	}

	private MemberFacilityBookingDto buildMemberFacilityBookingDto()
	{
		MemberFacilityBookingDto memberFacilityBookingDto = new MemberFacilityBookingDto();
		memberFacilityBookingDto.setFacilityType(Constant.FACILITY_TYPE_TENNIS);
		memberFacilityBookingDto.setAttributeType("TENNISCRT-MINI");
		memberFacilityBookingDto.setBookingDate("2015-09-24");
		memberFacilityBookingDto.setBeginTime(11);
		memberFacilityBookingDto.setEndTime(12);
		memberFacilityBookingDto.setAgeRangeCode(AgeRangeCode.ADULT.name());
		memberFacilityBookingDto.setCount(1);
		memberFacilityBookingDto.setCustomerId(2l);
		memberFacilityBookingDto.setReserveVia("FD");
		memberFacilityBookingDto.setPaymentMethod(Constant.CASH);
		memberFacilityBookingDto.setLocation("FD1");
		return memberFacilityBookingDto;
	}
}
