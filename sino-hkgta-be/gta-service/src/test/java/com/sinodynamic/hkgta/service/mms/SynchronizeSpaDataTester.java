package com.sinodynamic.hkgta.service.mms;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.service.BaseServiceTester;

public class SynchronizeSpaDataTester extends BaseServiceTester {
    
    @Autowired
    private SynchronizeSpaData synchronizeSpaData;
    
    @Test
    public void synchronizeAppointments() {
	
	try {
	    synchronizeSpaData.synchronizeAppointments();
	} catch (Exception e) {
	    Assert.fail(e.getMessage());
	}
    }
    
    @Test
    public void synchronizePayments() {
	
	try {
	    synchronizeSpaData.synchronizePayments();
	} catch (Exception e) {
	    Assert.fail(e.getMessage());
	}
    }
}
