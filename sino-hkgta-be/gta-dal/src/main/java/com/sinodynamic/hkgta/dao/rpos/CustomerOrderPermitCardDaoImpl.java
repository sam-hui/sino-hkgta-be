package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
@Repository
public class CustomerOrderPermitCardDaoImpl extends GenericDao<CustomerOrderPermitCard> implements CustomerOrderPermitCardDao {
	
	public CustomerOrderPermitCard getByCandidateCustomerId(Long candidateCustomerId){
		String hqlstr = " from CustomerOrderPermitCard c where c.candidateCustomer.candidateId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(candidateCustomerId);
		return (CustomerOrderPermitCard) getUniqueByHql(hqlstr, param);
	}

	@Override
	public List<CustomerOrderPermitCard> getUnACTList(
			String status) {
		String hql = "from CustomerOrderPermitCard c where effectiveFrom between ? and ? and status <> ?";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Timestamp begin = new Timestamp(calendar.getTimeInMillis());
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Timestamp end = new Timestamp(calendar.getTimeInMillis());
		List<Serializable> param = new ArrayList<Serializable>();
		try {
			param.add(sdf.parse(sdf.format(begin)));
			param.add(sdf.parse(sdf.format(end)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		param.add(status);
		return this.getByHql(hql, param);
	}

	@Override
	public List<CustomerOrderPermitCard> getExpireCustomerOrderPermitCard(
			Date date) {
		StringBuilder sql = new StringBuilder("select copc.linked_card_no as  linkedCardNo ,copc.status as status from customer_order_permit_card copc , permit_card_master card  ")
			.append(" where  copc.linked_card_no = card.card_no and copc.cardholder_customer_id = card.mapping_customer_id and card.status <> 'DPS' and card.status <> 'RTN' ")
			.append(" and copc.effective_to <? ");

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(date);
		
		return this.getDtoBySql(sql.toString(), param, CustomerOrderPermitCard.class);
	}

}
