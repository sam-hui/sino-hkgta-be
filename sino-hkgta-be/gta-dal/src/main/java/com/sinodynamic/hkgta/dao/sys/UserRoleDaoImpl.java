package com.sinodynamic.hkgta.dao.sys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.UserRole;
@Repository
public class UserRoleDaoImpl extends GenericDao<UserRole>  implements UserRoleDao {

	@Override
	public List<UserRoleDto> getUserRoleListByUserId(String userId) {
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		return getDtoBySql("SELECT s.user_id as userId,s.role_id as roleId,m.role_name as roleName from user_role s INNER JOIN role_master m on(m.role_id=s.role_id) where s.user_id=? ORDER BY s.role_id", param, UserRoleDto.class);
	}

	@Override
	public Object[] getUserCheckPermissions(String userId,String programId) {
		StringBuffer sb =  new StringBuffer();
		sb.append("SELECT p.program_id,min(p.access_right) as access_right from role_master m,role_program p where m.role_id=p.role_id and m.role_id in(SELECT s.role_id  from user_role s where s.user_id=? ORDER BY s.role_id) and p.program_id=? GROUP BY p.program_id");
		List<Serializable> param =  new ArrayList<Serializable>();
		param.add(userId);
		param.add(programId);
		Object object = this.getUniqueBySQL(sb.toString(), param);
		if(object!=null){
			return (Object[])object;
		}else{
			return null;
		}
	}

	
}
