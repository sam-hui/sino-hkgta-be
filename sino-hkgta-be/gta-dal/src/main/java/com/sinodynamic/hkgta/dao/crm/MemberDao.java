package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberType;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface MemberDao extends IBaseDao<Member> {
	public MemberDto getMemberCashvalue(Long customerId, String type);
	public void addMember(Member m);

	public void getMemberList(ListPage<Member> page, String countHql, String hql, List<?> param) throws Exception ;

	public void updateMemberer(Member m);

	public void deleteMember(Member m) throws Exception ;
	
	public Member getMemberById(Serializable id);
	
	public MemberType getMemberTypeByTypeName(String typeName);
	
	public String findLargestAcademyNo();
	
	public boolean validateAcademyID(String academyNo);
	
	public boolean updateStatus(String status, Long customerId);
	
	public boolean updateStatus(String status, Long customerId,String loginUserId);

	public ListPage<Member> getDependentMembers(ListPage<Member> page,
			Long customerID);
	
	public String getMemberTypeByAcademyNo(String academyNo);
	
	public List<Member> getListMemberBySuperiorId(Long customerId);
	
	public Member getMemberByAcademyNo(String academyNo) throws HibernateException;
	
	public Member getMemberByCustomerId(Long customerID) throws HibernateException;
	
	public Member getMemberByUserId(String userId) throws HibernateException;
	
	public MemberDto checkDMCreditLimit(Long customerId);

	public MemberDto getMemberPurchaseLimit(Long customerId);

	public MemberDto getMemberTransactionLimit(Long customerId);
	
	public MemberCashvalueDto getMemberCashvlueInfo(String customerId);
	
	public boolean updateRelationship(String relationshipCode, Long customerId,String loginUserId);

	public ListPage<Member> getMemberList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType);

	public List<AdvanceQueryConditionDto> assembleQueryConditions(List<SysCode> genderSysCodes, List<SysCode> memberStatusCodes);
	
	public boolean isMemberActive(Long customerId);

	public ListPage<Member> getRecentServedMemberList(ListPage<Member> page, String sortBy, String isAscending, Long[] customerId);
	
	public Date getExpiredDate(String customerId);
	
	public List<Member> getAllMembers() throws Exception;
	
	public Long getMemberCustomerId(String academyNO);

}
