
package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.mysql.fabric.xmlrpc.base.Array;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;

@Repository
public class PresentationMaterialsSeqDaoImpl extends GenericDao<PresentMaterialSeq> implements PresentationMaterialsSeqDao{

	@Override
	public void savePresentationMaterialsSeq(PresentMaterialSeq pms) throws HibernateException {
		super.save(pms);
	}

	@Override
	public int getPresentSeq(Long materialId, Long presentId) {
		// TODO Auto-generated method stub
		
//		String hql = " select p.presentSeq from PresentMaterialSeq p where p.materialId = " + materialId+"and p.presentId ="+presentId;	
//		int presentSeq = (int) super.getUniqueByHql(hql);	
		
		String sql = " SELECT present_seq FROM present_material_seq where present_id= "+ presentId +" and material_id= "+materialId ;
		
		int presentSeq  = (int) getCurrentSession().createSQLQuery(sql).uniqueResult();    
			
		return presentSeq;
	}
	
	public List<PresentMaterialSeq> getListByPresentId(Long presentId){
		String hql = " from PresentMaterialSeq pms where pms.id.presentId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(presentId);
		return getByHql(hql, param);
	}
}

