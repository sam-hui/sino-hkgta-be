package com.sinodynamic.hkgta.dao.pos;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantItemMaster;

@Repository
public class RestaurantItemMasterDaoImpl extends GenericDao<RestaurantItemMaster> implements RestaurantItemMasterDao  {

}
