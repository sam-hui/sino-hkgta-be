package com.sinodynamic.hkgta.dao.mms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;

public interface SpaMemberSyncDao extends IBaseDao<SpaMemberSync> {
    
    public List<SpaMemberSync> getSpecificSpaMemberSync(Long customerId, String status);
    
    public List<GuestRequestDto> getMemberInfoList2Synchronize(String status);
    
    public void addSpaMemberSyncWhenInsert(Member member);
    
    public void addSpaMemberSyncWhenUpdate(CustomerProfile oldThis, CustomerProfile newThis);

}
