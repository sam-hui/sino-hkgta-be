package com.sinodynamic.hkgta.dao.pos;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;

public interface RestaurantCustomerBookingDao extends IBaseDao<RestaurantCustomerBooking>
{
	List<RestaurantCustomerBooking> getRestaurantCustomerBookingListByRestaurantIdAndCustomerId(String restaurantId, Long customerId);
	
	public int updateRestaurantCustomerBookingExpired() throws Exception;

	List<RestaurantCustomerBooking> getRestaurantCustomerBookingConfirmedList();
	
	int getRestaurantCustomerBookingTotalPartySize(String restaurantId,Date bookingDate,Integer startTime,Integer endTime)  throws Exception;
}
