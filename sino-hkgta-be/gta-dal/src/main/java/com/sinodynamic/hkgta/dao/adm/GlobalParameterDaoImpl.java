package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class GlobalParameterDaoImpl extends GenericDao<GlobalParameter> implements GlobalParameterDao {

	@Override
	public ListPage<GlobalParameter> getGlobalParameterList(ListPage<GlobalParameter> pListPage, String sqlState, String sqlCount, List<Serializable> param)
	{
		return listByHql(pListPage, sqlCount, sqlState, param);
	}


}
