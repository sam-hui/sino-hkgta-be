package com.sinodynamic.hkgta.dao.crm;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo;
import com.sinodynamic.hkgta.dto.staff.StaffCoachInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRoaster;

public interface StaffCoachRoasterDao extends IBaseDao<StaffCoachRoaster>{
	CoachRosterInfo loadPresentRoster(String coachId);
	CoachRosterInfo loadCustomizedRoster(String coachId, Date start, Date end);
	List<StaffCoachRoaster> loadRawPresentRoster(String coachId);
	List<StaffCoachRoaster> loadRawCustomizedRoster(String coachId);
	List<StaffCoachRoaster> loadRawCustomizedRoster(String coachId, Date start, Date end);
	List<StaffCoachRoaster> loadOffdutyRoster(String coachId,Date begin, Date end);
	
	List<StaffCoachInfoDto> getOnDutyCoachByTimeslot(String beginDatetime, String endDatetime) throws Exception;
}
