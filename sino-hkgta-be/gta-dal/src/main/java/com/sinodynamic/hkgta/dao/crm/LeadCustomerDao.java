package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;

/**
 * 
 * @author Becky
 *
 */

public interface LeadCustomerDao extends IBaseDao<CustomerProfile> {
		
	public CustomerProfile getByCustomerId(Long customerId);
	
	public Serializable savePotentialCustomerProfile(CustomerProfile customerProfile);
	
	public void saveCustomerPreEnrollStage(CustomerPreEnrollStage customerPreEnrollStage);
	
	public void savePotentialCustomerEnrollment(CustomerEnrollment customerEnrollment);
	
	public void updatePotentialCustomerProfile(CustomerProfile customerProfile);

	public void deleteLeadCustomerProfile(CustomerProfile customerProfile);

}
