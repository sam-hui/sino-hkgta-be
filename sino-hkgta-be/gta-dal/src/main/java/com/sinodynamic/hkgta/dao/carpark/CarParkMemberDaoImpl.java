package com.sinodynamic.hkgta.dao.carpark;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dto.carpark.*;
import com.sinodynamic.hkgta.dao.GenericDao;


@Repository
public class CarParkMemberDaoImpl extends GenericDao implements CarParkMemberDao{
	
	Logger logger = Logger.getLogger(CarParkMemberDaoImpl.class);
	
	public List<CarParkMemberCheckDto> getCarParkMemberList(){
		
		String sql = "(select "
				+ "m.academy_no, "
				+ "p.card_no, "
				+ "conv(p.factory_serial_no, 16, 10) card_serial_no, "
				+ "concat(c.surname,' ', c.given_name) person_name, "
				+ "1 type, "
				+ "ca.customer_input licence_plate_no, "
				+ "c.phone_mobile, "
				+ "max(cs.expiry_date) expiry_date " 
				+ "from member m, "
				+ "permit_card_master p, "
				+ "customer_service_acc cs, " 
				+ "customer_profile c left join customer_addition_info ca on c.customer_id=ca.customer_id and ca.caption_id=5 "
				+ "where m.customer_id=c.customer_id "
				+ "and p.mapping_customer_id=m.customer_id "
				+ "and cs.customer_id=m.customer_id "
				+ "and m.member_type in ('IPM', 'CPM') "
				+ "and m.status='ACT' "
				+ "and cs.status = 'ACT' "
				+ "and current_date() between cs.effective_date and cs.expiry_date "
				+ "group by m.academy_no, "
				+ "p.factory_serial_no, "
				+ "c.surname,c.given_name, "
				+ "ca.customer_input, "
				+ "c.phone_mobile "
				+ "union "
				+ "select dm.academy_no, "
				+ "p.card_no, "
				+ "conv(p.factory_serial_no, 16, 10), "
				+ "concat(c.surname,' ', c.given_name), "
				+ "1, "
				+ "ca.customer_input, "
				+ "c.phone_mobile, "
				+ "max(cs.expiry_date) "
				+ "from member dm, "
				+ "member pm, "
				+ "permit_card_master p, "
				+ "customer_service_acc cs, "
				+ "customer_profile c left join customer_addition_info ca on c.customer_id=ca.customer_id and ca.caption_id=5 "
				+ "where dm.customer_id=c.customer_id "
				+ "and dm.customer_id=p.mapping_customer_id "
				+ "and dm.status='ACT' "
				+ "and dm.member_type in ('IDM', 'CDM') "
				+ "and dm.superior_member_id=pm.customer_id  "
				+ "and pm.customer_id=cs.customer_id "
				+ "and pm.status='ACT' "
				+ "and cs.status = 'ACT' "
				+ "and current_date() between cs.effective_date and cs.expiry_date "
				+ "group by dm.academy_no, "
				+ "p.factory_serial_no, "
				+ "c.surname,c.given_name, " 
				+ "ca.customer_input, "
				+ "c.phone_mobile "
				+ "order by academy_no) t where 1=1 ";
		
		String sqlState = "select t.academy_no academyNo, "
				+ "t.card_no cardNo, "
				+ "t.card_serial_no cardSerialNo, "
				+ "t.person_name personName, "
				+ "t.type, "
				+ "t.licence_plate_no licensePlateNo, "
				+ "t.phone_mobile phoneMobile, "
				+ "t.expiry_date expiryDate from " 
				+ sql;
		
		return getDtoBySql(sqlState, null, CarParkMemberCheckDto.class);
					
	}
	
	public List<CarParkMemberCheckDto> getCarParkStaffList(){
		
		String sql = "(SELECT "
				+ "m.staff_no, "
				+ "p.card_id, "
				+ "CONV(p.factory_serial_no, 16, 10) card_serial_no, "
				+ "CONCAT(s.surname,' ', s.given_name) person_name, "
				+ "11 type, "
				+ "s.licence_plate_no, "
				+ "s.phone_mobile, "
				+ "m.quit_date expiry_date "
				+ "FROM staff_master m, "
				+ "staff_profile s, "
				+ "internal_permit_card p "
				+ "WHERE m.user_id=s.user_id "
				+ "AND m.user_id=p.staff_user_id "
				+ "AND m.status='ACT' "
				+ "AND CURRENT_DATE() >= m.employ_date "
				+ "AND ( CURRENT_DATE() <= m.quit_date OR m.quit_date IS NULL)) t where 1=1 "; 
		
		String sqlState = "select t.staff_no academyNo, "
				+ "t.card_id cardNo, "
				+ "t.card_serial_no cardSerialNo, "
				+ "t.person_name personName, "
				+ "t.type, "
				+ "t.licence_plate_no licensePlateNo, "
				+ "t.phone_mobile phoneMobile, "
				+ "t.expiry_date expiryDate from "
				+ sql;
		
		return getDtoBySql(sqlState, null, CarParkMemberCheckDto.class);
	}

}
