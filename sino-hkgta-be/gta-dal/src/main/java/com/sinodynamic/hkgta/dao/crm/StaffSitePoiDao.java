package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;

public interface StaffSitePoiDao extends IBaseDao<StaffSitePoiMonitor>{
	
	public List<StaffSitePoiMonitor> listBySiteId(Long sitePoiId);
	
	public StaffSitePoiMonitor findByStaff(String satffUserId);
	
}
