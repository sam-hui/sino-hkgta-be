package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CustomerLeadNoteDto;
import com.sinodynamic.hkgta.entity.crm.CustomerLeadNote;
import com.sinodynamic.hkgta.util.pagination.ListPage;
@Repository
public class CustomerLeadNotesDaoImpl extends GenericDao<CustomerLeadNote> implements CustomerLeadNotesDao{
	
	@Override
	public ListPage<CustomerLeadNote> listByCustomerId(Long customerId, ListPage<CustomerLeadNote> page) {
		StringBuilder sql=new StringBuilder().append("SELECT c.sys_id as sysId, ")
				.append("c.customer_id as customerId, ").append("c.subject as subject, ")
	            .append("c.note_date as noteDate, ").append("c.content as content, ")
	            .append("c.create_by as createBy, ").append("concat_ws(' ', sp.given_name, sp.surname) as author, ")
	            .append("c.create_date as createDate ")
                .append("FROM customer_lead_notes c LEFT JOIN staff_profile sp on c.create_by = sp.user_id ")
                .append("WHERE c.customer_id = ?");
                //.append(" ORDER BY c.create_date");

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		
		String countSql = " select count(1) from ( "+ sql.toString() +" ) t";
		return listBySqlDto(page, countSql, sql.toString(), param, new CustomerLeadNoteDto());
	}
	
	@Override
	public CustomerLeadNote getCustomerLeadNoteById(Long nodeId) {
		return get(CustomerLeadNote.class, nodeId);
	}

	@Override
	public boolean saveCustomerLeadNote(CustomerLeadNote customerLeadNote)
			throws Exception {
		return saveOrUpdate(customerLeadNote);
	}

	@Override
	public boolean deleteById(Long nodeId) throws Exception {
		return deleteById(CustomerLeadNote.class, nodeId);
	}

}
