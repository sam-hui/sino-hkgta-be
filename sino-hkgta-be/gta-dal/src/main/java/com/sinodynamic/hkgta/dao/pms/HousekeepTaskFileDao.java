package com.sinodynamic.hkgta.dao.pms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;

public interface HousekeepTaskFileDao extends IBaseDao<HousekeepTaskFile>{
	
}
