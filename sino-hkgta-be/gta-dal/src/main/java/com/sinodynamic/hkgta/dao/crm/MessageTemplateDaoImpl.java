package com.sinodynamic.hkgta.dao.crm;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.MessageTemplateDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository
public class MessageTemplateDaoImpl  extends GenericDao<MessageTemplate> implements MessageTemplateDao {

	@Override
	public MessageTemplate getTemplateByFunctionId(String colValue) {
		List<MessageTemplate> templates = this.getByCol(MessageTemplate.class, "functionId", colValue, null);
		if (templates!=null && templates.size()>0)
			return templates.get(0);
		else
			return null;
	}
	
	public MessageTemplateDto getTemplateByOrderNoOrCustomerIdAndTemplateType(String templateType, Long orderNo, Long customerId ) {
		MessageTemplateDto dto = new MessageTemplateDto();
		List<MessageTemplate> templates = this.getByCol(MessageTemplate.class, "functionId", templateType, null);
		if (templates!=null && templates.size()>0){
			MessageTemplate template =  templates.get(0);
			String content = template.getContentHtml();
			Long localCustomerId = null;
			String fromName = Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER;
			if(customerId==null){
				CustomerOrderHd  hd = (CustomerOrderHd) getUniqueByHql(" from CustomerOrderHd hd where hd.orderNo = ? ", Arrays.asList((Serializable)orderNo));
				if(hd!=null){
					localCustomerId = hd.getCustomerId();
				}
			}else{
				localCustomerId = customerId;
			}
			CustomerProfile profile = (CustomerProfile) getUniqueByHql(" from CustomerProfile cp where cp.customerId = ? ", Arrays.asList((Serializable) localCustomerId));
			if(profile==null) return null;
				
			content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, profile.getSalutation() + " "+profile.getGivenName() + " "+ profile.getSurname())
					.replace(Constant.PLACE_HOLDER_FROM_USER, fromName).replace("\\n", "\n");
			dto.setMessageSubject(template.getMessageSubject());
			dto.setContent(content);
			dto.setContactEmail(profile.getContactEmail());
			return dto;
		}
		else{
			return null;
		}
	}
	
//	public MessageTemplate getTemplateByFunctionId(String colValue) {
//		List<MessageTemplate> templates = this.getByCol(MessageTemplate.class, "functionId", colValue, null);
//		if (templates!=null && templates.size()>0)
//			return templates.get(0);
//		else
//			return null;
//	}

	@Override
	public boolean saveMessageTemplate(MessageTemplate mt) throws Exception {
		return saveOrUpdate(mt);
	}

	@Override
	public MessageTemplate getMessageTemplateById(Long templateId) throws Exception {
		return get(MessageTemplate.class, templateId);
	}

	@Override
	public boolean deleteMessageTemplateById(Long templateId) throws Exception {
		
		return deleteById(MessageTemplate.class, templateId);
	}

	@Override
	public List<MessageTemplate> selectAllMessageTemplate() throws Exception {
		
		List<MessageTemplate> result = getByHql("from MessageTemplate");
		return result;
	}

	@Override
	public List<String> selectTemplateTypeList() {
	    
	    String sql = "select distinct template_type as templateType from message_template where template_type is not null";
	    List<MessageTemplate> list = getDtoBySql(sql, null, MessageTemplate.class);
	    if (list == null || list.size() == 0) return null;
	    
	    List<String> result = new ArrayList<String>();
	    for (MessageTemplate mt : list) {
		result.add(mt.getTemplateType());
	    }
	    
	    return result;
	}

	@Override
	public List<MessageTemplate> getMessageTemplatesByType(String templateType) {
	    
	    if (StringUtils.isEmpty(templateType)) return null;
	    
	    String hql = "select new MessageTemplate(templateId, functionId, templateName) from MessageTemplate where templateType = '" + templateType + "' order by templateName";
	    List<MessageTemplate> result = getByHql(hql);
	    return result;
	}
	
	
	
}
