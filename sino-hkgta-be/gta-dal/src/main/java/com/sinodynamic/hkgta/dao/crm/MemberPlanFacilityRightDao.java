/*
 * Copyright (C) HKGTA
 * File name:    MemberPlanFacilityRightDao.java
 * Author:       Li_Chen
 * Version:      1.0
 * Date:         2015-4-29
 * Description:  
 * Revision history: 
 * 2015-4-29  Li_Chen  The initial creation 
 * 
 */

package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.FacilityEntitlementDto;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;

public interface MemberPlanFacilityRightDao extends IBaseDao<MemberPlanFacilityRight>{
	public void savePlanFacilityRight(List<MemberPlanFacilityRight> list);

	public List<FacilityEntitlementDto> getEffectiveMemberPlanFacilityRightsByCustomerId(
			Long customerID);

	boolean getEffectiveMemberFacilityRight(Long customerId);

	public String getEffectiveMemberFacilityRightsInMyAccount(Long customerId);
	
	public int deletePlanFacilityRight(Long customerId, Long planNo);
	
	public int deletePlanFacilityRight(Long customerId);
	
	public List<MemberPlanFacilityRight> getEffectiveFacilityRightByCustomerId(Long customerId);

	public MemberPlanFacilityRight getEffectiveRightByCustomerIdAndFacilityType(
			Long customerId, String facilityType);
}
