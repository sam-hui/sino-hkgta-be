package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilitySubTypePos;

@Repository
public class FacilitySubTypePosDaoImpl extends GenericDao<FacilitySubTypePos> implements FacilitySubTypePosDao {

	public FacilitySubTypePos getFacilitySubTypePos(String subtypeId, String ageRangeCode, String rateType)
	{
		StringBuffer hql = new StringBuffer(" from FacilitySubTypePos where facilitySubType.subTypeId = ? and ageRangeCode=? and rateType=? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(subtypeId);
		paramList.add(ageRangeCode);
		paramList.add(rateType);
		List<FacilitySubTypePos> subTypes = super.getByHql(hql.toString(), paramList);
		return (null !=subTypes && subTypes.size() >0)?subTypes.get(0):null;
	}
}
