package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.DeliveryRecordDetailDto;
import com.sinodynamic.hkgta.dto.crm.DeliveryRecordDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@SuppressWarnings("rawtypes")
public interface DeliveryRecordDao extends IBaseDao {
	
	public ListPage<DeliveryRecordDto> getDeliveryRecord(ListPage<DeliveryRecordDto> page);
	
	public ListPage<DeliveryRecordDetailDto> getDeliveryRecordDetailByJobId(ListPage<DeliveryRecordDetailDto> page,Long jobId,String deliveryStatus);
	
	public boolean updateErrorCountInBatchSendStatementHd(Long batchId);
	
	public int getStatetmentSendStatus(String dateString, Long customerId);

}
