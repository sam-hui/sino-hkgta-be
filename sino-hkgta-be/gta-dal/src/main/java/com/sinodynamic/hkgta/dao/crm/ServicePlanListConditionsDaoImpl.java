package com.sinodynamic.hkgta.dao.crm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("servicePlanListConditions")
public class ServicePlanListConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Service Plan ID", "planNo", "java.lang.String", "", 1);
		final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Service Plan", "planName", "java.lang.String", "", 2);
		
		final List<SysCode> admissionType=new ArrayList<>();
		SysCode s7=new SysCode();
		s7.setCategory("admissionType");
		s7.setCodeDisplay("Individual");
		s7.setCodeValue("IPM");
		admissionType.add(s7);

		SysCode s8=new SysCode();
		s8.setCategory("admissionType");
		s8.setCodeDisplay("Coporate");
		s8.setCodeValue("CPM");
		admissionType.add(s8);
		final AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Admission Type", "subscriberType", "java.lang.String", admissionType, 3);
		
		final List<SysCode> passType=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("passPeriodType");
		s1.setCodeDisplay("Weekdays Only");
		s1.setCodeValue("WD");
		passType.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("passPeriodType");
		s2.setCodeDisplay("All Days");
		s2.setCodeValue("FULL");
		passType.add(s2);
		final AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Pass Type", "passPeriodType", "java.lang.String", passType, 4);
		final List<SysCode> status=new ArrayList<>();
		//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Active");
		s3.setCodeValue("ACT");
		status.add(s3);

		SysCode s4=new SysCode();
		s4.setCategory("status");
		s4.setCodeDisplay("Inactive");
		s4.setCodeValue("NACT");
		status.add(s4);
		
		SysCode s5=new SysCode();
		s5.setCategory("status");
		s5.setCodeDisplay("Expired");
		s5.setCodeValue("EXP");
		status.add(s5);

//		SysCode s6=new SysCode();
//		s6.setCategory("status");
//		s6.setCodeDisplay("Cancelled");
//		s6.setCodeValue("CAN");
//		status.add(s6);
		final AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Status", "status", "java.lang.String", status, 5);
		final AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Contract in Month", "contractLengthMonth", "java.lang.Integer", "", 6);
		final AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Start Date", "effectiveStartDate", "java.util.Date", "", 7);
		final AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Expiry Date", "effectiveEndDate", "java.util.Date", "", 8);
		return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6, condition7,condition8);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		return null;
	}

}
