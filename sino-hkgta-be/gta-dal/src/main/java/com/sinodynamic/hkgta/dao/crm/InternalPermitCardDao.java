package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;

public interface InternalPermitCardDao extends IBaseDao<InternalPermitCard> {

    	public String getTempPassProfileList(String status) throws Exception;
    	
    	public String getTempPassProfileHistoryList(String status) throws Exception;

	public List<InternalPermitCard> getTempCardContractorId(Long contractorId);

	void updateCardInfo(InternalPermitCard card);
	
	InternalPermitCard getCardById(Long cardId);
		
}
