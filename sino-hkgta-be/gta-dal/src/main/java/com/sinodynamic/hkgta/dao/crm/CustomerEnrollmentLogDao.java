package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollmentLog;

public interface CustomerEnrollmentLogDao extends IBaseDao<CustomerEnrollmentLog>{

	public CustomerEnrollmentLog getLatestEnrollLogByEnrollId(Long enrollId);
}
