package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdditionalInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;

@Repository
public class CustomerAdditionInfoDaoImpl extends GenericDao<CustomerAdditionInfo> implements CustomerAdditionInfoDao {

	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		return this.get(t, t.getId());
	}

	public void saveCustomerAdditionInfo(CustomerAdditionInfo t)
			{
		this.save(t);

	}

	public void updateCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		this.update(t);

	}

	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		this.delete(t);

	}

	@Override
	public List<CustomerAdditionInfo> getCustomerAdditionInfoListByCustomerID(Long customerID) {
		return getByCol(CustomerAdditionInfo.class, "id.customerId", customerID, "createDate desc");
	}
	
	public CustomerAdditionInfo getByCustomerIdAndCaptionId(Long customerId,Long captionId){
		String hqlstr = " from CustomerAdditionInfo c where c.id.customerId = ?  and c.id.captionId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(captionId);
		return (CustomerAdditionInfo) getUniqueByHql(hqlstr, param);
	}
	
	
	@Override
	public List<AdditionalInfoDto> getByCustomerIdAndCategory(Long customerId,
			String category) throws HibernateException {
		// TODO Auto-generated method stub

		StringBuilder sql = new StringBuilder();
		
		sql.append("	 SELECT c.customer_id AS customerId, p.caption_id AS captionId, p.category AS category, p.caption AS caption, " )
		.append("		c.customer_input AS inputData FROM customer_addition_info_caption p LEFT JOIN ( SELECT " )
		.append("				customer_addition_info.customer_id AS customer_id, customer_addition_info.customer_input AS customer_input, ")
		.append(" customer_addition_info.caption_id AS caption_id FROM customer_addition_info ")
		.append("			WHERE customer_addition_info.customer_id = ? ) c ON c.caption_id = p.caption_id ") 
		.append("		WHERE p.category = ? ");
	
		List paramList = new ArrayList<String>();
		paramList.add(customerId);
		paramList.add(category);
		
		List<AdditionalInfoDto> addinfo = super.getDtoBySql(sql.toString(), paramList, AdditionalInfoDto.class);
		return addinfo;
	}

	@Override
	public int deleteByCustomerId(Long customerId) throws HibernateException {
		// TODO Auto-generated method stub
		String hql = " delete from customer_addition_info  where customer_id = ? ";
		return super.deleteByHql(hql, customerId);
	}

	
	public List<CustomerAdditionInfoDto> getFavorByCustomerId(Long customerId){
		String sql = " select c.caption_id as captionId, c.customer_input as customerInput from customer_addition_info c where c.customer_id = ?  and c.caption_id in ('8','9','10','11','12') ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		return getDtoBySql(sql, param, CustomerAdditionInfoDto.class);
	}
	
	public List<CustomerAdditionInfoDto> getByCustomerIdAndCategoryForTablet(Long customerId,String category){

		String sql = "SELECT\n" +
				"	caption.caption AS caption,\n" +
				"	info.customer_input AS customerInput\n"+
				"FROM\n" +
				"	customer_addition_info info,\n" +
				"	customer_addition_info_caption caption\n" +
				"WHERE\n" +
				"	info.caption_id = caption.caption_id\n" +
				"AND info.customer_id = ?\n" +
				"AND caption.category = ?\n" +
				"ORDER BY\n" +
				"	caption.display_order ASC";
	   List<Serializable> param = new ArrayList<Serializable>();
	   param.add(customerId);
	   param.add(category);
	   
	   return getDtoBySql(sql, param, CustomerAdditionInfoDto.class);
	}
}
