package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;

public interface MemberReservedFacilityDao extends IBaseDao<MemberReservedFacility>{

	public List<MemberReservedFacility> getMemberReservedFacilityList(long resvId);
	
	public boolean deleteMemberReservedFacility(MemberReservedFacility memberReservedFacility);
	
	public MemberReservedFacility getMemberReservedFacilityListByFacilityTimeslotId(long facilityTimeslotId);
	
	public int deleteMemberReservedFacilityByFacilityTimeslotId(Long facilityTimeslotId) throws HibernateException;
}
