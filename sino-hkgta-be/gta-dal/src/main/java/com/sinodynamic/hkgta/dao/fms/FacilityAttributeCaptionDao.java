package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;

public interface FacilityAttributeCaptionDao extends IBaseDao<FacilityAttributeCaption>{
	
	List<FacilityAttributeCaption> getFacilityAttributeCaptionByFuzzy(String attributeId);

	FacilityAttributeCaption getFacilityAttributeCaptionByAttributeId(String attributeId);
}
