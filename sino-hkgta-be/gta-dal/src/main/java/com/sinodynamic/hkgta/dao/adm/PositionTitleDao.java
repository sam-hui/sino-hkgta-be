package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;
import com.sinodynamic.hkgta.entity.crm.PositionTitle;

/**
 * search for DepartmentStaffDao 
 * @author Vian Tang
 * 
 * @since June 29 2015
 *
 */
public interface PositionTitleDao extends IBaseDao<PositionTitle> {
	
	List<PositionTitle> getAllPositionTitle(Long departId) throws HibernateException;
}
