package com.sinodynamic.hkgta.dao.sys;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;

@Repository
public class UserMgrAuditDaoImpl extends GenericDao<UserRecordActionLog>
		implements UserMgrAuditDao {

}
