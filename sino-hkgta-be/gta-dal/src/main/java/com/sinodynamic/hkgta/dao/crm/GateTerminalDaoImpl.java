package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.GateTerminal;


@Repository
public class GateTerminalDaoImpl extends GenericDao<GateTerminal> implements GateTerminalDao {
	Logger logger = Logger.getLogger(GateTerminalDaoImpl.class);

	@Override
	public List<GateTerminal> getAllTerminal() throws HibernateException
	{
		StringBuilder hql = new StringBuilder();
		hql.append("FROM GateTerminal t");
		List<GateTerminal> allTerminal = getByHql(hql.toString());
		
		return allTerminal;
	}

	
	
	

	


}	
