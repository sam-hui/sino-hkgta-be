package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerServiceAccDto;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.EnrollmentHistoryDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.pagination.ListPage;
@Repository
public class CustomerServiceDaoImpl extends GenericDao<CustomerServiceAcc> implements CustomerServiceDao{

	public Serializable saveCustomerServiceAcc(CustomerServiceAcc customerServiceAcc){
		return save(customerServiceAcc);
	}
	
	public CustomerServiceAcc getAccountNoByCustomerId(Long customerId) {
		String hqlstr = "from CustomerServiceAcc c where c.customerId = "+ customerId;
		return (CustomerServiceAcc) getUniqueByHql(hqlstr);
	}
	
	@SuppressWarnings("unchecked")
	public ServicePlan getPlanByPlanName(String planName) {
		String hqlstr = "from ServicePlan s where s.planName = " + planName;

		//TODO: fix move to servicePlanDao
		//List<ServicePlan> servicePlan = (List<ServicePlan>) getByHql(hqlstr);
		List<ServicePlan> servicePlan = null;

		if (servicePlan != null && servicePlan.size() > 0) {
			return servicePlan.get(0);
		} else {
			return null;
		}
	}

	@Override
	public CustomerServiceAcc getLatestCustomerService(Long customerId) {
		List<CustomerServiceAcc> serviceAccs = getByHql("from CustomerServiceAcc csa where csa.effectiveDate is not null and csa.customerId="+customerId+" order by accNo desc");
		return (null != serviceAccs && serviceAccs.size()>0)?serviceAccs.get(0):null;
	}

	@Override
	public int updateCustomerServiceAccStatus(BigInteger customerId) {
		// TODO Auto-generated method stub
		  String sql2 = "select acc_no from customer_service_acc where status ='NACT' and customer_id = "+customerId;	
		  String sql = " UPDATE customer_service_acc SET status ='ACT' where status ='NACT' and customer_id = "+customerId;
		  Object obj = getCurrentSession().createSQLQuery(sql2).uniqueResult();
		  Integer accNo = null == obj?0:(Integer)obj;
                    if (accNo==0 )
                    {
                    	return  accNo;
                    }            
                    
		  getCurrentSession().createSQLQuery(sql).executeUpdate(); 
		  return  accNo;
	}

	@Override
	public int getAccNo(BigInteger customerId) {
		// TODO Auto-generated method stub
		String sql = "select acc_no from customer_service_acc where status ='NACT' and customer_id = "+customerId;	
		Object obj = getCurrentSession().createSQLQuery(sql).uniqueResult();
		  Integer accNo = null == obj?0:(Integer)obj;
                   if (accNo==0 )
                   {
                   	return  accNo;
                   }            	
		return accNo;
	}

	@Override
	public List<CustomerServiceAcc> selectExpiringMember(Integer expiringDays) throws Exception {
		
		Date curDate = new Date();
		Date expiryDate = CommUtil.addDays(curDate, expiringDays);
		String expiryDateStr = CommUtil.dateFormat(expiryDate, null);
		String hql = "from CustomerServiceAcc where status = 'ACT' and expiryDate = '" + expiryDateStr + "'";
		List<CustomerServiceAcc> result = getByHql(hql);
		return result;
	}
	@Override
	public List<CustomerServiceAcc> getExpiredCustomerServiceAccs() throws Exception {
		
		Date curDate = new Date();
		String expiryDateStr = CommUtil.dateFormat(curDate, null);
		String hql = "from CustomerServiceAcc where status = 'ACT' and expiryDate < '" + expiryDateStr + "'";
		List<CustomerServiceAcc> result = getByHql(hql);
		return result;
	}
	@Override
	public CustomerServiceAcc getCurrentActiveAcc(Long customerID) {
		String hqlstr = "from CustomerServiceAcc where  status = 'ACT' and customerId = ? and date_format( ?,'%Y-%m-%d') BETWEEN effectiveDate and expiryDate";
		List<Serializable> param  = new ArrayList<Serializable>();
		param.add(customerID);
		param.add(new Date());
		CustomerServiceAcc customerServiceAcc = (CustomerServiceAcc)getUniqueByHql(hqlstr,param);
		return customerServiceAcc;
	}

	@Override
	public CustomerServiceAccDto getLatestServiceAccByCustomerId(Long customerID)
			throws HibernateException {
		// TODO Auto-generated method stub
		
		String sql = 	"SELECT customer_id AS customerId, MAX(acc.effective_date) AS effectiveDate, DATE_FORMAT(expiry_date, '%Y/%m/%d') AS expiryDate FROM customer_service_acc acc"
				+" WHERE acc.`status` = 'ACT' AND acc.customer_id = ? ";
		
		List<Long> paramList = new ArrayList<Long>();
		paramList.add(customerID);
		
		List<CustomerServiceAccDto> acc = super.getDtoBySql(sql, paramList, CustomerServiceAccDto.class);
		
		if(null == acc || acc.size() == 0)
			return null;
		
		return acc.get(0);
			
	}

	@Override
	public List<EnrollmentHistoryDto> getEnrollmentHistoryList(Long customerID) {
		List<Object> param = new ArrayList<Object>();
		param.add(customerID);
		String sql = "SELECT\n" +
				"	c.effective_date AS joinDate,\n" +
				"	c.expiry_date AS expiryDate\n" +
				"FROM\n" +
				"	customer_service_acc c,\n" +
				"	customer_order_hd hd\n" +
				"WHERE\n" +
				"	hd.order_status NOT IN ('REJ', 'CAN')\n" +
				"AND c.order_no = hd.order_no\n" +
				"AND c. STATUS IN ('ACT', 'NACT', 'EXP')\n" +
				"AND c.effective_date IS NOT NULL\n" +
				"AND c.effective_date IS NOT NULL\n" +
				"AND c.customer_id = ?\n" +
				"ORDER BY\n" +
				"	c.effective_date DESC";
		return getDtoBySql(sql, param, EnrollmentHistoryDto.class);
		
	}

	@Override
	public List<CustomerServiceAcc> getEffctiveRenewCustomerServiceAccs() {
		String hqlstr = "from CustomerServiceAcc where  status = 'NACT' and date_format( ?,'%Y-%m-%d') BETWEEN effectiveDate and expiryDate)";
		List<Serializable> param  = new ArrayList<Serializable>();
		param.add(new Date());
		List<CustomerServiceAcc> customerServiceAccs = (List<CustomerServiceAcc>)getByHql(hqlstr,param);
		return customerServiceAccs;
	}
	
	public CustomerAccountInfoDto getNearestServiceAccountByCustomerId(Long customerId) {
		String sql = "SELECT\n" +
				"	plan.plan_name AS planName,\n" +
				"	inter.effective_date AS effectiveDate,\n" +
				"	inter.expiry_date AS expiryDate,\n" +
				"	plan.contract_length_month AS contractLength,\n" +
				"	orderHd.order_total_amount AS orderTotalAmount,\n" +
				"  inter.acc_cat as serviceAccount\n" +
				"FROM\n" +
				"	customer_service_acc inter,\n" +
				"	customer_service_subscribe sub,\n" +
				"	service_plan plan,\n" +
				"	customer_order_hd orderHd\n" +
				"WHERE\n" +
				"	sub.acc_no = inter.acc_no\n" +
				"AND plan.plan_no = sub.service_plan_no\n" +
				"AND orderHd.order_no = inter.order_no\n" +
				"AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"AND inter.customer_id = ?\n" +
				"ORDER BY\n" +
				"	inter.expiry_date DESC\n" +
				"LIMIT 0,1";
		List<Serializable> param  = new ArrayList<Serializable>();
		param.add(new Date());
		param.add(customerId);
		
		List<CustomerAccountInfoDto> accountInfoList = getDtoBySql(sql, param, CustomerAccountInfoDto.class);
		if(accountInfoList!=null&&accountInfoList.size()>0){
			return accountInfoList.get(0);
		}else{
			return null;
		}
	}

}
