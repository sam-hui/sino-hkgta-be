package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaPicPath;

public interface SpaPicPathDao extends IBaseDao<SpaPicPath>{
	
	public Serializable addSpaPicPath(SpaPicPath spaPicPath);
	public SpaPicPath getByPicId(Long picId);

}
