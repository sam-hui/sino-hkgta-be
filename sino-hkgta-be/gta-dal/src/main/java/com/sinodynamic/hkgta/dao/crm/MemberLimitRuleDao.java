package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;

public interface MemberLimitRuleDao extends IBaseDao<MemberLimitRule>{
	public Serializable saveMemberLimitRuleImpl(MemberLimitRule mLimitRule);
	
	public void saveMemberLimitRule(List<MemberLimitRule> list);
	
	public MemberLimitRule getEffectiveByCustomerId(Long customerId);
	
	public List<MemberLimitRule>  getEffectiveListByCustomerId(Long customerId);
	
	public BigDecimal getTransactionLimitOfMember(Long customerId);

	public boolean getDayPassPurchaseNoOfMember(Long customerId);

	boolean getMemberTrainingRight(Long customerId);
	
	boolean getMemberEventsRight(Long customerId);

	boolean updateMmberTransactionLimit(Long customerId, BigDecimal tran);

	boolean updateMemberFacilitiesRight(Long customerId, String facilityRight);

	boolean updateMemberTrainingRight(Long customerId, String trainingRight);

	boolean updateMemberEventsRight(Long customerId, String eventRight);

	boolean updateMemberDayPassPurchasing(Long customerId, String dayPass);
	
	public int deleteMemberLimitRule(Long customerId,String excludedLimitType);
	
	public List<MemberLimitRuleDto> getEffectiveMemberLimitRuleDtoByCustomerId(Long customerId);
	
	public MemberLimitRule getEffectiveMemberLimitRule(Long customerId,String limitType);
	
}
