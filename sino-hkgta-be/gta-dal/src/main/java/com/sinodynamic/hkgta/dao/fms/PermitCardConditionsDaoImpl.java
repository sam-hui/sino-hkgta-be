package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("permitCardConditions")
public class PermitCardConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao {

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions() {
	
    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
	AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyNo", "java.lang.String", "", 1);
	AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member", "memberName", "java.lang.String", "", 2);
	
	final List<SysCode> memberType=new ArrayList<>();
	SysCode s1=new SysCode();
	s1.setCategory("permitCardmemberType");
	s1.setCodeDisplay("Corporate Dependent Member");
	s1.setCodeValue("Corporate Dependent Member");
	memberType.add(s1);

	SysCode s2=new SysCode();
	s2.setCategory("permitCardmemberType");
	s2.setCodeDisplay("Corporate Primary Member");
	s2.setCodeValue("Corporate Primary Member");
	memberType.add(s2);
	
	SysCode s3=new SysCode();
	s3.setCategory("permitCardmemberType");
	s3.setCodeDisplay("Individual Primary Member");
	s3.setCodeValue("Individual Primary Member");
	memberType.add(s3);
	
	SysCode s4=new SysCode();
	s4.setCategory("permitCardmemberType");
	s4.setCodeDisplay("Individual Dependent Member");
	s4.setCodeValue("Individual Dependent Member");
	memberType.add(s4);

	
	AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Member Type", "memberTypeName", "java.lang.String", memberType, 3);
	AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Card No", "cardNo", "java.lang.String", "", 4);
	AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Issue Date", "cardIssueDate", "java.util.Date", "", 6);
	return Arrays.asList(condition1, condition2, condition3, condition4, condition5);
    }

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
	
    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
	AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyNo", "java.lang.String", "", 1);
	AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member", "memberName", "java.lang.String", "", 2);
	
	final List<SysCode> memberType=new ArrayList<>();
	SysCode s1=new SysCode();
	s1.setCategory("permitCardmemberType");
	s1.setCodeDisplay("Corporate Dependent Member");
	s1.setCodeValue("Corporate Dependent Member");
	memberType.add(s1);

	SysCode s2=new SysCode();
	s2.setCategory("permitCardmemberType");
	s2.setCodeDisplay("Corporate Primary Member");
	s2.setCodeValue("Corporate Primary Member");
	memberType.add(s2);
	
	SysCode s3=new SysCode();
	s3.setCategory("permitCardmemberType");
	s3.setCodeDisplay("Individual Primary Member");
	s3.setCodeValue("Individual Primary Member");
	memberType.add(s3);
	
	SysCode s4=new SysCode();
	s4.setCategory("permitCardmemberType");
	s4.setCodeDisplay("Individual Dependent Member");
	s4.setCodeValue("Individual Dependent Member");
	memberType.add(s4);


	
	AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Member Type", "memberTypeName", "java.lang.String", memberType, 3);
	AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Card No", "cardNo", "java.lang.String", "", 4);
	AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Issue Date", "cardIssueDate", "java.util.Date", "", 6);
	return Arrays.asList(condition1, condition2, condition3, condition4, condition5);
    }

    
}
