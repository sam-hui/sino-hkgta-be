package com.sinodynamic.hkgta.dao.fms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateDate;

public interface FacilityUtilizationRateDateDao extends IBaseDao<FacilityUtilizationRateDate>{

	FacilityUtilizationRateDate getFacilityUtilizationRateDate(String weekDay,Date specialDate);

	List<FacilityUtilizationRateDate> getSpecialRateDateList(String facilityType, int year, String subType);

	List<FacilityUtilizationRateDate> getSpecialRateDateListByDate(String facilityType, Date specialDate, String subType);
	
	FacilityUtilizationRateDate getSpecialRateDate(String facilityType, Date specialDate,String facilitySubtypeId);
}
