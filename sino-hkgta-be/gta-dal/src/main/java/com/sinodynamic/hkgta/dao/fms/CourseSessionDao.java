package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDetailsDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionSmsParamDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.dto.fms.MemberCourseAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.SessionMemberDto;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.fms.CourseSession;

public interface CourseSessionDao extends IBaseDao<CourseSession> {
    
    public Serializable addCourseSession(CourseSession session);
    
    public boolean updateCourseSession(CourseSession session);
    
    public boolean deleteCourseSession(CourseSession session);
    
    public List<CourseSession> selectCourseSessionByCourseNo(String courseNo);
    
    public List<StaffProfile> selectAvailableCoach(String type, String beginDatetime, String endDatetime);
    
    public CourseSession getCourseSessionById(Long sysId);
    
    public List<CourseSessionDetailsDto> getCourseSessionDetails(String courseId);
    
    public List<StaffProfile> selectAvailableCoachContainCurrent(String type, String beginDatetime, String endDatetime, String staffId);
    
    public List<CourseSession> selectCourseSessionByCourseId(Integer courseId);
    
    public Long getNextSessionNo(Long courseId);

    public void moveSessionNoForword(Long courseId, Long sessionNO);
    
    public List<CourseSessionDto> getCoachSessionList(String staffId, Date fromDate, Date endDate);
    
    public List<MemberCourseAttendanceDto> getMemberAttendanceList(String sessionId);
    
    public List<SessionMemberDto> getStudentAttendanceInfo(String courseId, String sessionNo);
    
    public CourseSession getLatestCourseSessionByCourseId(String courseId);
    
    public ContractHelper getSessionPeriodOfCourse(String courseId);
    
    public CourseSessionSmsParamDto getCourseSessionReminderInfo(Long sysId);
    
    public List<FacilityMasterDto> getCourseFacilitiesByCourseSessionId(Integer courseSessionId);
    
    public List<MemberFacilityTypeBookingDto> getActiveSessionByCourseId(Long courseId);
    
    public List<CourseSessionDetailsDto> getCourseSessionDetails(String courseId, String device);
}
