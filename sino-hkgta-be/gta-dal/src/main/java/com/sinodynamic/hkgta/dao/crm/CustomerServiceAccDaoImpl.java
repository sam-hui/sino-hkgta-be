package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
@Repository
public class CustomerServiceAccDaoImpl extends GenericDao<CustomerServiceAcc> implements CustomerServiceAccDao {

	@Override
	public CustomerServiceAcc getAactiveByCustomerId(Long customerId) {
		String hqlstr = "from CustomerServiceAcc where status='ACT' and customerId="+ customerId;
		return (CustomerServiceAcc) getUniqueByHql(hqlstr);
	}

	@Override
	public boolean isCustomerServiceValid(String sql, List<Serializable> params) {
	    
	    if (StringUtils.isEmpty(sql) || params == null || params.size() == 0) return false;
	    
	    Object result = getUniqueBySQL(sql, params);
	    return (result != null ? true : false);

	}

}
