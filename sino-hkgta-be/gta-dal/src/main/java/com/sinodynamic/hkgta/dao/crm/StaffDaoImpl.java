package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository
public class StaffDaoImpl extends GenericDao<StaffMaster> implements StaffDao {

	private Logger logger = Logger.getLogger(StaffDaoImpl.class);
	
	public String statffInfoValidate(String staffNo, String psd){
		
		logger.info("StaffDao.statffInfoValidate ...");
		String masterHql = "from StaffMaster where staffNo = " + staffNo;
		List<?> list = null; 

			
			list = getByHql(masterHql);
			
			if (list == null || list.size() == 0) 
				return "{\"status\":\"error\",\"name\":\"\"}";
			
			StaffMaster staffMaster = (StaffMaster) list.get(0);
			String userId = staffMaster.getUserId();

			String userHql = "from UserMaster where userId = " + userId;

			List<?> userlst = getByHql(userHql);
			
			if (userlst == null || userlst.size() == 0) return "{\"status\":\"error\",\"name\":\"\"}";
			UserMaster user = (UserMaster) userlst.get(0);
			String password = user.getPassword();
			
			if (!psd.equals(password)) {
				logger.info("Invalid Password!");				
				return "{\"status\":\"error\",\"name\":\"\"}";

			}
			
			String sql = "from staffProfile where userId = "+ userId;

			List<?> staffNameList = getByHql(sql);
			
			StaffProfile staffProfile = (StaffProfile) staffNameList.get(0);
			String staffName = staffProfile.getGivenName()+" "+ staffProfile.getSurname();
			return "{\"status\":\"Success\",\"name\":\"" + staffName + "\"}";

	}
	
	public String getStaffNoByUserId(String userId)
	{
		String hql = "FROM StaffMaster where userId = ?" ;
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		StaffMaster staff = (StaffMaster)getUniqueByHql(hql, param);
		
		return staff.getStaffNo();
	}

	@Override
	public void addStaffMaster(StaffMaster sm) throws HibernateException {
		// TODO Auto-generated method stub
		super.save(sm);
	}

	@Override
	public void updateStaffMaster(StaffMaster sm) throws HibernateException {
		// TODO Auto-generated method stub
		super.update(sm);
	}

	@Override
	public StaffMaster getStaffMasterByUserId(String userId)
			throws HibernateException {
		// TODO Auto-generated method stub
		return super.get(StaffMaster.class, userId);
	}

	@Override
	public StaffMaster getStaffMasterByStaffNo(String StaffNo)
			throws HibernateException {
		// TODO Auto-generated method stub
		
		String hql = "FROM StaffMaster where staffNo = ?" ;
			
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(StaffNo);
		StaffMaster staff = (StaffMaster)getUniqueByHql(hql, param);
		
		return staff;
	}

	@Override
	public String saveStaffMaster(StaffMaster sm) throws HibernateException {
		// TODO Auto-generated method stub
		return (String)super.save(sm);
	}

	@Override
	public StaffMasterInfoDto getStaffMasterDtoByUserId(String userId)
			throws HibernateException {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT m.user_id AS userId, m.staff_no AS staffNo, m.staff_type AS staffType, m.`status` AS status,")
		.append("		m.position_title_code AS positionTitle, p.position_name AS positionTitleName, m.employ_date colEmployDate, m.quit_date colQuitDate,")
		.append(" sc.code_display as staffTypeDisplay, dpt.depart_id as departId, branch.name as departName")
		.append("	FROM staff_master m")
		.append("  LEFT JOIN sys_code sc ON  sc.code_value = m.staff_type AND ( sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType') ")
		.append("  LEFT JOIN department_staff dpt ON  dpt.staff_user_id = m.user_id ")
		.append("  LEFT JOIN department_branch branch ON  dpt.depart_id = branch.depart_id ")
		.append("  LEFT JOIN position_title p ON  m.position_title_code = p.position_code ")
		.append(" WHERE m.user_id = ? ");
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		List<StaffMasterInfoDto> staffList = super.getDtoBySql(sql.toString(), param, StaffMasterInfoDto.class);
		if(null != staffList && staffList.size() > 0)
			return staffList.get(0);
		return null;
	}
	
	@Override
	public boolean isStaffNoUsed(String staffNo) throws HibernateException {
		// TODO Auto-generated method stub
		List<StaffMaster> sList = super.getByCol(StaffMaster.class, "staffNo", staffNo, null);
		return null != sList && sList.size() > 0;
	}

	@Override
	public List<StaffMaster> getEexpiredActStaffMaster()
			throws HibernateException {
		// TODO Auto-generated method stub
		String hql = " FROM StaffMaster where quitDate <= current_date() AND status = 'ACT' ";
		return super.getByHql(hql);
	}
	
	@Override
	public List<StaffMaster> getStaffMasterByPositionTitle(String positionTitle) {
		String hql = "select s from StaffMaster s inner join s.staffProfile where s.positionTitle = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(positionTitle);
		return this.getByHql(hql,param);
	}

	@Override
	public List<StaffMaster> getStaffMasterByRoomCrewRoleCode(String crewRoleId) {
		StringBuffer hql = new StringBuffer(
				"select distinct sm from StaffMaster sm, SysCode sc"
				+ " where sc.codeValue = sm.staffType "
				+ " and sc.category = ? "
				+ " and sm.staffType = ?");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Constant.SYSCODE_CREW_ROLE);
		paramList.add(crewRoleId);
		
		List<StaffMaster> result = super.getByHql(hql.toString(), paramList);
		return result;
	}
}
