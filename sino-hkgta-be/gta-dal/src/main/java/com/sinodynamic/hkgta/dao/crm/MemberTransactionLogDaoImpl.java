package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MemberTransactionLog;

@Repository
public class MemberTransactionLogDaoImpl extends GenericDao<MemberTransactionLog> implements MemberTransactionLogDao {

}
