package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.BigDecimalType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.account.DDIDto;
import com.sinodynamic.hkgta.entity.crm.DdiTransactionLog;
@Repository
public class DDITransactionLogDaoImpl extends GenericDao<DdiTransactionLog> implements DDITransactionLogDao {

	@Override
	public List<DDIDto> queryDDITransactionList(int year, int month) {
		/*String hql = "SELECT"
					+ " cot.paid_amount,"
					+ " cot.`status`,"
					+ " cot.transaction_no,"
					+ "ohd.customer_id,"
					+ " mpa.account_no,"
					+ " mpa.bank_account_name,"
					+ " mpa.bank_id,"
					+ " mpa.originator_bank_code"
				    + " FROM"
					+ " customer_order_trans cot,"
					+ " customer_order_hd ohd,"
					+ " member_payment_acc mpa"
				    + " WHERE"
					+ " cot.order_no = ohd.order_no"
				    + " AND mpa.customer_id = ohd.customer_id"
				    + " AND cot.payment_method_code = 'DDI'"
				    + " AND cot.`status` = 'SUC'"
				    + " AND year(transaction_timestamp)=:year and month(transaction_timestamp)=:month"
				    + " AND NOT EXISTS ("
					+ " SELECT 1  from "
					+ "	ddi_transaction_log dtl"
					+ " WHERE"
					+ "	dtl.transaction_no = cot.transaction_no)";*/
		String sql = "SELECT"
				+ " sum(cot.paid_amount) as amount,"
				+ " cot.`status` as status,"
				+ " cot.transaction_no as transactionNo,"
				+ "ohd.customer_id as customerId,"
				+ " mpa.account_no as accountNumber,"
				+ " mpa.bank_account_name as accountName,"
				+ " mpa.bank_id as bankId,"
				+ " mpa.originator_bank_code as originatorBankCode"
			    + " FROM"
				+ " customer_order_trans cot,"
				+ " customer_order_hd ohd,"
				+ " member_payment_acc mpa"
			    + " WHERE"
				+ " cot.order_no = ohd.order_no"
			    + " AND mpa.customer_id = ohd.customer_id"
			    //+ " AND cot.payment_method_code = 'DDI'"
			    + " AND cot.`status` = 'SUC'"//??????
			    + " AND year(transaction_timestamp)=? and month(transaction_timestamp)=?"
			    + " AND NOT EXISTS ("
				+ " SELECT 1  from "
				+ "	ddi_transaction_log dtl"
				+ " WHERE"
				+ "	dtl.transaction_no = cot.transaction_no)"
				+ " group by ohd.customer_id,mpa.bank_id";
	
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("amount", BigDecimalType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("transactionNo", LongType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("accountNumber", StringType.INSTANCE);
		typeMap.put("accountName", StringType.INSTANCE);
		typeMap.put("bankId", StringType.INSTANCE);
		typeMap.put("originatorBankCode", StringType.INSTANCE);
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(year);
		param.add(month);
		
		List<DDIDto> ddiList = getDtoBySql(sql.toString(), param, DDIDto.class, typeMap);
		
		return ddiList;
	}

	
}
