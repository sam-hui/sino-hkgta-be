package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;

@Repository
public class StaffMasterDaoImpl extends GenericDao<StaffMaster> implements StaffMasterDao
{

	@Override
	public List<StaffMaster> getExpireStaff() {
		String hql = " from StaffMaster  where quitDate < ? and status<> ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(new Date());
		param.add("INACT");
		List<StaffMaster> list = this.getByHql(hql, param);
		return list;
	}
}
