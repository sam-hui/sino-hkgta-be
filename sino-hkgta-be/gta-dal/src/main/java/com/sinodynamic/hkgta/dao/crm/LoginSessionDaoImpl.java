package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.LoginSession;
import com.sinodynamic.hkgta.entity.crm.LoginSessionPK;

@Repository
public class LoginSessionDaoImpl extends GenericDao<LoginSession> implements LoginSessionDao{

	@Override
	public LoginSession getByToken(String authToken) throws HibernateException {
		// TODO Auto-generated method stub
		String hql = " FROM LoginSession l WHERE l.sessionToken = ?";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(authToken);
		
		return (LoginSession)super.getUniqueByHql(hql, paramList);
	}

	@Override
	public void saveSessionToken(LoginSession sessionToken) throws HibernateException
	{
		this.save(sessionToken);	
	}

	@Override
	public LoginSession getByUserId(String userId) throws HibernateException
	{
		String hql = " FROM LoginSession l WHERE l.userId = ?";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userId);
		
		return (LoginSession)super.getUniqueByHql(hql, paramList);
	}
	
	@Override
	public LoginSession getByUserIdAndDevice(String userId, String device) throws HibernateException
	{
		String hql = " FROM LoginSession l WHERE l.id.userId = ? and l.id.deviceAccess = ?";
		
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userId);
		paramList.add(device);
		
		return (LoginSession)super.getUniqueByHql(hql, paramList);
	}
	
	@Override
	public boolean deleteSessionToken(String userId, String device) throws HibernateException
	{
		LoginSessionPK id = new LoginSessionPK();
		id.setUserId(userId);
		id.setDeviceAccess(device);
		
		if (null == super.get(LoginSession.class, id)) return true;
		
		return super.deleteById(LoginSession.class, id);
	}

}
