package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;
import com.sinodynamic.hkgta.util.pagination.ListPage;
@Repository
public class HousekeepPresetParameterDaoImpl extends GenericDao<HousekeepPresetParameter> implements HousekeepPresetParameterDao {

	@Override
	public List<HousekeepPresetParameter> getHousekeepPresetParameterList() {

		StringBuffer hql = new StringBuffer(
				"from HousekeepPresetParameter h where h.status = 'ACT' ");
		return super.getByHql(hql.toString(), null);
	}

	@Override
	public List<HousekeepPresetParameter> getHousekeepPresetParameterCatList() {
		StringBuffer hql = new StringBuffer(
				"select  distinct new HousekeepPresetParameter(paramCat) from HousekeepPresetParameter h where h.status = 'ACT' ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		return super.getByHql(hql.toString(), paramList);
	}

	public ListPage<HousekeepPresetParameter> getHousekeepPresetParameterList(ListPage<HousekeepPresetParameter> pListPage, String sqlState, String sqlCount, List<Serializable> param) throws HibernateException
	{
		return listByHql(pListPage, sqlCount, sqlState, param);
	}
}
