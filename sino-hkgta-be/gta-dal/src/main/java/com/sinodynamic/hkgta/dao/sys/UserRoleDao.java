package com.sinodynamic.hkgta.dao.sys;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.UserRole;

public interface UserRoleDao extends IBaseDao<UserRole>{

	public List<UserRoleDto> getUserRoleListByUserId(String userId);

	public Object[] getUserCheckPermissions(String userId,String programId);



}
