package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;

public interface PresentMaterialSeqDao extends IBaseDao<PresentMaterialSeq>  {
	
	public boolean saveOrUpdatePMS(PresentMaterialSeq pms);
	
	public int getPresentSeq(Long materialId,Long presentId);
}

