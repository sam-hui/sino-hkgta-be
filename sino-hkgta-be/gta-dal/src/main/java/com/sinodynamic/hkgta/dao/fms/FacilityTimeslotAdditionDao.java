package com.sinodynamic.hkgta.dao.fms;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotAddition;

public interface FacilityTimeslotAdditionDao extends IBaseDao<FacilityTimeslotAddition>{
	FacilityTimeslotAddition getFacilityTimeslotAddition(long facilityTimeslotId);
	int deleteFacilityTimeslotAdditionByFacilityTimeslotId(Long facilityTimeslotId) throws HibernateException;
}
