package com.sinodynamic.hkgta.dao.pos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;

@Repository
public class RestaurantBookingQuotaDaoImpl extends GenericDao<RestaurantBookingQuota> implements RestaurantBookingQuotaDao  {

	@Override
	public List<RestaurantBookingQuota> getRestaurantBookingQuotaList(String restaurantId, Integer bookingTime)
	{
		String hql = " from RestaurantBookingQuota where restaurantMaster.restaurantId = ? "
				+ " and ( "
				+ "  (startTime <= ? and ? < (case when (startTime>endTime) then (endTime+2400) else endTime end) ) "
				+ " or ((case when (startTime>endTime) then (startTime-2400) else startTime end) <= ? and ? < endTime )"
				+ ")";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(restaurantId);
		params.add(bookingTime);
		params.add(bookingTime);
		params.add(bookingTime);
		params.add(bookingTime);
		return super.getByHql(hql, params);
	}

}
