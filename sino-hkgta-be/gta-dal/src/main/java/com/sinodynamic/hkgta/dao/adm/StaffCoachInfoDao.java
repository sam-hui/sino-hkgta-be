package com.sinodynamic.hkgta.dao.adm;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.AvailableCoachDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffCoachInfo;

/**
 * search for StaffCoachInfo 
 * @author Vian Tang
 * 
 * @since June 29 2015
 *
 */
public interface StaffCoachInfoDao extends IBaseDao<StaffCoachInfo> {
	
	StaffCoachInfo getByUserId(String userId) throws HibernateException;
	
	List<AvailableCoachDto> getAvailableCoach(Date date,String timeslot,String staffType);
	
	PrivateCoachInfoDto getCoachProfile(String coachId);
}
