package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRatePos;
@Repository
public class StaffCoachRatePosDaoImpl extends GenericDao<StaffCoachRatePos> implements StaffCoachRatePosDao{

	@Override
	public List<StaffCoachRatePos> getStaffCoachRatePosByUserId(String userId) {
		StringBuilder hql = new StringBuilder("FROM  StaffCoachRatePos s where s.userId=?");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		List<StaffCoachRatePos>  list = this.getByHql(hql.toString(), param);
		return list;
	}

}
