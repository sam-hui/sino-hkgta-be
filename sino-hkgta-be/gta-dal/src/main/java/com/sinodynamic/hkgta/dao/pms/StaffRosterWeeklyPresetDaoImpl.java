package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.StaffRosterWeeklyPreset;

@Repository
public class StaffRosterWeeklyPresetDaoImpl extends GenericDao<StaffRosterWeeklyPreset> implements StaffRosterWeeklyPresetDao {

	@Override
	public List<StaffRosterWeeklyPreset> getStaffRosterWeeklyPreset(String presetName) {
		StringBuilder hql = new StringBuilder(
				"from StaffRosterWeeklyPreset s where s.presetName = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(presetName);
		List<StaffRosterWeeklyPreset> staffRosterWeeklyPresets = super.getByHql(hql.toString(), paramList);
		return staffRosterWeeklyPresets;
	}

	@Override
	public int delete(String presetName) {
		String hql = "DELETE from StaffRosterWeeklyPreset s where s.presetName = ? ";
		List<Serializable> param = new ArrayList<>();
		param.add(presetName);
		return super.hqlUpdate(hql, param);
	}

	@Override
	public List<StaffRosterWeeklyPreset> searchByPresetName(String presetName) {
		StringBuilder hql = new StringBuilder(
				"from StaffRosterWeeklyPreset s ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		if (presetName != null) {
			hql.append("where upper(s.presetName) like ? ");
			paramList.add("%" + presetName.toUpperCase() + "%");
		}
		hql.append(" order by s.presetName ");
		
		List<StaffRosterWeeklyPreset> staffRosterWeeklyPresets = super.getByHql(hql.toString(), paramList);
		return staffRosterWeeklyPresets;
	}

}
