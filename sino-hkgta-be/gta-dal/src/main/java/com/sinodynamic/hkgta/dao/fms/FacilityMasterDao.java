package com.sinodynamic.hkgta.dao.fms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterQueryDto;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;

public interface FacilityMasterDao extends IBaseDao<FacilityMaster>
{

	public List<FacilityMaster> getFacilityMasterList(Integer venueFloor, String facilityType);

	public List<FacilityMasterQueryDto> getFacilityMasterList(String facilityType);

	public List<FacilityMaster> getFacilityVenueFloorList(String facilityType, String venueCode);

	int getFacilityMasterCount(String facilityType, String facilityAttribute);
	
	int getFacilityMasterCount(String facilityType, String facilityAttribute,List<Long> floors,List<Long> facilityNos);
	
	public FacilityMaster getByFacilityNo(String facilityNo);
	
	public FacilityMaster getByFacilityNo(long facilityNo);

	public List<FacilityMaster> getFacilityMasterByIdBatch(String[] facilityNo);

	public List<FacilityMaster> getAllVenueFloor();

	public List<FacilityItemDto> getFacilityStatusByFloorAndTime(String type, String beginDatetime, String endDatetime, Long floor);

	public List<FacilityItemDto> getAdditionFacilityStatusByTime(String type, String beginDatetime, String endDatetime);
	
	public List<FacilityMaster> getFacilityMasterAvailableList(String facilityType,String facilityAttribute,Date[] times,Integer venueFloor);
	public List<FacilityMaster> getFacilityMasterAvailableList(String facilityType,String facilityAttribute,Date[] times,List<Long> floors, List<Long> facilityNos);
	
	public List<BayTypeDto> getFacilityBayType(String facilityType);

	public List<FacilityMaster> getFacilityByAttribute(String facilityType, String facilityAttribute);

	public int countFacilityMasterByType(String facilityType, List<Long> floors, List<Long> facilityNos);
}
