package com.sinodynamic.hkgta.dao.ical;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.ical.PublicHoliday;

public interface GtaPublicHolidayDao extends IBaseDao<PublicHoliday> {
	
     public PublicHoliday viewHolidayDetail(Long holiday_id) throws Exception;
     
     public void deleteById(Long holiday_id) throws Exception;
     
     public void saveHoliday(List<PublicHoliday> holidays) throws Exception;
     
     public List<Date> getNearestHolidays(Date date) ;
}
