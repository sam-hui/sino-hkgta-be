package com.sinodynamic.hkgta.dao.pos;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantItemMaster;

public interface RestaurantItemMasterDao extends IBaseDao<RestaurantItemMaster> {

}
