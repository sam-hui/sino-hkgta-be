package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanOfferPos;

@Repository
public class ServicePlanOfferPosDaoImpl extends GenericDao<ServicePlanOfferPos> implements ServicePlanOfferPosDao {

	@Override
	public ServicePlanOfferPos getServicePlanOfferPosByFK(Long servPosId, String posItemNo) {
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(servPosId);
		param.add(posItemNo);
		return (ServicePlanOfferPos)this.getUniqueByHql("from ServicePlanOfferPos where servPosId=? and posItemNo=?", param);
	}

}
