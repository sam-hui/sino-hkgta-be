package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.dto.mms.SpaCancelledNotificationInfoDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;

@Repository
public class SpaAppointmentRecDaoImpl extends GenericDao<SpaAppointmentRec>
		implements SpaAppointmentRecDao {

	public Serializable saveSpaAppointmenRec(SpaAppointmentRec sapAppointmenRec){

		return save(sapAppointmenRec);
	}

	public SpaAppointmentRec getBySysId(Long sysId){
		
		return null;
	}

	@Override
	public List<SpaAppointmentRec> getSpaAppointmentRecByExtinvoiceNo(String extInvoiceNo) {
	    
	    String hql = "from SpaAppointmentRec where extInvoiceNo = ? order by sysId";
	    List<Serializable> params =  new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    return getByHql(hql, params);
	}

	@Override
	public int deleteByExtInvoiceNo(String extInvoiceNo) {
	    
	    String hql = "delete from spa_appointment_rec where ext_invoice_no = ?";
	    List<Serializable> params =  new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    return deleteByHql(hql, params);
	}

	@Override
	public void updateSpaAppointmentRec(SpaAppointmentRec spaAppointmentRec) {
	    update(spaAppointmentRec);
	}

	@Override
	public List<MmsOrderItemDto> getAppointmentItemsByExtinvoiceNo(String extInvoiceNo) {
	    
	    String sql = "select  sar.ext_appointment_id as appointmentId, sar.ext_employee_code as therapistCode,"
	    	+ "sar.ext_service_code as itemId, "
	    	+ "sar.service_name as service, "
	    	+ "concat(sar.ext_employee_first_name, ' ', sar.ext_employee_last_name) as therapist, "
	    	+ "cod.item_total_amout as rate, "
	    	+ "sar.status as status, "
	    	+ "sar.start_datetime as startTime, "
	    	+ "sar.end_datetime as endTime "
	    	+ "from "
	    	+ "spa_appointment_rec sar "
	    	+ "left join customer_order_det cod on sar.order_det_id = cod.order_det_id "
	    	+ "where sar.ext_invoice_no = ? ";
	    
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    
	    return getDtoBySql(sql, params, MmsOrderItemDto.class);
	    
	}

	@Override
	public Date getLastestSpaSyncTime() {
	    
	    String sql = "select max(ext_sync_timestamp) from spa_appointment_rec where create_by = 'spa_sync'";
	    Date date = (Date) getUniqueBySQL(sql, null);
	    return date;
	}

	@Override
	public List<SpaAppointmentRec> getAppointmentRecsByAppointmentId(String appointmentId) {
	    
	    String hql = "from SpaAppointmentRec where extAppointmentId = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(appointmentId);
	    return getByHql(hql, params);
	}

	@Override
	public List<SpaAppointmentRec> getAppointmentRecsByInvoiceNo(String extInvoiceNo) {
	    
	    String hql = "from SpaAppointmentRec where extInvoiceNo = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    return getByHql(hql, params);
	}

	@Override
	public List<SpaCancelledNotificationInfoDto> getUserForSpaAppointmentBeginNotification() {
	    
	    String sql = "select "
	    	+ "mb.user_id as userId, "
	    	+ "sar.start_datetime as startDatetime, "
	    	+ "sar.end_datetime as endDatetime, "
	    	+ "sar.service_name as serviceName, "
	    	+ "sar.ext_employee_first_name as employFN, "
	    	+ "sar.ext_employee_last_name as employLN "
	    	+ "from spa_appointment_rec sar left join member mb on sar.customer_id = mb.customer_id "
	    	+ "where sar.status = 'RSV' and timestampdiff(minute, sar.start_datetime, date_add(current_timestamp(), interval 1 hour)) = 0";
	    
	    return getDtoBySql(sql, null, SpaCancelledNotificationInfoDto.class);
	}

        @Override
        public List<SpaCancelledNotificationInfoDto> getUserForSpaAppointmentEndNotification() {
    
        	String sql = "select "
        	    	+ "mb.user_id as userId, "
        	    	+ "sar.start_datetime as startDatetime, "
        	    	+ "sar.end_datetime as endDatetime, "
        	    	+ "sar.service_name as serviceName, "
        	    	+ "sar.ext_employee_first_name as employFN, "
        	    	+ "sar.ext_employee_last_name as employLN "
        		+ "from spa_appointment_rec sar left join member mb on sar.customer_id = mb.customer_id "
        		+ "where sar.status = 'RSV' and timestampdiff(minute, sar.end_datetime, date_add(current_timestamp(), interval 10 minute)) = 0";
        
        	return getDtoBySql(sql, null, SpaCancelledNotificationInfoDto.class);
        }
	
}
