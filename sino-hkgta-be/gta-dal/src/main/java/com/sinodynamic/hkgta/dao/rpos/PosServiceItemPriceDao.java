package com.sinodynamic.hkgta.dao.rpos;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;

public interface PosServiceItemPriceDao extends IBaseDao<PosServiceItemPrice> {

	//get the renew item's price
	List getRenewItemPrice (Long planNo,String offerCode);
	
	public PosServiceItemPrice getByItemNo(String itemNo);
	
	public PosServiceItemPrice getByServicePlanNo(Long planNo);

}
