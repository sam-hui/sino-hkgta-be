package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;


public interface CustomerServiceAccDao extends IBaseDao<CustomerServiceAcc> {

	CustomerServiceAcc getAactiveByCustomerId(Long customerId);
	
	boolean isCustomerServiceValid(String sql, List<Serializable> params);
	
}
