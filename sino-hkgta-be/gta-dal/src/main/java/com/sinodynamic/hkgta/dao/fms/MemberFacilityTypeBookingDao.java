package com.sinodynamic.hkgta.dao.fms;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface MemberFacilityTypeBookingDao extends IBaseDao<MemberFacilityTypeBooking>{

	public MemberFacilityTypeBooking getMemberFacilityTypeBooking(long resvId)  throws HibernateException;

	public MemberFacilityTypeBooking getMemberFacilityTypeBookingIncludeAllStatus(long resvId)  throws HibernateException;
	
	public boolean updateMemberFacilityTypeBooking(MemberFacilityTypeBooking memberFacilityTypeBooking) throws HibernateException;

	public List<MemberFacilityTypeBooking> getMemberFacilityTypeBookingByCustomerId(long customerId)  throws HibernateException;
	
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingByOrderNo(Long orderNo)  throws HibernateException;
	
	public String getAllMemberFacilityTypeBooking(String facilityType, String dateRange, String customerId,boolean isPushMessage) throws Exception;
	
	public List<MemberFacilityTypeBooking> getMemberFacilityTypeBooking(long customerId,Date createDate,String facilityType)  throws Exception;
	
	public ListPage<MemberFacilityTypeBooking> getMemberBookingRecordsByCustomerId(ListPage<MemberFacilityTypeBooking> page,MemberFacilityTypeBookingDto dto,Long customerId,String appType,String year,String month,String day,String startDate,String endDate);
	
	public boolean deleteBySql(Long resvId);
	
	public List<MemberFacilityTypeBookingDto> getRoomReservationFacilities(Long roomResvId);
	
	public int getMemberFacilityBookedCountForQuota(String facilityType,Date bookingDateTime) throws Exception;
	
	public ListPage<MemberFacilityTypeBooking> getDailyMonthlyFacilityUsage(String timePeriodType, ListPage page, String selectedDate, String facilityType);

	public byte[] getDailyMonthlyFacilityUsageAttachment(String timePeriodType, String selectedDate, String fileType, String sortBy, String isAscending, String facilityType) throws Exception;

	public ListPage<MemberFacilityTypeBooking> getDailyMonthlyPrivateCoaching(String timePeriodType, ListPage page, String selectedDate, String facilityType);

	public byte[] getDailyMonthlyPrivateCoachingAttachment(String timePeriodType, String selectedDate, String fileType, String sortBy, String isAscending, String facilityType) throws Exception;

	public long getTotalFacilityTypeQty(long resvId);

	public long getTotalCheckedInFacilityTypeQty(long resvId);
	
	public int getMemberFacilityTypeBookingBayCount(long customerId,Date createDate,String facilityType)  throws Exception;
}
