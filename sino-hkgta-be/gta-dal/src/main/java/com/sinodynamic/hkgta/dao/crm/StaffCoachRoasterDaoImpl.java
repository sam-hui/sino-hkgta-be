package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo.CoachDayRateInfo;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo.TimeSliceRate;
import com.sinodynamic.hkgta.dto.staff.StaffCoachInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRoaster;
import com.sinodynamic.hkgta.util.constant.Constant;
@Repository
public class StaffCoachRoasterDaoImpl extends GenericDao<StaffCoachRoaster> implements StaffCoachRoasterDao {

	private static Logger logger = LoggerFactory.getLogger(StaffCoachRoasterDaoImpl.class);
	
	@Override
	public List<StaffCoachRoaster> loadRawPresentRoster(String coachId) {
		String hql = "from StaffCoachRoaster where coachId = ? and weekDay is not null and onDate is null";
		List<StaffCoachRoaster> roster = this.getByHql(hql, Arrays.<Serializable>asList(coachId));
		
		return roster;
	}

	@Override
	public List<StaffCoachRoaster> loadRawCustomizedRoster(String coachId) {
		String hql = "from StaffCoachRoaster where coachId = ? and weekDay is null and onDate is not null";
		List<StaffCoachRoaster> roster = this.getByHql(hql, Arrays.<Serializable>asList(coachId));
		
		return roster;
	}

	@Override
	public List<StaffCoachRoaster> loadRawCustomizedRoster(String coachId, Date start, Date end) {
		String hql = "from StaffCoachRoaster where coachId = ? and weekDay is null and onDate is not null and onDate between ? and ?";
		List<StaffCoachRoaster> roster = this.getByHql(hql, Arrays.<Serializable>asList(coachId,start,end));
		
		return roster;
	}
	
	private int getDayOfWeek(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	@Override
	public CoachRosterInfo loadCustomizedRoster(String coachId, Date start, Date end) {
		return loadRoster(coachId, start, end, true);
	}
	
	@Override
	public CoachRosterInfo loadPresentRoster(String coachId) {
		return loadRoster(coachId, null, null, false);
	}
	
	private CoachRosterInfo loadRoster(String coachId, Date start, Date end, boolean isCustomized) {
		List<StaffCoachRoaster> roster = this.loadRawPresentRoster(coachId);
		
		//use customized roster to override present roster
		if(isCustomized){
			Collections.sort(roster);
			List<StaffCoachRoaster> customizedRoster = this.loadRawCustomizedRoster(coachId,start,end);
			for(StaffCoachRoaster customizedItem : customizedRoster){
				this.evict(customizedItem);
				customizedItem.setWeekDay(String.valueOf(getDayOfWeek(customizedItem.getOnDate())));
				customizedItem.setOnDate(null);
				int index = Collections.binarySearch(roster, customizedItem);
				if(index >= 0){
					roster.remove(index);
					roster.add(index,customizedItem);
				}
			}
		}
		
		//convert List<StaffCoachRoaster> to CoachRosterInfo
		HashMap<String, List<StaffCoachRoaster>> counter = new HashMap<String, List<StaffCoachRoaster>>();
		
		for(StaffCoachRoaster rosterItem : roster){
			if(counter.containsKey(rosterItem.getWeekDay())){
				counter.get(rosterItem.getWeekDay()).add(rosterItem);
			}else
			{
				List<StaffCoachRoaster> weekdayRoster = new ArrayList<StaffCoachRoaster>();
				weekdayRoster.add(rosterItem);
				counter.put(rosterItem.getWeekDay(), weekdayRoster);
			}
		}
		
		List<CoachDayRateInfo> dayRateList = new ArrayList<CoachDayRateInfo>();
		for(String weekday : counter.keySet()){
			CoachDayRateInfo dayRate = new CoachDayRateInfo();
			dayRate.setWeekDay(Integer.parseInt(weekday));
			
			List<TimeSliceRate> sliceList = new ArrayList<TimeSliceRate>();
			for(StaffCoachRoaster weekdayRosterItem : counter.get(weekday)){
				TimeSliceRate sliceRate = new TimeSliceRate();
				sliceRate.setBeginTime(weekdayRosterItem.getBeginTime());
				
				String rateType = weekdayRosterItem.getRateType();
				if(rateType == null){
					rateType = Constant.RateType.OFF.toString();
				}
				
				sliceRate.setRateType(rateType);
				sliceList.add(sliceRate);
			}
			dayRate.setRateList(sliceList);
			dayRateList.add(dayRate);
		}
		CoachRosterInfo rosterInfo = new CoachRosterInfo();
		rosterInfo.setDayRateList(dayRateList);
		rosterInfo.setCoachId(coachId);
		return rosterInfo;
	}

	@Override
	public List<StaffCoachRoaster> loadOffdutyRoster(String coachId,Date begin, Date end) {
		String hql = "from StaffCoachRoaster where coachId = ? and offDuty in ('Y','B') and (onDate is null or onDate between ? and ?)";
		List<StaffCoachRoaster> roster = this.getByHql(hql, Arrays.<Serializable>asList(coachId, begin, end));
		return roster;
	}

	@Override
	public List<StaffCoachInfoDto> getOnDutyCoachByTimeslot(String beginDatetime, String endDatetime) throws Exception {
	    
	    if (StringUtils.isEmpty(beginDatetime) || StringUtils.isEmpty(endDatetime)) return null;
	    
	    if(logger.isDebugEnabled())
	    {
	    	logger.debug("beginDateTime:" + beginDatetime + " endDatetime:" + endDatetime);
	    }
	    
	    String datePart = beginDatetime.substring(0, 10);
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	    cal.setTime(sdf.parse(beginDatetime));
	    int weekday = cal.get(Calendar.DAY_OF_WEEK);
	    String timeslot = getTimeslot(beginDatetime, endDatetime);
	    
	    String sql = "select distinct coach_user_id as userId from ("
	    	+ "select template.coach_user_id as coach_user_id, "
	    	+ "if(extra.coach_user_id is null, template.off_duty, extra.off_duty) as off_duty "
	    	+ "from (select distinct coach_user_id, begin_time, off_duty "
	    	+ "from staff_coach_roster where week_day = "
	    	+ weekday
	    	+ " and begin_time in "
	    	+ timeslot
	    	+ ") template left join "
	    	+ "(select distinct coach_user_id, begin_time, off_duty from staff_coach_roster where on_date = '"
	    	+ datePart
	    	+ "' and begin_time in "
	    	+ timeslot
	    	+ ") extra on template.coach_user_id = extra.coach_user_id and "
	    	+ "template.begin_time = extra.begin_time) t where t.off_duty = 'Y'";
	    
	    if(logger.isDebugEnabled())
	    {
	    	logger.debug("sql:" + sql);
	    }
	    return (List<StaffCoachInfoDto>)getDtoBySql(sql, null, StaffCoachInfoDto.class);

	}
	
	private static String getTimeslot(String beginDatetime, String endDatetime) {
	    
	    int beginHour = Integer.parseInt(beginDatetime.substring(11, 13));
	    int endHour = Integer.parseInt(endDatetime.substring(11, 13));
	    
	    StringBuilder sb = new StringBuilder("(-1,");  //in case that the begintime equals to endtime, it will cause sql error as the timeslot will be empty
	    while (beginHour < endHour) {
		sb.append(beginHour).append(",");
		beginHour++;
	    }
	    
	    String timeslot = sb.substring(0, sb.length() - 1) + ")";
	    return timeslot;

	}
	
//	public static void main(String[] args) {
//	    
//	    String param = getTimeslot("2015-07-03 09:00", "2015-07-03 11:00");
//	    System.out.println(param);
//	}
}
