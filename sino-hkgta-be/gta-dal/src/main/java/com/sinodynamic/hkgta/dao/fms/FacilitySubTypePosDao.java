package com.sinodynamic.hkgta.dao.fms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilitySubTypePos;

public interface FacilitySubTypePosDao extends IBaseDao<FacilitySubTypePos>{

	FacilitySubTypePos getFacilitySubTypePos(String subtypeId,String ageRangeCode,String rateType);
}
