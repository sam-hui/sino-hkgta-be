package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.DeliveryRecordDetailDto;
import com.sinodynamic.hkgta.dto.crm.DeliveryRecordDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;


@SuppressWarnings("rawtypes")
@Repository
public class DeliveryRecordDaoImpl extends GenericDao implements DeliveryRecordDao {

	
	@SuppressWarnings("unchecked")
	public ListPage<DeliveryRecordDto> getDeliveryRecord(ListPage<DeliveryRecordDto> page) {
		
		String sql=" select cond.* from (SELECT\n" +
				" 	hd.create_date as nativeDeliveryDate,\n"+
				" 	hd.statement_month as nativeMonthOfStatement,\n"+
				"   DATE_FORMAT(hd.create_date,'%Y-%b-%d %H:%i') AS deliveryDate,\n" +
				"	hd.batch_id AS jobId,\n" +
				"	DATE_FORMAT(hd.statement_month,'%Y-%b') AS monthOfStatement,\n" +
				"  count(list.email_send_id) as totalCount,\n" +
				"  sum(case when content.status = 'SENT' then 1 else 0 end) as successCount,\n" +
				"  sum(case when content.status = 'FAIL'  then 1 else 0 end) as failureCount,\n" +
				"  concat_ws(' ', sp.given_name, sp.surname) as sendBy\n" +
				"from\n" +
				"batch_send_statement_list list\n" +
				"left join batch_send_statement_hd hd \n" +
				"on list.batch_id = hd.batch_id \n" +
				"left join customer_email_content content \n" +
				"on content.send_id = list.email_send_id\n" +
				"left join staff_profile sp \n" +
				"on sp.user_id = hd.create_by\n" +
				"GROUP BY  hd.batch_id ) cond\n"+
				"<SearchCondition>\n";
		
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " where "+page.getCondition();
			sql = sql.replace("<SearchCondition>", condition);
		}else{
			sql = sql.replace("<SearchCondition>", "");
		}
		
		String countSql = " select count(cc.jobId) from ( "+ sql+" ) cc";
		return listBySqlDto(page, countSql, sql, null, new DeliveryRecordDto());	
	}
	
	@SuppressWarnings("unchecked")
	public ListPage<DeliveryRecordDetailDto> getDeliveryRecordDetailByJobId(ListPage<DeliveryRecordDetailDto> page,Long jobId,String deliveryStatus) {
		
		String sql = 
				" select cond.* from ( select\n" +
				"  m.academy_no as academyId,\n" +
				"  concat_ws(' ', cp.salutation, cp.given_name, cp.surname) as member,\n" +
				"  case m.member_type\n" +
				"     when 'IPM' then 'Individual'\n" +
				"     when 'IDM' then 'Individual'\n" +
				"     when 'CPM' then 'Corporate'\n" +
				"     when 'CDM' then 'Corporate'\n" +
				"     else 'Other'\n" +
				"  end as memberType,\n" +
				"  list.cashvalue_balance as balance,\n" +
				"  case content.status \n" +
				"     when 'SENT' then 'Success'\n" +
				"     else 'Failure'\n" +
				"  end as deliveryStatus,\n" +
				"  content.status as nativeDeliveryStatus,\n"+
				"  ifnull(content.server_return_code,'') as smtpErrorCode,\n" +
				"  content.recipient_customer_id as customerId\n" +
				"from\n" +
				"	batch_send_statement_list list,\n" +
				"	customer_email_content content\n" +
				"left join customer_profile cp on cp.customer_id = content.recipient_customer_id\n" +
				"left join member m on m.customer_id = cp.customer_id\n" +
				"where\n" +
				"	list.email_send_id = content.send_id\n" +
				"  and list.batch_id = ? ) cond\n "+
				"  where 1=1 \n"+
				"<SearchCondition>\n";;
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(jobId);
		
		if(!deliveryStatus.equalsIgnoreCase("ALL")){
			sql = sql+" and nativeDeliveryStatus = ? ";
			param.add(deliveryStatus);
		}
		
		
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " and "+page.getCondition();
			sql = sql.replace("<SearchCondition>", condition);
		}else{
			sql = sql.replace("<SearchCondition>", "");
		}
		
		String countSql = " select count(cc.academyId) from ( "+ sql+" ) cc";
		return listBySqlDto(page, countSql, sql.toString(), param, new DeliveryRecordDetailDto());	
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateErrorCountInBatchSendStatementHd(Long batchId){
		String hqlstr=" update BatchSendStatementHd hd set hd.errorCount = hd.errorCount + 1  where hd.batchId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(batchId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public int getStatetmentSendStatus(String dateString, Long customerId){
		String sql = "SELECT\n" +
				" hd.batch_id as jobId \n" +
				"FROM\n" +
				"	batch_send_statement_hd hd,\n" +
				"	batch_send_statement_list list,\n" +
				"	customer_email_content content\n" +
				"WHERE\n" +
				"	hd.batch_id = list.batch_id\n" +
				"AND list.email_send_id = content.send_id\n" +
				"AND DATE_FORMAT(hd.statement_month, '%Y-%m') = ? \n" +
				"AND content.recipient_customer_id = ? "+
				"AND  content.status = 'SENT' ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(dateString);
		param.add(customerId);
		List<DeliveryRecordDto> deliveryRecordDtoList = getDtoBySql(sql, param, DeliveryRecordDto.class);
		if(deliveryRecordDtoList!=null) return deliveryRecordDtoList.size();
		return 0;
	}
	
}
