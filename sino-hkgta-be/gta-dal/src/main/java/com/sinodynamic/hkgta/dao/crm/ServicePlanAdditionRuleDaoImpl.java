package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;

@Repository
public class ServicePlanAdditionRuleDaoImpl extends GenericDao<ServicePlanAdditionRule> implements ServicePlanAdditionRuleDao{

	@Override
	public List getServicePlanAdditionRuleByPlanNo(Long planNo) {
		String sql = "SELECT sprm.right_code right_code,sprm.input_value_type input_value_type,spar.input_value input_value,sprm.description description "
				+ "  FROM service_plan_addition_rule spar INNER JOIN service_plan_right_master sprm ON spar.right_code = sprm.right_code AND spar.plan_no = "+planNo;
		return getCurrentSession().createSQLQuery(sql).list();
	}
	
	public ServicePlanAdditionRule getByPlanNoAndRightCode(Long planNo, String rightCode){
		String hqlstr = " from ServicePlanAdditionRule s where s.servicePlan.planNo = ? and s.rightCode = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		param.add(rightCode);
		List<ServicePlanAdditionRule> list = getByHql(hqlstr, param);
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}

}
