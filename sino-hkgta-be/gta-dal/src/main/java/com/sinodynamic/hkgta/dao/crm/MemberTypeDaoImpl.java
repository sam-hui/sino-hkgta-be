package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MemberType;

@Repository
public class MemberTypeDaoImpl extends GenericDao<MemberType> implements MemberTypeDao{

	public MemberType getByMemberTypeName(String memberTypeName) {
		String hqlstr = "from MemberType where typeName = '"+memberTypeName+"'";
		return (MemberType) getUniqueByHql(hqlstr);
	}

	@Override
	public MemberType getByMemberTypeCode(String typeCode) {
		String hqlstr = "from MemberType where typeCode = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(typeCode);
		return (MemberType)getUniqueByHql(hqlstr, param);
	}
}
