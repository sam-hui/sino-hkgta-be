package com.sinodynamic.hkgta.dao.pos;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
@Repository
public class RestaurantMasterDaoImpl extends GenericDao<RestaurantMaster> implements RestaurantMasterDao {

	@Override
	public List<RestaurantMaster> getRestaurantMasterList()
	{
		String hql = "from RestaurantMaster";
		return super.getByHql(hql);
	}

	@Override
	public RestaurantMaster getRestaurantMaster(String restaurantId) {
		return get(RestaurantMaster.class, restaurantId);
	}
}
