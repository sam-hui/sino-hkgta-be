package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;

@Repository
public class CustomerAddressDaoImpl extends GenericDao<CustomerAddress> implements CustomerAddressDao {

	@Override
	public List<CustomerAddress> getCustomerAddressesByCustomerID(Long customerID) {	
		return getByCol(CustomerAddress.class, "id.customerId", customerID, null);
	}
	
	public CustomerAddress getByCustomerIDAddressType(Long customerId, String addressType) {
		String hqlstr = " from CustomerAddress c where c.id.customerId = ? and c.id.addressType = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(addressType);
		return (CustomerAddress) getUniqueByHql(hqlstr, param);
	}

	@Override
	public int deleteByCustomerId(Long customerId) throws HibernateException {
		// TODO Auto-generated method stub
		String hql = " delete from customer_address  where customer_id = ? ";
		return super.deleteByHql(hql, customerId);
	}
}
