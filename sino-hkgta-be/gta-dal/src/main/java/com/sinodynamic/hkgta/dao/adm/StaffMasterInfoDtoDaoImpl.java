package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class StaffMasterInfoDtoDaoImpl extends GenericDao<StaffMasterInfoDto> implements StaffMasterInfoDtoDao
{

	@Override
	public ListPage<StaffMasterInfoDto> getStaffMasterList(ListPage<StaffMasterInfoDto> page, String countHql, String hql) throws HibernateException
	{
		// TODO Auto-generated method stub

		return super.listBySqlDto(page, countHql, hql, new ArrayList<Serializable>(), new StaffMasterInfoDto());
	}

	public StaffMaster getByUserId(String userId)
	{
		String hql = " from StaffMaster s where s.userId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		return (StaffMaster) this.getUniqueByHql(hql, param);
	}

	@Override
	public List<StaffMasterInfoDto> getStaffMasterByPositionTitle(String positionTitle)
	{
		String hql = " from StaffMaster s where s.positionTitle = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(positionTitle);
		return this.getByHql(hql, param);
	}

	@Override
	public List<StaffMaster> getStaffMasterByStaffType(String staffType)
	{
		String hql = " from StaffMaster s where s.staffType = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(staffType);
		List<StaffMasterInfoDto> dtos = this.getByHql(hql, param);
		List<StaffMaster> retList = new ArrayList<StaffMaster>(dtos.size());
		for (Object o : dtos)
		{
			retList.add((StaffMaster) o);
		}
		return retList;
	}

	@Override
	public List<StaffMaster> getExpireStaff() {
		String hql = " from StaffMaster s where quitDate < ? and status<>";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(new Date());
		param.add("INACT");
		List<StaffMaster> list = this.getDtoByHql(hql, param, StaffMaster.class);
		return list;
	}
}
