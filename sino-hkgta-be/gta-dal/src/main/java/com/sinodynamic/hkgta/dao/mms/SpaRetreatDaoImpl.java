package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;
import com.sinodynamic.hkgta.entity.mms.SpaRetreat;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class SpaRetreatDaoImpl extends GenericDao<SpaRetreat> implements SpaRetreatDao {
	
	@Override
	public Serializable addSpaRetreat(SpaRetreat spaRetreat)
	{
		return this.save(spaRetreat);
	}
	
	@Override
	public SpaRetreat getByRetId(Long retId)
	{
		return this.get(SpaRetreat.class, retId);
	}
	
	
	public List<SpaRetreat> getAllSpaRetreats()
	{
		String hqlstr  = " from SpaRetreat m";
		List<SpaRetreat> resultList = getByHql(hqlstr);
		return resultList;
	}
	
	public Long getMaxDisplayOrder()
	{
		String sql = "select max(display_order)as maxOrder from spa_retreat";
		Object result = getUniqueBySQL(sql, null);
	    return Long.valueOf(result!=null?result.toString():"0");
	}
	
}
