package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;

public interface SpaCategoryDao extends IBaseDao<SpaCategory>{

	public Serializable addSpaCategory(SpaCategory spaCategory);
	public SpaCategory getByCategoryId(Long categoryId);
	public List<SpaCategory> getAllSpaCategorys();
}
