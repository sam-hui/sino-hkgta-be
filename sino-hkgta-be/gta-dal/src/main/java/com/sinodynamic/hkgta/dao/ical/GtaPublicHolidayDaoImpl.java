package com.sinodynamic.hkgta.dao.ical;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.ical.PublicHoliday;

@Repository
public class GtaPublicHolidayDaoImpl extends GenericDao<PublicHoliday>
		implements GtaPublicHolidayDao {

	@Override
	public PublicHoliday viewHolidayDetail(Long holiday_id) throws Exception {
		return get(PublicHoliday.class, holiday_id);
	}

	@Override
	public void deleteById(Long holiday_id) throws Exception {
		deleteById(holiday_id);
	}

	@Override
	public void saveHoliday(List<PublicHoliday> holidays) throws Exception {

		Session session = this.getsessionFactory().openSession();
		session.beginTransaction();

		int i = 0;
		for (PublicHoliday day : holidays) {
			if (session.contains(day)) {
				continue;
			} else {		
				PublicHoliday exsitingDay = isExisting(day);
				if (exsitingDay != null){
					 session.update(exsitingDay);
				} else {
					 session.save(day);	
				}
				i++;
			}

			if (i % 20 == 0) {
				session.getTransaction().commit();
				session.flush();
				session.clear();
				session.beginTransaction();
			}
		}
		session.getTransaction().commit();
	}
	
	private PublicHoliday isExisting(PublicHoliday day){
		String sql = "SELECT p FROM PublicHoliday p where p.holidayDate = ? and p.holidayType = ? ";
		List param = new ArrayList();
		param.add(day.getHolidayDate());
		param.add(day.getHolidayType());

		List<PublicHoliday> days = getByHql(sql, param);
	    if (days.size() > 0){
	    	return days.get(0);
	    }
		return null;
	}

	@Override
	@Transactional
	public List<Date> getNearestHolidays(Date date) {
		Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		int year = c.get(Calendar.YEAR);
		String sql = "select holiday_date as holidayDate from public_holiday ho where YEAR(holiday_date)= ? or YEAR(holiday_date)=? ";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(year);
		param.add(year + 1);
		List<PublicHoliday> holidays = getDtoBySql(sql.toString(), param, PublicHoliday.class);
		List<Date> dates = new ArrayList<Date>();
		for(PublicHoliday h: holidays) {
			dates.add(h.getHolidayDate());
		}
		return dates;
	}
	
}
