package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerCheckExsitDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentSearchDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.util.pagination.ListPage;


public interface CustomerProfileDao extends IBaseDao<CustomerProfile>{
	
	public ListPage<CustomerProfile> getCustomerProfileList(ListPage<CustomerProfile> page, CustomerProfileDto t);
	
    public Serializable addCustomerProfile(CustomerProfile t);
	
	public void deleteCustomerProfile(CustomerProfile t);
	
	public void updateCustomerProfile(CustomerProfile t);

	public CustomerProfile getCustomerProfile(CustomerProfile t);
	
	public CustomerProfile getCustomerProfileByCustomerId(String customerId);
	
	public CustomerProfile getById(Long customerId);
	
	public boolean checkAvailablePassportNo(Long customerId,String passportType,String passportNo);
	
	public MemberInfoDto getCustomerInfo(Long customerId);
	
	public CustomerCheckExsitDto checkAvailableByPassportTypeAndPassportNo(String passportType,String passportNO);
	
	public CustomerProfile getCustomerProfileByPassportTypeAndPassportNo(String passportType,String passportNO);
	
	public ListPage<CustomerProfile> getCustomerEnrollments(ListPage<CustomerProfile> page,CustomerEnrollmentSearchDto dto);

	public List<CustomerProfile> quickSearchCustomerIdByAcademyIdOrPassportNo(String number);

	public List<CustomerProfile> getCustomerIdByUserId(String userId);

	public DependentMemberInfoDto getDependentMemberInfoByCustomerId(Long customerId);
	
	public List<DependentMemberInfoDto> getDependentMemberInfoListBySuperiorId(Long superiorCustomerId);
	
	public CustomerProfile getCorporateMemberDetailByCustomerId(Long customerId);

	public CustomerProfile getUniqueCustomerProfileByPassportTypeAndPassportNo(String passportType, String passportNO) ;

	public boolean checkAvailableContactEmail(String contactEmail, Long customerId);
	
	public MemberDto getAcademyNoReservationStatus(Long customerId);
	
	public CustomerProfile getCustomerProfileByCustomerId(Long customerId);

	public String getNameByUserId(String userId);
	
	public Integer updateProfileAndSignatureFile(CustomerProfile customerProfile, Long customerProfileVersion) throws Exception;

	public Integer getVersionById(Long customerId);
	
	public AgeRange getServicePlanAgeRange(Long planNo) throws Exception;
	
	public CustomerProfile getLimitedCustomerInfoForTablet(Long customerId);

	public CustomerProfile getPrimaryDetailForAutoFillingDependent(Long customerId);
	
	public CustomerAccountInfoDto getPreviewCardInfoByNearestServiceAccount(Long customerId);
	
	
}