package com.sinodynamic.hkgta.dao.onlinepayment;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;

@Repository
public class PaymentGatewayDaoImpl extends GenericDao<PaymentGateway> implements
		PaymentGatewayDao {
	
}
