package com.sinodynamic.hkgta.dao.crm.pub;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.Notice;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface NoticeDao extends IBaseDao<Notice> {

	ListPage<Notice> queryNoticeList(String catId, Integer pageNumber, Integer pageSize);

}
