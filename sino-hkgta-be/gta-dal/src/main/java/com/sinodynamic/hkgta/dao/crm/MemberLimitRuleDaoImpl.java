package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanRightDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeDto;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.PlanRight;

@Repository
public class MemberLimitRuleDaoImpl extends GenericDao<MemberLimitRule> implements MemberLimitRuleDao {
	
	public Serializable saveMemberLimitRuleImpl(MemberLimitRule mLimitRule){
		return save(mLimitRule);
	}
	
	public void saveMemberLimitRule(List<MemberLimitRule> list) {
		if (list != null && list.size() > 0) {
			for (MemberLimitRule memberLimitRule : list) {
				save(memberLimitRule);
			}
		}
	}
	
	public MemberLimitRule getEffectiveByCustomerId(Long customerId){
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+" and date_format( ?,'%Y-%m-%d')>=m.effectiveDate and date_format( ?,'%Y-%m-%d') <= m.expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> ruleList = super.getByHql(hqlstr, param);
		if(null != ruleList && ruleList.size() > 0)
			return ruleList.get(0);
		return null;
	}
	
	public List<MemberLimitRule>  getEffectiveListByCustomerId(Long customerId){
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+" and date_format( ?,'%Y-%m-%d')>=m.effectiveDate and date_format( ?,'%Y-%m-%d') <= m.expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		return getByHql(hqlstr, param);
	}
	
	@Override
	public BigDecimal getTransactionLimitOfMember(Long customerId){
		String hql="select numValue from MemberLimitRule where customerId = ? and limitType =? "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(LimitType.TRN.name());
		param.add(currentDate);
		param.add(currentDate);
		BigDecimal tran = (BigDecimal) getUniqueByHql(hql,param);
		if(tran == null){
			return null;
		}
		//this.updateMemberFacilitiesRight((long)1,"allow");
		return tran.setScale(2);
	}
	
	@Override
	public boolean getDayPassPurchaseNoOfMember(Long customerId){
		String hql="from MemberLimitRule where customerId = ? and limitType =? "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(PlanRight.D1.name());
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> daypassPermission = getByHql(hql,param);
		for(MemberLimitRule item : daypassPermission){
				if (item != null){
				if (item.getNumValue().equals(new BigDecimal("0.00"))){
					return false;
				} 
			}
		}
		return true;
	}
    

	@Override
	public boolean getMemberTrainingRight(Long customerId) {
		String hql ="from MemberLimitRule where customerId = ? and limitType like 'TRAIN%' "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> permissionList = getByHql(hql,param);
		for(MemberLimitRule item : permissionList){
			if (item.getTextValue().equals("true")){
				return true;
			} 
		}
		return false;
	}

	@Override
	public boolean getMemberEventsRight(Long customerId) {
		String hql ="from MemberLimitRule where customerId = ? and limitType like 'EVENT%' "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> permissionList = getByHql(hql,param);
		for(MemberLimitRule item : permissionList){
			if (item.getTextValue().equals("true") ){
				return true;
			} 
		}
		return false;
	}
	
	@Override
	public boolean updateMmberTransactionLimit(Long customerId, BigDecimal tran){
		String sql="update member_limit_rule set num_value = ? where customer_id = ? and limit_type =? "
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
//		String sql2="select num_value as numValue from member_limit_rule where customer_id = (select superior_member_id from member where customer_id = ?) and limit_type = ?";
//		String sql3="select available_balance as availableBanlance from member_cashvalue where customer_id = (select superior_member_id from member where customer_id = ?)";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(tran);
		param1.add(customerId);
		param1.add(LimitType.TRN.name());
		param1.add(currentDate);
		param1.add(currentDate);
		List<Serializable> param2 = new ArrayList<Serializable>();
		param2.add(customerId);	
		param2.add(LimitType.CR.name());
		List<Serializable> param3 = new ArrayList<Serializable>();
		param3.add(customerId);		
//		List<ServicePlanRightDto> numValue = getDtoBySql(sql2, param2,ServicePlanRightDto.class);
//		List<ServicePlanRightDto> availableBanlance = getDtoBySql(sql3, param3,ServicePlanRightDto.class);
//		if(numValue==null)return false;
//		if(availableBanlance==null) return false;
		
//		BigDecimal limit = numValue.get(0).getNumValue();
//		BigDecimal balance = availableBanlance.get(0).getAvailableBanlance();
	
//		if(limit == null) limit= new BigDecimal(0);
//		if(balance == null) balance= new BigDecimal(0);
//		BigDecimal total = limit.add(balance);
//		if(tran.compareTo(total) == 1)return false;
		
		int i = sqlUpdate(sql, param1);
		if (i >0) return true;
		return false;
	}
	
	@Override
	public boolean updateMemberDayPassPurchasing(Long customerId, String dayPass){
		String sql="update member_limit_rule set num_value = '0' where customer_id = ? and limit_type =? "
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql2="update member_limit_rule set num_value = ?  where customer_id = ? and limit_type =?"
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql3="select num_value as numValue from member_limit_rule where customer_id = (select superior_member_id from member where customer_id = ?) and limit_type =?"
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(customerId);
		param1.add(PlanRight.D1.name());
		param1.add(currentDate);
		param1.add(currentDate);
		int i = 0;
		if(dayPass.equals("1")){
			List<ServicePlanRightDto> dayPassValueList = getDtoBySql(sql3, param1, ServicePlanRightDto.class);
			if(dayPassValueList.size()>0){
				for(ServicePlanRightDto item: dayPassValueList){
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(item.getNumValue());
					param2.add(customerId);
					param2.add(PlanRight.D1.name());
					param2.add(currentDate);
					param2.add(currentDate);
					i += sqlUpdate(sql2, param2);
					}
			}
		}else if(dayPass.equals("0")){
			i = sqlUpdate(sql, param1);
			
		}
		if (i >0) return true;
		return false;
		
	}
	
	@Override
	public boolean updateMemberFacilitiesRight(Long customerId,String facilityRight){
		String sql="update member_plan_facility_right m set m.permission = 'NO' where m.customer_id = ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effective_date and m.expiry_date";
		String sql2="update member_plan_facility_right m set m.permission = ? where m.customer_id = ? and m.facility_type_code = ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effective_date and m.expiry_date";
		String sql3="select facility_type_code as typeCode,permission from member_plan_facility_right where customer_id = (select superior_member_id from member where customer_id = ?)"
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN effective_date and expiry_date";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(customerId);	
		param1.add(currentDate);
		int i = 0;	
		if(facilityRight.equals("1")){
			List<FacilityTypeDto> permissionMappingList = getDtoBySql(sql3, param1, FacilityTypeDto.class);
			if(permissionMappingList.size()>0){
				for (FacilityTypeDto item : permissionMappingList) {
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(item.getPermission());
					param2.add(customerId);	
					param2.add(item.getTypeCode());
					param2.add(currentDate);
					i+=sqlUpdate(sql2, param2);
				}
			}
		}
		else if(facilityRight.equals("0")){
			i = sqlUpdate(sql, param1);
		}
		
		if (i >0) return true;
		return false;
	}
	
	@Override
	public boolean updateMemberTrainingRight(Long customerId,String trainRight){
		String sql="update member_limit_rule set text_value = 'false' where customer_id = ? and limit_type like 'TRAIN%' "
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql2="update member_limit_rule set text_value = 'true' where customer_id = ? and limit_type = ?"
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql3="select right_code as rightCode from service_plan_right_master where right_type = ?";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(customerId);	
		param1.add(currentDate);
		param1.add(currentDate);
		List<Serializable> param3 = new ArrayList<Serializable>();
		param3.add(PlanRight.TRAIN1.getType());
		int i = 0;
		
		if(trainRight.equals("1")){
			List<ServicePlanRightDto> trainingRightCodeList = getDtoBySql(sql3, param3, ServicePlanRightDto.class);
			if(trainingRightCodeList.size()>0){
				for(ServicePlanRightDto item: trainingRightCodeList){
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(customerId);	
					param2.add(item.getRightCode());
					param2.add(currentDate);
					param2.add(currentDate);
					i += sqlUpdate(sql2, param2);
					}
			}
		}else if(trainRight.equals("0")){
			i = sqlUpdate(sql, param1);
			
		}
		if (i >0) return true;
		return false;
		
		
	}
	
	@Override
	public boolean updateMemberEventsRight(Long customerId,String eventRight){
		String sql="update member_limit_rule set text_value = 'false' where customer_id = ? and limit_type like 'EVENT%'  "
				+ " and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql2="update member_limit_rule set text_value = 'true' where customer_id = ? and limit_type = ?  "
				+ " and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date";
		String sql3="select right_code as rightCode from service_plan_right_master where right_type = ?";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(customerId);
		param1.add(currentDate);
		param1.add(currentDate);
		
		List<Serializable> param3 = new ArrayList<Serializable>();
		param3.add(PlanRight.EVENT1.getType());
		int i = 0;
		
		if(eventRight.equals("1")){
			List<ServicePlanRightDto> eventsRightCodeList = getDtoBySql(sql3, param3, ServicePlanRightDto.class);
			if(eventsRightCodeList.size()>0){
				for(ServicePlanRightDto item: eventsRightCodeList){
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(customerId);	
					param2.add(item.getRightCode());
					param2.add(currentDate);
					param2.add(currentDate);
					i += sqlUpdate(sql2, param2);
					}
			}
		}else if(eventRight.equals("0")){
			i = sqlUpdate(sql, param1);
			
		}
		if (i >0) return true;
		return false;
	}
	
	public int deleteMemberLimitRule(Long customerId,String excludedLimitType) {
		if(StringUtils.isEmpty(excludedLimitType)){
			Object[] param = new Object[]{customerId};
			return this.deleteByHql("DELETE m from member_limit_rule m where m.customer_id = ? ",param);
		}else{
			Object[] param = new Object[]{customerId,excludedLimitType};
			return this.deleteByHql("DELETE m from member_limit_rule m where m.customer_id = ? and m.limit_type != ? ",param);
		}
		
	}
	
	
	public List<MemberLimitRuleDto> getEffectiveMemberLimitRuleDtoByCustomerId(Long customerId) {
		System.out.println("================Current Date is:"+ new Date() + "====================");
		String sql = " select mlr.limit_type as rightCode,\n"
				+ "CASE \n"
				+ "   when sprm.input_value_type = 'INT' THEN round(mlr.num_value)\n"
				+ "   when sprm.input_value_type = 'TXT' THEN mlr.text_value\n"
				+ "END as inputValue,\n"
				+ "sprm.right_type as rightType,\n"
				+ "mlr.description as description\n"
				+ "from member_limit_rule mlr, service_plan_right_master sprm where mlr.limit_type = sprm.right_code and mlr.customer_id = ? and date_format( ?,'%Y-%m-%d')>=mlr.effective_date and date_format( ?,'%Y-%m-%d') <= mlr.expiry_date";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(new Date());
		param.add(new Date());
		return getDtoBySql(sql, param, MemberLimitRuleDto.class);
	}

	@Override
	public MemberLimitRule getEffectiveMemberLimitRule(Long customerId, String limitType)
	{
		System.out.println("================Current Date is:"+ new Date() + "====================");
		String hql="from MemberLimitRule where customerId = ? and limitType =? and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(limitType);
		param.add(new Date());
		param.add(new Date());
		return (MemberLimitRule) getUniqueByHql(hql,param);
	}

}
