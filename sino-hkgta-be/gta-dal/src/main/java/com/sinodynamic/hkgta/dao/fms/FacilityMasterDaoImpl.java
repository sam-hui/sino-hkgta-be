package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterQueryDto;
import com.sinodynamic.hkgta.dto.fms.IntervalParam;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;

@Repository
public class FacilityMasterDaoImpl extends GenericDao<FacilityMaster> implements FacilityMasterDao {

	@Override
	public List<FacilityMaster> getFacilityMasterList(Integer venueFloor, String facilityType) {

		StringBuffer hql = new StringBuffer(
				"from FacilityMaster as fm where fm.status = 'ACT' and upper(fm.facilityTypeBean.typeCode) = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());

		if (venueFloor != null) {
			hql.append(" and fm.venueFloor=?");
			paramList.add(venueFloor.longValue());
		}
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<FacilityMaster> getFacilityVenueFloorList(String facilityType, String venueCode) {

		StringBuffer hql = new StringBuffer();
		hql.append(" SELECT DISTINCT new FacilityMaster(fm.venueFloor)  ").append(" FROM FacilityMaster as fm  ")
				.append(" where  fm.status = 'ACT' AND fm.facilityTypeBean.typeCode = ? and fm.venueFloor is not null ");
		if (!StringUtils.isEmpty(venueCode)) {
			hql.append("  and fm.venueMaster.venueCode=? ");
		}
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		if (!StringUtils.isEmpty(venueCode)) {
			paramList.add(venueCode);
		}

		return super.getByHql(hql.toString(), paramList);
	}

    @Override
    public FacilityMaster getByFacilityNo(String facilityNo) {
    	return get(FacilityMaster.class, facilityNo);
    }
    
    @Override
    public FacilityMaster getByFacilityNo(long facilityNo){
    	return get(FacilityMaster.class, facilityNo);
    }

    @Override
    public List<FacilityMaster> getFacilityMasterByIdBatch(String[] facilityNos) {
	
		if (facilityNos == null || facilityNos.length == 0)
		    return null;
	
		StringBuilder hqlComp = new StringBuilder(
			"from FacilityMaster where facilityNo in (");
		for (String facilityNo : facilityNos) {
		    hqlComp.append(facilityNo).append(",");
		}
		String hqlTemp = hqlComp.toString();
		String hqlStr = hqlTemp.substring(0, hqlTemp.length() - 1) + ")";
	
		return getByHql(hqlStr);
    }

	@Override
	public List<FacilityMasterQueryDto> getFacilityMasterList(String facilityType)
	{
		String hql = "select fm.venueFloor AS venueFloor,fm.facilityNo as facilityNo,fm.facilityTypeBean.typeCode as facilityType,fm.facilityName as facilityName,fm.status AS status,faa.inputValue as ballFeedingStatus "+ 
					 " from FacilityMaster fm  , FacilityAdditionAttribute faa where fm.facilityNo = faa.id.facilityNo and faa.id.attributeId ='BALLFEEDSWITCH' "+
					 " and UPPER(fm.facilityTypeBean.typeCode)=? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(facilityType.toUpperCase());
		return getDtoByHql(hql, param, FacilityMasterQueryDto.class);
	}

	@Override
	public int getFacilityMasterCount(String facilityType, String facilityAttribute)
	{
		String sql ="";
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			sql = "select count(1) from facility_master a, facility_addition_attribute b, facility_attribute_caption c where a.facility_no = b.facility_no and b.attribute_id = c.attribute_id and UPPER(a.facility_type) = ? "
					+ " and b.attribute_id = ? and a.status ='ACT' ";
		}else{
			sql = "select count(1) from facility_master a where UPPER(a.facility_type) = ? "
					+ " and a.facility_subtype_id = ? and a.status ='ACT' ";
		}
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(facilityAttribute);
		Object count = super.getUniqueBySQL(sql, paramList);
		return ((Number)count).intValue();
	}

    @Override
    public List<FacilityMaster> getAllVenueFloor() {

	String hql = "select distinct f.venueFloor as venueFloor from FacilityMaster f where f.venueFloor is not null order by f.venueFloor";
	return (List<FacilityMaster>) getDtoByHql(hql, null, FacilityMaster.class);
    }

    @Override
    public List<FacilityItemDto> getFacilityStatusByFloorAndTime(String type,
	    String beginDatetime, String endDatetime, Long floor) {

	String startTime = "'" + beginDatetime + "'";
	String endTime = "'" + endDatetime + "'";
	String facilityType = "'" + type + "'";
	
	String floorStatement = null;
	if ("TENNIS".equals(type)) {
	    floorStatement = "is null";
	} else if ("GOLF".equals(type)) {
	    floorStatement = "= " + floor;
	}

	String sql = "select distinct"
		+ " fm.facility_no as facilityNo,"
		+ " fm.facility_name as facilityName,"
		+ " ifnull(ft_op.status, ifnull(ft_rs.status, ifnull(ft_mt.status, 'VC'))) as status,"
		+ " ifnull(vm.venue_name, 'null') as venueName,"
//		+ " faa.attribute_id as attributeId"
		+ " GROUP_CONCAT(faa.attribute_id separator ',') AS attributes "
		+ " from facility_master fm left join facility_addition_attribute faa ON fm.facility_no = faa.facility_no"
		+ " left join venue_master vm on fm.venue_code = vm.venue_code"
		+ " left join (select facility_no, status from facility_timeslot where end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and status = 'MT') ft_mt on fm.facility_no = ft_mt.facility_no "
		+ "left join (select facility_no, status from facility_timeslot where end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and status in ('OP', 'TA')) ft_op on fm.facility_no = ft_op.facility_no "
		+ "left join (select facility_no, status from facility_timeslot where end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and status = 'RS') ft_rs on fm.facility_no = ft_rs.facility_no "
		+ "where fm.venue_floor " + floorStatement
		+ " and fm.status = 'ACT' and " + "fm.facility_type = "
		+ facilityType;
	
//        	if ("GOLF".equals(type)) {
//        	    sql = sql + " AND faa.attribute_id in ('GOLFBAYTYPE-RH', 'GOLFBAYTYPE-LH', 'GOLFBAYTYPE-CAR')";
//        	}
        	sql = sql + " group by fm.facility_no order by fm.facility_no";

	return (List<FacilityItemDto>) getDtoBySql(sql, null,
		FacilityItemDto.class);
    }

    @Override
    public List<FacilityItemDto> getAdditionFacilityStatusByTime(String type, String beginDatetime, String endDatetime) {
	
	String startTime = "'" + beginDatetime + "'";
	String endTime = "'" + endDatetime + "'";
	String facilityType = "'" + type + "'";

	String sql = "select distinct "
		+ "fm.facility_no as facilityNo, "
		+ "fm.facility_name as facilityName, "
		+ "ifnull(ft_op.status, ifnull(ft_rs.status, ifnull(ft_mt.status, 'VC'))) as status "
		+ "from facility_master fm left join facility_addition_attribute faa on fm.facility_no = faa.facility_no "
		+ "left join (select facility_no, status from facility_timeslot where end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and status = 'MT') ft_mt on fm.facility_no = ft_mt.facility_no "
		+ "left join (select facility_no, status from facility_timeslot where end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and status in ('OP', 'TA')) ft_op on fm.facility_no = ft_op.facility_no "
		+ "left join (select facility_no, status from facility_timeslot where end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and status = 'RS') ft_rs on fm.facility_no = ft_rs.facility_no where "
		+ "fm.status = 'ACT' and fm.facility_type = "
		+ facilityType
		+ " and faa.attribute_id = 'GOLFBAYTYPE-CAR'"
		+ " order by fm.facility_no";

	return (List<FacilityItemDto>) getDtoBySql(sql, null, FacilityItemDto.class);
    }

	@Override
	public List<FacilityMaster> getFacilityMasterAvailableList(String facilityType, String facilityAttribute,Date[] times,Integer venueFloor)
	{
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityAttribute);
		String hql ="";
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			hql = " select a from FacilityMaster a " 
					+ " , FacilityAdditionAttribute b  " 
					+ " where a.facilityNo = b.id.facilityNo and a.status ='ACT' and b.id.attributeId = ? and a.facilityNo not in ( select distinct t.facilityMaster.facilityNo from FacilityTimeslot t" 
					+ " where t.facilityMaster.facilityNo is not null and beginDatetime in ( ";
			for (int i = 0; i < times.length; i++)
			{
				hql += "?,";
				paramList.add(times[i]);
			}
			hql = hql.substring(0, hql.length() - 1);
			hql += " ) and t.status <> ? ) and a.facilityTypeBean.typeCode=?  and a.venueFloor=? order by a.facilityNo ASC ";
			paramList.add(FacilityStatus.CAN.name());
			paramList.add(facilityType.toUpperCase());
			paramList.add(venueFloor.longValue());
		}else{
			hql = " select a from FacilityMaster a " 
					+ " where a.status ='ACT' and a.facilitySubtypeId=? and a.facilityNo not in ( select distinct t.facilityMaster.facilityNo from FacilityTimeslot t" 
					+ " where t.facilityMaster.facilityNo is not null and beginDatetime in ( ";
			for (int i = 0; i < times.length; i++)
			{
				hql += "?,";
				paramList.add(times[i]);
			}
			hql = hql.substring(0, hql.length() - 1);
			hql += " ) and t.status <> ? ) and a.facilityTypeBean.typeCode=?   order by a.facilityNo ASC ";
			paramList.add(FacilityStatus.CAN.name());
			paramList.add(facilityType.toUpperCase());
		}
		
		return super.getByHql(hql, paramList);
	}

	@Override
	public List<BayTypeDto> getFacilityBayType(String facilityType) {
		String sql = "";
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			sql = "select a.attributeId as id, a.caption as value from FacilityAttributeCaption a where a.facilityType=?";
		}else{
			sql = "select a.subTypeId as id, a.name as value from FacilitySubType a where a.facilityTypeCode=?";
		}
		List<BayTypeDto> dtoList = this.getDtoByHql(sql, Arrays.asList(facilityType), BayTypeDto.class);
		return dtoList;
	}

	@Override
	public List<FacilityMaster> getFacilityByAttribute(String facilityType, String facilityAttribute) {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT DISTINCT fm FROM FacilityMaster fm, FacilityAdditionAttribute at ");
		hql.append(" where fm.status = 'ACT' AND fm.facilityNo = at.id.facilityNo AND fm.facilityTypeBean.typeCode = ? ");
		hql.append(" AND at.id.attributeId=?");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(facilityAttribute);

		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public int getFacilityMasterCount(String facilityType, String facilityAttribute,List<Long> floors,List<Long> facilityNos)
	{
		List<Serializable> paramList = new ArrayList<Serializable>();
		String sql ="";
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			sql = "select count(1) from facility_master a, facility_addition_attribute b, facility_attribute_caption c where a.facility_no = b.facility_no and b.attribute_id = c.attribute_id and UPPER(a.facility_type) = ? "
					+ " and b.attribute_id = ? and a.status ='ACT' ";
			paramList.add(facilityType.toUpperCase());
			paramList.add(facilityAttribute);
			if(null != floors && floors.size() > 0){
				sql += " and a.venue_floor in ( ";
				for(Long floor : floors){
					sql += " ?,";
					paramList.add(floor);
				}
				sql = sql.substring(0,sql.length()-1);
				sql += " )";
			}
			
			if(null != facilityNos && facilityNos.size() > 0){
				sql += " and a.facility_no in ( ";
				for (int i = 0; i < facilityNos.size(); i++)
				{
					sql += "?,";
					paramList.add(facilityNos.get(i).intValue());
				}
				sql = sql.substring(0,sql.length()-1);
				sql += " )";
			}
		}else{
			sql = "select count(1) from facility_master a where UPPER(a.facility_type) = ? "
					+ " and a.facility_subtype_id = ? and a.status ='ACT' ";
			paramList.add(facilityType.toUpperCase());
			paramList.add(facilityAttribute);
		}
		Object count = super.getUniqueBySQL(sql, paramList);
		return ((Number)count).intValue();
	}

	@Override
	public int countFacilityMasterByType(String facilityType, List<Long> floors, List<Long> facilityNos)
	{
		String hql ="";
		List<Serializable> paramList = new ArrayList<Serializable>();
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			hql = "select count(a.facilityNo) from FacilityMaster a, FacilityAdditionAttribute b, FacilityAttributeCaption c where a.facilityNo = b.id.facilityNo and b.id.attributeId = c.attributeId and UPPER(a.facilityTypeBean.typeCode) = ? "
					+ " and b.id.attributeId like ? and a.status ='ACT' ";
			paramList.add(facilityType.toUpperCase());
			paramList.add("GOLFBAYTYPE%");
			if(null != floors && floors.size() > 0){
				hql += " and a.venueFloor in ( ";
				for(Long floor : floors){
					hql += " ?,";
					paramList.add(floor.longValue());
				}
				hql = hql.substring(0,hql.length()-1);
				hql += " )";
			}
			
			if(null != facilityNos && facilityNos.size() > 0){
				hql += " and a.facilityNo in ( ";
				for (int i = 0; i < facilityNos.size(); i++)
				{
					hql += "?,";
					paramList.add(facilityNos.get(i));
				}
				hql = hql.substring(0,hql.length()-1);
				hql += " )";
			}
		}else{
			hql = "select count(a.facilityNo) from FacilityMaster a where UPPER(a.facilityTypeBean.typeCode) = ? "
					+ " and a.facilitySubtypeId is not null and a.status ='ACT' ";
			paramList.add(facilityType.toUpperCase());
		}
		Object count = super.getUniqueByHql(hql, paramList);
		return ((Number)count).intValue();
	}

	@Override
	public List<FacilityMaster> getFacilityMasterAvailableList(String facilityType, String facilityAttribute, Date[] times, List<Long> floors, List<Long> facilityNos)
	{
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityAttribute);
		String hql ="";
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			hql = " select a from FacilityMaster a " 
					+ " , FacilityAdditionAttribute b  " 
					+ " where a.facilityNo = b.id.facilityNo and a.status ='ACT' and b.id.attributeId = ? and a.facilityNo not in ( select distinct t.facilityMaster.facilityNo from FacilityTimeslot t" 
					+ " where t.facilityMaster.facilityNo is not null and beginDatetime in ( ";
			for (int i = 0; i < times.length; i++)
			{
				hql += "?,";
				paramList.add(times[i]);
			}
			hql = hql.substring(0, hql.length() - 1);
			hql += " ) and t.status <> ? ) and a.facilityTypeBean.typeCode=? ";
			paramList.add(FacilityStatus.CAN.name());
			paramList.add(facilityType.toUpperCase());
			
			if(null != floors && floors.size() > 0){
				hql += " and a.venueFloor in ( ";
				for(Long floor : floors){
					hql += " ?,";
					paramList.add(floor.longValue());
				}
				hql = hql.substring(0,hql.length()-1);
				hql += " )";
			}
			
			if(null != facilityNos && facilityNos.size() > 0){
				hql += " and a.facilityNo in ( ";
				for (int i = 0; i < facilityNos.size(); i++)
				{
					hql += "?,";
					paramList.add(facilityNos.get(i));
				}
				hql = hql.substring(0,hql.length()-1);
				hql += " )";
			}
		}else{
			hql = " select a from FacilityMaster a " 
					+ " where a.status ='ACT' and a.facilitySubtypeId=? and a.facilityNo not in ( select distinct t.facilityMaster.facilityNo from FacilityTimeslot t" 
					+ " where t.facilityMaster.facilityNo is not null and beginDatetime in ( ";
			for (int i = 0; i < times.length; i++)
			{
				hql += "?,";
				paramList.add(times[i]);
			}
			hql = hql.substring(0, hql.length() - 1);
			hql += " ) and t.status <> ? ) and a.facilityTypeBean.typeCode=? ";
			paramList.add(FacilityStatus.CAN.name());
			paramList.add(facilityType.toUpperCase());
		}
		return super.getByHql(hql, paramList);
	}
    
}