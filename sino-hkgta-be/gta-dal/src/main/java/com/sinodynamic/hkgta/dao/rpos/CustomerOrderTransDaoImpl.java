package com.sinodynamic.hkgta.dao.rpos;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.MemberTopupDto;
import com.sinodynamic.hkgta.dto.crm.MemberTransactionDto;
import com.sinodynamic.hkgta.dto.crm.MonthlyEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.MonthlyPrivateCoachingRevenueDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.SettlementReportDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.pagination.ListPage;


@Repository
public class CustomerOrderTransDaoImpl extends GenericDao<CustomerOrderTrans>
		implements CustomerOrderTransDao {

	@Override
	public List<CustomerOrderTrans> getPaymentDetailsByOrderNo(Long orderNo) {
		String hql = "from CustomerOrderTrans c where c.customerOrderHd.orderNo=" + orderNo + " order by transactionTimestamp desc";
		
		List<CustomerOrderTrans> customerOrderTranses = getByHql(hql);
		return customerOrderTranses;
	}

	@Override
	public List<CustomerOrderTrans> getPaymentDetailsByTransactionNO(Long transactionNO) {
		String hql = "from CustomerOrderTrans c where c.transactionNo= " + transactionNO;
		
		List<CustomerOrderTrans> customerOrderTranses = getByHql(hql);
		return customerOrderTranses;
	}

	@Override
	public List getLatestTransationActivaties(int totalSize) {
		String sql = "SELECT cot.transaction_timestamp update_date,cot.`status` status,CONCAT_WS(' ',cp.salutation,cp.given_name,cp.surname) username "
				+ "from customer_order_hd coh INNER JOIN customer_order_trans cot INNER JOIN customer_profile cp "
				+ "ON coh.order_no = cot.order_no AND coh.customer_id = cp.customer_id AND cot.`status` in ('SUC','NSC') ORDER BY  update_date DESC LIMIT 0,"+totalSize;
		return getCurrentSession().createSQLQuery(sql).list();
	}

	public Serializable saveCustomerOrderTrans(
			CustomerOrderTrans customerOrderTrans) {
		return save(customerOrderTrans);
	}
	@Override
	public ListPage<CustomerOrderTrans> getTransactionList(ListPage<CustomerOrderTrans> page,Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending,String clientType, String filterSQL) {
		String hql = "SELECT CONCAT(p.salutation, ' ',p.givenName, ' ',p.surname ) as memberName,"
				+ " c.transactionTimestamp as transactionTimestamp, date_format(c.transactionTimestamp,'%Y-%b-%d') as transactionDate, date_format(c.transactionTimestamp,'%H:%i') as transactionTime,"
				+ " c.transactionNo as transactionNo,c.paymentMethodCode as paymentMethodCode,c.paidAmount as paidAmount,ps.itemCatagory as itemCatagory,ps.description as description "
				+ " from CustomerOrderTrans c,CustomerOrderHd h,CustomerProfile p,CustomerOrderDet d,Member m ,PosServiceItemPrice ps"
				+ " WHERE c.customerOrderHd.orderNo = h.orderNo  and h.orderNo = d.customerOrderHd.orderNo ";
		if (!"APP".equals(clientType)) {
			hql += " and d.itemNo != 'CVT0000001' AND d.itemNo != 'SRR00000001' AND d.itemNo != 'CVR00000001' ";
		}
		hql += " and (c.status = ? or c.status=?) and h.customerId = m.customerId"
				+ " and m.customerId = p.customerId and (m.customerId = ? or m.superiorMemberId = ?)and d.itemNo = ps.itemNo"
				+ " and d.orderDetId=(select min(dd.orderDetId) from CustomerOrderDet dd where dd.customerOrderHd.orderNo=h.orderNo) <AdvancedSearch> <OrderBy>";
		String countHql = "SELECT count(h.orderNo)"
				+ " from CustomerOrderTrans c,CustomerOrderHd h,CustomerProfile p,CustomerOrderDet d,Member m ,PosServiceItemPrice ps"
				+ " WHERE c.customerOrderHd.orderNo = h.orderNo  and h.orderNo = d.customerOrderHd.orderNo ";
		if (!"APP".equals(clientType)) {
			countHql += " and d.itemNo != 'CVT0000001' AND d.itemNo != 'SRR00000001' AND d.itemNo != 'CVR00000001' ";
		}
		countHql +=" and (c.status = ? or c.status=?) and h.customerId = m.customerId"
				+ " and m.customerId = p.customerId and (m.customerId = ? or m.superiorMemberId = ?)and d.itemNo = ps.itemNo"
				+ " and d.orderDetId=(select min(dd.orderDetId) from CustomerOrderDet dd where dd.customerOrderHd.orderNo=h.orderNo) <AdvancedSearch> ";
		
		if(org.apache.commons.lang.StringUtils.isNotEmpty(filterSQL)){
			hql = hql.replace("<AdvancedSearch>", "and " + filterSQL);
			countHql = countHql.replace("<AdvancedSearch>", "and " + filterSQL);
		}else{
			hql = hql.replace("<AdvancedSearch>", "");
			countHql = countHql.replace("<AdvancedSearch>", "");
		}
		
		StringBuilder orderBy = new StringBuilder("");
		if(CommUtil.notEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled + " asc ");
			}else{
				orderBy.append(orderByFiled + " desc ");
			}
		}
		hql = hql.replace("<OrderBy>", orderBy.toString());
		List<Object> param = new ArrayList<Object>();
		param.add(CustomerTransationStatus.SUC.getDesc());
		param.add(CustomerTransationStatus.REFUND.getDesc());
		param.add(customerID);
		param.add(customerID);
		return listByHqlDto(page, countHql, hql, param, new MemberTransactionDto());
	}

	@Override
	public ListPage<CustomerOrderTrans> getTopupHistory(
			ListPage<CustomerOrderTrans> page, Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String filterSQL) {
		String hql = "SELECT CONCAT(p.salutation, ' ',p.givenName, ' ',p.surname ) as memberName,c.transactionTimestamp as transactionTimestamp,c.transactionNo as transactionNo,c.paymentMethodCode as paymentMethodCode,c.paidAmount as paidAmount"
				+ " from CustomerOrderTrans c,CustomerOrderHd h,CustomerProfile p,CustomerOrderDet d,Member m "
				+ " WHERE c.customerOrderHd.orderNo = h.orderNo  and h.orderNo = d.customerOrderHd.orderNo and d.itemNo = 'CVT0000001' and h.customerId = m.customerId"
				+ " and c.status = ? and m.customerId = p.customerId and (m.customerId = ? or m.superiorMemberId = ?)"
				+ " and d.orderDetId=(select min(dd.orderDetId) from CustomerOrderDet dd where dd.customerOrderHd.orderNo=h.orderNo) <AdvancedSearch> <OrderBy>";
		String countHql = "SELECT count(h.orderNo)"
				+ " from CustomerOrderTrans c,CustomerOrderHd h,CustomerProfile p,CustomerOrderDet d,Member m "
				+ " WHERE c.customerOrderHd.orderNo = h.orderNo  and h.orderNo = d.customerOrderHd.orderNo and d.itemNo = 'CVT0000001' and h.customerId = m.customerId"
				+ " and c.status = ? and m.customerId = p.customerId and (m.customerId = ? or m.superiorMemberId = ?)"
				+ " and d.orderDetId=(select min(dd.orderDetId) from CustomerOrderDet dd where dd.customerOrderHd.orderNo=h.orderNo) <AdvancedSearch> ";
		
		if(org.apache.commons.lang.StringUtils.isNotEmpty(filterSQL)){
			hql = hql.replace("<AdvancedSearch>", "and " + filterSQL);
			countHql = countHql.replace("<AdvancedSearch>", "and " + filterSQL);
		}else{
			hql = hql.replace("<AdvancedSearch>", "");
			countHql = countHql.replace("<AdvancedSearch>", "");
		}
		
		StringBuilder orderBy = new StringBuilder("");
		if(CommUtil.notEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled + " asc ");
			}else{
				orderBy.append(orderByFiled + " desc ");
			}
		}
		hql = hql.replace("<OrderBy>", orderBy.toString());
		List<Object> param = new ArrayList<Object>();
		param.add(CustomerTransationStatus.SUC.getDesc());
		param.add(customerID);
		param.add(customerID);
		return listByHqlDto(page, countHql, hql, param, new MemberTopupDto());
	}

	@Override
	public List<CustomerOrderTrans> getTimeoutPayments(Long timeoutMin) {
		String hql = "from CustomerOrderTrans c where c.status = ? and c.paymentMedia = ? and TIMESTAMPDIFF(MINUTE,c.transactionTimestamp,NOW()) > ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add("PND");
		param.add("OP");
		param.add(timeoutMin);
		return getByHql(hql, param);
	}
	
	public BigDecimal getApprovedPaymentAmount(Long orderNo){
		String sql = "select sum(ct.paid_amount) as paidAmount from customer_order_trans ct where ct.status = ? GROUP BY ct.order_no having ct.order_no = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(CustomerTransationStatus.SUC.name());
		param.add(orderNo);
		Object paidAmount = getUniqueBySQL(sql, param);
		BigDecimal approvedAmount =(BigDecimal) paidAmount;
		return approvedAmount;
	}

	@Override
	public BigDecimal getCustomerOrderTransAmoutByDay(long customerId, Date date)
	{
		String sql = "select sum(paidAmount) as totalAmount from CustomerOrderTrans c where c.status = ? and c.customerOrderHd.customerId = ? and date(c.transactionTimestamp) =? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(Constant.Status.SUC.toString());
		param.add(customerId);
		param.add(date);
		Object totalAmount = super.getUniqueBySQL(sql, param);
		return new BigDecimal(((Number)totalAmount).doubleValue());
	}

	@Override
	public BigDecimal getCustomerOrderTransAmoutByMonth(long customerId, int month)
	{
		String sql = "select sum(paidAmount) as totalAmount from CustomerOrderTrans c where c.status = ? and c.customerOrderHd.customerId = ? and MONTH(c.transactionTimestamp) =? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(Constant.Status.SUC.toString());
		param.add(customerId);
		param.add(month);
		Object totalAmount = super.getUniqueBySQL(sql, param);
		return new BigDecimal(((Number)totalAmount).doubleValue());
	}
	
	public byte[] getInvoiceReceipt(Long orderNo,String transactionNo,String receiptType) throws JRException{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/"+"jasper/";
		String parentjasperPath = null;
		if(!StringUtils.isEmpty(transactionNo) || (!StringUtils.isEmpty(orderNo) && "daypass".equalsIgnoreCase(receiptType))){
			parentjasperPath = reportPath+"receipt.jasper";
		}else if(!StringUtils.isEmpty(orderNo) && !"daypass".equalsIgnoreCase(receiptType)){
			parentjasperPath = reportPath+"invoice.jasper";
		}
		String baseIreport = reportPath;
		File reFile = new File(parentjasperPath);
		Map<String,Object> parameters = new HashMap<String,Object>();
		DataSource ds=SessionFactoryUtils.getDataSource(this.getsessionFactory());
	    Connection dbconn=DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		
		if(!StringUtils.isEmpty(transactionNo)){
			parameters.put("signature",reportPath+"signature.jpg" );
			if ("topup".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_topup.jasper");
			}else if ("course".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_course.jasper");
			}else if ("daypass".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_daypass.jasper");
			}else if ("guestroombook".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_guestroombook.jasper");
			}else if ("golfbay".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_golfbay.jasper");
			}else if ("tenniscourt".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_tenniscourt.jasper");
			}else if ("golfcoach".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_golfcoach.jasper");
			}else if ("tenniscoach".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_tenniscoach.jasper");
			}else if ("wellness".equalsIgnoreCase(receiptType)) {
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport_wellness.jasper");
			}else{
				parameters.put("SUBREPORT_DIR", baseIreport+"receipt_subreport.jasper");
			}
			parameters.put("transactionNo", transactionNo);
		}else if(!StringUtils.isEmpty(orderNo) && !"daypass".equalsIgnoreCase(receiptType)){
			parameters.put("SUBREPORT_DIR", baseIreport+"invoice_subreport.jasper");
			parameters.put("orderNo", orderNo);
		}
		
		parameters.put("logo_header",reportPath+"logo_header.jpg");
		parameters.put("logo_footer",reportPath+"logo_footer.jpg");
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));

		exporter.exportReport();
		return outPut.toByteArray();
		
	}

	@Override
	public ListPage<CustomerOrderTrans> getSattlementReport(ListPage<CustomerOrderTrans> page,
			SearchSettlementReportDto dto) {
		String sql = "SELECT  trans.transaction_timestamp as transactionDate,"
				+ " trans.transaction_no as transactionId,hd.order_no as orderNo,hd.order_date as orderDate, "
				+ " mem.academy_no as academyId,CONCAT(pro.given_name,' ',pro.surname) as memberName, "
				+ " plan.plan_name as servicePlan,CONCAT(sp.given_name,' ',sp.surname) as salesman, "
				+ " (CASE SUBSTR(det.item_no,1,3) WHEN 'REN' THEN 'RENEW' WHEN 'SRV' THEN 'ENROLL' ELSE '' END) as enrollType, "
				+ " trans.payment_method_code as paymentMethod,trans.payment_media as paymentMedia,trans.payment_location_code as location, "
				+ " hd.order_status as orderStatus,trans.`status` as transStatus,trans.create_by as createBy,trans.update_by as updateBy, "
				+ " trans.update_date as updateDate,trans.audit_by as auditBy,trans.audit_date as auditDate,1 as qty,trans.paid_amount as transAmount "
				+ " FROM 		customer_order_trans trans, customer_order_hd hd, customer_order_det det,customer_profile pro, "
				+ " member mem, service_plan plan,service_plan_pos pos,staff_profile sp,customer_enrollment enroll,service_plan_offer_pos spo "
				+ " WHERE 	trans.order_no = hd.order_no and hd.order_no = det.order_no and hd.customer_id = mem.customer_id "
				+ " and pro.customer_id = mem.customer_id and pos.plan_no = plan.plan_no "
				+ " and hd.order_no = det.order_no  and sp.user_id = enroll.sales_follow_by and enroll.customer_id = hd.customer_id "
				+ " and spo.serv_pos_id = pos.serv_pos_id and (pos.pos_item_no = det.item_no or det.item_no = spo.pos_item_no) "
				+ " and plan.pass_nature_code = 'LT' and (det.item_no LIKE 'SRV%' OR det.item_no LIKE 'RENEW%')  <QueryCondition> <OrderBy>";
		
		StringBuilder queryCondition = new StringBuilder();
		List<Object> param = new ArrayList<Object>();
		String transactonDate = dto.getTransactionDate();
		if (CommUtil.notEmpty(transactonDate)) {
			if ("customize".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN ? AND ? ");
				param.add(dto.getStartDate());
				param.add(dto.getEndDate());
			}else if ("0d".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) = CURRENT_DATE ");
			}else if ("7d".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) AND CURRENT_DATE ");
			}else if ("14d".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY) AND CURRENT_DATE ");
			}else if ("1m".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN DATE(DATE_SUB(CURRENT_TIMESTAMP,INTERVAL 1 MONTH)) AND CURRENT_DATE ");
			}else if ("2m".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN DATE(DATE_SUB(CURRENT_TIMESTAMP,INTERVAL 2 MONTH)) AND CURRENT_DATE ");
			}else if ("3m".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN DATE(DATE_SUB(CURRENT_TIMESTAMP,INTERVAL 3 MONTH)) AND CURRENT_DATE ");
			}else if ("6m".equalsIgnoreCase(transactonDate)) {
				queryCondition.append(" and DATE(trans.transaction_timestamp) BETWEEN DATE(DATE_SUB(CURRENT_TIMESTAMP,INTERVAL 6 MONTH)) AND CURRENT_DATE ");
			}
		}
		
		if (CommUtil.notEmpty(dto.getOrderStatus())) {
			queryCondition.append(" and hd.order_status = ? ");
			param.add(dto.getOrderStatus());
		}
		
		if (CommUtil.notEmpty(dto.getTransStatus())) {
			queryCondition.append(" and trans.status = ? ");
			param.add(dto.getTransStatus());
		}
		
		if (CommUtil.notEmpty(dto.getEnrollType())) {
			if ("RENEW".equalsIgnoreCase(dto.getEnrollType())) {
				queryCondition.append(" and det.item_no LIKE 'RENEW%' ");
			}else {
				queryCondition.append(" and det.item_no LIKE 'SRV%' ");
			}
		}
		
		if (CommUtil.notEmpty(dto.getLocation())) {
				queryCondition.append(" and trans.payment_location_code = ? ");
				param.add(dto.getLocation());
		}
		
		if (CommUtil.notEmpty(dto.getSalesman())) {
			queryCondition.append(" and enroll.sales_follow_by = ? ");
			param.add(dto.getSalesman());
		}
		
		if (CommUtil.notEmpty(dto.getCreateBy())) {
			queryCondition.append(" and trans.create_by = ? ");
			param.add(dto.getCreateBy());
		}
		
		if (CommUtil.notEmpty(dto.getAuditBy())) {
			queryCondition.append(" and trans.audit_by = ? ");
			param.add(dto.getAuditBy());
		}
		
		sql = sql.replace("<QueryCondition>", queryCondition.toString());
		if (CommUtil.notEmpty(dto.getFileType())) {
			if ("true".equalsIgnoreCase(dto.getIsAscending())) {
				sql = sql.replace("<OrderBy>", "order by "+dto.getSortBy()+" asc");
			}else {
				sql = sql.replace("<OrderBy>", "order by "+dto.getSortBy()+" desc");
			}
		}else {
			sql = sql.replace("<OrderBy>", "");
		}
		
		if(!org.apache.commons.lang.StringUtils.isBlank(page.getCondition())){
			String condition = " and "+page.getCondition();
			sql = sql + condition;
		}
		
		String countSql = "SELECT COUNT(tt.transactionDate) from ( " + sql + " ) tt";
		if (CommUtil.notEmpty(dto.getFileType())) {
			ListPage<CustomerOrderTrans> listPage = new ListPage<CustomerOrderTrans>();
			listPage.setDtoList(getDtoBySql(sql, param, SettlementReportDto.class));
			return listPage;
		}else {
			return listBySqlDto(page, countSql, sql, param, new SettlementReportDto());	
		}
		
		
	}
	
	@SuppressWarnings("rawtypes")
	public byte[] getRenewalEnrollmentAttachment(String fileType,String sortBy,String isAscending) throws JRException {
		StringBuilder queryCondition = new StringBuilder("");
		String enrollType = "ENROLL";
		if ("RENEW".equalsIgnoreCase(enrollType)) {
			queryCondition.append(" det.item_no LIKE 'RENEW%' ");
		} else {
			queryCondition.append(" det.item_no LIKE 'SRV%' ");
		}
		
		
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String parentjasperPath = reportPath + "EnrollmentRenewal.jasper";
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("fileType", fileType);
		if(!StringUtils.isEmpty(queryCondition.toString())){
			parameters.put("SqlCondition",queryCondition.toString() );
		}
		
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if("true".equalsIgnoreCase(isAscending)){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equals(fileType)){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}
	
	public ListPage<CustomerOrderTrans> getMonthlyEnrollment(ListPage<CustomerOrderTrans> page,String year, String month) {
		String sql="SELECT DISTINCT result.* FROM (\n" +
				"SELECT\n" +
				"	DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d %T') AS transactionDate,\n" +
				"	trans.transaction_no AS transactionId,\n" +
				"	trans.order_no AS orderNo,\n" +
				"	DATE_FORMAT(hd.order_date, '%Y/%m/%d') AS orderDate,\n" +
				"	m.academy_no AS academyId,\n" +
				"	CONCAT_WS(\n" +
				"		' ',\n" +
				"		cp.given_name,\n" +
				"		cp.surname\n" +
				"	) AS patronName,\n" +
				"  plan.plan_name as planName,\n" +
				"  hd.order_total_amount as orderTotalAmount,\n" +
				"  CONCAT_WS(' ',staffProfile.given_name,staffProfile.surname) as salesMan,\n" +
				"  'Enroll' as enrollType,\n" +
				"  CASE trans.payment_method_code\n" +
				"			when 'VISA' then 'VISA'\n" +
				"      when 'MASTER' then 'MASTER'\n" +
				"      when 'CASH' then 'Cash'\n" +
				"      when 'CV' then 'Cash Value'\n" +
				"      when 'BT' then 'Bank Transfer'\n" +
				"      when 'CHEQUE' then 'Cheque'\n" +
				"      when 'cheque' then 'Cheque'\n" +
				"      when 'VAC' then 'Virtual Account'\n" +
				"      when 'UPAY' then 'Union Pay'\n" +
				"      when 'OTH' then 'N/A'\n" +
				"  END as paymentMethodCode,\n" +
				"  CASE trans.payment_media\n" +
				"			WHEN 'OTH' THEN 'N/A'\n" +
				"      WHEN 'OP' THEN 'Online Payment'\n" +
				"      WHEN 'ECR' THEN 'ECR Terminal'\n" +
				"      WHEN 'FTF' THEN 'N/A'\n" +
				"      WHEN '' THEN 'N/A'\n" +
				"      ELSE 'N/A'\n" +
				"	END as paymentMedia,\n" +
				"  trans.payment_location_code as location,\n" +
				"  trans.status as status,\n" +
				"  (select CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) from staff_profile staffProfileCreateBy where staffProfileCreateBy.user_id = trans.create_by) as createBy,\n" +
				"  (select CONCAT_WS(' ',staffProfileUpdateBy.given_name,staffProfileUpdateBy.surname) from staff_profile staffProfileUpdateBy where staffProfileUpdateBy.user_id = trans.update_by) as updateBy,\n" +
				"  DATE_FORMAT(trans.update_date,'%Y/%m/%d') as updateDate,\n" +
				"  (select CONCAT_WS(' ',staffProfileAuditBy.given_name,staffProfileAuditBy.surname) from staff_profile staffProfileAuditBy where staffProfileAuditBy.user_id = trans.audit_by) as auditBy,\n" +
				"  DATE_FORMAT(trans.audit_date,'%Y/%m/%d') as auditDate,\n" +
				"  1 as qty,\n" +
				"  trans.paid_amount as transAmount\n" +
				"FROM\n" +
				"	customer_order_trans trans,\n" +
				"	customer_order_hd hd,\n" +
				"	member m,\n" +
				"	customer_profile cp,\n" +
				"  customer_order_det det,\n" +
				"  pos_service_item_price pricePos,\n" +
				"  service_plan plan,\n" +
				"  service_plan_pos planPos,\n" +
				"  customer_enrollment enrollment\n" +
				"LEFT JOIN staff_profile staffProfile on staffProfile.user_id = enrollment.sales_follow_by\n" +
				"WHERE\n" +
				"	hd.order_no = trans.order_no\n" +
				"AND enrollment.customer_id = m.customer_id\n" +
				"AND m.customer_id = hd.customer_id\n" +
				"AND cp.customer_id = m.customer_id\n" +
				"AND det.order_no = hd.order_no\n" +
				"AND pricePos.item_no = det.item_no\n" +
				"AND planPos.pos_item_no = pricePos.item_no\n" +
				"AND plan.plan_no = planPos.plan_no\n" +
				"AND pricePos.item_catagory = 'SRV'\n" +
				"AND trans.status = 'SUC'\n" +
				"AND date_format(trans.transaction_timestamp,'%Y-%m') = date_format(?,'%Y-%m')\n" +
				"AND plan.pass_nature_code = 'LT'\n"+
				"UNION ALL\n" +
				"SELECT\n" +
				"	DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d %T') AS transactionDate,\n" +
				"	trans.transaction_no AS transactionId,\n" +
				"	trans.order_no AS orderNo,\n" +
				"	DATE_FORMAT(hd.order_date, '%Y/%m/%d') AS orderDate,\n" +
				"	m.academy_no AS academyId,\n" +
				"	CONCAT_WS(\n" +
				"		' ',\n" +
				"		cp.given_name,\n" +
				"		cp.surname\n" +
				"	) AS patronName,\n" +
				"  plan.plan_name as planName,\n" +
				"  hd.order_total_amount as orderTotalAmount,\n" +
				"  CONCAT_WS(' ',staffProfile.given_name,staffProfile.surname) as salesMan,\n" +
				"  'Renew' as enrollType,\n" +
				"  CASE trans.payment_method_code\n" +
				"			when 'VISA' then 'VISA'\n" +
				"      when 'MASTER' then 'MASTER'\n" +
				"      when 'CASH' then 'Cash'\n" +
				"      when 'CV' then 'Cash Value'\n" +
				"      when 'BT' then 'Bank Transfer'\n" +
				"      when 'CHEQUE' then 'Cheque'\n" +
				"      when 'cheque' then 'Cheque'\n" +
				"      when 'VAC' then 'Virtual Account'\n" +
				"      when 'UPAY' then 'Union Pay'\n" +
				"      when 'OTH' then 'N/A'\n" +
				"  END as paymentMethodCode,\n" +
				"  CASE trans.payment_media\n" +
				"			WHEN 'OTH' THEN 'N/A'\n" +
				"      WHEN 'OP' THEN 'Online Payment'\n" +
				"      WHEN 'ECR' THEN 'ECR Terminal'\n" +
				"      WHEN 'FTF' THEN 'N/A'\n" +
				"      WHEN '' THEN 'N/A'\n" +
				"      ELSE 'N/A'\n" +
				"	END as paymentMedia,\n" +
				"  trans.payment_location_code as location,\n" +
				"  trans.status as status,\n" +
				"  (select CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) from staff_profile staffProfileCreateBy where staffProfileCreateBy.user_id = trans.create_by) as createBy,\n" +
				"  (select CONCAT_WS(' ',staffProfileUpdateBy.given_name,staffProfileUpdateBy.surname) from staff_profile staffProfileUpdateBy where staffProfileUpdateBy.user_id = trans.update_by) as updateBy,\n" +
				"  DATE_FORMAT(trans.update_date,'%Y/%m/%d') as updateDate,\n" +
				"  (select CONCAT_WS(' ',staffProfileAuditBy.given_name,staffProfileAuditBy.surname) from staff_profile staffProfileAuditBy where staffProfileAuditBy.user_id = trans.audit_by) as auditBy,\n" +
				"  DATE_FORMAT(trans.audit_date,'%Y/%m/%d') as auditDate,\n" +
				"  1 as qty,\n" +
				"  trans.paid_amount as transAmount\n" +
				"FROM\n" +
				"	customer_order_trans trans,\n" +
				"	customer_order_hd hd,\n" +
				"	member m,\n" +
				"	customer_profile cp,\n" +
				"  customer_order_det det,\n" +
				"  pos_service_item_price pricePos,\n" +
				"  service_plan plan,\n" +
				"  service_plan_pos planPos,\n" +
				"  service_plan_offer_pos offerPos,\n" +
				"  customer_enrollment enrollment\n" +
				"LEFT JOIN staff_profile staffProfile on staffProfile.user_id = enrollment.sales_follow_by\n" +
				"WHERE\n" +
				"	hd.order_no = trans.order_no\n" +
				"AND enrollment.customer_id = m.customer_id\n" +
				"AND m.customer_id = hd.customer_id\n" +
				"AND cp.customer_id = m.customer_id\n" +
				"AND det.order_no = hd.order_no\n" +
				"AND pricePos.item_no = det.item_no\n" +
				"AND offerPos.pos_item_no = pricePos.item_no\n" +
				"AND offerPos.serv_pos_id = planPos.serv_pos_id\n" +
				"AND plan.plan_no = planPos.plan_no\n" +
				"AND pricePos.item_catagory = 'SRV'\n" +
				"AND trans.status = 'SUC'\n" +
				"AND date_format(trans.transaction_timestamp,'%Y-%m') = date_format(?,'%Y-%m')\n" +
				"AND plan.pass_nature_code = 'LT'\n"+
				") result\n";
		List<Serializable> param = new ArrayList<Serializable>();
		Date selectedDate = CommUtil.formatTheGivenYearAndMonth(year,month);
		param.add(selectedDate);
		param.add(selectedDate);
		String countSql = " SELECT count(x.transactionDate) from ( " +sql+") x";
		return listBySqlDto(page, countSql, sql, param, new MonthlyEnrollmentDto());
	}
	
	@SuppressWarnings("rawtypes")
	public byte[] getMonthlyEnrollmentAttachment(String year, String month, String fileType,String sortBy,String isAscending) throws JRException {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String parentjasperPath = reportPath + "MonthlyEnrollment.jasper";
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("selectedMonth", year + "-" + String.format("%0" + 2 + "d", Integer.valueOf(month)) + "-01");
		parameters.put("fileType", fileType);
		
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if("true".equalsIgnoreCase(isAscending)){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equals(fileType)){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JRAbstractExporter exportedByFileType(String fileType, JasperPrint jasperPrint, ByteArrayOutputStream outPut) {
		JRAbstractExporter exporter = null;
		switch (fileType) {
		case "pdf":
			exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
			break;
		case "csv":
			exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
			break;
		default:
		}

		return exporter;
	}
	
	public ListPage<CustomerOrderTrans> getCorporateMembershipSettlement(ListPage<CustomerOrderTrans> page, String status,Long filterByCustomerId){
		List<Serializable> listParam = new ArrayList<Serializable>();
		String sql = new String("SELECT\n" +
				"	m.academy_no as academyNo,\n" +
				"  CONCAT_WS(' ',cp.salutation,cp.given_name, cp.surname) as memberName,\n" +
				"  enrollment.enroll_date as enrollDateDB,\n" +
				"  enrollment.status as status,\n" +
				"  (SELECT \n" +
				"			 CONCAT_WS(' ',staffProfileFollowBy.given_name,staffProfileFollowBy.surname) \n" +
				"   FROM staff_profile staffProfileFollowBy \n" +
				"   WHERE staffProfileFollowBy.user_id = enrollment.sales_follow_by\n" +
				"  ) as salesFollowBy,\n" +
				"  \n" +
				"	orderDet.item_total_amout - \n" +
				"  (SELECT\n" +
				"       if(sum(trans.paid_amount)is null,0,sum(trans.paid_amount))\n" +
				"   FROM\n" +
				"	     customer_order_det orderDet,\n" +
				"	     customer_order_trans trans\n" +
				"   WHERE\n" +
				"	     orderDet.order_no = trans.order_no\n" +
				"   AND trans.order_no = orderHd.order_no\n" +
				"   AND trans.status = 'SUC'\n" +
				"  ) AS balanceDue,\n" +
				"  (SELECT\n" +
				"			count(1) \n" +
				"   FROM\n" +
				"	    customer_order_det orderDet,\n" +
				"	    customer_order_trans trans\n" +
				"   WHERE\n" +
				"	 (\n" +
				"		 trans.`status` = 'FAIL'\n" +
				"		 OR trans.`status` = 'VOID'\n" +
				"	 )\n" +
				"   AND orderDet.order_no = trans.order_no\n" +
				"   AND trans.order_no = orderHd.order_no\n" +
				"   ) AS failTransNo,\n" +
				"   IFNULL(serviceAcc.period_code,sp.pass_period_type) as passPeriodType,\n" +
				"   m.effective_date as effectiveDateDB,\n" +
				"   corporateProfile.company_name as companyName,\n" +
				"   enrollment.enroll_id as enrollId,\n" +
				"   orderHd.order_no as orderNo,\n"+
				"   cp.customer_id as customerId\n"+
				"FROM\n" +
				"	customer_order_det orderDet,\n" +
				"  customer_order_hd orderHd,\n" +
				"	pos_service_item_price psip,\n" +
				"	service_plan_pos spp,\n" +
				"  customer_profile cp,\n" +
				"  customer_enrollment enrollment,\n" +
				"  customer_enroll_po enrollPo,\n" +
				"  service_plan sp,\n" +
				"  corporate_member corporateMember,\n" +
				"  corporate_profile corporateProfile,\n" +
				"  member m\n" +
				"LEFT JOIN customer_service_acc serviceAcc ON serviceAcc.customer_id = m.customer_id \n" +
				"WHERE\n" +
				"   spp.plan_no = sp.plan_no\n" +
				"AND orderDet.order_no = orderHd.order_no\n" +
				"AND	orderDet.item_no = psip.item_no\n" +
				"AND psip.item_no = spp.pos_item_no\n" +
				"AND psip.item_no LIKE \"SRV%\"\n" +
				"AND cp.customer_id = orderHd.customer_id\n" +
				"AND m.customer_id = cp.customer_id\n" +
				"AND enrollment.customer_id = cp.customer_id\n" +
				"AND enrollPo.order_det_id = orderDet.order_det_id\n" +
				"AND enrollPo.enroll_id = enrollment.enroll_id\n" +
				"AND m.member_type = 'CPM'\n" +
				"AND corporateMember.customer_id = m.customer_id\n" +
				"AND corporateProfile.corporate_id = corporateMember.corporate_id\n");
		
		
		if(!status.equalsIgnoreCase("ALL")){
			sql = sql + "AND enrollment.status = ?\n";
			listParam.add(status);
		}
		
		if(filterByCustomerId != null){
			sql = sql + "AND m.customer_id = ?\n";
			listParam.add(filterByCustomerId);
		}
		
		if(!StringUtils.isEmpty(page.getCondition())){
			sql = "SELECT * FROM ( "+sql + ") result\n";
			String condition = " WHERE "+page.getCondition();
			sql = sql + (condition);
		}
		
		String countHql = " SELECT count(1) FROM ( " +sql + " )  countSQL";
		
		return listBySqlDto(page, countHql, sql, listParam, new CustomerEnrollmentDto());
	}
	
	@Override
	public ListPage<CustomerOrderTrans> getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage page,
			String selectedDate, String facilityType) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d') AS transDate, ")
				.append(" trans.transaction_no AS transId, ")
				.append(" DATE_FORMAT(b.begin_datetime_book, '%Y/%m/%d') AS resvDate, ")
				.append(" b.resv_id AS resvId, ")
				.append(" m.academy_no AS academyId, ")
				.append(" CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) AS patronName, ")
				.append(" b.exp_coach_user_id AS coachId, ")
				.append(" CONCAT(sp.given_name, ' ', sp.surname) AS coachName, ")
				.append(" CASE b.resv_facility_type WHEN 'GOLF' THEN 'Golf' WHEN 'TENNIS' THEN 'Tennis' ELSE b.resv_facility_type END AS facilityType, ")
				.append(" CASE trans.payment_method_code WHEN 'CV' THEN 'Cash Value' WHEN 'CASH' THEN 'Cash' WHEN 'BT' THEN 'Bank Transfer' ELSE trans.payment_method_code END AS paymentMethod, ")
				.append(" CASE trans.payment_media WHEN 'OP' THEN 'Online Payment' WHEN 'ECR' THEN 'ECR Terminal' WHEN 'OTH' THEN 'Other' WHEN NULL THEN 'N/A' WHEN '' THEN 'N/A' ELSE trans.payment_media END AS paymentMedia, ")
				.append(" CASE trans.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Web Portal' ELSE trans.payment_location_code END AS location, ") 
				.append(" CASE trans.status WHEN 'SUC' THEN 'Complete' WHEN 'RFU' THEN 'Refund' WHEN 'REJ' THEN 'Refund' ELSE trans.status END AS status, ")
				.append(" b.facility_type_qty AS qty, ")
				.append(" trans.paid_amount AS transAmount ")
				.append("FROM ")
				.append(" member_facility_type_booking b ")
				.append(" LEFT JOIN member_facility_book_addition_attr a ON b.resv_id = a.resv_id ")
				.append(" LEFT JOIN member m ON b.customer_id = m.customer_id")
				.append(" LEFT JOIN customer_profile cp ON b.customer_id = cp.customer_id ")
				.append(" LEFT JOIN staff_profile sp ON b.exp_coach_user_id = sp.user_id ")
				.append(" LEFT JOIN customer_order_hd hd ON hd.order_no = b.order_no ")
				.append(" LEFT JOIN customer_order_trans trans ON hd.order_no = trans.order_no ")
				.append("WHERE ")
				.append(" b.exp_coach_user_id IS NOT NULL ")
				.append(" AND b.status IN ('RSV' , 'ATN', 'NAT', 'CAN') ")
				.append(" AND trans.status IN ('SUC', 'REJ', 'RFU') ");

		List<Serializable> param = new ArrayList<Serializable>();
		if ("GOLF".equalsIgnoreCase(facilityType) || "TENNIS".equalsIgnoreCase(facilityType)) {
			sql.append(" AND b.resv_facility_type = ? ");
			param.add(facilityType);
		}
		/* Daily Facility Usage Report */
		if ("DAILY".equalsIgnoreCase(timePeriodType)) {
			sql.append(" AND DATE_FORMAT(trans.transaction_timestamp, '%Y-%m-%d') = date_format(?, '%Y-%m-%d') ");
			param.add(selectedDate);
		} else {
			/* Monthly Facility Usage Report */
			sql.append(" AND DATE_FORMAT(trans.transaction_timestamp, '%Y-%m') = date_format(?, '%Y-%m') ");
			param.add(selectedDate);
		}
		String countSql = " SELECT count(x.transId) from ( " + sql + " ) x";
		return listBySqlDto(page, countSql, sql.toString(), param, new MonthlyPrivateCoachingRevenueDto());
	}

	// TODO
	@Override
	public byte[] getDailyMonthlyPrivateCoachingRevenueAttachment(String timePeriodType, String selectedDate,
			String fileType, String sortBy, String isAscending, String facilityType) throws Exception {

		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String parentjasperPath = reportPath + "MonthlyPrivateCoachingRevenue.jasper";
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("resvDate", selectedDate);
		parameters.put("facilityType", facilityType);
		parameters.put("TimePeriodType", timePeriodType);
		parameters.put("fileType", fileType);
		// Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if ("true".equalsIgnoreCase(isAscending)) {
			sortField.setOrder(SortOrderEnum.ASCENDING);
		} else {
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		// Ignore pagination for csv
		if ("csv".equals(fileType)) {
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}

		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}

	@Override
	public Date getLastestSpaSyncTime() {
	    
	    String hql = "select max(transactionTimestamp) from CustomerOrderTrans where createBy = 'spa_sync'";
	    Date date = (Date) getUniqueByHql(hql);
	    return date;
	}
	
}
