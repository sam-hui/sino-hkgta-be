package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdditionalInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;

public interface CustomerAdditionInfoDao extends IBaseDao<CustomerAdditionInfo> {
	
	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void saveCustomerAdditionInfo(CustomerAdditionInfo t);

	public void updateCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public List<CustomerAdditionInfo> getCustomerAdditionInfoListByCustomerID(Long customerID);
	
	public CustomerAdditionInfo getByCustomerIdAndCaptionId(Long customerId,Long captionId);
	
	public List<AdditionalInfoDto> getByCustomerIdAndCategory(Long customerId,String category) throws HibernateException;
	
	public int deleteByCustomerId(Long customerId) throws HibernateException;
	
	public List<CustomerAdditionInfoDto> getFavorByCustomerId(Long customerId);
	
	public List<CustomerAdditionInfoDto> getByCustomerIdAndCategoryForTablet(Long customerId,String category);
}
