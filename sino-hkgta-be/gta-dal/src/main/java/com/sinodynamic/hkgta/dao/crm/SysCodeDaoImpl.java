package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository
public class SysCodeDaoImpl extends GenericDao<SysCode> implements SysCodeDao {

	@Override
	public List<SysCode> selectSysCodeByCategory(String category){
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(category);
		List<SysCode> result = getByHql("select new SysCode(codeValue, codeDisplay,codeDisplayNls) from SysCode where category = ? order by displayOrder", param);
		
		return result;
	}
	
	@Override
	public List<SysCode> getSysCodeByCategory(String category){
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(category);
		List<SysCode> result = getByHql("from SysCode where category = ? order by displayOrder", param);
		
		return result;
	}
	
	public List<SysCode> selectSysCodeByCategoryWithoutEmptyCodeValue(String category){
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(category);
		List<SysCode> result = getByHql("select new SysCode(codeValue, codeDisplay,codeDisplayNls) from SysCode where category = ? and codeValue !='' and codeValue is not null order by displayOrder", param);
		
		return result;
	}

	@Override
	public List<SysCode> selectAllSysCodeCategory() throws Exception {
		List<Serializable> param = new ArrayList<Serializable>();
		
		List<SysCode> result = getByHql("select distinct new SysCode(category) from SysCode  order by displayOrder", param);
		
		return result;
	}
	
	public SysCode getByCategoryAndCodeValue(String category,String codeValue){
		String hqlstr = " from SysCode s where s.category = ? and s.codeValue = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(category);
		param.add(codeValue);
		return (SysCode) getUniqueByHql(hqlstr, param);
	}

	@Override
	public List<SysCode> getSysCodeByMutiCategory(String category)
	{
		List<SysCode> result = getByHql("select new SysCode(codeValue, codeDisplay,codeDisplayNls) from SysCode where category in ( " + category + " ) order by codeDisplay", null);
		return result;
	}

}
