package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.RemarksDto;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.PositionTitle;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class RemarksDaoImpl extends GenericDao<CustomerPreEnrollStage>
		implements RemarksDao {

	public void addRemark(CustomerPreEnrollStage remarkInfo) {

		save(remarkInfo);

	}

	public List<CustomerPreEnrollStage> getRemarkList(Long customerId){
		String hqlstr = "from CustomerPreEnrollStage where customerId = "+customerId;
		return getByHql(hqlstr);
	}
	
	public ListPage<CustomerPreEnrollStage> getRemarkList(ListPage<CustomerPreEnrollStage> page,Long customerId){
		String hqlstr = "from CustomerPreEnrollStage c where c.customerId = ?";
		String countHql = "select count(*) from CustomerPreEnrollStage where customerId = ?";
		List<Long> param = new ArrayList<Long>();
		param.add(customerId);
		return (ListPage<CustomerPreEnrollStage>) listByHql(page, countHql, hqlstr, param);

	}
	
	public int countUnreadRemark(Long customerId,String loginUserId){
		String hqlStaffMaster = " from StaffMaster s where s.userId = ? ";
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(loginUserId);
		StaffMaster staffMaster = (StaffMaster) getUniqueByHql(hqlStaffMaster, param1);
		
		StringBuilder hql = new StringBuilder(" from CustomerPreEnrollStage cpes, StaffMaster sm "
				+ "where cpes.commentStatus = 'NR' and cpes.customerId =? and cpes.commentBy != ? and sm.userId = cpes.commentBy and (sm.positionTitle = ? or sm.positionTitle = ?) ");
		List<Serializable> param2 = new ArrayList<Serializable>();
		param2.add(customerId);
		param2.add(loginUserId);
		if(staffMaster != null){
			if(Constant.ROLE_SALES.equalsIgnoreCase(staffMaster.getPositionTitle())||PositionTitle.SP.name().equalsIgnoreCase(staffMaster.getPositionTitle())){
				hql.append("and cpes.salesUserId = ? ");
				param2.add(Constant.ROLE_OFFICER);
				param2.add(PositionTitle.SO.name());
				param2.add(loginUserId);
				return getByHql(hql.toString(), param2).size();
			}
			if (Constant.ROLE_OFFICER.equalsIgnoreCase(staffMaster.getPositionTitle())||PositionTitle.SO.name().equalsIgnoreCase(staffMaster.getPositionTitle())){
				param2.add(Constant.ROLE_SALES);
				param2.add(PositionTitle.SP.name());
				return getByHql(hql.toString(), param2).size();
			}
		}
		return 0;
//		String hqlstr = "from CustomerPreEnrollStage c where c.commentStatus = 'NR' and c.customerId = ? and c.commentBy !=? ";
//		List<Serializable> param = new ArrayList<Serializable>();
//		param.add(customerId);
//		param.add(loginUserId);
		
	}
	
	public Integer countUnreadRemarkByDevice(Long customerId,String loginUserId,String loginDevice){
		String sql = null;
		if("PC".equalsIgnoreCase(loginDevice)){
			sql = "SELECT\n" +
					"	count(*)\n" +
					"FROM\n" +
					"	customer_pre_enroll_stage stage\n" +
					"WHERE\n" +
					"	stage.customer_id = ?\n" +
					"AND stage.comment_status = 'NR'\n" +
					"AND stage.comment_by_device != 'PC'\n" +
					"AND stage.comment_by != ?";
		}else{
			sql = "SELECT\n" +
					"	count(*)\n" +
					"FROM\n" +
					"	customer_pre_enroll_stage stage\n" +
					"WHERE\n" +
					"	stage.customer_id = ?\n" +
					"AND stage.comment_status = 'NR'\n" +
					"AND stage.comment_by_device = 'PC'\n" +
					"AND stage.sales_user_id = ?";
		}
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(loginUserId);
		Object result = getUniqueBySQL(sql, param);
		Integer countValue = 0;
		if (result != null) {
			countValue = ((Number) result).intValue();
		}
		return countValue;
	}
	
	public ListPage<CustomerPreEnrollStage> getRemarkListByDto(ListPage<CustomerPreEnrollStage> page,Long customerId){
		String sql = "SELECT\n" +
				"	(\n" +
				"		SELECT\n" +
				"			CONCAT_WS(\n" +
				"				' ',\n" +
				"				staffProfileCommentBy.given_name,\n" +
				"				staffProfileCommentBy.surname\n" +
				"			)\n" +
				"		FROM\n" +
				"			staff_profile staffProfileCommentBy\n" +
				"		WHERE\n" +
				"			staffProfileCommentBy.user_id = stage.comment_by\n" +
				"	) AS commentBy,\n" +
				"	stage.comment_date AS commentDate,\n" +
				"	stage.comments AS contentRemarks,\n" +
				"	stage.customer_id AS customerId,\n" +
				"	stage.comment_status AS commentStatus\n" +
				"FROM\n" +
				"	customer_pre_enroll_stage stage\n" +
				"WHERE\n" +
				"	stage.customer_id = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		String countSql = " SELECT count(1) FROM ( " + sql + " ) countSQL";
		return listBySqlDto(page, countSql, sql, param, new RemarksDto());
	}
	
	public Boolean readRemarkByDevice(Long customerId,String loginUserId,String loginDevice){
		String sql = null;
		if("PC".equalsIgnoreCase(loginDevice)){
			sql = "UPDATE customer_pre_enroll_stage stage\n" +
					"SET stage.comment_status = NULL\n" +
					"WHERE\n" +
					"	stage.customer_id = ?\n" +
					"AND stage.comment_status = 'NR'\n" +
					"AND stage.comment_by_device != 'PC'\n" +
					"AND stage.comment_by != ?";
		}else{
			sql = "UPDATE customer_pre_enroll_stage stage\n" +
					"SET stage.comment_status = NULL\n" +
					"WHERE\n" +
					"	stage.customer_id = ?\n" +
					"AND stage.comment_status = 'NR'\n" +
					"AND stage.comment_by_device = 'PC'\n" +
					"AND stage.sales_user_id = ?";
		}
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(loginUserId);

		if (sqlUpdate(sql, param) > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public int countUnreadPayment(Long orderNo){
		StringBuilder hql = new StringBuilder(" select count(t.transactionNo) from CustomerOrderTrans t "
				+ "where t.readBy is null and t.paymentMethodCode != ? and (t.status = ? or t.status = ?) and t.customerOrderHd.orderNo = ? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(Constant.CREDIT_CARD);
		param.add(Constant.Status.FAIL.name());
		param.add(Constant.Status.VOID.name());
		param.add(orderNo);
		try {
			Long count = (Long)getUniqueByHql(hql.toString(), param);
			return count.intValue();
		} catch (HibernateException e) {
			e.printStackTrace();
			return 0;
		}
		
	}
	
	public Serializable saveOrUpdateRemark(CustomerPreEnrollStage cPreEnrollStage){
		return saveOrUpdate(cPreEnrollStage);
	}
}
