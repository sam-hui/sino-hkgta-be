package com.sinodynamic.hkgta.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;
/**
 * 
 * @author Angus_Zhu
 *
 */
@Repository("commonBySQLDao")
public class CommonDataQueryBySQLDaoImpl extends AbstractCommonDataQueryDao implements CommonDataQueryBySQLDao {

	private Logger logger = Logger.getLogger(CommonDataQueryBySQLDaoImpl.class);
	
	@Override
	public void generateSearchStr(SearchRuleDto searchRule, StringBuilder sb) {
		String searchField = searchRule.getField();
		String searchOper = searchRule.getOp();
		String searchString = searchRule.getData();
				if (StringUtils.isNotEmpty(searchField) && StringUtils.isNotEmpty(searchOper)) {
					if (EQUAL.equals(searchOper)) {
						sb.append(LEFT_BRACKET).append(searchField).append(" = ").append(SINGLE_QUOTES).append(searchString).append(SINGLE_QUOTES).append(SPACE).append(RIGHT_BRACKET);
					} else if (NOT_EQUAL.equals(searchOper)) {
						sb.append(LEFT_BRACKET).append(searchField).append(" != ").append(SINGLE_QUOTES).append(searchString).append(SINGLE_QUOTES).append(SPACE).append(RIGHT_BRACKET);
					} else if (LESS_THAN.equals(searchOper)) {
						sb.append(LEFT_BRACKET).append(searchField).append("< ").append(SINGLE_QUOTES).append(searchString).append(SINGLE_QUOTES).append(SPACE).append(RIGHT_BRACKET);
					} else if (LESS_EQUAL.equals(searchOper)) {
						sb.append(LEFT_BRACKET).append(searchField).append("<= ").append(SINGLE_QUOTES).append(searchString).append(SINGLE_QUOTES).append(SPACE).append(RIGHT_BRACKET);
					} else if (GREATER_THAN.equals(searchOper)) {
						sb.append(LEFT_BRACKET).append(searchField).append(" > ").append(SINGLE_QUOTES).append(searchString).append(SINGLE_QUOTES).append(SPACE).append(RIGHT_BRACKET);
					} else if (GREATER_EQUAL.equals(searchOper)) {
						sb.append(LEFT_BRACKET).append(searchField).append(" >= ").append(SINGLE_QUOTES).append(searchString).append(SINGLE_QUOTES).append(SPACE).append(RIGHT_BRACKET);
					}  else if (IS_NULL.equals(searchOper)) {// is null
						sb.append(LEFT_BRACKET).append(searchField).append(" is null").append(SPACE).append(RIGHT_BRACKET);
					} else if (IS_NOT_NULL.equals(searchOper)) {// is not null
						sb.append(LEFT_BRACKET).append(searchField).append("  is not null ").append(SPACE).append(RIGHT_BRACKET);
					} else if (IN.equals(searchOper)) {// is in
						sb.append(LEFT_BRACKET).append(searchField).append(" in  (").append(getValueOfInConditions(searchString)).append(")  )");
					} else if (NOT_IN.equals(searchOper)) {// is not in
						sb.append(LEFT_BRACKET).append(searchField).append(" not in (").append(getValueOfInConditions(searchString)).append(")  )");
					}else if (BEGIN_WITH.equals(searchOper)) {// begins with
						sb.append(LEFT_BRACKET).append(searchField).append("  like '").append(searchString).append("%' )");
					} else if (NOT_BEGIN_WITH.equals(searchOper)) {// does not begin with
						sb.append(LEFT_BRACKET).append(searchField).append(" not like  '").append(searchString).append("%'  )");
					} else if (END_WITH.equals(searchOper)) {// end with
						sb.append(LEFT_BRACKET).append(searchField).append(" like  '%").append(searchString).append("' )");
					} else if (NOT_END_WITH.equals(searchOper)) {// does not end with
						sb.append(LEFT_BRACKET).append(searchField).append(" not like '%").append(searchString).append("' )");
					} else if (CONTAINS.equals(searchOper)) {// contains
						sb.append(LEFT_BRACKET).append(searchField).append(" like  '%").append(searchString).append("%'  )");
					}
				}		
	}



	@Override
	public void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class dtoClass) {
			
			String searchCondition = getSearchCondition(queryDto, joinSQL);
			if(logger.isDebugEnabled()){
				logger.debug("searchCondition:" + searchCondition);
			}
			StringBuilder sb = new StringBuilder(searchCondition);
			addOrderByStr(sb, queryDto);
			setPageAllSize(page,sb.toString());
			logger.debug("query sql:" + sb.toString());
			Query query = getCurrentSession().createSQLQuery(sb.toString()).setResultTransformer(Transformers.aliasToBean(dtoClass));
			query.setMaxResults(page.getSize());
			query.setFirstResult(page.getStart());
			page.setList(query.list());
	}



	@Override
	public void getAdvanceQuerySpecificResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class dtoClass) {
		String searchCondition = getSearchCondition(queryDto, joinSQL);
		StringBuilder sb = new StringBuilder(searchCondition);
		addOrderByStr(sb, queryDto);
		setPageAllSize(page,sb.toString());
		logger.debug(sb.toString());
		Query query = getCurrentSession().createSQLQuery(sb.toString()).setResultTransformer(Transformers.aliasToBean(dtoClass));
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}

	private void setPageAllSize(ListPage page, String searchCondition) {
		int indexOf = searchCondition.toUpperCase().indexOf("FROM");
		String substring = searchCondition.substring(indexOf);
		String queryCountStr="select count(*) allsize From ("+searchCondition+") s";
		logger.debug("the count query is :"+queryCountStr);
		BigInteger object = (BigInteger) getCurrentSession().createSQLQuery(queryCountStr).uniqueResult();
		page.setAllSize(object.intValue());
		this.initFirstPage(page);
	}


	private void setPageAllSize(ListPage page, String searchCondition, List<Object> paramList) {
		int indexOf = searchCondition.toUpperCase().indexOf("FROM");
		String substring = searchCondition.substring(indexOf);
		String queryCountStr="select count(*) allsize From ("+searchCondition+") s";
		logger.debug("the count query is :"+queryCountStr);
		Query query = getCurrentSession().createSQLQuery(queryCountStr);
		if (paramList != null && paramList.size() > 0) {
			  for (int i=0; i < paramList.size(); i++ )
				  query.setParameter(i, paramList.get(i));
		}
		BigInteger object = (BigInteger) query.uniqueResult();
		page.setAllSize(object.intValue());
		this.initFirstPage(page);
	}


	@Override
	public void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto) {
		StringBuilder sb = new StringBuilder();
		sb.append(sql);
		addOrderByStr(sb, queryDto);
		Query query = getCurrentSession().createSQLQuery(sb.toString()).setResultTransformer(Transformers.aliasToBean(dto));
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}



	@Override
	public void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class clazz, List<Object> paramList)
	{
	
		StringBuilder sb = new StringBuilder();
		String searchCondition = getSearchCondition(queryDto, joinSQL);
		sb.append(searchCondition);
		addOrderByStr(sb, queryDto);
		setPageAllSize(page,searchCondition, paramList);
		logger.debug(searchCondition);
		Query query = getCurrentSession().createSQLQuery(searchCondition);
		query.setResultTransformer(Transformers.aliasToBean(clazz));
		if (paramList != null && paramList.size() > 0) {
			  for (int i=0; i < paramList.size(); i++ )
				  query.setParameter(i, paramList.get(i));
		}
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}



	@Override
	public void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto, List<Object> paramList)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(sql);
		addOrderByStr(sb, queryDto);
		Query query = getCurrentSession().createSQLQuery(sb.toString());
		query.setResultTransformer(Transformers.aliasToBean(dto));
		if (paramList != null && paramList.size() > 0) {
			  for (int i=0; i < paramList.size(); i++ )
				  query.setParameter(i, paramList.get(i));
		}
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}



	@Override
	public void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap)
	{
		StringBuilder sb = new StringBuilder();
		String searchCondition = getSearchCondition(queryDto, joinSQL);
		sb.append(searchCondition);
		addOrderByStr(sb, queryDto);
		searchCondition=sb.toString();
		setPageAllSize(page,searchCondition);
		logger.debug(searchCondition);
		Query query = getCurrentSession().createSQLQuery(searchCondition).setResultTransformer(Transformers.aliasToBean(dto));
		for(Map.Entry<String, org.hibernate.type.Type> entry : typeMap.entrySet()){
			if(StringUtils.isNotEmpty(entry.getKey()) && null != entry.getValue()){
				((SQLQuery) query).addScalar(entry.getKey(), entry.getValue());
			}
		}
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
		
	}



	@Override
	public void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(joinSQL);
		addOrderByStr(sb, queryDto);
		Query query = getCurrentSession().createSQLQuery(sb.toString()).setResultTransformer(Transformers.aliasToBean(dto));
		for(Map.Entry<String, org.hibernate.type.Type> entry : typeMap.entrySet()){
			if(StringUtils.isNotEmpty(entry.getKey()) && null != entry.getValue()){
				((SQLQuery) query).addScalar(entry.getKey(), entry.getValue());
			}
		}
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}




	
}
