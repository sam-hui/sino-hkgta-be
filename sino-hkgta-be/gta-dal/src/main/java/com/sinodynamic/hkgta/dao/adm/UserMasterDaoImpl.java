package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class UserMasterDaoImpl extends GenericDao<UserMaster> implements UserMasterDao{
	Logger logger = Logger.getLogger(UserMasterDao.class);
	
	
	public UserMaster getUserByLoginId(String loginId) {
		
		String hqlstr = "from UserMaster where loginId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(loginId);
		return (UserMaster)getUniqueByHql(hqlstr, param);
	}
	
	public UserMaster getUserByUserId(String userId){
		
		String hqlstr = "from UserMaster where userId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		return (UserMaster)getUniqueByHql(hqlstr, param);
	}

	public void addUserMaster(UserMaster m){
		this.save(m);
		
	}

	public void getUserMasterList(ListPage<UserMaster> page, String countHql,
			String hql, List<?> param) throws Exception {
		this.listByHql(page, countHql, hql, param);
		
	}

	public void updateUserMaster(UserMaster m) throws Exception {
		this.update(m);
		
	}

	public void deleteUserMaster(UserMaster m) throws Exception {
		this.delete(m);
		
	}

	@Override
	public String saveUserMaster(UserMaster m) throws HibernateException {
		// TODO Auto-generated method stub
		return (String)super.save(m);
	}
	@Override
	public StaffDto getStaffDto(String userId){
		StringBuilder sql = new StringBuilder(" select um.user_id as userId, um.nickname as nickname, um.user_type as userType,  concat(sf.given_name, ' ', sf.surname) staffName , sm.staff_type as staffType ")
			.append(" from user_master um  left join staff_master sm on um.user_id=sm.user_id ")
			.append("  left join staff_profile sf on um.user_id=sf.user_id ")
			.append(" where um.user_id=? ");
		List<Serializable>	param = new ArrayList<Serializable>();
		param.add(userId);
		List<StaffDto> list = this.getDtoBySql(sql.toString(), param, StaffDto.class);
		if(list !=null && list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * Method to check if the typed loginId exists or not for update
	 * (The current userId is ignored)
	 * @param loginId
	 * @param userId
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableLoginId(String loginId, String userId){
		String sqlstr = " select c.user_id as userId from user_master c where c.login_id = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(loginId);
		List<UserMaster> userMasterList = getDtoBySql(sqlstr, listParam, UserMaster.class);
		if (userMasterList != null){
			if (userMasterList.size() == 0) {
				return true;
			} else if (userMasterList.size() > 0) {
				UserMaster userMaster = userMasterList.get(0);
				if (userMaster != null) {// This user email exists
					if (userMaster.getUserId().equals(userId)) {
						return true;// update itself
					}
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public ListPage<UserMaster> getUserMasterList(String orderColumn, String order,
			int pageSize, int currentPage, String filters) {
		ListPage<UserMaster> pageParam = new ListPage<UserMaster>(pageSize, currentPage);
		
		
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		
		pageParam.addDescending("updateDate");
		
		String hql = "from UserMaster ";
		if(!StringUtils.isEmpty(filters)){
			hql = hql + "where " + filters;
		}
		
		String count = "select count(*) " +hql;
		return this.listByHql(
				pageParam,
				count,
				hql,
				null);
	}
	
}
