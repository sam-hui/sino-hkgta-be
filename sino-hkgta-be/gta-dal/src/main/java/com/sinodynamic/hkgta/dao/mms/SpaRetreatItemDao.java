package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaRetreatItem;

public interface SpaRetreatItemDao extends IBaseDao<SpaRetreatItem>{

	public Serializable addSpaRetreatItem(SpaRetreatItem spaRetreatItem);
	public SpaRetreatItem getByItemId(Long itemId);
}
