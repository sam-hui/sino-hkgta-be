package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;

public interface CustomerEmailAttachDao extends IBaseDao<CustomerEmailAttach> {
	
	public void saveCustomerEmailAttach(CustomerEmailAttach cea);
}
