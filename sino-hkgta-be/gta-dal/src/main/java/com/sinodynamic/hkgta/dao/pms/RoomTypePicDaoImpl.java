package com.sinodynamic.hkgta.dao.pms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.pms.RoomPicPathDto;
import com.sinodynamic.hkgta.entity.pms.RoomPicPath;
@Repository
public class RoomTypePicDaoImpl extends GenericDao<RoomPicPath> implements RoomTypePicDao {

	@Override
	public void saveRoomPic(List<RoomPicPathDto> pics)
	{
		if (null == pics) return;
		for(RoomPicPathDto p : pics)
		{
			RoomPicPath pic = new RoomPicPath();
			pic.setRoomTypeCode(p.getRoomTypeCode());
			pic.setServerFilename(p.getServerFilename());
			pic.setDisplayOrder(p.getDisplayOrder());
			pic.setPicType(p.getPicType());
			
			super.save(pic);
		}
		
	}

	@Override
	public void updateRoomPic(List<RoomPicPathDto> pics)
	{
		StringBuffer del = new StringBuffer();
		for(RoomPicPathDto pic : pics)
		{
			if ("DEL".equalsIgnoreCase(pic.getAction()))
			{
				del.append(",").append(pic.getId());
				continue;
			}
			
			if ("NEW".equalsIgnoreCase(pic.getAction()))
			{
				List<RoomPicPathDto> newPic = new ArrayList<RoomPicPathDto>();
				newPic.add(pic);
				saveRoomPic(newPic);
				continue;
			}
			
			RoomPicPath originPic = super.get(RoomPicPath.class, pic.getId());
			if (null == originPic) continue; 
			originPic.setDisplayOrder(pic.getDisplayOrder());
			
		}
		
		if (StringUtils.isEmpty(del.toString())) return;
		
		String delSql = "DELETE FROM room_pic_path WHERE pic_id in (" + del.toString().substring(1) + ")";
		super.deleteByHql(delSql);
	}

	

}
