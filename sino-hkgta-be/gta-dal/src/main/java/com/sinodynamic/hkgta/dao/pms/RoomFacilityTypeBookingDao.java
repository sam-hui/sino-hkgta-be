package com.sinodynamic.hkgta.dao.pms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomFacilityTypeBooking;

public interface RoomFacilityTypeBookingDao extends IBaseDao<RoomFacilityTypeBooking> {

	List<RoomFacilityTypeBooking> getBookingByFacilityTypeBookingId(Long facilityTypeBookingId);
}
