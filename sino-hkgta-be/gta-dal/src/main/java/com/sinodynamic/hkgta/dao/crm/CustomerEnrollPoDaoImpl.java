package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPo;

@Repository
public class CustomerEnrollPoDaoImpl extends GenericDao<CustomerEnrollPo> implements CustomerEnrollPoDao{
	
	@Override
	public List<CustomerEnrollPo> getOrderNoListByEnrollId(Long EnrollId ){
		
 		String hql = "select c.orderNo as orderNo from CustomerEnrollPo c where c.customerEnrollment.enrollId = " + EnrollId;
		
		return getByHql(hql);
	}
	
	public Serializable saveCustomerEnrollPo(CustomerEnrollPo cEnrollPo){
		return save(cEnrollPo);
	}
	
	public List<CustomerEnrollPo> getListByEnrollId(Long EnrollId ){
 		String hql = " from CustomerEnrollPo c where c.customerEnrollment.enrollId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(EnrollId);
		return getByHql(hql, param);
	}
	
	public CustomerEnrollPo getEnrollPoByOrderNo(Long orderNo ){
 		String hql = " from CustomerEnrollPo c where c.orderNo = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(orderNo);
		return (CustomerEnrollPo) getUniqueByHql(hql, param);
	}
	
	public int deleteCustomerEnrollPoByEnrollId(Long enrollId) {
		Object[] param = new Object[]{enrollId};
		return this.deleteByHql("DELETE m from customer_enroll_po m where m.enroll_id = ? ",param);
	}

}
