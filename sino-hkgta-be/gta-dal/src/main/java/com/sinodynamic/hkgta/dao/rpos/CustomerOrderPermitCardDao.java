package com.sinodynamic.hkgta.dao.rpos;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;

public interface CustomerOrderPermitCardDao extends IBaseDao<CustomerOrderPermitCard>{
	
	public CustomerOrderPermitCard getByCandidateCustomerId(Long candidateCustomerId);

	public List<CustomerOrderPermitCard> getUnACTList(String string);

	public List<CustomerOrderPermitCard> getExpireCustomerOrderPermitCard(
			Date date);
	
}
