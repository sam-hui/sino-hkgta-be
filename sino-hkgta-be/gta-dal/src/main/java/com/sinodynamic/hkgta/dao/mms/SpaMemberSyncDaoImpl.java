package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository
public class SpaMemberSyncDaoImpl extends GenericDao<SpaMemberSync> implements SpaMemberSyncDao {

    @Override
    public List<GuestRequestDto> getMemberInfoList2Synchronize(String status) {
	
	String sql = "select "
		+ "sms.sys_id as sysId, "
		+ "(select sms1.spa_guest_id from spa_member_sync sms1 where sms1.customer_id = sms.customer_id and sms1.action = 'INSERT' and sms1.status = 'CMP') as guestId, "
		+ "sms.action as action, "
		+ "me.academy_no as guestCode, "
		+ "cp.given_name as firstName, "
		+ "cp.surname as lastName, "
		+ "cp.phone_mobile as mobilePhone, "
		+ "cp.contact_email as email, "
		+ "cp.gender as gender, "
		+ "cp.postal_address1 as address1, "
		+ "cp.postal_address2 as address2 "
		+ "from spa_member_sync sms "
		+ "inner join member me on me.customer_id = sms.customer_id "
		+ "inner join customer_profile cp on sms.customer_id = cp.customer_id "
		+ "where sms.status = ?";
	
	List<Serializable> params = new ArrayList<Serializable>();
	params.add(status);
	
	return getDtoBySql(sql, params, GuestRequestDto.class);
    }

    @Override
    public List<SpaMemberSync> getSpecificSpaMemberSync(Long customerId, String status) {
	
	String hql = "from SpaMemberSync where customerId = ? and status = ?";
	List<Serializable> params = new ArrayList<Serializable>();
	params.add(customerId);
	params.add(status);
	return getByHql(hql, params);
    }

    @Override
    public void addSpaMemberSyncWhenInsert(Member member) {
	
	SpaMemberSync entity = new SpaMemberSync();
	entity.setCustomerId(member.getCustomerId());
	entity.setAction(Constant.MEMBER_SYNCHRONIZE_INSERT);
	entity.setCreateDate(new Date());
	entity.setRetryCount(0);
	entity.setStatus(Constant.STATUS_WAITING_TO_SYNCHRONIZE);
	save(entity);
    }

    @Override
    public void addSpaMemberSyncWhenUpdate(CustomerProfile oldThis, CustomerProfile newThis) {
	
	if (oldThis == null || newThis == null) return;
	String surName = CommUtil.nvl(newThis.getSurname());
	String givenName = CommUtil.nvl(newThis.getGivenName());
	String contactEmail = CommUtil.nvl(newThis.getContactEmail());
	String phoneMobile = CommUtil.nvl(newThis.getPhoneMobile());
	String gender = CommUtil.nvl(newThis.getGender());
	
	if (surName.equals(oldThis.getSurname()) && givenName.equals(oldThis.getGivenName())
		&& contactEmail.equals(oldThis.getContactEmail())
		&& phoneMobile.equals(oldThis.getPhoneMobile())
		&& gender.equals(oldThis.getGender())) return;
	
	SpaMemberSync entity = new SpaMemberSync();
	entity.setCustomerId(oldThis.getCustomerId());
	entity.setAction(Constant.MEMBER_SYNCHRONIZE_UPDATE);
	entity.setCreateDate(new Date());
	entity.setRetryCount(0);
	entity.setStatus(Constant.STATUS_WAITING_TO_SYNCHRONIZE);
	save(entity);
    }
    
}
