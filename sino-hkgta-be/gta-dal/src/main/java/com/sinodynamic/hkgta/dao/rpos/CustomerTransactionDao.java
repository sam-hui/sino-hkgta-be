package com.sinodynamic.hkgta.dao.rpos;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CustomerTransactionDao  extends IBaseDao<CustomerOrderHd>{
	public ListPage<CustomerOrderHd> getCustomerTransactionList(ListPage<CustomerOrderHd> page,String balanceStatus,String serviceType,String orderBy,boolean isAscending,String searchText,String filterBySalesMan,String mode, String enrollStatusFilter,String memberType);
	public BigDecimal countBalanceDueStatus(Long orderNo);
	public BigInteger getCustomerID(Long orderNo);
	public List<AdvanceQueryConditionDto> assembleQueryConditions(List<SysCode> statusCodes,String serviceType);
//	public ListPage<CustomerOrderHd> getCustomerTransactionRenewalList(
//			ListPage<CustomerOrderHd> page, String balanceStatus,
//			String serviceType, String orderBy, boolean isAscending,
//			String searchText, String filterBySalesMan, String mode,
//			String enrollStatusFilter);
}