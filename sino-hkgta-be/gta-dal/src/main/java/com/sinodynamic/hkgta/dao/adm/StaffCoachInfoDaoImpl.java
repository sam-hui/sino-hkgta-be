package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.AvailableCoachDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffCoachInfo;

@Repository
public class StaffCoachInfoDaoImpl extends GenericDao<StaffCoachInfo> implements StaffCoachInfoDao{

	@Override
	public StaffCoachInfo getByUserId(String userId) throws HibernateException
	{
		String hql = " from StaffCoachInfo s where s.userId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		return (StaffCoachInfo) this.getUniqueByHql(hql,param);
	}

	@Override
	public List<AvailableCoachDto> getAvailableCoach(Date date, String timeslot, String staffType) {
		String partTime = "PT"+staffType;
		String fullTime = "FT"+staffType;
		if(date ==null){
			String allCoachSql = "SELECT \n" +
					"    m.user_id as coachId, c.speciality as speciality, c.personal_info as qualification, CONCAT (s.given_name, ' ' ,s.surname) AS nickname, s.portrait_photo as portraitPhoto\n" +
					"FROM\n" +
					"    staff_master m,\n" +
					"    staff_coach_info c\n" +
					"        LEFT JOIN\n" +
					"    user_master u ON (u.user_id = c.user_id)\n" +
					"        LEFT JOIN\n" +
					"    staff_profile s ON (s.user_id = c.user_id)\n" +
					"WHERE\n" +
					" 		exists (select 1 from staff_coach_rate_pos r" +
					"			 where r.user_id=m.user_id  and r.rate_type in('HI','LO') " +
					" 			group by r.rate_type having count(r.rate_type)>=1) "+
					"		 AND    m.user_id = c.user_id\n" +
					"        AND m.status = 'ACT'\n" +
					"        AND m.employ_date < NOW()\n" +
					"        AND (m.quit_date is null or m.quit_date > NOW())\n" +
					"        AND (m.staff_type = ?\n" +
					"        OR m.staff_type = ?)\n";
			return this.getDtoBySql(allCoachSql, Arrays.asList(partTime,fullTime), AvailableCoachDto.class);
		}else{
			String[] slot = timeslot.split(",");
			String coachByDateSql = 
					"select \n" +
							"    m.user_id as coachId,\n" +
							"    c.speciality as speciality, c.personal_info as qualification, \n" + 
							"    CONCAT (s.given_name, ' ' ,s.surname) AS nickname,\n" +
							"    s.portrait_photo as portraitPhoto\n" +
							"from\n" +
							"    staff_master m,\n" +
							"    staff_coach_info c\n" +
							"        left join\n" +
							"    user_master u on (u.user_id = c.user_id)\n" +
							"        left join\n" +
							"    staff_profile s on (s.user_id = c.user_id)\n" +
							"where\n" +
							"        m.employ_date < NOW()\n" +
							"        AND (m.quit_date is null or m.quit_date > NOW())\n" +
							"		 and exists (select 1 from staff_coach_rate_pos r" +
							"			 where r.user_id=m.user_id  and r.rate_type in('HI','LO') " +
							" 			group by r.rate_type having count(r.rate_type)>=1) "+
							"        and   m.user_id = c.user_id\n" +
							"        and m.STATUS = 'ACT'\n" +
							"        and u.user_type = 'STAFF'\n" +
							"        and (m.staff_type = ?\n" +
							"        or m.staff_type = ?)\n" +
							"        and m.user_id not in (select \n" +
							"            coach_user_id\n" +
							"        from\n" +
							"            staff_coach_roster\n" +
							"        where\n" +
							"            begin_time in (" + getQuestionMasks(slot) + ") and off_duty in ('Y','B')\n" +
							"                and on_date = ?)\n" +
							"        and m.user_id not in (select \n" +
							"            coach_user_id\n" +
							"        from\n" +
							"            staff_coach_roster\n" +
							"        where\n" +
							"             begin_time in (" + getQuestionMasks(slot) + ") and week_day = ? and off_duty in ('Y','B')\n" +
							"                and coach_user_id not in (select \n" +
							"                    coach_user_id\n" +
							"                from\n" +
							"                    staff_coach_roster\n" +
							"                where\n" +
							"                    on_date = ?\n" +
							"                        and rate_type is not null))\n" +
							"        and m.user_id not in (select \n" +
							"            staff_user_id\n" +
							"        from\n" +
							"            staff_timeslot\n" +
							"        where\n" +
							"        status = 'OP' and \n" +
							"            date_format(begin_datetime, '%Y-%m-%d') = ?\n" +
							"                and date_format(begin_datetime, '%H') in (" + getQuestionMasks(slot) + "))\n";
			String dateStr = DateFormatUtils.format(date, "yyyy-MM-dd");
			Calendar dateTime = Calendar.getInstance();
			dateTime.setTime(date);
			
			List<Serializable> params = new ArrayList<Serializable>();
			params.add(partTime);
			params.add(fullTime);
			params.addAll(CollectionUtils.arrayToList(slot));
			params.add(date);
			params.addAll(CollectionUtils.arrayToList(slot));
			params.add(dateTime.get(Calendar.DAY_OF_WEEK));
			params.add(date);
			params.add(dateStr);
			params.addAll(CollectionUtils.arrayToList(slot));
			return this.getDtoBySql(coachByDateSql, params, AvailableCoachDto.class);
		}
	}

	private String getQuestionMasks(Object[] values){
		String[] masks = new String[values.length];
		Arrays.fill(masks,"?");
		return StringUtils.join(masks,",");
	}
	
	@Override
	public PrivateCoachInfoDto getCoachProfile(String coachId) {
		StringBuffer sql = new StringBuffer(); 
				
				sql.append("SELECT ")
				.append("s.portrait_photo as portraitPhoto, ")
				.append("CONCAT (s.given_name, ' ' ,s.surname) AS nickname, ")
				.append("(SELECT position_name FROM position_title WHERE position_code = m.position_title_code) as positionTitle, ")
				.append("(CASE (s.gender) when 'F' then 'F' when 'M' then 'M' else 'N' end) as sex, ")
				.append("s.date_of_birth as dateOfBirth, ")
				.append("(SELECT code_display FROM sys_code where category = 'nationality' and code_value = s.nationality) as nationality, ")
				.append("c.personal_info as qualification, ")
				.append("c.speciality ")
				.append("FROM ")
				.append("user_master u ")
				.append("LEFT JOIN ")
				.append("staff_profile s ON (s.user_id = u.user_id) ")
				.append("LEFT JOIN ")
				.append("staff_master m ON (u.user_id = m.user_id) ")
				.append("LEFT JOIN ")
				.append("staff_coach_info c ON (c.user_id = u.user_id) ")
				.append("WHERE ")
				.append("u.user_id = '").append(coachId).append("'");
				
		List<PrivateCoachInfoDto> dtoList = this.getDtoBySql(sql.toString(), null, PrivateCoachInfoDto.class);
		Assert.notEmpty(dtoList, "No such coach");
		Assert.isTrue(dtoList.size()==1, "More than one coach have the same coach id");
		return dtoList.get(0);
	}

	
}
