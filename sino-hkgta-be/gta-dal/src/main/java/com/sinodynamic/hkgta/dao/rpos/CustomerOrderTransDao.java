package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CustomerOrderTransDao extends IBaseDao<CustomerOrderTrans> {

	public List<CustomerOrderTrans> getPaymentDetailsByOrderNo(Long orderNo);
	
	public List<CustomerOrderTrans> getPaymentDetailsByTransactionNO(Long transactionNO);

	public List getLatestTransationActivaties(int totalSize);
	
	public Serializable saveCustomerOrderTrans(CustomerOrderTrans customerOrderTrans);
	
	public ListPage<CustomerOrderTrans> getTransactionList(ListPage<CustomerOrderTrans> page, Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String clientType, String filterSQL);

	public ListPage<CustomerOrderTrans> getTopupHistory(
			ListPage<CustomerOrderTrans> page, Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String filterSQL);
	
	public List<CustomerOrderTrans> getTimeoutPayments(Long timeoutMin);
	
	public BigDecimal getApprovedPaymentAmount(Long orderNo);
	
	public BigDecimal getCustomerOrderTransAmoutByDay(long customerId,Date date);
	
	public BigDecimal getCustomerOrderTransAmoutByMonth(long customerId,int month);
	
	public byte[] getInvoiceReceipt(Long orderNo,String transactionNo,String receiptType) throws JRException;

	public ListPage<CustomerOrderTrans> getSattlementReport(ListPage<CustomerOrderTrans> page,
			SearchSettlementReportDto dto);
	
	public ListPage<CustomerOrderTrans> getMonthlyEnrollment(ListPage<CustomerOrderTrans> page,String year, String month);
	
	public byte[] getMonthlyEnrollmentAttachment(String year,String month,String fileType,String sortBy,String isAscending) throws JRException;

	public ListPage<CustomerOrderTrans> getCorporateMembershipSettlement(ListPage<CustomerOrderTrans> page, String status,Long filterByCustomerId);
	
	public ListPage<CustomerOrderTrans> getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage<?> page,
			String selectedDate, String facilityType);

	public byte[] getDailyMonthlyPrivateCoachingRevenueAttachment(String timePeriodType, String selectedDate,
			String fileType, String sortBy, String isAscending, String facilityType) throws Exception;
	
	public Date getLastestSpaSyncTime();
	
	public byte[] getRenewalEnrollmentAttachment(String fileType,String sortBy,String isAscending) throws JRException;

}
