package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;


import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CorporateMemberDto;
import com.sinodynamic.hkgta.dto.membership.SearchSpendingSummaryDto;
import com.sinodynamic.hkgta.dto.membership.SpendingSummaryDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class CorporateMemberDaoImpl extends GenericDao<CorporateMember> implements CorporateMemberDao {

	public Serializable addCorporateMember(CorporateMember corporateMember){
		return save(corporateMember);
	}
	
	public List<CorporateMember> getAllCorporateMember(Long corporateId){
		String hqlstr = " from CorporateMember cm where cm.corporateProfile.corporateId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		return getByHql(hqlstr, param);
	}
	
	public List<CorporateMember> getAllCorporateMemberByStatus(Long corporateId,String status){
		String hqlstr = " from CorporateMember cm where cm.corporateProfile.corporateId = ? and cm.status =? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		param.add(status);
		return getByHql(hqlstr, param);
	}
	
	public boolean updateCorporateMember(CorporateMember corporateMember){
		return update(corporateMember);
	}
	
	public boolean updateStatus(String status, Long customerId){
		String hqlstr=" update CorporateMember cm set cm.status = ? where cm.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	
	public boolean updateStatus(String status, Long customerId, String loginUserId){
		String hqlstr=" update CorporateMember cm set cm.status = ?, cm.updateBy = ?, cm.updateDate = ? where cm.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(loginUserId);
		param.add(new Date());
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}

	@Override
	public ListPage<CorporateMember> getSpendingSummary(ListPage<CorporateMember> page,
			SearchSpendingSummaryDto dto) {
		String sql = "SELECT coh.create_date as orderDate,m.academy_no as academyNo,CONCAT(cp.salutation,' ',cp.given_name,' ',cp.surname) as member,"
				+ " pos.description as description,cot.paid_amount as spending,pos.item_catagory as itemCatagory "
				+ " FROM customer_order_hd coh,customer_order_trans cot,customer_order_det cod,pos_service_item_price pos,corporate_member cm,member m,customer_profile cp " 
				+ " where cot.order_no = coh.order_no AND cot.payment_method_code = 'CV' AND cot.`status` = 'SUC' "
				+ " AND coh.order_no = cod.order_no and coh.customer_id = cm.customer_id and cod.item_no = pos.item_no "
				+ " and cm.customer_id = cp.customer_id and cm.customer_id = m.customer_id  <QueryCondition>";
			
		StringBuilder queryCondition = new StringBuilder();
		List<Object> param = new ArrayList<Object>();
		String dateFiled = dto.getDateField();
		String dateValue = dto.getDateVaule();
		if (CommUtil.notEmpty(dateFiled)) {
			if (CommUtil.notEmpty(dateValue)) {
				if ("M".equalsIgnoreCase(dateFiled)) {
					String[] yearMonth = dateValue.split("/");
					queryCondition.append(" and ( YEAR(coh.order_date) = ? and MONTH(coh.order_date) = ? )");
					param.add(yearMonth[0]);
					param.add(yearMonth[1]);
				}else if ("Y".equals(dateFiled)) {
					queryCondition.append(" and  YEAR(coh.order_date) = ? ");
					param.add(dateValue);
				}
			}
		}
		String corporateId = dto.getCorporateId();
		String customerId = dto.getCustomerId();
		if (CommUtil.notEmpty(corporateId)) {
			if ("ALL".equals(corporateId)) {
				if (!CommUtil.notEmpty(customerId) || "ALL".equalsIgnoreCase(customerId)) {
					
				}else {
					queryCondition.append(" and m.customer_id = ? ");
					param.add(Integer.parseInt(customerId));
				}
			}else {
				if (CommUtil.notEmpty(customerId) && !"ALL".equalsIgnoreCase(customerId)) {
					queryCondition.append(" and cm.corporate_id = ? and m.customer_id = ? ");
					param.add(Integer.parseInt(corporateId));
					param.add(Integer.parseInt(customerId));
				}else {
					queryCondition.append(" and cm.corporate_id = ? ");
					param.add(Integer.parseInt(corporateId));
				}
			}
		}else {
			if (!CommUtil.notEmpty(customerId) || "ALL".equalsIgnoreCase(customerId)) {
				
			}else {
				queryCondition.append(" and m.customer_id = ? ");
				param.add(Integer.parseInt(customerId));
			}
		}
			
		sql = sql.replace("<QueryCondition>", queryCondition.toString());
		
 		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " and "+page.getCondition();
			sql = sql + condition;
		}
		
		String countSql = "SELECT COUNT(tt.orderDate) from ( " + sql + " ) tt";
		return listBySqlDto(page, countSql, sql, param, new SpendingSummaryDto());	
	}
	
	@Override
	public List<CorporateMemberDto> getMemberDropList(Long corporateId) {
		String sql = "select cm.customer_id as customerId, concat(cp.salutation, ' ',cp.given_name, ' ', cp.surname) as memberName from corporate_member cm, customer_profile cp where cm.customer_id = cp.customer_id and cm.corporate_id = ? order by cp.salutation, cp.given_name, cp.surname ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		return getDtoBySql(sql, param, CorporateMemberDto.class);
	}

	@Override
	public List<CorporateMemberDto> checkCreditLimit(Long corporateId) {
		String sql = "SELECT a.customer_id as customerId,a.total_credit_limit as totalCreditLimit,s.totalSum, a.total_credit_limit-s.totalSum as remainSum \n" +
				"FROM(SELECT cm.customer_id,acc.total_credit_limit FROM corporate_member cm, corporate_service_acc acc WHERE cm.corporate_id = acc.corporate_id AND cm.corporate_id = ?) a\n" +
				"LEFT JOIN (SELECT (case when SUM(r.num_value) is null then 0 else SUM(r.num_value) end) AS totalSum,r.customer_id FROM	member_limit_rule r, member m,corporate_member c WHERE date_format( ?,'%Y-%m-%d')>=r.effective_date and date_format( ?,'%Y-%m-%d') <= r.expiry_date and r.customer_id = m.customer_id AND r.customer_id = c.customer_id and m.member_type = ? and r.limit_type = ? GROUP BY r.customer_id) s \n" +
				"ON s.customer_id = a.customer_id";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		param.add(currentDate);
		param.add(currentDate);
		param.add(MemberType.CPM.name());
		param.add(LimitType.CR.name());
		return (List<CorporateMemberDto>)getDtoBySql(sql, param, CorporateMemberDto.class);
	}
	
	public BigDecimal getAllocatedCreditAmountByCorporateId(Long corporateId){
		String sql = "SELECT\n" +
				"	ifnull(sum(rule.num_value),0) AS allocatedAmount\n" +
				"FROM\n" +
				"	corporate_service_acc acc,\n" +
				"	corporate_member corpMember,\n" +
				"	member_limit_rule rule\n" +
				"WHERE\n" +
				"	corpMember.corporate_id = acc.corporate_id\n" +
				"AND rule.customer_id = corpMember.customer_id\n" +
				"AND acc.status = 'ACT'\n" +
				"AND rule.limit_type = 'CR'\n" +
				"AND date_format(now(), '%Y-%m-%d') BETWEEN rule.effective_date\n" +
				"AND rule.expiry_date\n" +
				"AND acc.corporate_id = ?";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		BigDecimal remainingSum  = (BigDecimal) getUniqueBySQL(sql, param);
		return remainingSum;
	}
	
	public BigDecimal getRemainAvailableCreditLimit(Long corporateId, Long customerId) {
		String sql = "SELECT\n" +
				"	corporate.totalCreditLimit - IFNULL(\n" +
				"		allocated.allocatedAmount,\n" +
				"		0\n" +
				"	) AS remainSum\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			acc.total_credit_limit AS totalCreditLimit,\n" +
				"			acc.corporate_id AS corporateId\n" +
				"		FROM\n" +
				"			corporate_service_acc acc\n" +
				"		WHERE\n" +
				"			acc.corporate_id = ?\n" +
				"		AND acc.status = 'ACT'\n" +
				"       AND acc.acc_no = (select max(subacc.acc_no) from corporate_service_acc subacc where acc.corporate_id =subacc.corporate_id)\n"+
				"	) corporate\n" +
				"LEFT JOIN (\n" +
				"	SELECT\n" +
				"		sum(rule.num_value) AS allocatedAmount,\n" +
				"		acc.corporate_id AS corporateId\n" +
				"	FROM\n" +
				"		corporate_service_acc acc,\n" +
				"		corporate_member corpMember,\n" +
				"		member_limit_rule rule\n" +
				"	WHERE\n" +
				"		corpMember.corporate_id = acc.corporate_id\n" +
				"	AND rule.customer_id = corpMember.customer_id\n" +
				"	AND acc.status = 'ACT'\n" +
				"	AND rule.limit_type = ?\n" +
				"	AND date_format(?, '%Y-%m-%d') BETWEEN rule.effective_date\n" +
				"	AND rule.expiry_date\n" +
				"   <filteroutforcurrentcustomerid>"+
				"	AND acc.corporate_id = ?\n" +
				"   AND acc.acc_no = (select max(subacc.acc_no) from corporate_service_acc subacc where acc.corporate_id =subacc.corporate_id)\n"+
				") allocated ON allocated.corporateId = corporate.corporateId";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		param.add(LimitType.CR.name());
		param.add(new Date());
		if(customerId!=null){
			sql = sql.replace("<filteroutforcurrentcustomerid>", " AND rule.customer_id != ?\n");
			param.add(customerId);
		}else{
			sql = sql.replace("<filteroutforcurrentcustomerid>", "");
		}
		param.add(corporateId);
		BigDecimal remainingSum  = (BigDecimal) getUniqueBySQL(sql, param);
		return remainingSum;
	}
	
	@Override
	public CorporateMember getCorporateMemberById(Long customerId) {
		return get(CorporateMember.class, customerId);
	}

	@Override
	public BigDecimal getTotalSpending(ListPage<CorporateMember> page,SearchSpendingSummaryDto dto) {
		String sql = "SELECT SUM(cot.paid_amount) "
				+ " FROM customer_order_hd coh,customer_order_trans cot,customer_order_det cod,pos_service_item_price pos,corporate_member cm,member m,customer_profile cp " 
				+ " where cot.order_no = coh.order_no AND cot.payment_method_code = 'CV' AND cot.`status` = 'SUC'"
				+ " AND	coh.order_no = cod.order_no and coh.customer_id = cm.customer_id and cod.item_no = pos.item_no "
				+ " and cm.customer_id = cp.customer_id and cm.customer_id = m.customer_id  <QueryCondition>";
			
		StringBuilder queryCondition = new StringBuilder();
		List<Serializable> param = new ArrayList<Serializable>();
		String dateFiled = dto.getDateField();
		String dateValue = dto.getDateVaule();
		if (CommUtil.notEmpty(dateFiled)) {
			if (CommUtil.notEmpty(dateValue)) {
				if ("M".equals(dateFiled)) {
					String[] yearMonth = dateValue.split("/");
					queryCondition.append(" and ( YEAR(coh.order_date) = ? and MONTH(coh.order_date) = ? )");
					param.add(yearMonth[0]);
					param.add(yearMonth[1]);
				}else if ("Y".equals(dateFiled)) {
					queryCondition.append(" and  YEAR(coh.order_date) = ? ");
					param.add(dateValue);
				}
			}
		}
		String corporateId = dto.getCorporateId();
		String customerId = dto.getCustomerId();
		if (CommUtil.notEmpty(corporateId)) {
			if ("ALL".equals(corporateId)) {
				if (!CommUtil.notEmpty(customerId) || "ALL".equals(customerId)) {
					
				}else {
					queryCondition.append(" and m.customer_id = ? ");
					param.add(Integer.parseInt(customerId));
				}
			}else {
				if (CommUtil.notEmpty(customerId) && !"ALL".equals(customerId)) {
					queryCondition.append(" and cm.corporate_id = ? and m.customer_id = ? ");
					param.add(Integer.parseInt(corporateId));
					param.add(Integer.parseInt(customerId));
				}else {
					queryCondition.append(" and cm.corporate_id = ? ");
					param.add(Integer.parseInt(corporateId));
				}
			}
		}else {
			if (!CommUtil.notEmpty(customerId) || "ALL".equals(customerId)) {
				
			}else {
				queryCondition.append(" and m.customer_id = ? ");
				param.add(Integer.parseInt(customerId));
			}
		}
			
		sql = sql.replace("<QueryCondition>", queryCondition.toString());
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " and "+page.getCondition();
			sql = sql + condition;
		}
		return (BigDecimal)getUniqueBySQL(sql, param);
	}
}

