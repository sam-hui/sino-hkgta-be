package com.sinodynamic.hkgta.dao.crm.pub;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.Notice;
import com.sinodynamic.hkgta.util.constant.NoticeStatus;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class NoticeDaoImpl extends GenericDao<Notice> implements NoticeDao {

	@Override
	public ListPage<Notice> queryNoticeList(String catId, Integer pageNumber, Integer pageSize) {

		StringBuilder hql = new StringBuilder();
		hql.append("select n from Notice n, SysCode s  where s.codeValue = n.noticeType and s.category = 'noticeType' ");
		hql.append(" and n.status = ?");
		hql.append(" and n.noticeBegin <=? ");
		
		StringBuilder countHql = new StringBuilder();
		countHql.append("select count(*) from Notice n, SysCode s where s.codeValue = n.noticeType and s.category = 'noticeType'");
		countHql.append(" and n.status = ?");
		countHql.append(" and n.noticeBegin <=? ");
		
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(NoticeStatus.ACT.toString());
		params.add(new Date());
		
		if (StringUtils.isNotBlank(catId)) {
			hql.append(" and s.codeValue = ? ");
			countHql.append(" and s.codeValue = ? ");
			params.add(catId);
		}

		hql.append(" order by n.noticeBegin desc, UPPER(n.subject) asc");
		
		ListPage<Notice> page = new ListPage<Notice>();
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		
		page = listByHql(page, countHql.toString(), hql.toString(), params);

		return page;
	}

}
