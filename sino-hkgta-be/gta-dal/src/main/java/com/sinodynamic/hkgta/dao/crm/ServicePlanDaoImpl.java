package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.DailyDayPassUsageDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository("Daypasslist")
public class ServicePlanDaoImpl extends GenericDao<ServicePlan> implements ServicePlanDao , AdvanceQueryConditionDao{
	
	Logger logger = Logger.getLogger(ServicePlanDaoImpl.class);

	public Serializable saveServicePlan(ServicePlan servicePlan) {
		return save(servicePlan);
	}

	public List<FacilityType> getFacilityTypes(){
		List<FacilityType> types = excuteByHql(FacilityType.class, "from FacilityType");
		return types;
	}
	
	public List<ServicePlan> getAllServicePlan() throws HibernateException{
		
		String hql = "from ServicePlan where passNatureCode= 'LT'";
		List<ServicePlan> lst = new ArrayList<ServicePlan>();
		
		try{
		lst = (List<ServicePlan>)getByHql(hql);
		System.out.println("list is: "+lst.size());
		} catch(HibernateException e){
			logger.error("Error message:" + e.getMessage());
		} finally{
			
		}
		return lst;
		
	}

	public ServicePlan getServicePlanById(Long planNo) {
		return get(ServicePlan.class , planNo);
	}
	
	public ServicePlanAdditionRule getServicePlanAdditionRuleByFK(long planNo,String rightCode){
		List<Serializable> serial = new ArrayList<Serializable>();
		serial.add(planNo);
		serial.add(rightCode);
		ServicePlanAdditionRule rule = (ServicePlanAdditionRule)getUniqueByHql("from ServicePlanAdditionRule rule where rule.planNo=? and rule.rightCode=?",serial);
		return rule;
	}
	
	public List<ServicePlanRightMaster> getServicePlanRights(){
		List<ServicePlanRightMaster> rights = excuteByHql(ServicePlanRightMaster.class, "from ServicePlanRightMaster");
		return rights;
	}
	
	
	public ServicePlanFacility getServicePlanFacilityByFK(long planNo, String facilityType){
		List<Serializable> serial = new ArrayList<Serializable>();
		serial.add(planNo);
		serial.add(facilityType);
		
		ServicePlanFacility servicePlanFacility = (ServicePlanFacility)getUniqueByHql("from ServicePlanFacility servicePlanFacility where servicePlanFacility.servicePlanId=? and servicePlanFacility.facilityTypeCode=?",serial);
		return servicePlanFacility;
	}

	public void deleteServicePlan(ServicePlan sp) throws HibernateException {
		this.delete(sp);	
	}
	/**used for pc purchase daypass
	 * list all the daypass which their effectiveStartDate<=current_date<=effectiveEndDate,member can buy all these daypass,
	 */
	@Override
	public List<ServicePlan> getServiceplanByDate(DaypassPurchaseDto dto) {
		String hql=" from ServicePlan sp where current_date >= effectiveStartDate  and  current_date <= effectiveEndDate and  passNatureCode='DP' and status='ACT' ";
		if(dto.getCustomerId()==null){
			hql+=" and subscriberType='STF'  ";
		}else{
			hql+=" and (subscriberType ='M' ) ";
		}
		return getByHql(hql);
	}
	/**used for webprotal purchase daypass
	 * list all the daypass which their effectiveStartDate<=current_date<=effectiveEndDate,member can buy all these daypass,
	 */
	@Override
	public List<ServicePlan> getValidDaypass() {
		String hql=" from ServicePlan sp where current_date >= effectiveStartDate  and  current_date <= effectiveEndDate and  passNatureCode='DP' and status='ACT' and subscriberType ='M' ";
		return getByHql(hql, null);
	}
	
	@Override
	public List<ServicePlan> getValidServicPlan() {
		String hql = "from ServicePlan s where s.passNatureCode = 'LT' and s.status != 'NACT' and s.subscriberType = 'IPM' and ? BETWEEN s.effectiveStartDate AND effectiveEndDate order by s.planName ASC";
		List<ServicePlan> lst = new ArrayList<ServicePlan>();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(new Date());
		try{
		lst = (List<ServicePlan>)getByHql(hql,param);
		System.out.println("list is: "+lst.size());
		} catch(HibernateException e){
			logger.error("Error message:" + e.getMessage());
		} finally{
			
		}
		return lst;
	}
	
	public List<ServicePlan> getValidCorporateServicPlan() {
		String hql = "from ServicePlan s where s.passNatureCode = 'LT' and s.status != 'NACT' and s.subscriberType = 'CPM' and date_format( ?,'%Y-%m-%d') BETWEEN s.effectiveStartDate AND effectiveEndDate order by s.planName ASC";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(new Date());
		return getByHql(hql,param);
	}

	@Override
	public List<PosServiceItemPrice> getDaypassPriceItemNo(Long planNo, String type) {
		StringBuilder sql = new StringBuilder(" select p.item_no as itemNo, p.item_catagory as itemCatagory , p.item_price as itemPrice from ")
			.append(" service_plan s, service_plan_pos sp,  pos_service_item_price p  ") ;
		if(type.equals(Constant.ServiceplanType.DAYPASS.toString())){
			sql.append(" , service_plan_rate_pos pos ");
		} else if(type.equals(Constant.ServiceplanType.SERVICEPLAN.toString())){
			sql.append(" , service_plan_offer_pos  pos ");
		}
		sql.append(" where s.plan_no = sp.plan_no and sp.serv_pos_id=pos.serv_pos_id ");
		sql.append("and (p.item_no=sp.pos_item_no or p.item_no=pos.pos_item_no) and s.plan_no=?");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		return this.getDtoBySql(sql.toString(), param, PosServiceItemPrice.class);
	}

	@Override
	public List<ServicePlan> getExpiredServicePlans(String status) {
		String hql = "from ServicePlan s where effectiveEndDate < ? and status = ?";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Timestamp begin = new Timestamp(calendar.getTimeInMillis());
		List<ServicePlan> plans = new ArrayList<ServicePlan>();
		
		try{
			plans = getByHql(hql,Arrays.<Serializable>asList(begin, status));
		} catch(HibernateException e){
			logger.error("Error message:" + e.getMessage());
		} finally{}
		return plans;
	}
	@Override
	 public  String getServiceplanQuotaById(Long customerId){
		StringBuilder sql = new StringBuilder(" select mlr.num_value as  totalMemberQuota  from member_limit_rule mlr ")
			.append(" inner join service_plan_right_master sprm on mlr.limit_type=sprm.right_code ")
			.append(" where  mlr.customer_id=?  and  sprm.right_type=? and date_format( ?,'%Y-%m-%d')>=mlr.effective_date and date_format( ?,'%Y-%m-%d') <= mlr.expiry_date  ");
		logger.info("getServiceplanQuota: \n"+ sql.toString());
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(Constant.DAY_PASS_PURCHASING);
		param.add(currentDate);
		param.add(currentDate);
		BigDecimal quota = (BigDecimal) this.getUniqueBySQL(sql.toString(), param);
		if(null!=quota){
			String quotaStr = quota.toString();
			return quotaStr.substring(0, quotaStr.lastIndexOf("."));
		}
		return null;
	 }

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		 AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Day Pass ID","planNo","java.lang.Long","",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Day Pass Name","planName","java.lang.String","",2);
		final List<SysCode> status=new ArrayList<>();
		//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled
		SysCode s1=new SysCode();
		s1.setCategory("status");
		s1.setCodeDisplay("Active");
		s1.setCodeValue("ACT");
		status.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("status");
		s2.setCodeDisplay("Inactive");
		s2.setCodeValue("NACT");
		status.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Expired");
		s3.setCodeValue("EXP");
		status.add(s3);

//		SysCode s4=new SysCode();
//		s4.setCategory("status");
//		s4.setCodeDisplay("Cancelled");
//		s4.setCodeValue("CAN");
//		status.add(s4);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Status","status","java.lang.String",status,3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Start Date","effectiveStartDate","java.util.Date","",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Expiry Date","effectiveEndDate","java.util.Date","",5);
		final List<SysCode> subscriberType=new ArrayList<>();
		SysCode s4=new SysCode();
		s4.setCategory("subscriberType");
		s4.setCodeDisplay("Member");
		s4.setCodeValue("M");
		subscriberType.add(s4);

		SysCode s5=new SysCode();
		s5.setCategory("subscriberType");
		s5.setCodeDisplay("Staff");
		s5.setCodeValue("STF");
		subscriberType.add(s5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Purchased By","subscriberType","java.lang.String",subscriberType,6);
		return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServicePlan getByplanName(String planName) {
		StringBuilder hql = new StringBuilder();
		hql.append("FROM ServicePlan h where h.planName like '%" + planName + "%' order by createDate desc");
		
		return getByHql(hql.toString()).get(0);
	}

	@Override
	public ListPage getDailyDayPassUsageList(String date,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL) {

		
		ListPage pageParam = new ListPage(pageSize, currentPage);
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("planNo", LongType.INSTANCE);
		typeMap.put("orderNo", LongType.INSTANCE);
		typeMap.put("purchaseDate", DateType.INSTANCE);
		typeMap.put("daypassName", StringType.INSTANCE);
		typeMap.put("activationDate", DateType.INSTANCE);
		typeMap.put("deactivationDate", DateType.INSTANCE);
		typeMap.put("purchaseType", StringType.INSTANCE);
		typeMap.put("academyId", StringType.INSTANCE);
		typeMap.put("patronName", StringType.INSTANCE);
		typeMap.put("staffName", StringType.INSTANCE);
		typeMap.put("paymentMethodCode", StringType.INSTANCE);
		typeMap.put("paymentMedia", StringType.INSTANCE);
		typeMap.put("location", StringType.INSTANCE);
		typeMap.put("createBy", StringType.INSTANCE);
		typeMap.put("createDate", DateType.INSTANCE);
		typeMap.put("price", BigDecimalType.INSTANCE);
		typeMap.put("orderStatus", StringType.INSTANCE);
		typeMap.put("qty", LongType.INSTANCE);
		
		
		
		String sql = 
				"SELECT planNo, " +
				"       orderNo, " +
				"       purchaseDate, " +
				"       daypassName, " +
				"       activationDate, " +
				"       deactivationDate, " +
				"       purchaseType, " +
				"       academyId, " +
				"       patronName, " +
				"       staffName, " +
				"       paymentMethodCode, " +
				"       paymentMedia, " +
				"       location, " +
				"       createBy, " +
				"       createDate, " +
				"       price, " +
				"       orderStatus, " +
				"       qty FROM " + 
				"       (SELECT DISTINCT " +
				"    plan.plan_no planNo, " +
				"    t.order_no orderNo, " +
				"    t.order_date purchaseDate, " +
				"    plan.plan_name daypassName, " +
				"    daypassNo.activationDate activationDate, " +
				"    daypassNo.deactivationDate deactivationDate, " +
				"    CASE " +
				"        WHEN t.staff_user_id IS NULL THEN 'Member' " +
				"        ELSE 'Staff' " +
				"    END purchaseType, " +
				"    (CASE " +
				"        WHEN " +
				"            t.staff_user_id IS NULL " +
				"        THEN " +
				"            (SELECT " +
				"                    m.academy_no " +
				"                FROM " +
				"                    member m " +
				"                WHERE " +
				"                    m.customer_id = t.customer_id) " +
				"        ELSE NULL " +
				"    END) academyId, " +
				"    (CASE " +
				"        WHEN " +
				"            t.staff_user_id IS NULL " +
				"        THEN " +
				"            (SELECT " +
				"                    CONCAT(c.salutation, " +
				"                                ' ', " +
				"                                c.given_name, " +
				"                                ' ', " +
				"                                c.surname) " +
				"                FROM " +
				"                    customer_profile c " +
				"                WHERE " +
				"                    c.customer_id = t.customer_id) " +
				"        ELSE NULL " +
				"    END) patronName, " +
				"    CASE " +
				"        WHEN " +
				"            t.staff_user_id IS NOT NULL " +
				"        THEN " +
				"            (SELECT " +
				"                    CONCAT(c.given_name, ' ', c.surname) " +
				"                FROM " +
				"                    staff_profile c " +
				"                WHERE " +
				"                    c.user_id = t.staff_user_id) " +
				"        ELSE NULL " +
				"    END staffName, " +
				"    CASE tran.payment_method_code " +
				"        WHEN 'VISA' THEN 'VISA' " +
				"        WHEN 'MASTER' THEN 'MASTER' " +
				"        WHEN 'CASH' THEN 'Cash' " +
				"        WHEN 'CV' THEN 'Cash Value' " +
				"        WHEN 'BT' THEN 'Bank Transfer' " +
				"        WHEN 'CHEQUE' THEN 'Cheque' " +
				"        WHEN 'cheque' THEN 'Cheque' " +
				"        WHEN 'VAC' THEN 'Virtual Account' " +
				"        WHEN 'UPAY' THEN 'Union Pay' " +
				"        WHEN 'OTH' THEN 'N/A' " +
				"    END AS paymentMethodCode, " +
				"    CASE tran.payment_media " +
				"        WHEN 'OTH' THEN 'N/A' " +
				"        WHEN 'OP' THEN 'Online Payment' " +
				"        WHEN 'ECR' THEN 'ECR Terminal' " +
				"        WHEN 'FTF' THEN 'N/A' " +
				"        WHEN '' THEN 'N/A' " +
				"        ELSE 'N/A' " +
				"    END AS paymentMedia, " +
				"    CASE tran.payment_location_code " +
				"        WHEN 'FD1' THEN 'Front desk' " +
				"        WHEN 'BO' THEN 'Back office' " +
				"        WHEN 'FD2' THEN 'Front desk' " +
				"        WHEN 'PS' THEN 'Proshop' " +
				"        WHEN 'MOBILE' THEN 'APP' " +
				"        WHEN '' THEN 'N/A' " +
				"        ELSE 'N/A' " +
				"    END AS location, " +
				"    (SELECT CONCAT(profile.given_name, ' ', profile.surname) FROM staff_profile profile WHERE profile.user_id = t.create_by) createBy, " +
				"    t.create_date createDate, " +
				"    (daypassPrice.totalPrice / 1) price, " +
				"    CASE t.order_status " +
				"        WHEN 'OPN' THEN 'Open' " +
				"        WHEN 'CAN' THEN 'Cancelled' " +
				"        WHEN 'CMP' THEN 'Completed' " +
				"        WHEN 'REJ' THEN 'Rejected' " +
				"        ELSE 'N/A' " +
				"    END AS orderStatus, " +
				"    daypassNo.qty qty " +
				"FROM " +
				"    customer_order_hd t " +
				"        INNER JOIN " +
				"    customer_order_trans tran ON t.order_no = tran.order_no " +
				"        INNER JOIN " +
				"    (SELECT " +
				"        CONCAT(temp.planNo, ' ', temp.orderNo) planNoOrderNo, " +
				"            SUM(temp.itemprice) totalPrice, " +
				"            temp.planNo planNo, " +
				"            temp.orderNo orderNo " +
				"    FROM " +
				"        ((SELECT " +
				"        plan.plan_no planNo, " +
				"            hd.order_no orderNo, " +
				"            det.item_no, " +
				"            det.order_qty num, " +
				"            det.item_total_amout itemprice " +
				"    FROM " +
				"        customer_order_det det " +
				"    INNER JOIN customer_order_hd hd ON det.order_no = hd.order_no " +
				"    INNER JOIN service_plan_pos pos ON pos.pos_item_no = det.item_no " +
				"    INNER JOIN service_plan plan ON plan.plan_no = pos.plan_no) UNION (SELECT " +
				"        plan.plan_no planNo, " +
				"            hd.order_no orderNo, " +
				"            det.item_no, " +
				"            det.order_qty num, " +
				"            det.item_total_amout itemprice " +
				"    FROM " +
				"        customer_order_det det " +
				"    INNER JOIN customer_order_hd hd ON det.order_no = hd.order_no " +
				"    INNER JOIN service_plan_rate_pos rate ON rate.pos_item_no = det.item_no " +
				"    INNER JOIN service_plan_pos pos ON rate.serv_pos_id = pos.serv_pos_id " +
				"    INNER JOIN service_plan plan ON plan.plan_no = pos.plan_no) ) temp " +
				"    GROUP BY planNoOrderNo) daypassPrice ON daypassPrice.orderNo = t.order_no " +
				"        INNER JOIN " +
				"    (SELECT " +
				"        COUNT(temp.planNoOrderNo) qty, " +
				"            temp.planNoOrderNo planNoOrderNo, " +
				"            temp.planNo planNo, " +
				"            temp.orderNo orderNo, " +
				"            temp.deactivationDate deactivationDate, " +
				"            temp.activationDate activationDate " +
				"    FROM " +
				"        (SELECT " +
				"        copc.service_plan_no planNo, " +
				"            t.order_no orderNo, " +
				"            CONCAT(copc.service_plan_no, ' ', t.order_no) planNoOrderNo, " +
				"            copc.effective_from activationDate, " +
				"            copc.effective_to deactivationDate " +
				"    FROM " +
				"        customer_order_hd t " +
				"    INNER JOIN customer_order_permit_card copc ON t.order_no = copc.customer_order_no) temp " +
				"    GROUP BY temp.planNoOrderNo) daypassNo ON daypassPrice.planNoOrderNo = daypassNo.planNoOrderNo " +
				"        INNER JOIN " +
				"    service_plan plan ON plan.plan_no = daypassNo.planNo " +
				"WHERE " +
				"    t.order_date = ?) sub ";
		
		if(!StringUtils.isEmpty(filterSQL)){
			sql = sql + "WHERE " + filterSQL;
		}
		
		List<Serializable> params = new ArrayList<Serializable>(); 
		params.add(date);
		return this.listBySqlDto(
				pageParam,
				getCountSql(sql),
				sql,
				params,
				new DailyDayPassUsageDto(),
				typeMap);
	}
	
	String getCountSql(String sql){
		return "SELECT COUNT(*) FROM (" + sql +") sub";
	}
}
