package com.sinodynamic.hkgta.dao.rpos;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerTransactionListDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class CustomerTransactionDaoImpl extends GenericDao<CustomerOrderHd>
		implements CustomerTransactionDao {
	@Override
	public ListPage<CustomerOrderHd> getCustomerTransactionList(
			ListPage<CustomerOrderHd> page, String balanceStatus,
			String serviceType, String orderBy, boolean isAscending,
			String searchText,String filterBySalesMan, String mode, String enrollStatusFilter,String memberType) {
		StringBuffer sb = new StringBuffer();
		String modeCondition = null;
		String enrollCondition = null;
		String newFields = null;
		String newTransNumberSql = null;
		String servicePlanLeftJoinSql = null;
		String newTransField = null;
		String enrollStatusSql = null;
		String orderStatusSql = null;
		String rejectOrderStatusSql = null;
		String staffProfileJoinSql = null;
		if ("backoffice".equals(mode)) {
			modeCondition = "";
			filterBySalesMan = "";
			newFields = ",sp.plan_name servicePlan,CASE trans.enrollStatus WHEN 'TOA' THEN CONCAT('(',DATEDIFF(trans.effective_date,NOW()),')') ELSE null END AS noOfDays,trans.enrollStatus,concat(spr.given_name, ' ', spr.surname) salesFollowBy,IFNULL(trans.new_trans_number,0) newTransNumber,trans.order_status orderStatus,trans.academy_no academyNo,trans.update_date orderUpdateDateToConvert ";
			newTransField = ",n.new_trans_number,h.order_status,h.update_date ";
			enrollCondition = ",enroll.`status` enrollStatus,enroll.sales_follow_by,enroll.enroll_date,enroll.create_date enrollCreateDate,mem.effective_date,mem.academy_no ";
			if (MemberType.CPM.name().equals(memberType)) {
				enrollCondition = enrollCondition+ " ,corp.company_name AS companyName ";
			}
			newTransNumberSql = "LEFT JOIN (SELECT order_no,count(*) new_trans_number FROM customer_order_trans WHERE customer_order_trans.`status` = \"\" OR customer_order_trans.`status` IS NULL GROUP BY order_no) n ON h.order_no = n.order_no ";
			servicePlanLeftJoinSql = "LEFT OUTER JOIN service_plan_pos spp ON cod.item_no = spp.pos_item_no LEFT OUTER JOIN service_plan sp ON spp.plan_no = sp.plan_no "; 
			enrollStatusSql = " and t.enrollStatus in ('APV','PYF') ";
			orderStatusSql = " and t.orderStatus = 'OPN' ";
			rejectOrderStatusSql = " and t.orderStatus in ('CAN','REJ') ";
			staffProfileJoinSql = " JOIN staff_profile spr ON trans.sales_follow_by = spr.user_id ";
		} else {
			modeCondition = "OR order_item.status = \"\" OR order_item.status is null";
			newFields = " ,trans.order_status AS orderStatus ";
			newTransField = ",h.order_status ";
			enrollCondition = " ,enroll.enroll_date,enroll.create_date enrollCreateDate ";
			newTransNumberSql = "";
			servicePlanLeftJoinSql = "";
			enrollStatusSql = "";
			orderStatusSql = "";
			rejectOrderStatusSql = " AND t.orderStatus = 'OPN'";
			staffProfileJoinSql = "";
		}
		sb.append("(SELECT trans.order_no orderNo, trans.customer_id customerId,trans.order_date effectiveDateToConvert,trans.enroll_date enrollDateToConvert,trans.enrollCreateDate,CONCAT(trans.salutation,' ',trans.given_name,' ',trans.surname) customerName");
		if (MemberType.CPM.name().equals(memberType)) {
			sb.append(", trans.companyName as companyName ");
		}
		sb.append(",trans.order_total_amount orderTotalAmount,trans.balance_due balanceDue,trans.status,trans.contact_email contactEmail,psip.description,spop.offer_code offerCode" + newFields);
		sb.append(" FROM (SELECT trans.*,mem.status, cp.surname, cp.given_name,cp.company_name,cp.salutation,cp.contact_email" + enrollCondition + " FROM ");
		sb.append(" (SELECT h.order_no,h.order_date,h.customer_id,h.order_total_amount,ifnull(h.order_total_amount - p.paid,h.order_total_amount) balance_due " + newTransField);
		//sb.append("FROM customer_order_hd h left join(");
		sb.append("FROM (select hd.*, plan.plan_no, plan.pass_nature_code  from service_plan plan  join service_plan_pos pos on pos.plan_no = plan.plan_no  join pos_service_item_price price on price.item_no = pos.pos_item_no  join customer_order_det det on det.item_no = price.item_no  join customer_order_hd hd on hd.order_no = det.order_no  where plan.pass_nature_code = 'LT'  union select hd.*, plan.plan_no, plan.pass_nature_code  from service_plan plan  join service_plan_pos pos on pos.plan_no = plan.plan_no  join service_plan_offer_pos spop on spop.serv_pos_id = pos.serv_pos_id join pos_service_item_price price on price.item_no = spop.pos_item_no  join customer_order_det det on det.item_no = price.item_no  join customer_order_hd hd on hd.order_no = det.order_no  where plan.pass_nature_code = 'LT') h left join(");
		
		sb.append("SELECT order_item.order_no,sum(paid_amount) paid FROM (");
		sb.append("SELECT d.order_no, t.paid_amount,t.`status` FROM customer_order_det d, customer_order_trans t ");
		sb.append("WHERE d.order_no = t.order_no ) order_item WHERE order_item.status = 'SUC' " + modeCondition);
		sb.append(" GROUP BY order_item.order_no ) p on h.order_no = p.order_no " + newTransNumberSql + " ) trans, customer_profile cp , customer_enrollment enroll , member mem ");
		if (MemberType.CPM.name().equals(memberType)) {
			sb.append(",corporate_member cm,corporate_profile corp ");
		}
		sb.append("WHERE trans.customer_id = cp.customer_id and cp.customer_id = enroll.customer_id and enroll.customer_id = mem.customer_id and mem.member_type = '" + memberType + "'");
		if (MemberType.CPM.name().equals(memberType)) {
			sb.append(" AND cm.customer_id = mem.customer_id AND cm.corporate_id = corp.corporate_id ");
		}
		if(filterBySalesMan.length()>0)
			sb.append(" and enroll.sales_follow_by = '"+filterBySalesMan +"' ");
		sb.append(" ) trans ");
		sb.append("join customer_order_det cod on trans.order_no = cod.order_no join pos_service_item_price psip on cod.item_no = psip.item_no " + staffProfileJoinSql);
		sb.append("left outer join service_plan_offer_pos spop on cod.item_no = spop.pos_item_no " + servicePlanLeftJoinSql + " WHERE psip.item_catagory = 'SRV') t where 1=1 ");

		if (balanceStatus
				.equalsIgnoreCase(Constant.TRANSACTION_BALANCEDUE_PENDING)) {
			sb.append(" and t.balanceDue > 0");
		} else if (balanceStatus
				.equalsIgnoreCase(Constant.TRANSACTION_BALANCEDUE_COMPLETED)) {
			sb.append(" and t.balanceDue <= 0");
		}
		if (serviceType.equals(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)) {
			sb.append(" and t.offerCode = '"
					+ Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL + "' ");
			sb.append(orderStatusSql);
		} else if (serviceType.equals(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)) {
			sb.append(" and t.offerCode is null ");
			if(!StringUtils.isEmpty(enrollStatusFilter)){
				if (enrollStatusFilter.equalsIgnoreCase("All")){
					sb.append(enrollStatusSql);
				}
				else {
					sb.append(" and t.enrollStatus = '" + enrollStatusFilter + "' ");
				}
				sb.append(orderStatusSql);
			}
			//serviceType is Rejected
		} else if (serviceType.equalsIgnoreCase("Reject")){
			sb.append(rejectOrderStatusSql);
		}
		
		//Sales Kit only need to show the "OPN" status
		if("saleskit".equals(mode)){
			sb.append(rejectOrderStatusSql);
		}
		if (CommUtil.notEmpty(searchText)) {
			sb.append(" and (t.customerName like ? or t.companyName like ?)");
		}
		
		if(!StringUtils.isEmpty(page.getCondition())){
			sb.append(" and ").append(page.getCondition());
		}
		if(orderBy.endsWith("effectiveDate"))
			orderBy = "enrollDateToConvert";
		if(orderBy.endsWith("updateDate"))
			orderBy = "orderUpdateDateToConvert";
		sb.append(" order by t." + orderBy + (isAscending ? " asc " : " desc "));
		
		if(!orderBy.endsWith("enrollCreateDate"))
			sb.append(" ,t.enrollCreateDate");

		String sqlState = "select * from " + sb.toString();
		String sqlCount = "select count(*) from " + sb.toString();
		

		if (CommUtil.notEmpty(searchText)) {
			return listBySqlDto(page, sqlCount, sqlState, Arrays.asList(new String[] {"%" + searchText + "%", "%" + searchText + "%"}),
				new CustomerTransactionListDto());
		}
		else
		{
			return listBySqlDto(page, sqlCount, sqlState, null,
					new CustomerTransactionListDto());
		}
	}
	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(List<SysCode> statusCodes, String serviceType){
		
		final List<SysCode> servicePlan=new ArrayList<>();
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("planNo", LongType.INSTANCE);
		typeMap.put("planName", StringType.INSTANCE);
		
		if (serviceType.equals(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)){
		
			final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("To Verify","t.newTransNumber", BigInteger.class.getName(),"",1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Order #","t.orderNo", BigInteger.class.getName(),"",2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Enrollment Date","t.effectiveDateToConvert", Date.class.getName(),"",3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Customer Name","t.customerName", String.class.getName(),"",4);
			//AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Member Surname","trans.surname", String.class.getName(),"",3);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Enroll. Status","t.enrollStatus", String.class.getName(),statusCodes,5);
			String sql = "SELECT plan.plan_no as planNo, price.description as planName FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN pos_service_item_price price ON price.item_no = pos.pos_item_no where price.item_catagory='SRV' ";
			getServiceplanDescriptionList(servicePlan, typeMap, sql);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Service Plan","t.description", String.class.getName(),servicePlan,6);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Sales person","t.salesFollowBy", String.class.getName(),"",7);
			//AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Type","t.offerCode", String.class.getName(),"",8);
			AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Amount","t.orderTotalAmount", BigDecimal.class.getName(),"",8);
			AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Balance Due","t.balanceDue", BigDecimal.class.getName(),"",9);
		
			return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8,condition9);
		}
		else if (serviceType.equals(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)) {
			final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("To Verify","t.newTransNumber", BigInteger.class.getName(),"",1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Order #","t.orderNo", BigInteger.class.getName(),"",2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Renewal Date","t.effectiveDateToConvert", Date.class.getName(),"",3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Academy ID","t.academyNo", String.class.getName(),"",4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Customer Name","t.customerName", String.class.getName(),"",5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Order Status","t.orderStatus", String.class.getName(),statusCodes,6);
			String sql = "SELECT plan.plan_no as planNo, price.description as planName FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN service_plan_offer_pos spop ON spop.serv_pos_id = pos.serv_pos_id JOIN pos_service_item_price price ON price.item_no = spop.pos_item_no where price.item_catagory='SRV'  ";
			getServiceplanDescriptionList(servicePlan, typeMap, sql);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Service Plan","t.description", String.class.getName(),servicePlan,7);
			AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Amount","t.orderTotalAmount", BigDecimal.class.getName(),"",8);
			AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Balance Due","t.balanceDue", BigDecimal.class.getName(),"",9);
			
			return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8,condition9);
		}
		else {
			final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Order Date","t.effectiveDateToConvert", Date.class.getName(),"",1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Order Close Date","t.orderUpdateDateToConvert", Date.class.getName(),"",2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Customer Name","t.customerName", String.class.getName(),"",3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Order Status","t.orderStatus", String.class.getName(),statusCodes,4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Enroll Type","t.academyNo", String.class.getName(),"",5);
			String sql = "SELECT plan.plan_no as planNo, price.description as planName FROM service_plan plan JOIN service_plan_pos pos ON pos.plan_no = plan.plan_no JOIN pos_service_item_price price ON price.item_no = pos.pos_item_no where price.item_catagory='SRV' ";
			getServiceplanDescriptionList(servicePlan, typeMap, sql);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Service Plan","t.description", String.class.getName(),servicePlan,6);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Amount","t.orderTotalAmount", BigDecimal.class.getName(),"",7);
			AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Balance Due","t.balanceDue", BigDecimal.class.getName(),"",8);
			
			return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8);
		}
		
	}

	private void getServiceplanDescriptionList(final List<SysCode> servicePlan,
			Map<String, org.hibernate.type.Type> typeMap, String sql) {
		List<ServicePlan> list = this.getDtoBySql(sql, null, ServicePlan.class,typeMap);
		for(ServicePlan s : list){
			SysCode s1=new SysCode();
			s1.setCategory("servicePlan");
			s1.setCodeDisplay(s.getPlanName());
			s1.setCodeValue(s.getPlanName());
			servicePlan.add(s1);
		}
	}
	
//	@Override
//	public ListPage<CustomerOrderHd> getCustomerTransactionRenewalList(
//			ListPage<CustomerOrderHd> page, String balanceStatus,
//			String serviceType, String orderBy, boolean isAscending,
//			String searchText,String filterBySalesMan, String mode, String enrollStatusFilter) {}

	@Override
	public BigDecimal countBalanceDueStatus(Long orderNo) {
		String sql1 = "select orderTotalAmount from CustomerOrderHd c where c.orderNo = "
				+ orderNo;
		String sql2 = "select sum(c.paidAmount) from CustomerOrderTrans c where c.customerOrderHd.orderNo = "
				+ orderNo
				+ " and c.status =  'SUC' "
				+ " group by c.customerOrderHd.orderNo";
		BigDecimal orderTotalAmount = (BigDecimal) getUniqueByHql(sql1);
		BigDecimal orderTotalPaid = (BigDecimal) getUniqueByHql(sql2);
		if (orderTotalAmount != null && orderTotalPaid != null) {
			return orderTotalAmount.subtract(orderTotalPaid);
		}else if (orderTotalAmount != null && orderTotalPaid == null) {
			return orderTotalAmount;
		}
		return BigDecimal.ZERO;
	}

	@Override
	public BigInteger getCustomerID(Long orderNo) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String hql = "select c from CustomerOrderHd c where c.orderNo = "
				+ orderNo;
		List<CustomerOrderHd> pList = super.getByHql(hql);
		CustomerOrderHd customerOrderHd = pList.size() > 0 ? pList.get(0)
				: null;
		return BigInteger.valueOf(customerOrderHd.getCustomerId());
	}
}
