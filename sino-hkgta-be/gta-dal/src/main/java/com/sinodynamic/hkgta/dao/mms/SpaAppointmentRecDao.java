package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.dto.mms.SpaCancelledNotificationInfoDto;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;

public interface SpaAppointmentRecDao extends IBaseDao<SpaAppointmentRec>{

    public Serializable saveSpaAppointmenRec(SpaAppointmentRec sapAppointmenRec);	
	
    public SpaAppointmentRec getBySysId(Long sysId);
    
    public List<SpaAppointmentRec> getSpaAppointmentRecByExtinvoiceNo(String extInvoiceNo);
    
    public int deleteByExtInvoiceNo(String extInvoiceNo);
    
    public void updateSpaAppointmentRec(SpaAppointmentRec spaAppointmentRec);
    
    public List<MmsOrderItemDto> getAppointmentItemsByExtinvoiceNo(String extInvoiceNo);
    
    public Date getLastestSpaSyncTime();
    
    public List<SpaAppointmentRec> getAppointmentRecsByAppointmentId(String appointmentId);
    
    public List<SpaAppointmentRec> getAppointmentRecsByInvoiceNo(String extInvoiceNo);
    
    public List<SpaCancelledNotificationInfoDto> getUserForSpaAppointmentBeginNotification();
    
    public List<SpaCancelledNotificationInfoDto> getUserForSpaAppointmentEndNotification();
	
}
