package com.sinodynamic.hkgta.dao.fms;

import java.util.Date;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.CourseListDto;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import net.sf.jasperreports.engine.JRException;

public interface CourseMasterDao extends IBaseDao<CourseMaster> {

	public CourseListDto getCourseInfo(String courseId);

	/**
	* @param
	* @return void
	* @author Vineela_Jyothi
	* Date: Jul 22, 2015
	*/
	public String getCommonQueryByCourseType(String courseType, String status,
			String expired, String customerId);
	
	public String getCommonQueryByCourseTypeAndroid(String courseType, String status,
		String expired, String customerId);
	
	/**
	 * 
	 * @param courseId
	 * @return
	 * @author Miranda_Zhang
	 * @throws Exception 
	 * @date Aug 12, 2015
	 */
	public Date getCourseSessionStartDate(Long courseId) throws Exception ;
	
	/**
	 * 
	 * @param courseId
	 * @return
	 * @author Miranda_Zhang
	 * @throws Exception 
	 * @date Aug 12, 2015
	 */
	public Date getCourseSessionEndDate(Integer courseId) throws Exception;

	public CourseMaster getCourseMaseterByTypeAndName(String type, String name);

	ListPage<CourseMaster> getMonthlyCourseCompletionReport(ListPage<CourseMaster> page, Integer closedYear, Integer closedMonth, String courseType);

	ListPage<?> getMonthlyCourseRevenueReport(ListPage page, Integer year, Integer month, String facilityType);
}
