package com.sinodynamic.hkgta.dao.rpos;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassDto;

public interface PurchasedDaypassDtoDao extends IBaseDao<PurchasedDaypassDto>{

}
