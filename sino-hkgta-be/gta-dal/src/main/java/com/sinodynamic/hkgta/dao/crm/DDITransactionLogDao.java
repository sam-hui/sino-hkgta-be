package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.account.DDIDto;
import com.sinodynamic.hkgta.entity.crm.DdiTransactionLog;

public interface DDITransactionLogDao extends IBaseDao<DdiTransactionLog>{
	
	public List<DDIDto> queryDDITransactionList(int year, int month);

}
