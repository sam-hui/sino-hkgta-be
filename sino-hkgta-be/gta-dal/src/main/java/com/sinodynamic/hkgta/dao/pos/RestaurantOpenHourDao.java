package com.sinodynamic.hkgta.dao.pos;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantOpenHour;

public interface RestaurantOpenHourDao extends IBaseDao<RestaurantOpenHour>
{
	RestaurantOpenHour getRestaurantOpenHour(String restaurantId);
}
