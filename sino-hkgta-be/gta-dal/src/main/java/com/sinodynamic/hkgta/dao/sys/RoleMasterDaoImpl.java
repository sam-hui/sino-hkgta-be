package com.sinodynamic.hkgta.dao.sys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;

@Repository
public class RoleMasterDaoImpl extends GenericDao<RoleMaster> implements RoleMasterDao{

	@Override
	public List<RoleMaster> getRoleListByUserId(String userId) throws HibernateException {
		// TODO Auto-generated method stub
		String hql = " SELECT new RoleMaster(r.roleId, r.status, r.forUserType, r.roleName) FROM RoleMaster r , UserRole ur WHERE r.roleId = ur.id.roleId AND r.status = 'ACT' AND ur.id.userId = ? ";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userId.trim());
		
		return super.getByHql(hql, paramList);
	}

	@Override
	public boolean chkRoleNameExist(String roleName)
	{
		RoleMaster r = super.getUniqueByCol(RoleMaster.class, "roleName", roleName);
		if(r==null)
			return false;
		else
			return true;
	}

	@Override
	public String getRoleListSQL()
	{
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT * ")
		.append("FROM ( ")
			.append("SELECT r.role_name AS roleName ")
				.append(",r.role_id AS roleId ")
				.append(",r.status AS status ")
				.append(",r.for_user_type AS forUserType ")
				.append(",( ")
					.append("CASE u.user_id ")
						.append("WHEN u.user_id IS NULL ")
							.append("THEN 1 ")
						.append("ELSE 0 ")
						.append("END ")
					.append(") hasBeenUsed ")
			.append("FROM role_master r ")
			.append("LEFT JOIN user_role u ON u.role_id = r.role_id ")
			.append("GROUP BY r.role_id ")
			.append(") tb ")
		.append("WHERE 1 = 1 ");
		
		return sql.toString();
	}

}
