package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.UserPreferenceSettingDto;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;


@SuppressWarnings("rawtypes")
public interface NotificationOnOffSettingDao extends IBaseDao<UserPreferenceSetting>{

	public List<UserPreferenceSettingDto> getNotificationOnOffSetting(Long customerId) throws Exception;

}
