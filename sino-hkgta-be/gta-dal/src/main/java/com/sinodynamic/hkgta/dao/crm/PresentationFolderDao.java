package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;

/**
 * 
 * @author Vineela_Jyothi
 *
 */
public interface PresentationFolderDao extends IBaseDao<ServerFolderMapping>  {
		
	public List<ServerFolderMapping> getTemplateFolderList() throws HibernateException;
	
}
