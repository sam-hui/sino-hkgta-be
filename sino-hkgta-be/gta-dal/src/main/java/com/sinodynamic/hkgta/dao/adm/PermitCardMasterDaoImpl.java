package com.sinodynamic.hkgta.dao.adm;

/*
 * @Author Becky
 * @Date 4-29
*/
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
@Repository
public class PermitCardMasterDaoImpl  extends GenericDao<PermitCardMaster> implements PermitCardMasterDao {
	
	public List<PermitCardMaster> getPermitCardMasterByCustomerId(Long customerId){
		
		String hql = "from PermitCardMaster p where p.mappingCustomerId = " + customerId;
		
		return getByHql(hql);
		
	}

	@Override
	public PermitCardMaster getPermitCardMasterByCardNo(String cardNo)
			throws HibernateException {
		return super.get(PermitCardMaster.class, cardNo);
	}

	@Override
	public boolean updatePermitCardMaster(PermitCardMaster permitCardMaster)
			throws HibernateException {
		// TODO Auto-generated method stub
		return super.update(permitCardMaster);
	}

	@Override
	public boolean linkCardForMember(PermitCardMaster permitCardMaster) throws HibernateException {
		return super.update(permitCardMaster);
	}

	/**
	 * @author Vineela_Jyothi
	 */
	@Override
	public String getCards(String status) {
		
		StringBuilder sql = new StringBuilder(""); 
		
		sql.append("select b.academy_no as academyNo, concat(c.salutation, ' ', c.given_name, ' ', c.surname) as memberName,"
			+ " a.card_no as cardNo, a.card_type_name as cardTypeName, a.card_purpose as cardPurpose, b.customer_id as customerId,"
			+ " a.remark, a.status, a.card_issue_date as cardIssueDate, b.member_type as memberType, m.type_name as memberTypeName,"
			+ " b.superior_member_id as superiorMemberId,"
			+ " a.status_update_date as statusUpdateDate, a.update_by as updateBy"
			+ " from member b"
			+ " left join permit_card_master a ON a.mapping_customer_id = b.customer_id"
			+ ", customer_profile c, member_type m"
			+ " where b.customer_id = c.customer_id"
			+ " and b.status = 'ACT'"
			+ " and b.member_type not in ('HG', 'MG')"
			+ " and c.is_deleted <>'Y'"
			+ " and b.member_type = m.type_code"
			+ " and (a.card_no is null or a.status_update_date = (select max(status_update_date) from permit_card_master pcm"
			+ " where b.customer_id = pcm.mapping_customer_id ))");

		if ("NCD".equalsIgnoreCase(status)) {
			sql.append(" and (a.status is null or a.status <> 'ISS')");
			
		} else if ("ISS".equalsIgnoreCase(status)) {
		    	sql.append(" and (a.status = 'ISS')");
		}
		
		String joinSql = "SELECT * FROM (" + sql.toString() + ") t";
		return joinSql;
		
	}

	
	@Override
	public List<PermitCardMaster> getIssuedCardByCustomerId(Long customerId) {
	    
	    String hql = "from PermitCardMaster p where p.status = 'ISS' and p.mappingCustomerId = " + customerId;
	    return getByHql(hql);
	}

	@Override
	public PermitCardMaster getLatestCustomerOrderPermitCardByCustermerId(
			Long customerId) {
		String hql = "from PermitCardMaster p where p.mappingCustomerId = " + customerId + "order by statusUpdateDate desc";
		List<PermitCardMaster> list = getByHql(hql);
		if(null==list || list.size()<=0){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"PermitCardMaster mappingCustomerId not right:"+customerId});
		}
		return list.get(0);
	}
	
}
