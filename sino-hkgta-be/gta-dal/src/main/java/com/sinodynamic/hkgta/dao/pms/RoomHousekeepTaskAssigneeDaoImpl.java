package com.sinodynamic.hkgta.dao.pms;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTaskAssignee;
@Repository
public class RoomHousekeepTaskAssigneeDaoImpl extends GenericDao<RoomHousekeepTaskAssignee> implements RoomHousekeepTaskAssigneeDao {
	
}
