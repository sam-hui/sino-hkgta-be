package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;


public interface FacilitySubTypeDao extends IBaseDao<FacilitySubType>{

	List<FacilitySubType> getFacilitySubType(String facilityType,String subType);
	
	List<FacilitySubType> getFacilitySubType(String facilityType);

	FacilitySubType getById(
			String facilitySubtypeId);
	
}
