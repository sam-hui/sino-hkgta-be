package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.pms.RoomType;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;

@Repository("guestroomAdvanceSearch")
public class RoomReservationRecDaoImpl extends GenericDao<RoomReservationRec> implements RoomReservationRecDao , AdvanceQueryConditionDao{
	@Autowired
	private RoomTypeDao roomTypeDao;
	@Override
	public List<RoomReservationRec> getAllFailedReservations(int timeout) {
		Date now = new Date();
		List<RoomReservationRec> bookings = new ArrayList<RoomReservationRec>();
		String hql1 = 
				
				"SELECT rec FROM RoomReservationRec AS rec, CustomerOrderTrans AS txn " +
				"WHERE rec.orderNo = txn.customerOrderHd.orderNo AND " + 
				"txn.status IN ('FAIL' , 'VOID') AND " + 
				"rec.status IN ('RSV' , 'PAY') AND " + 
				"TIMESTAMPDIFF(MINUTE, rec.requestDate, ?) >= ?";
		String hql2 = 
				
				"FROM  RoomReservationRec rec " +
				"WHERE " +
				"rec.status IN ('RSV' , 'PAY') AND rec.orderNo IS NULL AND TIMESTAMPDIFF(MINUTE, rec.requestDate, ?) >= ? ";
		
		bookings.addAll(getByHql(hql1, Arrays.<Serializable>asList(now, timeout)));
		bookings.addAll(getByHql(hql2, Arrays.<Serializable>asList(now, timeout)));
		return bookings;
	}

	@Override
	public RoomReservationRec getReservationByConfirmId(String confirmId) {
		RoomReservationRec book = getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
		return book;
	}
	
	public String getGuestroomAdvanceSearch(Map<String, org.hibernate.type.Type> typeMap) {
		typeMap.put("reservationId", StringType.INSTANCE);
		typeMap.put("confirmId", StringType.INSTANCE);
		typeMap.put("academyID", StringType.INSTANCE);
		typeMap.put("patronName", StringType.INSTANCE);
		typeMap.put("arrivalDate", StringType.INSTANCE);
		typeMap.put("departureDate", StringType.INSTANCE);
		typeMap.put("roomTypeCode", StringType.INSTANCE);
		typeMap.put("roomId", StringType.INSTANCE);
		typeMap.put("rateCode", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("totalCharge", LongType.INSTANCE);
		typeMap.put("roomTypeName", StringType.INSTANCE);
		typeMap.put("paymentMethod", StringType.INSTANCE);
		
		StringBuilder sql = new StringBuilder("select *  from (select distinct ").
				append(" r.resv_id reservationId,  ")
				.append(" r.confirm_id  confirmId ,")
				.append("  m.academy_no  academyID, ")
				.append(" CONCAT( c.salutation, ' ', c.given_name, ' ', c.surname ) patronName ,")
				//.append("  date_format(r.arrival_date, '%Y-%m-%d')  arrivalDate , r.arrival_date  arrival_date,  ")
				.append("  date_format(r.arrival_date, '%Y-%m-%d')  arrivalDate , ")
				//.append("  date_format(r.departure_date, '%Y-%m-%d')  departureDate, r.departure_date  departure_date , ")
				.append("  date_format(r.departure_date, '%Y-%m-%d')  departureDate,  ")
				.append(" r.room_type_code roomTypeCode,")
				.append(" r.room_id roomId, ")
				.append("  r.rate_code rateCode, ")
				.append(" trans.payment_method_code  paymentMethod , ")
				.append("  (det.item_total_amout * det.order_qty) totalCharge, ")
				.append(" rt.type_name  roomTypeName, ")
				.append("  CASE WHEN  r.checkout_timestamp is not null  THEN 'CO' WHEN (  r.checkout_timestamp is  null  and   r.checkin_timestamp is not null ) THEN 'CI' ELSE  r.status END as status,  ").
				append(" m.customer_id customerId ").
				append(" from room_reservation_rec r inner join member m on r.customer_id=m.customer_id ")
				.append(" inner join customer_profile c on m.customer_id=c.customer_id ")
				.append(" left join customer_order_trans trans on 	trans.order_no=r.order_no	")
				.append("  Left join customer_order_det det on r.confirm_id = det.ext_ref_no  ")
				.append(" left join room_type rt on rt.type_code=r.room_type_code  ")
				.append("  where  r.status in ('SUC','RFU') ) t "); 
		return sql.toString();
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Confirm.#","confirmId","java.lang.Long","",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Academeny ID","academyID","java.lang.String","",2);
		
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Partron Name","patronName","java.lang.String","",3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Arrival Date","arrivalDate","java.util.Date","",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Departure Date","departureDate","java.util.Date","",5);
		List<RoomType> roomType = roomTypeDao.getByHql("FROM  RoomType ");
				
		Collection<SysCode> roomTypeCode = CollectionUtil.map(roomType, new CallBack<RoomType, SysCode> (){

			@Override
			public SysCode execute(RoomType r, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(r.getTypeCode());
				sysCode.setCodeDisplay(r.getTypeName());
				return sysCode;
			}
			
		});
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Room Type","roomTypeCode","java.lang.String",new ArrayList<SysCode>(roomTypeCode),6);
		
		
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Room #","roomId","java.lang.Long","",7);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Rate Code","rateCode","java.lang.String","",8);
		final List<SysCode> status=new ArrayList<>();
		//SUS-success; IGN-ignore; CAN-cancelled (need refund)
		SysCode s1=new SysCode();
		s1.setCategory("status");
		s1.setCodeDisplay("Ready to Check-in");
		s1.setCodeValue("SUC");
		status.add(s1);
		
		SysCode s2=new SysCode();
		s2.setCategory("status");
		s2.setCodeDisplay("Checked-in");
		s2.setCodeValue("CI");
		status.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Cancelled");
		s3.setCodeValue("CAN");
		status.add(s3);
		
		SysCode s4=new SysCode();
		s4.setCategory("status");
		s4.setCodeDisplay("Checked-out");
		s4.setCodeValue("CO");
		status.add(s4);
		
		SysCode s5=new SysCode();
		s5.setCategory("status");
		s5.setCodeDisplay("Refund");
		s5.setCodeValue("RFU");
		status.add(s5);
		
		AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Status","status","java.lang.String",status,9);
		AdvanceQueryConditionDto condition10 = new AdvanceQueryConditionDto("Total Charge","totalCharge","java.lang.Long","",10);
		AdvanceQueryConditionDto condition11 = new AdvanceQueryConditionDto("Room Name","roomTypeName","java.lang.String",status,11);
		return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8,condition9,condition10,condition11);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		return null;
	}

	

}
