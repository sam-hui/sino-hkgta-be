package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.olap4j.type.DecimalType;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.MemberFacilityAttendanceDto;
import com.sinodynamic.hkgta.dto.crm.TrainingRecordDto;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class MemberFacilityAttendanceDaoImpl extends GenericDao  implements MemberFacilityAttendanceDao{

	@Override
	public List<MemberFacilityAttendance> getMemberFacilityAttendanceList(
			long facilityTimeslotId, int customerId) {
		
		StringBuffer hql = new StringBuffer("from MemberFacilityAttendance f where 1=1 ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		
		if(facilityTimeslotId>0){
			hql.append(" and f.id.facilityTimeslotId=? ");
			paramList.add(facilityTimeslotId);
		}
		if(customerId >0){
			hql.append(" and f.id.customerId=? ");
			paramList.add(customerId);
		}
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public int deleteMemberFacilityAttendanceByFacilityTimeslotId(
			Long facilityTimeslotId) throws HibernateException {
		// TODO Auto-generated method stub
		String sql = " delete from member_facility_attendance where facility_timeslot_id = ? ";
		return super.deleteByHql(sql, facilityTimeslotId);
	}

	@Override
	public MemberFacilityAttendance getAttendaneByResvId(Long resvId) throws HibernateException
	{
		String hql = "FROM MemberFacilityAttendance atte where atte.id.customerId = ? and atte.id.facilityTimeslotId = ? ";
		StringBuffer sql = new StringBuffer();
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("facilityTimeslotId", LongType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		
		sql.append("SELECT atte.facility_timeslot_id as facilityTimeslotId, ")
				.append("atte.customer_id as customerId ")
		.append("FROM member_facility_attendance atte ")
        .append("LEFT JOIN member_reserved_facility res on res.facility_timeslot_id = atte.facility_timeslot_id ")
		.append("WHERE res.resv_id = ?");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(resvId);
		List<MemberFacilityAttendanceDto> attlist = getDtoBySql(sql.toString(), param, MemberFacilityAttendanceDto.class, typeMap);
		
		if(null != attlist && attlist.size()>0)
		{
			MemberFacilityAttendanceDto attendence = attlist.get(0);
			List<Serializable> paramId = new ArrayList<Serializable>();
			paramId.add(attendence.getCustomerId());
			paramId.add(attendence.getFacilityTimeslotId());
			return (MemberFacilityAttendance) getUniqueByHql(hql, paramId);
		}
		return null;
	}
	
	@Override
	public MemberFacilityAttendance getAttendaneById(Long timeslotId, Long customerId) throws HibernateException
	{
		String hql = "FROM MemberFacilityAttendance atte where atte.id.customerId = ? and atte.id.facilityTimeslotId = ? ";

		List<Serializable> paramId = new ArrayList<Serializable>();
		paramId.add(0, customerId);
		paramId.add(1, timeslotId);
		return (MemberFacilityAttendance) getUniqueByHql(hql, paramId);
	}
	
	@Override
	public List<TrainingRecordDto> getTrainingCommentDetail(Long timeslotId, Long customerId){
		StringBuffer sql = new StringBuffer();
		List<Serializable> param = new ArrayList<Serializable>();
		
		sql.append("SELECT atte.facility_timeslot_id as facilityTimeslotId, ")
				.append("atte.customer_id as customerId, ")
				.append("DATE_FORMAT(booking.begin_datetime_book,'%Y/%m/%d %H:%i') AS beginBookTime, ")
				.append("DATE_FORMAT(DATE_ADD(booking.end_datetime_book, INTERVAL 1 SECOND), '%Y/%m/%d %H:%i')AS endBookTime, ")
				.append("concat( sp.given_name,' ', sp.surname) coachName, ")
				.append("atte.trainer_comment AS comment, ")
				.append("atte.score AS score, ")
				.append("atte.trainer_comment_date AS trainerCommentDate, ")
				.append("atte.assignment AS assignment, ")
				.append("sp.portrait_photo AS trainerImagePath ")
				.append("FROM member_facility_attendance atte  ")
				.append("LEFT JOIN member_reserved_facility res on res.facility_timeslot_id = atte.facility_timeslot_id ")
				.append("LEFT JOIN member_facility_type_booking booking on res.resv_id = booking.resv_id ")
				.append("LEFT JOIN staff_profile sp on sp.user_id = booking.exp_coach_user_id ")
				.append("WHERE 1=1 ");
		
		if (null != timeslotId) {
			sql.append("and atte.facility_timeslot_id = ? ");
			param.add(timeslotId);
		}
		
		if (null != customerId) {
			sql.append("and atte.customer_id = ? ");
			param.add(customerId);
		}
		
		return getDtoBySql(sql.toString(), param, TrainingRecordDto.class);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public ListPage<TrainingRecordDto> getTrainingRecordList(ListPage<TrainingRecordDto> page, List<Serializable> param, String countSql, String querySql)
	{
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("facilityTimeslotId", LongType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("attendTime", DateType.INSTANCE);
		typeMap.put("coachName", StringType.INSTANCE);
		typeMap.put("comment", StringType.INSTANCE);
		typeMap.put("score", BigDecimalType.INSTANCE);
		typeMap.put("beginBookTime", StringType.INSTANCE);
		typeMap.put("endBookTime", StringType.INSTANCE);
		typeMap.put("facilityType", StringType.INSTANCE);
		return super.listBySqlDto(page, countSql, querySql, param, new TrainingRecordDto(), typeMap);
	}
}
