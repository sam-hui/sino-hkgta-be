package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.TrainingRecordDto;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface MemberFacilityAttendanceDao extends IBaseDao{

	public List<MemberFacilityAttendance> getMemberFacilityAttendanceList(long facilityTimeslotId,int customerId);
	
	int deleteMemberFacilityAttendanceByFacilityTimeslotId(Long facilityTimeslotId) throws HibernateException;
	
	public MemberFacilityAttendance getAttendaneByResvId(Long resvId) throws HibernateException;
	
	public MemberFacilityAttendance getAttendaneById(Long timeslotId, Long customerId) throws HibernateException;
	
	public ListPage<TrainingRecordDto> getTrainingRecordList(ListPage<TrainingRecordDto> pListPage, List<Serializable> param, String countSql, String querySql);

	public List<TrainingRecordDto> getTrainingCommentDetail(Long timeslotId, Long customerId);
}
