package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;

@Repository
public class CustomerServiceSubscribeDaoImpl extends
		GenericDao<CustomerServiceSubscribe> implements
		CustomerServiceSubscribeDao {

	public void saveServiceSubscribe(
			CustomerServiceSubscribe customerServiceSubscribe) {
		if (customerServiceSubscribe == null)
			throw new HibernateException("CustomerServiceAcc is null!");

		save(customerServiceSubscribe);

	}

	@Override
	public List<CustomerServiceSubscribe> getCustomerServiceSubscribeByAccNo(
			Long accNo) {
		String hqlstr = "from CustomerServiceSubscribe where id.accNo = "+accNo;
		List<CustomerServiceSubscribe> customerServiceSubscribes = getByHql(hqlstr);
		return customerServiceSubscribes;
	}
	
}
