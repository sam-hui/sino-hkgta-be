package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;

public interface MemberPaymentAccDao extends IBaseDao<MemberPaymentAcc>{

	public Serializable addMemberPaymentAcc(MemberPaymentAcc memberPaymentAcc);
	
	public MemberPaymentAcc getMemberPaymentAccByAccountNo(String accNo);
	
	public MemberPaymentAcc getMemberPaymentAccByCustomerId(Long customerId);
}
