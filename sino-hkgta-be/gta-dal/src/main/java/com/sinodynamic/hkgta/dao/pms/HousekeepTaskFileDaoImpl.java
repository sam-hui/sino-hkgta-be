package com.sinodynamic.hkgta.dao.pms;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
@Repository
public class HousekeepTaskFileDaoImpl extends GenericDao<HousekeepTaskFile> implements HousekeepTaskFileDao {
	
}
