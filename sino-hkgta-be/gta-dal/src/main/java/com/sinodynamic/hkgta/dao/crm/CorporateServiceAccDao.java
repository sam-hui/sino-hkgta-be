package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CorporateServiceAccDto;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;

public interface CorporateServiceAccDao extends IBaseDao<CorporateServiceAcc>{

	public Serializable saveCorpSerAcc(CorporateServiceAcc corpSerAcc);
	
	public CorporateServiceAcc getByCorporateId(Long corporateId,String status);
	
	public boolean updateCorpSerAcc(CorporateServiceAcc corporateServiceAcc);
	
	public CorporateServiceAcc getByAccNo(Long accNo);
	
	public Long getLatestAccNoForCorporate(Long corporateId);

	public CorporateServiceAccDto getAactiveByCustomerId(Long corporateId);
	
}
