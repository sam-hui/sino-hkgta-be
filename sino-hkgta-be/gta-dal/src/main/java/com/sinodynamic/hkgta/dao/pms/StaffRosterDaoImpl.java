package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;

@Repository
public class StaffRosterDaoImpl extends GenericDao<StaffRoster> implements StaffRosterDao {

	@Override
	public List<StaffRoster> getStaffRoster(String staffUserId, Date beginDate, Date endDate) {

		StringBuilder hql = new StringBuilder(
				"from StaffRoster s where s.staffUserId = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(staffUserId);
		
		if (beginDate != null) {
			hql.append(" and s.onDate >=? ");
			paramList.add(beginDate);
		}
		
		if (endDate != null) {
			hql.append(" and s.onDate <=? ");
			paramList.add(endDate);
		}
		List<StaffRoster> staffRosterList = super.getByHql(hql.toString(), paramList);
		return staffRosterList;
	}

	@Override
	public List<StaffRoster> getStaffRoster(String staffUserId, Date onDate, Integer beginTime, Integer endTime) {

		StringBuilder hql = new StringBuilder(
				"from StaffRoster s where s.staffUserId = ? ");
		
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(staffUserId);
		if (onDate != null) {
			hql.append(" and s.onDate =? ");
			paramList.add(onDate);
		}
		
		if (beginTime != null) {
			hql.append(" and s.beginTime >=? ");
			paramList.add(beginTime);
		}
		
		if (endTime != null) {
			hql.append(" and s.endTime <=? ");
			paramList.add(endTime);
		}
		
		List<StaffRoster> staffRosterList = super.getByHql(hql.toString(), paramList);
		return staffRosterList;
	}

	@Override
	public List<StaffRoster> getStaffRoster(String staffUserId, Date onDate) {
		return getStaffRoster(staffUserId, onDate, null, null);
	}

}
