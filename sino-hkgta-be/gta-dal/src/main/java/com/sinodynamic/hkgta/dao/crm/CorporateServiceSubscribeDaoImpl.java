package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceSubscribe;

@Repository
public class CorporateServiceSubscribeDaoImpl extends GenericDao<CorporateServiceSubscribe>
		implements CorporateServiceSubscribeDao {

	public Serializable saveCorpSerSub(CorporateServiceSubscribe corpSerSub) {
		return save(corpSerSub);
	}

	public CorporateServiceSubscribe getByAccountNo(Long accountNo) {
		String hqlstr = " from CorporateServiceSubscribe c where c.id.accNo = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(accountNo);
		List<CorporateServiceSubscribe> tempList = getByHql(hqlstr, listParam);
		if(tempList!=null&&tempList.size()>0) {
			return (CorporateServiceSubscribe) tempList.get(0);
		}
		return null;
	}
	
	public CorporateServiceSubscribe getByCorporateId(Long corporateId){
		String hqlstr = " select csc from CorporateServiceSubscribe csc, CorporateServiceAcc csa "
				+ "where csc.id.accNo = csa.accNo and csa.corporateProfile.corporateId = ? and csa.status = 'ACT' ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(corporateId);
		return (CorporateServiceSubscribe) getUniqueByHql(hqlstr, listParam);

	}

}
