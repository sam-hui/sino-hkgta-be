
package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;


/**
 * Email operator interface
 * @author Mianping_Wu
 *
 */
public interface CustomerEmailContentDao extends IBaseDao<CustomerEmailContent>{
	
	public Serializable addCustomerEmail(CustomerEmailContent ca) throws HibernateException;
	
	public boolean updateCustomerEmail(CustomerEmailContent ca) throws HibernateException;
}