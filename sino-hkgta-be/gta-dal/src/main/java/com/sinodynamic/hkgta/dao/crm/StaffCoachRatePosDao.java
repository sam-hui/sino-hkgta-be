package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRatePos;

public interface StaffCoachRatePosDao extends IBaseDao<StaffCoachRatePos>{
	public List<StaffCoachRatePos> getStaffCoachRatePosByUserId(String  userId);
}
