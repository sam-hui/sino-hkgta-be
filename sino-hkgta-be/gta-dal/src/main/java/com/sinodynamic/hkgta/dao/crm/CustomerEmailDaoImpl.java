package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;

@Repository
public class CustomerEmailDaoImpl extends GenericDao<CustomerEmailContent> implements CustomerEmailContentDao {


	public Serializable addCustomerEmail(CustomerEmailContent ca) throws HibernateException {
		return save(ca);
	}

	public boolean updateCustomerEmail(CustomerEmailContent ca) throws HibernateException {
		return saveOrUpdate(ca);
	}

}
