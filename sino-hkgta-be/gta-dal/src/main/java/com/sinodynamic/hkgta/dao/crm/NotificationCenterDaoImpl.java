package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;

@SuppressWarnings("rawtypes")
@Repository
public class NotificationCenterDaoImpl extends GenericDao implements NotificationCenterDao {
	
	Logger logger = Logger.getLogger(ServicePlanDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override

	public List<NotificationMemberDto> getNotificationMemberList(NotificationDto notificationDto) {
		String sql = "<StaffUnionSql> SELECT\n" +
				"	*\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			m.customer_id customerId,\n" +
				"			CONCAT(\n" +
				"				cp.given_name,\n" +
				"				' ',\n" +
				"				cp.surname\n" +
				"			) customerName,\n" +
				"			IF(LENGTH(cp.gender)>0,cp.gender,'U') gender,\n" +
				"			TIMESTAMPDIFF(\n" +
				"				YEAR,\n" +
				"				cp.date_of_birth,\n" +
				"				NOW()\n" +
				"			) age,\n" +
				"			cp.contact_email email,\n" +
				"			cp.phone_mobile phoneMobile,\n" +
				"			CASE m.member_type\n" +
				"		WHEN 'IPM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN customer_service_subscribe css ON sp.plan_no = css.service_plan_no\n" +
				"				LEFT JOIN customer_service_acc csa ON css.acc_no = csa.acc_no\n" +
				"				WHERE\n" +
				"					csa.customer_id = m.customer_id\n" +
				"				AND csa. STATUS = 'ACT'\n" +
				"				AND NOW() BETWEEN csa.effective_date\n" +
				"				AND csa.expiry_date\n" +
				"			)\n" +
				"		WHEN 'CPM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN corporate_service_subscribe corss ON sp.plan_no = corss.service_plan_no\n" +
				"				LEFT JOIN corporate_service_acc corsa ON corss.acc_no = corsa.acc_no\n" +
				"				AND corsa. STATUS = 'ACT'\n" +
				"				AND now() BETWEEN corsa.effective_date\n" +
				"				AND corsa.expiry_date\n" +
				"				LEFT JOIN corporate_profile corp ON corsa.corporate_id = corp.corporate_id\n" +
				"				LEFT JOIN corporate_member corm ON corp.corporate_id = corm.corporate_id\n" +
				"				WHERE\n" +
				"					corm.customer_id = m.customer_id\n" +
				"			)\n" +
				"		WHEN 'IDM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN customer_service_subscribe css ON sp.plan_no = css.service_plan_no\n" +
				"				LEFT JOIN customer_service_acc csa ON css.acc_no = csa.acc_no\n" +
				"				WHERE\n" +
				"					csa.customer_id = m.superior_member_id\n" +
				"				AND csa. STATUS = 'ACT'\n" +
				"				AND NOW() BETWEEN csa.effective_date\n" +
				"				AND csa.expiry_date\n" +
				"			)\n" +
				"		WHEN 'CDM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN corporate_service_subscribe corss ON sp.plan_no = corss.service_plan_no\n" +
				"				LEFT JOIN corporate_service_acc corsa ON corss.acc_no = corsa.acc_no\n" +
				"				AND corsa. STATUS = 'ACT'\n" +
				"				AND NOW() BETWEEN corsa.effective_date\n" +
				"				AND corsa.expiry_date\n" +
				"				LEFT JOIN corporate_profile corp ON corsa.corporate_id = corp.corporate_id\n" +
				"				LEFT JOIN corporate_member corm ON corp.corporate_id = corm.corporate_id\n" +
				"				WHERE\n" +
				"					corm.customer_id = m.superior_member_id\n" +
				"			)\n" +
				"       WHEN m.member_type = 'MG' and m.member_type = 'HG' THEN \n" +
				"                   (SELECT sp.plan_no from service_plan sp  LEFT JOIN customer_order_permit_card copc on sp.plan_no = copc.service_plan_no \n" +
				"                     WHERE copc.cardholder_customer_id = m.customer_id\n" +
				"                     AND copc. STATUS = 'ACT'    \n" +
				"                     AND NOW() BETWEEN copc.effective_from  AND copc.effective_to ) \n" +
				"		ELSE\n" +
				"			NULL\n" +
				"		END AS planNo,\n" +
				"		m.member_type memberType,\n" +
				"		m.`status`,\n" +
				"		CASE m.member_type\n" +
				"	WHEN 'IPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				acc.expiry_date\n" +
				"			FROM\n" +
				"				customer_service_acc acc\n" +
				"			WHERE\n" +
				"				acc.customer_id = m.customer_id\n" +
				"			AND acc. STATUS = 'ACT'\n" +
				"			AND NOW() BETWEEN acc.effective_date\n" +
				"			AND acc.expiry_date\n" +
				"		)\n" +
				"	WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				coracc.expiry_date\n" +
				"			FROM\n" +
				"				corporate_service_acc coracc\n" +
				"			LEFT JOIN corporate_profile cp ON coracc.corporate_id = cp.corporate_id\n" +
				"			LEFT JOIN corporate_member cm ON cp.corporate_id = cm.corporate_id\n" +
				"			WHERE\n" +
				"				cm.customer_id = m.customer_id\n" +
				"			AND coracc. STATUS = 'ACT'\n" +
				"			AND now() BETWEEN coracc.effective_date\n" +
				"			AND coracc.expiry_date\n" +
				"		)\n" +
				"	WHEN 'IDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				acc.expiry_date\n" +
				"			FROM\n" +
				"				customer_service_acc acc\n" +
				"			WHERE\n" +
				"				acc.customer_id = m.superior_member_id\n" +
				"			AND acc. STATUS = 'ACT'\n" +
				"			AND NOW() BETWEEN acc.effective_date\n" +
				"			AND acc.expiry_date\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				coracc.expiry_date\n" +
				"			FROM\n" +
				"				corporate_service_acc coracc\n" +
				"			LEFT JOIN corporate_profile cp ON coracc.corporate_id = cp.corporate_id\n" +
				"			LEFT JOIN corporate_member cm ON cp.corporate_id = cm.corporate_id\n" +
				"			WHERE\n" +
				"				cm.customer_id = m.superior_member_id\n" +
				"			AND coracc. STATUS = 'ACT'\n" +
				"			AND NOW() BETWEEN coracc.effective_date\n" +
				"			AND coracc.expiry_date\n" +
				"		)\n" +
				"   WHEN m.member_type ='MG' and m.member_type = 'HG' THEN \n" +
				"       (SELECT copc.effective_to from customer_order_permit_card copc WHERE copc.cardholder_customer_id = m.customer_id\n" +
				"           AND copc. STATUS = 'ACT'    \n" +
				"           AND NOW() BETWEEN copc.effective_from AND copc.effective_to ) \n" +
				"	ELSE\n" +
				"		NULL\n" +
				"	END AS expiryDate\n" +
				"	FROM\n" +
				"		member m\n" +
				"	LEFT JOIN customer_profile cp ON cp.customer_id = m.customer_id\n" +
				"	) t\n" +
				" WHERE\n" +
				"	1=1 <CheckedDayPass> ";
		
		
		String staffSql = "SELECT * FROM (SELECT\n" +
				"	CONCAT('T', ch.contractor_id) customerId,\n" +
				"	CONCAT(\n" +
				"		ch.given_name,\n" +
				"		' ',\n" +
				"		ch.surname\n" +
				"	) customerName,\n" +
				"	ch.gender,\n" +
				"	NULL AS age,\n" +
				"	ch.contact_email email,\n" +
				"	ch.phone_mobile phoneMobile,\n" +
				"	NULL AS planNo,\n" +
				"	'S' AS memberType,\n" +
				"	ch.`status`,\n" +
				"	ch.period_to AS expiryDate\n" +
				"FROM\n" +
				"	contract_helper ch ) t WHERE 1=1 \n" +
				"UNION";
		
		List<Serializable> param = new ArrayList<Serializable>();
		Boolean withConditon = false;
		if (!notificationDto.getSendToall()) {
			if (null != notificationDto.getMemberType() && notificationDto.getMemberType().length > 0) {
				withConditon = true;
				String tempStr = "";
				for (String memberType : notificationDto.getMemberType()) {
					switch (memberType) {
					case "COR":
						String subCor = "";
						if(notificationDto.getMemberCorType() != null && notificationDto.getMemberCorType().length >0){
							subCor = subCor + " ( ";
							for (String memberCorType : notificationDto.getMemberCorType()) {
								subCor = subCor + " t.memberType = ? or"; 
								param.add(memberCorType);
							}
							subCor = getSqlStr(subCor,"or");
							subCor = subCor + " ) and";
						}
						if(notificationDto.getMemberCorStatus()!=null && notificationDto.getMemberCorStatus().length >0){
							subCor = subCor + "  ( ";
							for (String memberCorStatus : notificationDto.getMemberCorStatus()) {
								subCor = subCor + " t.`status` = ? or";
								param.add(memberCorStatus);
							}
							subCor = getSqlStr(subCor,"or");
							subCor = subCor + " ) and";
						}
						if(subCor.trim().length()>0){
							subCor = getSqlStr(subCor,"and");
							tempStr = tempStr + " ("+subCor+") or ";
						}
						break;
					case "IND":
						String subInd = "";
						if(notificationDto.getMemberIndType()!=null && notificationDto.getMemberIndType().length>0){
							subInd = subInd + " ( ";
							for (String memberIndType : notificationDto.getMemberIndType()) {
								subInd = subInd + " t.memberType = ? or"; 
								param.add(memberIndType);
							}
							subInd = getSqlStr(subInd,"or");
							subInd = subInd + " ) and";
						}
						if(notificationDto.getMemberIndStatus()!=null && notificationDto.getMemberIndStatus().length>0){
							subInd = subInd + "  ( ";
							for (String memberIndStatus : notificationDto.getMemberIndStatus()) {
								subInd = subInd + " t.`status` = ? or";
								param.add(memberIndStatus);
							}
							subInd = getSqlStr(subInd,"or");
							subInd = subInd + " ) and";
						}
						if(subInd.trim().length()>0){
							subInd = getSqlStr(subInd,"and");
							tempStr = tempStr + " ("+subInd+") or ";
						}
						break;
					default:
						break;
					} 
				}
				if(tempStr.trim().length()>0){
					tempStr = getSqlStr(tempStr,"or");
					sql = sql + " and ( " + tempStr +" ) ";
				}
			}
			
			if (null != notificationDto.getServicePlan() && notificationDto.getServicePlan().length > 0) {
				withConditon = true;
				sql = sql + " and ( ";
				for (String servicePlan : notificationDto.getServicePlan()) {
					sql = sql + " t.planNo = ? or";
					param.add(servicePlan);
				}
				sql = getSqlStr(sql,"or");
				sql = sql + " ) ";
			}
		
			if (notificationDto.getDayPass()!=null && notificationDto.getDayPass()) {
				if(!withConditon){
					sql = sql.replace("<CheckedDayPass>"," and (t.memberType = 'MG' or t.memberType = 'HG') ");
					withConditon=true;
				}else{
					sql = sql.replace("<CheckedDayPass>"," ");
				}
			}else{
				sql = sql.replace("<CheckedDayPass>"," and t.memberType != 'MG' and t.memberType != 'HG' ");
			}
			
			if (notificationDto.getTempPass()!=null && notificationDto.getTempPass()) {
				if(!withConditon){
					sql = staffSql.replace("UNION", " ");
				}else{
					sql = sql.replace("<StaffUnionSql>", staffSql);
					sql = "select * from ( "+sql+" ) t where 1=1 ";
				}
				withConditon = true;
			}
			
			if (null != notificationDto.getGender() && notificationDto.getGender().length > 0) {
				withConditon = true;
				sql = sql + " and ( ";
				for (String gender : notificationDto.getGender()) {
					if(StringUtils.isEmpty(gender)){
						sql = sql + " t.gender = 'U' or";
					}else{
						sql = sql + " t.gender = ? or";
						param.add(gender);
					}
				}
				sql = getSqlStr(sql,"or");
				sql = sql + " ) ";
			}
			
			if (null != notificationDto.getAge() && notificationDto.getAge().length == 2 
					&& notificationDto.getAge()[0]!=null && notificationDto.getAge()[1]!=null) {
				withConditon = true;
				sql = sql + " and ( t.age >= ? and t.age <= ? )";
				param.add(notificationDto.getAge()[0]);
				param.add(notificationDto.getAge()[1]);
			}
			
			if (null != notificationDto.getExpireMonth()) {
				withConditon = true;
				if(notificationDto.getExpireMonth() == 0){
					sql = sql + " and ( TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) = ?)";
				}else{
					sql = sql + " and ( TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) < ? and TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) >0 )";
				}
				param.add(notificationDto.getExpireMonth());
			}
		}
		
		List<NotificationMemberDto> dtoList = null;
		if(!notificationDto.getSendToall() && !withConditon){
			return dtoList;
		}
		
		sql = sql.replace("<CheckedDayPass>", "");
		sql = sql.replace("<StaffUnionSql>", "");
		dtoList = this.getDtoBySql(sql, param, NotificationMemberDto.class);
		return dtoList;
	}
	
	private String getSqlStr(String sql,String regx){
		if(sql.trim().endsWith(regx)){
			int corIndex = sql.lastIndexOf(regx);
			sql = sql.substring(0, corIndex);
		}
		return sql;
	}

	@Override
	public void saveSMSRecord(NotificationDto notificationDto) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
