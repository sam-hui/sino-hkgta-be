package com.sinodynamic.hkgta.dao.fms;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;


@Repository("golfCoaching")
public class PrivateCoachBookListDtoDaoImpl extends GenericDao<PrivateCoachBookListDto> implements PrivateCoachBookListDtoDao, AdvanceQueryConditionDao {

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Member ID","memberID","java.lang.String","",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member Name","memberName","java.lang.String","",2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Date","showDate3","java.util.Date","",3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Time","showTime","java.lang.String","",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Bay Type","bayType","java.lang.String","",5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Coach","coach","java.lang.String","",6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Remark","remark","java.lang.String","",7);
		return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPrivateCoachBookList(String status, Integer show,String facilityType, String customerId) {
		StringBuilder listSql = new StringBuilder();
		listSql.append("SELECT * FROM ( ")
				.append("SELECT m.academy_no AS memberID, b.resv_id as resvId, b.exp_coach_user_id as coachId")
				.append(" , CASE WHEN (b.end_datetime_book< now() and b.status<> 'ATN' )then 'OD'  ELSE  b.status END as  status")
				.append("  , b.create_date  as createDate ")
				.append(",CONCAT (cp.salutation , ' ', cp.given_name, ' ', cp.surname ) AS memberName ")
				.append(",CONCAT (date_format(b.begin_datetime_book, '%Y/%m/%d'),' ',date_format(b.begin_datetime_book, '%H:%i') ,'-' ,date_format(b.end_datetime_book + interval 1 second, '%H:%i')) AS showDate2 ")
				.append(",date_format(b.begin_datetime_book, '%Y-%m-%d') AS showDate3 ")
				.append(",CONCAT (date_format(b.begin_datetime_book, '%H:%i') ,'-' ,date_format(b.end_datetime_book, '%H:%i')) AS showTime ")
				.append(",b.resv_facility_type AS facilityType ")
				.append(",case when b.resv_facility_type='TENNIS' then b.facility_subtype_id else a.attribute_id end as facilityAttribute ")
				.append(", CASE  WHEN b.resv_facility_type = 'TENNIS' THEN fst.name  ELSE fa.caption END  as BayType ")
				.append(",CONCAT (sp.given_name ,' ' ,sp.surname) AS coach ")
				.append(",b.customer_remark AS remark ")
				.append(",IF(IFNULL(sub.attendance_status,0) = 0,'Pending','Attended') AS attendanceStatus ")
				.append(",sub1.reserveType AS reserveType ")
				.append("FROM member_facility_type_booking b ")
				.append("LEFT JOIN member_facility_book_addition_attr a ON b.resv_id = a.resv_id ")
				.append("LEFT JOIN member m ON b.customer_id = m.customer_id ")
				.append("LEFT JOIN customer_profile cp ON b.customer_id = cp.customer_id ")
				.append("LEFT JOIN staff_profile sp ON b.exp_coach_user_id = sp.user_id ")
				.append("LEFT JOIN facility_attribute_caption fa ON a.attribute_id = fa.attribute_id  ")
				.append("Left JOIN facility_sub_type fst on b.facility_subtype_id = fst.subtype_id  ")
				//Sub table 1
				.append("LEFT JOIN (SELECT booking.resv_id, SUM(IF(attend.attend_time IS NULL,0,1) ) attendance_status FROM member_facility_type_booking booking ")
				.append("JOIN member_reserved_facility mem_facility ON mem_facility.resv_id = booking.resv_id ")
				.append("JOIN facility_timeslot slot ON slot.facility_timeslot_id = mem_facility.facility_timeslot_id ")
				.append("LEFT JOIN member_facility_attendance attend ON attend.facility_timeslot_id = slot.facility_timeslot_id GROUP BY booking.resv_id) sub ON sub.resv_id = b.resv_id ")
				//Sub table 2
				.append("LEFT JOIN (SELECT DISTINCT booking.resv_id ")
				
				.append(",CASE WHEN booking.exp_coach_user_id IS NOT NULL and booking.order_no IS NOT NULL ")
				.append("THEN 'Private Coaching' ")
				.append("WHEN r.is_bundle='Y' ")
				.append("THEN 'Guest Room Bundle' ")
				.append("WHEN booking.exp_coach_user_id IS NULL and booking.order_no IS NOT NULL ")
				.append("THEN 'Member Booking' ")
				.append("WHEN booking.exp_coach_user_id IS NOT NULL and booking.order_no is NULL ")
				.append("THEN 'Maintenance Offer' ")
				.append("END as reserveType ")
				
				.append("FROM member_facility_type_booking booking ")
				.append("LEFT JOIN member_reserved_facility mem_facility ON mem_facility.resv_id = booking.resv_id ")
				.append("LEFT JOIN facility_timeslot slot ON slot.facility_timeslot_id = mem_facility.facility_timeslot_id ")
				.append("LEFT JOIN room_facility_type_booking r on r.facility_type_resv_id = booking.resv_id) sub1 ON sub1.resv_id = b.resv_id ")
				.append("WHERE b.resv_facility_type = '").append(facilityType).append("'    ");
		if(0==show){
			listSql.append("  and  date_format(b.begin_datetime_book, '%Y-%m-%d')= CURRENT_DATE  ");
		}else if (30==show){
			listSql.append("  and  date_format(b.begin_datetime_book, '%Y-%m-%d')>= CURRENT_DATE  and date_format(b.begin_datetime_book, '%Y-%m-%d') <= CURRENT_DATE+ interval 30 day ");
		}else if(90==show){
			listSql.append("  and  date_format(b.begin_datetime_book, '%Y-%m-%d')>= CURRENT_DATE  and date_format(b.begin_datetime_book, '%Y-%m-%d') <= CURRENT_DATE+ interval 90 day ");
		}
		
		if (!StringUtils.isEmpty(customerId))
		{
			listSql.append(" and b.customer_id = " + customerId + " ");
		}
		listSql.append("AND b.exp_coach_user_id IS NOT NULL ")
				.append("ORDER BY b.create_date ) t ")
				.append("WHERE 1=1 and status<> 'PND'  ")
				.append(" and status<> ")
				.append("'").append(status).append("'    ");
		
		return listSql.toString();
	}
	
}
