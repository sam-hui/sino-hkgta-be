package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.DdInterfaceFileLog;
@Repository
public class DDInterfaceFileLogDaoImpl extends GenericDao<DdInterfaceFileLog> implements DDInterfaceFileLogDao{

}
