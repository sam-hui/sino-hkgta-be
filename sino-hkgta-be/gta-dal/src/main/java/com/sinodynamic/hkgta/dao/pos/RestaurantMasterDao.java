package com.sinodynamic.hkgta.dao.pos;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;

public interface RestaurantMasterDao extends IBaseDao<RestaurantMaster> {

	public List<RestaurantMaster> getRestaurantMasterList();
	
	public RestaurantMaster getRestaurantMaster(String restaurantId);

}
