package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
@Repository
public class PresentationFolderDetailsDaoImpl extends GenericDao<PresentMaterial> implements PresentationFolderDetailsDao {
	
	@Override
	public List<PresentMaterial> getFolderDetails(Long folderId)
			throws HibernateException {
//		String hql = "select materialId, materialFilename, status from PresentMaterial where folderId = ? ";
//		List<Serializable> paramList = new ArrayList<Serializable>();
//		paramList.add(folderId);
		
		return super.getByCol(PresentMaterial.class, "folderId", folderId, null);
		
	}

}
