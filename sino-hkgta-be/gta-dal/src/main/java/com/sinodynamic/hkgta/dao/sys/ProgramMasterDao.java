package com.sinodynamic.hkgta.dao.sys;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;

public interface ProgramMasterDao extends IBaseDao<ProgramMaster>{

}
