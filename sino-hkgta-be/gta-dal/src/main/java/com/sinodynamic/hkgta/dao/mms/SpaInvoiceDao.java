package com.sinodynamic.hkgta.dao.mms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaInvoice;


public interface SpaInvoiceDao extends IBaseDao<SpaInvoice> {
    
    public void saveSpaInvoice(SpaInvoice spaInvoice);
    
    public void truncateSpaInvoice();
    
    public List<SpaInvoice> getSynPaymentInvoiceList();
}
