package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MemberTransactionLog;

public interface MemberTransactionLogDao extends IBaseDao<MemberTransactionLog> {

}
