package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.joda.time.LocalDate;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;

public interface ContractHelperDao extends IBaseDao<ContractHelper>{

	public Serializable createTempPassProfile(ContractHelper profile) throws Exception;
	
	public ContractHelper getTempPassProfileDetails(Long contractorId) throws Exception;
	
	public ContractHelper getTempPassProfileDetailsByHKID(String HKId) throws Exception;
	
	public void updateProfile(ContractHelper profile);
	
	public int getCountByPassType(Long value);

	public List<ContractHelper> getTempPassProfilesExpiringToday(LocalDate date);

}
