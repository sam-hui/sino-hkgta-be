package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("mmsOrderListConditions")
public class MmsOrderConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
	    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Payment Order", "orderNO", "java.lang.String", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member ID", "memberId", "java.lang.String", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Member", "memberName", "java.lang.String", "", 3);
		
		List<SysCode> statusList = new ArrayList<SysCode>();
		SysCode s1=new SysCode();
		s1.setCategory("Status");
		s1.setCodeDisplay("Open");
		s1.setCodeValue("Open");
		statusList.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("Status");
		s2.setCodeDisplay("Cancelled");
		s2.setCodeValue("Cancelled");
		statusList.add(s2);		
		
		SysCode s3=new SysCode();
		s3.setCategory("Status");
		s3.setCodeDisplay("Completed");
		s3.setCodeValue("Completed");
		statusList.add(s3);
		
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Status", "statusValue", "java.lang.String", statusList, 4);
		
		return Arrays.asList(condition1, condition2, condition3,condition4);
	}

	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
	    
	    return null;
	}

}
