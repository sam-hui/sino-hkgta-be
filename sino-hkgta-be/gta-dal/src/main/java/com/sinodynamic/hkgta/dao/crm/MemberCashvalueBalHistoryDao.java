package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.statement.SearchStatementsDto;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface MemberCashvalueBalHistoryDao extends IBaseDao<MemberCashvalueBalHistory> {

	public ListPage<MemberCashvalueBalHistory> getStatements(ListPage page, SearchStatementsDto dto);

}
