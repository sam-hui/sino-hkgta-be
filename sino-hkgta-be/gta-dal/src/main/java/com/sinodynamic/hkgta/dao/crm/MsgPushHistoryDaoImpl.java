package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MsgPushHistory;

@Repository
public class MsgPushHistoryDaoImpl extends GenericDao<MsgPushHistory> implements MsgPushHistoryDao {

    @Override
    public void addMsgPushHistory(MsgPushHistory msgPushHistory) {
	save(msgPushHistory);
    }

    @Override
    public void modifyMsgPushHistory(MsgPushHistory msgPushHistory) {
	update(msgPushHistory);
    }
    
}
