package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CorporateAdditionAddress;

@Repository
public class CorporateAdditionAddressDaoImpl extends GenericDao<CorporateAdditionAddress>
		implements CorporateAdditionAddressDao {

	public Serializable saveCorpAdditionAddress(
			CorporateAdditionAddress corpAdditionAddress) {
		return save(corpAdditionAddress);
	}
	
	public boolean updateCorpAdditionAddress(CorporateAdditionAddress corpAdditionAddress){
		return update(corpAdditionAddress);
	}
	

}
