package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.HkArea;
import com.sinodynamic.hkgta.entity.crm.HkDistrict;

@SuppressWarnings("rawtypes")
public interface HKAreaDistrictDao extends IBaseDao{

	public List<HkArea> getHKArea();
	
	public List<HkDistrict> getHKDistrict();
}
