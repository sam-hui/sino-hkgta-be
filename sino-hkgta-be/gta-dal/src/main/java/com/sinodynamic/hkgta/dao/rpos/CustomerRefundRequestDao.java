package com.sinodynamic.hkgta.dao.rpos;

import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.DaypassRefundDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.dto.rpos.PaymentInfoDto;
import com.sinodynamic.hkgta.dto.rpos.RefundInfoDto;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CustomerRefundRequestDao extends
		IBaseDao<CustomerRefundRequest> ,AdvanceQueryConditionDao{

	/**search data, within how many days
	 * @param base 
	 * @param days 0 = all days , 
	 *             1 = today ,
	 *             2 = 2 days
	 *             3 = 3 days 
	 * @return
	 */
	ListPage<CustomerRefundRequest> getPendingRefundList(Date base, int days,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
	
	ListPage<CustomerRefundRequest> getServiceRefundList(String status,Long customerId,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
	
	ListPage<CustomerRefundRequest> getCashValueRefundList(Date base, Integer days,String status, Long customerId,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
	
	PaymentInfoDto getPaymentInfo(Long txnId);
	
	RefundInfoDto getRefundInfo(Long txnId);
	
	PrivateCoachBookListDto getPrivateCoachBookingDetail(Long refundId, String bookingType);
	
	FacilityReservationDto getFacilityBookingDetail(Long refundId);
	
	CourseEnrollment getEnrollment(Long refundId);

	DaypassRefundDto getDaypassBookingDetail(Long refundId);
	
}
