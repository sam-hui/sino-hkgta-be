package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CorporateProfileDto;
import com.sinodynamic.hkgta.dto.crm.CorporateServiceAccDto;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository
public class CorporateServiceAccDaoImpl extends GenericDao<CorporateServiceAcc>
		implements CorporateServiceAccDao {

	public Serializable saveCorpSerAcc(CorporateServiceAcc corpSerAcc) {
		return save(corpSerAcc);
	}
	
	public CorporateServiceAcc getByCorporateId(Long corporateId,String status){
		String hqlstr = " from CorporateServiceAcc c where c.corporateProfile.corporateId = ? and c.status = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(corporateId);
		listParam.add(status);
		List<CorporateServiceAcc> tempList = getByHql(hqlstr, listParam);
		if(tempList==null) return null;
		return (CorporateServiceAcc) getByHql(hqlstr, listParam).get(0);
	}
	
	public CorporateServiceAcc getByAccNo(Long accNo){
		String hqlstr = " from CorporateServiceAcc c where c.accNo = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(accNo);
		return (CorporateServiceAcc) getUniqueByHql(hqlstr, listParam);
	}
	
	public boolean updateCorpSerAcc(CorporateServiceAcc corporateServiceAcc){
		return update(corporateServiceAcc);
	}
	
	public Long getLatestAccNoForCorporate(Long corporateId){
		String sql = " select max(csa.acc_no) as accNo from corporate_service_acc csa where csa.corporate_id = ? Group By csa.corporate_id";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		List<CorporateProfileDto> dto = getDtoBySql(sql,param,CorporateProfileDto.class);;
		if(dto!=null&&dto.size()>0) return dto.get(0).getAccNo();
		return null;
	}

	@Override
	public CorporateServiceAccDto getAactiveByCustomerId(Long corporateId) {
		StringBuilder sql = new StringBuilder();
		sql.append(" select csa.acc_no  as accNo, csa.corporate_id as corporateId, csa.effective_date as effectiveDate, csa.expiry_date as expiryDate ,csa.period_code as periodCode  ")
			.append(" from corporate_service_acc csa where csa.status=? and csa.corporate_id=? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(Constant.Status.ACT.toString());
		param.add(corporateId);
		 List<CorporateServiceAccDto> list = this.getDtoBySql(sql.toString(), param, CorporateServiceAccDto.class);
		 if(list!=null && list.size()>0){
			 return list.get(0);
		 }
		return null ;
	}
	
}
