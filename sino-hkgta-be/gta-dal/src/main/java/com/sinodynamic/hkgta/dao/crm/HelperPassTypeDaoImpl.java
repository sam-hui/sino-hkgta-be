package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.HelperPassType;
import com.sinodynamic.hkgta.util.pagination.ListPage;


@Repository
public class HelperPassTypeDaoImpl extends GenericDao<HelperPassType> implements HelperPassTypeDao {
	Logger logger = Logger.getLogger(HelperPassTypeDaoImpl.class);

	@Override
	public List<HelperPassType> getAllPassType() throws HibernateException
	{
		String now =  new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		String hql = "FROM HelperPassType h where h.status = 'ACT' and startDate <= '" + now
			+ "' and expiryDate >= '" + now
			+ "' order by typeName asc";
		List<HelperPassType> passTypeList = getByHql(hql.toString());
		
		return passTypeList;
	}

	@Override
	public ListPage<HelperPassType> getPassTypeList(ListPage<HelperPassType> page, String hql, String counthql, List<Serializable> param) throws HibernateException
	{
		return listByHql(page, counthql, hql, param);
	}

	@Override
	public HelperPassType getById(Long id) throws HibernateException
	{
		return super.get(HelperPassType.class, id);
	}
	
	@Override
	public HelperPassType getByName(String tempPassName) throws HibernateException
	{
		StringBuilder hql = new StringBuilder();
		hql.append("FROM HelperPassType h where h.typeName like '%" + tempPassName + "%' order by createDate desc");
		
		return getByHql(hql.toString()).get(0);
	}
	
	@Override
	public boolean deleteById(Long id) throws HibernateException
	{
		return super.deleteById(HelperPassType.class, id);
	}
	
	@Override
	public List<HelperPassType> getExpiryPassTypeByCurdate(String expireDate) {
	    
	    String hql = "from HelperPassType where status = 'ACT' and DATE_FORMAT(expiryDate,'%Y-%m-%d') <= '" + expireDate + "'";
	    return getByHql(hql);
	}

	@Override
	public boolean updatePassType(HelperPassType passType) {
	    
	    return update(passType);
	}
	

}	
