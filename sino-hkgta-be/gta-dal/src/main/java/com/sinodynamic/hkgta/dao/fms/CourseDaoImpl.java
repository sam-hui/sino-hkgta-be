package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.CourseListDto;
import com.sinodynamic.hkgta.dto.fms.CourseSimpleInfoDto;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class CourseDaoImpl extends GenericDao<CourseMaster> implements
		CourseDao {

	@Override
	public ListPage<CourseMaster> getCourseList(ListPage<CourseMaster> page,
			CourseListDto dto) {

		List<Serializable> listParam = new ArrayList<Serializable>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM (SELECT cm.course_id as courseId, cm.course_name as courseName, cm.regist_begin_date as registBeginDate, cm.course_description as courseDescription, "
				+ "cm.regist_due_date as registDueDate, cm.status, cm.capacity, cm.create_date as createDate, cm.member_acceptance as memberAcceptance, "
				+ "(CASE WHEN cm.member_acceptance='A' then (SELECT COUNT(*) FROM course_enrollment ce "
				+ "WHERE ce.course_id = cm.course_id AND ce.status ='REG') when cm.member_acceptance='M' then (SELECT COUNT(*) FROM course_enrollment ce "
				+ "WHERE ce.course_id = cm.course_id AND (ce.status ='REG' or ce.status ='ACT')) else 0 end) AS enrollment, "
				+ "(select min(cs.begin_datetime) from course_session cs where cs.course_id=cm.course_id) as periodStart, "
				+ "(select max(cs.end_datetime) from course_session cs where cs.course_id=cm.course_id) as periodEnd "
				+ "FROM course_master cm WHERE ");

		if (CommUtil.notEmpty(dto.getCourseType())) {
			sql.append("cm.course_type = ? ");
			listParam.add(dto.getCourseType());
		}

		if (!dto.getStatus().equalsIgnoreCase("ALL")) {
			sql.append("AND cm.status = ? ");
			listParam.add(dto.getStatus());
		}


		if (StringUtils.isNotEmpty(dto.getExpired())) {
			if (dto.getExpired().equalsIgnoreCase("N"))
				sql.append(")t WHERE (periodEnd is null OR periodEnd >= current_date()) AND status not like 'CAN' ");
			else if (dto.getExpired().equalsIgnoreCase("Y"))
				sql.append(")t WHERE periodEnd < current_date() OR status like 'CAN' ");
		}

		String countHql = " SELECT count(*)"
				+ sql.substring(sql.indexOf("*") + 1);
		return listBySqlDto(page, countHql, sql.toString(), listParam, dto);
	}

	@Override
	public CourseMaster getCourseMasterById(Long courseId) {
		return courseId == null ? null : get(CourseMaster.class, courseId);
	}

	@Override
	public CourseSimpleInfoDto getCourseInfo(String courseId) {

		String sql = "select distinct cm.course_id as courseId, cm.course_name as courseName, "
				+ "ps.item_price as price, date_format(min(cs.begin_datetime), '%Y/%m/%d') as beginDate, "
				+ "date_format(max(cs.end_datetime), '%Y/%m/%d') as dueDate, "
				+ "count(cs.sys_id) as sessions from course_master cm "
				+ "left join course_session cs on (cm.course_id = cs.course_id and cs.status = 'ACT') "
				+ "left join pos_service_item_price ps on cm.pos_item_no = ps.item_no "
				+ "where cm.course_id = " + courseId;

		List<CourseSimpleInfoDto> result = (List<CourseSimpleInfoDto>) getDtoBySql(
				sql, null, CourseSimpleInfoDto.class);
		return result.size() > 0 ? result.get(0) : null;
	}

	/**
	 * @author Mianping_Wu
	 * @date 7/14/2015
	 * @return
	 */
	@Override
	public void updateCourseMaster(CourseMaster course) {
		update(course);
	}

	@Override
	public List<CourseMaster> getTomorrowCourses(Date begin, Date end) {
	    
	    String sql = "SELECT "
	    	+ "cm.course_id AS courseId, "
	    	+ "cm.course_name AS courseName, "
	    	+ "cm.course_type AS courseType "
	    	+ "FROM course_session cs "
	    	+ "LEFT JOIN course_master cm ON (cs.course_id = cm.course_id) "
	    	+ "WHERE cm.status IN ('OPN' , 'FUL') AND cs.status = 'ACT' "
	    	+ "and cs.begin_datetime between ? and ? "
	    	+ "GROUP BY cs.course_id";
	    	
	    	List<Serializable> params = new ArrayList<Serializable>();
	    	params.add(begin);
	    	params.add(end);
	    
		List<CourseMaster> result = (List<CourseMaster>) getDtoBySql(sql, params, CourseMaster.class);
		return result;
	}

}
