package com.sinodynamic.hkgta.dao.crm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dao.fms.PrivateCoachBookListDtoDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
@Repository("enrollList")
public class CustomerEnrollmentDtoDaoImpl extends  GenericDao<CustomerEnrollmentDto> implements CustomerEnrollmentDtoDao, AdvanceQueryConditionDao {
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Enrollment Date","enrollDateDB","java.util.Date","",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Academy ID","academyNo","java.lang.String","",2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Customer Name","memberName","java.lang.String","",3);

		final List<SysCode> orderStatus=new ArrayList<>();
//		SysCode s4=new SysCode();
//		s4.setCategory("orderStatus");
//		s4.setCodeDisplay("All Status");
//		s4.setCodeValue("");
//		orderStatus.add(s4);

		SysCode s5=new SysCode();
		s5.setCategory("orderStatus");
		s5.setCodeDisplay("Active(No Card)");
		s5.setCodeValue("ANC");
		orderStatus.add(s5);
		
		SysCode s6=new SysCode();
		s6.setCategory("orderStatus");
		s6.setCodeDisplay("Approved");
		s6.setCodeValue("APV");
		orderStatus.add(s6);
		
		SysCode s7=new SysCode();
		s7.setCategory("orderStatus");
		s7.setCodeDisplay("Cancelled");
		s7.setCodeValue("CAN");
		orderStatus.add(s7);

		SysCode s8=new SysCode();
		s8.setCategory("orderStatus");
		s8.setCodeDisplay("Completed");
		s8.setCodeValue("CMP");
		orderStatus.add(s8);
		
		SysCode s9=new SysCode();
		s9.setCategory("orderStatus");
		s9.setCodeDisplay("New");
		s9.setCodeValue("NEW");
		orderStatus.add(s9);
		
		SysCode s10=new SysCode();
		s10.setCategory("orderStatus");
		s10.setCodeDisplay("Open");
		s10.setCodeValue("OPN");
		orderStatus.add(s10);
		
		SysCode s11=new SysCode();
		s11.setCategory("orderStatus");
		s11.setCodeDisplay("Payment Approved");
		s11.setCodeValue("PYA");
		orderStatus.add(s11);

		SysCode s12=new SysCode();
		s12.setCategory("orderStatus");
		s12.setCodeDisplay("Rejected");
		s12.setCodeValue("REJ");
		orderStatus.add(s12);
		
		SysCode s13=new SysCode();
		s13.setCategory("orderStatus");
		s13.setCodeDisplay("To active");
		s13.setCodeValue("TOA");
		orderStatus.add(s13);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Enroll Status","status","java.lang.String",orderStatus,4);
		final List<SysCode> passPeriodType=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("passPeriodType");
		s1.setCodeDisplay("Weekdays Only");
		s1.setCodeValue("WD");
		passPeriodType.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("passPeriodType");
		s2.setCodeDisplay("All Days");
		s2.setCodeValue("FULL");
		passPeriodType.add(s2);
		
//		SysCode s3=new SysCode();
//		s3.setCategory("passPeriodType");
//		s3.setCodeDisplay("No serviceplan");
//		s3.setCodeValue(null);
//		passPeriodType.add(s3);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Member Type","passPeriodType","java.lang.String",passPeriodType,5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Sales Person","salesFollowBy","java.lang.String","",6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("# of Pay Failed","failTransNo","java.math.BigInteger","",7);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Balance Due","balanceDue","java.lang.String","",8);
		return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8);
		//AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Remark","remark","java.lang.String","",9);
		//return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8,condition9);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}
}


