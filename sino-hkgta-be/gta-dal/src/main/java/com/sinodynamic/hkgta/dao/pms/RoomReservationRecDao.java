package com.sinodynamic.hkgta.dao.pms;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.pms.RoomType;

public interface RoomReservationRecDao  extends IBaseDao<RoomReservationRec> {
	List<RoomReservationRec> getAllFailedReservations(int timeout);
	RoomReservationRec getReservationByConfirmId(String confirmId);
	String getGuestroomAdvanceSearch(Map<String, org.hibernate.type.Type> typeMap);
}
