package com.sinodynamic.hkgta.dao.crm;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

@Repository("member_overview")
public class MemberOverviewConditionDaoImpl implements AdvanceQueryConditionDao {

	
	public  List<AdvanceQueryConditionDto> assembleQueryConditions() {
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID","m.academyNo","java.lang.String","",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member","c.givenName","java.lang.String","",2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Member Type","m.memberType","java.lang.String","membertype",3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Status","m.status","java.lang.Integer","memberstatus",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Service Plan","sp.name","java.lang.String","",5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Join Date","m.firstJoinDate","java.util.Date","",6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Expiry Date","csa.expiryDate","java.util.Date","expiredDays",7);
		 return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		// TODO Auto-generated method stub
		return null;
	}


}
