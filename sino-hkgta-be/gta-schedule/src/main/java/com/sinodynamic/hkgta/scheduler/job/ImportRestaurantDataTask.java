package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.ImportRestaurantDataTaskService;

/**
 * <p>
 * sync with oasis restaurant menus and categories
 * </p>
 * @author Mason Yang
 *
 */
public class ImportRestaurantDataTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger logger = LoggerFactory.getLogger(ImportRestaurantDataTask.class);

	private WebApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		try {

			logger.debug("ImportRestaurantDataTask run at:"
					+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			ImportRestaurantDataTaskService importRestaurantDataTaskService = applicationContext
					.getBean(ImportRestaurantDataTaskService.class);

			importRestaurantDataTaskService.importRestaurant();
			logger.info("ImportRestaurantDataTask.checkOnlinePaymentStatus auto execute end ...");

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
