package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderDetService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.constant.Constant;

@Component
public class OnlinePaymentCheckTaskService {
	private Logger logger = Logger.getLogger(OnlinePaymentCheckTaskService.class);

	@Resource(name = "appProperties")
	private Properties appProps;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;

	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private CustomerOrderDetService customerOrderDetService;
	
	@Autowired
	private GuessRoomCheckService guessRoomCheckService;
	
	@Autowired
	private MMSService mmsService;
	/**
	 * This method is used to check whether the online payment is success.
	 */
	public void checkOnlinePaymentStatus() {

		logger.error("OnlinePaymentCheckTask.checkOnlinePaymentStatus() start ..");
		
		List<CustomerOrderTrans> customerOrderTranses = customerOrderTransService.getTimeoutPayments();
		logger.error("result list size from search:" + customerOrderTranses);
		for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
			customerOrderTrans = paymentGatewayService.queryResult(customerOrderTrans);
			customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
			
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			if (null != customerOrderHd)
			{
				CustomerOrderDet customerOrderDet = customerOrderDetService.getCustomerOrderDet(customerOrderHd.getOrderNo());
				logger.error("orderNo:" + customerOrderHd.getOrderNo() + " orderItemNo:" + customerOrderDet.getItemNo());
				if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo()) && customerOrderDet.getItemNo().contains(Constant.FACILITY_PRICE_PREFIX))
				{
					memberFacilityTypeBookingService.voidMemberFacilityTypeBooking(customerOrderTrans.getCustomerOrderHd().getOrderNo(), "system");
				}
				if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo()) && customerOrderDet.getItemNo().startsWith("MMS"))
				{
//					if(logger.isDebugEnabled()){
						logger.error("cancel MMS appointment, invoiceNo=" + customerOrderHd.getVendorRefCode());
//					}
					try {
						mmsService.canCelAppointment(customerOrderHd.getVendorRefCode(),"System");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			
		}
		/**          This is for guess room check          **/
		guessRoomCheckService.checkGuessRoomStatus();
		
		logger.error("OnlinePaymentCheckTask.checkOnlinePaymentStatus() end ..");
	}
	
}
