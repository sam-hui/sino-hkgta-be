package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.crm.account.DDATaskService;



/**
 * <p>
 * process DDA report file:success report and failed report file,
 * and log the key data into DB
 * </p>
 * @author Nick_Xiong
 *
 */
public class DDAReporterTask extends QuartzJobBean implements  ApplicationContextAware{
	private Logger ddxLogger = LoggerFactory.getLogger("ddx");
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
	    
		ddxLogger.debug("DDAReporterTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		DDATaskService ddaTaskService = applicationContext.getBean(DDATaskService.class);

		ddaTaskService.ddaTask();
		
		ddxLogger.debug("DDAReporterTask.ddaTask auto execute end ...");
	}


	

}
