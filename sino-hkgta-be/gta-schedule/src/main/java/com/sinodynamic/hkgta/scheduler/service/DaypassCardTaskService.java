package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderPermitCardService;
@Component
public class DaypassCardTaskService {
	private final Logger logger = LoggerFactory.getLogger(DaypassCardTaskService.class); 
	@Autowired
	private CustomerOrderPermitCardService customerOrderPermitCardService;
	
	@Autowired
	private ServicePlanService servicePlanService;

	public void autoACTDaypassPermitCard() {
		logger.error("DaypassCardTaskService auto update  CustomerOrderPermitCard begin..."+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		customerOrderPermitCardService.updateCustomerOrderPermitCardStatus();
		logger.error("DaypassCardTaskService auto update  CustomerOrderPermitCard end..."+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
	}
	public void autoDisposeDaypassCard() {
		logger.error("DaypassCardTaskService auto  DisposeDaypassCard begin..."+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		servicePlanService.autoDisposeDaypassCard();
		logger.error("DaypassCardTaskService auto  DisposeDaypassCard end..."+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
	}
}
