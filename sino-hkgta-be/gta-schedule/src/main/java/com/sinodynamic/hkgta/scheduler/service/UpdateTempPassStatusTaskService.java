package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.TempPassService;

@Component
public class UpdateTempPassStatusTaskService {
    
    private Logger logger = Logger.getLogger(UpdateTempPassStatusTaskService.class);
    
    @Autowired
    private TempPassService tempPassService;
    
    public void closeCourseOnRegistDueDate() {
	
	logger.error("UpdateTempPassStatusTaskService.tempPassService invocation start ...");
	try {
	    
	    tempPassService.updateStatusForExpiredTempPass();
	    
	} catch(Exception e) {
	    
	    e.printStackTrace();
	    logger.info(e.toString());
	}
	
	logger.error("UpdateTempPassStatusTaskService.tempPassService invocation end ...");
    }
}
