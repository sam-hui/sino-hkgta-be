package com.sinodynamic.hkgta.scheduler.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;

@Component
public class GeneratePatronAccountStatementTaskService {
	private Logger logger = Logger.getLogger(GeneratePatronAccountStatementTaskService.class);
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	public void generatePatronAccountStatement() {
		try {
			customerOrderTransService.generatePatronAccountStatement();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Generate patron account statement failed!");
		}
	}
}
