package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.CourseSessionReminderTaskService;

/**
 * <p>
 * reminder coach and member to join the course in advance 1 hour
 * </p>
 * @author Vicky Wang
 *
 */
public class CourseSessionReminderTask extends QuartzJobBean implements  ApplicationContextAware{
	private final Logger logger = LoggerFactory.getLogger(CourseSessionReminderTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		logger.debug("CourseSessionReminderTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		CourseSessionReminderTaskService courseReminderTaskService = applicationContext.getBean(CourseSessionReminderTaskService.class);

		courseReminderTaskService.sendAppReminderMsg();
		
		logger.info("CourseSessionReminderTask.autoSendingJob auto execute end ...");
	}

}
