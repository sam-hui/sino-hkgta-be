package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

@Deprecated
public class GuessRoomCheckTask extends QuartzJobBean implements ApplicationContextAware{
private final Logger logger = LoggerFactory.getLogger(GuessRoomCheckTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		/**THIS JOB HAS BEEN REMOVED**/
		
		/*logger.info("GuessRoomCheckTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		GuessRoomCheckService guessRoomCheckService = applicationContext.getBean(GuessRoomCheckService.class);

		guessRoomCheckService.checkGuessRoomStatus();
		
		logger.info("guessRoomCheckService.checkGuessRoomStatus auto execute end ...");*/
	}
}
