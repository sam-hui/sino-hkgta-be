package com.sinodynamic.hkgta.scheduler.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.fms.RoomHousekeepStatusLogService;
import com.sinodynamic.hkgta.service.pms.RoomHousekeepTaskService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.response.MessageResult;

@Component
public class AutoUpdateRoomStatusTaskService {
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Autowired
	private RoomHousekeepStatusLogService roomHousekeepStatusLogService;

	@Autowired
	private PMSApiService pMSService;
	
	public void autoUpdateRoomStatus() {
		try {
			List<RoomHousekeepTask> cancelTasks = roomHousekeepTaskService.getRoomHousekeepTaskUnCmpList(RoomHousekeepTaskJobType.RUT.name());
			for (RoomHousekeepTask task : cancelTasks)
			{
				String oldStatus = task.getStatus();
				if(!RoomHousekeepTaskStatus.CAN.name().equals(oldStatus)){
					task.setStatus(RoomHousekeepTaskStatus.CAN.name());
					task.setUpdateDate(new Date());
					roomHousekeepTaskService.update(task);
				}
			}
			
			List<Room> rooms = roomService.getRoomList();
			if(null != rooms && rooms.size() >0){
				for(Room room : rooms){
					UpdateRoomStatusDto statusDto = new UpdateRoomStatusDto();
					statusDto.setRoomNo(room.getRoomNo());
					statusDto.setStatus(RoomStatus.D.toString());
					MessageResult  messageResult = roomService.updateRoomStatus(statusDto);
					if(null != messageResult && messageResult.isSuccess()){
						room.setStatus(RoomStatus.D.toString());
						room.setUpdateDate(new Date());
						roomService.updateRoom(room);
					}
					RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
					roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
					roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.toString());
					roomHousekeepTask.setJobType(RoomHousekeepTaskJobType.RUT.toString());
					roomHousekeepTask.setScheduleDatetime(new Date());
					roomHousekeepTask.setRoom(room);
					roomHousekeepTask.setStatusDate(new Date());
					roomHousekeepTaskService.saveRoomHousekeepTask(roomHousekeepTask);
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return;
		}
	}
	
	private RoomHousekeepStatusLog buildRoomStatusLog(Long taskId, String statusFrom, String statusTo, String changeBy) {
		RoomHousekeepStatusLog log = new RoomHousekeepStatusLog();
		log.setTaskId(taskId);
		log.setStatusFrom(statusFrom);
		log.setStatusTo(statusTo);
		log.setStatusChgBy(changeBy);
		log.setStatusDate(new Date());
		return log;
	}
	
	 public static long getDiffMinutes(Date startDate,Date endDate) {  
         long time1 = startDate.getTime();  
         long time2 = endDate.getTime();  
         long diff;  
         if(time1<time2) {  
             diff = time2 - time1;  
         } else {  
             diff = time1 - time2;  
         }  
         long day = diff / (24 * 60 * 60 * 1000);  
         long hour = (diff / (60 * 60 * 1000) - day * 24);  
         long min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);  
         long sec = (diff/1000-day*24*60*60-hour*60*60-min*60);  
         return day*24*60+hour*60+min+sec/60;  
	 } 
	
}
