package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.mms.SynchronizeSpaData;

/**
 * <p>
 * Synchronize all the mms appointments to hkgta from current date to 14 days later.
 * </p>
 * @author Mianping Wu
 *
 */
public class SpaAppointmentsSynchronizeTask extends QuartzJobBean implements  ApplicationContextAware {

	private final Logger logger = LoggerFactory.getLogger(SpaAppointmentsSynchronizeTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	    this.applicationContext = (WebApplicationContext)applicationContext;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
	    
	    try {
		
		SynchronizeSpaData synchronizeSpaData = applicationContext.getBean(SynchronizeSpaData.class);
		synchronizeSpaData.synchronizeAppointments();
		
	    } catch (Exception e) {
		
		e.printStackTrace();
		logger.error(e.getMessage(), e);
	    }
		
	}
}
