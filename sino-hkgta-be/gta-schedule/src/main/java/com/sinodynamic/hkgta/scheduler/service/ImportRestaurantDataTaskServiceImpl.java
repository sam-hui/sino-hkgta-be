package com.sinodynamic.hkgta.scheduler.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantItemMasterDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMasterDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMenuCatDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMenuItemDao;
import com.sinodynamic.hkgta.dto.pms.FoodItemDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantItemMaster;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.constant.RestaurantMenuItemStatus;

@Component
public class ImportRestaurantDataTaskServiceImpl implements ImportRestaurantDataTaskService {

	private Logger logger = Logger.getLogger(ImportRestaurantDataTaskServiceImpl.class);

	@Autowired
	private PMSApiService pmsApiService;

	@Autowired
	private RestaurantMenuItemDao restaurantMenuItemDao;

	@Autowired
	private RestaurantItemMasterDao restaurantItemMasterDao;
	
	@Autowired
	private RestaurantMasterDao restaurantMasterDao;

	@Autowired
	private RestaurantMenuCatDao restaurantMenuCatDao;

	@Transactional
	public void importRestaurant() throws Exception {
		
			List<RestaurantMaster> restaurantMasterList = restaurantMasterDao.getRestaurantMasterList();

			for (RestaurantMaster restaurantMaster : restaurantMasterList) {

				String restaurantId = restaurantMaster.getRestaurantId();
				List<FoodItemDto> foodItemDtoList = pmsApiService.queryOrdersByOutletId(restaurantId);
				if (foodItemDtoList != null) {

					for (FoodItemDto foodItemDto : foodItemDtoList) {
						String itemNo = foodItemDto.getId();

						String catId = null;
						if (itemNo != null && itemNo.length() > 3) {
							catId = itemNo.substring(0, 3);
						} else {
							logger.error("restaurant category not found:" + itemNo);
							continue;
						}
						

						RestaurantMenuCat restaurantMenuCat = saveRestaurantMenuCat(restaurantMaster, restaurantId, catId);
						saveRestaurantMenuItem(restaurantId, catId, itemNo, foodItemDto, restaurantMenuCat);
						
					}
				}

			}
	}

	private RestaurantMenuCat saveRestaurantMenuCat(RestaurantMaster restaurantMaster, String restaurantId,
			String catId) {
		RestaurantMenuCat restaurantMenuCat = restaurantMenuCatDao.getRestaurantMenuCat(restaurantId, catId);

		if (restaurantMenuCat == null) {
			restaurantMenuCat = new RestaurantMenuCat();
			restaurantMenuCat.setCatId(catId);
			restaurantMenuCat.setRestaurantMaster(restaurantMaster);
			restaurantMenuCat.setCategoryName(catId);
			restaurantMenuCat.setStatus("ACT");
			restaurantMenuCat.setCreateDate(new Date());
		}

		restaurantMenuCatDao.saveOrUpdate(restaurantMenuCat);
		return restaurantMenuCat;
	}
	
	private void saveRestaurantMenuItem(String restaurantId, String catId, String itemNo, FoodItemDto foodItemDto, RestaurantMenuCat restaurantMenuCat) {
		RestaurantMenuItem restaurantMenuItem = restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		
		if (restaurantMenuItem == null) {
			restaurantMenuItem = new RestaurantMenuItem();
		}

		BigDecimal onlinePrice = new BigDecimal(foodItemDto.getPrice());
		
		RestaurantItemMaster restaurantItemMaster = saveRestaurantItemMaster(itemNo, foodItemDto, onlinePrice);
		
		restaurantMenuItem.setRestaurantItemMaster(restaurantItemMaster);
		restaurantMenuItem.setOnlinePrice(onlinePrice);
		
		if (restaurantMenuItem.getCreateDate() == null) {
			restaurantMenuItem.setCreateDate(new Date());
		}
		restaurantMenuItem.setStatus(RestaurantMenuItemStatus.S.toString());
		
		if (restaurantMenuItem.getDisplayOrder() == null) {
			Integer maxDisplayOrder = restaurantMenuItemDao.getMaxDisplayOrder();
			if (maxDisplayOrder == null) {
				maxDisplayOrder = 0;
			}
			restaurantMenuItem.setDisplayOrder(++maxDisplayOrder);
		}

		restaurantMenuItem.setRestaurantMenuCat(restaurantMenuCat);

		restaurantMenuItemDao.saveOrUpdate(restaurantMenuItem);
	}

	private RestaurantItemMaster saveRestaurantItemMaster(String itemNo, FoodItemDto foodItemDto, BigDecimal onlinePrice) {
		RestaurantItemMaster restaurantItemMaster = restaurantItemMasterDao.get(RestaurantItemMaster.class, itemNo);
		if (restaurantItemMaster == null) {
			restaurantItemMaster = new RestaurantItemMaster();
		}
		restaurantItemMaster.setItemNo(itemNo);
		restaurantItemMaster.setItemName(foodItemDto.getDesc());
		
		restaurantItemMaster.setSyncDate(new Date());
		restaurantItemMaster.setDefaultPrice(onlinePrice);
		if (restaurantItemMaster.getCreateDate() == null) {
			restaurantItemMaster.setCreateDate(new Date());
		}
		
		restaurantItemMasterDao.saveOrUpdate(restaurantItemMaster);
		return restaurantItemMaster;
	}
}
