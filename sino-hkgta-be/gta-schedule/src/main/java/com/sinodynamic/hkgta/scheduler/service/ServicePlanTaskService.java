package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
@Component
public class ServicePlanTaskService {
	private Logger logger = Logger.getLogger(ServicePlanTaskService.class);
	@Autowired
	ServicePlanService servicePlanService;
	public void autoDeactivateServicePlanAndDP() {
		try{
			logger.error("ServicePlanTaskService auto Deactivate ServicePlan and daypass begin..."+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			servicePlanService.deactivateServicePlans();
			logger.error("ServicePlanTaskService auto Deactivate ServicePlan and daypass end..."+new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		}catch(Exception e){
			logger.error(e);
		}
	}
}
