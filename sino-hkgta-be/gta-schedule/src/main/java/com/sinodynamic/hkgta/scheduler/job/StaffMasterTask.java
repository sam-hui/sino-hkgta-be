package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;


import com.sinodynamic.hkgta.scheduler.service.StaffMasterTaskService;

/**
 * <p>
 * auto change the staff's status to inactive if the staff's quit data is the last day.
 * </p>
 * @author Zero Wang
 *
 */
public class StaffMasterTask  extends QuartzJobBean implements  ApplicationContextAware{
	

	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		StaffMasterTaskService staffMasterTaskService = applicationContext.getBean(StaffMasterTaskService.class);

		staffMasterTaskService.autoUNACTQuitStaff();
		
	}
}
