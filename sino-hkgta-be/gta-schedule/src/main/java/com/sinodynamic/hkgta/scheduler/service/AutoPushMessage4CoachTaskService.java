package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.fms.TrainerAppService;

@Component
public class AutoPushMessage4CoachTaskService {
private Logger logger = Logger.getLogger(AutoPushMessage4CoachTaskService.class);
    
    @Autowired
    private TrainerAppService trainerAppService;
	public void autoPushMsgInAdvance2Day() {

		logger.info("TrainerAppService.auto push message invocation start ...");
		try {
			logger.error("AutoPushMessage4CoachTaskService.autoPushMsgInAdvance2Day invocation start ...");
			trainerAppService.autoPushMsgInAdvance2Day();
			logger.error("AutoPushMessage4CoachTaskService.autoPushMsgInAdvance2Day invocation end ...");

		} catch (Exception e) {

			e.printStackTrace();
			logger.info(e.toString());
		}

		logger.info("TrainerAppService.auto push message invocation end ...");
	}
   
    
}
