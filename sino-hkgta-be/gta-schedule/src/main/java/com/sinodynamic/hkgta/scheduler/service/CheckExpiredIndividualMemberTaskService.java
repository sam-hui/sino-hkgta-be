package com.sinodynamic.hkgta.scheduler.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;

@Component
public class CheckExpiredIndividualMemberTaskService {
	private Logger logger = Logger.getLogger(CheckExpiredIndividualMemberTaskService.class);
	@Autowired
	private CustomerServiceAccService customerServiceAccService;
	public void updateExpiredIndividualMemberStatus() {
		try {
			customerServiceAccService.handleExpiredCustomerServiceAcc();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Handle expiried CustomerServiceAcc failed!");
		}
	}

}
