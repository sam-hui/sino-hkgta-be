package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.DaypassCardTaskService;

/**
 * <p>
 * Auto disposal the day pass card if the day pass card has expired
 * </p>
 * @author Zero Wang
 *
 */
public class DaypassCardTask  extends QuartzJobBean implements  ApplicationContextAware{
	

	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		DaypassCardTaskService daypassCardTaskService = applicationContext.getBean(DaypassCardTaskService.class);

		daypassCardTaskService.autoACTDaypassPermitCard();
		
		daypassCardTaskService.autoDisposeDaypassCard();
		
	}
}
