package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoPushMessage4CoachTaskService;


/**
 * <p>
 * Auto push message to coach in adavance 2 days
 * </p>
 * @author Zero Wang
 *
 */
public class AutoPushMessage4CoachTask  extends QuartzJobBean implements  ApplicationContextAware{
	

	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		AutoPushMessage4CoachTaskService service = applicationContext.getBean(AutoPushMessage4CoachTaskService.class);

		service.autoPushMsgInAdvance2Day();
		
	}
}
