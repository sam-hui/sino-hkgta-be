package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.crm.membercash.VirtualAccountTaskService;

/**
 * <p>
 * Top up cash value-- by VA/CHEQUE
 * </p>
 * @author Nick_Xiong
 *
 */
public class VirtualAccTopupTask extends QuartzJobBean implements  ApplicationContextAware{
	private Logger ddxLogger = LoggerFactory.getLogger("ddx");	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
	    
		ddxLogger.debug("VirtualAccTopupTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		VirtualAccountTaskService virtualAccTopupTaskService = applicationContext.getBean(VirtualAccountTaskService.class);

		virtualAccTopupTaskService.downloadVirtualAccountTransactionExtract();
		
		ddxLogger.debug("VirtualAccTopupTask.downloadVirtualAccountTransactionExtract auto execute end ...");
	}


	

}
