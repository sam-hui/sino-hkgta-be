package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;

@Component
public class FacilityBookingEndRemindService
{
	@Autowired
	MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private MessageTemplateService messageTemplateService;

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	public void pushMessageNotification(){
		try
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
			List<FacilityReservationDto> facilityReservationDtoList = memberFacilityTypeBookingService.getAllTodayMemberFacilityTypeBooking(true);
			if(null != facilityReservationDtoList && facilityReservationDtoList.size() > 0){
				for(FacilityReservationDto facilityReservationDto : facilityReservationDtoList) {
					long diff = format.parse(facilityReservationDto.getBookingDate() + " " + facilityReservationDto.getEndTime()).getTime() - new Date().getTime();
					long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
					if (diffInMinutes > 0 && diffInMinutes <= 10 ) {
						MessageTemplate messageTemplate;
						if (facilityReservationDto.getResvFacilityType().equalsIgnoreCase("GOLF")){
							messageTemplate = messageTemplateService.getTemplateByFuncId( "golf_end_remind");
						}else{
							messageTemplate = messageTemplateService.getTemplateByFuncId( "tennis_end_remind");
						}
						String content = messageTemplate.getContent();
						String subject = messageTemplate.getMessageSubject();

						String message = content
								.replace("{bookingDate}", facilityReservationDto.getBookingDate())
								.replace("{endTime}", facilityReservationDto.getEndTime())
								.replace("{facilityType}", facilityReservationDto.getResvFacilityType())
								.replace("{facilityTypeQty}",
										Long.toString(facilityReservationDto.getFacilityTypeQty()));

						devicePushService.pushMessage(new String[]{facilityReservationDto.getUserId()}, subject, message, "member");
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*public void sendSMSNotification(){
		try
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
			List<FacilityReservationDto> facilityReservationDtoList = memberFacilityTypeBookingService.getAllTodayMemberFacilityTypeBooking();
			for(FacilityReservationDto facilityReservationDto : facilityReservationDtoList) {
				long diff = format.parse(facilityReservationDto.getBookingDate() + " " + facilityReservationDto.getEndTime()).getTime() - new Date().getTime();
				long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
				if (diffInMinutes > 0 && diffInMinutes <= 10 ) {
					List<String> phonenumbers = new ArrayList<String>();
					CustomerProfile customerProfile = customerProfileService.getByCustomerID(facilityReservationDto.getCustomerId().longValue());
					phonenumbers.add(customerProfile.getPhoneMobile());
					MessageTemplate messageTemplate;
					if (facilityReservationDto.getResvFacilityType().equalsIgnoreCase("GOLF")){
						messageTemplate = messageTemplateService.getTemplateByFuncId( "golf_end_remind");
					}else{
						messageTemplate = messageTemplateService.getTemplateByFuncId( "tennis_end_remind");
					}
					String content = messageTemplate.getContent();
					String message = content.replace("{bookingDate}", facilityReservationDto.getBookingDate()).replace("{startTime}", facilityReservationDto.getStartTime()).replace("{facilityType}", facilityReservationDto.getResvFacilityType()).replace("{facilityTypeQty}", Integer.toString(facilityReservationDto.getFacilityTypeQty()));
					shortMessageService.sendSMS(phonenumbers, message, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}*/
}
