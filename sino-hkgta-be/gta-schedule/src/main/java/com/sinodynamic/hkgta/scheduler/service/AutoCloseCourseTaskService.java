package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.fms.CourseService;

@Component
public class AutoCloseCourseTaskService {
    
    private Logger logger = Logger.getLogger(AutoCloseCourseTaskService.class);
    
    @Autowired
    private CourseService courseService;
    
    public void closeCourseOnRegistDueDate() {
	
	logger.info("AutoCloseCourseTask.closeCourseOnRegistDueDate invocation start ...");
	try {
	    
	    courseService.closeCourseOnDueDate();
	    
	} catch(Exception e) {
	    
	    e.printStackTrace();
	    logger.info(e.toString());
	}
	
	logger.info("AutoCloseCourseTask.closeCourseOnRegistDueDate invocation end ...");
    }
}
