package com.sinodynamic.hkgta.scheduler.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.validation.groups.Default;

import org.apache.regexp.recompile;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.sinodynamic.hkgta.dto.carpark.CarParkMemberCheckDto;
import com.sinodynamic.hkgta.service.carpark.CarParkMemberCheckService;
import com.sinodynamic.hkgta.util.CarkParkMemberUtil;
import com.sinodynamic.hkgta.util.constant.CarParkCSVRowType;
import com.sinodynamic.hkgta.util.constant.CarParkUserType;

@Component
public class DownloadCarParkMemberTaskService {
	
	private Logger logger = Logger.getLogger(DownloadCarParkMemberTaskService.class);
	
	public static DateFormat fileNameDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
	public static boolean Verification_SuccessOrNot = false;
	public static Date currentSendDateTime = null;
	public final static String Default_Error_Code = "999";
	private final static String filePath = "carparkFile.path";
	
	@Autowired
	private CarParkMemberCheckService carParkMemberCheckService;

	public void DownloadCarParkMemberList(){
		
		logger.info("DownloadCarParkMemberTaskService.DownloadCarParkMemberList() Invocation start ...");
		Calendar calendar = Calendar.getInstance();
		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
		int currentMinute = calendar.get(calendar.MINUTE);
		if(currentHour == 1 && currentMinute == 0){
			Verification_SuccessOrNot = false;
		}
		if(Verification_SuccessOrNot){
			logger.debug("DownloadCarParkMemberTaskService.DownloadCarParkMemberList() -- The verification has passed ...");
			return;
		}
		currentSendDateTime = new Date();
		String currentDateTimeStr = formatCurrentDate(currentSendDateTime);
		try {
			String csvFilePath = getFilePath(filePath) + "FULL" + currentDateTimeStr + ".Send.txt";
			String[] header = {"\"" + CarParkCSVRowType.HEADER.getRowType() + "\"", 
							"\"" + currentDateTimeStr + "\"", 
							"\"" + "SEND" + "\""};
			CsvWriter wr =new CsvWriter(csvFilePath,',',Charset.forName("SJIS"));
			wr.setUseTextQualifier(false);
			wr.writeRecord(header);
			
			List<CarParkMemberCheckDto> carParkMemberCheckDtoList = carParkMemberCheckService.getCarParkMemberCheckList();
			List<CarParkMemberCheckDto> carParkStaffCheckDtoList = carParkMemberCheckService.getCarParkStaffCheckList();
			List<CarParkMemberCheckDto> carParkMemberCheckDtos = new ArrayList<CarParkMemberCheckDto>();
			carParkMemberCheckDtos.addAll(carParkMemberCheckDtoList);
			carParkMemberCheckDtos.addAll(carParkStaffCheckDtoList);
			if(null != carParkMemberCheckDtos && carParkMemberCheckDtos.size() != 0){
				for(CarParkMemberCheckDto carParkMemberCheckDto: carParkMemberCheckDtos){
					String newCardNo = "";
					String fullCardNo = "";
					if (carParkMemberCheckDto.getType().intValue() == CarParkUserType.MEMBER.getRole().intValue()){
						newCardNo = CarkParkMemberUtil.formatNumber(8, carParkMemberCheckDto.getCardNo());
						fullCardNo = CarkParkMemberUtil.getCheckDigitByLuhnAlogrithm(newCardNo);
					}
					else if (carParkMemberCheckDto.getType().intValue() == CarParkUserType.STAFF.getRole().intValue()) {
						newCardNo = CarkParkMemberUtil.formatNumber(6, carParkMemberCheckDto.getCardNo());
						fullCardNo ="S" + CarkParkMemberUtil.getCheckDigitByLuhnAlogrithm(newCardNo);
					}
					
					String[] row = {"\"" + CarParkCSVRowType.DETAIL.getRowType() + "\"", 
							"\"" + String.valueOf(carParkMemberCheckDto.getAcademyNo()) + "\"", 
							"\"" + fullCardNo + "\"",
							"\"" + String.valueOf(carParkMemberCheckDto.getCardSerialNo()) + "\"", 
							"\"" + carParkMemberCheckDto.getPersonName() + "\"", 
							"\"" + carParkMemberCheckDto.getType() + "\"", 
							"\"" + (null == carParkMemberCheckDto.getLicencePlateNo()? "" : carParkMemberCheckDto.getLicencePlateNo()) + "\"",
							"\"" + carParkMemberCheckDto.getPhoneMobile() + "\"", 
							"\"" + String.valueOf(getFullExpiryDate(carParkMemberCheckDto.getExpiryDate())) + "\"", 
							"\"" + Default_Error_Code + "\""};
				
					wr.writeRecord(row);
				}
			}
			String[] footer = {"\"" + CarParkCSVRowType.FOOTER.getRowType() + "\"",
							"\"" + String.valueOf(carParkMemberCheckDtoList.size()) + "\""};
			wr.writeRecord(footer);
			wr.close();
			logger.info("DownloadCarParkMemberTaskService.DownloadCarParkMemberList() Invocation end ...");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.debug("DownloadCarParkMemberTaskService.DownloadCarParkMemberList() Invocation --- Unexpected Error: " + e.toString());
		}

	}
	
	public void VerifyReturnCarParkMemberList(){
		
		logger.info("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() Invocation start ...");
		if(Verification_SuccessOrNot){
			logger.debug("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() The verification has passed ...");
			return;
		}
		if(currentSendDateTime == null){
			logger.debug("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() No previous send file was created ...");
			return;
		}		
		//Date currentDateTime = new Date();
		//String currentDateTimeStr = formatCurrentDate(currentDateTime);
		String previousSendTimeStr = formatCurrentDate(currentSendDateTime);
			
		try {
			File csvRoot = new File(getFilePath(filePath));
			File[] csvFiles = csvRoot.listFiles();
			for(File csvFile: csvFiles){
				if(csvFile.getName().length() == 29 
						&& csvFile.getName().substring(4, 16).equals(previousSendTimeStr.substring(0, 12)) 
						&& csvFile.getName().substring(19, 25).toUpperCase().equals("RETURN")){
					ArrayList<String[]> csvList = new ArrayList<String[]>();
					CsvReader csvReader = new CsvReader(csvFile.getAbsolutePath(), ',', Charset.forName("SJIS"));
					
					csvReader.readHeaders();
					
					while(csvReader.readRecord()){
						csvList.add(csvReader.getValues());
					}
					csvReader.close();
					if(csvList.size() > 1){
						for(int row=0; row < csvList.size() - 1; row++){
							if(!csvList.get(row)[8].toString().equals("0")){
								Verification_SuccessOrNot = false;
								logger.debug("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() verification failed ...");
								return;
							}
						}
						Verification_SuccessOrNot = true;
						logger.info("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() verification passed ...");
						//Delete the csv send file
						if(DeleteAllFullDumpSendFile(filePath)){
							logger.info("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() All the created files have been deleted ...");
						}
						break;
					}
				}
			}
			logger.info("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() Invocation end ...");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug("DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() --- Unexpected Error: " + e.toString());
		}
		
	}
	
	public Boolean DeleteAllFullDumpSendFile (String appPath) throws IOException{
		
//		String sendFilePath = getFilePath() + "FULL" + fileName + ".Send.txt";
//		File sendFile = new File(sendFilePath);
//		if(sendFile.delete()){
//			return true;
//		}
//		return false;
		
		String fileDir = getFilePath(appPath);
		File csvFile = new File(fileDir);
		if (!csvFile.exists() || !csvFile.isDirectory()){
			return false;
		}
		File[] csvFileList = csvFile.listFiles();
		for (File file : csvFileList){
			if (file.isFile()){
				file.delete();
			}
		}
		return true;
		
		
	}
	
	public static String formatCurrentDate(Date d){
		
		String currentDate = fileNameDateFormat.format(d);
		return currentDate;
	}
	
	public static String getFullExpiryDate(Date d){
		
//		d.setHours(23);
//		d.setMinutes(59);
//		d.setSeconds(59);
		String expiryDate = df.format(d);
		return expiryDate;
	}
	
	private static String getFilePath(String appPath) throws IOException{
		
		Resource resource = new ClassPathResource("placeholder/app.properties");
		Properties appProps = PropertiesLoaderUtils.loadProperties(resource);
		String path = appProps.getProperty(appPath);
		
		return path;
	}
	
}
