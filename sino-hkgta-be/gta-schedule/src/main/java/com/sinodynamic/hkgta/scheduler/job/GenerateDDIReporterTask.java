package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.crm.account.DDITaskService;



/**
 * <p>
 * Generate the DDI transaction report(Every month run one time on the first day of month)
 * </p> 
 * @author Nick_Xiong
 *
 */
public class GenerateDDIReporterTask extends QuartzJobBean implements  ApplicationContextAware{
	private Logger ddxLogger = LoggerFactory.getLogger("ddx");	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
	    
		ddxLogger.debug("GenerateDDIReporterTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		DDITaskService ddiTaskService = applicationContext.getBean(DDITaskService.class);

		ddiTaskService.generateDDIReport();
		
		ddxLogger.info("GenerateDDIReporterTask.generateDDIReport auto execute end ...");
	}


	

}
