package com.sinodynamic.hkgta.scheduler.job;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.GtaPublicHolidayTaskService;

/**
 * <p>
 * to run every year end, to extract public holidays from hk government and store in public_holiday
 * </p>
 * @author Will Hu
 *
 */
public class GtaPublicHolidayTask extends QuartzJobBean implements ApplicationContextAware{
    
	private final Logger logger = LoggerFactory.getLogger(GtaPublicHolidayTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		logger.info("GatPublicHolidayTask  run at:" + new Date().toString());

		GtaPublicHolidayTaskService service = applicationContext.getBean(GtaPublicHolidayTaskService.class);
		if (!service.execute()){
			service.reTry();
		} 
		
		logger.info("GatPublicHolidayTask end ...");
	}
}
