package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.util.constant.RoomStatus;

@Component
public class SynchronizeRoomStatusTaskService {
	private Logger logger = Logger.getLogger(OnlinePaymentCheckTaskService.class);
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private PMSApiService pMSService;
	
	public void synchronizeRoomStatus()
	{/*
		try
		{
			Map<String,String> statusMap = pMSService.queryHouseKeepingStatus();
			if(null != statusMap && statusMap.size() >0){
				List<Room> rooms = roomService.getRoomList();
				if(null != rooms && rooms.size() > 0){
					for (Room room : rooms)
					{
						String status = statusMap.get(room.getRoomNo());
						if (!StringUtils.isEmpty(status))
						{
							if (status.indexOf("D") > 0)
								room.setStatus(RoomStatus.D.name());
							else if (status.indexOf("R") > 0)
								room.setStatus(RoomStatus.R.name());
							else if (status.indexOf("VC") > 0)
								room.setStatus(RoomStatus.VC.name());
							else if (status.indexOf("OC") > 0)
								room.setStatus(RoomStatus.OC.name());
							room.setUpdateDate(new Date());
							roomService.updateRoom(room);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Synchronize room status failed!");
		}

	*/}
}
