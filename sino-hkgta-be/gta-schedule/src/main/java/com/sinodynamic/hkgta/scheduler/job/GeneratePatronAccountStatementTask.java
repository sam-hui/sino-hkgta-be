package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.GeneratePatronAccountStatementTaskService;

/**
 * <p>
 * Auto generate last month 's Monthly Statement Report at 00:01 am on 1st of every month
 * </p>
 * @author Allen Yu
 *
 */
public class GeneratePatronAccountStatementTask extends QuartzJobBean implements ApplicationContextAware {

private final Logger logger = LoggerFactory.getLogger(GeneratePatronAccountStatementTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
	    
		logger.debug("GeneratePatronAccountStatementTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		GeneratePatronAccountStatementTaskService generatePatronAccountStatementTaskService = applicationContext.getBean(GeneratePatronAccountStatementTaskService.class);

		generatePatronAccountStatementTaskService.generatePatronAccountStatement();
		
		logger.info("CheckExpiredIndividualMemberTaskService.updateExpiredIndividualMemberStatus auto execute end ...");
	}
}
