package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.StartRenewServiceAccountTaskService;

/**
 * <p>
 * Set status to Active(ACT) for Individual Primary Member and his Service 
 * when the Individual Pamary Member's Renew Service effective date is arriving 
 * </p>
 * @author Allen Yu
 *
 */
public class StartRenewServiceAccountTask extends QuartzJobBean implements  ApplicationContextAware{
	private final Logger logger = LoggerFactory.getLogger(StartRenewServiceAccountTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
	    
		logger.debug("StartRenewServiceAccountTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		StartRenewServiceAccountTaskService startRenewServiceAccountTaskService = applicationContext.getBean(StartRenewServiceAccountTaskService.class);

		startRenewServiceAccountTaskService.startRenewServiceAccount();
		
		logger.info("StartRenewServiceAccountTask.startRenewServiceAccount auto execute end ...");
	}

}