package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoUpdateRoomStatusTaskService;

/**
 * <p>
 * Update room status at 0 o'clock everyday
 * </p>
 * @author Annie Xiao
 *
 */
public class AutoUpdateRoomStatusTask extends QuartzJobBean implements  ApplicationContextAware{
	
	private final Logger logger = LoggerFactory.getLogger(AutoUpdateRoomStatusTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
	    
		logger.debug("AutoUpdateRoomStatusTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		AutoUpdateRoomStatusTaskService autoUpdateRoomStatusTaskService = applicationContext.getBean(AutoUpdateRoomStatusTaskService.class);

		autoUpdateRoomStatusTaskService.autoUpdateRoomStatus();
		
		logger.info("AutoUpdateRoomStatusTaskService.autoUpdateRoomStatus auto execute end ...");
	}


	

}
