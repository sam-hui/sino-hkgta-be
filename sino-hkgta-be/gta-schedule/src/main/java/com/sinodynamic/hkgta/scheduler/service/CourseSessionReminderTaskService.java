package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.util.DateCalcUtil;

@Component
public class CourseSessionReminderTaskService {
	
    private Logger logger = Logger.getLogger(CourseSessionReminderTaskService.class);
    
    private static final String APPLICATION_NAME_MEMBER = "member";
    private static final String APPLICATION_NAME_COACH = "trainer";
	    
    @Autowired
    private CourseService courseService;
    
    @Autowired
    private ShortMessageService smsService;
    
    @Autowired
    @Qualifier("asynchronizedPushService")
    private DevicePushService devicePushService;
    
    @Autowired
    private CourseEnrollmentService courseEnrollmentService;
        
    public void sendAppReminderMsg() {
	
	    	logger.error("CourseSessionReminderTaskService.sendAppReminderMsg invocation start ...");
	    	try {
		    
	    	    Date beginTime= new Date();
	    	    Date endTime = DateCalcUtil.GetNextHour(beginTime);
	    	    
//	    	    if(logger.isDebugEnabled())
//	    	    {
	    	    	logger.error("startTime:" + beginTime + " endTime:" + endTime);
//	    	    }
	    	    
	    	    //send app reminder to member
	    	    Map<String, List<UserDevice>> container = courseEnrollmentService.getAppMemberAndReminderMsg(beginTime, endTime, APPLICATION_NAME_MEMBER);
	    	    if (container != null && container.size() > 0) 
	    	    {
	    	    	for (Map.Entry<String, List<UserDevice>> entry : container.entrySet()) {
	    	    		
	    	    		String message =  entry.getKey();
//	    	    		if(logger.isDebugEnabled())
//	    	    		{
	    	    			logger.error("message for member:" + message);
//	    	    		}
	    	    		String[] userIds = getUserIds(entry.getValue());
	    	    		devicePushService.pushMessage(userIds, message, APPLICATION_NAME_MEMBER);
	    	    	}
	    	    }    
	    	    	    		
	    	    //send app reminder to coach
	    		Map<String, Set<String>> coachContainer = courseEnrollmentService.getAppCoachAndReminderMsg(beginTime, endTime, APPLICATION_NAME_COACH);
	    		if(coachContainer !=null && coachContainer.size()>0)
	    		{
	    		   for(Map.Entry<String, Set<String>> coachEntry : coachContainer.entrySet())
	    		   {
	    			   String message = coachEntry.getKey();
	    			   Set<String> coachSet = coachEntry.getValue();
	    			   if(logger.isDebugEnabled())
//	    			   {
	    				   logger.error("message for coach:" + message);
	    				   logger.error("coach user list:" + coachSet.toString());
//	    			   }
	    			   String[] coachUserIds = coachSet.toArray(new String[coachSet.size()]);
	    			   devicePushService.pushMessage(coachUserIds, message, APPLICATION_NAME_COACH);
	    		   }
	    		}
	    	        	    
		    
        	} catch(Exception e) {
        		   e.printStackTrace();
        		   logger.info(e.toString());
        	}
		
	    	logger.error("CourseSessionReminderTaskService.sendAppReminderMsg invocation end ...");
    }
    
    private String[] getUserIds(List<UserDevice> users) {
	
	String[] userIds = new String[users.size()];
	int index = 0;
	if(logger.isDebugEnabled())
	{
		logger.debug("userIds:");
	}
	for (UserDevice user : users) {
		logger.debug(user.getUserId());
	    userIds[index] = user.getUserId();
	    index++;
	}
	return userIds;
    }
    
    

}
