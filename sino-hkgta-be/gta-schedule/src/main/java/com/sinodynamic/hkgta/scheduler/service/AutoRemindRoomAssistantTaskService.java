package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.pms.RoomCrewService;
import com.sinodynamic.hkgta.service.pms.RoomHousekeepTaskService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.RoomCrewRoleType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;

@Component
public class AutoRemindRoomAssistantTaskService {
	private Logger logger = Logger.getLogger(AutoRemindRoomAssistantTaskService.class);

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Autowired
	private RoomCrewService roomCrewService;
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	

	public void autoRemindRoomAssistant() {
		
		logger.info("AutoRemindRoomAssistantTask.autoRemindRoomAssistant auto execute start ...");
		try {
			GlobalParameter  assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
			GlobalParameter  inspector = globalParameterService.getGlobalParameter("HK-MAXRESP-INSPECTOR");
			GlobalParameter  supervisor = globalParameterService.getGlobalParameter("HK-MAXRESP-SUPERVISOR");
			Integer assistantResTime = Integer.parseInt(assistant.getParamValue());
			Integer inspectorResTime = Integer.parseInt(inspector.getParamValue());
			Integer supervisorResTime = Integer.parseInt(supervisor.getParamValue());
			List<RoomHousekeepTaskQueryDto> roomHousekeepTasks = roomHousekeepTaskService.getRemindRoomHousekeepTaskList(assistantResTime,inspectorResTime,supervisorResTime);
			if(null != roomHousekeepTasks && roomHousekeepTasks.size() > 0){
				System.out.println("roomHousekeepTasks.size="+roomHousekeepTasks.size());
				for (RoomHousekeepTaskQueryDto task : roomHousekeepTasks){
					
					MessageTemplate template = messageTemplateService.getTemplateByFuncId(Constant.HOUSEKEEP_PUSHMESSAGE_EXPIRED);
					if(null != template && !StringUtils.isEmpty(template.getContent())){
						String subject = template.getMessageSubject();
						String message = template.getContent().replace("{taskType}",  null == task?"":RoomHousekeepTaskJobType.getDesc(task.getJobType()))
								.replace("{roomNo}", task.getRoomNo())
								.replace("{taskId}", null == task?"":task.getTaskId()+"");
						
						devicePushService.pushMessage(new String[]{task.getStaffUserId()}, subject, message, "housekeeper");
						
						RoomHousekeepTask roomHousekeepTask = roomHousekeepTaskService.getRoomHousekeepTaskById(task.getTaskId().longValue());
						if(task.getCrewRoleId().equals(RoomCrewRoleType.ISP.name())){
							roomHousekeepTask.setAlertSent(1L);
						}else if(task.getCrewRoleId().equals(RoomCrewRoleType.SPV.name())){
							roomHousekeepTask.setAlertSent(2L);
						}else if(task.getCrewRoleId().equals(RoomCrewRoleType.MGR.name())){
							roomHousekeepTask.setAlertSent(3L);
						}
						
						roomHousekeepTask.setLastAlertTimestamp(new Date());
						roomHousekeepTaskService.update(roomHousekeepTask);
						logger.info("-----auto remind userId: "+task.getStaffUserId()+"--"+task.getRoomId()+"--"+task.getJobType()+"--"+task.getTaskStatus()+"--"+task.getTaskId());
					}
					
				}
			}
			
		} catch (Exception e) {
			logger.error("autoRemindRoomAssistant", e);
			e.printStackTrace();
			return;
		}
		logger.info("AutoRemindRoomAssistantTask.autoRemindRoomAssistant auto execute end ...");
	}
}
