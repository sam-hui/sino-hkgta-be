/**
 * 
 * @author Miranda_Zhang
 * @date Sep 30, 2015
 */
package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.util.DateCalcUtil;

/**
 *
 * @author Miranda_Zhang
 * @date Sep 30, 2015
 */
@Component
public class CourseReminderTaskService {
	
    private Logger logger = Logger.getLogger(CourseReminderTaskService.class);
    
    private static final String APPLICATION_NAME = "member";
	    
    @Autowired
    private CourseService courseService;
    
    @Autowired
    private ShortMessageService smsService;
    
    @Autowired
    @Qualifier("asynchronizedPushService")
    private DevicePushService devicePushService;
    
    @Autowired
    private CourseEnrollmentService courseEnrollmentService;
    
    @SuppressWarnings("unchecked")
    public void sendReminderMsg() {
	    logger.info("CourseReminderTask.sendReminderMsg invocation start ...");
		try {
		    
	    	    Date now= new Date();
	    	    Date tomorrow = DateCalcUtil.getNearDay(now, 1);
	    	    Date beginTime = DateCalcUtil.getBeginDateTime(tomorrow);
	    	    Date endTime = DateCalcUtil.getEndDateTime(tomorrow);
	    	    
		    List<Map<String, Object>> senderInfos = courseService.sendReminderMsg(beginTime, endTime);
		    if (senderInfos != null && senderInfos.size() > 0) {
			for (Map<String, Object> smsInfo: senderInfos) {
    				List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
    				String message = (String) smsInfo.get("message");
    				smsService.sendSMS(phoneNumbers, message, now);
			}
		    }
		    
		} catch(Exception e) {
		    
		    e.printStackTrace();
		    logger.info(e.toString());
		}
		
		logger.info("CourseReminderTask.sendReminderMsg invocation end ...");
    }
    
    
    public void sendAppReminderMsg() {
	
	    	logger.info("CourseReminderTask.sendAppReminderMsg invocation start ...");
	    	try {
		    
	    	    Date now= new Date();
	    	    Date tomorrow = DateCalcUtil.getNearDay(now, 1);
	    	    Date beginTime = DateCalcUtil.getBeginDateTime(tomorrow);
	    	    Date endTime = DateCalcUtil.getEndDateTime(tomorrow);
	    	    
	    	    Map<String, List<UserDevice>> container = courseEnrollmentService.getAppMemberAndReminderMsg(beginTime, endTime, APPLICATION_NAME);
	    	    if (container == null || container.size() == 0) return;
	    	    
	    	    for (Map.Entry<String, List<UserDevice>> entry : container.entrySet()) {
	    		
	    		String message =  entry.getKey();
	    		String[] userIds = getUserIds(entry.getValue());
	    		devicePushService.pushMessage(userIds, message, APPLICATION_NAME);
	    	    }
	    	    
		    
        	} catch(Exception e) {
        		   e.printStackTrace();
        		   logger.info(e.toString());
        	}
		
	    	logger.info("CourseReminderTask.sendAppReminderMsg invocation end ...");
    }
    
    private String[] getUserIds(List<UserDevice> users) {
	
	String[] userIds = new String[users.size()];
	int index = 0;
	for (UserDevice user : users) {
	    userIds[index] = user.getUserId();
	    index++;
	}
	return userIds;
    }
    
    

}
