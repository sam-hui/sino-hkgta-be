package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.ServicePlanTaskService;

/**
 * <p>
 * auto change the active status to inactive status according to the deactive date of service plan or day pass
 * </p>
 * @author Zero Wang
 *
 */
public class ServicePlanTask extends QuartzJobBean implements  ApplicationContextAware{
	

	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		ServicePlanTaskService taskService =  applicationContext.getBean(ServicePlanTaskService.class);  
		taskService.autoDeactivateServicePlanAndDP();
	}
}
