package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.mms.MemberSynchronizeService;

@Component
public class MemberSynchronizeTaskService {
    
    private Logger logger = Logger.getLogger(MemberSynchronizeTaskService.class);
    
    @Autowired
    private MemberSynchronizeService memberSynchronizeService;
    
    public void memberSynchronize() {
	
	logger.info("MemberSynchronizeTaskService.memberSynchronize invocation start ...");
	try {
	    
	    memberSynchronizeService.synchronizeGtaMember2Mms();
	    
	} catch(Exception e) {
	    
	    e.printStackTrace();
	    logger.error(e.getMessage(), e);
	}
	
	logger.info("MemberSynchronizeTaskService.memberSynchronize invocation end ...");
    }
}
