package com.sinodynamic.hkgta.notification;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;

@Service
public class SMSServiceImpl implements SMSService {
	final Logger logger = LoggerFactory.getLogger(SMSServiceImpl.class); 
	@Autowired
	@Resource(name="jaxbMarshaller")
	private Jaxb2Marshaller jaxbMarshaller ;
	
	@Value(value="${gateway.url}")
	private String gatewayUrl;
	
	@Value(value ="#{smsProperties['username']}")
	private String username;
	
	@Value(value="${password}")
	private String password;
	
	@Value(value="${senderid}")
	private String senderid;
	
	@Value(value ="#{smsProperties['paramName']}")
	private String paramName;
	
	@Value(value ="#{smsProperties['socketTimeout']}")
	private int socketTimeout;
	
	@Value(value ="#{smsProperties['connectTimeout']}")
	private int connectTimeout;
   
	private  SMSResponse send(String apiUrl,Map<String,String> params) throws Exception {
		
		CloseableHttpClient client = HttpClients.createDefault();
	    try {
	      HttpPost post = new HttpPost(apiUrl);
	      RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(this.socketTimeout).setConnectTimeout(this.connectTimeout).build();//设置请求和传输超时时间
	      post.setConfig(requestConfig);
	      List<NameValuePair> qparams = new ArrayList<NameValuePair>();
	      
	      for(String key: params.keySet()) {
	    	  qparams.add(new BasicNameValuePair(key, params.get(key)));
	      }
	     
	      post.setEntity(new UrlEncodedFormEntity(qparams, "UTF-8"));
	      HttpResponse httpResponse = client.execute(post);
	    
	      String responseString = EntityUtils.toString(httpResponse.getEntity());
	      
	      return xmlToObject(responseString);
	      
	    } catch (Exception e) {
	      throw e;
	    } finally{
	    	client.close();
	    }

	}
	
		
	
	private SMSResponse  xmlToObject(String xmlStr)throws Exception {

		StringReader sr = new StringReader(xmlStr);
		
		StreamSource source = new StreamSource(sr);
		
		SMSResponse smsResponse = (SMSResponse) jaxbMarshaller.unmarshal(source);

		return smsResponse;

	}

	private SMSRequest assembelRequestParam(String userName,String password, List<String> phonenumbers,String content, String sendId,Date sendDateTime) {
		SMSRequest request = new SMSRequest();
		request.setCorrelationid(System.currentTimeMillis() + "");
		request.setUsername(userName);
		request.setPassword(password);
		
		Messages messages = new Messages();
		List<Message> messageList = new ArrayList<Message>();
		for(String phoneNumber :phonenumbers) {
			Message message = new Message();
			
			
			if(null != sendDateTime){
				message.setScheduledatetime(sendDateTime);
			} else {
				message.setScheduledatetime(new Date());
			}
			
			message.setPhonenumbers(phoneNumber);
			message.setContent(content);
			message.setSenderid(sendId);
			messageList.add(message);
		}
		
		messages.setMessages(messageList);
		request.setMessages(messages);
		return request;
	}
	
	@Override
	public SMSResponse sendSMS(List<String> phonenumbers,String content,Date sendDateTime) throws Exception  {
		SMSRequest request = assembelRequestParam(this.username,this.password,phonenumbers,content,this.senderid,sendDateTime);
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		 
		StreamResult result = new StreamResult(pw);
		jaxbMarshaller.marshal(request, result);
	
		Map<String,String> params = new HashMap<String, String>();
		params.put(this.paramName, sw.toString());
		//System.out.println("message=" + sw.toString());
		logger.debug("request data=" + sw.toString());
		SMSResponse response = send(this.gatewayUrl,params);
		return response;
	}

}
