package com.sinodynamic.hkgta.notification;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "sendrequest")
@XmlType(propOrder = { "correlationid", "username", "password","messages"})
public class SMSRequest implements Serializable {
	private static final long serialVersionUID = 5409724800528690778L;

	private String correlationid;

	private String username;

	private String password;

	private Messages messages;

	@XmlElement(name = "correlationid")
	public String getCorrelationid() {
		return correlationid;
	}

	public void setCorrelationid(String correlationid) {
		this.correlationid = correlationid;
	}

	@XmlElement(name = "username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@XmlElement(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@XmlElement(name = "messages")
	public Messages getMessages() {
		return messages;
	}

	public void setMessages(Messages messages) {
		this.messages = messages;
	}

}
