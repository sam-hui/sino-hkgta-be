package com.sinodynamic.hkgta.controller.fms;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.TrainingRecordDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.security.service.UserMasterService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.backoffice.coachmanage.CoachManagementService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.fms.TrainerAppService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class TrainerController extends ControllerBase
{
    private Logger logger = Logger.getLogger(TrainerController.class);

    @Autowired
    private TrainerAppService trainerAppService;
    @Autowired
    private CoachManagementService coachManagementService;
    @Autowired
	MemberFacilityTypeBookingService memberFacilityTypeBookingService;
    @Autowired
	private MessageTemplateService messageTemplateService;
    @Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
    @Autowired
    private UserMasterService userMasterService;
    
    /*@RequestMapping(value = "/coach/students", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult students(@RequestParam(value = "coachId", required = true) String coachId, @RequestParam(value = "studentName", required = false) String studentName) {
        *//***
         * SELECT customer_profile.*, member.academy_no FROM
         * member_facility_type_booking, customer_profile, member WHERE
         * member_facility_type_booking.customer_id =
         * customer_profile.customer_id AND customer_profile.customer_id =
         * member.customer_id member_facility_type_booking.exp_coach_user_id=?
         *//*

        return responseResult;
    }*/

    /*@RequestMapping(value = "/coach/students/trainningRecords", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult studentAttendanceRecords(@RequestParam(value = "coachId", required = true) String coachId) {
        *//***
         * SELECT member_facility_attendance.* FROM
         * member_facility_type_booking, member_reserved_facility,
         * facility_timeslot, member_facility_attendance WHERE
         * member_reserved_facility.resv_id =
         * member_facility_type_booking.resv_id AND
         * member_reserved_facility.facility_timeslot_id =
         * facility_timeslot.facility_timeslot_id AND
         * member_facility_attendance.facility_timeslot_id =
         * facility_timeslot.facility_timeslot_id And
         * member_facility_type_booking.exp_coach_user_id=?
         *//*

        return responseResult;
    }
*/

	@RequestMapping(value = "/training/comment", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult queryStudentTrainningComment(
			@RequestParam(value = "facilityTimeslotId", required = false) String facilityTimeslotId,
			@RequestParam(value = "customerId", required = true) String customerId) {
		try {
			return trainerAppService.getTrainingComments(
					Long.parseLong(facilityTimeslotId),
					Long.parseLong(customerId));
		}catch (GTACommonException gtaException) {
			gtaException.printStackTrace();
			logger.debug("TrainerController.queryStudentTrainningComment");
			responseResult
					.initResult(gtaException.getError());
			return responseResult;
		}catch (Exception e) {
			e.printStackTrace();
			logger.debug("TrainerController.queryStudentTrainningComment");
			responseResult
					.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}


    @RequestMapping(value = "/training/comment", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseResult updateStudentTrainningComment(@RequestParam(value = "customerId", required = true) String customerId, @RequestParam(value = "facilityTimeslotId", required = true) String facilityTimeslotId, @RequestParam(value = "score", required = true) String score, @RequestParam(value = "trainerComment", required = true) String trainerComment, @RequestParam(value = "assignment", required = false) String assignment) throws Exception {
    	responseResult = trainerAppService.updateTrainingComments(Long.parseLong(facilityTimeslotId), Long.parseLong(customerId), Double.parseDouble(score), trainerComment, assignment);
    	if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())){
			
    		Thread pushScoredMsgThread = new Thread(new PushScoredMsgThread(Long.parseLong(facilityTimeslotId),customerId));
    		pushScoredMsgThread.start();
		}
		return responseResult;
    	
    }
    private class PushScoredMsgThread implements Runnable {
		
		private final long facilityTimeslotId;
		private final String customerId;
		public PushScoredMsgThread(long facilityTimeslotId,String customerId) {
			this.facilityTimeslotId = facilityTimeslotId;
			this.customerId = customerId;
		}

		@Override
		public void run() {
			try {
				Long resvId = memberFacilityTypeBookingService.getresvIdByFacilityTimeslotId(facilityTimeslotId, Long.parseLong(customerId));
				MemberFacilityTypeBooking booking = memberFacilityTypeBookingService.getMemberFacilityTypeBooking(resvId);
				/*
				 * Your training record is ready by {username} for {bookingDate} {startTime}
				 */
				MessageTemplate messageTemplate = messageTemplateService.getTemplateByFuncId( "scored_remind");
				String content = messageTemplate.getContent();
				Date begin = new Date(booking.getBeginDatetimeBook().getTime());
				String startTime = Integer.toString(DateCalcUtil.getHourOfDay(begin)) + ":00";
				StaffDto staffDto = userMasterService.getStaffDto(booking.getExpCoachUserId());
				String username ="";
				if(StringUtils.isNotBlank(staffDto.getStaffName())){
					username = staffDto.getStaffName().trim();
				} else if(StringUtils.isNotBlank(staffDto.getNickname())){
					username = staffDto.getNickname().trim();
				}else {
					username = staffDto.getUserId();
				}
				content = content.replaceAll(MessageTemplateParams.UserName.getParam(), username);
				content = content.replaceAll(MessageTemplateParams.BookingDate.getParam(), DateCalcUtil.formatDate(begin))
						.replaceAll(MessageTemplateParams.StartTime.getParam(), startTime);
				logger.error("TrainerController  PushScoredMsgThread push msg to member run start ...resvId:"+resvId);
				devicePushService.pushMessage(new String[]{customerId},content, "member");
				logger.error("TrainerController  PushScoredMsgThread push msg to member run end ...resvId:"+resvId);
			} catch (Exception e) {
				logger.error("error sent email", e);
				throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{"error sent email:",e.getMessage()});
			}
		}
	}
    @RequestMapping(value = "/coaching/rollcall/{resvId}/{qrCode}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseResult rollCallforCoaching(@PathVariable(value="resvId") String resvId, @PathVariable(value="qrCode") String qrCode) throws Exception {
        
    	return trainerAppService.rollCallforCoaching(Long.parseLong(resvId), qrCode);
    }
    
    @RequestMapping(value = "/coaching/hasrollcall/{resvId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseResult hasRollCall(@PathVariable(value="resvId") String resvId) throws Exception {
        
    	return trainerAppService.hasRollCall(Long.parseLong(resvId));
    }
    
    @RequestMapping(value="/coaching/trainingRecord", method=RequestMethod.GET)
    @ResponseBody 
    public ResponseResult getTrainingRecord(
    		@RequestParam (value = "sortBy", defaultValue = "attendTime", required = false) String sortBy,
			@RequestParam (value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam (value = "pageNumber", defaultValue = "1", required = false) int pageNumber,
			@RequestParam (value = "pageSize", defaultValue = "10", required = false) int pageSize,
			@RequestParam (value = "filterBy", defaultValue = "", required = false) String filterBy,
			@RequestParam (value = "customerId", defaultValue = "", required = false) Long customerId,
			@RequestParam (value = "coachId", defaultValue = "", required = false) String coachId) throws Exception {
		
		logger.info("get training record start!");
		
		
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ListPage<TrainingRecordDto> currentPage = trainerAppService.getTrainingRecord(page, customerId, coachId, sortBy, isAscending, filterBy);

		Data records = new Data();
		records.setList(currentPage.getDtoList());
		records.setRecordCount(currentPage.getAllSize());
		records.setCurrentPage( currentPage.getNumber());
		records.setTotalPage(currentPage.getAllPage());
		records.setPageSize(currentPage.getSize());
		records.setLastPage(currentPage.isLast());
		
		responseResult.initResult(GTAError.Success.SUCCESS, records);
		return responseResult;
	}

    /*@RequestMapping(value = "/coach/trainning/monthTrainnings", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult queryMonthTrainningItemsByCoachId(@RequestParam(value = "year", required = true) String year, @RequestParam(value = "month", required = true) String month, @RequestParam(value = "day", required = false) String day, @RequestParam(value = "coachId", required = true) String coachId) {
        *//***
         * --private coaching 
         * SELECT customer_profile.*,
         * member_facility_type_booking.begin_datetime_book,
         * member_facility_type_booking.end_datetime_book,
         * facility_type.description FROM member_facility_type_booking,
         * customer_profile, facility_type WHERE
         * member_facility_type_booking.customer_id =
         * customer_profile.customer_id and
         * member_facility_type_booking.resv_facility_type =
         * facility_type.type_code and
         * member_facility_type_booking.exp_coach_user_id=? 
         * -– course 
         * SELECT
         * sessions.*, cm.course_name FROM ( SELECT course_session.* FROM
         * staff_master, staff_timeslot, course_session WHERE
         * course_session.coach_timeslot_id = staff_timeslot.staff_timeslot_id
         * AND staff_timeslot.staff_user_id = staff_master.user_id AND
         * staff_master.user_id = '[S5645]' ) sessions LEFT JOIN course_master
         * cm ON sessions.course_id = cm.course_id
         *//*

        return responseResult;
    }*/

    /*@RequestMapping(value = "/coach/coachingDetails", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult viewPrivateCoachingDetails(@RequestParam(value = "year", required = true) String year, @RequestParam(value = "month", required = true) String month, @RequestParam(value = "day", required = false) String day, @RequestParam(value = "coachId", required = true) String coachId) {
        return null;
    }
*/
    @RequestMapping(value = "/staffs/coach/scheduleRoster", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult getCoachScheduleRoster(@RequestParam(value = "staffNo", required = true) String staffNo, @RequestParam(value = "fromDate", required = true) String fromDate, @RequestParam(value = "endDate", required = true) String endDate) throws Exception {
    	String formatPattern = "yyyy-MM-dd";
    	Date start = DateUtils.parseDate(fromDate, new String[]{formatPattern});
    	Date end = DateUtils.parseDate(endDate, new String[]{formatPattern});
    	
    	List<CoachRosterInfo> list = coachManagementService.loadMultipleWeekCustomizedRoster(staffNo, start, end);
    	
    	responseResult.initResult(GTAError.Success.SUCCESS, list);
        return responseResult;
    }

    /*@RequestMapping(value = "/staffs/coach/scheduleRoster", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseResult updCoachScheduleRoster(@RequestParam(value = "staffNo", required = true) String staffNo, @RequestParam(value = "ratetype", required = true) String ratetype, @RequestParam(value = "ondate", required = true) String ondate, @RequestParam(value = "begintime", required = true) String begintime, @RequestParam(value = "endtime", required = true) String endtime, @RequestParam(value = "offduty", required = false) String offduty,
            @RequestParam(value = "weekday", required = false) String weekday) {
        CoachScheduleDto coachDto = new CoachScheduleDto();

        *//***
         * // get userId by staff no String userId= select user_id from
         * staff_master where staff_no=:requestStaffNo; // the on_date record is
         * unique for a user,in another way, a usrer should has only one on_date
         * record,in this scenorio, we assume the on_date record is not exsited,
         * otherwise the update operation should be applied in this scenario
         * instead of create operationn. create a HI rate to override the
         * default LO at 7:00am on 2015-6-21, INSERT INTO
         * `hkgta`.`staff_coach_roster` (`coach_user_id`, `rate_type`,
         * `on_date`, `begin_time`, `end_time`) VALUES ('[S001]', 'HI',
         * '2015-6-21', 700, 759);
         * 
         * // if staff_coach_roster is LO, it may update it as HI` and vice
         * versa update `hkgta`.`staff_coach_roster` set rate_type='HI' where
         * coach_user_id=:userId and on-date =:reqDate and
         * begin_time=:reqBeginTime and end_time=:reqEndTime;
         * 
         * //for day off requirment,if the record of roaster table is LO or HI,
         * update `hkgta`.`staff_coach_roster` set
         * rate_type=NULL,week_day=NULL,off_duty='Y' where coach_user_id=:userId
         * and on-date =:reqDate and begin_time=:reqBeginTime and
         * end_time=:reqEndTime; /***********end for day off requirment
         *************//*

        return responseResult;
    }*/

    

    /*@RequestMapping(value = "/staffs/coach/newemail", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult getCoachNewMsgNum(@RequestParam(value = "staffNo", required = true) String staffNo) {
        CoachNewMessageDto coachMsgNumDto = new CoachNewMessageDto();

        
         * 站内信，notice_recipient,notice // get userId by staff no String userId=
         * select user_id from staff_master where staff_no=:requestStaffNo; //
         * get the amount of unread message select count(*) from
         * notice_recipient where recipient_user_id=:userId and status='unread'
         * and 1=1;
         

        return responseResult;
    }*/

    /*@RequestMapping(value = "/staffs/coach/email", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult getCoachEmail(@RequestParam(value = "staffNo", required = true) String staffNo) {
        CoachMsgLstDto coachMsgNumDto = new CoachMsgLstDto();

        
         * // get userId by staff no String userId= select user_id from
         * staff_master where staff_no=:requestStaffNo; //get the message list
         * select nt.* ,nr.* from notice_recipient nr, notice nt where
         * nr.recipient_user_id=:userId and nt.notice_id=nr.notice_id;
         

        return responseResult;
    }*/

    @RequestMapping(value = "/staffs/coach/achivement/month", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult getCoachMonthyAchivement(@RequestParam(value = "staffId", required = true) String staffId, @RequestParam(value = "requestMonth", required = true) String requestMonth) throws ParseException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("coachId", staffId);
        params.put("requestMonth", requestMonth);
        
        String pattern = "yyyy-MM";
        Date start = DateUtils.parseDate(requestMonth, new String[]{pattern});
        Date end = new DateTime(start).dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue().toLocalDateTime().toDate();
        
        params.put("start", start);
        params.put("end", end);
    	responseResult.initResult(GTAError.Success.SUCCESS, this.trainerAppService.calculateCoachAchievement(params));
        

        return responseResult;
    }

    /**
     * 
     * @param trainningType:Course, Coaching
     * @param id: the PK
     * @return
     */
   /* @RequestMapping(value = "/coach/trainningDetails", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult trainningDetails(@RequestParam(value = "trainningType") String trainningType,@RequestParam(value = "id") String id) {

        – for Coaching,第一张图,第三张图
        SELECT
        member_facility_type_booking.*, customer_profile.gender,
        customer_profile.given_name,
        facility_type.description,
        facility_attribute_caption.caption,
        facility_master.facility_name – 球道
        FROM
        member_facility_type_booking,
        customer_profile,
        facility_type,
        member_facility_book_addition_attr,
        facility_attribute_caption,
        facility_addition_attribute,
        facility_master
        WHERE
        member_facility_type_booking.customer_id = customer_profile.customer_id
        AND member_facility_type_booking.resv_facility_type = facility_type.type_code
        AND member_facility_type_booking.resv_id = member_facility_book_addition_attr.resv_id
        AND facility_attribute_caption.attribute_id = member_facility_book_addition_attr.attribute_id
        AND facility_attribute_caption.attribute_id = facility_addition_attribute.attribute_id
        AND facility_master.facility_no = facility_addition_attribute.facility_no
        AND member_facility_type_booking.resv_id =?
        – for course 第二张第四张图 ,对应story：View course detail including title, price, course type, participants and description.
        SELECT
        sessions.*, 
        (select count(1) from course_session where course_session.course_id= sessions.course_id) sessionCount,
        cm.course_name
        FROM
        (
        SELECT
        course_session.*
        FROM
        staff_master,
        staff_timeslot,
        course_session
        WHERE
        course_session.coach_timeslot_id = staff_timeslot.staff_timeslot_id
        AND staff_timeslot.staff_user_id = staff_master.user_id
        AND staff_master.user_id = '[S5645]'
        ) sessions
        LEFT JOIN course_master cm ON sessions.course_id = cm.course_id

        return responseResult;
    }*/
    

	@RequestMapping(value = "/staffs/coach/reservation/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoachReservationLst(@RequestParam(value = "staffNo",required = true) String staffNo) 
	{
		responseResult.initResult(GTAError.Success.SUCCESS, trainerAppService.getReservationList(staffNo, "beginTime", "asc", 20, 1, null).getList());
		return responseResult;
	}
	
	@RequestMapping(value = "/staffs/coach/reservation/listByDate/", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoachReservationLstByDate(@RequestParam(value = "staffNo",required = true) String staffNo, @RequestParam(value = "resvDate", required = true) String resvDate,@RequestParam (value = "pageNumber", defaultValue = "1", required = false) int pageNumber,
			@RequestParam (value = "pageSize", defaultValue = "1000", required = false) int pageSize) 
	{
		responseResult.initResult(GTAError.Success.SUCCESS, trainerAppService.getReservationListByDate(staffNo, "beginTime", "asc", pageSize, pageNumber, resvDate).getList());
		return responseResult;
	}
	
	@RequestMapping(value = "/staffs/coach/train/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPrivateCoachTrainedMemberListBy(@RequestParam(value = "staffNo",required = true) String staffNo, @RequestParam(value="isAscending",defaultValue="", required = false) String isAscending){
		List<CustomerProfileDto>  list = trainerAppService.getPrivateCoachTrainedMemberList(staffNo, isAscending);
		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	@RequestMapping(value = "/staffs/coach/train/findByName", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPrivateCoachTrainedMemberList(@RequestParam(value = "staffNo",required = true) String staffNo,@RequestParam(value = "name",required = true) String name, @RequestParam(value="isAscending",defaultValue="", required = false) String isAscending){
		List<CustomerProfileDto>  list = trainerAppService.getPrivateCoachTrainedMemberListByName(staffNo,name,isAscending);
		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	
}
