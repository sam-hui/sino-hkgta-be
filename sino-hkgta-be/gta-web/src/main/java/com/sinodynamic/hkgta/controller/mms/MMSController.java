package com.sinodynamic.hkgta.controller.mms;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import scala.actors.threadpool.Arrays;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SettleMMSPaymentByCashValueResponseDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryResponseDto;
import com.sinodynamic.hkgta.dto.mms.SpaCenterInfoDto;
import com.sinodynamic.hkgta.dto.mms.SpaPicPathDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatItemDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistRequestType;
import com.sinodynamic.hkgta.integration.spa.response.Therapist;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.mms.SpaAppointmentRecService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.AdvancePeriodType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping(value="/mmsprocessor")
@Scope("prototype")
public class MMSController extends ControllerBase{

	private Logger logger = Logger.getLogger(MMSController.class);
	
	@Autowired
	private MMSService mmsService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private SpaAppointmentRecService spaAppointmentRecService;
	  
	  
	
	@ResponseBody 
	@RequestMapping(value="/queryTopCategories",method=RequestMethod.GET)
	public ResponseResult queryTopCategories() throws Exception {
		
		try {
			return mmsService.queryTopCategories();
		}catch(Exception e){
			
			logger.error(MMSController.class.getName() + " queryTopCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;		
		
		}
	}
	
	@ResponseBody 
	@RequestMapping(value="/getLocalTopCategories",method=RequestMethod.GET)
	public ResponseResult getLocalTopCategories(@RequestParam(value = "device", required=false) String device) throws Exception {
		
		try {
			if(device == null || device.length()<=0)
				device = "WP";
			return mmsService.getLocalTopCategory(device);
			
		}catch(Exception e){
			
			logger.error(MMSController.class.getName() + " getLocalTopCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;		
		
		}
	}
	
	
	@ResponseBody 
	@RequestMapping(value="/querySubCategories/{categoryId}",method=RequestMethod.GET)
	public ResponseResult querySubCategories(@PathVariable("categoryId") String categoryId) throws Exception {
		
		try {
			if(StringUtils.isEmpty(categoryId))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "categoryId cannot be null.");				
			}else
			{
			  responseResult =  mmsService.getSubcategories(categoryId);
			}
			
		}catch(Exception e){

			logger.error(MMSController.class.getName() + " querySubCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());				
		}
		
		return responseResult;	
	}
	
	@ResponseBody 
	@RequestMapping(value="/getCenterTherapists",method=RequestMethod.GET)
	public ResponseResult getCenterTherapists(Date requestDate) throws Exception {
		
		try {
			if(requestDate ==null)
				requestDate = new Date();
			return mmsService.getCenterTherapists(requestDate);
		}catch(Exception e){

			logger.error(MMSController.class.getName() + " getCenterTherapists failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION,e.getMessage());
			return responseResult;
		
		}
	}
	
	@ResponseBody 
	@RequestMapping(value="/getAvailableTherapists",method=RequestMethod.GET)
	public ResponseResult getAvailableTherapistsForService( 
			@RequestParam(value = "requestDate", required=true) Date requestDate,
			@RequestParam(value = "serviceCode", required=true) String serviceCode, 
			@RequestParam(value = "therapistType", required=false) String therapistType)
	{
		
		if(logger.isDebugEnabled()){
			logger.debug("getAvailableTherapistsForService start.");
		}
		try{
			if("0".equals(therapistType)){				
				return mmsService.therapistsAvailableForService(requestDate,serviceCode,TherapistRequestType.FEMALE);				
			}else if("1".equals(therapistType)){
				return mmsService.therapistsAvailableForService(requestDate,serviceCode,TherapistRequestType.MALE);
			}else{
				return mmsService.therapistsAvailableForService(requestDate,serviceCode,TherapistRequestType.ANY);
			}
		
		}catch(Exception e){

			logger.error(MMSController.class.getName() + " getAvailableTherapistsForService failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		
		}
	}
	
	
	@ResponseBody 
	@RequestMapping(value="/getAvailableTimeSlots",method=RequestMethod.GET)
	public ResponseResult getAvailableTimeSlots(@RequestParam(value = "startTime", required=false) String startTime,
			@RequestParam(value = "checkRoomAvailablity", required=false) boolean checkRoomAvailablity,
			@RequestParam(value = "therapistRequestType", required=false) String therapistRequestType,
			@RequestParam(value = "allAvailableSlots", required=false) boolean allAvailableSlots,
			@RequestParam(value = "serviceCodeList", required=true) String serviceCodeList,
			@RequestParam(value = "advanceBookingSlots", required=false) boolean advanceBookingSlots,
			@RequestParam(value = "therapistCode", required=true) String therapistCode,
			@RequestParam(value = "device", required=false) String device ) throws Exception {	
		
		try {
			GetAvailableTimeSlotsRequest param = new GetAvailableTimeSlotsRequest();
			
//			param.setAdvanceBookingSlots(advanceBookingSlots);
//			param.setAllAvailableSlots(allAvailableSlots);
//			param.setCheckRoomAvailablity(checkRoomAvailablity);
			
			param.setAdvanceBookingSlots(advanceBookingSlots);
			param.setAllAvailableSlots(true);
			param.setCheckRoomAvailablity(true);
			
			if (serviceCodeList != null && !"".equals(serviceCodeList)) {
				String[] list = serviceCodeList.split(",");
				List<String> serviceList = Arrays.asList(list);
				param.setServiceCodeList(serviceList);
			}else{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "ServiceCode cannot be null");
				return  responseResult;
			}
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (startTime != null && !"".equals(startTime)) {				
				param.setStartTime(df.parse(startTime));
			} else {
				param.setStartTime(new Date());
			}
			if(therapistCode!=null && !"".equals(therapistCode)){
				param.setTherapistCode(therapistCode);
				param.setTherapistRequestType("2");  //Specific therapist
			}else{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "TherapistCode cannot be null");
				return  responseResult;
			}

			if (logger.isDebugEnabled()) {
				logger.debug("getAvailableTimeSlots() start");
				logger.debug("input parameter:" + param.toString());
			}
			if(device!=null && "AP".equals(device))
			{
				return mmsService.getFormatAvailableTimeSlots(param);
			}else
			{
				return mmsService.getAvailableTimeSlots(param);
			}
			
			
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " getAvailableTimeSlots failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		
		}
	}
	
	@ResponseBody 
	@RequestMapping(value="/queryServiceListByCategoryId/{categoryId}",method=RequestMethod.GET)
	public ResponseResult queryServiceList(@PathVariable("categoryId") String categoryId)throws Exception {
		try 
		{
		
			if(StringUtils.isEmpty(categoryId))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "categoryId for service cannot be null.");				
			}else
			{
				responseResult = mmsService.queryServiceList(categoryId);
			}
			 
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " queryServiceList failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
				
		}
		return responseResult;
	}
	
	@ResponseBody 
	@RequestMapping(value="/queryServiceDetails/{serviceId}",method=RequestMethod.GET)
	public ResponseResult queryServiceDetails(@PathVariable("serviceId") String serviceId){
		
		try
		{
			if(StringUtils.isEmpty(serviceId))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "serviceId cannot be null.");				
			}else
			{
				responseResult = mmsService.queryServiceDetails(serviceId);
			}
						
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " queryServiceDetails failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());			
		}
		return responseResult;
	}
	
	@ResponseBody 
	@RequestMapping(value="/queryAppointmentDetail/{invoiceNo}",method=RequestMethod.GET)
	public ResponseResult queryAppointmentDetail(@PathVariable("invoiceNo") String invoiceNo){
		
		try 
		{
			if(StringUtils.isEmpty(invoiceNo))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "invoiceNo cannot be null.");				
			}else
			{
				responseResult = mmsService.getAppointmentDetail(invoiceNo);
			}
			
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " queryAppointmentDetail failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());	
		}
		return responseResult;
	}
	
	@ResponseBody 
	@RequestMapping(value="/queryLatestAppointmentDetail/{invoiceNo}",method=RequestMethod.GET)
	public ResponseResult queryLatestAppointmentDetail(@PathVariable("invoiceNo") String invoiceNo){
		
		try 
		{
			if(StringUtils.isEmpty(invoiceNo))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "invoiceNo cannot be null.");				
			}else
			{
				responseResult = mmsService.getLatestAppointmentDetail(invoiceNo);
			}			
			
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " queryLatestAppointmentDetail failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());			
		}
		return responseResult;
	}
	
	@ResponseBody 
	@RequestMapping(value="/queryTherapist",method=RequestMethod.GET)
	public ResponseResult queryAppointmentDetail(
			@RequestParam(value = "therapistCode" , required=true) String therapistCode,
			@RequestParam(value="requestDate" ,required=false)  String requestDate)
	{
		
		if(logger.isDebugEnabled()){
			logger.debug("queryAppointmentDetail start.");
		}
		try 
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			if(requestDate!=null && !"".equals(requestDate)){
			    Date inputDate = df.parse(requestDate);
			    Therapist therapist =  mmsService.getTherapistInfo(therapistCode, inputDate);
			    responseResult.initResult(GTAError.Success.SUCCESS, therapist);
			}else
			{
				Therapist therapist =  mmsService.getTherapistInfo(therapistCode, new Date());
				 responseResult.initResult(GTAError.Success.SUCCESS, therapist);
			}
						
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " queryAppointmentDetail failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
		
		return responseResult;
	}
	
	
	@ResponseBody 
	@RequestMapping(value="/createReservation",method=RequestMethod.POST)
	public ResponseResult createReservation(@RequestBody SpaAppointmentDto spaAppointmentDto ){	
		
		try {
			return mmsService.bookAppointment(spaAppointmentDto);
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error("error occured:" + e.getErrorMsg());
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getErrorMsg());
			return responseResult;
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " createReservation failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}
	
	@ResponseBody 
	@RequestMapping(value="/cancelReservation",method=RequestMethod.PUT)
	public ResponseResult cancelReservation(@RequestParam(value = "invoiceNo", required=true) String invoiceNo){	
		try 
		{
			if(StringUtils.isEmpty(invoiceNo))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "invoiceNo cannot be null.");
				return responseResult;
			}else
			{
			 String userId = getUser().getUserId();
			 ResponseResult rs =  mmsService.canCelAppointment(invoiceNo,userId);
			 if ("0".equals(rs.getReturnCode())) spaAppointmentRecService.sendSmsWhenCancelSpaOrder(invoiceNo);
			 return rs;
			}
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " cancelReservation failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}
	
	@ResponseBody 
	@RequestMapping(value="/cancelService",method=RequestMethod.PUT)
	public ResponseResult cancelService(@RequestParam(value = "appointmentId", required=true) String appointmentId){	
		try 
		{	
			if(StringUtils.isEmpty(appointmentId))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "appointmentId cannot be null.");	
				return responseResult;
			}else {
			 String userId = getUser().getUserId();
			 ResponseResult rs =  mmsService.canCelAppointmentService(appointmentId,userId);
			 if ("0".equals(rs.getReturnCode())) spaAppointmentRecService.sendSmsWhenCancelSpaOrderItem(appointmentId);
			 return rs;
			}
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " cancelService failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}
	
	
	@ResponseBody 
	@RequestMapping(value="/payment",method=RequestMethod.POST)
	public ResponseResult payment(@RequestBody MMSPaymentDto paymentDto){
		try {			
			paymentDto.setIsRemote("N");
			return mmsService.payment(paymentDto);	
			
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " payment failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}
	
	@ResponseBody 
	@RequestMapping(value="/getMemberCashValue/{customerId}",method=RequestMethod.GET)
	public ResponseResult getMemberCashValue(@PathVariable(value = "customerId") String customerId){
		try 
		{
			if(StringUtils.isEmpty(customerId))
			{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "customerId cannot be null.");				
			}else
			{
				responseResult = mmsService.getMemberInfo(customerId);
			}			
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " payment failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/transactionstatus/{transactionNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionStatus(@PathVariable(value = "transactionNo") Long transactionNo)
	{

		logger.info("MMSController.getTransactionStatus start ...");

		try
		{
			Map<String, Object> resultMap = new HashMap<String, Object>();
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans==null) {
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "TransactionNo Not found.");
				return responseResult;
			}
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			resultMap.put("orderNo", customerOrderHd.getOrderNo());
			resultMap.put("status", customerOrderHd.getOrderStatus());
			resultMap.put("transactionStatus", customerOrderTrans);
			logger.info("MMSController.getTransactionStatus invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/generalSetting", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAdvancedResPeriod(){
		return globalParameterService.getGlobalParameterById(AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
	}
	
	@RequestMapping(value = "/uploadAsset", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult upload(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam("type")String type) throws IOException {

		try 
		{
			if(StringUtils.isEmpty(type))
				type = "WP";
			InputStream input = file.getInputStream();
			String fileName = file.getOriginalFilename();
			
			BufferedImage  img = ImageIO.read(input);
			
			int width = img.getWidth();
			int height = img.getHeight();
			if(logger.isDebugEnabled())
			{
				logger.debug("fileName:" + fileName + "width:" + img.getWidth() + " height:" + img.getHeight());
			}
			if(!fileName.toLowerCase().endsWith(".jpg") && !fileName.toLowerCase().endsWith(".jpeg") && !fileName.toLowerCase().endsWith(".png"))
			{
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Only support jpg/jpeg/png format.");
				return responseResult;
			}
			if("WP".equals(type))
			{
				if(width != 430 || height !=75)
				{
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 430x75px");
					return responseResult;
				}
			}else if("APP".equals(type))
			{
				if(width != 480 || height !=280)
				{
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 480x280px");
					return responseResult;
				}
			}else if("GS".equals(type))
			{
				if(width != 1080 || height !=290)
				{
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 1080x290px");
					return responseResult;
				}
			}else if("RET".equals(type))
			{
				if(width != 260 || height !=260)
				{
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 260x260px");
					return responseResult;
				}
			}
			String saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.WELLNESS);
			
			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("fileName", saveFilePath);
			responseResult.initResult(GTAError.Success.SUCCESS, imgPathMap);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}

		return responseResult;
	}
	
	@RequestMapping(value = "/updateSpaCenterInfo", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateSpaCenterInfo(@RequestBody SpaCenterInfoDto dto) {
		try {
			LoginUser user = super.getUser();
			String userId = user.getUserId();
			return mmsService.updateSpaCenterInfo(dto, userId);
			
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getSpaCenterInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaCenterInfo() {
		try {
			SpaCenterInfoDto dto = mmsService.getSpaCenterInfo();
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getSpaCategory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaCategory() {
		try {
			Long latestPicId = mmsService.getLatestUpdatedPicId();
			List<SpaCategoryDto> resultList =  mmsService.getSpaCategoryList();
			SpaCategoryResponseDto response = new SpaCategoryResponseDto();
			response.setLatestUpdatedPicId(latestPicId);
			response.setSpaCategory(resultList);
			responseResult.initResult(GTAError.Success.SUCCESS, response);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/updateCategoryImage", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateCategoryImage(@RequestBody List<SpaPicPathDto> dtoList) {
		try {
			responseResult =  mmsService.updateSpaCategoryPic(dtoList, getUser().getUserId());
			
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getSpaRetreatList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatList(
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters,
			@RequestParam(value = "sortBy", required = false, defaultValue="displayOrder") String sortBy, 
			@RequestParam(value = "isAscending", required = false, defaultValue = "true") String isAscending, 
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber, 
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "device", required = false) String device) 
	{
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		
		try {
			if(StringUtils.isEmpty(device))
			{
				device="PC";
			}
			
			String joinHQL = mmsService.getSpaRetreatListSql();
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			
			if(!StringUtils.isEmpty(status))
			{
				joinHQL += " where m.status = '" + status + "'";
			}
			
			ResponseResult rs = null;
			if (advanceQueryDto != null && advanceQueryDto.getRules()!=null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{	
				logger.debug("get advance search result.");
				if(!StringUtils.isEmpty(status))
				{
					joinHQL += " and";
				}else
				{
					if(!"PC".equals(device))//for webportal and app, only can display those records with status 'Show'
					{
						joinHQL += " where m.status = 'Show'";
					}else
					{
						joinHQL += " where ";
					}
				}
				advanceQueryDto.setSortBy(sortBy);
				advanceQueryDto.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(advanceQueryDto, joinHQL, page, SpaRetreatDto.class);			
			}
			else
			{
				if(!"PC".equals(device) && StringUtils.isEmpty(status)) //for webportal and app, only can display those records with status 'Show'
				{
					joinHQL += " where m.status = 'Show'";
				}
				logger.debug("getInitial search result.");
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, SpaRetreatDto.class);
			}						
			return rs;
			
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/updateSpaRetreat", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateSpaRetreat(@RequestBody SpaRetreatDto dto) {
		try {
			  mmsService.updateSpaRetreat(dto, getUser().getUserId());
			  responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/addSpaRetreat", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult addSpaRetreat(@RequestBody SpaRetreatDto dto) {
		try {
			  mmsService.updateSpaRetreat(dto, getUser().getUserId());
			  responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getSpaRetreatItemList/{retId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatItemList(
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters,
			@RequestParam(value = "sortBy", required = false, defaultValue="itemId") String sortBy, 
			@RequestParam(value = "isAscending", required = false, defaultValue = "true") String isAscending, 
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber, 
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "status", required = false) String status,
			@PathVariable(value = "retId") Long retId,
			@RequestParam(value = "device", required = false) String device) {
		
		
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		
		try {
			if(StringUtils.isEmpty(device))
			{
				device="PC";
			}
			
			String joinHQL = mmsService.getSpaRetreatItemListSql(retId);
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			if(!StringUtils.isEmpty(status))
			{
				joinHQL += " where a.status = '" + status + "'";
			}
			ResponseResult rs = null;
			if (advanceQueryDto != null && advanceQueryDto.getRules()!=null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{	
//				joinHQL += " where ";
				logger.debug("get advanceSearch result.");
				if(!StringUtils.isEmpty(status))
				{
					joinHQL += " and";
				}else
				{
					if(!"PC".equals(device))//for webportal and app, only can display those records with status 'Show'
					{
						joinHQL += " where a.status = 'Show'";
					}else
					{
						joinHQL += " where ";
					}
				}
				advanceQueryDto.setSortBy(sortBy);
				advanceQueryDto.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(advanceQueryDto, joinHQL, page, SpaRetreatItemDto.class);			
			}
			else
			{
				logger.debug("getInitial search result.");
				if(!"PC".equals(device) && StringUtils.isEmpty(status)) //for webportal and app, only can display those records with status 'Show'
				{
					joinHQL += " where a.status = 'Show'";
				}
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, SpaRetreatItemDto.class);
			}
			
			return rs;
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/updateSpaRetreatItem", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateSpaRetreatItem(@RequestBody SpaRetreatItemDto dto) {
		try {
			  mmsService.updateSpaRetreatItem(dto, getUser().getUserId());
			  responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/addSpaRetreatItem", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult addSpaRetreatItem(@RequestBody SpaRetreatItemDto dto) {
		try {
			  mmsService.updateSpaRetreatItem(dto, getUser().getUserId());
			  responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/switchRetreatOrder", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult switchRetreatOrder(
			@RequestParam(value = "sourceRetId", required = true) Long sourceRetId, 
			@RequestParam(value = "moveType", required = true) String moveType) {
		try {
			mmsService.switchRetreatOrder(sourceRetId, moveType, super.getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/sendReceipt", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendReceipt(@RequestParam(value = "invoiceNo", required = true) String invoiceNo ) {
		try {
			 return mmsService.sendReceipt(invoiceNo);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
}
