package com.sinodynamic.hkgta.controller.common;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.QRCodeUtil;

@Controller
@RequestMapping("/common")
public class GetFileController extends ControllerBase {

	@RequestMapping(value = "/getCustomerFile", method = RequestMethod.GET,produces = {"image/jpeg","image/png","image/gif"})
	@ResponseBody
	public byte[] getFile(@RequestParam("filePath") String filePath,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (null == filePath || "".equals(filePath)) {
			return null;
		}
		String absoluteFilePath = "";
		try {
			absoluteFilePath = FileUpload
					.getBasePath(FileUpload.FileCategory.USER) + filePath;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream in = null;
		try {

			logger.debug("absoluteFilePath=" + absoluteFilePath);
			in = new FileInputStream(absoluteFilePath);
			return IOUtils.toByteArray(in);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			logger.debug("finally");
			if (in != null)
				in.close();
		}
	}
	
	@RequestMapping(value = "/pdf", method = RequestMethod.GET,produces="application/pdf")
	@ResponseBody
	public byte[] getPdfFile(@RequestParam("fileName") String fileName,
					@RequestParam(value="fileCategory", defaultValue = "")  String fileCategory,
					HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if(StringUtils.isEmpty(fileName)){
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.getWriter().write("no pdf file found!");
			return null;
		}
		String absoluteFilePath = "";
		try {
			if("notification".equals(fileCategory)){
				absoluteFilePath = FileUpload.getBasePath(FileUpload.FileCategory.NOTIFICATION) + fileName;
			}else{
				absoluteFilePath = FileUpload.getBasePath(FileUpload.FileCategory.USER) + fileName;
			}
		} catch (Exception e1) {
			logger.error("get base path failed",e1);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write("get pdf store path failed!");
			return null;
		}
		InputStream in = null;
		try {
			logger.debug("absoluteFilePath=" + absoluteFilePath);
			in = new FileInputStream(absoluteFilePath);
			byte[] byteArray = IOUtils.toByteArray(in);
			response.setContentType("application/pdf");  
			response.setContentLength(byteArray.length);  
			response.setHeader("Content-Disposition", "inline; filename="+fileName);  
			return byteArray;
		} catch (Exception e) {
			logger.error("get file failed !",e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write("get file failed !");
			return null;
		} finally {
			logger.debug("finally");
			IOUtils.closeQuietly(in);
		}
	}
	
	
	@RequestMapping(value = "/getQRcode", method = RequestMethod.GET,produces = {"image/png"})
	@ResponseBody
	public byte[] getQRcode(@RequestParam("dpId") String dpId, HttpServletRequest request, HttpServletResponse response){
		String suffix = "DP";
		try {
			if(StringUtils.isEmpty(dpId)){
				response.setStatus(HttpStatus.NOT_FOUND.value());
				response.getWriter().write("day pass Id is null");
				return null;
			}
			QRCodeUtil generater = new QRCodeUtil();
			BufferedImage bi = generater.encoderQRCode(suffix+dpId);
			InputStream is = QRCodeUtil.BufferedImage2InputStream(bi);
			byte[] byteArray = IOUtils.toByteArray(is);
			response.setContentType("image/png");
			response.setHeader("Content-Disposition", "attachment;filename="+suffix+dpId+".png");
			response.setContentLength(byteArray.length);  
			return byteArray;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	@RequestMapping(value = "/getImage", method = RequestMethod.GET,produces = {"image/jpeg","image/png","image/gif"})
	@ResponseBody
	public byte[] getFile(@RequestParam("filePath") String filePath, @RequestParam("fileCategory") String fileCategory,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if (null == filePath || "".equals(filePath)) {
			return null;
		}
		
		if (!filePath.startsWith("/")) {
			filePath = "/" + filePath;
		}
		String absoluteFilePath = "";
		try {
			
			switch (fileCategory) {
			case "profile":
				absoluteFilePath = FileUpload
						.getBasePath(FileUpload.FileCategory.USER) + filePath;
				break;
			case "material":
				absoluteFilePath = FileUpload
						.getBasePath(FileUpload.FileCategory.MATERIAL) + filePath;
				break;
			case "transaction":
				absoluteFilePath = FileUpload
						.getBasePath(FileUpload.FileCategory.TRANSACTION) + filePath;
				break;
			case "course":
				absoluteFilePath = FileUpload
						.getBasePath(FileUpload.FileCategory.COURSE) + filePath;
				break;
			case "restaurant":
				absoluteFilePath = FileUpload
				.getBasePath(FileUpload.FileCategory.RESTAURANT) + filePath;
				break;
			case "notification":
				absoluteFilePath = FileUpload
				.getBasePath(FileUpload.FileCategory.NOTIFICATION) + filePath;
				break;
			case "advertise":
				absoluteFilePath = FileUpload
				.getBasePath(FileUpload.FileCategory.ADVERTISE) + filePath;
				break;
			case "guestroom":
				absoluteFilePath = FileUpload
				.getBasePath(FileUpload.FileCategory.GUESTROOM) + filePath;
				break;
			case "notice":
				absoluteFilePath = FileUpload
				.getBasePath(FileUpload.FileCategory.NOTICE) + filePath;
				break;
			case "wellness":
				absoluteFilePath = FileUpload
				.getBasePath(FileUpload.FileCategory.WELLNESS) + filePath;
				break;
			default:
				break;
			}
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		InputStream in = null;
		try {

			logger.debug("absoluteFilePath=" + absoluteFilePath);
			in = new FileInputStream(absoluteFilePath);
			return IOUtils.toByteArray(in);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			logger.debug("finally");
			if (in != null)
				in.close();
		}
	}
	
	@RequestMapping(value = "/getAttachFile", method = RequestMethod.GET)
	public void getAttachFile(@RequestParam("fileName") String fileName,
					@RequestParam(value="fileCategory", defaultValue = "")  String fileCategory,
					HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		if(StringUtils.isEmpty(fileName)){
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.getWriter().write("File not found!");
		}
		String fileSuffix = fileName.substring(fileName.lastIndexOf("."));
		String absoluteFilePath = "";
		try {
			if("notification".equals(fileCategory)){
				absoluteFilePath = FileUpload.getBasePath(FileUpload.FileCategory.NOTIFICATION) + fileName;
			}
		} catch (Exception e1) {
			logger.error("get base path failed",e1);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write("Get file path failed!");
		}
		InputStream in = null;
		try {
			logger.debug("absoluteFilePath=" + absoluteFilePath);
			
			if(fileSuffix.endsWith("pdf")){
				response.setContentType("application/pdf");  
			}else if(fileSuffix.endsWith("jpeg")){
				response.setContentType("image/jpeg");  
			}else if(fileSuffix.endsWith("gif")){
				response.setContentType("image/gif");
			}else if (fileSuffix.endsWith("png")){
				response.setContentType("image/png"); 
			}else if(fileSuffix.endsWith("gif")){
				response.setContentType("image/gif"); 
			}else if(fileSuffix.endsWith("html")){
				response.setContentType("text/html"); 
			}else if(fileSuffix.endsWith("m4v") ||fileSuffix.endsWith("mp4")||fileSuffix.endsWith("mov")||fileSuffix.endsWith("avi") ){
				response.setHeader("Cache-Control", "max-age=21600");
				response.setHeader("Content-Disposition", "inline; filename="+absoluteFilePath);  
				response.sendRedirect(absoluteFilePath);  
			}else{
				response.setContentType("application/octet-stream");
				response.setHeader("Content-disposition", "attachment;filename="+fileName.substring(fileName.indexOf("@")+1));
			}
			
			in = new FileInputStream(absoluteFilePath);
			byte[] byteArray = IOUtils.toByteArray(in);
			response.setContentLength(byteArray.length);  
			response.getOutputStream().write(byteArray);
		} catch (Exception e) {
			logger.error("Get file failed !",e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write("Get file failed !");
		} finally {
			logger.debug("finally");
			IOUtils.closeQuietly(in);
		}
	}
	
}


