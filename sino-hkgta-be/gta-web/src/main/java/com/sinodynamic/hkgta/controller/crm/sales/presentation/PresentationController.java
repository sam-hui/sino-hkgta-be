package com.sinodynamic.hkgta.controller.crm.sales.presentation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.FolderAndDetailsDto;
import com.sinodynamic.hkgta.dto.crm.MaterialFolderDetailsDto;
import com.sinodynamic.hkgta.dto.crm.PresentMaterialDto;
import com.sinodynamic.hkgta.dto.crm.PresentationDetailDto;
import com.sinodynamic.hkgta.dto.crm.PresentationEmailDto;
import com.sinodynamic.hkgta.dto.crm.PresentationWithMaterialDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.presentation.PresentMaterialService;
import com.sinodynamic.hkgta.service.crm.sales.presentation.PresentationService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.OrderByType;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/presentation")
@Scope("prototype")
public class PresentationController extends ControllerBase<PresentationBatch> {
	
	private Logger logger = Logger.getLogger(PresentationController.class); 
	
	@Autowired
	private PresentationService presentationService;
	
	@Autowired
	private PresentMaterialService presentMaterialService;
	
	@Autowired
	private CustomerEmailContentService customerEmailService;
	
	/**
	 * 1.  update the PresentationBatch
	 * 2.  delete the PresentMateralSeq by PresentationBatch
	 * 3   save the PresentMateralSeq by PresentMateral
	 * @param presentId   PresentationBatch
	 * @param presentName  PresentationBatch
	 * @param materialist PresentMateralSeq 
	 * @return
	 */
	@RequestMapping(value = "/{presentId}", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updatePresentationBatch(@PathVariable(value="presentId") Long presentId, @RequestBody  PresentationWithMaterialDto ptDto) { 
		logger.info("===============================updatePresentation start=============================");
		ResponseResult result = new ResponseResult();
		try {
			PresentationBatch p = presentationService.getPresentationById(presentId);
			if(null == p){
				result.setReturnCode("1");
				result.setErrorMessageEN("The presentation isn't exist!");
				return result;
			}
			
			if(!super.getUser().getUserId().equals(p.getCreateBy())){
				result.setReturnCode("1");	
				result.setErrorMessageEN("you can't update other's presentation!");
				return result;
			}
			ptDto.setPresentId(presentId);
			ptDto.setUpdateBy(super.getUser().getUserId());
			result = presentationService.updatePresentation(ptDto);
		}  catch (Exception e) {
			e.printStackTrace();
			result.setErrorMessageEN("update presentation happen exception,please contact administrator,thank you!");
			result.setReturnCode(ResponseMsgConstant.ERRORCODE);
			return result;
		}
		logger.info("===============================updatePresentation success=============================");
		return result;
	}

	@RequestMapping(value = "/convert/{presentId}", method = RequestMethod.GET)
	public @ResponseBody String convert(@PathVariable(value = "presentId") String presentId) {		
	   return presentationService.convert(Long.parseLong(presentId));	
	}
			
	@RequestMapping(value="/email", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMsg sendPresetationByEmail(@RequestBody PresentationEmailDto mailReq) {
		
		logger.info("PresentationController.sendPresetationWithEmail invocation start ...");
		 
		String presentID = mailReq.getPresentID();
		String customerID = mailReq.getCustomerID();
		String messageSubject = mailReq.getMessageSubject();
		String emailBody = mailReq.getEmailBody();
		String emailTo = mailReq.getEmailTo();
		
		boolean updateSuccess = false;
		boolean mailSuccess = false;
		File attach = null;
		
		if ((CommUtil.nvl(customerID).length() == 0 && CommUtil.nvl(emailTo).length() == 0) ||
				((CommUtil.nvl(customerID).length() != 0 && CommUtil.nvl(emailTo).length() != 0))) {
			logger.error("customerID && emailTo can not be both null or not null!");
			return new ResponseMsg("-6", "customerID && emailTo can not be both null or not null!");
		}
		
		if (messageSubject == null || messageSubject.trim().length() == 0) {
			logger.error("emailTitle can not be null");
			return new ResponseMsg("-6", "emailTitle can not be null!");
		}
		
		if (emailBody == null || emailBody.trim().length() == 0) {
			logger.error("emailBody can not be null");
			return new ResponseMsg("-6", "emailBody can not be null!");
		}
		
		try {
			
			//get attachments
			if (presentID != null && presentID.trim().length() > 0) {
				
				String fileName = presentationService.convert(Long.parseLong(presentID));
				attach = getAttachment(fileName);
				
				if (attach == null || !attach.exists()) {
					logger.error("Error in generating pdf prentation!");
					return new ResponseMsg("-4", "Error in generating pdf prentation!");
				}
			}
			
			//save customer email content with attachments
			CustomerEmailContent cec = null;
			
			if (CommUtil.nvl(customerID).length() != 0) {
				cec = customerEmailService.sendPresentationEmailToCustomer(attach, Long.parseLong(customerID), messageSubject, emailBody);
			}
			
			if (CommUtil.nvl(emailTo).length() != 0) {
				
				if (!CommUtil.validateEmailAddress(emailTo)) {
					
					logger.error("Bad email address list detected!");
					return new ResponseMsg("-7", "Bad email address list detected!");
				}
				cec = customerEmailService.sendPresentationEmailToSomebody(attach, emailTo, messageSubject, emailBody);
			}
			
			if (cec == null) {
				logger.error("No customer exists with id: " + customerID);
				return new ResponseMsg("-3", "No Customer exists with id: " + customerID + "!");
			}
			
			//send email start
			File[] attaches = null;
			if (attach != null) attaches = new File[] { attach };
			logger.info("recipientEmail=" + cec.getRecipientEmail() + ", emailTitle=" + messageSubject + ", emailBody=" + emailBody + ", attaches=" + (attach == null ? "" : attach));
			
			mailSuccess = MailSender.sendEmail(cec.getRecipientEmail(), null, null, messageSubject, emailBody, attaches);
			if (!mailSuccess) {
				cec.setStatus(EmailStatus.FAIL.getName());
				updateSuccess = customerEmailService.modifyCustomerEmailContent(cec);
				if (!updateSuccess) {
					logger.error("Error in updating mail status!");
					return new ResponseMsg("-2", "Error in updating mail status!");
				}
				
				logger.error("Error in Sending mail!");
				return new ResponseMsg("-1", "Error in Sending mail!");
			}
			
			//update email status
			cec.setStatus(EmailStatus.SENT.getName());
			updateSuccess = customerEmailService.modifyCustomerEmailContent(cec);
			if (!updateSuccess) {
				logger.error("Error in updating mail status!");
				return new ResponseMsg("-2", "Error in updating mail status!");
			}
			
			logger.info("PresentationController.sendPresetationWithEmail invocation end ...");
			return new ResponseMsg("0",  "Sending Successful");
			
		} catch (Exception e) {
			
			logger.error(e.toString());
			e.printStackTrace();
			return new ResponseMsg("-5",  "Unknown errors happened!");
		}

	}
	
	private File getAttachment(String fileName) {
		
		if (fileName == null) return null;
		return new File(fileName);
	}
	
	@RequestMapping(value="/{presentId}", method=RequestMethod.DELETE, produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseMsg deletePresentation(@PathVariable(value="presentId") Long presentId){
		try {
			PresentationBatch p = new PresentationBatch();
			p.setPresentId(presentId);
			return presentationService.deletePresentation(p);
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error(PresentationController.class.getName()+" deletePresentation fail!");
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_EN);
		}
	}
	
	@RequestMapping(value="/", method=RequestMethod.POST, produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseMsg createPresentation(@RequestBody PresentationWithMaterialDto p){
		try {
			String userId = super.getUser().getUserId();
			p.setCreateBy(userId);
			p.setUpdateBy(userId);
			return presentationService.savePresentation(p);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error(PresentationController.class.getName()+" savePresentation fail!");
			return new ResponseMsg(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_EN);
		}
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult list(
			@RequestParam(value="searchText" , required = false) String searchText,
 			@RequestParam(value="sortBy" , required = false, defaultValue = "presentationname") String sortBy,
 			@RequestParam(value="isAscending" , required = false) Boolean isAscending,
 			@RequestParam(value="isMyPresentation" , required = false, defaultValue = "true") Boolean isMyPresentation,
 			@RequestParam(value="pageNumber" , required = false, defaultValue = "1") int pageNumber,
 			@RequestParam(value="pageSize" , required = false, defaultValue = "3") int pageSize) {
		
		logger.info("PresentationController.getPresentationList start ...");
		
//		ResponseResult result = new ResponseResult();
		
		ResponseResult result = new ResponseResult("0","",new Data());
		
		page = new ListPage<PresentationBatch>(pageSize, pageNumber);
		
		List<PresentationDetailDto> data = new ArrayList<PresentationDetailDto>();
		
		if(null == isAscending){
			isAscending = true;
		}
		
		try {
			page = presentationService.getPresentationList(getWhoCreatedBy(isMyPresentation), getSortBy(sortBy), isAscending, searchText, page);
			if (null!=page)
			{
				PresentationDetailDto present = null;
				for(PresentationBatch p : page.getList())
				{
					present = new PresentationDetailDto(p);
					data.add(present);
				}
				
				int count = page.getList().size();
				if(count == 0)
					return new ResponseResult("0","No record find!",new Data());
				
				result.getListData().setList(data);
				result.getListData().setRecordCount(page.getAllSize());
				result.getListData().setTotalPage(page.getAllPage());
				result.getListData().setCurrentPage(page.getNumber());
				result.getListData().setPageSize(page.getSize()==count?page.getSize():count);
				result.getListData().setLastPage(page.isLast());
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	//================================================
	//get userId from request
	/**
	 * @param isMyPresentation
	 * @return
	 */
	private String getWhoCreatedBy(Boolean isMyPresentation){
		if(null == isMyPresentation || !isMyPresentation)
			return null;
		return super.getUser().getUserId();
	}
	
	@RequestMapping(value="/clone/{presentId}", method=RequestMethod.POST,produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseResult cloneByID(@PathVariable(value="presentId") Long presentId,
			@RequestBody PresentationBatch p) throws Exception {
		
		logger.info("PresentationController.getPresentationList start ...");
		
		String userId = super.getUser().getUserId();
		p.setCreateBy(userId);
		Long newPresentId = presentationService.cloneById(presentId, p.getPresentName(), p.getDescription(), p.getCreateBy());
		responseResult.initResult(GTAError.Success.SUCCESS, new PresentationBatch(newPresentId));
		return responseResult;
		
	}
	
	@RequestMapping(value="/{presentId}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPresentationDetails(@PathVariable(value="presentId") Long presentId) {
		
		logger.info("PresentationController.getPresentation Details start ...");
		
		ResponseResult result = new ResponseResult();
		
		try {
			
			PresentationBatch present = presentationService.getPresentationById(presentId);
			if (null == present)
			{
				result.setErrorMessageEN("No Presentation Found for present id :" + presentId);
				result.setReturnCode("0");
				return result;
			}
			
			List<PresentMaterialSeq> seqs = present.getPresentMaterialSeqs();
			
			List<PresentMaterialDto> presentMaterials = presentationService.getPresentMaterials(seqs);
			
			PresentationWithMaterialDto pDetail = presentationService.getPresentDto(present, presentMaterials);
			
			result.setData(pDetail);
			result.setErrorMessageEN("");
			result.setReturnCode("0");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private OrderByType getSortBy(String sortBy)
	{
		OrderByType orderType = null;
		switch (sortBy.toLowerCase())
		{
		case "presentationname":
			orderType = OrderByType.PresentationName;
			break;
		case "createby":
			orderType = OrderByType.CreateBy;
			break;
		case "updatedate":
			orderType = OrderByType.UpdateDate;
			break;
		default:
			orderType = OrderByType.PresentationName;
			break;
		}
		
		return orderType;
	}
	
	/**
	 * This method is used to retrieve the list of presentation folders
	 * @return ResponseResult - Holds response code, error/success message, pagination details and list of presentation folders
	 * @throws IOException
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/folders/list", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getTemplateFolderList() throws IOException	{
		logger.debug("PresentationService.getTemplateFolderList invocation start ...");

		List<ServerFolderMapping> folderList = null;
		ResponseResult result = null;

		try {
			folderList = presentationService.getTemplateFolderList();
			if(folderList != null &&folderList.size()>0){
				result = new ResponseResult();
				
				result.setData(folderList);
				result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
				result.setErrorMessageEN("");
			}
			return result;
			
		} catch (Exception e) {
			logger.error(PresentationController.class.getName()+" getTemplateFolderList error!");
			e.printStackTrace();
			return new ResponseResult(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_TC, ResponseMsgConstant.ERRORMSG_EN);
		}
	}
	
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/folders/{folderId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getFolderDetail(@PathVariable(value="folderId") String folderId) throws IOException {
		logger.debug("PresentationService.getFolderDetail invocation start ...");
		
		if(StringUtils.isEmpty(folderId))
			return new ResponseResult("1", "", "param folderId is null!");
		
		//ServerFolderMapping folder = presentationService.getServerFolderMappingById(Long.valueOf(folderId));
		
		//String pathPrefixKey = folder.getPathPrefixKey();
		
		List<PresentMaterialDto> materialListDto = new ArrayList<PresentMaterialDto>();
		ResponseResult result = null;
		try {
			List<PresentMaterial> materialList = presentationService.getFolderDetails(Long.valueOf(folderId));
			if(materialList != null &&materialList.size()>0){
				
				for(PresentMaterial pm : materialList){
					PresentMaterialDto presentMaterialDto = new PresentMaterialDto();
					presentMaterialDto.setMaterialId(pm.getMaterialId());
					//presentMaterialDto.setMaterialFilename(pathPrefixKey+pm.getMaterialFilename());
					presentMaterialDto.setMaterialFilename("/presentation/get_media?material_id="+pm.getMaterialId()+"&media_type="+pm.getMaterialType());
					
					
					//String materialFilename = pm.getMaterialFilename();
					//String extname = materialFilename.substring(materialFilename.lastIndexOf("."));
					//presentMaterialDto.setThumbnail(pathPrefixKey+materialFilename.replace(extname, "") + "_small" + extname);
					presentMaterialDto.setThumbnail("/presentation/get_thumbnail?material_id="+pm.getMaterialId()+"&media_type="+pm.getMaterialType());
					presentMaterialDto.setMaterialType(pm.getMaterialType());
					presentMaterialDto.setStatus(pm.getStatus());
					materialListDto.add(presentMaterialDto);
				}
				
				result = new ResponseResult();				
				result.setData(materialListDto);
				result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
				result.setErrorMessageEN("");
				
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(PresentationController.class.getName()+" getFolderDetail error!");
			return new ResponseResult(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_TC, ResponseMsgConstant.ERRORMSG_EN);
		}
	}
	
	
	@RequestMapping(value = "/folders/details", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getTemplateFolderAndDetails(@RequestParam(required=false,defaultValue="") Long presentId) throws IOException {
		
		logger.debug("PresentationController.getTemplateFolderAndDetails invocation start ...");
		List<ServerFolderMapping> folderList = new ArrayList<ServerFolderMapping>();
		ResponseResult result = new ResponseResult();
		try {
			folderList = presentationService.getTemplateFolderList();
			List<FolderAndDetailsDto> folderAndDetailsDtoList = new ArrayList<FolderAndDetailsDto>();
			if(folderList != null &&folderList.size()>0){
				List<PresentMaterialSeq> presentMaterialSeqList = presentationService.getPresentMaterialSeqByPresentId(presentId);
				for (ServerFolderMapping sfm : folderList)
				{  	
					//String pathPrefixKey = sfm.getPathPrefixKey();
					
					List<PresentMaterial> materialList1 = presentationService.getFolderDetails(Long.valueOf(sfm.getFolderId()));
					List<MaterialFolderDetailsDto> materialList = new ArrayList<MaterialFolderDetailsDto>();
					for (PresentMaterial pm : materialList1)
					{  	
						boolean checkUsage = false;
						if (presentMaterialSeqList != null && presentMaterialSeqList.size() > 0) {
							for (PresentMaterialSeq seq : presentMaterialSeqList) {
								if (seq.getId().getMaterialId().equals(pm.getMaterialId())) {
									checkUsage = true;
								}
							}
						}
						if (!checkUsage) {
							MaterialFolderDetailsDto materialFolderDetailsDto = new MaterialFolderDetailsDto();
							materialFolderDetailsDto.setMaterialId(pm.getMaterialId());
							materialFolderDetailsDto.setStatus(pm.getStatus());
							// String materialFilename =
							// pm.getMaterialFilename();
							// String pathOnServer =
							// appProps.getProperty(pathPrefixKey);
							// String extname =
							// materialFilename.substring(materialFilename.lastIndexOf("."));
							// String thumbnailFileName =
							// materialFilename.replace(extname, "") + "_small"
							// + extname;

							// String materialFilenameThumbnail = pathPrefixKey
							// + File.separator + pm.getFolderId() +
							// File.separator + thumbnailFileName;
							// String materialFilenameThumbnail = pathPrefixKey
							// + pm.getFolderId() + "/"+ thumbnailFileName;
							// materialFolderDetailsDto.setMaterialFilename(pathPrefixKey
							// + File.separator + pm.getFolderId() +
							// File.separator +pm.getMaterialFilename());
							// materialFolderDetailsDto.setMaterialFilename(pathPrefixKey
							// + pm.getFolderId() + "/"
							// +pm.getMaterialFilename());

							materialFolderDetailsDto.setMaterialFilename("/presentation/get_media?material_id=" + pm.getMaterialId() + "&media_type=" + pm.getMaterialType());

							// materialFolderDetailsDto.setThumbnail(materialFilenameThumbnail);

							materialFolderDetailsDto.setThumbnail("/presentation/get_thumbnail?material_id=" + pm.getMaterialId() + "&media_type=" + pm.getMaterialType());

							materialList.add(materialFolderDetailsDto);
						
						// materialFolderDetailsDto.setThumbnail(materialFilename.);
						}
					        
					}
					
					FolderAndDetailsDto folderAndDetailsDto = new FolderAndDetailsDto();
					if(materialList != null &&materialList.size()>0){
					folderAndDetailsDto.setMaterialList(materialList);
					}
					if(sfm.getDisplayName() != null) {
					folderAndDetailsDto.setFolderName(sfm.getDisplayName());
					}
					if(sfm.getFolderId()!= null) {
					folderAndDetailsDto.setFolderId(sfm.getFolderId());
					}
					if(folderAndDetailsDto!= null) {
					folderAndDetailsDtoList.add(folderAndDetailsDto);
					}
				}  
				result.setData(folderAndDetailsDtoList);
				result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
				result.setErrorMessageEN("");
				
			}
			return result;
		} catch (Exception e) {
			logger.error(PresentationController.class.getName()+" getTemplateFolderAndDetails error!");
			e.printStackTrace();
			return new ResponseResult(ResponseMsgConstant.ERRORCODE, ResponseMsgConstant.ERRORMSG_TC, ResponseMsgConstant.ERRORMSG_EN);
		}
		
	}

	private void FolderAndDetailsDto() {
		// TODO Auto-generated method stub		
	}
	
	
}
