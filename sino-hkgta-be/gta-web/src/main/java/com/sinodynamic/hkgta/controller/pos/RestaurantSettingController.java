package com.sinodynamic.hkgta.controller.pos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pos.RestaurantImageDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantMasterDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.pos.RestaurantMasterService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
public class RestaurantSettingController extends ControllerBase<RestaurantMaster> {

	@Autowired
	private RestaurantMasterService restaurantMasterService;
	
	@RequestMapping(value = "/restaurant/{restaurantId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateRestaurant(@RequestBody RestaurantMasterDto restaurantMasterDto, @PathVariable(value = "restaurantId") String restaurantId) {
		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			restaurantMasterService.updateRestaurant(restaurantMasterDto, restaurantId, userId);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/restaurant/{restaurantId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurant(@PathVariable(value = "restaurantId") String restaurantId) {
		try {
			RestaurantMasterDto restaurantMasterDto = restaurantMasterService.getRestaurant(restaurantId);
			responseResult.initResult(GTAError.Success.SUCCESS, restaurantMasterDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/restaurant/{restaurantId}/{imageFeatureCode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getParticularRestaurant(@PathVariable(value = "restaurantId") String restaurantId,
			@PathVariable(value = "imageFeatureCode")  String imageFeatureCode,
			@RequestParam(value = "deviceType", defaultValue = "AP") String deviceType) {
		try {
			RestaurantMasterDto restaurantMasterDto = restaurantMasterService.getRestaurant(restaurantId);
			
			List<RestaurantImageDto> specifiedImages = new ArrayList<RestaurantImageDto>();
			
			List<RestaurantImageDto> images =  restaurantMasterDto.getRestaurantImages();
			if (images.size() > 0) {
				for (RestaurantImageDto image : images){
					if (deviceType.equals(image.getForDeviceType())){
						if (imageFeatureCode.equals(image.getImageFeatureCode()) || imageFeatureCode.equals("All")){
							specifiedImages.add(image);
						}	
					}
				}
			}
			
			restaurantMasterDto.setRestaurantImages(specifiedImages);
			
			responseResult.initResult(GTAError.Success.SUCCESS, restaurantMasterDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
}