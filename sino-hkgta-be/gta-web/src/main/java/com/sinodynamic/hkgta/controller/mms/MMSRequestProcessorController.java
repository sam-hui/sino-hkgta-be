package com.sinodynamic.hkgta.controller.mms;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.mms.CancelReservationResponseDto;
import com.sinodynamic.hkgta.dto.mms.CancelSpaPaymentResponseDto;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SettleMMSPaymentByCashValueResponseDto;
import com.sinodynamic.hkgta.dto.mms.SettleMMSPaymentByGuestFoliorResponseDto;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping(value="/mmsintegration")
public class MMSRequestProcessorController extends ControllerBase{
	
	private static String TOKEN_KEY="i37iq3nlu3a4b6v9yrjxrjeit3qhptg534v8u54hj40joo0hoo6lzb60s7otiamm32j"
			+ "k60y5hg89jr0u3pwu816868bi23x5r6nmtjcybepum39l45tu3axy5dwz9rb8dol2lewvxgwl6s91h50334zjj3du77xnr7yzkl98fb5feaj3055bu1cyf6ixsdnplw5br4ri";
	
	private static String TOKEY_KEY_UUID = "ceeb83109a59489abff4e3b98fab59ec";
	
	
	@Autowired
	private MMSService mmsService;
		
	@ResponseBody 
	@RequestMapping(value="/settleMMSPaymentByCashValue",method=RequestMethod.POST)
	public SettleMMSPaymentByCashValueResponseDto settleMMSPaymentByCashValue(HttpServletRequest request,
	@RequestParam(value = "invoiceId", required=true) String invoiceId,
	@RequestParam(value = "amount", required=true) BigDecimal amount){	
		
		MMSPaymentDto paymentDto = new MMSPaymentDto();
		paymentDto.setAmount(amount);
		paymentDto.setInvoiceNo(invoiceId);
		paymentDto.setIsRemote("Y");
		
//		String token = request.getHeader("api_key");
//		
//		if(logger.isDebugEnabled())
//		{
//		   logger.debug("api_key:" + token);	
//		}
//		String decrypteKey = mmsService.aesDecode(token, TOKEY_KEY_UUID);
//		
		SettleMMSPaymentByCashValueResponseDto  response = new SettleMMSPaymentByCashValueResponseDto();
//		
//		if(decrypteKey == null || decrypteKey.indexOf(TOKEN_KEY)<0)
//		{
//			response.setReturnCode("1");
//			response.setMessage("Invalid token value.");
//			return response;
//		}			
		try {					
			
			ResponseResult result =  mmsService.payment(paymentDto);			
			MMSPaymentDto returnDto = (MMSPaymentDto) result.getDto();			
			
			response.setReturnCode(result.getReturnCode());
			if(returnDto!=null)
			{
				SettleMMSPaymentByCashValueResponseDto.Data  data = response.new Data();
				data.setOrderNo(returnDto.getOrderNo());
				data.setTransactionNo(returnDto.getTransNo().toString());
				data.setSettleTimestamp(returnDto.getSettleTime());
				response.setData(data);
				response.setMessage("Payment By CashValue successful.");
			}else
			{
				response.setMessage(result.getErrorMessageEN());
			}
		
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
			response.setReturnCode(responseResult.getReturnCode());
			response.setMessage(responseResult.getErrorMessageEN());
			
		}catch(Exception e){
			logger.error(MMSController.class.getName() + " settleMMSPaymentByCashValue failed!", e);
			response.setReturnCode("1");	
			response.setMessage(e.getMessage());
		}
		
		return response;
	}
	
	@RequestMapping(value = "/encode", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult aesEncode(@RequestParam(value = "content", required=true) String content){
		String encryptContent = mmsService.aesEncode(content+TOKEN_KEY, TOKEY_KEY_UUID);
		responseResult.initResult(GTAError.FacilityError.SUCCESS);
		responseResult.setData(encryptContent);
		
		return responseResult;
	}
	
	
	@RequestMapping(value = "/decode", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult aesDecode(@RequestParam(value = "content", required=true) String content){
		String decryptContent = mmsService.aesDecode(content, TOKEY_KEY_UUID);
		
		responseResult.initResult(GTAError.FacilityError.SUCCESS);
		responseResult.setData(decryptContent.substring(0, decryptContent.indexOf(TOKEN_KEY)));
		return responseResult;
	}
	
	@RequestMapping(value="/settleMMSPaymentByGuestFolio",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SettleMMSPaymentByGuestFoliorResponseDto settleMMSPaymentByGuestFolio(String memberId,BigDecimal amount,String invoiceId,String transactionId,String transactionTimestamp){
		SettleMMSPaymentByGuestFoliorResponseDto response = new SettleMMSPaymentByGuestFoliorResponseDto();
		response.setReturnCode("0");
		SettleMMSPaymentByGuestFoliorResponseDto.Data  data = response.new Data();
		data.setOrderNo("123456");
		data.setTransactionNo("12345");
		data.setSettleTimestamp("2015-12-30 23:59:59");
		response.setData(data);
		return response;
	}
	
	@RequestMapping(value="/cancelSpaReservation",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CancelReservationResponseDto cancelReservation(String memberId,String appointmentId,String invoiceId){
		CancelReservationResponseDto response = new CancelReservationResponseDto();
		response.setReturnCode("0");	
		CancelReservationResponseDto.Data data = response.new Data();
		response.setData(data);
        return response;
	}

	@RequestMapping(value="/cancelSpaPayment",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CancelSpaPaymentResponseDto cancelSpaPayment(String memberId,String invoiceId,String transactionId){
		CancelSpaPaymentResponseDto response = new CancelSpaPaymentResponseDto();
		response.setReturnCode("0");	
		CancelSpaPaymentResponseDto.Data data = response.new Data();
		data.setUniqueIdentifier("1234");
		response.setData(data);
        return response;
	}
}
