package com.sinodynamic.hkgta.controller.upload;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Controller
public class DownloadContoller extends ControllerBase<File> {

	private static final String RELATE_PATH = "/files/";

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	@ResponseBody
	public void downloadFile(@RequestParam("fileName") String fileName,
			HttpServletRequest request, HttpServletResponse response) {

		try {
			if (StringUtils.isEmpty(fileName)) {

				exceptionHandler(new GTACommonException(
						GTAError.CommonError.UNEXPECTED_EXCEPTION,
						"Parameter finame is required!"), response, request);

				return;
			}

			String filePath = request.getServletContext().getRealPath("/") + RELATE_PATH + java.net.URLDecoder.decode(fileName, "ASCII");
            
			FileDownloader downloader = new FileDownloader(filePath);
			FileDownloader.Result rs = downloader.downLoad(request, response);

			if (rs.equals(FileDownloader.Result.SUCCESS)) {
				return;
			} else {
				exceptionHandler(new GTACommonException(
						GTAError.CommonError.UNEXPECTED_EXCEPTION,
						"Error: " + rs.name()), response, request);
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static class FileDownloader {
		public static enum Result {
			SUCCESS,

			FILE_NOT_FOUND,

			READ_WRITE_FAIL,

			UNKNOWN_EXCEPTION;
		}

		public static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

		public static final String DEFAULT_CONTENT_TYPE = "application/force-download";

		private String filePath;

		private String contentType = DEFAULT_CONTENT_TYPE;

		private int bufferSize = DEFAULT_BUFFER_SIZE;

		public FileDownloader(String filePath) {
			this.filePath = filePath;
		}

		public Result downLoad(HttpServletRequest request,
				HttpServletResponse response) {
			try {
				
				File file = new File(filePath);
				if (!file.exists()){
					throw new FileNotFoundException("FileNotFoundException: filePath");
				}
			
				downLoadFile(request, response, file);
			} catch (Exception e) {
				if (e instanceof FileNotFoundException)
					return Result.FILE_NOT_FOUND;
				if (e instanceof IOException)
					return Result.READ_WRITE_FAIL;

				return Result.UNKNOWN_EXCEPTION;
			}

			return Result.SUCCESS;
		}

		private void downLoadFile(HttpServletRequest request,
				HttpServletResponse response, File file) throws IOException {

			String fileName = file.getName();
			int length = (int) file.length();
			int begin = 0;
			int end = length - 1;

			response.setContentType(contentType);
			response.setContentLength(length);
			response.setHeader("Content-Disposition", "attachment;filename="
					+ fileName);

			String contentRange = String.format("bytes %d-%d/%d", begin, end,
					length);
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Range", contentRange);
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);

			downloadFileByRange(response, file, begin, end);
		}

		private void downloadFileByRange(HttpServletResponse response,
				File file, int begin, int end) throws IOException {
			InputStream is = null;
			OutputStream os = null;

			try {
				byte[] b = new byte[bufferSize];
				is = new BufferedInputStream(new FileInputStream(file));
				os = new BufferedOutputStream(response.getOutputStream());

				is.skip(begin);

				for (int i, left = end - begin + 1; left > 0
						&& ((i = is.read(b, 0, Math.min(b.length, left))) != -1); left -= i)
					os.write(b, 0, i);

				os.flush();
			} finally {
				if (is != null) {
					is.close();
				}
				if (os != null) {
					os.close();
				}
			}
		}
	}

}
