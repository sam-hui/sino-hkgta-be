package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.ListPageDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.ErrorCodeException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype") 
public class ServicePlanController extends ControllerBase{
	@Autowired
	private ServicePlanService servicePlanService  ;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	
	
	public Logger logger = Logger.getLogger(ServicePlanController.class);

	private static ResponseMsg verifyServicePlan(ServicePlanDto servicePlanJson,ResponseMsg status){
			
		
		if(StringUtils.isEmpty(servicePlanJson.getPlanName())){
			status.initResult(GTAError.DayPassError.ERRORMSG_PLANNAME_NULL);
			return status;		
		}	
		
		if(StringUtils.isEmpty(servicePlanJson.getPassPeriodType())){
			status.initResult(GTAError.DayPassError.ERRORMSG_PASSPERIODTYPE_NULL);
			return status;		
		}	
		
		
		if(servicePlanJson.getEffectiveEndDate()==null || servicePlanJson.getEffectiveStartDate()==null){
			status.initResult(GTAError.DayPassError.ERRORMSG_DATE_NULL);	
			return status;		
		}
				
						
		if(servicePlanJson.getContractLengthMonth()== null){
			status.initResult(GTAError.DayPassError.ERRORMSG_CONTRACTLENGTHINMONTHS_NULL);	
			return status;		
		}
		
		if(servicePlanJson.getAgeRange()== null){
			status.initResult(GTAError.DayPassError.ERRORMSG_AGERANGE_NULL);	
			return status;		
		}
		
		
		if(servicePlanJson.getPosServiceItemPrice()== null){
			status.initResult(GTAError.DayPassError.ERRORMSG_PRICE_NULL);	
			return status;		
		}
			
		
//		ResponseMsg status = new ResponseMsg();
		if(servicePlanJson.getEffectiveStartDate().compareTo(servicePlanJson.getEffectiveEndDate())>0){
//			status.setReturnCode(ResponseMsgConstant.ERRORCODE_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE);
//			status.setErrorMessageEN(ResponseMsgConstant.ERRORMSG_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE_EN);
			status.initResult(GTAError.DayPassError.ERRORMSG_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE);		
			return status;
		}
		if(StringUtils.isEmpty(servicePlanJson.getPlanName())){
//			status.setReturnCode(ResponseMsgConstant.ERRORCODE_PLANNAME_NULL);
//			status.setErrorMessageEN(ResponseMsgConstant.ERRORMSG_PLANNAME_NULL_EN);	
			status.initResult(GTAError.DayPassError.ERRORMSG_PLANNAME_NULL);	
			return status;		
		}
		status.initResult(GTAError.Success.SUCCESS);
		return status;				
	}
	
	/* (non-Javadoc)
	 * @see com.hkgta.webservice.crm.ServicePlanService#saveServicePlan(com.hkgta.json.ServicePlanJson)
	 */
	@RequestMapping(value = "/serviceplan/save_serviceplan", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseMsg saveServicePlan(HttpServletRequest request){
		try{
					
			ServicePlanDto servicePlanJson = (ServicePlanDto) parseJson(request, ServicePlanDto.class);		
			ResponseMsg validationError = verifyServicePlan(servicePlanJson,responseMsg);
			if (!GTAError.Success.SUCCESS.getCode().equals(validationError.getReturnCode())) {
				return validationError;
			}
			
			servicePlanJson.setCreateBy(this.getUser().getUserId());
			servicePlanJson.setCreateDate(new Date());
			servicePlanJson.setUpdateBy(this.getUser().getUserId());
			servicePlanJson.setUpdateDate(new Date());
			responseMsg =  servicePlanService.saveServicePlan(servicePlanJson);
			return responseMsg;
			
			
		}catch(ErrorCodeException e){
			responseMsg.initResult(e.getErrorCode(),e.getArgs());
			return responseMsg;
		}
		catch(Exception e){
			logger.error(e);
			responseMsg.initResult(GTAError.DayPassError.FAIL_SAVE_SRV_PLAN);
			return responseMsg;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.hkgta.webservice.crm.ServicePlanService#saveServicePlan(com.hkgta.json.ServicePlanJson)
	 */
	@RequestMapping(value = "/serviceplan/update_serviceplan", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg updateServicePlan(HttpServletRequest request) {
		try{
			ServicePlanDto servicePlanJson = (ServicePlanDto) parseJson(request, ServicePlanDto.class);
			ResponseMsg validationError = verifyServicePlan(servicePlanJson,responseMsg);
			if (!GTAError.Success.SUCCESS.getCode().equals(validationError.getReturnCode())) {
				return validationError;
			}
			
			servicePlanJson.setUpdateBy(this.getUser().getUserId());
			servicePlanJson.setUpdateDate(new Date());
			return servicePlanService.updateServicePlan(servicePlanJson);
		}catch(ErrorCodeException e){
			responseMsg.initResult(e.getErrorCode(),e.getArgs());
			return responseMsg;
		}
		catch(Exception e){
			logger.error(e);
			responseMsg.initResult(GTAError.DayPassError.FAIL_UPD_SRV_PLAN);
			return responseMsg;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.hkgta.webservice.crm.ServicePlanService#getServicPlans(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/serviceplan/get_allserviceplan", method = RequestMethod.GET)
	@ResponseBody
	public List<ServicePlanDto> getAllServicPlan() {
		try {
			logger.info("serviceplan list retrieve start...");
			List<ServicePlanDto> retList = servicePlanService.getAllServicPlan();
			return retList;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@RequestMapping(value = "/serviceplan/get_serviceplan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getServicPlan(ServicePlanDto model) {
		try {
			logger.info("serviceplan retrieve start...");
			ServicePlanDto planJson = new ServicePlanDto();
			planJson.setPlanName(model.getPlanName());
			ServicePlanDto servicePlanDto = servicePlanService.getServicPlan(planJson);
			ResponseResult result = new ResponseResult("0", "success", servicePlanDto);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			ResponseResult result = new ResponseResult("-1", "", "fail");
			return result;
		}
	}
	@RequestMapping(value = "/serviceplan/delete_serviceplan/{planNo}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseMsg deleteServicePlan(@PathVariable(value="planNo") Long planNo) {
		ResponseMsg status = null;
		//NULL/empty-paid but not confirmed; SUC - payment success; NSC - payment not success; CAN - payment cancelled
		if(this.servicePlanService.isServicePlanUsed(planNo)){
			responseMsg.initResult(GTAError.DayPassError.SRV_IS_USED);
			//status = new ResponseMsg("-1", "serviceplan is used by other, cannot be deleted");
			return responseMsg;
		}
		status = servicePlanService.deleteServicPlan(planNo);
		return status;
	}
	@RequestMapping(value = "/serviceplan/duplicate_serviceplan/{planNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg duplicateServicePlan(@PathVariable(value="planNo") Long planNo) {
		String userId = this.getUser().getUserId();
		ResponseMsg status = servicePlanService.duplicateServicePlan(planNo,userId);
		return status;
	}
	@RequestMapping(value = "/serviceplan/change_serviceplan_status/{planNo}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg changeServicePlanStatus(@PathVariable(value="planNo") Long planNo, @RequestParam("status") String status) {
		ResponseMsg responseMsg = null;
		//NULL/empty-paid but not confirmed; SUC - payment success; NSC - payment not success; CAN - payment cancelled
//		if(this.servicePlanService.isServicePlanUsed(planNo, "SUC")){
//			status = new ResponseMsg("-1", "chenggong", "serviceplan is used by other, cannot change status");
//			return status;
//		}
		String userId = this.getUser().getUserId();
		responseMsg = servicePlanService.changeServicePlanStatus(planNo,status,userId);
		return responseMsg;
		
	}
	@RequestMapping(value = "/serviceplan/get_serviceplan_byid/{planNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getServicePlanById(@PathVariable(value="planNo") Long planNo) {
		try {
			logger.info("serviceplan retrieve start...");
			ServicePlanDto planJson =  servicePlanService.getServicPlanById(planNo);
			ResponseResult result = new ResponseResult("0", "success", planJson);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			ResponseResult result = new ResponseResult("-1","", "fail");
			return result;
		}
		
		
	}
	@RequestMapping(value = "/serviceplan/get_serviceplanpage", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult getPageList(@RequestBody ListPageDto planPageDto) {
		ResponseResult responseResult = servicePlanService.getPageList(planPageDto);
		return responseResult;
	}
	
	@RequestMapping(value = "/serviceplan/list", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="", required = false) String status,
			@RequestParam(value="isAscending",defaultValue="true", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		String joinSQL = servicePlanService.getServicePlans(typeMap);
		if(!"All".equalsIgnoreCase(status) && !StringUtils.isEmpty(status)){
			joinSQL += " and sub.status= '"+status+"'  ";
		}
		ResponseResult tmpResult = null;
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL = joinSQL + " AND ";
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, ServicePlanDto.class, typeMap);
		}
		else
		{
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, ServicePlanDto.class, typeMap);
		}
		
		for(Object sp : tmpResult.getListData().getList())
		{
			((ServicePlanDto)sp).setEffectiveStartDate((DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((ServicePlanDto)sp).getEffectiveStartDate(), "yyyy-MM-dd"))));
			((ServicePlanDto)sp).setEffectiveEndDate((DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((ServicePlanDto)sp).getEffectiveEndDate(), "yyyy-MM-dd"))));
		}
		
		return tmpResult;
	}

	
	@RequestMapping(value ="/serviceplan/{type}/{id}/{activationDate}/{deactivationDate}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getServiceplanByDate(
			@PathVariable(value="type") String type,
			@PathVariable(value="id") String id,
			@PathVariable(value="activationDate") String activationDate,
			@PathVariable(value="deactivationDate") String deactivationDate,
			HttpServletRequest request, HttpServletResponse response) {	
		try {
				DaypassPurchaseDto dto =null;
				if(StringUtils.equals("STAFF", type)){
					 dto = new DaypassPurchaseDto(null, id, activationDate, deactivationDate);
				}else{
					 dto = new DaypassPurchaseDto(Long.valueOf(id), "", activationDate, deactivationDate);
				}
				return servicePlanService.getServiceplanByDate(dto);
		} catch (Exception e) {
			logger.error("get day pass failed!", e);
			responseResult.initResult(GTAError.DayPassError.GET_DAY_PASS_FAILED);
			return responseResult;
		}
	}
	
	@RequestMapping(value ="/serviceplan/getValidDaypass", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getValidDaypass() {	
		try {
			List<ServicePlanDto> list = servicePlanService.getValidDaypass();
			responseResult.initResult(GTAError.Success.SUCCESS);
			responseResult.setData(list);
			return this.responseResult;
		} catch (Exception e) {
			logger.error("get day pass failed!", e);
			responseResult.initResult(GTAError.DayPassError.GET_DAY_PASS_FAILED);
			return responseResult;
		}
	}
	
	@RequestMapping(value ="/serviceplan/getDailyDayPassUsage", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDailyDayPassUsage(
			@RequestParam(value = "sortBy", defaultValue = "purchaseDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "filters", required=false) String filters,
			@RequestParam(value = "date",  required = true) String date) throws Exception{	
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, servicePlanService.getDailyDayPassUsage(date, sortBy, (isAscending.equals("true") ? "asc" : "desc"), pageSize, pageNumber, query));
		return this.responseResult;
	}
/**	
	@RequestMapping(value = "/serviceplan/get_daypass_list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDayPassPageList(int pageSize,int pageNumber,String propertyName, String order ) 
	{
		DaypassDto dto = new DaypassDto();
		dto.setPageNumber(pageNumber);
		dto.setPageSize(pageSize);
		dto.setPropertyName(propertyName);
		dto.setOrder(order);
		return servicePlanService.getDayPassPageList(dto);
	}
*/		
	
}
