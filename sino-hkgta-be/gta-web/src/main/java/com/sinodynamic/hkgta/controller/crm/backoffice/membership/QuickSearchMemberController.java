package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberQuickSearchService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/membership")
public class QuickSearchMemberController extends ControllerBase {

	@Autowired
	MemberQuickSearchService memberQuickSearchService;

	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "/manualSearch/{number}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult quickSeachMember(@PathVariable("number") String number) {

		try {
			return memberQuickSearchService.quickSeachMember(number);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("QuickSearchMemberController.quickSeachMember Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/profile/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult displayQuickSeachMemberProfile(@PathVariable("customerID") Long customerID) {
		logger.info("QuickSearchMemberController.displayQuickSeachMemberProfile start ...");
		try {
			CustomerProfile cp = customerProfileService.getByCustomerID(customerID);
			responseResult.initResult(GTAError.Success.SUCCESS, cp);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("QuickSearchMemberController.displayQuickSeachMemberProfile Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/membertype/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberType(@PathVariable("customerID") Long customerID) {
		logger.info("QuickSearchMemberController.getMemberType start ...");
		try {
			String memberType = memberService.getMemberTypeByCustomerId(customerID);
			responseResult.initResult(GTAError.Success.SUCCESS);
			responseResult.setData(memberType);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("QuickSearchMemberController.getMemberType Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
}
