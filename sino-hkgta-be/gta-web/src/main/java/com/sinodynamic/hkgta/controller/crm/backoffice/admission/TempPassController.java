package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.ContractHelperDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.crm.TempPassProfileDto;
import com.sinodynamic.hkgta.dto.crm.TempPassTypeDetailsDto;
import com.sinodynamic.hkgta.dto.crm.TempPassTypeDto;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.TempPassService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/temppass")
public class TempPassController extends ControllerBase
{

	private Logger logger = Logger.getLogger(TempPassController.class);

	@Autowired
	private TempPassService tempPassService;
	
    @Autowired
	private AdvanceQueryService advanceQueryService;

	/**
	 * This method is used to create a TempPass profile.
	 * 
	 * @param ContractHelperDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/profiles", method = RequestMethod.POST)
	public @ResponseBody ResponseResult createTempPassProfile(@RequestBody ContractHelperDto dto)
	{

		try
		{
			String userId = super.getUser().getUserId();
			dto.setCreateBy(userId);
			dto.setUpdateBy(userId);
			return tempPassService.createTempPassProfile(dto);

		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " createTempPassProfile Failed!", e);
			responseResult.initResult(GTAError.TempPassError.FAIL_CREATE_TEMP_PROFILE);
			return responseResult;
		}
	}

	/**
	 * This method is used to retrieve list of TempPass.
	 * 
	 * @param status
	 * @param sortBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return ResponseResult (list of cards, pagination details etc)
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/profiles/list", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult getTempPassProfileList(
			@RequestBody(required = false) AdvanceQueryDto filters,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "history", required = false, defaultValue = "N") String history,
			@RequestParam(value = "sortBy", required = false, defaultValue = "createDate") String sortBy, 
			@RequestParam(value = "isAscending", required = false, defaultValue = "false") String isAscending, 
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber, 
			@RequestParam(value = "pageSize", required = false, defaultValue = "3") int pageSize) {

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try
		{
			String joinHQL = null;
			if ("N".equalsIgnoreCase(history))
				joinHQL = tempPassService.getTempPassProfileList(status);
			else {
				joinHQL = tempPassService.getTempPassProfileHistoryList(status);
			}
			
			
			if (filters != null && filters.getRules().size() > 0 && filters.getGroupOp() != null && filters.getGroupOp().length() > 0)
			{
				if (joinHQL.indexOf("WHERE") > 0) {
					joinHQL += " AND ";
				} else {
					joinHQL += " WHERE ";
				}
				
				filters.setSortBy(sortBy);
				filters.setAscending(isAscending);
				//added by vicky wang to implement the advance search of Temp pass by cardId.
				for(SearchRuleDto rule: filters.getRules()){
					if(logger.isDebugEnabled()){
						logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:" + rule.getData());
					}
					if("cardId".equals(rule.field)&&(rule.data.toUpperCase())!=null){
						String tempId = rule.data.toUpperCase();
						//below if logic is mainly for 'contain' & 'startWith'
						if("TP".equalsIgnoreCase(tempId) || "T".equalsIgnoreCase(tempId)){
						   if("cn".equals(rule.getOp()) || "bw".equals(rule.getOp()) || "bn".equals(rule.getOp()))
							{
							   rule.setData("");
							}
						}else if("P".equalsIgnoreCase(tempId) && "cn".equals(rule.getOp())){							
							//this logic is for 'Contain' 'P'
							rule.setData("");
						}else if(tempId.startsWith("TP")){
						
							try{
							int cardNo = Integer.parseInt(tempId.substring(2));
							if(cardNo ==0)  //for the case that input value like 'TP00'
							{
								rule.setData("");
							}else{
								rule.setData(String.valueOf(cardNo));
							}						    
							}catch(Exception e){
								//if input cardId is invalid,  keep the input value for search.
								logger.error("input cardId is invalid:" + e.getMessage());
							}
						}else
						{
							try {
								int cardNo = Integer.parseInt(tempId);
								rule.setData(String.valueOf(cardNo));
							}catch(Exception e){
								//if input cardId is invalid,  keep the input value for search.
								logger.error("input cardId is invlid:" + e.getMessage());
							}
						}
					}
				}
				//add end  for advance search.
				return advanceQueryService.getAdvanceQueryCustomizedResultBySQL(filters, joinHQL, page, TempPassProfileDto.class);			
			}
			else
			{
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				return advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, TempPassProfileDto.class);
			}

		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " getTempPassProfileList Failed!", e);
			responseResult.initResult(GTAError.TempPassError.FAIL_RETRIEVE_TEMP_PROFILES);
			return responseResult;
		}
	}


	/**
	 * This method is to update the status of Contractor and status of
	 * associated card
	 * 
	 * @param ContractHelperDto
	 * @param customerId
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/profiles", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateProfile(@RequestBody ContractHelperDto dto)
	{
		LoginUser user = super.getUser();
		if (user == null)
		{

			logger.error("Current User is null!");
			responseResult.initResult(GTAError.TempPassError.CURRENT_USER_NULL);
			return responseResult;
		}

		String userId = user.getUserId();
		dto.setUpdateBy(userId);
		dto.setUpdateDate(new Date());

		try
		{
			return tempPassService.updateProfile(dto);

		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " changeProfileStatus Failed!", e);
			responseResult.initResult(GTAError.TempPassError.FAIL_UPDATE_PROFILE);

			return responseResult;
		}

	}

	/**
	 * This method is to get the details of TempPass profile Details by
	 * contractorId
	 * 
	 * @param contractorId
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/profiles/{contractorId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getProfileDetails(@PathVariable(value = "contractorId") String contractorId)
	{

		try
		{
			return tempPassService.getProfileDetails(contractorId);

		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " getProfileDetails Failed!", e);
			responseResult.initResult(GTAError.TempPassError.GET_PROFILE_DETAILS_FAIL);
			return responseResult;

		}

	}

	@RequestMapping(value = "/passtype", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult createTempPassType(@RequestBody TempPassTypeDetailsDto tempPassTypeDetail)
	{
		LoginUser currentUser = getUser();
		return tempPassService.createTempPassType(tempPassTypeDetail,currentUser.getUserId());
	}

	@RequestMapping(value = "/passtype/{passTypeId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateTempPassType(@RequestBody TempPassTypeDetailsDto tempPassTypeDetail, @PathVariable(value = "passTypeId") Long passTypeId)
	{
		LoginUser currentUser = getUser();
		tempPassTypeDetail.setTempPassId(passTypeId);
		return tempPassService.updateTempPassType(tempPassTypeDetail,currentUser.getUserId());
	}

	@RequestMapping(value = "/passtype/status/{passTypeId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult changeTempPassStatus(@RequestBody TempPassTypeDetailsDto tempPassTypeDetail, @PathVariable(value = "passTypeId") Long passTypeId)
	{
		tempPassTypeDetail.setTempPassId(passTypeId);
		return tempPassService.changeTempPassStatus(tempPassTypeDetail);
	}

	@RequestMapping(value = "/passtype/clone/{passTypeId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult cloneTempPassType(@PathVariable(value = "passTypeId") Long passTypeId)
	{
		LoginUser currentUser = getUser();
		return tempPassService.cloneTempPassTypeById(passTypeId,currentUser.getUserId());
	}

	@RequestMapping(value = "/passtype/{passTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTempPassType(@PathVariable(value = "passTypeId") Long passTypeId)
	{
		return tempPassService.getTempPassTypeById(passTypeId);
	}

	@RequestMapping(value = "/passtype/{passTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteTempPassType(@PathVariable(value = "passTypeId") Long passTypeId)
	{
		try
		{
			return tempPassService.deleteTempPassTypeById(passTypeId);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getStackTrace());
			responseResult.initResult(GTAError.TempPassError.DELETE_TEMP_PASS_FAIL);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/passtype/list", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult getTempPassTypeList(
		@RequestBody(required = false) AdvanceQueryDto filters,
		@RequestParam(value = "status", required = false) String status, 
		@RequestParam(value = "sortBy", required = false, defaultValue="") String sortBy, 
		@RequestParam(value = "isAscending", required = false, defaultValue = "false") String isAscending, 
		@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber, 
		@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try
		{
			
			String joinHQL = tempPassService.getTempPassTypeList(status);
			ResponseResult rs = null;
			if (filters != null && filters.getRules().size() > 0 && filters.getGroupOp() != null && filters.getGroupOp().length() > 0)
			{
	
				joinHQL += " where ";
				filters.setSortBy(sortBy);
				filters.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(filters, joinHQL, page, TempPassTypeDto.class);			
			}
			else
			{
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, TempPassTypeDto.class);
			}
			
			if (rs != null && rs.getData() != null) {
			    
			    List<TempPassTypeDto> result = (List<TempPassTypeDto>) ((Data)rs.getData()).getList();
			    if (result != null && result.size() > 0) {
				for (TempPassTypeDto dto : result) {
				    dto.setStartDate((DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(dto.getStartDate(), "yyyy-MM-dd"))));
				    dto.setExpiryDate((DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(dto.getExpiryDate(), "yyyy-MM-dd"))));
				}
			    }
			}
			return rs;
		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " getTempPassTypeList Failed!", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * This method is used to link the card to a TempPass profile.
	 * 
	 * @param TempPassProfileDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/profiles/linkcard", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult linkCard(@RequestBody TempPassProfileDto dto)
	{
		try
		{
			String userId = super.getUser().getUserId();
			dto.setUpdateBy(userId);
			return tempPassService.linkCard(dto);

		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " linkCard Failed!", e);
			responseResult.initResult(GTAError.TempPassError.LINK_CARD_FAILED);
			return responseResult;
		}
	}

	/**
	 * This method is used to link the card to a TempPass profile.
	 * 
	 * @param TempPassProfileDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
//	@RequestMapping(value = "/profiles/replacecard", method = RequestMethod.PUT)
//	public @ResponseBody ResponseResult replaceCard(@RequestBody TempPassProfileDto dto)
//	{
//		try
//		{
//			String userId = super.getUser().getUserId();
//			dto.setUpdateBy(userId);
//			return tempPassService.replaceCard(dto);
//
//		}
//		catch (Exception e)
//		{
//			logger.error(TempPassController.class.getName() + " replaceCard Failed!", e);
//			responseResult.initResult(GTAError.TempPassError.FAIL_REPLACE_CARD);
//			return responseResult;
//		}
//	}

	/**
	 * This method is to get the details of TempPass profile Details by HKID
	 * 
	 * @param contractorId
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/profiles/hkid/{hkId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getProfileDetailsByHKID(@PathVariable(value = "hkId") String hkId)
	{

		try
		{
			return tempPassService.getProfileDetailsByHKID(hkId);

		}
		catch (Exception e)
		{
			logger.error(TempPassController.class.getName() + " getProfileDetailsByHKID Failed!", e);
			responseResult.initResult(GTAError.TempPassError.GET_PROFILE_DETAILS_BY_HKID_FAIL);
			return responseResult;

		}

	}
	
	
	/**
	 * This method is used by Scheduler job to update the status of Profile and the card that are expire today.
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/task/test", method = RequestMethod.PUT)
	@ResponseBody
	public void updateStatusForExpiredTempPass() {
		
		try {
			
			tempPassService.updateStatusForExpiredTempPass();

		} catch (Exception e) {
			
			e.printStackTrace();
		}

	} 
	
	
	@RequestMapping(value = "/card/return", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult returnCard(@RequestParam(value = "cardId") String cardId, @RequestParam(required = false, value = "contractorId") String contractorId) {

		try
		{
			return tempPassService.returnInternalPermitCard(cardId, getUser().getUserId(), contractorId);

		} catch (Exception e) {
			
			logger.error(TempPassController.class.getName() + " returnCard Failed!", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;

		}

	}
	
	
	@RequestMapping(value = "/card/disposal/{cardId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult disposalCard(@PathVariable(value="cardId") String cardId) {

		try
		{
			return tempPassService.disposalInternalPermitCard(cardId, getUser().getUserId());

		} catch (Exception e) {
			
			logger.error(TempPassController.class.getName() + " disposalCard Failed!", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;

		}

	}

}
