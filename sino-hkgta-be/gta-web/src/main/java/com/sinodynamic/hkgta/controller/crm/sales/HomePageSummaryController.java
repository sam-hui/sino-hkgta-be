package com.sinodynamic.hkgta.controller.crm.sales;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.UserLoginRequest;
import com.sinodynamic.hkgta.dto.crm.CustomerActionContentDto;
import com.sinodynamic.hkgta.dto.crm.HomePageSummaryDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.staff.SecurityQuestionDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.GtaAuthenticationToken;
import com.sinodynamic.hkgta.security.GtaInvocationSecurityMetadataSource;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.sales.HomePageSummaryService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class HomePageSummaryController extends ControllerBase<String>
{
	public Logger logger = Logger.getLogger(HomePageSummaryController.class);

	@Autowired
	private HomePageSummaryService homePageSummaryService;

	@Autowired
	private UserMasterService userMasterService;
	
	@Autowired
	private GtaInvocationSecurityMetadataSource sourceMap;
	
	@Autowired
	private MemberService memberService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;

	@RequestMapping(value = "/home/summary", method = RequestMethod.GET)
	public @ResponseBody ResponseResult countCustomerProfile()
	{
		logger.debug("HomePageSummaryController.countCustomerProfile invocation start ...");
		HomePageSummaryDto ret = null;

		try
		{
			String staffId = null;
			LoginUser user = getUser();
			if (user != null)
				staffId = user.getUserId();

			ret = homePageSummaryService.countNumber(staffId);
			responseResult.initResult(GTAError.Success.SUCCESS, ret);
			logger.debug("HomePageSummaryController.countCustomerProfile invocation end ...");

		}
		catch (Exception e)
		{

			logger.error(e.toString());
			responseResult.initResult(GTAError.LoginError.GET_SUMMARY_FAILED);
		}

		return responseResult;
	}

	@RequestMapping(value = "/home/news", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getLatestActionContent(@RequestParam(value = "totleSize", required = false, defaultValue = "20") String totleSize)
	{
		try
		{
			List<CustomerActionContentDto> customerActionContent = homePageSummaryService.getLatestActionContent(Integer.parseInt(totleSize));
			responseResult.initResult(GTAError.Success.SUCCESS, customerActionContent);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			responseResult.initResult(GTAError.LoginError.GET_NEWS_FAILED);
			return responseResult;
		}

		return responseResult;
	}

	@RequestMapping(value = { "/retriveMemberToken/{academyId}" }, method = RequestMethod.POST)
	public @ResponseBody ResponseResult retriveMemberToken(@PathVariable(value = "academyId") String academyNo) throws Exception
	{
		Member member = memberService.getMemberByAcademyNo(academyNo);
		if (null == member)
		{
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		}

		UserMaster usermaster = userMasterService.getUserByUserId(member.getUserId());
		if (usermaster != null)
		{

			GtaAuthenticationToken authenticationToken = new GtaAuthenticationToken(usermaster.getUserId(), usermaster.getPassword());
			Authentication authentication = this.authManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}
		else
		{
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		}

		String token = TokenUtils.getUUIDToken(member.getUserId(), "Mobile", "Mobile");
		String base64token = Base64.encodeBase64String((token.getBytes()));
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoginUser authUser = (LoginUser) auth.getPrincipal();
		
		LoginUserDto user = new LoginUserDto(authUser.getUserId(),
				authUser.getUserNo(), authUser.getUserName(),
				authUser.getUserType(), authUser.getFullname(), authUser.getPortraitPhoto(), authUser.getDateOfBirth(), authUser.getPositionTitle(), authUser.getFirstName(), authUser.getLastName(), authUser.getMemberType());

		user.setToken(base64token);
		try
		{
			userMasterService.SaveSessionToken(member.getUserId(), token, "Mobile");
			usermaster.setLastLoginFailTime(null);
			usermaster.setLoginFailCount(null);
			userMasterService.updateUserMaster(usermaster);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED);
			return responseResult;
		}

		Map<String, String> memberToken = new HashMap<String, String>();
		memberToken.put("memberToken", base64token);

		responseResult.initResult(GTAError.Success.SUCCESS, user);
		return responseResult;
	}
	
	@RequestMapping(value = {"/home/login", "/home/stafflogin", "/device/login", "/memberapp/login", "/device/stafflogin", "/device/saleskitlogin"} , method = RequestMethod.POST)	
	public @ResponseBody ResponseResult login(@RequestBody UserLoginRequest loginUser, HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "device", required = false) String device) throws IOException
	{
		String loginDevice = CommUtil.getAccessDevice(request.getHeader("User-Agent"));
		logger.info("Login Request From Device : " + loginDevice);
		logger.info("User Agent:" + request.getHeader("User-Agent"));
		
		String URI = request.getRequestURI();
		if (URI.contains("/device") || URI.contains("/memberapp"))
		{
			loginDevice = loginDevice.equals("UnKnown") ? "Mobile" : loginDevice;
		}
		
		loginUser.setLocation(getLocation(URI, loginUser));

		UserMaster usermaster = null;
		try
		{
			String username = loginUser.getUsername().trim();
			usermaster = userMasterService.getUserByLoginId(username);
			if (usermaster != null)
			{
				validateURI(usermaster, URI);
				if(usermaster.getUserType().equals(UserType.STAFF.getType()) && StringUtils.isEmpty(loginUser.getLocation()) && URI.contains("/home/stafflogin"))
				{
					responseResult.initResult(GTAError.LoginError.LOCATION_REQUIRED);
					return responseResult;
				}
				
				if (userMasterService.hasBeenLocked(usermaster))
				{
					responseResult.initResult(GTAError.LoginError.USER_HAS_BEEN_LOCKED, new Object[] { appProps.get(Constant.LOGIN_FAIL_LOCK_PERIOD) });
					return responseResult;
				}
				boolean requireValidate = userMasterService.requireValidateKaptchaCode(loginDevice) && userMasterService.requireValidateKaptchaCode(usermaster);
				if (requireValidate)
				{
					String kaptchaExpected = SessionMap.getInstance().getSession(request.getHeader("sessionId"));
					
					if (StringUtils.isEmpty(loginUser.getValidCode()) || StringUtils.isEmpty(kaptchaExpected) || !kaptchaExpected.equals(loginUser.getValidCode()))
					{
						userMasterService.updateLoginFailTime(usermaster);
						responseResult.initResult(StringUtils.isEmpty(loginUser.getValidCode()) ? GTAError.LoginError.VALID_CODE_REQUIRED : GTAError.LoginError.VALID_CODE_ERROR);
						return responseResult;
					}
				}
				String psw = CommUtil.getMD5Password(username, loginUser.getPassword());
				if(logger.isDebugEnabled()){
					logger.debug("username:" + username + " password:" + loginUser.getPassword() + " encrypt password:" + psw);					
				}
				GtaAuthenticationToken authenticationToken = new GtaAuthenticationToken(usermaster.getUserId(), psw);
				Authentication authentication = this.authManager.authenticate(authenticationToken);
				
				salesKitLoginValidation(URI, authentication);
				
				
				
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
			else
			{
				logger.error("userMaster is null, login failed");
				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
				return responseResult;
			}
		}
		catch (BadCredentialsException | UsernameNotFoundException e)
		{
			try
			{
				userMasterService.updateLoginFailTime(usermaster);
			}
			catch (Exception e1)
			{
				String[] args = { usermaster.getUserId(), DateConvertUtil.getStrFromDate(new Date()) };
				logger.warn(getI18nMessge(GTAError.LoginError.UPDATE_LOGIN_FAIL_TIME_FAILED.getCode(), args), e1);
				e1.printStackTrace();
			}
			e.printStackTrace();
			if (userMasterService.requireValidateKaptchaCode(loginDevice) && userMasterService.requireValidateKaptchaCode(usermaster))
			{
				responseResult.initResult(new GTAError[] { GTAError.LoginError.LOGIN_FAILED, GTAError.LoginError.VALID_CODE_REQUIRED });
			}
			else
			{
				logger.error("login faild.",e);
				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
			}
			return responseResult;

		}
		catch (GTACommonException e)
		{
			if (StringUtils.isEmpty(e.getErrorMsg()))
			{
				responseResult.initResult(e.getError());
			}
			else
			{
				responseResult.initResult(e.getError(), e.getErrorMsg());
			}
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error("login faild.",e);
			responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
			return responseResult;
		}
		

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoginUser authUser = (LoginUser) auth.getPrincipal();
		String token = TokenUtils.getUUIDToken(authUser.getUserId(), loginDevice, loginUser.getLocation());
		String base64token = Base64.encodeBase64String((token.getBytes()));

		try
		{
			userMasterService.SaveSessionToken(authUser.getUserId(), token, loginDevice);
			usermaster.setLastLoginFailTime(null);
			usermaster.setLoginFailCount(null);
			userMasterService.updateUserMaster(usermaster);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED);
			return responseResult;
		}
		
		if (null == usermaster.getPasswdChangeDate())
		{
			response.addHeader("token", base64token);
			responseResult.initResult(GTAError.LoginError.REQUIRE_TO_CHANGE_PSW, (Object) authUser.getUserId());
			return responseResult;
		}
		

		LoginUserDto user = new LoginUserDto(authUser.getUserId(),
				authUser.getUserNo(), authUser.getUserName(),
				authUser.getUserType(), authUser.getFullname(), authUser.getPortraitPhoto(), authUser.getDateOfBirth(), authUser.getPositionTitle(), authUser.getFirstName(), authUser.getLastName(), authUser.getMemberType());

		responseResult.initResult(GTAError.LoginError.LOGIN_SUCCESS, user);
		SessionMap.getInstance().addLocation(user.getUserId(), loginUser.getLocation());
		response.addHeader("token", base64token);
		return responseResult;
	}
	
	
	
	private void salesKitLoginValidation(String URI, Authentication authentication)
	{
		String skLoginURI = "/saleskitlogin";
		if (!URI.contains(skLoginURI)) return;
		
		
		Collection<ConfigAttribute> accessRoles = sourceMap.getResourceMap().get(skLoginURI);
		if (null == accessRoles) return;
		
		Collection<GrantedAuthority> Authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
		StringBuffer userRole = new StringBuffer();
		for(GrantedAuthority a : Authorities)
		{
			userRole.append(a.getAuthority()).append(",");
		}
		
		String roles = userRole.toString();
		Iterator<ConfigAttribute> iterator = accessRoles.iterator();
		
		boolean hasAccessRight = false;
		while(iterator.hasNext())
		{
			if (roles.contains(iterator.next().getAttribute())) hasAccessRight = true;
			break;
		}
		
		if (!hasAccessRight)
		{
			throw new GTACommonException(GTAError.LoginError.LOGIN_FAILED);
		}
	}

	private String getLocation(String URI, UserLoginRequest loginUser)
	{
		if (!StringUtils.isEmpty(loginUser.getLocation()))
		{
			return loginUser.getLocation();
		}
			
		if (URI.contains("/device") || URI.contains("/memberapp"))
		{
			return "Mobile";
		}
		if (URI.contains("/home/login"))
		{
			return "WP";
		}
		
		return "";
	}

	private void validateURI(UserMaster usermaster, String URI)
	{
		String userType = usermaster.getUserType();
		
		if ((userType.equals(UserType.STAFF.getType()) || userType.equals(UserType.ADMIN.getType())) && (!(URI.contains("/stafflogin") || URI.contains("/saleskitlogin") || URI.contains("/memberapp/login"))))
		{
			throw new GTACommonException(GTAError.LoginError.LOGIN_FAILED);
		}
		
		if (userType.equals(UserType.CUSTOMER.getType()) && !URI.contains("/login"))
		{
			throw new GTACommonException(GTAError.LoginError.LOGIN_FAILED);
		}
		
	}

	@RequestMapping(value = "/member/securityQuestion", method = RequestMethod.POST)
	public @ResponseBody ResponseResult setSecurityQuestion(@RequestBody SecurityQuestionDto questions) throws Exception
	{
		return memberService.setSecurityQuestion(questions);
	}

	@RequestMapping(value = "/home/logout", method = RequestMethod.GET)
	public @ResponseBody ResponseResult logout(HttpServletRequest request,@RequestParam(required=false) String arnApplication) throws IOException
	{
		String userId = super.getUser().getUserId();
		String device = CommUtil.getAccessDevice(request.getHeader("User-Agent"));

		boolean pass = userMasterService.deleteSessionToken(userId, device);

		if (pass)
		{
			if (!StringUtils.isEmpty(arnApplication) && (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IPhone")))
			{
				devicePushService.deleteUserDevice(userId,arnApplication);
			}
			
			if (!userMasterService.requireValidateKaptchaCode(device))
			{
				responseResult.initResult(GTAError.LoginError.LOGOUT_SUCCESS);
			}
		}
		else
		{
			responseResult.initResult(GTAError.LoginError.LOGOUT_FAILED);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/home/resetIdAndPassword", method = RequestMethod.POST)
	public @ResponseBody ResponseResult resetLoginIdAndPassword(){
		
		try {
			List<Member> members = memberService.getAllMembers();
			if (members == null || members.size() == 0) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
				return responseResult;
			}
			String defaultPassword = "123456";
			userMasterService.setAllLoginIdAndPassword(members, defaultPassword);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
			
		}
		
	}

}