package com.sinodynamic.hkgta.controller.crm.backoffice.coachmanage;


import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo;
import com.sinodynamic.hkgta.service.crm.backoffice.coachmanage.CoachManagementService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/coach_management")
public class CoachManagementController extends ControllerBase{
	@Autowired
	CoachManagementService coachManagementService;
	//Save or update
	@RequestMapping(value="/present_roster", method={RequestMethod.POST,RequestMethod.PUT})
	@ResponseBody
	public ResponseMsg savePresentRoster(@RequestBody CoachRosterInfo rosterInfo){
		try{
			ResponseMsg msg = verifyPrice(rosterInfo);
			if(msg !=null){
				return msg;
			}
			coachManagementService.savePresentRoster(this.getUser().getUserId(), rosterInfo);
			this.responseMsg.initResult(GTAError.Success.SUCCESS);
			return this.responseMsg;
		}catch(GTACommonException e){
			this.responseMsg.initResult(e.getError(), e.getArgs());
			return this.responseMsg;
		}
	}
	@RequestMapping(value="/present_roster/{coach_id}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPresentRoster(@PathVariable("coach_id") String coach_id){
		try{
			CoachRosterInfo info = coachManagementService.loadPresentRoster(coach_id);
			this.responseResult.initResult(GTAError.Success.SUCCESS, info);
			return this.responseResult;
		}catch (GTACommonException e){
			this.responseResult.initResult(e.getError(),e.getArgs());
			return this.responseResult;
		}
	}

	
	@RequestMapping(value="/customized_roster", method={RequestMethod.POST})
	@ResponseBody
	public ResponseMsg saveCustomizedRoster(@RequestBody CoachRosterInfo rosterInfo){
		try{
			coachManagementService.saveCustomizedRoster(this.getUser().getUserId(), rosterInfo);
			this.responseMsg.initResult(GTAError.Success.SUCCESS);
			return this.responseMsg;
		}catch(GTACommonException e){
			this.responseMsg.initResult(e.getError(), e.getArgs());
			return this.responseMsg;
		}
	}
	
	@RequestMapping(value="/customized_roster/{coach_id}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCustomizedRoster(@PathVariable("coach_id") String coach_id, @RequestParam("start") String start, @RequestParam("end") String end){
		try{
			Date dateStart = null;
			Date dateEnd = null;
			try{
				String[] patterns = new String[]{"yyyy-MM-dd"};
				dateStart = DateUtils.parseDate(start, patterns);
				dateEnd = DateUtils.parseDate(end, patterns);
				dateEnd = DateUtils.addSeconds(DateUtils.addDays(dateEnd, 1), -1);
			}catch (Exception e){
				throw new GTACommonException(GTAError.CoachMgrError.FAIL_PARSE_DATE);
			}
			CoachRosterInfo info = coachManagementService.loadCustomizedRoster(coach_id, dateStart, dateEnd);
			this.responseResult.initResult(GTAError.Success.SUCCESS, info);
			return this.responseResult;
		}catch (GTACommonException e){
			this.responseResult.initResult(e.getError(),e.getArgs());
			return this.responseResult;
		}
	}
	private ResponseMsg verifyPrice(CoachRosterInfo info){
		//Low Rate can not null
		if(null==info.getLowRatePrice() || null==info.getHighRatePrice()){
			this.responseMsg.initResult(GTAError.DayPassError.ERRORMSG_RATE_NULL);	
			return this.responseMsg;		
		}	
		//Low Rate can not less than 0
		if(info.getLowRatePrice().compareTo(BigDecimal.ZERO) < 0){
			this.responseMsg.initResult(GTAError.DayPassError.ERRORMSG_RATE_LOWERTHAN_ZERO);	
			return this.responseMsg;		
		}	
		//High Rate can null, if have can not less than 0
		if(info.getLowRatePrice()!=null && info.getHighRatePrice().compareTo(BigDecimal.ZERO) < 0){
			this.responseMsg.initResult(GTAError.DayPassError.ERRORMSG_RATE_LOWERTHAN_ZERO);	
			return this.responseMsg;	
		}
		//Higt Rate >= Low Rate
		if(info.getHighRatePrice()!=null && info.getLowRatePrice().compareTo(info.getHighRatePrice()) > 0){
			this.responseMsg.initResult(GTAError.DayPassError.ERRORMSG_LOWRATE_GREATERTHAN_HIGHRATE);	
			return this.responseMsg;		
		}	
		return null;

	}
}
