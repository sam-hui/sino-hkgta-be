package com.sinodynamic.hkgta.controller.fms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.service.fms.FacilityEmailService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * email controller
 * @author Mason_Yang
 *
 */
@Controller
public class FacilityEmailController extends ControllerBase<FacilityMaster> {

	@Autowired
	private FacilityEmailService facilityEmailService;
	
	@RequestMapping(value = {"/facility/reservation/receipt/send/{resvId}", "/member/facility/reservation/receipt/send/{resvId}"}, method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendRecipt(@PathVariable(value = "resvId") Integer resvId)
	{

		try {
			facilityEmailService.sendReceiptConfirmationEmail(resvId,getUser().getUserId());
			responseResult.setErrorMsg(initErrorMsg(GTAError.Success.SUCCESS));
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.TemplateError.MESSAGE_TEMPLATE_OTHER_ERROR));
		}
		
		return responseResult;
	}
	
}
