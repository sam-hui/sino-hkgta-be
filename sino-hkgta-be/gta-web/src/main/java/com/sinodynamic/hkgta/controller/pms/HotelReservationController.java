package com.sinodynamic.hkgta.controller.pms;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.MemberAccountInfoDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationChangeDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.PaymentResult;
import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.service.pms.HotelReservationServiceFacade;
import com.sinodynamic.hkgta.service.pms.RoomTypeService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.AppType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class HotelReservationController extends ControllerBase {

	@Autowired
	private PMSApiService pmsApiService;

	@Autowired
	private HotelReservationService hotelReservationService;

	@Autowired
	private RoomTypeService roomTypeService;

	@Autowired
	private PaymentGatewayService paymentGatewayService;

	@Autowired
	private HotelReservationServiceFacade hotelReservationServiceFacade;

	@Autowired
	private CustomerProfileService customerProfileService;

	@Autowired
	private MessageTemplateService messageTemplateService;

	@Autowired
	private CustomerEmailContentService cecService;

	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private UsageRightCheckService usageRightCheckService;
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private MemberService memberService;
	@RequestMapping(value = "/guestroom/reservation", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult guestroomAdvanceSearch(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="", required = false) String sortBy,
			@RequestParam(value = "arrivalDate", required = false) String arrivalDate,
			@RequestParam(value = "departureDate", required = false) String departureDate,
			@RequestParam(value="isAscending",defaultValue="true", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		String joinSQL = hotelReservationServiceFacade.getGuestroomAdvanceSearch(typeMap);
		joinSQL += "where 1=1 ";
		if(StringUtils.isEmpty(sortBy)){
			sortBy = "confirmId";
			isAscending = "false";
		}
		if(!StringUtils.isEmpty(arrivalDate)){
			joinSQL += "  and arrivalDate = '"+ arrivalDate +"'";
		}else {
			sortBy = "arrivalDate";
			isAscending = "false";
		}
		if(!StringUtils.isEmpty(departureDate)){
			joinSQL += "  and departureDate = '"+ departureDate +"'";
		}
		
		ResponseResult tmpResult = null;
		// advance search
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
		if(advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL = joinSQL + " AND ";
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
	
			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, RoomReservationDto.class,typeMap);
		}
		else
		{
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, RoomReservationDto.class,typeMap);
		}
		
		return tmpResult;
	}
	
	@RequestMapping(value = "/hotel/reservation/check", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getHotelAvailable(@RequestParam(value = "stayBeginDate", defaultValue = "", required = false) final String stayBeginDate,
			@RequestParam(value = "stayEndDate", defaultValue = "", required = false) final String stayEndDate,
			@RequestParam(value = "adultCount", defaultValue = "2", required = false) final Integer adultCount,
			@RequestParam(value = "childCount", defaultValue = "0", required = false) final Integer childCount,
			@RequestParam(value = "customerId", defaultValue = "0", required = false) final Long customerId,
			@RequestParam(value = "appType", defaultValue = "MEMBER", required = false) final AppType appType) throws Exception {
		
		CallBackExecutor executor = new CallBackExecutor(HotelReservationController.class);

		return (ResponseResult)executor.execute(new AbstractCallBack(){
				@Override
				public Object doTry() throws Exception {
					UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(customerId, UsageRightCheckService.FacilityCode.GUEST);
					if(!facilityRight.isCanBook() && AppType.MEMBER.equals(appType)){
						throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, "No permission to perform this operation");
					}
					
					CheckHotelAvailableDto dto = new CheckHotelAvailableDto(stayBeginDate, stayEndDate, adultCount, childCount);

					MessageResult msgResult = pmsApiService.getkHotelAvailable(dto);
					if (msgResult.isSuccess()) {
						RoomStayDto availableRooms = (RoomStayDto) msgResult.getObject();
						List<RoomTypeDto> rooms = roomTypeService.getRoomListByTypes(availableRooms, null ,appType);
						responseResult.initResult(GTAError.Success.SUCCESS, rooms);
					} else {
						responseResult.initResult(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[] { msgResult.getMessage() });
					}

					return responseResult;
				}
		});
	}

	@RequestMapping(value = "/hotel/reservation", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult bookHotel(@RequestBody List<RoomResDto> reservations) throws Exception {
		try{
			responseResult.initResult(GTAError.Success.SUCCESS, hotelReservationServiceFacade.bookHotel(reservations, getUser().getUserId()));
			return responseResult;
		}catch(GTACommonException e){
			if(GTAError.GuestRoomError.ROOM_NOT_ENOUGH == e.getError()){
				responseResult.initResult(GTAError.GuestRoomError.ROOM_NOT_ENOUGH, new Object[]{e.getArgs()[0]} , e.getArgs()[1]);
				return responseResult;
			}else{
				throw e;
			}
		}
	}
	
	@RequestMapping(value = "/hotel/reservation/{customerId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult bookHotel(@RequestBody List<RoomResDto> reservations, @PathVariable("customerId") Long customerId) throws Exception {
		try{
			UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(customerId, UsageRightCheckService.FacilityCode.GUEST);
			if(!facilityRight.isCanBook()){
				throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, "No permission to perform this operation");
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS, hotelReservationServiceFacade.bookHotel(reservations, getUser().getUserId()));
			return responseResult;
		}catch(GTACommonException e){
			if(GTAError.GuestRoomError.ROOM_NOT_ENOUGH == e.getError()){
				responseResult.initResult(GTAError.GuestRoomError.ROOM_NOT_ENOUGH, new Object[]{e.getArgs()[0]} , e.getArgs()[1]);
				return responseResult;
			}else{
				throw e;
			}
		}
	}
	
	@RequestMapping(value = "/hotel/payment/precancel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult precancel(@RequestBody HotelReservationPaymentDto paymentDto) throws Exception {
		hotelReservationServiceFacade.precancelReservation(paymentDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/hotel/payment/requestcancel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult requestCancel(@RequestBody HotelReservationCancelDto cancelDto) throws Exception {
		cancelDto.setCurrentDate(new Date());
		cancelDto.setUserId(getUser().getUserId());
		
		hotelReservationServiceFacade.cancelReservation(cancelDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/hotel/payment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult payment(@RequestBody HotelReservationPaymentDto paymentDto) throws Exception {
		String paymentMethod = paymentDto.getPaymentMethod();

		if (paymentDto.getCurrentDate() == null)
			paymentDto.setCurrentDate(new Date());

		paymentDto.setStaffUserId(getUser().getUserId());
		Long transactionNo = null;
		if (StringUtils.equalsIgnoreCase(paymentMethod, Constant.CASH_Value)||
				StringUtils.equalsIgnoreCase(paymentMethod, Constant.CASH)||
				StringUtils.equalsIgnoreCase(paymentMethod, Constant.PRE_AUTH)) {
			PaymentResult pr = hotelReservationServiceFacade.hotelReservationLocalPayment(paymentDto);
			
			responseResult.initResult(GTAError.Success.SUCCESS, pr);
			
			if(!StringUtils.equalsIgnoreCase(paymentMethod, Constant.PRE_AUTH)){
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("userId", getUser().getUserId());
				params.put("userName", getUser().getUserName());
				transactionNo = Long.parseLong(pr.getTransactionNo());
				hotelReservationServiceFacade.sendGuestroomReservationEmail(transactionNo, params);
			}
		}

		if (StringUtils.equalsIgnoreCase(paymentMethod, Constant.CREDIT_CARD)) {
			CustomerOrderTrans customerOrderTrans = hotelReservationServiceFacade.hotelReservationOnlinePayment(paymentDto);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("terminal", paymentDto.getTerminal());
			params.put("userId", getUser().getUserId());
			params.put("userName", getUser().getUserName());
			
			String redirectUrl = paymentGatewayService.payment(customerOrderTrans,params);
			Map<String, String> redirectUrlMap = new HashMap<String, String>();
			transactionNo = customerOrderTrans.getTransactionNo();
			redirectUrlMap.put("redirectUrl", redirectUrl);
			redirectUrlMap.put("transactionNo", String.valueOf(transactionNo));
			redirectUrlMap.put("orderNo", customerOrderTrans.getCustomerOrderHd().getOrderNo().toString());
			redirectUrlMap.put("amount", customerOrderTrans.getPaidAmount().toString());
			responseResult.initResult(GTAError.Success.SUCCESS, redirectUrlMap);
		}


		return responseResult;
	}
	
	@RequestMapping(value = "/hotel/change", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult change(@RequestBody HotelReservationChangeDto changeDto) throws Exception {
		changeDto.setUserId(getUser().getUserId());
		changeDto.setCurrentDate(new Date());
		hotelReservationServiceFacade.changeReservation(changeDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/guestroom/creditcardpayment/callback", method = RequestMethod.POST)
	@ResponseBody
	public String callbackAfterPayment(@RequestBody PosResponse posResponse) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", getUser().getUserId());
		params.put("userName", getUser().getUserName());
		hotelReservationServiceFacade.processCallbackAfterCreditCardLocalPayment(posResponse, params);
		return "OK";
	}
	
	@RequestMapping(value = "/guestroom/trivialInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult trivialInfo(@RequestParam("customerId") Long customerId) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		MemberAccountInfoDto currentAccount = (MemberAccountInfoDto)customerProfileService.getAccountInfo(customerId).getDto();
		
		String memberType = currentAccount.getMemberType();
		if(MemberType.IDM.name().equals(memberType) || MemberType.CDM.name().equals(memberType)){
			Member member = memberService.getMemberByAcademyNo(currentAccount.getAcademyNo());
			MemberAccountInfoDto superiorAccount = (MemberAccountInfoDto)customerProfileService.getAccountInfo(member.getSuperiorMemberId()).getDto();
			result.put("primaryCreditLimit", superiorAccount.getLimitValue());
			result.put("creditLimit", null);
			result.put("spendingLimit", currentAccount.getLimitValue());
		}else{
			result.put("primaryCreditLimit", null);
			result.put("creditLimit", currentAccount.getLimitValue());
			result.put("spendingLimit", null);
		}
		result.put("academyId", currentAccount.getAcademyNo());
		
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}
}
