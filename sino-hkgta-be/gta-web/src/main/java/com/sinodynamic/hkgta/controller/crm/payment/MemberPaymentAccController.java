package com.sinodynamic.hkgta.controller.crm.payment;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.service.crm.payment.MemberPaymentAccService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/paymentacc")
public class MemberPaymentAccController extends ControllerBase<MemberPaymentAcc> {
	
	@Autowired
	private MemberPaymentAccService memberPaymentAccService;
	
	
	@RequestMapping(value = "/{customerId}",  method=RequestMethod.GET)
	public @ResponseBody ResponseResult getMemberVirtualAcc(@PathVariable(value="customerId") Long customerId) {
		
		if (customerId == null) return null;
		String accNo = memberPaymentAccService.getVccNoByCustomerId(customerId);
		Map<String, String> map = new TreeMap<String, String>();
		map.put("accNo", CommUtil.formatVirtualAcc(accNo));
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}

}
