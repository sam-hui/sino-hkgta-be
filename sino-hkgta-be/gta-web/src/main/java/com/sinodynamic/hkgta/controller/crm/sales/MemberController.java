package com.sinodynamic.hkgta.controller.crm.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class MemberController extends ControllerBase<Member>{
	public Logger logger = Logger.getLogger(MemberController.class); 
	
	@Autowired
	private MemberService memberService;
	
	// register member
	@RequestMapping(value = "/member_reg", method = RequestMethod.POST)
	public void register(HttpServletRequest request,HttpServletResponse response) {
		try {
			 Member member = parseJson(request,Member.class);
			 memberService.addMember(member);;
			 page.getList().add(member);
			 writeJson(page, response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// query member
	@RequestMapping(value = "/memberGet/{id}", method = RequestMethod.GET)
	public @ResponseBody Object getMembers(@PathVariable(value = "id") String id) {
		try {
			if(id != null && !"ALL".equals(id)){
				Member member = memberService.getMemberById(new Long(id));
				MemberCashvalue mc = member.getMemberCashvalue();
				if (mc != null) {
					mc.setMember(null);
				}
				for (MemberPaymentAcc mpa: member.getMemberPaymentAccs()) {
					mpa.setMember(null);
				}
				for (MemberPlanFacilityRight mpfr : member.getMemberPlanFacilityRights()) {
					mpfr.setMember(null);
				}
				return member;
				//this.writeJson(member, response);
				//String testjw = this.getI18nMessge("NotBlank");
				//System.out.println(testjw);
			}else{
				memberService.getMemberList(page, null);
				return page;
				//this.writeJson(page, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// update member
	@RequestMapping(value = "/memberUpdate", method = RequestMethod.PUT)
	public void updateMember(HttpServletRequest request, HttpServletResponse response) {
		try {
			 String paraJson = fetchRequestJson(request);
			 Member member = parseJson(paraJson,Member.class);
			 memberService.updateMember(member);
			 page.getList().add(member);
			 writeJson(page, response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// delete member
	@RequestMapping(value = "/memberDelete/{id}", method = { RequestMethod.DELETE })
	public void deleteMember(@PathVariable(value = "id") Long id,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Member member = new Member();
			member.setCustomerId(id);
			memberService.deleteMember(member);
			memberService.getMemberList(page, member);
			writeJson(page, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/member/profile", method = { RequestMethod.GET })
	public ResponseResult getMemberInfo(@PathVariable(value = "qrCode") String qrCode) {
	      MemberInfoDto memberInfo= new  MemberInfoDto();
	      /*SELECT   m.*
	      FROM   permit_card_master pcm
	      LEFT JOIN  member m ON m.customer_id = pcm.mapping_customer_id
	      WHERE   card_no = SUBSTRING('100000003X', 1, 9)*/
		  return responseResult;
	}
	
	
	

}