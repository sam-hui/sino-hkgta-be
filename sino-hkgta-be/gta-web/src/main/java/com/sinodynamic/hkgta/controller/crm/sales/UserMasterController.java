package com.sinodynamic.hkgta.controller.crm.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;


@Controller
public class UserMasterController extends ControllerBase<UserMaster> {

	@Autowired
	private UserMasterService userService;

	// register UserMaster
	@RequestMapping(value = "/userReg", method = RequestMethod.PUT)
	public void register(HttpServletRequest request,HttpServletResponse response) {
		try {
			UserMaster user = parseJson(request, UserMaster.class);
			userService.addUserMaster(user);
			page.getList().add(user);
			writeJson(page, response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// query UserMaster
	@RequestMapping(value = "/userGet/{id}", method = RequestMethod.GET)
	public void getUserMaster(@PathVariable(value = "id") String id, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			if(id != null && !"ALL".equals(id)){
				UserMaster user = userService.getUserByLoginId(id);
				this.writeJson(user, response);
			}else{
				userService.getUserMasterList(page, null);;
				this.writeJson(page, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// update UserMaster
	@RequestMapping(value = "/userUpdate", method = RequestMethod.POST)
	public void updateUserMaster(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String paraJson = fetchRequestJson(request);
			UserMaster user = parseJson(paraJson, UserMaster.class);
			userService.updateUserMaster(user);;
			page.getList().add(user);
			this.writeJson(page, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// delete UserMaster
	@RequestMapping(value = "/userDelete/{id}", method = { RequestMethod.DELETE })
	public void deleteUserMaster(@PathVariable(value = "id") String id,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			UserMaster userMaster = new UserMaster();
			userMaster.setUserId(id);
			userService.deleteUserMaster(userMaster);
			userService.getUserMasterList(page, userMaster);
			this.writeJson(page, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
