package com.sinodynamic.hkgta.controller.pos;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantMenuItemDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.pos.RestaurantMenuItemService;
import com.sinodynamic.hkgta.util.JsonUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RestaurantMenuItemStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
public class MenuController extends ControllerBase<RestaurantMenuItem> {

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private RestaurantMenuItemService restaurantMenuItemService;

	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/restaurant/menu/list/{restaurantId}/{catId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult listMenu(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@PathVariable(value = "restaurantId") String restaurantId,
			@PathVariable(value = "catId") String catId,
			@RequestParam(value = "sortBy", defaultValue = "displayOrder") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters) {

		StringBuilder hql = new StringBuilder();
		hql.append(
				"select i.restaurantMenuCat.catId as catId, i.restaurantMenuCat.restaurantMaster.restaurantId as restaurantId, i.restaurantItemMaster.itemNo as itemNo, i.restaurantItemMaster.itemName as itemName, i.restaurantItemMaster.defaultPrice as defaultPrice, i.onlinePrice as onlinePrice, ");
		hql.append("i.imageFileName as imageFileName, i.displayOrder as displayOrder, i.status as status");
		hql.append(" from RestaurantMenuItem i ");

		if (StringUtils.isNotBlank(catId)) {
			hql.append(" where i.restaurantMenuCat.catId = '").append(catId).append("'");
		}

		if (StringUtils.isNotBlank(restaurantId)) {
			hql.append(" and i.restaurantMenuCat.restaurantMaster.restaurantId = '").append(restaurantId).append("'");
		}
		ListPage<RestaurantMenuItem> page = new ListPage<RestaurantMenuItem>();

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		AdvanceQueryDto advanceQueryDto = JsonUtil.fromJson(filters, AdvanceQueryDto.class);

		if (advanceQueryDto == null) {
			advanceQueryDto = new AdvanceQueryDto();
		}

		if (advanceQueryDto.getRules() != null && !advanceQueryDto.getRules().isEmpty()) {
			hql.append(" and ");
		}
		advanceQueryDto.setSortBy(sortBy);
		advanceQueryDto.setAscending(isAscending);
		ResponseResult responseResult = advanceQueryService.getAdvanceQueryResultByHQL(advanceQueryDto, hql.toString(),
				page, RestaurantMenuItemDto.class);

		Integer[] minAndMaxDisplayOrder = restaurantMenuItemService.getMinAndMaxDisplayOrder(restaurantId, catId);
		
		Data data = responseResult.getListData();
		
		List<RestaurantMenuItemDto> restaurantMenuItemDtos =  null;
		if (data != null) {
			restaurantMenuItemDtos = (List<RestaurantMenuItemDto>) data.getList();
		}
		
		if (restaurantMenuItemDtos != null) {
			for (RestaurantMenuItemDto restaurantMenuItemDto: restaurantMenuItemDtos) {
				
				if (restaurantMenuItemDto.getDisplayOrder().equals(minAndMaxDisplayOrder[0])) {
					restaurantMenuItemDto.setIsFirst(Boolean.TRUE);
				} 
				if (restaurantMenuItemDto.getDisplayOrder().equals(minAndMaxDisplayOrder[1])) {
					restaurantMenuItemDto.setIsLast(Boolean.TRUE);
				}
			}
		}

		return responseResult;
	}

	@RequestMapping(value = "/restaurant/menu/{restaurantId}/{catId}/{itemNo}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateMenuItem(
			@PathVariable(value = "restaurantId") String restaurantId,
			@PathVariable(value = "catId") String catId,
			@PathVariable(value = "itemNo") String itemNo,
			@RequestBody RestaurantMenuItemDto restaurantMenuItemDto) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			restaurantMenuItemDto.setRestaurantId(restaurantId);
			restaurantMenuItemDto.setCatId(catId);
			restaurantMenuItemDto.setItemNo(itemNo);
			restaurantMenuItemService.updateMenuItem(itemNo, restaurantMenuItemDto, userId);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurant/menu/status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult changeSatus(@RequestBody RestaurantMenuItemDto restaurantMenuItemDto) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			String status = restaurantMenuItemDto.getStatus();
			restaurantMenuItemService.changeStatus(restaurantMenuItemDto, status, userId);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurant/menu//{restaurantId}/{catId}/{itemNo}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteMenu(@PathVariable(value = "restaurantId") String restaurantId,
			@PathVariable(value = "catId") String catId, @PathVariable(value = "itemNo") String itemNo) {
		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			
			RestaurantMenuItemDto restaurantMenuItemDto = new RestaurantMenuItemDto();
			restaurantMenuItemDto.setRestaurantId(restaurantId);
			restaurantMenuItemDto.setCatId(catId);
			restaurantMenuItemDto.setItemNo(itemNo);
			
			restaurantMenuItemService.deleteMenuItem(restaurantMenuItemDto, userId);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurant/menu/order", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult displayOrder(@RequestBody RestaurantMenuItemDto restaurantMenuItemDto) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			restaurantMenuItemService.changeDisplayOrder(restaurantMenuItemDto, userId);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/restaurant/advanceSearchItem", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantMenuItemConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list = new ArrayList<>();
			
			list.add(new AdvanceQueryConditionDto("Item No", "i.restaurantItemMaster.itemNo", "java.lang.String", "", 1));
			list.add(new AdvanceQueryConditionDto("Item Name", "i.restaurantItemMaster.itemName", "java.lang.String", "", 2));
			list.add(new AdvanceQueryConditionDto("Default Price", "i.restaurantItemMaster.defaultPrice", "java.lang.String", "", 3));
			list.add(new AdvanceQueryConditionDto("Online Price", "i.onlinePrice", "java.math.BigDecimal", "", 4));
			list.add(new AdvanceQueryConditionDto("Display Order", "i.displayOrder", "java.lang.Integer", "", 5));
			
			AdvanceQueryConditionDto statusDto = new AdvanceQueryConditionDto("Status", "i.status", "java.lang.String", "", 6);
			list.add(statusDto);
			
			List<SysCode> options = new ArrayList<>();
            SysCode sysCodeShow = new SysCode();
            sysCodeShow.setCodeDisplay(RestaurantMenuItemStatus.S.getDesc());
            sysCodeShow.setCodeValue(RestaurantMenuItemStatus.S.toString());
            options.add(sysCodeShow);
            
            SysCode sysCodeHidden = new SysCode();
            sysCodeHidden.setCodeDisplay(RestaurantMenuItemStatus.H.getDesc());
            sysCodeHidden.setCodeValue(RestaurantMenuItemStatus.H.toString());
            options.add(sysCodeHidden);
            
            statusDto.setSelectName(statusDto.getDisplayName());
            statusDto.setOptions(options);
            
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
}
