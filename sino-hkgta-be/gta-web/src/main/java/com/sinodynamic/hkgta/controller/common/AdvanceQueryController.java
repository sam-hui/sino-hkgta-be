package com.sinodynamic.hkgta.controller.common;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberQuickSearchService;
import com.sinodynamic.hkgta.service.crm.cardmanage.DayPassPurchaseService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/common")
public class AdvanceQueryController extends ControllerBase {	

	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private DayPassPurchaseService dayPassPurchaseService;
	
	@Autowired
	private MemberQuickSearchService memberQuickSearchService;

	@RequestMapping(value = "/advance/query", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="") String sortBy,
			@RequestParam(value="isAscending",defaultValue="true") String isAscending,
			@RequestParam("joinHQL") String joinHQL,
			@RequestParam(value="queryType",defaultValue="HQL") String queryType,
			@RequestParam(value="filters",defaultValue="") String filters,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			// advance search
			if (StringUtils.isNotEmpty(filters)) {
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				if (StringUtils.equals("HQL", queryType)) {
					return advanceQueryService.getAdvanceQueryResultByHQL(queryDto, joinHQL, page);
				}
				return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinHQL, page,null);
			}
			// no advance condition search
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			return advanceQueryService.getInitialQueryResultByHQL(queryDto, joinHQL, page);
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}


	@RequestMapping(value = "/advanceConditions/{category}/{type}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getConditions(@PathVariable("category") String category,@PathVariable("type") String type, 
			HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list;
			if(StringUtils.equalsIgnoreCase("code", type)){
				 ServletContext context = request.getSession().getServletContext();    
				 WebApplicationContext ctx  = WebApplicationContextUtils.getWebApplicationContext(context);
				 list = advanceQueryService.assembleQueryConditions(category,(AdvanceQueryConditionDao)(ctx.getBean(category)));
			}else if (StringUtils.equalsIgnoreCase("GOLF", type) || StringUtils.equalsIgnoreCase("TENNIS", type)) {
				 list = advanceQueryService.assembleQueryConditions(category, type);
			}else{
				 list = advanceQueryService.assembleQueryConditions(category);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/card/{cardNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult mappingCustomerByCardNo(@PathVariable(value="cardNo") String cardNo,
			HttpServletRequest request, HttpServletResponse response) {
		try {
				return advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(cardNo)));	
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.AdvQueryError.CARD_MAPPING_FAILED);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/commonLookup/{number}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult lookupCustomer(@PathVariable(value="number") String number,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		if (number.toUpperCase().startsWith("DP"))
		{
			ResponseResult responseResult =  dayPassPurchaseService.searchDayPassById(number);
			
			return responseResult;
		}
		
		if (number.length()== 9 && number.matches("[\\d]{9}"))
		{
			return advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(number)));	
		}
		else
		{
			return memberQuickSearchService.quickSeachMember(number);
		}
		
	}
	

	@RequestMapping(value = "/advanceCourseSearch/{category}/{courseType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseConditions(@PathVariable("category") String category, @PathVariable("courseType") String courseType,
			HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list;
			if (!(StringUtils.equalsIgnoreCase("GSS", courseType) || StringUtils.equalsIgnoreCase("TSS", courseType))) {
				responseResult.initResult(GTAError.CourseError.INVALID_COURSE_TYPE);
				return responseResult;
			
			}
			list = advanceQueryService.assembleCourseQueryConditions(courseType);
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/advanceCourseSearch/temppass", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTemppassConditions(HttpServletRequest request, HttpServletResponse response,@RequestParam(value="history",defaultValue="N") String history) {	
		try {
			if("N".equals(history))
			{
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleTemppassQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
			}else
			{

				List<AdvanceQueryConditionDto> list = advanceQueryService.assembleTemppassHistoryQueryConditions();
				responseResult.initResult(GTAError.Success.SUCCESS, list);
				return responseResult;				
			}
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/advanceSearch/helptype", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getHelppassTypeConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleHelpPassTypeQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/advanceSearch/MmsOrder", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMmsOrderConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleMmsOrderQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/advanceSearch/academy", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAcademyCardConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assemblePermitCardQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/advanceCourseMembersSearch/{category}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseMembersConditions(@PathVariable("category") String category,
			HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list;
			list = advanceQueryService.assembleCourseMembersQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}


	@RequestMapping(value = "/advanceSearchRestaurantMenuCat", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantMenuCateConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list;
			list = advanceQueryService.assembleRestaurantMenuQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/SpaRetreatList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleSpaRetreatConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/advanceSearch/SpaRetreatItemList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatItemConditions(HttpServletRequest request, HttpServletResponse response) {	
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleSpaRetreatItemConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

}
