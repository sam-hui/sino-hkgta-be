package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.NotificationCenterService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GTAError.CommonError;
import com.sinodynamic.hkgta.util.exception.ErrorCodeException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/notification")
@Scope("prototype") 
public class NotificationCenterController extends ControllerBase{
	
	@Autowired
	private NotificationCenterService notificationService;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	static String[] EXTENSIONS = {".jpg",".png",".gif",".jpeg",".m4v",".mp4",".mov",".avi",".pdf",".html",".xls",".xlsx",".doc",".docx",".pptx",".ppt",".zip",".tar",".tgz",".rar"};
	
	public Logger logger = Logger.getLogger(NotificationCenterController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/email", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult sendNotificationByEmail(@RequestBody NotificationDto notificationDto){
		try {
			notificationDto.setSenderUserId(this.getUser().getUserId());
			responseResult = notificationService.getNotificationMemberList(notificationDto);
			List<NotificationMemberDto> memList = (List<NotificationMemberDto>)responseResult.getDto();
			Map<String,String> map  = notificationService.saveEmailRecord(notificationDto, memList);
			notificationService.sendNotificationByEmail(notificationDto,memList,map);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/sms", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult sendNotificationBySMS(@RequestBody NotificationDto notificationDto){
		try{
			return notificationService.sendNotificationBySMS(notificationDto);
		}catch(ErrorCodeException e){
			responseResult.initResult(e.getErrorCode(),e.getArgs());
			return responseResult;
		}
		catch(Exception e){
			logger.error(e);
			responseResult.initResult(CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/getRecipients", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult getRecipients(@RequestBody NotificationDto notificationDto){
		try{
			return notificationService.getNotificationMemberList(notificationDto);
		}catch(ErrorCodeException e){
			responseResult.initResult(e.getErrorCode(),e.getArgs());
			return responseResult;
		}
		catch(Exception e){
			logger.error(e);
			responseResult.initResult(CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/uploadEmailAttachment", method = RequestMethod.POST)
	public @ResponseBody ResponseResult uploadAttachment(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		try {
			if (file == null || file.isEmpty()) {
				logger.info("File is empty!");
				responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION, "File is empty!");
				return responseResult;
			}
			String fileName = file.getOriginalFilename();
			String fileSuffix = fileName.substring(fileName.lastIndexOf("."));
			if(!Arrays.asList(EXTENSIONS).contains(fileSuffix.toLowerCase())){
				throw new ErrorCodeException(GTAError.NotificationError.UNSUPPORTED_FILE, new String[]{fileSuffix});
			}
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			final String timeStr = sdf.format(new Date(System.currentTimeMillis()));
			String newFileName = timeStr +"@"+ fileName;
			String saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.NOTIFICATION, newFileName,false);
			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("imagePath", "/"+saveFilePath);
			responseResult.initResult(GTAError.Success.SUCCESS,imgPathMap);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION, "File upload is empty!");
			return responseResult;
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getSysEmail", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSysConfigEmail() {
		String sysEmail = this.appProps.getProperty("mail.from");
		Map<String, String> map = new HashMap<String, String>();
		map.put("sysEmail", sysEmail);
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}
	
}
