package com.sinodynamic.hkgta.controller.salesreport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.sys.ProgramMasterDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.security.service.RoleProgramService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
@RequestMapping("/salesreport")
public class SalesReportController extends ReportController {

	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private CustomerOrderTransService customerOrderTransService;

	@Autowired
	private UserMasterService userMasterService;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private ServicePlanService servicePlanService;

	@Autowired
	private RoleProgramService roleProgramService;

	@Autowired
	private UserRoleService userRoleService;

	@RequestMapping(value = "/getAllSalesmen", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllSalesmen() {
		ResponseResult responseResult = customerEnrollmentService.getAllSalesmen();
		return responseResult;
	}

	@RequestMapping(value = "/getAllCreatedStaff", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllCreatedStaff() {
		ResponseResult responseResult = customerOrderTransService.getAllCreatedStaff();
		return responseResult;
	}

	@RequestMapping(value = "/getAllAuditedStaff", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllAuditedStaff() {
		ResponseResult responseResult = customerOrderTransService.getAllAuditedStaff();
		return responseResult;
	}

	@RequestMapping(value = "/getSattlementReport", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSattlementReport(
			@RequestParam(value = "sortBy", defaultValue = "transactionId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "transactionDate", defaultValue = "0d", required = false) String transactionDate,
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "orderStatus", defaultValue = "", required = false) String orderStatus,
			@RequestParam(value = "transStatus", defaultValue = "", required = false) String transStatus,
			@RequestParam(value = "enrollType", defaultValue = "", required = false) String enrollType,
			@RequestParam(value = "location", defaultValue = "", required = false) String location,
			@RequestParam(value = "salesman", defaultValue = "", required = false) String salesman,
			@RequestParam(value = "createBy", defaultValue = "", required = false) String createBy,
			@RequestParam(value = "auditBy", defaultValue = "", required = false) String auditBy) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		SearchSettlementReportDto dto = new SearchSettlementReportDto(sortBy, pageNumber, pageSize, isAscending,
				transactionDate, startDate, endDate, orderStatus, transStatus, enrollType, location, salesman,
				createBy, auditBy);

		ResponseResult responseResult = customerOrderTransService.getSattlementReport(page, dto);
		return responseResult;
	}

	@RequestMapping(value = "/getSattlementReportExport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getSattlementReport(
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "transactionDate", defaultValue = "0d", required = false) String transactionDate,
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "orderStatus", defaultValue = "", required = false) String orderStatus,
			@RequestParam(value = "transStatus", defaultValue = "", required = false) String transStatus,
			@RequestParam(value = "enrollType", defaultValue = "", required = false) String enrollType,
			@RequestParam(value = "location", defaultValue = "", required = false) String location,
			@RequestParam(value = "salesman", defaultValue = "", required = false) String salesman,
			@RequestParam(value = "createBy", defaultValue = "", required = false) String createBy,
			@RequestParam(value = "auditBy", defaultValue = "", required = false) String auditBy,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		handleExpirySession(request, response);
		SearchSettlementReportDto dto = new SearchSettlementReportDto(fileType, sortBy, pageNumber, pageSize,
				isAscending, transactionDate, startDate, endDate, orderStatus, transStatus, enrollType, location,
				salesman, createBy, auditBy);

		ResponseResult responseResult = customerOrderTransService.getSattlementReportExport(page, dto, response);
		return (byte[]) responseResult.getData();
	}
	
	/**
	 * @deprecated Tempory not using. It's a refactored version of the settlement report.
	 * @author Liky_Pan
	 */
	@Deprecated
	@RequestMapping(value = "/getEnrollmentRenewalAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getEnrollmentRenewalAttach(
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Enrollment and Renewal", fileType);
			
			return customerOrderTransService.getEnrollmentRenewal(fileType, sortBy, isAscending);
		} catch (Exception e) {
			logger.debug("SalesReportController.getEnrollmentRenewalAttach Exception:", e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getMonthlyEnrollment", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMonthlyEnrollment(
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "year",  required = true) String year,
			@RequestParam(value = "month", required = true) String month) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = customerOrderTransService.getMonthlyEnrollment(page,year,month);
		return responseResult;
	}
	
	@RequestMapping(value = "/getMonthlyEnrollmentAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMonthlyEnrollmentAttach(
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Montly Statement("+year+"-"+String.format("%0" + 2 + "d", Integer.valueOf(month))+")", fileType);
			
			return customerOrderTransService.getMonthlyEnrollmentAttachment(year, month,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.debug("SalesReportController.getMonthlyEnrollmentAttach Exception:", e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyDayPassUsageAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyDayPassUsageAttach(
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "purchaseDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Daily Day Pass Usage Report - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date)), fileType);
			
			return servicePlanService.getDailyDayPassUsageAttachment(date,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.debug("SalesReportController.getDailyDayPassUsageAttach Exception:", e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyEnrollment", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDailyEnrollment(
			@RequestParam(value = "sortBy", defaultValue = "enrollDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "enrollDate", defaultValue = "", required = true) String enrollDate) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = customerEnrollmentService.getDailyEnrollment(page, enrollDate);
		return responseResult;
	}
	@RequestMapping(value = "/getDailyEnrollmentAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyEnrollmentAttach(
			@RequestParam(value = "enrollDate", required = true) String enrollDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "enrollDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Daily Report of Patron Acquisition - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(enrollDate)), fileType);
			return customerEnrollmentService.getDailyEnrollmentAttach(enrollDate,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.getDailyEnrollmentAttach Exception:", e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyLeads", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDailyLeads(
			@RequestParam(value = "sortBy", defaultValue = "createDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "selectedDate", required = true) String selectedDate) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = customerEnrollmentService.getDailyLeads(page, selectedDate);
		return responseResult;
	}
	
	@RequestMapping(value = "/getDailyLeadsAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyLeadsAttach(
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "createDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Daily Generated Prospect Reports - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)), fileType);
			return customerEnrollmentService.getDailyLeadsAttach(selectedDate,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.getDailyEnrollmentAttach Exception:", e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyFacilityUsage", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDailyFacilityUsage(
			@RequestParam(value = "sortBy", defaultValue = "resvIdString", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = memberFacilityTypeBookingService.getDailyMonthlyFacilityUsage("DAILY",page, selectedDate, facilityType);
		return responseResult;
	}
	@RequestMapping(value = "/getDailyFacilityUsageAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyFacilityUsageAttach(
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			String facilityTypeConvert = "GOLF".equals(facilityType)||"TENNIS".equals(facilityType)?("TENNIS".equals(facilityType)?"Tennis":"Golf"):"All";
			chooseFileType(request, response, "Daily Facility Usage Report ("+facilityTypeConvert+") - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)), fileType);
			return memberFacilityTypeBookingService.getDailyFacilityUsageAttach("DAILY",selectedDate,fileType,sortBy,isAscending,facilityType);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.getDailyEnrollmentAttach Exception:", e);
			return null;
		}
	}
	
	@RequestMapping(value = "/getMonthlyFacilityUsage", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getMonthlyFacilityUsage(
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
        String selectedDate = CommUtil.formatTheGivenYearAndMonthWithString(year, month);
		ResponseResult responseResult = memberFacilityTypeBookingService.getDailyMonthlyFacilityUsage("MONTHLY",page, selectedDate, facilityType);
		return responseResult;
	}
	@RequestMapping(value = "/getMonthlyFacilityUsageAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMonthlyFacilityUsageAttach(
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			String selectedDate = CommUtil.formatTheGivenYearAndMonthWithString(year, month);
			String facilityTypeConvert = "GOLF".equals(facilityType)||"TENNIS".equals(facilityType)?("TENNIS".equals(facilityType)?"Tennis":"Golf"):"All";
			chooseFileType(request, response, "Monthly Facility Usage Report ("+facilityTypeConvert+") - " + new SimpleDateFormat("yyyy-MMM").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)), fileType);
			return memberFacilityTypeBookingService.getDailyFacilityUsageAttach("MONTHLY",selectedDate,fileType,sortBy,isAscending,facilityType);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.getDailyEnrollmentAttach Exception:", e);
			return null;
		}
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSalesReportCategories() {
		if (getUser() != null && !StringUtils.isEmpty(getUser().getUserId())) {
			List<UserRoleDto> roles = userRoleService.getUserRoleListByUserId(getUser().getUserId());
			if (roles == null || roles.size() == 0) {
				responseResult.initResult(GTAError.Success.SUCCESS, new ArrayList<Object>(0));
				return responseResult;
			}
			List<Long> userRoleIds = new ArrayList<Long>(roles.size());
			for (UserRoleDto userRoleDto : roles) {
				userRoleIds.add(userRoleDto.getRoleId());
			}
			List<ProgramMasterDto> categories = roleProgramService.getSalesReportCategories(userRoleIds);
			responseResult.initResult(GTAError.Success.SUCCESS, categories);
		}
		return responseResult;
	}

}
