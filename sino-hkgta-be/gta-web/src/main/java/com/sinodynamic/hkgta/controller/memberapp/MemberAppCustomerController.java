package com.sinodynamic.hkgta.controller.memberapp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.memberapp.MemberAppCustomerAdditionInfoDto;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerAdditionInfoService;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
@RequestMapping("/memberApp")
public class MemberAppCustomerController extends ControllerBase{
	public Logger logger = Logger.getLogger(MemberAppCustomerController.class);
	@Autowired
	private CustomerAdditionInfoService customerAdditionInfoService;
	
	
	
	@RequestMapping(value = "/editCustomerAdditionInfo", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editAdditionInfo(@RequestBody MemberAppCustomerAdditionInfoDto additionInfoDto){
		ResponseResult responseResult = new ResponseResult();
		try {
			LoginUser currentUser = getUser();
			responseResult = customerAdditionInfoService.editCustomerAdditionInfo(additionInfoDto,currentUser.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerProfileController update CustomerAdditionInfo failed", e);
			responseResult = new ResponseResult("1","Update failed!");
		}
		return responseResult;
	}
}
