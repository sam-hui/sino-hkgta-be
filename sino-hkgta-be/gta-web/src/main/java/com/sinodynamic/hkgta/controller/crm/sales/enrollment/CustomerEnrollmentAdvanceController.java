package com.sinodynamic.hkgta.controller.crm.sales.enrollment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.CustomerCheckExsitDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerAdditionInfoService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Sam_Pang
 * @date Apr 29, 2015 4:06:27 PM 
 * @Description: check customer whether exist and query all exist information
 */
@Controller
@RequestMapping("/enrollment")
public class CustomerEnrollmentAdvanceController extends ControllerBase<ResponseResult>{

	public Logger logger = Logger.getLogger(CustomerEnrollmentAdvanceController.class);
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private CustomerAdditionInfoService customerAdditionInfoService;
	
	
	@RequestMapping(value="/checkPassportNO", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkWhetherExist(
			@RequestParam(value ="idType") String idType,
			@RequestParam(value = "idNO") String idNO,
			@RequestParam(value = "enrollType",defaultValue="IDM", required = false) String enrollType){		
		logger.info("CustomerEnrollmentAdvanceController.checkWhetherExist start ...");
		try {
			
			if(!PassportType.HKID.name().equals(idType) && !PassportType.VISA.name().equals(idType)){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.IDTYPE_INVALID);
				return responseResult;
			}
			if(PassportType.HKID.name().equals(idType)&& !CommUtil.validateHKID(idNO)){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.HKID_INVALID);
				return responseResult;
			}
			if(PassportType.VISA.name().equals(idType)&& !CommUtil.validateVISA(idNO)){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.PASSPORT_INVALID);
				return responseResult;
			}
			CustomerCheckExsitDto dto = customerProfileService.checkExistPassportNO(idType, idNO);
			if(dto!=null&&MemberType.IDM.name().equals(dto.getMemberType())&&"Y".equals(dto.getIsDeleted())){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.DELETE_DEP_CANNOT_IMPORT);
				return responseResult;
			}
			
			/*Pass the checking, Act as a normal registration*/
			if(null == dto){
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
			
			/*Enrolling for Primary Member (PM,DM)*/
			else if((EnrollStatus.NEW.name().equals(dto.getEnrollStatus())||EnrollStatus.APV.name().equals(dto.getEnrollStatus())
					||EnrollStatus.PYA.name().equals(dto.getEnrollStatus())||EnrollStatus.TOA.name().equals(dto.getEnrollStatus()))
					&&(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()))){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.ENROLLING,dto);
				return responseResult;
			}
			
			/*Upgrade dependent member (PM:Upgrade, DM:Display)*/
			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType())||MemberType.CDM.name().equalsIgnoreCase(dto.getMemberType())){//Used to import the dependent	
				responseResult.initResult(GTAError.EnrollmentAdvanceError.UPGRADE_FOR_DEPENDENT,dto);
				return responseResult;
				
			/*Re-enroll the primary member or import the PM and converted to DM when it was rejected or canceled (PM: Re-enroll, DM:Import)*/	
			}else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType())
					&&(EnrollStatus.REJ.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.CAN.name().equalsIgnoreCase(dto.getEnrollStatus()))){	
				responseResult.initResult(GTAError.EnrollmentAdvanceError.RE_ENROLL_FOR_REJ_CAN,dto);
				return responseResult;
				
			/*Display primary member with Active/Inactive status for ANC or CMP (PM,DM)*/
			}else if((MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType())||MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()))
					&&(EnrollStatus.ANC.name().equals(dto.getEnrollStatus())||EnrollStatus.CMP.name().equals(dto.getEnrollStatus()))){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.ID_REG_ALREADY,new Object[]{dto.getMemberType(),PassportType.VISA.name().equals(idType)?"Passport":idType,idNO},dto);
				return responseResult;
			}
			
			/*HG,MG detected(PM: Import, DM: display)*/
			else if(MemberType.MG.name().equalsIgnoreCase(dto.getMemberType())||MemberType.HG.name().equalsIgnoreCase(dto.getMemberType())){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.MG_HG_DETECTED,dto);
				return responseResult;
			}
			
			/*Lead detected(PM: Import, DM: display)*/
			else if(dto.getMemberType()==null&&EnrollStatus.OPN.name().equalsIgnoreCase(dto.getEnrollStatus())){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.ENROLL_LEAD,dto);
				return responseResult;
			}
			/* To be confirmed*/
			/*
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType())&&Constant.General_Status_NACT.equals(dto.getStatus())
					&&(EnrollStatus.ANC.name().equals(dto.getEnrollStatus())||EnrollStatus.CMP.name().equals(dto.getEnrollStatus()))){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.INACT_IPM_RENEW,dto);
				return responseResult;
			}
			*/
			
			
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentAdvanceController.checkWhetherExist",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("CustomerEnrollmentAdvanceController.checkWhetherExist end ...");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	
	@RequestMapping(value="/import/{customerID}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult importExistInfo(@PathVariable("customerID") Long customerID){
		logger.info("CustomerEnrollmentAdvanceController.importExistInfo start ...");
		
		try {	
			CustomerProfile cp = customerProfileService.getByCustomerID(customerID);
			responseResult.initResult(GTAError.Success.SUCCESS,cp);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerEnrollmentAdvanceController.importExistInfo",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	
	
}
