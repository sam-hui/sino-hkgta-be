package com.sinodynamic.hkgta.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.HelperPassTypeService;

@Component
public class CheckHelperPassTypeExpireTask {

    private Logger logger = Logger.getLogger(CheckHelperPassTypeExpireTask.class);

    @Autowired
    private HelperPassTypeService helperPassTypeService;

    public void autoDailyRun() {

	logger.info("Job CheckHelperPassTypeExpireTask.autoDailyRun start...");

	try {

	    helperPassTypeService.checkIfHelperPassTypeExpired();

	} catch (Exception e) {

	    e.printStackTrace();
	    logger.info(e.toString());
	}

	logger.info("Job CheckHelperPassTypeExpireTask.autoDailyRun finished...");
    }

}
