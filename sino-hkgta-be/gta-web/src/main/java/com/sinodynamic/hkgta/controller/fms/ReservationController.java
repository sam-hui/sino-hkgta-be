package com.sinodynamic.hkgta.controller.fms;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.fms.BookingRecordDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationInitDataDto;
import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.dto.membership.FacilityTransactionInfo;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.AgeRangeService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.fms.FacilityAdditionAttributeService;
import com.sinodynamic.hkgta.service.fms.FacilityAttributeCaptionService;
import com.sinodynamic.hkgta.service.fms.FacilityEmailService;
import com.sinodynamic.hkgta.service.fms.FacilityMasterService;
import com.sinodynamic.hkgta.service.fms.FacilitySubTypeService;
import com.sinodynamic.hkgta.service.fms.FacilityTimeslotService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.AgeRangeCode;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
public class ReservationController extends ControllerBase
{

	private Logger logger = Logger.getLogger(ReservationController.class);

	@Autowired
	private FacilityAttributeCaptionService facilityAttributeCaptionService;

	@Autowired
	private FacilityMasterService facilityMasterService;

	@Autowired
	private FacilityTimeslotService facilityTimeslotService;

	@Autowired
	private AgeRangeService ageRangeService;

	@Autowired
	private GlobalParameterService globalParameterService;

	@Autowired
	private MemberCashvalueService memberCashValueService;

	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;

	@Autowired
	private FacilityEmailService facilityEmailService;

	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;

    @Autowired
    private ShortMessageService  smsService;
    
    @Autowired
    private CustomerProfileService customerProfileService;

	@Autowired
	private MessageTemplateService messageTemplateService;
	
	@Autowired
	private FacilitySubTypeService facilitySubTypeService;
	
	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value = "/facility/reservation/initdata", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityReservationTypeData(@RequestParam(value = "facilityType", required = true, defaultValue = "GOLF") String facilityType)
	{
		logger.info("ReservationController.getFacilityReservationTypeData start ...");
		if (StringUtils.isEmpty(facilityType))
		{
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return responseResult;
		}
		try
		{
			List<FacilityReservationInitDataDto> facilityTypeDtos = new ArrayList<FacilityReservationInitDataDto>();
			List<FacilityReservationInitDataDto> ageDtos = new ArrayList<FacilityReservationInitDataDto>();
			setFacilityTypeDtos(facilityType, facilityTypeDtos);
			setAgeDtos(ageDtos);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("facilityAttribute", facilityTypeDtos);
			resultMap.put("ageList", ageDtos);
			String paramValue = "";
			String facTypeConst = Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityType) ? Constant.ADVANCEDRESPERIOD_GOLF : Constant.ADVANCEDRESPERIOD_TENNIS;
			ResponseResult response = globalParameterService.getGlobalParameterById(facTypeConst);
			if (null != response)
			{
				if (null != response.getData())
				{
					GlobalParameter globalParameter = (GlobalParameter) response.getData();
					if (null != globalParameter)
					{
						paramValue = globalParameter.getParamValue();
					}
				}
			}
			resultMap.put("advancedResPeriod", paramValue);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			logger.info("ReservationController.getFacilityReservationTypeData invocation end...");
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/facility/{facilityType}/{facilityAttribute}/count", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCountOfFacilitiesByTypeAndAttribute(@PathVariable(value = "facilityType") String facilityType, @PathVariable(value = "facilityAttribute") String facilityAttribute)
	{
		try
		{
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			int facilityCount = facilityMasterService.countFacilityMasterByTypeAndAttribute(facilityType, facilityAttribute,facilityNos);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("facilityCount", facilityCount);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
		}
		catch (Exception ex)
		{
			logger.error("Error counting facilityMaster by facilityType and facilityAttribute", ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/facility/reservation/price", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityReservationPrice(@RequestParam(value = "facilityType", required = true) String facilityType, @RequestParam(value = "customerId", required = false) Long customerId, @RequestParam(value = "bookingDate", required = true) String bookingDate, @RequestParam(value = "attributeType", required = true) String attributeType, @RequestParam(value = "ageRangeCode", required = true) String ageRangeCode, @RequestParam(value = "count", required = true) int count,
			@RequestParam(value = "beginTime", required = true) int beginTime, @RequestParam(value = "endTime", required = true) int endTime)
	{
		logger.info("ReservationController.getFacilityReservationPrice start ...");
		MemberFacilityBookingDto bookingDto = getMemberFacilityBookingDto(facilityType, customerId, bookingDate, attributeType, ageRangeCode, count, beginTime, endTime);
		boolean flag = checkFacilityReservationData(bookingDto);
		if (flag)
			return responseResult;
		try
		{
			bookingDto.setUpdateBy(this.getUser().getUserId());
			MemberFacilityBookingDto memberFacilityBookingDto = memberFacilityTypeBookingService.getMemberFacilityTypeBookingPrice(bookingDto,false);
			memberFacilityBookingDto.setUpdateBy(null);
			responseResult.initResult(GTAError.Success.SUCCESS, memberFacilityBookingDto);
			logger.info("ReservationController.getFacilityReservationPrice invocation end...");
			return responseResult;
		}
		catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@RequestMapping(value = "/member/facility/reservation/price", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberFacilityReservationPrice(@RequestParam(value = "facilityType", required = true) String facilityType, @RequestParam(value = "customerId", required = false) Long customerId, @RequestParam(value = "bookingDate", required = true) String bookingDate, @RequestParam(value = "attributeType", required = true) String attributeType, @RequestParam(value = "ageRangeCode", required = true) String ageRangeCode, @RequestParam(value = "count", required = true) int count,
			@RequestParam(value = "beginTime", required = true) int beginTime, @RequestParam(value = "endTime", required = true) int endTime)
	{
		logger.info("ReservationController.getFacilityReservationPrice start ...");
		MemberFacilityBookingDto bookingDto = getMemberFacilityBookingDto(facilityType, customerId, bookingDate, attributeType, ageRangeCode, count, beginTime, endTime);
		boolean flag = checkFacilityReservationData(bookingDto);
		if (flag)
			return responseResult;
		try
		{
			bookingDto.setUpdateBy(this.getUser().getUserId());
			MemberFacilityBookingDto memberFacilityBookingDto = memberFacilityTypeBookingService.getMemberFacilityTypeBookingPrice(bookingDto,true);
			memberFacilityBookingDto.setUpdateBy(null);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, memberFacilityBookingDto);
			logger.info("ReservationController.getFacilityReservationPrice invocation end...");
			return responseResult;
		}
		catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	private MemberFacilityBookingDto getMemberFacilityBookingDto(String facilityType, Long customerId, String bookingDate, String attributeType, String ageRangeCode, int count, int beginTime, int endTime)
	{
		MemberFacilityBookingDto bookingDto = new MemberFacilityBookingDto();
		bookingDto.setCustomerId(customerId);
		bookingDto.setFacilityType(facilityType);
		bookingDto.setBookingDate(bookingDate);
		bookingDto.setAttributeType(attributeType);
		bookingDto.setAgeRangeCode(ageRangeCode);
		bookingDto.setCount(count);
		bookingDto.setBeginTime(beginTime);
		bookingDto.setEndTime(endTime);
		return bookingDto;
	}

	@RequestMapping(value = "/facility/reservation", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult createFacilityReservation(@RequestBody MemberFacilityBookingDto bookingDto)
	{
		logger.info("ReservationController.createFacilityReservation start ...");
		boolean flag = checkFacilityReservationData(bookingDto);
		if (flag)
			return responseResult;
		if (null == bookingDto.getCustomerId() || bookingDto.getCustomerId() <= 0)
		{
			responseResult.initResult(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
			return responseResult;
		}
		else if (StringUtils.isEmpty(bookingDto.getPaymentMethod()))
		{
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return responseResult;
		}
		try
		{
			bookingDto.setUpdateBy(this.getUser().getUserId());
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			MemberFacilityBookingDto memberFacilityBookingDto = memberFacilityTypeBookingService.createMemberFacilityTypeBooking(bookingDto,false,facilityNos);
			if (bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CASH_Value) || bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CASH))
			{
				try
				{
					pushMessageNotification(memberFacilityBookingDto);
					sendFacilityBookingSMS(memberFacilityBookingDto.getOrderNo());
					facilityEmailService.sendReceiptConfirmationEmail(bookingDto.getResvId(),getUser().getUserId());
					
				}
				catch (Exception e)
				{
					logger.info(e.getMessage());
				}
			}
			
			responseResult.initResult(GTAError.FacilityError.SUCCESS, memberFacilityBookingDto);
			logger.info("ReservationController.createFacilityReservation invocation end...");
			return responseResult;
		}
		catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.FacilityError.TRANSACTION_FAIL, e.getMessage());
			return responseResult;
		}
		catch (Throwable e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.TRANSACTION_FAIL, e.getMessage());
			return responseResult;
		}
	}

	@RequestMapping(value = "/member/facility/reservation", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult createMemberFacilityReservation(@RequestBody MemberFacilityBookingDto bookingDto)
	{
		logger.info("ReservationController.createFacilityReservation start ...");
		boolean flag = checkFacilityReservationData(bookingDto);
		if (flag)
			return responseResult;
		if (null == bookingDto.getCustomerId() || bookingDto.getCustomerId() <= 0)
		{
			responseResult.initResult(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
			return responseResult;
		}
		else if (StringUtils.isEmpty(bookingDto.getPaymentMethod()))
		{
			responseResult.initResult(GTAError.FacilityError.PAYMENTMETHOD_ISNULL);
			return responseResult;
		}
		try
		{
			bookingDto.setUpdateBy(this.getUser().getUserId());
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			MemberFacilityBookingDto memberFacilityBookingDto = memberFacilityTypeBookingService.createMemberFacilityTypeBooking(bookingDto,true,facilityNos);
			if (bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CASH_Value) || bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CASH))
			{
				try
				{
					pushMessageNotification(memberFacilityBookingDto);
					sendFacilityBookingSMS(memberFacilityBookingDto.getOrderNo());
					facilityEmailService.sendReceiptConfirmationEmail(bookingDto.getResvId(),getUser().getUserId());
				}
				catch (Exception e)
				{
					logger.info(e.getMessage());
				}
			}
			
			responseResult.initResult(GTAError.FacilityError.SUCCESS, memberFacilityBookingDto);
			logger.info("ReservationController.createFacilityReservation invocation end...");
			return responseResult;
		}
		catch (GTACommonException e)
		{
			logger.debug("ReservationController.createMemberFacilityReservation",e);
			if (null != e.getError()){
				if(null!= e.getArgs() && e.getArgs().length > 0)responseResult.initResult((GTAError) e.getError(),e.getArgs());
				else responseResult.initResult((GTAError) e.getError());
			}
			else
				responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		catch (Exception e)
		{
			logger.debug("ReservationController.createMemberFacilityReservation",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private boolean checkFacilityReservationData(MemberFacilityBookingDto bookingDto)
	{
		if (null == bookingDto)
		{
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return true;
		}
		else
		{
			if (StringUtils.isEmpty(bookingDto.getFacilityType()))
			{
				responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
				return true;
			}
			else if (StringUtils.isEmpty(bookingDto.getAttributeType()))
			{
				responseResult.initResult(GTAError.FacilityError.ATTRIBUTETYPE_ISNULL);
				return true;
			}
			else if (StringUtils.isEmpty(bookingDto.getAgeRangeCode()))
			{
				responseResult.initResult(GTAError.FacilityError.AGERANGECODE_ISNULL);
				return true;
			}
			else if (StringUtils.isEmpty(bookingDto.getBookingDate()))
			{
				responseResult.initResult(GTAError.FacilityError.BOOKINGDATE_ISNULL);
				return true;
			}
			else if (null == bookingDto.getCount() || bookingDto.getCount() <= 0)
			{
				responseResult.initResult(GTAError.FacilityError.COUNT_ISNULL);
				return true;
			}
			else if (bookingDto.getBeginTime() == null || bookingDto.getBeginTime() == 0)
			{
				responseResult.initResult(GTAError.FacilityError.STARTTIME_ISNULL);
				return true;
			}
			else if (bookingDto.getEndTime() == null || bookingDto.getEndTime() == 0)
			{
				responseResult.initResult(GTAError.FacilityError.ENDTIME_ISNULL);
				return true;
			}
		}
		return false;
	}

	private void setAgeDtos(List<FacilityReservationInitDataDto> ageDtos)
	{
		List<AgeRange> ageRanges = ageRangeService.getAllAgeRange();
		if (null != ageRanges && ageRanges.size() > 0)
		{
			for (AgeRange ageRange : ageRanges)
			{
				if (ageRange.getAgeRangeCode().equalsIgnoreCase(AgeRangeCode.ADULT.toString()) || ageRange.getAgeRangeCode().equalsIgnoreCase(AgeRangeCode.CHILD.toString()))
				{
					FacilityReservationInitDataDto dto = new FacilityReservationInitDataDto();
					dto.setName(ageRange.getAgeRangeCode());
					dto.setValue(ageRange.getAgeRangeCode());
					ageDtos.add(dto);
				}
			}
		}
	}

	private void setFacilityTypeDtos(String facilityType, List<FacilityReservationInitDataDto> facilityTypeDtos)
	{
		List<FacilityAttributeCaption> facilityAttributeCaption= null;
		if (facilityType.equalsIgnoreCase(Constant.FACILITY_TYPE_GOLF))
		{
			facilityAttributeCaption = facilityAttributeCaptionService.getFacilityAttributeCaptionByFuzzy(Constant.GOLFBAYTYPE);
			if (null != facilityAttributeCaption && facilityAttributeCaption.size() > 0)
			{
				for (FacilityAttributeCaption caption : facilityAttributeCaption)
				{
					if(caption.getAttributeId().equalsIgnoreCase(Constant.GOLFBAYTYPE_CAR) == true) continue;
					FacilityReservationInitDataDto dto = new FacilityReservationInitDataDto();
					dto.setName(caption.getCaption());
					dto.setValue(caption.getAttributeId());
					facilityTypeDtos.add(dto);
				}
				FacilityReservationInitDataDto tempSave = facilityTypeDtos.get(0);
				facilityTypeDtos.set(0, facilityTypeDtos.get(1));
				facilityTypeDtos.set(1, tempSave);
			}
		}
		else if (facilityType.equalsIgnoreCase(Constant.FACILITY_TYPE_TENNIS))
		{
			List<FacilitySubType> subTypes = facilitySubTypeService.getFacilitySubTypeByFacilityType(facilityType);
			if(null != subTypes && subTypes.size() > 0){
				for(FacilitySubType subType : subTypes){
					FacilityReservationInitDataDto dto = new FacilityReservationInitDataDto();
					dto.setName(subType.getName());
					dto.setValue(subType.getSubTypeId());
					facilityTypeDtos.add(dto);
				}
			}
		}

	}

	@RequestMapping(value = "/facility/list/available/{facilityType}/{specifiedDate}/{ageRange}/{facilityAttribute}/{numberOfFacility}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAvailableList(@PathVariable(value = "facilityType") String facilityType, @PathVariable(value = "specifiedDate") Date specifiedDate, @PathVariable(value = "ageRange") String ageRange, @PathVariable(value = "facilityAttribute") String facilityAttribute, @PathVariable(value = "numberOfFacility") int numberOfFacility)
	{
		logger.info("ReservationController.getAvailableList start ...");
		if (StringUtils.isEmpty(facilityType))
		{
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return responseResult;
		}
		try
		{
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, facilityMasterService.getFacilityAvailabilityByFacilityNos(facilityType, specifiedDate, facilityAttribute, numberOfFacility,facilityNos,false));
			logger.info("ReservationController.getFacilityReservationTypeData invocation end...");
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/member/facility/list/available/{facilityType}/{specifiedDate}/{ageRange}/{facilityAttribute}/{numberOfFacility}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberAvailableList(@PathVariable(value = "facilityType") String facilityType, @PathVariable(value = "specifiedDate") Date specifiedDate, @PathVariable(value = "ageRange") String ageRange, @PathVariable(value = "facilityAttribute") String facilityAttribute, @PathVariable(value = "numberOfFacility") int numberOfFacility)
	{
		logger.info("ReservationController.getAvailableList start ...");
		if (StringUtils.isEmpty(facilityType))
		{
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return responseResult;
		}
		try
		{
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, facilityMasterService.getFacilityAvailabilityByFacilityNos(facilityType, specifiedDate, facilityAttribute, numberOfFacility,facilityNos,true));
			logger.info("ReservationController.getFacilityReservationTypeData invocation end...");
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/facility/reservation/list/{facilityType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getReservationList(@PathVariable(value = "facilityType") String facilityType, @RequestParam(value = "sortBy", defaultValue = "a.begin_datetime_book") String sortBy, @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "isAscending", defaultValue = "true") String isAscending, @RequestParam(value = "dateRange", defaultValue = "ALL") String dateRange,
			@RequestParam(value = "customerId", required = false) String customerId, @RequestParam(value = "filters", required = false, defaultValue = "") String filters, HttpServletRequest request, HttpServletResponse response)
	{
		logger.info("ReservationController.getReservationList start ...");
		if (StringUtils.isEmpty(facilityType))
		{
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return responseResult;
		}
		try
		{
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			String joinHQL = memberFacilityTypeBookingService.getAllMemberFacilityTypeBooking(facilityType, dateRange, customerId,false);
			if (sortBy != null && isAscending != null && sortBy.length() > 0 && isAscending.length() > 0) {
				sortBy += ", createDate";
				isAscending += ", false";
			}
			else
			{
				sortBy = "createDate";
				isAscending = "false";
			}
			List<Object> parameter = Arrays.asList(new Object[] {new Date(), new Date(), new Date(), new Date()});
			if (advanceQueryDto != null && advanceQueryDto.getRules() != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{
				joinHQL += " and ";
				AdvanceQueryDto queryDto = advanceQueryDto;
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinHQL, page, FacilityReservationDto.class, parameter);
			}
			else
			{
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryResultBySQL(queryDto, joinHQL, page, FacilityReservationDto.class, parameter);
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/member/reservation/list/{facilityType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getReservationListForMember(@PathVariable(value = "facilityType") String facilityType, @RequestParam(value = "sortBy", defaultValue = "resvId") String sortBy, @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "isAscending", defaultValue = "true") String isAscending, @RequestParam(value = "dateRange", defaultValue = "ALL") String dateRange,
			@RequestParam(value = "customerId", required = true) String customerId, @RequestParam(value = "filters", required = false, defaultValue = "") String filters, HttpServletRequest request, HttpServletResponse response)
	{
		return getReservationList(facilityType, sortBy, pageNumber, pageSize, isAscending, dateRange, customerId, filters, request, response);
	}

	@RequestMapping(value = "/facility/reservation/finalize", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult finalizeFacilityReservation(@RequestParam(value = "transactionNo", required = true) Long transactionNo, @RequestParam(value = "responseCode", required = false) String responseCode
			, @RequestParam(value = "traceNumber", required = false) String traceNumber, @RequestParam(value = "remark", required = false) String remark, @RequestParam(value = "cardType", required = false) String cardType)
	{
		logger.info("ReservationController.finalizeFacilityReservation start ...");
		if (StringUtils.isEmpty(transactionNo))
		{
			responseResult.initResult(GTAError.FacilityError.RESVID_ISNULL);
			return responseResult;
		}
		try
		{
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans!=null){
				Long transacitonNo = memberFacilityTypeBookingService.finalizeMemberFacilityTypeBooking(transactionNo, this.getUser().getUserId(), responseCode, traceNumber, remark, cardType);
				
				try
				{
					sendFacilityBookingSMS(customerOrderTrans.getCustomerOrderHd().getOrderNo());
					MemberFacilityTypeBooking booking = memberFacilityTypeBookingService.getMemberFacilityTypeBookingByOrderNo(customerOrderTrans.getCustomerOrderHd().getOrderNo());
					if (null != booking)
					{
						facilityEmailService.sendReceiptConfirmationEmail(booking.getResvId(),getUser().getUserId());
					}
				}catch(Exception e){
					logger.error(e.getMessage());
				}
				
				Map<String, Long> resultMap = new HashMap<String, Long>();
				resultMap.put("transactionId", transacitonNo);
				responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
				logger.info("ReservationController.finalizeFacilityReservation invocation end...");
				return responseResult;
			}
			else
			{
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Not find transactionNo.");
				return responseResult;
			}
		}
		catch (Throwable e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.TRANSACTION_FAIL);
			return responseResult;
		}
	}

	private void sendFacilityBookingSMS(Long orderNo) throws Exception
	{
		MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingService.getMemberFacilityTypeBookingByOrderNo(orderNo);
		if (memberFacilityTypeBooking != null && memberFacilityTypeBooking.getCustomerId() > 0) {
			CustomerProfile customerProfile = customerProfileService.getByCustomerID(memberFacilityTypeBooking.getCustomerId());
			if (customerProfile != null) {
				String mobilePhone = customerProfile.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<String>();
				phonenumbers.add(mobilePhone);
				Date begin = new Date(memberFacilityTypeBooking.getBeginDatetimeBook().getTime());
				Date end = new Date(memberFacilityTypeBooking.getEndDatetimeBook().getTime());
				MessageTemplate messageTemplate;
				if (memberFacilityTypeBooking.getResvFacilityType().equalsIgnoreCase("GOLF")){
					messageTemplate = messageTemplateService.getTemplateByFuncId( "golf_book_confirm");
				}else{
					messageTemplate = messageTemplateService.getTemplateByFuncId( "tennis_book_confirm");
				}
				if(messageTemplate!=null){
					
					Calendar endCalendar = Calendar.getInstance();
					endCalendar.setTime(end);
					endCalendar.set(Calendar.SECOND, endCalendar.get(Calendar.SECOND) + 1);
					
					String content = messageTemplate.getContent();
					String facilityName =Constant.FACILITY_TYPE_GOLF.equals(memberFacilityTypeBooking.getResvFacilityType())?"Golf Bay":"Tennis Court";
					String message = content.replace("{bookingDate}", DateCalcUtil.formatDate(begin)).replace("{startTime}", DateCalcUtil.getHourAndMinuteOfDay(begin)).replace("{endTime}", DateCalcUtil.getHourAndMinuteOfDay(endCalendar.getTime())).replace("{facilityType}", facilityName).replace("{facilityTypeQty}", Long.toString(memberFacilityTypeBooking.getFacilityTypeQty()));
					smsService.sendSMS(phonenumbers, message, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
				}
				
			}
			

		}
	}

	private void sendCancelSMS(MemberFacilityTypeBooking memberFacilityTypeBooking) throws Exception
	{
		if (memberFacilityTypeBooking != null && memberFacilityTypeBooking.getCustomerId() > 0) {
			CustomerProfile customerProfile = customerProfileService.getByCustomerID(memberFacilityTypeBooking.getCustomerId());
			if (customerProfile != null) {
				String mobilePhone = customerProfile.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<String>();
				phonenumbers.add(mobilePhone);
				Date begin = new Date(memberFacilityTypeBooking.getBeginDatetimeBook().getTime());
				Date end = new Date(memberFacilityTypeBooking.getEndDatetimeBook().getTime());
				MessageTemplate messageTemplate;
				if (memberFacilityTypeBooking.getResvFacilityType().equalsIgnoreCase("GOLF")){
					messageTemplate = messageTemplateService.getTemplateByFuncId( "golf_book_cancel");
				}else{
					messageTemplate = messageTemplateService.getTemplateByFuncId( "tennis_book_cancel");
				}
				String content = messageTemplate.getContent();
				String message = content.replace("{bookingDate}", DateCalcUtil.formatDate(begin)).replace("{startTime}", Integer.toString(DateCalcUtil.getHourOfDay(begin))).replace("{endTime}", Integer.toString(DateCalcUtil.getHourOfDay(end) + 1)).replace("{facilityType}", memberFacilityTypeBooking.getResvFacilityType()).replace("{facilityTypeQty}", Long.toString(memberFacilityTypeBooking.getFacilityTypeQty()));
				smsService.sendSMS(phonenumbers, message, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
			}
		}
	}

	@RequestMapping(value = "/facility/reservation/void", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult voidFacilityReservation(@RequestParam(value = "transactionNo", required = true) Long transactionNo)
	{
		logger.info("ReservationController.voidFacilityReservation start ...");
		if (StringUtils.isEmpty(transactionNo))
		{
			responseResult.initResult(GTAError.FacilityError.RESVID_ISNULL);
			return responseResult;
		}
		try
		{
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			Long transacitonNo = memberFacilityTypeBookingService.voidMemberFacilityTypeBooking(customerOrderTrans.getCustomerOrderHd().getOrderNo(),getUser().getUserId());
			Map<String, Long> resultMap = new HashMap<String, Long>();
			resultMap.put("transactionId", transacitonNo);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			logger.info("ReservationController.voidFacilityReservation invocation end...");
			return responseResult;
		}
		catch (Throwable e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.TRANSACTION_FAIL);
			return responseResult;
		}
	}

	/**
	 * update Facility Reservation
	 * 
	 * @return responseResult
	 */
	@RequestMapping(value = "/facility/reservation/update", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg updateFacilityReservation(@RequestBody MemberFacilityBookingDto facilityReservationDto)
	{
		responseResult.initResult(GTAError.FacilityError.SUCCESS);
		return responseResult;
	}

	/**
	 * cancel Facility Reservation
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/facility/reservation/cancel/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg cancelFacilityTypeBookingCheck(@PathVariable(value = "resvId") long resvId)
	{
		try
		{
			boolean canRefund = memberFacilityTypeBookingService.cancelMemberFacilityTypeBookingCheck(resvId);

			Map<String, Boolean> resultMap = new HashMap<String, Boolean>();

			if (canRefund)
			{
				resultMap.put("canRefund", true);
			}
			else
			{
				resultMap.put("canRefund", false);
			}

			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			logger.info("ReservationController.cancelFacilitySpecialRate invocation end...");
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}

	/**
	 * cancel Facility Reservation
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/facility/reservation/{resvId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseMsg cancelFacilityTypeBooking(@PathVariable(value = "resvId") long resvId, @RequestParam(value = "remark", required = false) String remark, 
			@RequestParam(value = "requesterType",required = false)String requesterType, 
			@RequestParam(value = "createRefundRequest",required = false, defaultValue= "true") String createRefundRequest)
	{
		try
		{
			LoginUser currentUser = getUser();
			memberFacilityTypeBookingService.cancelMemberFacilityTypeBooking(resvId, currentUser.getUserId(), remark, requesterType, createRefundRequest);
			MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingService.getMemberFacilityTypeBookingIncludeAllStatus(resvId);
			sendCancelSMS(memberFacilityTypeBooking);

			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			logger.info("ReservationController.cancelFacilitySpecialRate invocation end...");
		}
		catch (GTACommonException e)
		{
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}

	@RequestMapping(value = "/facility/memberCashvalue", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberCashvalue(@RequestParam(value = "customerId", required = true) long customerId)
	{
		logger.info("ReservationController.getMemberCashvalue start ...");
		if (customerId <= 0)
		{
			responseResult.initResult(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
			return responseResult;
		}
		try
		{
			MemberCashvalue memberCashvalue = memberCashValueService.getMemberCashvalueByCustomerId(customerId);
			if (null != memberCashvalue)
			{
				MemberCashvalueDto memberCashvalueDto = new MemberCashvalueDto();
				memberCashvalueDto.setCustomerId(customerId);
				memberCashvalueDto.setAvailableBalance(memberCashvalue.getAvailableBalance());
				responseResult.initResult(GTAError.FacilityError.SUCCESS, memberCashvalue);
			}
			else
			{
				responseResult.initResult(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
			}
			logger.info("ReservationController.getMemberCashvalue invocation end...");
			return responseResult;
		}
		catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError())
				responseResult.initResult((GTAError) e.getError());
			else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@RequestMapping(value = "/facility/checkin/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCheckInList(@RequestParam(value = "facilityType", required = true, defaultValue = "GOLF") String facilityType, @RequestParam(value = "date", required = true) String theDate, @RequestParam(value = "beginTime", required = true) int beginTime, @RequestParam(value = "endTime", required = true) int endTime, @RequestParam(value = "venueFloor", required = false) Integer venueFloor)
	{

		logger.info("FacilityController.getFacilityList start ...");

		try
		{
			Date specifiedDate = DateCalcUtil.parseDate(theDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(specifiedDate);
			calendar.set(Calendar.HOUR_OF_DAY, beginTime);
			Date beginDatetime = calendar.getTime();
			calendar.set(Calendar.HOUR_OF_DAY, endTime);
			Date endDatetime = calendar.getTime();
			List<FacilityMaster> facilityMasterList = facilityMasterService.getFacilityMasterList(venueFloor, facilityType);
			List<FacilityTimeslot> facilityTimeslot = facilityTimeslotService.getFacilityTimeslotListByRange(facilityType, beginDatetime, endDatetime, venueFloor);
			Map<String, String> map = new HashMap<String, String>();
			calendar = Calendar.getInstance();
			if (facilityTimeslot != null && facilityTimeslot.size() > 0)
			{
				for (FacilityTimeslot timeslot : facilityTimeslot)
				{
					calendar.setTime(timeslot.getBeginDatetime());
					map.put(timeslot.getFacilityMaster().getFacilityNo() + "-" + calendar.get(Calendar.HOUR_OF_DAY), timeslot.getStatus());
				}
			}

			if (facilityMasterList == null || facilityMasterList.size() == 0)
				return null;

			List<Map<Map<String, String>, List<FacilityMasterDto>>> retData = new ArrayList<Map<Map<String, String>, List<FacilityMasterDto>>>();
			Map<String, Object> retMap = null;

			List<FacilityMasterDto> listData = null;

			List<Object> masterList = new ArrayList<Object>();

			for (int i = beginTime; i <= endTime; i++)
			{
				retMap = new HashMap<String, Object>();
				listData = new ArrayList<FacilityMasterDto>();
				for (FacilityMaster facility : facilityMasterList)
				{
					FacilityMasterDto dto = new FacilityMasterDto();
					dto.setFacilityNo(facility.getFacilityNo());
					dto.setFacilityName(facility.getFacilityName());
					dto.setVenueCode(null == facility.getVenueMaster() ? null : facility.getVenueMaster().getVenueCode());
					if (map.get(facility.getFacilityNo() + "-" + i) != null)
					{
						dto.setStatus(map.get(facility.getFacilityNo() + "-" + i));
					}
					else
					{
						dto.setStatus(FacilityStatus.VC.toString());
					}
					listData.add(dto);
				}
				retMap.put("startTime", i + ":00");
				retMap.put("facilityList", listData);

				masterList.add(retMap);
			}

			Data result = new Data(masterList);
			result.setCurrentPage(1);
			result.setLastPage(true);
			result.setPageSize(retData.size());
			result.setRecordCount(retData.size());
			result.setTotalPage(1);

			logger.info("FacilityController.getFacilityList invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, result);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
		
	@RequestMapping(value = "/member/booking/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllBookingRecordList(@RequestParam(value = "userId", required = true) String userId)
	{
		List<BookingRecordDto> ls = new ArrayList<BookingRecordDto>() ;
		/*SELECT b.resv_id,b.customer_id,
		(case when b.exp_coach_user_id is null then (case when b.resv_facility_type='Golf' then '1' else '2' end) else (case when b.resv_facility_type='Golf' then '3' else '4' end) end ) as bookTpye,
		(case when b.exp_coach_user_id is null then (case when b.resv_facility_type='Golf' then 'Golfing bay' else 'Tennis court' end) else (case when b.resv_facility_type='Golf' then 'Golf private coaching' else 'Tennis private coaching' end) end ) as facility,
		concat(date_format(b.begin_datetime_book,'%Y/%m/%d'),' ',date_format(b.begin_datetime_book,'%H:%I'),'-',date_format(b.end_datetime_book,'%H:%I'))as DateTime,
		case when b.resv_facility_type='Golf' then concat('Bay #: ',(case when t.max is null or t.min is null then 'N/A' else t.max end)) else concat('Court #: ',(case when t.max is null or t.min is null then 'N/A' else t.max end)) end as bookingDetails,
		(case when t.max is not null then 'Checked-in' else (case when TIMESTAMPDIFF(MINUTE,now(),b.begin_datetime_book)>30 then 'Pending for check-in' when TIMESTAMPDIFF(MINUTE,now(),b.begin_datetime_book)<0 then 'Ovew Check-in' else 'Ready for check-in' end) end) as state,
		b.customer_remark
		from member_facility_type_booking b
		INNER JOIN
		(SELECT r.resv_id,max(t.facility_no) as max,min(t.facility_no)as min from member_reserved_facility r,facility_timeslot t where r.facility_timeslot_id=t.facility_timeslot_id GROUP BY r.resv_id)t on(t.resv_id=b.resv_id)
		where b.customer_id in(SELECT m.customer_id from member m WHERE m.user_id='Jeremy_luo') 
		union all 
		SELECT 
		 e.enroll_id as resv_id,
		e.customer_id,
		 (case when c.course_type='GSS' then '5' when c.course_type='TSS' then '6' else '' end) as bookType,
		 (case when c.course_type='GSS' then 'Golf Course' when c.course_type='TSS' then 'Tennis Course' else '' end) as facility,
		 concat(date_format(t.begin_datetime,'%Y/%m/%d'),'-',date_format(t.end_datetime,'%Y/%m/%d'))as DateTime,
		 '' as bookingDetails,
		 '' as state,
		 e.internal_remark as customer_remark
		 from course_enrollment e,course_master c 
		 INNER JOIN (SELECT s.course_id,min(s.begin_datetime) as begin_datetime,max(s.end_datetime)as end_datetime from course_master c ,course_session s where s.course_id=c.course_id GROUP BY s.course_id)t on(t.course_id=c.course_id)
		 where e.course_id=c.course_id and e.customer_id in(SELECT m.customer_id from member m WHERE m.user_id='Jeremy_luo');
*/
		return responseResult;
	}
	@RequestMapping(value = "/mebmer/booking/golf_tennis", method = RequestMethod.GET)
	public ResponseResult getGolfOrTennisBookingRecordList(
			@RequestParam(value = "customerId", required = true) String customerId,
			@RequestParam(value = "minutes", required = false,defaultValue="30")Integer minutes)
	{
		List<PrivateCoachBookListDto> ls = new ArrayList<PrivateCoachBookListDto>() ;
		/*
	SELECT b.resv_id,date_format(b.begin_datetime_book,'%Y/%m/%d') as Date,
concat(date_format(b.begin_datetime_book,'%H:%I'),'-',date_format(b.end_datetime_book,'%H:%I'))as Time, 
a.attribute_id ,a.attr_value as bayType,b.facility_type_qty as qty FROM member_facility_type_booking b LEFT JOIN member_facility_book_addition_attr a on(b.resv_id=a.resv_id)  WHERE b.exp_coach_user_id is null and b.status='RSV'
and (b.begin_datetime_book - INTERVAL 30 MINUTE)<now() and b.end_datetime_book>now() and   b.customer_id = 1 ;
*/
		return responseResult;
	}
	
	@RequestMapping(value = "/creditcardpayment/callback", method = RequestMethod.POST)
	@ResponseBody
    public String callback(@RequestBody PosResponse posResponse) {
		logger.info("Got callback !!");
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug(gson.toJson(posResponse));
		if (posResponse.getReferenceNo() != null) {
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans != null) {
				if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
					finalizeFacilityReservation(transactionNo, posResponse.getResponseCode(), posResponse.getTraceNumber(), posResponse.getResponseText(), posResponse.getCardType().trim());
				}
				else
				{
					if (customerOrderTrans.getStatus().equalsIgnoreCase("PND"))
					{
						customerOrderTrans.setStatus("FAIL");
						customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
						customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
						customerOrderTrans.setInternalRemark(posResponse.getResponseText());
						customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
					}
				}
			}
		}
        return "OK";
    }

	@RequestMapping(value = "/reservation/transactionstatus/{transactionNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionStatus(@PathVariable(value = "transactionNo") Long transactionNo)
	{

		logger.info("ReservationController.getTransactionStatus start ...");

		try
		{
			Map<String, Object> resultMap = new HashMap<String, Object>();
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans==null) {
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Not found transactionNo.");
				return responseResult;
			}
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			resultMap.put("orderNo", customerOrderHd.getOrderNo());
			resultMap.put("status", customerOrderHd.getOrderStatus());
			resultMap.put("transactionStatus", customerOrderTrans);
			logger.info("ReservationController.getTransactionStatus invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/reservation/retry", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult retryTransaction(@RequestBody MemberFacilityBookingDto bookingDto)
	{

		logger.info("ReservationController.retryTransaction start ...");

		try
		{
			if( bookingDto.getPaymentMethod() != null && (bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CASH) || bookingDto.getPaymentMethod().equalsIgnoreCase(Constant.CASH_Value))
					&& bookingDto.getTransactionId() != null && bookingDto.getTransactionId() > 0 ) {
				voidFacilityReservation(bookingDto.getTransactionId());
				return createFacilityReservation(bookingDto);
			}
			else
			{
				Map<String, Object> resultMap = new HashMap<String, Object>();
				CustomerOrderHd customerOrderHd = customerOrderTransService.getCustomerOrderHdByOrderNo(bookingDto.getOrderNo());
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				customerOrderTrans.setCustomerOrderHd(customerOrderHd);
				customerOrderTrans.setPaidAmount(bookingDto.getTotalPrice());
				customerOrderTrans.setPaymentMethodCode(bookingDto.getPaymentMethod());
				customerOrderTrans.setPaymentRecvBy(this.getUser().getUserName());
				customerOrderTrans.setStatus(Constant.Status.PND.name());
				customerOrderTrans.setTransactionTimestamp(new Timestamp(new Date().getTime()));
				customerOrderTransService.addNewTransactionRecord(customerOrderTrans);
				resultMap.put("transactionNo", customerOrderTrans.getTransactionNo());
				resultMap.put("orderNo", customerOrderTrans.getCustomerOrderHd().getOrderNo());
				logger.info("ReservationController.retryTransaction invocation end...");
				responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
				return responseResult;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/facility/transaction/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityTransactionInfo(@PathVariable(value = "resvId") long resvId) {
		try
		{
			FacilityTransactionInfo facilityTransactionInfo = memberFacilityTypeBookingService.getFacilityTransactionInfo(resvId);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, facilityTransactionInfo);
			logger.info("ReservationController.getFacilityTransactionInfo invocation end...");
		}
		catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}
	
	public  String getFacilityAttributeName(String facilityType,String attributeId) {
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			FacilityAttributeCaption caption = facilityAttributeCaptionService.getFacilityAttributeCaptionByAttributeId(attributeId);
			if (null != caption)return caption.getCaption();
		}else{
			FacilitySubType subType = facilitySubTypeService.getFacilitySubTypeBySubTypeId(attributeId);
			if (null != subType)return subType.getName();
		}
		return "";
	}
	
	public void pushMessageNotification(MemberFacilityBookingDto facilityReservationDto){
		try
		{
			Member member = memberService.getMemberById(facilityReservationDto.getCustomerId());
			if(null != member){
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				long diff = format.parse(facilityReservationDto.getBookingDate() + " " + facilityReservationDto.getBeginTime()+":00").getTime() - new Date().getTime();
				long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
				if (diffInMinutes < 60 && diffInMinutes > 0) {
					MessageTemplate messageTemplate;
					if (facilityReservationDto.getFacilityType().equalsIgnoreCase("GOLF")){
						messageTemplate = messageTemplateService.getTemplateByFuncId( "golf_book_remind");
					}else{
						messageTemplate = messageTemplateService.getTemplateByFuncId( "tennis_book_remind");
					}
					String content = messageTemplate.getContent();
					String subject = messageTemplate.getMessageSubject();
					String message = content.replace("{facilityType}", facilityReservationDto.getFacilityType().toLowerCase())
							.replace("{timeLeft}", Long.toString(diffInMinutes))
							.replace("{bookingDate}", facilityReservationDto.getBookingDate())
							.replace("{startTime}", facilityReservationDto.getBeginTime()+":00")
							.replace("{facilityType}", facilityReservationDto.getFacilityType())
							.replace("{facilityTypeQty}", Long.toString(facilityReservationDto.getCount()));
					devicePushService.pushMessage(new String[]{member.getUserId()}, subject, message, "member");
					logger.info(facilityReservationDto.getFacilityType() +" reservation remind success");
				}
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
}
