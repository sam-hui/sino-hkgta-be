package com.sinodynamic.hkgta.controller.common;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/common")
public class ServerTimeController extends ControllerBase<Date>
{
	@RequestMapping(value = "/ServerTime", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getServerTime()
	{
		responseResult.initResult(GTAError.Success.SUCCESS, new Date());
		return responseResult;
	}
}
