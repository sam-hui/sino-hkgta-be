package com.sinodynamic.hkgta.controller.crm.sales.presentation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;
import com.sinodynamic.hkgta.service.crm.TestService;

@Controller
@RequestMapping("/test")
public class TestController extends
		ControllerBase<PresentationBatch> {

	@Autowired
	private TestService testService;

	@RequestMapping(value = "/caseone", method = RequestMethod.POST)
	public void test(HttpServletRequest request,
			HttpServletResponse response){
		try {
			testService.updatePres();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	

}
