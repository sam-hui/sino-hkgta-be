package com.sinodynamic.hkgta.controller.salesreport;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.GTAError;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReportController extends ControllerBase {

    @Autowired
    private UserMasterService userMasterService;

    protected void handleExpirySession(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getHeader("Referer");
        String base64Token = request.getHeader("token");
        GTAError errorCode = null;
        boolean valid = false;
        if (null==base64Token || StringUtils.isEmpty(base64Token))
        {
            base64Token = request.getParameter("token");
        }
        //If session is expire or token is invalid, redirect to login page.
        if (StringUtils.isEmpty(base64Token))
        {
            try {
                response.sendRedirect(url);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }else {

            byte[] t = Base64.decodeBase64(base64Token);
            String authToken = new String(t);
            valid = TokenUtils.validateUUIDToken(authToken);
            errorCode = userMasterService.updateSessionToken(authToken);
            if (!valid || !GTAError.Success.SUCCESS.equals(errorCode))
            {
                try {
                    response.sendRedirect(url);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    protected void chooseFileType(HttpServletRequest request,	HttpServletResponse response,String fileName,String fileType) {
        if("pdf".equalsIgnoreCase(fileType)){
            response.setHeader("Content-Disposition", "attachment; filename=" +fileName+".pdf");
            response.setContentType("application/pdf");
        }else{
            response.setHeader("Content-Disposition", "attachment; filename=" +fileName+".csv");
            response.setContentType("text/csv");
        }
    }
}
