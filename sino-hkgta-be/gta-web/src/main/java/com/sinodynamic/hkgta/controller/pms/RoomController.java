package com.sinodynamic.hkgta.controller.pms;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pms.AssignAdHocTaskDto;
import com.sinodynamic.hkgta.dto.pms.AssignMaintenanceTaskDto;
import com.sinodynamic.hkgta.dto.pms.PushMessageDto;
import com.sinodynamic.hkgta.dto.pms.RoomDetailDto;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomTaskDto;
import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.service.adm.StaffMasterService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.pms.HousekeepPresetParameterService;
import com.sinodynamic.hkgta.service.pms.HousekeepTaskFileService;
import com.sinodynamic.hkgta.service.pms.RoomHousekeepTaskService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RoomCrewRoleType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
public class RoomController extends ControllerBase
{

	private static final List<String> IMAGE_FORMAT = Arrays.asList("png", "jpeg", "bmp", "jpg", "gif", "BMP", "JPG", "JPEG", "PNG", "GIF");
	private Logger logger = Logger.getLogger(RoomController.class);

	@Autowired
	private RoomService roomService;
	
	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;
	
	@Autowired
	private HousekeepPresetParameterService housekeepPresetParameterService;
	
	@Autowired
	private HousekeepTaskFileService housekeepTaskFileService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Autowired
	private StaffMasterService staffMasterService;
	
	String JPEG_EXT = "jpeg";
	String JPG_EXT = "jpg";
	String PNG_EXT = "png";
	String GIF_EXT = "gif";
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/room/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoomList()
	{
		logger.info("RoomController.getRoomList start ...");
		try{
			List<RoomDto> rooms = roomService.getRoomListWithTaskCounts(getUser().getUserId());
			
			List<HousekeepPresetParameter> paramCatList = housekeepPresetParameterService.getHousekeepPresetParameterList();
			Map<String,List<HousekeepPresetParameter>> presetParameterMap = new HashMap<String,List<HousekeepPresetParameter>>();
			for(HousekeepPresetParameter presetParameter : paramCatList){
				List<HousekeepPresetParameter> parameters = presetParameterMap.get(presetParameter.getParamCat());
				if(null == parameters){
					parameters = new ArrayList<HousekeepPresetParameter>();
					parameters.add(presetParameter);
					presetParameterMap.put(presetParameter.getParamCat(), parameters);
				}else parameters.add(presetParameter);
			}
			List<Map<String,Object>> presetParameters = new ArrayList<Map<String,Object>>();
			Map<String,Object> presetMap = null;
			Set<Entry<String, List<HousekeepPresetParameter>>> parameterEntrySet = presetParameterMap.entrySet();
			Iterator it = parameterEntrySet.iterator();
			while(it.hasNext()){
				presetMap = new HashMap<String,Object>();
				Entry<String, List<HousekeepPresetParameter>> entry = (Entry<String, List<HousekeepPresetParameter>>) it.next();
				presetMap.put("category", entry.getKey());
				presetMap.put("detailList", entry.getValue());
				presetParameters.add(presetMap);
			}
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("roomList", rooms);
			result.put("presetParameters", presetParameters);
			logger.info("RoomController.getRoomList invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, result);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/room/{roomId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoomDetail(@PathVariable(value = "roomId") Long roomId)
	{
		logger.info("RoomController.getRoomDetail start ...");
		if(null == roomId){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try{
			RoomDetailDto roomDetail = roomService.getRoomDetail(roomId);
			logger.info("RoomController.getRoomDetail invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, roomDetail);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/member/room/{roomId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberRoomDetail(@PathVariable(value = "roomId") Long roomId)
	{
		logger.info("RoomController.getRoomDetail start ...");
		if(null == roomId){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try{
			RoomDetailDto roomDetail = roomService.getRoomDetail(roomId);
			logger.info("RoomController.getRoomDetail invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, roomDetail);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/room/task", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult assignRoomHousekeepTask(@RequestBody RoomHousekeepTaskDto taskDto)
	{
		logger.info("RoomController.saveRoomHousekeepTask start ...");
		boolean flag = checkRoomHousekeepTaskData(taskDto);
		if(!flag)return responseResult;
		if(null == taskDto.getBeginDate())taskDto.setBeginDate(new Date());
		taskDto.setUpdateBy(this.getUser().getUserId());
		try{
			roomHousekeepTaskService.assignRoomHousekeepTask(taskDto);
			logger.info("RoomController.saveRoomHousekeepTask invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS);
		}catch (GTACommonException e)
		{
			logger.error(e.getMessage());
			if (null != e.getError())
				responseResult.initResult((GTAError) e.getError());
			else
				responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}


	private boolean checkRoomHousekeepTaskData(RoomHousekeepTaskDto taskDto) {
		if(null == taskDto || null == taskDto.getRoomIds() || taskDto.getRoomIds().length ==0
					|| StringUtils.isEmpty(taskDto.getTaskDescription())
					|| StringUtils.isEmpty(taskDto.getJobType())){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return false;
		}
		return true;
	}
	@RequestMapping(value = "/room/task/{taskId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editRoomHousekeepTask(@PathVariable(value = "taskId") Long taskId,@RequestBody RoomHousekeepTaskDto taskDto)
	{
		if(taskId == null || null == taskDto || null == taskDto.getTaskDescription()){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		if(null == taskDto.getBeginDate())taskDto.setBeginDate(new Date());
		taskDto.setUpdateBy(this.getUser().getUserId());
		taskDto.setTaskId(taskId);
		try{
			roomHousekeepTaskService.updateRoomHousekeepTask(taskDto);
			RoomHousekeepTaskDto dto =  roomHousekeepTaskService.getRoomHousekeepTask(taskId);
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return responseResult;
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError())responseResult.initResult((GTAError) e.getError());
			return responseResult;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/room/{roomId}/{status}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult changeRoomStatus(@PathVariable(value = "roomId") Long roomId,@PathVariable(value = "status") String status)
	{
		logger.info("RoomController.changeRoomStatus start ...");
		if(null == roomId || StringUtils.isEmpty(status)){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try{
			return roomService.changeRoomStatus(roomId, status, this.getUser().getUserId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.CHANGE_STATUS_FAIL);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/room/task/{taskId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult cancelRoomHousekeepTask(@PathVariable(value = "taskId") Long taskId)
	{
		logger.info("RoomController.cancelRoomHousekeepTask start ...");
		if(null == taskId){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try{
			roomHousekeepTaskService.cancelRoomHousekeepTask(taskId, this.getUser().getUserId());
			RoomHousekeepTaskDto dto =  roomHousekeepTaskService.getRoomHousekeepTask(taskId);
			 responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return responseResult;
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError())responseResult.initResult((GTAError) e.getError());
			return responseResult;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/room/reassignTask/{taskId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult reassignRoomHousekeepTask(@PathVariable(value = "taskId") Long taskId,@RequestBody RoomHousekeepTaskDto taskDto)
	{
		logger.info("RoomController.reassignRoomHousekeepTask start ...");
		if(null == taskId || null == taskDto || null == taskDto.getTaskDescription()){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try{
			taskDto.setTaskId(taskId);
			taskDto.setUpdateBy(getUser().getUserId());;
			Long newTaskId = roomHousekeepTaskService.reassignRoomHousekeepTask(taskDto);
			RoomHousekeepTaskDto dto =  roomHousekeepTaskService.getRoomHousekeepTask(newTaskId);
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return responseResult;
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError())responseResult.initResult((GTAError) e.getError());
			return responseResult;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/room/task/get_thumbnail", method = RequestMethod.GET)
	@ResponseBody
	public void getThumbnailAsByteStream(@RequestParam(value = "fileId") Long fileId,@RequestParam(value = "isSmall",required = false) Boolean isSmall, HttpServletResponse resp) {
		try{
			HousekeepTaskFile housekeepTaskFile = housekeepTaskFileService.getHousekeepTaskFile(fileId);
			if(null != housekeepTaskFile){
				String fileSuffix = housekeepTaskFile.getServerFile().substring(housekeepTaskFile.getServerFile().lastIndexOf(".")+1,housekeepTaskFile.getServerFile().length()).toUpperCase();
				resp.setHeader("Cache-Control", "max-age=21600");
				if(JPEG_EXT.equalsIgnoreCase(fileSuffix) || JPG_EXT.equalsIgnoreCase(fileSuffix)){
					resp.setContentType("image/jpeg");
				}else if(PNG_EXT.equalsIgnoreCase(fileSuffix)){
					resp.setContentType("image/png");
				}else if(GIF_EXT.equalsIgnoreCase(fileSuffix)){
					resp.setContentType("image/gif");
				}
				resp.getOutputStream().write(housekeepTaskFileService.getMediaAsByteStream(fileId,isSmall));
			}
		}catch (Exception e){
		}
	}
	
	private byte[] getVoicefileAsByteStream(Long fileId) {
		try {
			return housekeepTaskFileService.getMediaAsByteStream(fileId, false);
		}
		catch (Exception e) {
			return new byte[0];
		}
	}

	@RequestMapping(value = "/room/task/get_aacfile", method = RequestMethod.GET, produces = "audio/aac")
	@ResponseBody
	public byte[] getAACfileAsByteStream(@RequestParam(value = "fileId") Long fileId) {
		return getVoicefileAsByteStream(fileId);
	}

	@RequestMapping(value = "/room/task/get_amrfile", method = RequestMethod.GET, produces = "audio/amr")
	@ResponseBody
	public byte[] getAMRfileAsByteStream(@RequestParam(value = "fileId") Long fileId) {
		return getVoicefileAsByteStream(fileId);
	}
	
	@RequestMapping(value = "/member/room/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberRoomList(@RequestParam(value = "isMyRoom",required=false) Boolean isMyRoom,@RequestParam(value = "roomNo",required=false) String roomNo)
	{
		logger.info("RoomController.getMemberRoomList start ...");
		if(null == isMyRoom)isMyRoom=false;
		return roomService.getRoomTaskList(getUser().getUserId(),isMyRoom,roomNo);
	}
	
	@RequestMapping(value = "/member/room/task/{taskId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editRoomHousekeepTaskByApp(@PathVariable(value = "taskId") Long taskId,@RequestBody UpdateRoomTaskDto taskDto)
	{
		logger.info("RoomController.editRoomHousekeepTaskByApp start ...");
		if(taskId == null || null == taskDto || null == taskDto.getAction()){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		taskDto.setTaskId(taskId);
		try{
			 roomHousekeepTaskService.updateRoomHousekeepTask(taskDto,getUser().getUserId());
			 RoomHousekeepTaskDto dto =  roomHousekeepTaskService.getRoomHousekeepTask(taskId);
			 responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return responseResult;
		}catch (GTACommonException e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			if (null != e.getError())responseResult.initResult((GTAError) e.getError());
			return responseResult;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/room/task/{taskId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoomHousekeepTask(@PathVariable(value = "taskId") Long taskId)
	{
		logger.info("RoomController.getRoomHousekeepTask start ...");
		if(null == taskId){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try{
			RoomHousekeepTaskDto roomHousekeepTaskDto = roomHousekeepTaskService.getRoomHousekeepTask(taskId);
			responseResult.initResult(GTAError.Success.SUCCESS,roomHousekeepTaskDto);
			logger.info("RoomController.getRoomHousekeepTask invocation end...");
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/member/assign/ad_hoc_task", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult assignAdHocTask(@RequestBody AssignAdHocTaskDto dto)
	{
		logger.info("RoomController.assignAdHocTask start ...");
		if(null == dto){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		return roomHousekeepTaskService.assignAdHocTask(dto,getUser().getUserId());
			
	}
	
	@RequestMapping(value = "/member/assign/maintenance_task", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult assignMaintenanceTask(@RequestBody AssignMaintenanceTaskDto dto)
	{
		logger.info("RoomController.assignMaintenanceTask start ...");
		if(null == dto ){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
	    return roomHousekeepTaskService.assignMaintenanceTask(dto,getUser().getUserId());
	}
	
	/**
	 *This function is used to save the profile photo for each enrollment record.
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/member/upload/file", method = RequestMethod.POST)
	public @ResponseBody ResponseResult uploadFile(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		ResponseResult responseResult = new ResponseResult();
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("{\"warning\":\"File is empty!\"}");
			return responseResult;
		}
		String saveFilePath = "";
		try {
			String ext = FilenameUtils.getExtension(file.getOriginalFilename()); 
			String newName =DateConvertUtil.parseDate2String(new Date() , "yyyyMMddHHmmss")+getUser().getUserId()+"."+ext;
			boolean isThumbnail=false;
			if(IMAGE_FORMAT.contains(ext))
				isThumbnail=true;
			saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.MATERIAL,newName,isThumbnail);
		} catch (Exception e) {
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("fail");
			return responseResult;
		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		responseResult.setReturnCode("0");
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("filePath", "/"+saveFilePath);
		responseResult.setData(imgPathMap);
		return responseResult;
	}
	
	@RequestMapping(value = "/member/role", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffType()
	{
		logger.info("RoomController.getMemberRole start ...");
	    return roomHousekeepTaskService.getStaffType(getUser().getUserId());
	}

/*   According to SINO, this register api should be called by hoursekeep app. The backend does not need to call register api.
 *  We need to create another backend api for staff save their deviceArn. 
 *  
 *  	
	@RequestMapping(value = "/member/push/register", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult pushRegister(@RequestBody RegisterDto dto)
	{
		logger.info("RoomController.pushRegister start ...");
		if(dto==null){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		if(org.apache.commons.lang.StringUtils.isBlank(dto.getToken())){
			responseResult.initResult(GTAError.HouseKeepError.PUSH_TOKEN_ISNULL);
			return responseResult;
		}
		if(org.apache.commons.lang.StringUtils.isBlank(dto.getLang())){
			responseResult.initResult(GTAError.HouseKeepError.PUSH_LANG_ISNULL);
			return responseResult;
		}
		if(org.apache.commons.lang.StringUtils.isBlank(dto.getPlatform())){
			responseResult.initResult(GTAError.HouseKeepError.PUSH_PLATFORM_ISNULL);
			return responseResult;
		}
		if(org.apache.commons.lang.StringUtils.isBlank(dto.getSystemVersion())){
			responseResult.initResult(GTAError.HouseKeepError.PUSH_SYSVERSION_ISNULL);
			return responseResult;
		}
	    return roomService.pushRegister(dto,getUser().getUserId());
	}
*/	
	
	@RequestMapping(value = "/member/task/message", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult pushMessageForTask(@RequestBody PushMessageDto dto)
	{
		if(null == dto || null == dto.getTaskId()){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		RoomHousekeepTask task = roomHousekeepTaskService.getRoomHousekeepTaskById(dto.getTaskId());
		if (null != task && (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.name()) || task.getJobType().equals(RoomHousekeepTaskJobType.SCH.name())))
		{
			roomHousekeepTaskService.autoRemindRoomAssistant(Constant.HOUSEKEEP_PUSHMESSAGE_EXPIRED,RoomCrewRoleType.RA.name(),task.getRoom(),task);
			
		}
	    responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
}
