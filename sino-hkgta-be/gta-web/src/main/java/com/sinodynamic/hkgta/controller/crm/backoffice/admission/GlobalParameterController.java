package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Controller
@RequestMapping("/global_parameter")
@Scope("prototype") 
public class GlobalParameterController extends ControllerBase<GlobalParameter>{

	@Autowired
	private GlobalParameterService globalParameterService ;
	
	@RequestMapping(value = "/get_globalparameter", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getGlobalParameter() 
	{
		logger.info("GlobalParameterController.getDayPassPageList invocation start...");
		String paramId ="DPDAYQUOTA";
		return globalParameterService.getGlobalParameterById(paramId);
	}
	
	@RequestMapping(value="/set_global_quota/{paramValue}", method = RequestMethod.PUT , produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseResult setDayPassGlobalQuota(@PathVariable(value="paramValue") String paramValue){
		
		logger.info("GlobalParameterController.setDayPassGlobalQuota invocation start...");
		ResponseResult result = globalParameterService.setGlobalQuota(paramValue,this.getUser().getUserId());
		return result;		
	}
	
//	@RequestMapping(value = "/profiles/hkid/{paramCat}", method = RequestMethod.GET)
//	@ResponseBody
//	public ResponseResult getGlobalParameterListByParamCat(@PathVariable(value = "paramCat") String paramCat){
//		
//	}
	@RequestMapping(value = "/paramCat", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getGlobalParameterListByParamCat(@RequestParam(value = "paramCat",required = true)String paramCat){
		List<GlobalParameter> list = this.globalParameterService.getGlobalParameterListByParamCat(paramCat);
		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	
	@RequestMapping(value = "/saveOrUpdatelist", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult saveOrUpdateGlobalParameterList(@RequestBody List<GlobalParameter> list) {
		if(null==list || list.size()<=0){
			this.responseResult.initResult(GTAError.CommomError.DATA_ISSUE,"GlobalParameterList is null");
			return this.responseResult;
		}
		List<GlobalParameter> paramerList = new ArrayList<GlobalParameter>();
		for(GlobalParameter g : list){
			if(null!=g.getParamId()){
				GlobalParameter paramer = this.globalParameterService.getGlobalParameter(g.getParamId());
				paramer.setParamValue(g.getParamValue());
				paramer.setUpdateDate(new Date());
				paramer.setUpdateBy(this.getUser().getUserId());
				paramerList.add(paramer);
			}else {
				g.setStatus(Constant.Status.ACT.toString());
				g.setCreateBy(this.getUser().getUserId());
				g.setCreateDate(new Timestamp(System.currentTimeMillis()));
			}
		}
		if(paramerList.size()>0){
			this.globalParameterService.saveOrUpdateGlobalParameters(paramerList);
		}else {
			this.globalParameterService.saveOrUpdateGlobalParameters(list);
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return this.responseResult;
	}
}
