package com.sinodynamic.hkgta.controller.crm.sales.enrollment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.CustomerEmailDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class CustomerEnrollmentEmailController extends ControllerBase<ResponseResult> {

	private Logger logger = Logger.getLogger(CustomerEnrollmentEmailController.class);

	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private UserMasterService userMasterService;

	@RequestMapping(value = "/enrollment/getEmailTemplate", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult openEmail(@RequestParam("emailType") String emailType, @RequestParam("receiveCustomerID") Long receiveCustomerID) {
		logger.info("CustomerEnrollmentEmailController.openEmail start ...");

		try {
			TransactionEmailDto dto = customerEmailContentService.getOpenEmailTemplate(receiveCustomerID, EmailType.ACTIVATION.getFunctionId(), super.getUser().getUserName());
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerEnrollmentEmailController.openEmail Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	@RequestMapping(value = "/enrollment/sendActivationEmail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendActivationEmail(@RequestBody CustomerEmailDto dto) {
		logger.info("CustomerEnrollmentEmailController.sendActivationEmail start ...");

		try {
			LoginUser currentUser = getUser();
			UserMaster userMaster = userMasterService.getUserMasterByCustomerId(dto.getNumber());
			boolean isInitial = false;
			String password = null;
			if(userMaster!=null&&userMaster.getPasswdChangeDate()==null) {
				isInitial = true;
				password = CommUtil.generateRandomPassword();
				userMaster.setPassword(CommUtil.getMD5Password(userMaster.getLoginId(), password));
				userMasterService.updateUserMaster(userMaster);
			}
			
			CustomerEmailContent cec = customerEmailContentService.setEmailContentForMemberActivationEmail(dto.getNumber(),
					dto.getEmailType(),currentUser.getUserId(),currentUser.getFullname(),isInitial,userMaster==null?"":userMaster.getLoginId(),password);
			if(cec==null) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
			mailThreadService.sendWithResponse(cec, null, null, null);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerEnrollmentEmailController.sendActivationEmail Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	// private File[] readPdf(String emailType){
	// String pdfName = EmailUtil.generatePdfName(emailType);
	// String pdfPath = appProps.getProperty("pdf.remote.base.url") + pdfName;
	// File file = new File(pdfPath);
	// return new File[]{file};
	// }

}
