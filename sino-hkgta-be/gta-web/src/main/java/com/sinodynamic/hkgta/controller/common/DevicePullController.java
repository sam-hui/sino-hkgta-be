package com.sinodynamic.hkgta.controller.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.entity.crm.MsgPushHistory;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.fms.MsgPushHistoryService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/pull")
public class DevicePullController extends ControllerBase<String> {
    
    
	@Autowired
	private MsgPushHistoryService msgPushHistoryService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;

	@RequestMapping(value = "/messages", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult pullMessages(
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "deviceArn") String deviceArn,
			@RequestParam(value = "application") String application,
			@RequestParam(value = "sortBy", required = false, defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", required = false, defaultValue = "false") String isAscending, 
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
	    
	    	
	    	try {
	    	    
	    	    String joinHQL = msgPushHistoryService.getSqlForPullMessages(userId, deviceArn, application);
	    	    AdvanceQueryDto queryDtoAlias = new AdvanceQueryDto();
	    	    queryDtoAlias.setSortBy(sortBy);
	    	    queryDtoAlias.setAscending(isAscending);
	    	    page.setNumber(pageNumber);
	    	    page.setSize(pageSize);
	    	    return advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDtoAlias, joinHQL, page, MsgPushHistory.class);
		    
		} catch (Exception e) {
		    
		    logger.error(DevicePullController.class.getName() + " pullMessages Failded!", e);
		    responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
		    return responseResult;
		}
	}
	
}
