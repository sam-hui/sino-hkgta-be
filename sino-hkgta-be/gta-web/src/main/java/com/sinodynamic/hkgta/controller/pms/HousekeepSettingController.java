package com.sinodynamic.hkgta.controller.pms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.pms.GlobalParameterDto;
import com.sinodynamic.hkgta.dto.pms.HousekeepPresetParameterDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.pms.HousekeepPresetParameterService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
public class HousekeepSettingController extends ControllerBase {

	@Autowired
	private HousekeepPresetParameterService housekeepPresetParameterService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/setting/presetTask/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPresetTaskList(
			@RequestParam(value = "paramCat",required = false) String paramCat,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, 
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters
			/* @RequestBody(required = false) AdvanceQueryDto advanceQueryDto */) {
		try
		{
			AdvanceQueryDto advanceQueryDto =(AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			StringBuilder sb = new StringBuilder("select paramId as paramId,paramCat as paramCat, description as description,maxInt as maxInt,displayScope as displayScope from HousekeepPresetParameter where status != 'NACT' ");
			if (!StringUtils.isEmpty(paramCat)) {
				sb.append(" and paramCat = '" + paramCat + "' ");
			}
			String joinHQL = sb.toString();
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{
				joinHQL += " and ";
			}
			AdvanceQueryDto queryDto = advanceQueryDto;
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			return advanceQueryService.getAdvanceQueryResultByHQL(queryDto, joinHQL, page, HousekeepPresetParameterDto.class);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/setting/presetTask", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult saveHousekeepPresetParameter(@RequestBody HousekeepPresetParameterDto parameterDto)
	{
		logger.info("HousekeepSettingController.saveHousekeepPresetParameter start ...");
		boolean flag = checkSaveHousekeepPresetParameterData(parameterDto);
		if(!flag){
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		HousekeepPresetParameterDto result = null;
		try{
			parameterDto.setUpdateBy(this.getUser().getUserId());
			HousekeepPresetParameter housekeepPresetParameter = housekeepPresetParameterService.saveHousekeepPresetParameter(parameterDto);
			if(null != housekeepPresetParameter){
				result = new HousekeepPresetParameterDto();
				result.setParamId(housekeepPresetParameter.getParamId());
				result.setParamCat(housekeepPresetParameter.getParamCat());
				result.setDescription(housekeepPresetParameter.getDescription());
				result.setDisplayScope(housekeepPresetParameter.getDisplayScope());
				result.setMaxInt(housekeepPresetParameter.getMaxInt());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("HousekeepSettingController.saveHousekeepPresetParameter invocation end...");
		responseResult.initResult(GTAError.Success.SUCCESS,result);
		return responseResult;
	}
	
	@RequestMapping(value = "/setting/presetTask/{paramId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getHousekeepPresetParameter(@PathVariable(value = "paramId") Long paramId)
	{
		logger.info("HousekeepSettingController.getHousekeepPresetParameter start ...");
		if(null == paramId){
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		HousekeepPresetParameterDto result = null;
		try{
			HousekeepPresetParameter housekeepPresetParameter = housekeepPresetParameterService.getHousekeepPresetParameter(paramId);
			if(null != housekeepPresetParameter){
				result = new HousekeepPresetParameterDto();
				result.setParamId(housekeepPresetParameter.getParamId());
				result.setParamCat(housekeepPresetParameter.getParamCat());
				result.setDescription(housekeepPresetParameter.getDescription());
				result.setDisplayScope(housekeepPresetParameter.getDisplayScope());
				result.setMaxInt(housekeepPresetParameter.getMaxInt());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("HousekeepSettingController.getHousekeepPresetParameter invocation end...");
		responseResult.initResult(GTAError.Success.SUCCESS,result);
		return responseResult;
	}
	
	@RequestMapping(value = "/setting/presetTask/{paramId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editHousekeepPresetParameter(@PathVariable(value = "paramId") Long paramId,@RequestBody HousekeepPresetParameterDto parameterDto)
	{
		logger.info("HousekeepSettingController.editHousekeepPresetParameter start ...");
		boolean flag = checkSaveHousekeepPresetParameterData(parameterDto);
		if(!flag || paramId == null){
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		HousekeepPresetParameterDto result = null;
		try{
			parameterDto.setParamId(paramId);
			parameterDto.setUpdateBy(this.getUser().getUserId());
			HousekeepPresetParameter housekeepPresetParameter = housekeepPresetParameterService.editHousekeepPresetParameter(parameterDto);
			if(null != housekeepPresetParameter){
				result = new HousekeepPresetParameterDto();
				result.setParamId(housekeepPresetParameter.getParamId());
				result.setParamCat(housekeepPresetParameter.getParamCat());
				result.setDescription(housekeepPresetParameter.getDescription());
				result.setDisplayScope(housekeepPresetParameter.getDisplayScope());
				result.setMaxInt(housekeepPresetParameter.getMaxInt());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("HousekeepSettingController.editHousekeepPresetParameter invocation end...");
		responseResult.initResult(GTAError.Success.SUCCESS,result);
		return responseResult;
	}
	
	@RequestMapping(value = "/setting/presetTask/{paramId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteHousekeepPresetParameter(@PathVariable(value = "paramId") Long paramId)
	{
		logger.info("HousekeepSettingController.deleteHousekeepPresetParameter start ...");
		if(paramId == null){
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		HousekeepPresetParameterDto result = null;
		try{
			housekeepPresetParameterService.deleteHousekeepPresetParameter(paramId,this.getUser().getUserId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("HousekeepSettingController.deleteHousekeepPresetParameter invocation end...");
		responseResult.initResult(GTAError.Success.SUCCESS,result);
		return responseResult;
	}

	private boolean checkSaveHousekeepPresetParameterData(HousekeepPresetParameterDto parameterDto)
	{
		if(null == parameterDto || parameterDto.getParamCat() == null || parameterDto.getDescription()==null){
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/setting/responsibleTime/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getResponsibleTimeList(
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, 
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters
			/* @RequestBody(required = false) AdvanceQueryDto advanceQueryDto */) {
		try {
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			String joinHQL = "select paramId as paramId,paramValue as paramValue,description as description from GlobalParameter  where paramCat = 'HK-MAXRESP' ";
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{
				joinHQL += " and ";
			}
			AdvanceQueryDto queryDto = advanceQueryDto;
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			responseResult = advanceQueryService.getAdvanceQueryResultByHQL(queryDto, joinHQL, page, GlobalParameterDto.class);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/setting/presetTask/paramCatList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getParamCatList()
	{
		try{
			List<HousekeepPresetParameter> housekeepPresetParameters = housekeepPresetParameterService.getHousekeepPresetParameterCatList();
			List<HousekeepPresetParameterDto> result = new ArrayList<HousekeepPresetParameterDto>();
			if(null != housekeepPresetParameters && housekeepPresetParameters.size() > 0){
				for(HousekeepPresetParameter parameter : housekeepPresetParameters){
					HousekeepPresetParameterDto dto = new HousekeepPresetParameterDto();
					dto.setParamCat(parameter.getParamCat());
					result.add(dto);
				}
			}
			logger.info("HousekeepSettingController.getParamCatList invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS,result);
			return responseResult;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	@RequestMapping(value = "/setting/responsibleTime/{paramId}/{paramValue}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editResponsibleTime(@PathVariable(value = "paramId") String paramId,@PathVariable(value = "paramValue") String paramValue)
	{
		logger.info("HousekeepSettingController.editResponsibleTime start ...");
		if(StringUtils.isEmpty(paramId) || StringUtils.isEmpty(paramValue)){
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		try{
			GlobalParameter globalParameter = globalParameterService.getGlobalParameter(paramId);
			if(null != globalParameter){
				globalParameter.setUpdateBy(this.getUser().getUserId());
				globalParameter.setUpdateDate(new Date());
				globalParameter.setParamValue(paramValue);
				globalParameterService.updateGlobalParameter(globalParameter);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("HousekeepSettingController.editResponsibleTime invocation end...");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
}
