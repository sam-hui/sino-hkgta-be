package com.sinodynamic.hkgta.scheduler;

import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;

@Component
public class OnlinePaymentCheckTask {
	private Logger logger = Logger.getLogger(OnlinePaymentCheckTask.class);

	@Resource(name = "appProperties")
	private Properties appProps;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;

	
	/**
	 * This method is used to check whether the online payment is success.
	 */
	public void checkOnlinePaymentStatus() {

		logger.info("OnlinePaymentCheckTask.checkOnlinePaymentStatus() start ..");
		
		List<CustomerOrderTrans> customerOrderTranses = customerOrderTransService.getTimeoutPayments();
		for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
			customerOrderTrans = paymentGatewayService.queryResult(customerOrderTrans);
			customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
		}
		
		logger.info("OnlinePaymentCheckTask.checkOnlinePaymentStatus() end ..");
	}
	
	public void test() {
		System.out.println(" i am executing...");
	}
}
