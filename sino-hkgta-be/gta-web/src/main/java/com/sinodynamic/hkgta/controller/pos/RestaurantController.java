package com.sinodynamic.hkgta.controller.pos;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantCustomerBookingDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantMasterDto;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.pos.RestaurantCustomerBookingService;
import com.sinodynamic.hkgta.service.pos.RestaurantMasterService;
import com.sinodynamic.hkgta.service.pos.RestaurantOpenHourService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RestaurantCustomerBookingStatus;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class RestaurantController extends ControllerBase {

	private Logger logger = Logger.getLogger(RestaurantController.class);

	@Autowired
	private RestaurantMasterService restaurantMasterService;
	
	@Autowired
	private RestaurantCustomerBookingService restaurantCustomerBookingService;
	
	@Autowired
    private CustomerProfileService customerProfileService;
	
	@Autowired
	private ShortMessageService  smsService;
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	
	@Autowired
	private RestaurantOpenHourService restaurantOpenHourService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@RequestMapping(value = "/restaurants", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantList(@RequestParam(value = "featureCode",required=false) String imageFeatureCode)
	{
		List<RestaurantMasterDto> retLst = restaurantMasterService.getRestaurantMasterList(imageFeatureCode);
		responseResult.initResult(GTAError.Success.SUCCESS, retLst);
		return responseResult;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/restaurants/{restaurantId}/reservations", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantReservations(@PathVariable(value = "restaurantId") String restaurantId,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber, 
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@RequestParam(value = "isHistory", defaultValue = "false") Boolean isHistory, 
			@RequestParam(value = "customerId", required = false) Long customerId,
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters) {

		try {
			int updateCount = restaurantCustomerBookingService.updateRestaurantCustomerBookingExpired();
			logger.info("update Restaurant Customer Booking Expired count: "+updateCount);
			AdvanceQueryDto advanceQueryDto =(AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			StringBuilder sql = new StringBuilder();
			sql.append("select rcb.resv_Id as resvId,rcb.restaurant_Id as restaurantId,"
					+ " date_format(book_Time,'%Y-%m-%d %H:%i:%s') as bookTime,rcb.party_Size as partySize,rcb.status as status,rcb.book_Via as bookVia,rcb.remark as remark, "
					+ " m.memberName as memberName,m.academyNo as academyNo "
					+ " from restaurant_customer_booking rcb left join (select m.customer_Id as customerId, m.academy_No as academyNo,concat(cp.salutation,' ',cp.given_name,' ',cp.surname) as memberName from member m join customer_profile cp on m.customer_Id = cp.customer_Id)m on m.customerId = rcb.customer_Id ");
			if (!StringUtils.isEmpty(restaurantId)) {
					sql.append(" where rcb.restaurant_id = '" + restaurantId + "' ");
				if (customerId != null && customerId > 0)
					sql.append(" and rcb.customer_id = " + customerId);
			}
			if(isHistory){
				sql.append(" and rcb.status in ('"+RestaurantCustomerBookingStatus.ATN.name()+"','"+RestaurantCustomerBookingStatus.DEL.name()+"','"+RestaurantCustomerBookingStatus.EXP.name()+"')");
			}else{
				sql.append(" and rcb.status  = '"+RestaurantCustomerBookingStatus.CFM.name()+"'");
			}
			String joinSQL = sql.toString();
			
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{
				joinSQL += " and ";
				AdvanceQueryDto queryDto = advanceQueryDto;
				queryDto.setSortBy(advanceQueryDto.getSortBy());
				queryDto.setAscending(advanceQueryDto.getAscending());
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinSQL, page, RestaurantCustomerBookingDto.class);
			}
			else
			{
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(advanceQueryDto.getSortBy());
				queryDto.setAscending(advanceQueryDto.getAscending());
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, RestaurantCustomerBookingDto.class);
			}
		} catch (Exception e) {
			logger.error("Error querying restaurant reservations", e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
			return responseResult;
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/restaurants/{restaurantId}/reservations", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult createRestaurantCustomerBooking(@PathVariable(value = "restaurantId") String restaurantId,@RequestBody RestaurantCustomerBookingDto bookingDto)
	{
		try{
			if (StringUtils.isEmpty(restaurantId) || null == bookingDto || bookingDto.getCustomerId() == null || null == bookingDto.getBookTime() || null == bookingDto.getPartySize() || bookingDto.getPartySize()==0)
			{
				responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
				return responseResult;
			}else if(!StringUtils.isEmpty(bookingDto.getRemark()) && bookingDto.getRemark().length() >300){
				responseResult.initResult(GTAError.RestaurantError.REMARK_TOO_LONG);
				return responseResult;
			}else if(DateCalcUtil.parseDateTime(bookingDto.getBookTime()).before(new Date())){
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_LESSTHAN_CURRENTTIME);
				return responseResult;
			}else if(DateCalcUtil.parseDate(bookingDto.getBookTime()).after(getFutureMonth(new Date(),3))){
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_OVER_ADVANCETIME);
				return responseResult;
			}
			
			bookingDto.setRestaurantId(restaurantId);
			responseResult = restaurantCustomerBookingService.saveRestaurantCustomerBooking(getUser().getUserId(),bookingDto);
			try{
				if(GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())){
					RestaurantCustomerBookingDto result = (RestaurantCustomerBookingDto) responseResult.getData();
					restaurantCustomerBookingService.sendRestaurantBookingSMS(result.getResvId().longValue(),"restaurant_book_confirm");
				}
			}catch (Exception e){
				logger.error(e.getMessage());
			}
		}catch (Exception e){
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/{resvId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult cancelRestaurantCustomerBooking(@PathVariable(value = "resvId") Long resvId)
	{
		try{
			responseResult = restaurantCustomerBookingService.cancelRestaurantCustomerBooking(getUser().getUserId(),resvId);
			
			try{
				if(GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())){
					RestaurantCustomerBookingDto result = (RestaurantCustomerBookingDto) responseResult.getData();
					restaurantCustomerBookingService.sendRestaurantBookingSMS(result.getResvId().longValue(),"restaurant_book_cancelled");
				}
			}catch (Exception e){
				logger.error(e.getMessage());
			}
		}catch (Exception e){
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantCustomerBooking(@PathVariable(value = "resvId") Long resvId)
	{
		if (null == resvId)
		{
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		RestaurantCustomerBookingDto rcb = restaurantCustomerBookingService.getRestaurantCustomerBookingByResvId(resvId);
		responseResult.initResult(GTAError.Success.SUCCESS, rcb);
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/{resvId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateRestaurantCustomerBooking(@PathVariable(value = "resvId") Long resvId,@RequestBody RestaurantCustomerBookingDto bookingDto)
	{
		try
		{
			if (null == resvId || null == bookingDto || null == bookingDto.getBookTime() || null == bookingDto.getPartySize())
			{
				responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
				return responseResult;
			}else if(!StringUtils.isEmpty(bookingDto.getRemark()) && bookingDto.getRemark().length() >300){
				responseResult.initResult(GTAError.RestaurantError.REMARK_TOO_LONG);
				return responseResult;
			}else if(DateCalcUtil.parseDateTime(bookingDto.getBookTime()).before(new Date())){
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_LESSTHAN_CURRENTTIME);
				return responseResult;
			}else if(DateCalcUtil.parseDate(bookingDto.getBookTime()).after(getFutureMonth(new Date(),3))){
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_OVER_ADVANCETIME,new Object[]{3});
				return responseResult;
			}
			bookingDto.setResvId(resvId);
			responseResult = restaurantCustomerBookingService.updateRestaurantCustomerBooking(getUser().getUserId(),bookingDto);
			try{
				if(GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())){
					RestaurantCustomerBookingDto result = (RestaurantCustomerBookingDto) responseResult.getData();
					restaurantCustomerBookingService.sendRestaurantBookingSMS(result.getResvId().longValue(),"restaurant_book_updated");
				}
			}catch (Exception e){
				logger.error(e.getMessage());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/available", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult available(@PathVariable(value = "restaurantId") String restaurantId,@RequestParam Integer partySize,@RequestParam String bookTime)
	{
		if (StringUtils.isEmpty(restaurantId) || StringUtils.isEmpty(bookTime))
		{
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try
		{
			boolean flag = restaurantOpenHourService.isAvailableRestaurant(restaurantId,partySize, DateCalcUtil.parseDateTime(bookTime),Constant.RESTAURANT_BOOK_VIA_FD);
			responseResult.initResult(GTAError.Success.SUCCESS,flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.Success.SUCCESS,false);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/member/restaurants/{restaurantId}/reservations/available", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult availableMemberBooking(@PathVariable(value = "restaurantId") String restaurantId,@RequestParam Integer partySize,@RequestParam String bookTime)
	{
		if (StringUtils.isEmpty(restaurantId) || StringUtils.isEmpty(bookTime))
		{
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try
		{
			boolean flag = restaurantOpenHourService.isAvailableRestaurant(restaurantId,partySize, DateCalcUtil.parseDateTime(bookTime),Constant.RESTAURANT_BOOK_VIA_OL);
			responseResult.initResult(GTAError.Success.SUCCESS,flag);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.Success.SUCCESS,false);
		}
		return responseResult;
	}
	
	public Date getFutureMonth(Date date,int month) throws Exception{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		return DateCalcUtil.parseDate(DateCalcUtil.formatDate(cal.getTime()));
	}
}
