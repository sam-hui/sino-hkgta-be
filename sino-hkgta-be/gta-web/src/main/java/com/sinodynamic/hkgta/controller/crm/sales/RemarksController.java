package com.sinodynamic.hkgta.controller.crm.sales;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.RemarksDto;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.RemarksService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
public class RemarksController extends ControllerBase<CustomerPreEnrollStage> {

	private Logger logger = Logger.getLogger(CustomerPreEnrollStage.class);
	
	@Autowired
	private RemarksService remarksService;
	
	/**
	 * Method to display unread remark
	 * @param customerId
	 * @return Response Result
	 */
	@RequestMapping(value="/remarks/countUnread/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	@Deprecated
	public ResponseResult displayUnreadRemark(@PathVariable Long customerId){
		try {
			LoginUser currentUser = getUser();
			return remarksService.checkUnreadRemark(customerId,currentUser.getUserId());
		} catch (Exception e) {
			logger.debug("RemarksController Error Message:", e);
			responseResult.initResult(GTAError.RemarksError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value="/remarks", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult addLeadRemarkByDevice(
			@RequestBody RemarksDto remarksDto){
		try {
			LoginUser currentUser = getUser();
			if(StringUtils.isEmpty(remarksDto.getCommentByDevice())){
				return remarksService.addRemark(remarksDto,currentUser.getUserId(),currentUser.getStaffType());
			}else{
				return remarksService.addRemarkByDevice(remarksDto,currentUser.getUserId(),remarksDto.getCommentByDevice());	
			}
			
		} catch (Exception e) {
			logger.debug("RemarksController Error Message:", e);
			responseResult.initResult(GTAError.RemarksError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	
	@RequestMapping(value="/remarks", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRemarkByCustomerIdAndDevice(
			@RequestParam(value = "customerId",required = true) Long customerId,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "sortBy", defaultValue = "commentDate") String sortBy,
			@RequestParam(value = "device",required = false) String device){
		try {
			if (isAscending.equals("true")) {
				page.addAscending(sortBy);
			} else if (isAscending.equals("false")) {
				page.addDescending(sortBy);
			}
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			LoginUser currentUser = getUser();
			if(StringUtils.isEmpty(device)){
				return remarksService.getRemarkList(page, customerId,currentUser.getUserId(),currentUser.getStaffType());
			}else{
				return remarksService.getRemarkListByDevice(page, customerId, getUser().getUserId(), device);
			}
			
		} catch (Exception e) {
			logger.debug("RemarksController Error Message:", e);
			responseResult.initResult(GTAError.RemarksError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	
	@RequestMapping(value="/remarks/countUnread/{device}/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult displayUnreadRemarkByDevice(
			@PathVariable(value = "customerId" ) Long customerId,
			@PathVariable(value = "device" )String device){
		try {
			LoginUser currentUser = getUser();
			return remarksService.checkUnreadRemarkByDevice(customerId,currentUser.getUserId(),device);
		} catch (Exception e) {
			logger.debug("RemarksController Error Message:", e);
			responseResult.initResult(GTAError.RemarksError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
}
