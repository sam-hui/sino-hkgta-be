package com.sinodynamic.hkgta.controller.crm.sales.transaction;



import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailAttachService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class TransactionEmailController extends ControllerBase<CustomerEmailContent>{
	
	private Logger logger = Logger.getLogger(TransactionEmailController.class);
	
	@Autowired
	private CustomerEmailContentService service;
	
	@Autowired
	private CustomerEmailAttachService customerEmailAttachService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private MailThreadService mailThreadService;
	                                                                                                                                       
	@RequestMapping(value="/transaction/getEmailTemplate", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult openEmail(@RequestParam("emailType") String emailType,@RequestParam("number") Long number){
		logger.info("TransactionEmailController.openEmail start ...");
		
		ResponseResult result = new ResponseResult("0", "Success", "Success");
				
		try {
			TransactionEmailDto dto = service.getOpenEmailTemplate(number, emailType,super.getUser().getUserName());
			result.setData(dto);
		} catch (Exception e) {
			logger.error(e.toString());
			result = new ResponseResult("1", "error", "error");
		}
					
		logger.info("TransactionEmailController.openEmail end ...");
		
		return result;
	}
	

	@RequestMapping(value="/transaction/sendInvoiceEmail", method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendInvoiceEmail(@RequestBody TransactionEmailDto dto) {	
		logger.info("TransactionEmailController.sendInvoiceEmail start ...");
		logger.info("Param is [" + dto.toString() + "]");
		dto.setEmailType(EmailType.INVOICE.toString());
		return sendEmail(dto);
		
	}
	
	@RequestMapping(value="/transaction/sendReceiptEmail", method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendReceiptEmail(@RequestBody TransactionEmailDto dto) {		
		logger.info("TransactionEmailController.sendReceiptEmail start ...");
		logger.info("Param is ["+ dto.toString() + "]");
		if (EmailType.DAYPASSRECEIPT.name().equalsIgnoreCase(dto.getEmailType())) {
			dto.setEmailType(dto.getEmailType());
		}else {
			dto.setEmailType(EmailType.RECEIPT.toString());
		}
		customerOrderTransService.getEmailDeailReady(dto,getUser().getUserName());
		return sendEmail(dto);
	}
	
	private ResponseResult sendEmail(TransactionEmailDto dto){

		try {	
			LoginUser currentUser = getUser();
			LoginUserDto userDto = new LoginUserDto();
			userDto.setUserName(currentUser.getUserName());
			userDto.setUserId(currentUser.getUserId());
			
			responseResult = customerOrderTransService.sentTransactionEmail(dto,userDto);
			
			/*CustomerEmailContent cec = service.sendEmail4Transaction(dto,currentUser.getUserName());
//			File[] pdfFiles = readPdf(dto.getEmailType());
			List<byte[]> attach = new ArrayList<byte[]>();
			String fileName = "ATTACHMENT"+".pdf";
			if(!StringUtils.isEmpty(dto.getEmailType())){
				fileName = dto.getEmailType().toUpperCase();
			}
			if(EmailType.RECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
				byte[] receiptAttach = customerOrderTransService.getInvoiceReceipt(null, dto.getTransactionNO().toString(),"serviceplan");
				attach.add(receiptAttach);
				fileName = fileName+"-"+dto.getTransactionNO().toString()+".pdf";
			}else if(EmailType.INVOICE.name().equalsIgnoreCase(dto.getEmailType())){
				byte[] invoiceAttach = customerOrderTransService.getInvoiceReceipt(dto.getOrderNO(), null,"serviceplan");
				attach.add(invoiceAttach);
				fileName = fileName+"-"+dto.getOrderNO()+".pdf";
			}else if(EmailType.DAYPASSRECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
				CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTransListByOrderNo(dto.getOrderNO()).get(0);
				byte[] invoiceAttach = customerOrderTransService.getInvoiceReceipt(dto.getOrderNO(), customerOrderTrans.getTransactionNo().toString(),"daypass");
				attach.add(invoiceAttach);
				fileName = fileName+"-"+dto.getOrderNO()+".pdf";
			}
			
			CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
			customerEmailAttach.setEmailSendId(cec.getSendId());
			customerEmailAttach.setAttachmentName(fileName);
			customerEmailAttachService.savecustomerEmailAttach(customerEmailAttach);
			List<String> fileNameList = new ArrayList<String>();
			fileNameList.add(fileName);
			List<String> mineTypeList = new ArrayList<String>();
			mineTypeList.add("application/pdf");
			boolean validateSend = true;
			if(EmailType.RECEIPT.name().equalsIgnoreCase(dto.getEmailType())||EmailType.INVOICE.name().equalsIgnoreCase(dto.getEmailType()) || EmailType.DAYPASSRECEIPT.name().equalsIgnoreCase(dto.getEmailType())){
				validateSend =MailSender.sendEmailWithBytesMineAttach(cec.getRecipientEmail(), dto.getCc(), null, cec.getSubject(), cec.getContent(), mineTypeList,attach,fileNameList);
			}else{
				validateSend = MailSender.sendEmail(cec.getRecipientEmail(), dto.getCc(), null, cec.getSubject(), cec.getContent(), null);
			}
			if(validateSend){
				cec.setStatus(EmailStatus.SENT.name());
				service.modifyCustomerEmailContent(cec);
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}else{
				cec.setStatus(EmailStatus.FAIL.name());
				service.modifyCustomerEmailContent(cec);
				responseResult = new ResponseResult("1", "Fail!");
			}*/
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("TransactionEmailController.sendEmail Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	
	private File[] readPdf(String emailType){
		String pdfName = CommUtil.generatePdfName(emailType);
		String pdfPath = appProps.getProperty("pdf.remote.base.url") + pdfName;		
		File file = new File(pdfPath);		
		return new File[]{file};
	}
}
