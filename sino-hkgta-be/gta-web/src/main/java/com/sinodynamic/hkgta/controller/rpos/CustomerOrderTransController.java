package com.sinodynamic.hkgta.controller.rpos;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.cardmanage.PCDayPassPurchaseService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping(value = "/transaction")
public class CustomerOrderTransController extends ControllerBase {
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	@Autowired
	private PCDayPassPurchaseService pcDayPassPurchaseService;
	public Logger logger = Logger.getLogger(CustomerOrderTransController.class);

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionList(
			@RequestParam(value = "balanceDue", defaultValue = "All") String balanceDue,
			@RequestParam(value = "serviceType", defaultValue = "All") String serviceType,
			@RequestParam(value = "sortBy", defaultValue = "enrollCreateDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") boolean isAscending,
			@RequestParam(value = "searchText", required= false,defaultValue = "") String searchText,
			@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "0") int pageSize,
			@RequestParam(value = "filterBy", defaultValue = "MY") String filterBy,
			@RequestParam(value = "enrollStatusFilter", defaultValue = "All") String enrollStatusFilter,
			@RequestParam(value = "mode", defaultValue = "saleskit") String mode,
			@RequestParam(value="filters",required= false,defaultValue= "") String filters,
			@RequestParam(value="memberType",required= false,defaultValue= "IPM") String memberType,
			@RequestParam(value = "device", defaultValue = "IOS") String device
			) {
		try {
			logger.info("Ready to get transaction list");
			page = new ListPage<CustomerOrderHd>();
			String filterBySalesMan = null;
			if(pageNumber>0){
				page.setNumber(pageNumber);
			}
			if(pageSize > 0){
				page.setSize(pageSize);
			}
			if(filterBy.equalsIgnoreCase("my")){
				String loginUserId =  getUser().getUserId();
				filterBySalesMan = (loginUserId == "-1")?"":loginUserId;
			}else if(filterBy.equalsIgnoreCase("ALL")){
				filterBySalesMan = "";
			}
			LoginUser currentUser = getUser();
			if(CommUtil.notEmpty(filters)){
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if(advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0){
					if(sortBy != null && sortBy.length()>0 ){
						advanceQueryDto.setSortBy(sortBy);
					}
					advanceQueryDto.setAscending(isAscending?" true ":" false ");
//					for(SearchRuleDto rule: advanceQueryDto.rules){
//						if("t.offerCode".equals(rule.field)&&"ENROLLMENT".equals(rule.data.toUpperCase())){
//							rule.setData(null);
//							rule.setOp(CommonDataQueryDao.IS_NULL);
//						}
//					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			ListPage<CustomerOrderHd> result = customerOrderTransService
					.getCustomerTransactionList(page, balanceDue, serviceType, sortBy,isAscending,searchText,filterBySalesMan, mode,currentUser.getUserId(), enrollStatusFilter,memberType,device);
			ResponseResult response = new ResponseResult();
			Data data = new Data();
			data.setList(result.getDtoList());
			data.setLastPage(result.isLast());
			data.setCurrentPage(result.getNumber());
			data.setRecordCount(result.getAllSize());
			data.setPageSize(result.getSize());
			data.setTotalPage(result.getAllPage());
			response.setData(data);
			response.setReturnCode("0");
			response.setErrorMessageEN("");
			return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			e.printStackTrace();
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/getAll/advancedSearch", method = RequestMethod.GET)
	public ResponseResult advanceSearch(@RequestParam(value = "serviceType", defaultValue = "Enrollment") String serviceType){
		responseResult.initResult(GTAError.Success.SUCCESS, customerOrderTransService.assembleQueryConditions(serviceType));
		return responseResult;
	} 

	@RequestMapping(value = "/saveCustomerOrderTrans", method = RequestMethod.PUT)
	@ResponseBody
	public  ResponseResult saveCustomerOrderTrans(
			@RequestBody CustomerOrderTransDto customerOrderTransDto) {
		
		try {

			JSONObject jsonObject = JSONObject.fromObject(customerOrderTransDto);
			Map<String, Long> transactionMap = new HashMap<String, Long>();
			System.out.println(jsonObject);
			if (customerOrderTransDto.getPaymentReceivedBy() == null || customerOrderTransDto.getPaymentReceivedBy().length() == 0) {
				customerOrderTransDto.setPaymentReceivedBy(this.getUser().getUserName());
			}
			customerOrderTransDto.setCreateByUserId(getUser().getUserId());
			customerOrderTransDto.setCreateByUserName(getUser().getUserName());
			ResponseResult responseResult = customerOrderTransService.savecustomerOrderTransService(customerOrderTransDto, transactionMap);
			// CreadCard return transaction No for POS purpose
			if (Constant.PaymentMethodCode.VISA.name().equals(customerOrderTransDto.getPaymentMethodCode())) {
				responseResult.setData(transactionMap);
				return responseResult;
			}
			if (responseResult.getReturnCode().equals("0")) {
				responseResult = customerOrderTransService.getPaymentDetailsByOrderNo(customerOrderTransDto.getOrderNo());
			}
			return responseResult;
		} catch (GTACommonException gta) {
			gta.printStackTrace();
			logger.debug("CustomerOrderTransController.saveCustomerOrderTrans Exception",gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerOrderTransController.saveCustomerOrderTrans Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	

	/**
	 * Used to handle the call back of POS payment for paying the service payment.
	 * E.g. Service payment: Enrollment, Renewal
	 * @author Changpan_Wu
	 * @param posResponse
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/servicePayment/posCallback", method = RequestMethod.POST)
	@ResponseBody
    public String callback(@RequestBody PosResponse posResponse) {
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug("add payment by credit card:"+gson.toJson(posResponse));
		responseResult = customerOrderTransService.handleCreditCardByPos(posResponse);
        return responseResult.getReturnCode();
    }
	/**
	 * Used to handle the call back of POS payment for purchase  payment.
	 * E.g. Service payment: Enrollment, Renewal
	 * @author Changpan_Wu
	 * @param posResponse
	 * @return
	 */
	@RequestMapping(value = "/daypassPayment/posCallback", method = RequestMethod.POST)
	@ResponseBody
    public String purchaseDaypassCallback(@RequestBody PosResponse posResponse) {
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug("add payment by credit card:"+gson.toJson(posResponse));
		responseResult = customerOrderTransService.handlePosPayDaypass(posResponse);
		if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())){
			DaypassPurchaseDto dto = new DaypassPurchaseDto();
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			dto.setTransactionNo(transactionNo);
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			dto.setOrderNo(customerOrderTrans.getCustomerOrderHd().getOrderNo());
			pcDayPassPurchaseService.sendEmail(dto,this.getUser().getUserId());
		}
        return responseResult.getReturnCode();
	}
	
	
	@RequestMapping(value = "/readTrans", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult readTrans(
			@RequestBody CustomerOrderTransDto customerOrderTransDto) {
		if (customerOrderTransDto != null && customerOrderTransDto.getTransactionNo() > 0) {
			customerOrderTransDto.setReadBy(this.getUser().getUserId());
		}
		ResponseResult responseResult = customerOrderTransService.readTrans(customerOrderTransDto);
		/*if (responseResult.getReturnCode().equals("0")){
			responseResult = customerOrderTransService.getPaymentDetailsByOrderNo(customerOrderTransDto.getOrderNo());
		}*/
		return responseResult;
	}
	
	@RequestMapping(value = "/paymentByCreditCard", method = RequestMethod.POST)
	public @ResponseBody ResponseResult  paymentByCreditCard(
			@RequestBody CustomerOrderTransDto customerOrderTransDto) {
		if (customerOrderTransDto.getPaymentReceivedBy() == null 
				|| customerOrderTransDto.getPaymentReceivedBy().length() == 0) {
			customerOrderTransDto.setPaymentReceivedBy(this.getUser().getUserName());
		}
		customerOrderTransDto.setCreateByUserId(getUser().getUserId());
		ResponseResult responseResult = customerOrderTransService
				.paymentByCreditCard(customerOrderTransDto);
		if ("0".equals(responseResult.getReturnCode())) {
			CustomerOrderTrans customerOrderTrans = (CustomerOrderTrans)responseResult.getDto();
			String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
			Map<String, String> redirectUrlMap = new HashMap<String, String>();
			redirectUrlMap.put("redirectUrl", redirectUrl);
			responseResult.setData(redirectUrlMap);
		}
		return responseResult;
	}

	@RequestMapping(value = "/getPaymentDetails/{orderNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getPaymentDetailsByOrderNo(
			@PathVariable(value = "orderNo") Long orderNo) {
		// String pageSize = request.getParameter("pageSize");
		// String pageNumber = request.getParameter("pageNumber");
		// page.setNumber(Integer.parseInt(pageNumber));
		// page.setSize(Integer.parseInt(pageSize));
		ResponseResult responseResult = customerOrderTransService
				.getPaymentDetailsByOrderNo(orderNo);
		return responseResult;
	}

	@RequestMapping(value = "/getPaymentDetailsAccountant/{orderNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getPaymentDetailsAccountantByOrderNo(
			@PathVariable(value = "orderNo") Long orderNo) {
		ResponseResult responseResult = customerOrderTransService
				.getPaymentDetailsAccountantByOrderNo(orderNo);
		return responseResult;
	}

	@RequestMapping(value = "/uploadAttachment", method = RequestMethod.POST)
	public @ResponseBody ResponseResult uploadAttachment(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		ResponseResult responseResult = new ResponseResult();
		try {
			if (file == null || file.isEmpty()) {
				logger.info("File is empty!");
				responseResult.setReturnCode("1");
				responseResult.setErrorMessageEN("File is empty!");
				return responseResult;
			}
			String saveFilePath = "";
				saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.USER);
				
			File serverFile = new File(saveFilePath);
		
			saveFilePath = serverFile.getName();
			responseResult.setReturnCode("0");
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("imagePath", "/"+saveFilePath);
			responseResult.setData(imgPathMap);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("upload file fail");
			return responseResult;
		}
	}

	@RequestMapping(value = "/getTransactionStatus/{transactionNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionStatus(@PathVariable(value = "transactionNo") Long transactionNo){
		CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
		if (null == customerOrderTrans) {
			responseResult.initResult(GTAError.PaymentError.TRANSACTION_NOT_EXISTS,new String[]{transactionNo.toString()});
		}else {
			Map<String, String> statusMap = new HashMap<String, String>();
			statusMap.put("status", customerOrderTrans.getStatus());
			responseResult.initResult(GTAError.Success.SUCCESS,statusMap);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/{transactionNo}/status", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateCustomerOrderTransStatusToFail(
			@PathVariable(value = "transactionNo") Long transactionNo) {
		CustomerOrderTrans trans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
		if(null != trans){
			trans.setStatus(Constant.Status.FAIL.name());
			trans.setUpdateBy(getUser().getUserId());
			trans.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderTransService.updateCustomerOrderTrans(trans);
			responseResult.initResult(GTAError.Success.SUCCESS,trans);
		}
		return responseResult;
	}
}