package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CorporateMemberDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberRightsDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.dto.membership.QuestionAnswerOrBirthDateDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.CorporateService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.DependentMemberService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberTransactionService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MembershipCardsManagmentService;
import com.sinodynamic.hkgta.service.crm.cardmanage.DayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings({ "rawtypes" })
@Controller
@RequestMapping("/membership")
@Scope("prototype")
public class CustomerMemberController extends ControllerBase {
	
	private Logger logger = Logger.getLogger(CustomerMemberController.class);
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private MemberTransactionService memberCashValueService;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	private @Autowired DependentMemberService dependentMemberService;
	
	private @Autowired MembershipCardsManagmentService membershipCardsManagmentService;
	
	@Autowired
	private DayPassPurchaseService dayPassPurchaseService;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private CorporateService corporateService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private MemberTransactionService memberTransactionService; 
	
	@RequestMapping(value = "/getMemberList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberList(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="") String sortBy,
			@RequestParam(value="isAscending",defaultValue="true") String isAscending,
			@RequestParam(value="filterByCustomerId",defaultValue="ALL") String filterByCustomerId,
			@RequestParam(value="status",defaultValue="ALL") String status,
			@RequestParam(value="expiry",defaultValue="ALL") String expiry,
			@RequestParam(value="planName",defaultValue="") String planName,
			@RequestParam(value="memberType",defaultValue="") String memberType,
			@RequestParam(value="filters",defaultValue="") String filters,
			HttpServletRequest request, HttpServletResponse response) {

		try {
			logger.info("Ready to get member overview list");
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if(advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0){
					for(SearchRuleDto rule: advanceQueryDto.rules){
						if(logger.isDebugEnabled()){
							logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:" + rule.getData());
						}
						if("t.status".equals(rule.field)&&"ACTIVE".equals(rule.data.toUpperCase())){
							rule.setData(Constant.General_Status_ACT);
						}
						else if("t.status".equals(rule.field)&&"INACTIVE".equals(rule.data.toUpperCase())){
							rule.setData(Constant.General_Status_NACT);
						}
						else if("t.status".equals(rule.field)&&"EXPIRED".equals(rule.data.toUpperCase())){
							rule.setData(Constant.General_Status_EXP);
						}
						if("t.planName".equals(rule.field)&&(rule.data.indexOf("'")>0)){
							rule.setData(rule.data.replace("'", "\\'"));
						}
					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			ResponseResult result= memberTransactionService.getMembersOverviewList(page, sortBy, isAscending, filterByCustomerId, status, expiry, planName,memberType);
			logger.info("Member overview list retrieve end...");
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getMemberList Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@ResponseBody
	@RequestMapping(value ="/getMemberList/advancedSearch", method=RequestMethod.GET)
	public ResponseResult advanceSearch(){
		responseResult.initResult(GTAError.Success.SUCCESS, memberTransactionService.assembleQueryConditions());
		return responseResult;
	} 
	
	@RequestMapping(value = "/getPersonalInfo/{customerID}",method = RequestMethod.GET)
	@ResponseBody
	private ResponseResult getCustomerInfo(@PathVariable("customerID") Long customerID) {
		CustomerProfile customerProfile = null;
		try {
			customerProfile = customerProfileService.getCustomerInfoByCustomerID(customerID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getCustomerInfo Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS,customerProfile);
		return responseResult;
	}
	
	@RequestMapping(value = "/getAccountInfo/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAccountInfo(@PathVariable("customerID") Long customerID){
		ResponseResult responseResult = new ResponseResult();
		responseResult = customerProfileService.getAccountInfo(customerID);
		return responseResult;
	}
	
	@RequestMapping(value = "/editLimitValue", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editLimitValue(@RequestBody CorporateMemberDto dto){
		try{
			BigDecimal creditLimit = dto.getTotalCreditLimit();
			if (null != creditLimit) {
				int check = creditLimit.compareTo(new BigDecimal(99999999.99).setScale(2,BigDecimal.ROUND_HALF_UP));
				if(!StringUtils.isEmpty(creditLimit)&&check==1){
					//return new ResponseResult("1", "", "The limit value exceeds the maximum! Maximum is 99999999.99!");
					responseResult.initResult(GTAError.MemberShipError.CREDIT_LIMIT_EXCEEDS_THE_MAXIMUM);
					return responseResult;
				}
				if (BigDecimal.ZERO.compareTo(creditLimit)>0) {
					//return new ResponseResult("1", "", "The limit value can not be negative!");
					responseResult.initResult(GTAError.MemberShipError.CREDIT_LIMIT_VALUE_INVALID);
					return responseResult;
				}
			}
			return customerProfileService.editLimitValue(dto.getCustomerId(),dto.getTotalCreditLimit(),getUser().getUserId(),dto.getMemberLimitRuleVersion());
		} catch(HibernateOptimisticLockingFailureException concurrent) {
				concurrent.printStackTrace();
				logger.debug("CustomerMemberController.editLimitValue Exception : ", concurrent);
				responseResult.initResult(GTAError.CommonError.BEEN_UPDATED, new String[]{"Credit Limit"});
				return responseResult;
		} catch(Exception e){
			e.printStackTrace();
			logger.debug("CustomerMemberController.editLimitValue Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	@RequestMapping(value= "/transaction/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCurrentBalance(@PathVariable("customerId") Long customerId){
		try {
			return memberCashValueService.getCurrentBalance(customerId);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getCurrentBalance Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value= "/transaction/total/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTotalAmount(@PathVariable("customerId") Long customerId,@RequestParam(value = "transactionType", defaultValue = "EXP") String transactionType){
		try {
			return memberCashValueService.getTotalAmount(customerId,transactionType);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getCurrentBalance Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/getDependentMembers/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDependentMembers(@PathVariable("customerID") Long customerID,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "memberName") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending){
		
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		
		return memberService.getDependentMembers(customerID,page);
		
	}
	
	@RequestMapping(value = "/transaction/topup", method  = RequestMethod.POST)
	@ResponseBody
	public ResponseResult topUpByCashCard(@RequestBody TopUpDto topUpDto){
		try {
			topUpDto.setStaffUserId(super.getUser().getUserId());
			ResponseResult responseResult = new ResponseResult();
			responseResult = memberCashValueService.topupByCashCard(topUpDto);
			if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode()) && PaymentMethod.CASH.name().equals(topUpDto.getPaymentMethod())) {
				TopUpDto dto = topUpDto;
				memberCashValueService.sentTopupEmail(dto);
			}
			return responseResult;
		} catch (Exception e) {
			logger.debug("CustomerMemberController Error Message:", e);
			responseResult.initResult(GTAError.MemberShipError.TOPUP_EXCEPTION);
			return responseResult;
		}
	}
	//TOPUP callback
	@RequestMapping(value = "/pospayment/callback", method = RequestMethod.POST)
	@ResponseBody
    public String callback(@RequestBody PosResponse posResponse) {
		logger.info("Got callback !!");
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug(gson.toJson(posResponse));
		if (posResponse.getReferenceNo() != null) {
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
				ResponseResult responseResult = memberCashValueService.handlePosPaymentResult(posResponse);
				if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())) {
					CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
					CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
					TopUpDto topUpDto = new TopUpDto();
					topUpDto.setCustomerId(customerOrderHd.getCustomerId());
					topUpDto.setTransactionNo(transactionNo);
					topUpDto.setPaymentMethod(CommUtil.getCardType(posResponse.getCardType()));
					memberCashValueService.sentTopupEmail(topUpDto);
				}
			}
			else
			{
				CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
				customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
				customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
				customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
				customerOrderTrans.setInternalRemark(posResponse.getResponseText());
				customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));
				customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
			}
		}
        return responseResult.getReturnCode();
    }
	
	@RequestMapping(value = "/transaction/topupByCard", method  = RequestMethod.POST)
	@ResponseBody
	public ResponseResult topUpByCard(@RequestBody TopUpDto topUpDto){
		try {
			topUpDto.setStaffUserId(super.getUser().getUserId());
			responseResult = memberCashValueService.topupByCard(topUpDto);
			if ("0".equals(responseResult.getReturnCode())) {
				CustomerOrderTrans customerOrderTrans = (CustomerOrderTrans)responseResult.getDto();
				String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
				Map<String, String> redirectUrlMap = new HashMap<String, String>();
				redirectUrlMap.put("redirectUrl", redirectUrl);
				redirectUrlMap.put("transactionNo", customerOrderTrans.getTransactionNo().toString());
				responseResult.setData(redirectUrlMap);
			}
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.topUpByCard Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value= "/transaction/getTransactionList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionList(@RequestParam("customerID") Long customerID,
			@RequestParam(value = "pageNumber", defaultValue ="1") String pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") String pageSize,
			@RequestParam(value = "sortBy", defaultValue = "c.transactionTimestamp") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "clientType", required = false) String clientType,
			@RequestParam(value = "filters", required = false) String filters){
		try {
			AdvanceQueryDto query = null;
			if (!StringUtils.isEmpty(filters)) {
				query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			}
			
			return memberCashValueService.getTransactionList(customerID, pageNumber, pageSize, sortBy, isAscending, clientType,query);
		} catch (GTACommonException gta) {
			gta.printStackTrace();
			logger.debug("CustomerMemberController.getTransactionList", gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getTransactionList", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value= "/transaction/getTransactionList/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult transactionListAdvancedSearch(){
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.memberCashValueService.transactionListAdvanceSearch());
		return this.responseResult;
	}
	
	@RequestMapping(value= "/transaction/getTopupHistory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTopupHistory(@RequestParam("customerID") Long customerID,
			@RequestParam("pageNumber") String pageNumber,
			@RequestParam("pageSize") String pageSize,
			@RequestParam(value = "sortBy", defaultValue = "c.transactionTimestamp") String sortBy,
			@RequestParam("isAscending") String isAscending,
			@RequestParam(value = "filters", required = false) String filters) throws Exception{
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}
		ResponseResult responseResult = memberCashValueService.getTopupHistory(customerID,pageNumber,pageSize,sortBy,isAscending,query);
		return responseResult;
	}
	
	@RequestMapping(value= "/transaction/getTopupHistory/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult topupHistoryAdvancedSearch(){
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.memberCashValueService.topupHistoryAdvanceSearch());
		return this.responseResult;
	}

	@RequestMapping(value = "/getCustomerName/{userId}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getCustomerIDByUserId(@PathVariable(value = "userId") String userId){
		try {
			return customerProfileService.getCustomerIdByUserId(userId);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getCustomerIDByUserId Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	

	@RequestMapping(value = "/getDependentMemberProfile/{customerId}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getDependentMemberProfileByCustomerId(@PathVariable(value = "customerId") Long customerId){
		try{
			return customerProfileService.getDependentMmeberInforByCustomerId(customerId);
		}catch (Exception e){
			e.printStackTrace();
			logger.debug("CustomerMemberController.getDependentMemberProfileByCustomerId Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	
	@RequestMapping(value = "/updateDependentMemberRights", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseResult updateDependentMembersRights(@RequestBody DependentMemberRightsDto dependentMemberRightsDto){
		try{
			Long customerId = dependentMemberRightsDto.getCustomerId();
			String facilityRight = dependentMemberRightsDto.getFacilityRight();
			String eventRight = dependentMemberRightsDto.getEventRight();
			String trainingRight = dependentMemberRightsDto.getTrainingRight();
			String dayPass =dependentMemberRightsDto.getDayPass();
			BigDecimal tran = dependentMemberRightsDto.getTran();
			return customerProfileService.updateDependentMembersRight(customerId, facilityRight, trainingRight, eventRight, dayPass, tran);
		}catch (Exception e){
			e.printStackTrace();
			logger.debug("CustomerMemberController.updateDependentMembersRights Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value={"/dependentCreate/{superiorMemberId}","/{superiorMemberId}/dependentmember"},method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult createDepartmentMember(@PathVariable(value = "superiorMemberId") String superiorMemberId,
			HttpServletRequest request){
		logger.info("CustomerMemberController.createDepartmentMember invocation start ...");
		try {
			CustomerProfile cProfile = (CustomerProfile) parseJson(request, CustomerProfile.class);
			if(StringUtils.isEmpty(superiorMemberId)){
				//return new ResponseResult("1", "Member's superiorMemberId is null!");
				responseResult.initResult(GTAError.MemberShipError.SUPERIOR_OF_MEMBER_IS_NULL);
				return responseResult;
			}
			
			Member member = new Member(Long.parseLong(superiorMemberId));
			cProfile.setMember(member);
			responseResult = dependentMemberService.saveDependentMemberInfo(cProfile, super.getUser().getUserId(),getUser().getUserName());
			if ("0".equals(responseResult.getReturnCode())) {
				customerProfileService.moveProfileAndSignatureFile((CustomerProfile) responseResult.getDto());
			}
			return responseResult;
		}catch(GTACommonException ce){
			logger.debug("createDepartmentMember Exception",ce);
			ce.printStackTrace();
			responseResult.initResult(ce.getError());
			return responseResult;
		}catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.createDepartmentMember Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value={"/dependentMember/{customerId}","/dependentUpdate/{customerId}"},method=RequestMethod.PUT)
	@ResponseBody 
	public ResponseMsg updateDepartmentMember(@PathVariable(value = "customerId") String customerId,
			HttpServletRequest request){
		
		logger.info("CustomerMemberController.updateDepartmentMember invocation start ...");
		String updateBy = super.getUser().getUserId();
		try {
			CustomerProfile cProfile = (CustomerProfile) parseJson(request, CustomerProfile.class);
			if(StringUtils.isEmpty(customerId)){
				//return new ResponseMsg("1", "Member's customerId is null!");
				responseResult.initResult(GTAError.MemberShipError.CUSTOMERID_OF_MEMBER_IS_EMPTY);
				return responseResult;
			}
			cProfile.setCustomerId(customerId);
			return dependentMemberService.updateDependentMemberInfo(cProfile, updateBy,getUser().getUserName());
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.updateDepartmentMember Exception : ",e);
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
	
	@RequestMapping(value="/updateprofilephoto",method=RequestMethod.POST)
	public @ResponseBody ResponseMsg updateprofilephoto(HttpServletRequest request, HttpServletResponse response){
		logger.info("CustomerMemberController.updateprofilephoto invocation start ...");
		try {
			CustomerProfile cProfile = (CustomerProfile)super.parseJson(request, CustomerProfile.class);
			cProfile.setUpdateBy(super.getUser().getUserId());
			return dependentMemberService.updateProfilePhoto(cProfile);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.updateprofilephoto Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/dependentMember/{academyNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDependentMemberInfo(@PathVariable String academyNo){
		logger.error("CustomerMemberController.getDepartmentMemberInfo start!");
		try {
			ResponseResult responseResult = dependentMemberService.checkDependentMember(academyNo);
			if(responseResult.getDto()==null){
				return responseResult;
			}
			MemberDto memberDto = (MemberDto) responseResult.getDto();
			if(memberDto.isDependentOrPrimary()){
				return dependentMemberService.getDependentMemberByAcademyNo(academyNo);
			}else{
				return responseResult;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getDependentMemberInfo Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/dependentMemberByCustomerId/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult dependentMemberByCustomerId(@PathVariable String customerId){
		logger.error("CustomerMemberController.dependentMemberByCustomerId start!");
		try {
			return dependentMemberService.getDepartMemberByCustomerId(customerId);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Search dependentMember failed!");
			responseResult.initResult(GTAError.MemberShipError.SEARCH_DEPENDENTMEMBER_FAILED);
			return responseResult; 
		}
	}
	
	/**
	 *This function is used to save the profile photo for each enrollment record.
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/saveprofilephoto", method = RequestMethod.POST)
	public @ResponseBody ResponseResult saveProfile(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		//ResponseResult responseResult = new ResponseResult();
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			//responseResult.setReturnCode("1");
			//responseResult.setErrorMessageEN("{\"warning\":\"File is empty!\"}");
			responseResult.initResult(GTAError.MemberShipError.FIlE_IS_EMPTY, "{\"warning\":\"File is empty!\"}");
			return responseResult;
		}
		String saveFilePath = "";
		try {
			saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.USER);
		} catch (Exception e) {
			//responseResult.setReturnCode("1");
			//responseResult.setErrorMessageEN("fail");
			responseResult.initResult(GTAError.MemberShipError.FIlE_UPLOAD_FAIL);
			return responseResult;
		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		//responseResult.setReturnCode("0");
		responseResult.initResult(GTAError.Success.SUCCESS);
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/"+saveFilePath);
		responseResult.setData(imgPathMap);
		return responseResult;
	}
	
	@RequestMapping(value = "/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getPrimaryMemberName(@PathVariable(value="customerId") Long customerId){
		logger.info("CustomerMemberController.getPrimaryMemberName invocation start ...");
		try {
			Member m = memberService.getMemberById(customerId);
			if(null == m){
				//return new ResponseResult("1", "can't find the member with member Id:"+customerId);
				responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_FOUND, new Object[]{customerId});
				return responseResult;
			}
			if("IPM".equals(m.getMemberType()) || "CPM".equals(m.getMemberType())){
				CustomerProfile cp = customerProfileService.getByCustomerID(customerId);
				MemberDto memberDto = new MemberDto();
				memberDto.setCustomerName(cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname());
				responseResult.initResult(GTAError.Success.SUCCESS,memberDto);
				return responseResult;
			}else{
				//return new ResponseResult("1", "Member type is not primaryMember!");
				responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_PRIMARY);
				return responseResult;		
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getPrimaryMemberName Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	
	@RequestMapping(value = "/recognitionHistory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRecognitionHistory(@RequestParam(value="pageNumber",defaultValue="1") Integer pageNumber,
			@RequestParam(value="pageSize",defaultValue="20") Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="createDate") String sortBy,
			@RequestParam(value="isAscending",defaultValue="false") String isAscending,
			HttpServletRequest request, HttpServletResponse response) {	
		String joinHQL="select  p.card_purpose cardType,m.customer_id id, CONCAT(c.salutation,' ',c.given_Name,' ',c.surname) name "
				+ "from PermitCardMaster p ,Member m ,CustomerProfile c "
			   +"	where  p.mappingCustomerId=m.customerId and c.customerId=m.customerId  ";
		try {
			String jsonStr = (String) request.getParameter("filters");
			if (jsonStr != null) {
				joinHQL +=" and ";
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(jsonStr, AdvanceQueryDto.class);
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				return advanceQueryService.getAdvanceQueryResultByHQL(queryDto, joinHQL, page);	
			}else{
				//no advance condition search 
				AdvanceQueryDto queryDto =new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				return advanceQueryService.getInitialQueryResultByHQL(queryDto, joinHQL, page);	
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getRecognitionHistory Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/getMyAccountRights/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberAccountRightsByCustomerId(@PathVariable(value = "customerId") Long customerId){
		try{
			return customerProfileService.getMmeberRightsByCustomerId(customerId);
		}catch (Exception e){
			e.printStackTrace();
			logger.debug("CustomerMemberController.getMemberAccountRightsByCustomerId Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/checkMemberPurchaseLimit", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkMemberLimit(@RequestParam(value = "customerId") Long customerId,@RequestParam(value="spending") BigDecimal spending){

		try{
			
			return memberService.checkTransactionLimit(customerId, spending);
			
		}catch (Exception e){
			logger.debug("MemberController check Member Purchase Limit Error:", e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateMemberStatus(@RequestParam(value="status") String status, @RequestParam(value ="customerId") Long customerId){
		try {
			if (status.equalsIgnoreCase("ACT")|| status.equalsIgnoreCase("NACT")) {
				return corporateService.changeMemberStatus(customerId, super.getUser().getUserId(), status,getUser().getFullname()==null?getUser().getUserName():getUser().getFullname());
			} else {
				//return new ResponseResult("1", "","Please provide a valid status!");
				responseResult.initResult(GTAError.MemberShipError.INVALID_STATUS);
				return responseResult;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.updateMemberStatus Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}


	@RequestMapping(value = "/{passportType}/{passportNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCustomerProfile(@PathVariable("passportType") String passportType,
											 @PathVariable("passportNo") String passportNo) {
		try {
			CustomerProfile customerProfile = customerProfileService.getCustomerProfileByPassportTypeAndPassportNo(passportType, passportNo);
			responseResult.initResult(GTAError.Success.SUCCESS, customerProfile);
			return responseResult;
		} catch (RuntimeException e) {
			responseResult.initResult(GTAError.LeadError.NOT_FOUND_CUSTOMER_PROFILE_BY_PASSPORT);
			return responseResult;
		}
	}
	
	@RequestMapping(value="/verifyMemberInfo/{surname}/{givenName}/{passportType}/{passportNo}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult verifyMemberInfo(
			@PathVariable(value="surname") String surname,
			@PathVariable(value="givenName") String givenName,
			@PathVariable(value="passportType") String passportType,
			@PathVariable(value="passportNo") String passportNo) throws Exception 
	
	{

		return customerProfileService.verifyMemberInfo(surname, givenName, passportType,passportNo);
	}
	
	@RequestMapping(value="/validaterAnswer", method=RequestMethod.POST)
	public @ResponseBody ResponseResult validateBirthDateOrQuestionAnswer(
			@RequestBody QuestionAnswerOrBirthDateDto questionAnswerOrBirthDateDto) throws Exception 
	
	{
		return customerProfileService.validateBirthDateOrQuestionAnswer(questionAnswerOrBirthDateDto);
	}
	@RequestMapping(value="/updatePwd", method=RequestMethod.POST)
	public @ResponseBody ResponseResult updatePwd(
			@RequestBody StaffPasswordDto staffPasswordDto) throws Exception 
	
	{
		return customerProfileService.updateMemberPwd(staffPasswordDto);
	}
	
	@RequestMapping(value="/getUserActiveRequest/{userId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserActiveRequest(@PathVariable(value = "userId") String userId){
		return customerProfileService.getUserActiveRequest(userId);
	}
	@RequestMapping(value = "/checkmember/{passportType}/{passportNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkMemberExistByPassportTypeAndPassportNo(@PathVariable("passportType") String passportType,
	 @PathVariable("passportNo") String passportNo) throws Exception {
		 this.responseResult = customerProfileService.checkMemberExistByPassportTypeAndPassportNo(passportType, passportNo);
		 return this.responseResult;
	 }
	
	/**
	 * Used for Showing Booking Records on My Booking Records of Portal(PC)
	 * @param customerId
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value="/bookingRecords",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBookingRecords(@RequestParam Long customerId,
			   @RequestParam(value="sortBy", defaultValue = "bookingDateFull") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "false") String isAscending){
		
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			return memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId,"PC",null,null,null,null,null);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getBookingRecords Exception ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * Used to Get My Booking For Member APP in Calendar(APP My Booking), and Current Booking for Web Portal (WP)
	 * E.g. Will return all the booking for the selected month(year: 2015, month: 12) for Web Portal
	 *      The booking from today for Web Portal
	 * @param customerId
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param appType WP: Web Portal; AP: Member APP
	 * @param year Mandatory for Member APP. E.g. 2015
	 * @param month Mandatory for Member APP. E.g. 12
	 * @return
	 */
	@RequestMapping(value="/myBooking",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMyBookingForMemberAppAndWebPortalCurrentBooking(@RequestParam Long customerId, 
			@RequestParam(value = "sortBy", defaultValue = "beginDate") String sortBy, 
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, 
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending, 
			@RequestParam(value = "appType", required = false, defaultValue = "WP") String appType,
			@RequestParam(value = "year", required = false, defaultValue = "") String year, 
			@RequestParam(value = "month", required = false, defaultValue = "")String month) {

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			ResponseResult result = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId , appType, year, month, null,null,null);
			final String mapIsAscending = isAscending;
			Map<String, List<MemberFacilityTypeBookingDto>> map = manualSortingInMap(mapIsAscending);

			Data data = (Data) result.getListData();
			assembleMyBookingDataInMap(data,map);
			result.setData(data);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}
	
	/**
	 * Used to Get the My Booking History for Web Portal(WP)
	 * @author Liky_Pan
	 * @since 2015-12-10
	 * @param customerId The customer Id of the Member
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param appType "WP" for Web Portal
	 * @param startDate The start date of the booking, E.g. 2015-11-01
	 * @param endDate The end date of the booking, E.g. 2015-12-01
	 * @return
	 */
	@RequestMapping(value="/webPortalBookingHistory",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMyBookingForWebPortal(@RequestParam Long customerId, 
			@RequestParam(value = "sortBy", defaultValue = "beginDate") String sortBy, 
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, 
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending, 
			@RequestParam(value = "appType", required = false, defaultValue = "WP") String appType,
			@RequestParam(value = "startDate", required = true) String startDate, 
			@RequestParam(value = "endDate", required = true)String endDate) {

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			ResponseResult result = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId , appType, null, null, null,startDate,endDate);
			final String mapIsAscending = isAscending;
			Map<String, List<MemberFacilityTypeBookingDto>> map = manualSortingInMap(mapIsAscending);
			Data data = (Data) result.getListData();
			assembleMyBookingDataInMap(data,map);
			result.setData(data);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}
	
	/**
	 * Used to Get Booking Records for Member APP's Home Page and Recognition Tablet's Booking History(APP, ANDROID TABLET)
	 * @param customerId
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param appType "AP" for Member APP, Android Recognition Tablet
	 * @param year Mandatory Year. E.g. 2015
	 * @param month Mandatory Month. E.g. 12
	 * @param day Mandatory Day. E.g. 01
	 * @return
	 */
	@RequestMapping(value={"/appHomeBooking","/personalBookingHistory"},method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBookingRecordsForMemberAppHomePageAndTabletBookingHistory(@RequestParam Long customerId, 
			@RequestParam(value = "sortBy", defaultValue = "beginDate") String sortBy, 
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, 
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending, 
			@RequestParam(value = "appType", required = false, defaultValue = "AP") String appType,
			@RequestParam(value = "year", required = true) String year, 
			@RequestParam(value = "month", required = true)String month,
			@RequestParam(value = "day", required = true)String day) {

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			
			if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)&&!StringUtils.isEmpty(day)){
				ResponseResult result =  memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId, appType, year, month, day,null,null);
				Data data = (Data) result.getListData();
				for (Object item : data.getList()) {
					MemberFacilityTypeBookingDto dto = (MemberFacilityTypeBookingDto) item;
					setRemainingTimeForMyBooking(dto);
				}
				
				responseResult.initResult(GTAError.Success.SUCCESS,data);
				return responseResult;
			}else{
				Data data = new Data();
				data.setList(new ArrayList());
				responseResult.initResult(GTAError.Success.SUCCESS,data);
				return responseResult;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}
	
	/**
	 * Used for Get Customized Information for Member on Tablet Recognition
	 * 
	 * @author Liky_Pan
	 * @param customerId
	 *            Customer Id of Current Member
	 * @return ResponseResult
	 */
	@RequestMapping(value="/personalInfo",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPersonalInfoForTablet(@RequestParam Long customerId){
		Map<String, Object> map = customerProfileService.getPersonalInfoForTablet(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS,map);
		return responseResult;
	}
	
	/**
	 * Import Primary Member's Part Detail for Auto-filling the Dependent
	 * Member's Form During Creating
	 * 
	 * @author Liky_Pan
	 * @param customerId
	 *            Customer Id of Primary Member
	 * @return ResponseResult
	 */
	@RequestMapping(value="/importPrimaryDetail",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPrimaryDetailForDependentAutoFilling(@RequestParam Long customerId){
		responseResult.initResult(GTAError.Success.SUCCESS,customerProfileService.importPrimaryDeailForAutofillingDependentForm(customerId));
		return responseResult;
	}
	
	private Map<String, List<MemberFacilityTypeBookingDto>> manualSortingInMap(final String mapIsAscending){
		return new TreeMap<String, List<MemberFacilityTypeBookingDto>>(new Comparator<String>() {
			public int compare(String obj1, String obj2) {
				if (mapIsAscending.equals("false")) {
					return obj2.compareTo(obj1);
				} else {
					return obj1.compareTo(obj2);
				}
			}
		});
	}
	
	private void assembleMyBookingDataInMap(Data data,Map<String, List<MemberFacilityTypeBookingDto>> map){
		for (Object item : data.getList()) {
			MemberFacilityTypeBookingDto dto = (MemberFacilityTypeBookingDto) item;
			setRemainingTimeForMyBooking(dto);
			String date = DateConvertUtil.getYMDFormatDate(dto.getBeginDate());
			if (!map.containsKey(date)) {
				List<MemberFacilityTypeBookingDto> list = new ArrayList<MemberFacilityTypeBookingDto>();
				list.add(dto);
				map.put(date, list);
			} else {
				map.get(date).add(dto);
			}
		}
		List<Map<String, List<MemberFacilityTypeBookingDto>>> alist = new ArrayList<Map<String, List<MemberFacilityTypeBookingDto>>>();
		alist.add(map);
		data.setList(alist);
	}
	
	private void setRemainingTimeForMyBooking(MemberFacilityTypeBookingDto dto){
		if (dto.getBeginDate() == null) {
			dto.setBeginDate(new Date());
		}
		if ("RESTAURANT".equals(dto.getFacilityType())) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dto.getBeginDate());
			cal.add(Calendar.HOUR, 2);
			dto.setEndDate(cal.getTime());
		}
		dto.setRemainingTime(DateConvertUtil.calcRemainingTime(dto.getBeginDate(),dto.getEndDate()));
	}
}

