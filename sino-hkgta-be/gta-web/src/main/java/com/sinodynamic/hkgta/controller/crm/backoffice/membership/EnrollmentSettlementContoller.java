package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.RemarksService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class EnrollmentSettlementContoller extends ControllerBase{
	
	public Logger logger = Logger.getLogger(EnrollmentSettlementContoller.class);
	
	@Autowired
	CustomerEnrollmentService customerEnrollmentService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private RemarksService remarksService;
	
	@Autowired
	private UserMasterService userMasterService;
	
	@RequestMapping(value = "/membership_mgr/assign_saleperson", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg assignSaleperson(@RequestParam("enrollment_id") Long enrollmentId,
										@RequestParam("saleperson") String salePerson) {
		try{
			String userId = getUser().getUserId();
			customerEnrollmentService.assignSaleperson(enrollmentId, salePerson, userId);
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		}catch (Exception e){
			logger.debug("EnrollmentSettlementContoller.assignSaleperson Exception",e);
			e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
	
	@RequestMapping(value = "/membership_mgr/change_enrollment_status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg changeEnrollmentStatus(@RequestParam("enrollment_id") Long enrollmentId,
											  @RequestParam("status") String status){
		try{
			customerEnrollmentService.updateEnrollmentStatus(enrollmentId, status,getUser().getUserId(),getUser().getUserName());
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		}catch (Exception e){
			logger.debug("EnrollmentSettlementContoller.changeEnrollmentStatus Exception",e);
			e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
	@RequestMapping(value = "/membership_mgr/change_renewal_status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg changeRenewalStatus(@RequestParam("orderNo") Long orderNo,
			@RequestParam("status") String status){
		try{
			return customerEnrollmentService.updateRenewalStatus(orderNo, status,getUser().getUserId(),getUser().getUserName());
		}catch (Exception e){
			logger.debug("EnrollmentSettlementContoller.changeRenewalStatus Exception",e);
			e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}

	@RequestMapping(value = "/enrollment/view_enrollment_records", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult viewEnrollmentRecords(
			@RequestBody  CustomerEnrollmentDto customerEnrollmentDto,
			@RequestParam(value = "device",defaultValue = "PC") String device) {
		LoginUser currentUser = getUser();
		ResponseResult result = this.customerEnrollmentService.viewEnrollmentRecords(customerEnrollmentDto,currentUser.getUserId(),device);
		
		return result;
	}
	
	@RequestMapping(value = "/enrollment/advancesearch", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult enrollmentQueryData(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value="orderBy",defaultValue="enrollCreateDate",required=false) String orderBy,
			@RequestParam(value="status",defaultValue="", required = false) String status,
			@RequestParam(value="isAscending",defaultValue="false",required=false) String isAscending,
			@RequestParam(value="offerCode",defaultValue="RENEW",required=false) String offerCode,
			@RequestParam(value="filters",defaultValue="",required=false) String filters,
			@RequestParam(value="filterByCustomerId",defaultValue="ALL") String filterByCustomerId,
			@RequestParam(value="device",defaultValue="PC",required=false) String device,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String joinHQL = this.customerEnrollmentService.getEnrollmentRecordsSQL(offerCode);
		// advance search
		if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)){
			joinHQL += " and t.offerCode =  '"+Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL.toString()+"'";
		}else if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)){
			joinHQL += " and t.offerCode is null  ";
		}
		if(!"All".equalsIgnoreCase(status) && !StringUtils.isEmpty(status)){
			joinHQL += " and  t.status =  '"+status+"'  ";
		}
		if(!"ALL".equals(filterByCustomerId)) {
			joinHQL += " and t.customerId2 = '" + filterByCustomerId + "' ";
		}
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinHQL +=" and ";
			if (!orderBy.endsWith("enrollCreateDate")){
				orderBy = orderBy + ",enrollCreateDate";
				isAscending = isAscending + ",false";
			}
			prepareQueryParameter(pageNumber, pageSize, orderBy, isAscending, advanceQueryDto);
			return dealWithEnrollmentAdvanceResult(advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinHQL, page,CustomerEnrollmentDto.class),device);
		}
		// no advance condition search
		AdvanceQueryDto queryDto = new AdvanceQueryDto();
		if (!orderBy.endsWith("enrollCreateDate")){
			orderBy = orderBy + ",enrollCreateDate";
			isAscending = isAscending + ",false";
		}
		prepareQueryParameter(pageNumber, pageSize, orderBy, isAscending, queryDto);
		return dealWithEnrollmentAdvanceResult(advanceQueryService.getInitialQueryResultBySQL(queryDto, joinHQL, page,CustomerEnrollmentDto.class), device);
	}
	private ResponseResult dealWithEnrollmentAdvanceResult(
			ResponseResult responseResult,String device) {
		LoginUser currentUser = getUser();
		Data data = (Data) responseResult.getData();
		List<CustomerEnrollmentDto> list = (List<CustomerEnrollmentDto>) data.getList();
		for (CustomerEnrollmentDto customerEnrollmentDto : list) {
			long customerId = customerEnrollmentDto.getCustomerId();
			customerEnrollmentDto.setEnrollDate(DateConvertUtil.getYMDDateAndDateDiff(customerEnrollmentDto
					.getEnrollDateDB()));
			//do not need to show remark on page
			ResponseResult temp =  this.remarksService.checkUnreadRemarkByDevice(customerId, currentUser.getUserId(), device);
			customerEnrollmentDto.setRemarkNo(temp.getData() + "");
			UserMaster userMaster = userMasterService.getUserMasterByCustomerId(customerId);
			Boolean checkActivationEmailEnabledStatus = EnrollStatus.ANC.name().equals(customerEnrollmentDto.getStatus())||EnrollStatus.CMP.name().equals(customerEnrollmentDto.getStatus());
			if(checkActivationEmailEnabledStatus&&userMaster!=null&&userMaster.getPasswdChangeDate()==null){
				customerEnrollmentDto.setIsActivationEmailEnabled(true);
			}else{
				customerEnrollmentDto.setIsActivationEmailEnabled(false);
			}
		}
		return responseResult;
	}
}
