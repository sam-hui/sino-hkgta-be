package com.sinodynamic.hkgta.controller.pos;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantMenuCatDto;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.pos.RestaurantMenuCatService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class MenuCategoryController extends ControllerBase {
	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private RestaurantMenuCatService restaurantMenuCatService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/restaurant/menuCategory/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult listMenuCategory(
			@RequestParam(value = "restaurantId",	defaultValue = "") String restaurantId,
			@RequestParam(value = "sortBy",	defaultValue = "") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters
			) {
		try
		{
			AdvanceQueryDto advanceQueryDto = null;
			if (filters != null && filters.length()>0) {
				advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			}
			String joinHQL = restaurantMenuCatService.getRestaurantListByRestaurantId();
			if (sortBy != null && isAscending != null && sortBy.length() > 0 && isAscending.length() > 0) {
				sortBy += ", cat_id";
				isAscending += ", false";
			}
			else
			{
				sortBy = "cat_id";
				isAscending = "false";
			}
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0)
			{
				joinHQL += " and ";
				AdvanceQueryDto queryDto = advanceQueryDto;
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinHQL, page, RestaurantMenuCatDto.class, Arrays.asList(new Object[] {restaurantId}));
			}
			else
			{
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryResultBySQL(queryDto, joinHQL, page, RestaurantMenuCatDto.class, Arrays.asList(new Object[] {restaurantId}));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	@RequestMapping(value = "/restaurant/{restaurantId}/menucategory/{catId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantMenuCat(
			@PathVariable(value = "restaurantId") String restaurantId,
			@PathVariable(value = "catId") String catId
			) {
		try {
			responseResult.initResult(GTAError.Success.SUCCESS,restaurantMenuCatService.getRestaurantMenuCat(restaurantId, catId));
			return responseResult;
		}catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Some error happened.", exception);
			responseResult.initResult(GTAError.CommomError.UNHANDLED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/restaurant/{restaurantId}/menucategory/{catId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateRestaurantMenuCat(
			@PathVariable(value = "restaurantId") String restaurantId,
			@PathVariable(value = "catId") String catId,
			@RequestBody RestaurantMenuCatDto restaurantMenuCatDto
			) {
		try {
			restaurantMenuCatService.updateRestaurantMenuCat(restaurantId, catId, restaurantMenuCatDto.getCategory_name(), this.getUser().getUserName());
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Some error happened.", exception);
			responseResult.initResult(GTAError.CommomError.UNHANDLED_EXCEPTION);
			return responseResult;
		}
	}
}
