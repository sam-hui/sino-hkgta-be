package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberTransactionService;
import com.sinodynamic.hkgta.util.JsonUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class RecentServedMembersController extends ControllerBase<Member> {

	private static final String SERVED_TIME_SORT_BY = "servedTime";

	private static Logger logger = Logger.getLogger(CustomerMemberController.class);

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private MemberTransactionService memberTransactionService;

	@ResponseBody
	@RequestMapping(value = "/member/recent", method = RequestMethod.GET)
	public ResponseResult getMemberList(
			@RequestParam(value = "sortBy", defaultValue = SERVED_TIME_SORT_BY) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "customerId") Long[] customerId,
			@RequestParam(value = SERVED_TIME_SORT_BY) String[] servedTime,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) {

		try {
			if (customerId == null || customerId.length <= 0) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
			Map<Long, String> customerIdMap = new HashMap<>();

			if (customerId != null) {

				for (int i = 0; i < customerId.length; i++) {
					customerIdMap.put(customerId[i], servedTime[i]);
				}

			}

			ListPage<Member> page = new ListPage<Member>();
			if (pageNumber > 0) {
				page.setNumber(pageNumber);
			}
			if (pageSize > 0) {
				page.setSize(pageSize);
			}
			if (!StringUtils.isEmpty(filters)) {

				AdvanceQueryDto advanceQueryDto = JsonUtil.fromJson(filters, AdvanceQueryDto.class);

				if (advanceQueryDto == null) {
					advanceQueryDto = new AdvanceQueryDto();
				}

				if (!SERVED_TIME_SORT_BY.equals(sortBy)) {
					if (sortBy != null && sortBy.length() > 0) {
						advanceQueryDto.setSortBy(sortBy);
					}
					advanceQueryDto.setAscending(isAscending);
				}

				if (advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
						&& advanceQueryDto.getGroupOp().length() > 0) {

					for (SearchRuleDto rule : advanceQueryDto.getRules()) {
						if ("t.status".equals(rule.field) && "ACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_ACT);
						} else if ("t.status".equals(rule.field) && "INACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_NACT);
						} else if ("t.status".equals(rule.field) && "EXPIRED".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_EXP);
						}
					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}

			Data result = memberTransactionService.getRecentServedMemberList(page,
					SERVED_TIME_SORT_BY.equals(sortBy) ? null : sortBy, isAscending, customerId);

			sortServedTime(customerIdMap, result, sortBy, isAscending);

			responseResult.initResult(GTAError.Success.SUCCESS, result);
		} catch (Exception e) {
			logger.error("advance query failed!", e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}

		return responseResult;
	}

	private void sortServedTime(Map<Long, String> customerIdMap, Data result, String sortBy, final String isAscending) {
		@SuppressWarnings("unchecked")
		List<DependentMemberDto> list = (List<DependentMemberDto>) result.getList();

		for (DependentMemberDto memberDto : list) {

			Long customerId = memberDto.getCustomerId();
			String servedTime = customerIdMap.get(customerId);
			memberDto.setServedTime(servedTime);
		}

		if (SERVED_TIME_SORT_BY.equals(sortBy)) {

			final DateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
			Collections.sort(list, new Comparator<DependentMemberDto>() {
				public int compare(DependentMemberDto o1, DependentMemberDto o2) {

					try {
						Date date1 = sdf.parse(o1.getServedTime());
						Date date2 = sdf.parse(o2.getServedTime());

						int compare = date1.compareTo(date2);

						if ("true".equals(isAscending)) {
							return compare;
						} else {
							return -compare;
						}

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
					return 0;
				}
			});
		}

	}

}
