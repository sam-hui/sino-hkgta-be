package com.sinodynamic.hkgta.controller.admin;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.sys.AssignProgramDto;
import com.sinodynamic.hkgta.dto.sys.AssignRoleDto;
import com.sinodynamic.hkgta.dto.sys.RoleDto;
import com.sinodynamic.hkgta.dto.sys.RoleMasterDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramDto;
import com.sinodynamic.hkgta.dto.sys.SysUserDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.security.service.RoleProgramService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.sys.ProgramMasterService;
import com.sinodynamic.hkgta.service.sys.RoleMasterService;
import com.sinodynamic.hkgta.service.sys.SysUserService;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class SystemController extends ControllerBase {
	@Autowired
	protected ResponseResult responseResult;
	@Autowired
	private RoleMasterService roleMasterService;
	@Autowired
	private ProgramMasterService programMasterService;
	@Autowired
	private RoleProgramService roleProgramService;
	@Autowired
	public UserRoleService userRoleService;
	
	@Autowired
	public AdvanceQueryService advanceQueryService;
	
	@Autowired
	SysUserService sysUserService;
	
	@Autowired
	private UserMasterService userService;
	
	@RequestMapping(value = "/sys/role/list", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="", required = false) String sortBy,
			@RequestParam(value="isAscending",defaultValue="true", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		
		LoginUser user = getUser();
		
		String joinSQL = roleMasterService.getRoleList();
		if (StringUtils.isNotEmpty(filters))
		{
			joinSQL = joinSQL + " AND ";
			AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);

			return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinSQL, page, RoleMasterDto.class);
		}
		// no advance condition search
		AdvanceQueryDto queryDto = new AdvanceQueryDto();
		prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
		return advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, RoleMasterDto.class);
	}
	
	@RequestMapping(value = "/sys/role", method = RequestMethod.POST)
	public @ResponseBody ResponseResult createRole(@RequestBody RoleMaster role) {
		return roleMasterService.createRole(role);
	}
	@RequestMapping(value = "/sys/role/status", method = RequestMethod.POST)
	public @ResponseBody ResponseResult changeStatus(@RequestBody RoleDto dto) {
		return roleMasterService.changeStatus(dto);
	}
	@RequestMapping(value = "/sys/role", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateRole(@RequestBody RoleMaster role) {
		try {
			if (roleMasterService.validateRole(role.getRoleId(), role.getRoleName())){
				responseResult.initResult(GTAError.SysError.ROLE_NAME_ALREADY_EXISTS);
				return responseResult;
			}
			return roleMasterService.updateRole(role);
		} catch (Exception e) {
			logger.error(SystemController.class.getName() + " updateRole Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_UPDATE_ROLE);
			return responseResult;
		}
	}
	@RequestMapping(value = "/sys/role/{roleId}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseResult deleteRole(@PathVariable(value = "roleId")BigInteger roleId ) {
		return roleMasterService.deleteRole(roleId);
	}
	@RequestMapping(value = "/sys/available/programs/{roleId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAvailablePrograms(@PathVariable(value="roleId") BigInteger roleId) {
		List<ProgramMaster> programMasterList = programMasterService.getAvailablePrograms(roleId);
		responseResult.initResult(GTAError.Success.SUCCESS, programMasterList);
		return responseResult;
	}
	@RequestMapping(value = "/sys/role_program/list/{roleId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getRoleProgramsByRoleId(@PathVariable(value="roleId") BigInteger roleId, @RequestParam(value="userType", required=false, defaultValue="STAFF") String userType) {
		return roleProgramService.getRoleProgramsByRoleId(roleId, userType);
	}
	
	@RequestMapping(value = "/sys/role_program/add", method = RequestMethod.POST)
	public @ResponseBody ResponseResult roleAddProg(@RequestBody RoleProgramDto dto) {
		return roleProgramService.roleAddProg(dto);
	}
	
	
	@RequestMapping(value = "/sys/role_program/del", method = RequestMethod.POST)
	public @ResponseBody ResponseResult roleDelProg(@RequestBody RoleProgramDto dto) {
		return roleProgramService.roleDelProg(dto);
	}
	
	
	@RequestMapping(value = "/sys/role_program/{roleId}/{programId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoleProg(@PathVariable(value="roleId")BigInteger roleId,@PathVariable(value="programId")String programId) {
		return roleProgramService.getRoleProg(roleId,programId);
	}
	
	@RequestMapping(value = "/sys/role_program/edit", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult roleEditProg(@RequestBody RoleProgramDto dto) {
		return roleProgramService.roleEditProg(dto);
	}
	
	
	@RequestMapping(value = "/sys/available/roles/{userType}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getRoleListByUserType(@PathVariable(value="userType") String userType) {
		List<RoleMaster> roleList = roleProgramService.getRoleListByUserType(userType);
		responseResult.initResult(GTAError.Success.SUCCESS, roleList);
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/user/roles/{userId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserRoleListByUserId(@PathVariable(value="userId") String userId) {
		List<UserRoleDto> roleList = userRoleService.getUserRoleListByUserId(userId);
		responseResult.initResult(GTAError.Success.SUCCESS, roleList);
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/assign/roles/", method = RequestMethod.POST)
	public @ResponseBody ResponseResult assignRole(@RequestBody AssignRoleDto dto) {
		return userRoleService.assignRole(dto,getUser().getUserId());
	}
	
	@RequestMapping(value = "/sys/remove/roles/", method = RequestMethod.POST)
	public @ResponseBody ResponseResult removeRole(@RequestBody AssignRoleDto dto) {
		return userRoleService.removeRole(dto);
	}
	
	@RequestMapping(value = "/sys/user/permissions/", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserPermissions() {
		return userRoleService.loadUserPermissionsV2(getUser().getUserId());
	}
	
	
	@RequestMapping(value = "/sys/assign/program", method = RequestMethod.POST)
	public @ResponseBody ResponseResult assignProgram(@RequestBody AssignProgramDto dto) {
		if (StringUtils.isEmpty(dto.getRoleName())) {
			responseResult.initResult(GTAError.SysError.ROLE_NAME_EMPTY);
			return responseResult;
		}
		if (roleMasterService.validateRole(dto.getRoleId(), dto.getRoleName())){
			responseResult.initResult(GTAError.SysError.ROLE_NAME_ALREADY_EXISTS);
			return responseResult;
		}
		responseResult = roleProgramService.assignProgram(dto);
		roleProgramService.refreshRole();
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/temp_user/new", method = RequestMethod.POST)
	public @ResponseBody ResponseResult createTempUser(@RequestBody SysUserDto user) throws Exception {
		user.setCreateBy(this.getUser().getUserId());
		user.setUpdateBy(this.getUser().getUserId());
		sysUserService.addSysUser(user);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/user/list", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserList(@RequestParam("pageNumber") Integer pageNumber,
													@RequestParam("pageSize") Integer pageSize,
													@RequestParam("sortBy") String sortBy,
													@RequestParam(value="filters", required=false) String filters,
													@RequestParam("isAscending") Boolean isAscending) throws Exception {
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}
		
		Data data = sysUserService.loadAllUsers(sortBy,
				(isAscending ? "asc" : "desc"), pageSize,
				pageNumber, query);
		
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/user/list/advanceSearch", method = RequestMethod.GET)
	public @ResponseBody ResponseResult userListAdvancedSearch() throws Exception {
		
		responseResult.initResult(GTAError.Success.SUCCESS,sysUserService.userListAdvanceSearch());
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/user/status", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateUserStatus(@RequestParam("userId") String userId,
			@RequestParam("status") String status) throws Exception {
		
		UserMaster user = userService.getUserByUserId(userId);
		user.setStatus(status);
		
		userService.updateUserMaster(user);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	
	@RequestMapping(value = "/sys/user", method = RequestMethod.GET)
	public @ResponseBody ResponseResult viewSysUser(@RequestParam("userId") String userId
			) throws Exception {
		
		responseResult.initResult(GTAError.Success.SUCCESS,sysUserService.viewSysUser(userId));
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/user", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult editSysUser(
			@RequestBody SysUserDto user) throws Exception {
		user.setUpdateBy(getUser().getUserId());
		
		sysUserService.editSysUser(user);
		sysUserService.refreshUserRole(user.getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
}