package com.sinodynamic.hkgta.controller.pms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pms.StaffRosterDto;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.pms.StaffRosterService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
public class StaffRosterController extends ControllerBase<StaffRoster> {

	@Autowired
	private StaffRosterService staffRosterService;

	@RequestMapping(value = "/staffRoster/month/{month}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffRoster(@PathVariable(value = "month") String month) {
		try {
			String staffUserId = getUser().getUserId();
			StaffRosterDto staffRosterDto = staffRosterService.getStaffRosterByMonth(staffUserId, month);
			responseResult.initResult(GTAError.Success.SUCCESS, staffRosterDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/staffRoster/weekly", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffRoster(String staffUserId, String beginDate) {
		try {
			StaffRosterDto staffRosterDto = staffRosterService.getStaffRoster(staffUserId, beginDate);
			responseResult.initResult(GTAError.Success.SUCCESS, staffRosterDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/staffRoster", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(@RequestBody StaffRosterDto staffRosterDto) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			staffRosterService.save(staffRosterDto, userId);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;

	}
}
