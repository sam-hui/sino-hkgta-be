package com.sinodynamic.hkgta.controller.crm.pub;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.pub.NoticeDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.pub.NoticeService;
import com.sinodynamic.hkgta.util.JsonUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/notice")
public class NoticeController extends ControllerBase<NoticeDto> {

	@Autowired
	private NoticeService noticeService;

	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(@RequestBody NoticeDto noticeDto) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			noticeService.save(noticeDto, userId);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;

	}
	
	@RequestMapping(value = "/{noticeId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable(value = "noticeId") Long noticeId, @RequestBody NoticeDto noticeDto) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			noticeDto.setNoticeId(noticeId);
			noticeService.update(noticeDto, userId);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;

	}

	@RequestMapping(value = "/status/{noticeId}/{status}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateStatus(@PathVariable(value = "noticeId") Long noticeId, @PathVariable String status) {

		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			noticeService.updateStatus(noticeId, status, userId);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;

	}
	
	@RequestMapping(value = "/{noticeId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getNotice(@PathVariable(value = "noticeId") Long noticeId) {
		try {
			
			NoticeDto noticeDto = noticeService.getNotice(noticeId);
			responseResult.initResult(GTAError.Success.SUCCESS, noticeDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/list/{catId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult listMenu(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@PathVariable(value = "catId") String catId,
			@RequestParam(value = "sortBy", defaultValue = "n.noticeBegin") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters) {

		StringBuilder hql = new StringBuilder();

		hql.append("select n.noticeId as noticeId, n.subject as subject, n.content as content, n.noticeType as noticeType, s.codeDisplay as categoryName, n.noticeBegin as noticeBegin, n.status as status,");
		hql.append("n.createBy as createBy, n.createDate as createDate, u.nickname as creator ");
		hql.append(" from Notice n, SysCode s, UserMaster u");
		hql.append(" where n.createBy = u.userId");
		hql.append(" and s.codeValue = n.noticeType and s.category = 'noticeType' ");

		if (StringUtils.isNotBlank(catId) && !"All".equalsIgnoreCase(catId)) {
			hql.append(" and s.codeValue = '").append(catId).append("'");
		}

		ListPage<RestaurantMenuItem> page = new ListPage<>();

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		AdvanceQueryDto advanceQueryDto = JsonUtil.fromJson(filters, AdvanceQueryDto.class);

		if (advanceQueryDto == null) {
			advanceQueryDto = new AdvanceQueryDto();
		}

		if (advanceQueryDto.getRules() != null && !advanceQueryDto.getRules().isEmpty()) {
			hql.append(" and ");
		}
		advanceQueryDto.setSortBy(sortBy);
		advanceQueryDto.setAscending(isAscending);
		ResponseResult responseResult = advanceQueryService.getAdvanceQueryResultByHQL(advanceQueryDto, hql.toString(),
				page, NoticeDto.class);

		
		return responseResult;
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult readNotice(
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "2") Integer pageSize) {
		try {

			Data data = noticeService.getNoticeList(pageNumber, pageSize);
			responseResult.initResult(GTAError.Success.SUCCESS, data);
		} catch (GTACommonException e) {
			logger.error(e.getMessage());
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
	
}
