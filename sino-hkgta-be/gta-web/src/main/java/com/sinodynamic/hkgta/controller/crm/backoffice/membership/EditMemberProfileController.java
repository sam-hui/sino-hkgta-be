package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.EditMemberProfileService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;



@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/membership")
public class EditMemberProfileController extends ControllerBase {

	private Logger logger = Logger.getLogger(EditMemberProfileController.class);
	
	@Autowired
	private EditMemberProfileService editMemberProfileService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg editEnrollment(HttpServletRequest request, HttpServletResponse response){
		try {
			LoginUser currentUser = getUser();
			CustomerProfile cProfile = (CustomerProfile) parseJson(request, CustomerProfile.class);
			logger.debug("editMemberProfile controller parameter: givenName ="+cProfile.getGivenName());
			cProfile.setUpdateBy(this.getUser().getUserId());
			ResponseMsg responseMsg =  editMemberProfileService.editMemberProfile(cProfile,currentUser.getUserId());
			
			/*
			 * Note: The Profile/Signature only be updated in the following
			 * method.
			 */
			if ("0".equals(responseMsg.getReturnCode())) {
				customerProfileService.moveProfileAndSignatureFile(cProfile);
			}
			return responseMsg;
		} catch(HibernateOptimisticLockingFailureException concurrent) {
			concurrent.printStackTrace();
			logger.debug("EditMemberProfileController.editEnrollment Exception", concurrent);
			responseMsg.initResult(GTAError.CommonError.BEEN_UPDATED_OR_DELETED, new String[]{"member profile"});
			return responseMsg;
		} catch (GTACommonException gta) {
			gta.printStackTrace();
			logger.debug("EditMemberProfileController.editEnrollment Exception", gta);
			responseMsg.initResult(gta.getError(),gta.getArgs());
			return responseMsg;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("EditMemberProfileController.editEnrollment Exception", e);
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
}
