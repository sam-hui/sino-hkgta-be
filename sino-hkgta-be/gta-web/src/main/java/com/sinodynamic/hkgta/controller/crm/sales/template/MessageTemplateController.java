package com.sinodynamic.hkgta.controller.crm.sales.template;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.MessageTemplateDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GTAError.TemplateError;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Controller
@RequestMapping("/template")
public class MessageTemplateController extends ControllerBase<MessageTemplate> {
	
	private Logger logger = Logger.getLogger(MessageTemplateController.class);

	@Autowired
	private MessageTemplateService templateService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	
	@RequestMapping(value = "/{templateType}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getTemplate(@PathVariable(value="templateType") String templateType) {
		MessageTemplate template = null;
		ResponseResult result = new ResponseResult();
		try {
			template = templateService.getTemplateByFuncId( templateType);
			
			String location = SessionMap.getInstance().getLocation(getUser().getUserId());
			//If sales kit call this API, it need replace the {fromname}.
			if ("Mobile".equalsIgnoreCase(location))
			{
				String content = template.getContent().replace(Constant.PLACE_HOLDER_FROM_USER, getUser().getUserName());	
				template.setContent(content);
			}
			result.setData(template);
			result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result.setErrorMessageEN(ResponseMsgConstant.ERRORCODE);
			result.setReturnCode(ResponseMsgConstant.ERRORCODE);
			return result;
		}
	}
	
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody ResponseMsg addTemplate(@RequestBody MessageTemplateDto mtd) {
		
		logger.info("MessageTemplateController.addTemplate invocation start ...");
		ResponseMsg reponseMsg = null;
		
		LoginUser user = getUser();
		if (user == null) {
			logger.error("Failed to get current user");
			return new ResponseMsg("-2","Failed to get current user!");
		}
		mtd.setStaffId(user.getUserId());
		
		try {
			
			templateService.saveMessageTemplate(mtd, null);
			reponseMsg = new ResponseMsg("0", null);
			
		} catch (Exception e) {
			
			e.getStackTrace();
			reponseMsg = new ResponseMsg("-1","Error happened in data persist!");
		}
		
		logger.info("MessageTemplateController.addTemplate invocation end ...");
		return reponseMsg;
	}
	
	
	@RequestMapping(value = "/{templateId}", method = RequestMethod.PUT)
	public @ResponseBody ResponseMsg modifyTemplate(@PathVariable(value="templateId") String templateId, @RequestBody MessageTemplateDto mtd) {
		
		logger.info("MessageTemplateController.modifyTemplate invocation start ...");
		ResponseMsg reponseMsg = null;
		
		LoginUser user = getUser();
		if (user == null) {
			logger.error("Failed to get current user");
			return new ResponseMsg("-3","Failed to get current user!");
		}
		mtd.setStaffId(user.getUserId());
		
		try {
			
			boolean success = templateService.saveMessageTemplate(mtd, Long.parseLong(templateId));
			if (!success) {
				reponseMsg = new ResponseMsg("-2", "Not exist record to update!");
			} else {
				reponseMsg = new ResponseMsg("0", null);
			}
			
		} catch (Exception e) {
			
			e.getStackTrace();
			reponseMsg = new ResponseMsg("-1", "Error happened in data persist!");
		}
		
		logger.info("MessageTemplateController.modifyTemplate invocation end ...");
		return reponseMsg;
		
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllTemplate() {
		
		logger.info("MessageTemplateController.getAllTemplate invocation start ...");
		ResponseResult responseResult = null;
		
		try {
			
			List<MessageTemplate> result = templateService.getAllMessageTemplate();
			responseResult = new ResponseResult("0", null, result);
			
		} catch (Exception e) {
			e.printStackTrace();
			responseResult = new ResponseResult("-1", "", "Error happened when search data!");
		}
		
		logger.info("MessageTemplateController.getAllTemplate invocation end ...");
		return responseResult;
	}
	
	
	@RequestMapping(value = "/{templateId}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseMsg removeTemplate(@PathVariable(value="templateId") String templateId) {
		
		logger.info("MessageTemplateController.removeTemplate invocation start ...");
		ResponseMsg reponseMsg = null;
		
		try {
			
			templateService.deleteMessageTemplate(Long.parseLong(templateId));
			reponseMsg = new ResponseMsg("0", null);
			
		} catch (Exception e) {
			e.printStackTrace();
			reponseMsg = new ResponseMsg("-1", "Error happened when delete data!");
		}
		
		logger.info("MessageTemplateController.removeTemplate invocation end ...");
		return reponseMsg;
	}
	
	@RequestMapping(value = "/{templateType}/{isGeneral}/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSpecificMessageInfo(
			@PathVariable(value = "templateType") String templateType,
			@PathVariable(value = "isGeneral") Boolean isGeneral,
			@PathVariable(value = "customerId") Long customerId,
			@RequestParam(value = "isContentHtml",required =false,defaultValue = "true") Boolean isContentPlain) {
		
		logger.info("MessageTemplateController.getSpecificMessageInfo invocation start ...");
		
		if (CommUtil.nvl(templateType).length() == 0 || isGeneral == null || customerId == null) {
			logger.error("Parameter error!");
			return new ResponseResult(initErrorMsg(GTAError.TemplateError.MESSAGE_TEMPLATE_PARAMETER_ERROR));
		}
		
		ResponseResult result = new ResponseResult();
		MessageTemplate template = null;
		CustomerProfile cp = null;
		
		try {
			
			template = templateService.getTemplateByFuncId( templateType);
			cp = customerProfileService.getById(customerId);
			if (template == null) {
				logger.error("templateType is invalid!");
				return new ResponseResult(initErrorMsg(GTAError.TemplateError.MESSAGE_TEMPLATE_TYPE_ERROR));
			}
			if (cp == null) {
				logger.error("customerId is invalid!");
				return new ResponseResult(initErrorMsg(GTAError.TemplateError.MESSAGE_TEMPLATE_CUSTOMERID_ERROR));
			}
			
			String userName = getName(cp);
			String fromName = null;
			if (isGeneral) {
				fromName = Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER;
			} else {
				
				LoginUser user = getUser();
				if (user == null) {
					logger.error("Failed to get current user!");
					return new ResponseResult(initErrorMsg(GTAError.TemplateError.MESSAGE_TEMPLATE_NO_CURRENTUSER));
				}
				fromName = user.getUserName();
			}
			
			String content = null;
			if(isContentPlain){
				content = template.getFullContent(userName,fromName);
			}else{
				content = template.getFullContentHtml(userName,fromName);
			}
//			String content = template.getContent();
//			content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, userName).replace(Constant.PLACE_HOLDER_FROM_USER, fromName).replace("\\n", "\n");
			MessageTemplateDto dto = new MessageTemplateDto();
			dto.setFunctionId(template.getFunctionId());
			dto.setTemplateName(template.getTemplateName());
			dto.setMessageSubject(template.getMessageSubject());
			dto.setContent(content);
			dto.setContactEmail(cp.getContactEmail());
			dto.setCustomerId(customerId);
			result.setData(dto);
			result.setErrorMsg(initErrorMsg(GTAError.Success.SUCCESS));
			
		} catch (Exception e) {
			
			e.printStackTrace();
			result.setErrorMsg(initErrorMsg(GTAError.TemplateError.MESSAGE_TEMPLATE_OTHER_ERROR));
		}
				
		return result;
	}
	
	private String getName(CustomerProfile cp) {
		
		if (cp == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName()).append(" ").append(cp.getSurname());
		return sb.toString();
	}
	
	
	@RequestMapping(value = "/templateType/list", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getTemplateTypeList() {
		
		logger.info("MessageTemplateController.getTemplateTypeList invocation start ...");
		
		try {
			
			return templateService.getTemplateTypeList();
			
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(TemplateError.MESSAGE_TEMPLATE_OTHER_ERROR);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/subtype/{templateType}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSubTemplateType(@PathVariable(value="templateType") String templateType) {
		
		logger.info("MessageTemplateController.getSubTemplateType invocation start ...");
		
		try {
			
			return templateService.getTemplatesByType(templateType);
			
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(TemplateError.MESSAGE_TEMPLATE_OTHER_ERROR);
			return responseResult;
		}
	}
	
}
