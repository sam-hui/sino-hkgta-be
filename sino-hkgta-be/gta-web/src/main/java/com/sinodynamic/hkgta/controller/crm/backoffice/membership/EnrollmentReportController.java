package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.EnrollmentReportService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Controller
public class EnrollmentReportController extends ControllerBase {
	@Autowired
	EnrollmentReportService enrollmentReportService;
	
	@RequestMapping(value="membership/enrollment_report",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult enrollmentReport(
			
			@RequestParam(value = "sortBy", defaultValue = "enrollDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") boolean isAscending,
			@RequestParam(value = "searchText", defaultValue = "") String searchText,
			@RequestParam(value = "pageNumber", defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			@RequestParam(value = "date", defaultValue = "1970/1/1 00:00:00") Date date){
		try{
			Data result = this.enrollmentReportService.generateEnrollmentReport(sortBy, isAscending?"asc":"desc", pageSize, pageNumber, date);
			BigDecimal totalAmount = this.enrollmentReportService.getMonthTotalAmount(date);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			
			resultMap.put("list", result);
			resultMap.put("totalAmount", totalAmount);
			//return new ResponseResult(ResponseMsgConstant.SUCCESS_MSG, resultMap);
			responseResult.initResult(GTAError.Success.SUCCESS, resultMap);
			return responseResult;
		}catch(Exception e){
			//ResponseResult error = new ResponseResult();
			//error.setReturnCode(ResponseMsgConstant.ERRORCODE);
			//error.setErrorMessageEN(e.getMessage());
			//return error;
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}
}
