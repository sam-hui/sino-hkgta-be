package com.sinodynamic.hkgta.controller.statement;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.statement.SearchStatementsDto;
import com.sinodynamic.hkgta.dto.statement.StatementParamsDto;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.crm.statement.DeliveryRecordService;
import com.sinodynamic.hkgta.service.crm.statement.MemberCashvalueBalHistoryService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/statement")
public class StatementController extends ControllerBase {
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	@Autowired
	private MemberCashvalueBalHistoryService memberCashvalueBalHistoryService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private DeliveryRecordService deliveryRecordService;
	
	@Autowired
	private UserMasterService userMasterService;
	
	@RequestMapping(value = "/generateManualStatement", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generateManualStatement(@RequestParam(value = "year", defaultValue = "", required = true) String year,
			@RequestParam(value = "month", defaultValue = "", required = true) String month) {
		try {
			customerOrderTransService.manualGenerateStatement(year,month);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Generate patron account statement failed!");
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/generatePatronAccountStatement", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generateStatement() {
		try {
			customerOrderTransService.generatePatronAccountStatement();;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Generate patron account statement failed!");
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/getStatements", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getStatements(
			@RequestParam(value = "sortBy", defaultValue = "customerId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters,
			@RequestParam(value = "month", defaultValue = "", required = true) String month,
			@RequestParam(value = "year", defaultValue = "", required = true) String year,
			@RequestParam(value = "memberType", defaultValue = "all", required = false) String memberType,
			@RequestParam(value = "deliveryStatus", defaultValue = "all", required = false) String deliveryStatus) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		
		SearchStatementsDto dto = new SearchStatementsDto(sortBy, pageNumber, pageSize, isAscending, month,year,memberType,deliveryStatus);
		ResponseResult responseResult = memberCashvalueBalHistoryService.getStatements(page,dto);
		return responseResult;
	}
	@RequestMapping(value = "/getStatementPDF", method = RequestMethod.GET,produces = "application/pdf")
	public @ResponseBody byte[] getStatementPDF(@RequestParam(value = "month", defaultValue = "", required = true) String month,
			@RequestParam(value = "year", defaultValue = "", required = true) String year,
			@RequestParam(value = "customerId", defaultValue = "", required = true) Long customerId,HttpServletRequest request,HttpServletResponse response){
		String url = request.getHeader("Referer");
		String base64Token = request.getHeader("token");
		GTAError errorCode = null;
		boolean valid = false;
		if (null==base64Token || StringUtils.isEmpty(base64Token))
		{
			base64Token = request.getParameter("token");
		}
		//If session is expire or token is invalid, redirect to login page.
		if (StringUtils.isEmpty(base64Token))
		{
			try {
				response.sendRedirect(url);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else {
			
			byte[] t = Base64.decodeBase64(base64Token);
			String authToken = new String(t);
			valid = TokenUtils.validateUUIDToken(authToken);
			errorCode = userMasterService.updateSessionToken(authToken);
			if (!valid || !GTAError.Success.SUCCESS.equals(errorCode))
			{
				try {
					response.sendRedirect(url);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		StatementParamsDto statementParamsDto = new StatementParamsDto();
		statementParamsDto.setMonth(month);
		statementParamsDto.setYear(year);
		statementParamsDto.setSent(false);
		statementParamsDto.setCustomerIds(new Long[]{customerId});
		statementParamsDto.setUserId(getUser().getUserId());
		ResponseResult responseResult = null;;
		try {
			responseResult = customerOrderTransService.getStatementPDF(statementParamsDto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new byte[]{1};
		}
		return (byte[])responseResult.getData();
	}
	
	@RequestMapping(value = "/sendStatement", method = RequestMethod.POST)
	public @ResponseBody ResponseResult sendStatement(@RequestBody StatementParamsDto statementParamsDto){
		try {
			statementParamsDto.setSent(true);
			statementParamsDto.setUserId(getUser().getUserId());
			return customerOrderTransService.getStatementPDF(statementParamsDto);
		} catch(GTACommonException ge){
			logger.debug("StatementController.sendStatement", ge);
			responseResult.initResult(ge.getError());
			return responseResult;
		}catch (Exception e) {
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			logger.debug("StatementController.sendStatement", e);
			return responseResult;
		}
		
	}
	
	@RequestMapping(value = "/getDeliveryStatment", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDeliveryStatement(
			@RequestParam(value = "sortBy", defaultValue = "deliveryDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters){
		
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		return deliveryRecordService.getDeliveryRecord(page);
		
	}
	
	@RequestMapping(value = "/getDeliveryStatmentDetail", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDeliveryStatmentDetail(
			@RequestParam(value = "sortBy", defaultValue = "academyId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam(value = "jobId",  required = true) Long jobId,
			@RequestParam(value = "deliveryStatus", defaultValue = "ALL", required = false) String deliveryStatus,
			@RequestParam(value="filters",defaultValue="", required = false) String filters){
		
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		return deliveryRecordService.getDeliveryRecordDetail(page,jobId,deliveryStatus);
		
	}
	
	@RequestMapping(value = "/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult advanceSearch(@RequestParam(value="module",defaultValue="") String module) {
		responseResult.initResult(GTAError.Success.SUCCESS, deliveryRecordService.assembleQueryConditions(module));
		return responseResult;
	}
	
}
