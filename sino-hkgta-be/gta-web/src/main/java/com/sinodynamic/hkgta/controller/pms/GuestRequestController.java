package com.sinodynamic.hkgta.controller.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.AssignAdHocTaskDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.pms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.pms.RoomHousekeepTaskService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;
import com.sinodynamic.hkgta.util.constant.UserType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
public class GuestRequestController extends ControllerBase<RoomHousekeepTask> {

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;

	@RequestMapping(value = "/guestRequest/{taskId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getGuestRequest(@PathVariable(value = "taskId") Long taskId) {
		try {
			GuestRequestDto guestRequestDto = roomHousekeepTaskService.getGuestRequestDto(taskId);
			responseResult.initResult(GTAError.Success.SUCCESS, guestRequestDto);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}

		return responseResult;
	}

	@RequestMapping(value = "/guestRequest/{taskId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult removeGuestRequest(@PathVariable(value = "taskId") Long taskId) {
		try {
			LoginUser loginUser = getUser();
			String userId = loginUser.getUserId();
			boolean isSuccess = roomHousekeepTaskService.removeGuestRequestDto(taskId, userId);
			if (isSuccess) {
				responseResult.initResult(GTAError.Success.SUCCESS);
			}  else {
				responseResult.initResult(GTAError.HouseKeepError.REMOVE_GUEST_REQUEST);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}

		return responseResult;
	}

	@RequestMapping(value = "/guestRequest", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult addGuestRequest(@RequestBody GuestRequestDto guestRequestDto) {
		try {
			LoginUser loginUser = getUser();
			String userId = loginUser.getUserId();

			AssignAdHocTaskDto dto = new AssignAdHocTaskDto();
			dto.setRoomId(guestRequestDto.getRoomId());
			dto.setTaskDescription(guestRequestDto.getTaskDescription());
			responseResult = roomHousekeepTaskService.assignAdHocTask(dto, RoomHousekeepTaskStatus.PND.toString(), userId);

		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}

		return responseResult;
	}

	@RequestMapping(value = "/guestRequest/approve/{taskId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult approveGuestRequest(@PathVariable(value = "taskId") Long taskId,
			@RequestBody GuestRequestDto guestRequestDto) {
		try {
			String taskDescription = guestRequestDto.getTaskDescription();
			LoginUser loginUser = getUser();
			String userId = loginUser.getUserId();
			boolean isSuccess = roomHousekeepTaskService.approveGuestRequestDto(taskId, taskDescription, userId);
			if (isSuccess) {
				responseResult.initResult(GTAError.Success.SUCCESS);
			} else {
				responseResult.initResult(GTAError.HouseKeepError.APPROVE_GUEST_REQUEST);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}

		return responseResult;
	}

	private AdvanceQueryDto parseAdvQueryJson(String paraJson) throws Exception {
		if (paraJson != null && paraJson.length() > 0) {
			return new Gson().fromJson(paraJson, AdvanceQueryDto.class);
		} else {
			return (AdvanceQueryDto) Class.forName(AdvanceQueryDto.class.getName()).newInstance();
		}
	}

	@RequestMapping(value = "/guestRequest/search", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult searchGuestRequest(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@RequestParam(value = "isPending", required = false) Boolean isPending,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters
			/*@RequestBody(required = false) AdvanceQueryDto advanceQueryDto*/) {
		try {
			AdvanceQueryDto advanceQueryDto = parseAdvQueryJson(filters);
			StringBuilder hql = new StringBuilder(
					"select a.taskId as taskId, a.createDate as createDate, a.taskDescription as taskDescription, a.status as status, ");
			hql.append(
					"a.room.roomNo as roomNo from RoomHousekeepTask a, UserMaster u join a.room r where a.requesterUserId = u.userId ");
			hql.append(" and u.userType = '").append(UserType.CUSTOMER.getType()).append("' ");

			if (Boolean.FALSE.equals(isPending)) {
				hql.append(" and a.status in ('").append(RoomHousekeepTaskStatus.OPN.toString()).append("','");
				hql.append(RoomHousekeepTaskStatus.CAN.toString()).append("')  ");
			} else if (Boolean.TRUE.equals(isPending)) {
				hql.append(" and a.status in ('").append(RoomHousekeepTaskStatus.PND.toString()).append("')  ");
			}

			if (StringUtils.isNotEmpty(status) && (RoomHousekeepTaskStatus.CAN.toString().equals(status) || 
					RoomHousekeepTaskStatus.OPN.toString().equals(status))) {
				hql.append(" and a.status = '").append(status).append("' ");
			}
			
			if (advanceQueryDto.getRules() != null && !advanceQueryDto.getRules().isEmpty()) {
				hql.append(" and ");
			}
			page.setNumber(pageNumber);
			page.setSize(pageSize);

			responseResult = advanceQueryService.getAdvanceQueryResultByHQL(advanceQueryDto, hql.toString(), page,
					GuestRequestDto.class);

			if (responseResult != null && responseResult.getListData() != null) {

				List<?> list = responseResult.getListData().getList();
				for (Object obj : list) {
					GuestRequestDto roomHousekeepTaskDto = (GuestRequestDto) obj;
					String createDateStr = DateConvertUtil.getYMDDateAndDateDiff(roomHousekeepTaskDto.getCreateDate());
					roomHousekeepTaskDto.setCreateDateStr(createDateStr);
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

}