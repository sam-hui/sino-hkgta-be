package com.sinodynamic.hkgta.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class GtaRequestFilter extends GenericFilterBean
{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
HttpServletRequest req = (HttpServletRequest)request;
		
		String uri = req.getRequestURI();
		String context = req.getContextPath();
		String pathinfo = req.getPathInfo();
		
		if (!EncryptTool.getInstance().needEncrypt(pathinfo))
		{
			chain.doFilter(req, response);
			return;
		}
		
		if (EncryptTool.getInstance().requireEncrypt(pathinfo) && !uri.contains(EncryptTool.getInstance().ID_PREFIX))
		{
			ResponseResult result = new ResponseResult();
			result.initResult(GTAError.CommonError.Error, "Request is invalid!");
			response.getOutputStream().write((new ObjectMapper().writeValueAsString(result)).getBytes());
			response.setContentType("application/json");
			return;
		}
		
		if (uri.contains(EncryptTool.getInstance().ID_PREFIX))
		{
			uri = EncryptTool.getInstance().decryptIds(uri, true);
			uri = uri.replace(context, "");
		}
		
		ResettableStreamHttpServletRequest  requestWrapper = new ResettableStreamHttpServletRequest(req); 
		
		Map<String, String[]> params = req.getParameterMap();
		Map<String, String[]> newParams = new HashMap<String, String[]>();
		for(String name : params.keySet())
		{
			String[] newValues = decryptParamValue(params, name);
			newParams.put(name, newValues);
		}
		requestWrapper.setParameterMap(newParams);
		
		BufferedReader reader =requestWrapper.getReader();
		StringBuffer json = new StringBuffer();
		String line = null;
		while ((line = reader.readLine()) != null) {
		 json.append(line);
		}
		reader.close();
		String newJson = EncryptTool.getInstance().decryptIds(json.toString(), true);
		
		requestWrapper.resetInputStream(newJson.getBytes());
		requestWrapper.setRequestURI(uri);
		
		chain.doFilter(requestWrapper, response);
	}

	private String[] decryptParamValue(Map<String, String[]> params, String name)
	{
		String[] values = params.get(name);
		String[] newValues = new String[values.length];
		int index = 0;
		for (String v : values)
		{
			if (!v.contains(EncryptTool.getInstance().ID_PREFIX))	
			{
				newValues[index] = v;
				index++;
				continue;
			}
			
			newValues[index] = EncryptTool.getInstance().decryptIds(v, true);
			index++;
		}
		
		return newValues;
	}

}
